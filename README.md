# Introdução

### Requisitos:

- Ubuntu 18.04 LTS: https://www.ubuntu.com/download/desktop
- mysql: `sudo apt install mysql-client php-mysql`
- git: `sudo apt install git`
- oh-my-zsh: https://github.com/robbyrussell/oh-my-zsh
- PHP: `sudo apt install php`
- Composer: Seguir instruções em: https://getcomposer.org/doc/faqs/how-to-install-composer-programmatically.md. Após executar o script será gerado um arquivo composer.phar que deve ser movido: `$ sudo mv composer.phar /usr/bin/composer`
- PHP x-debug: `$ sudo apt install php php-xdebug -y`
- php cs fixer e php-cs-fixer instalado como global pelo composer e disponível para acesso via $PATH:
   - `composer global require friendsofphp/php-cs-fixer`
   - `export PATH="$PATH:$HOME/.config/composer/vendor/bin"`
- Node.js >= 10: https://github.com/nodesource/distributions/blob/master/README.md#installation-instructions
- VS Code: https://code.visualstudio.com/ e extensões:
   - Docker
   - DotENV
   - EditorConfig for VS Code
   - ESLint
   - GitLens
   - Markdown All in One
   - PHP Debug
   - PHP Inteliphense
   - Vetur
   - Vue 2 Snippets
   - Vue Peek
   - vuetify-vscode
- VS Code User Settings: https://gist.github.com/marciodojr/c5650443b6653325a5af162568d86a05
- VS Code Key Bindings: https://gist.github.com/marciodojr/b2ee3832cb3f4eb2d47f258d1cac4753
- Docker (CE): https://docs.docker.com/install/linux/docker-ce/ubuntu/
- Seguir a seção “Manage Docker as a non-root user” na seguinte página: https://docs.docker.com/install/linux/linux-postinstall/
- Docker Compose: https://docs.docker.com/compose/install/#install-compose

### Iniciando o Projeto:

1. Clonar o projeto a partir do repositório no GitLab.
2. Executar o comando `docker-compose up` na raíz do projeto.
3. Após o build:
   - Em outro terminal, acessar o container do php através do comando `docker exec -it dpphp bash`
   - Instalar as dependências com o comando `composer install`
4. Aplicar os patches no banco de dados (raíz do projeto - fora de qualquer container):
   - Criar o arquivo settings.local.php dentro do diretório app/config:
```php
<?php

// app/config/settings.local.php

return [
    'db_patch' => [
        'host' => '127.0.0.1',
        'db_name' => 'deliveryprint',
        'db_user' => 'deliveryprint',
        'db_pass' => 'deliveryprint',
        'db_port' => '13306',
        'charset' => 'utf8mb4'
    ]
];
```
   - Verificar os patches já instalados através do comando `composer dbpatch-status`. Deve mostrar vários patchs na seção “patches to apply” e a seção “applied patches” deve estar vazia, com a mensagem “no patches found”
   - Aplicar os patches pendentes através do comando `composer dbpatch-update`
5. Executar o comando `chmod a+w app/data/print/`  na raíz do projeto para dar permissão de leitura e escrita a esse diretório.

### Alterações no banco de dados

1. Fazer login no container do banco de dados com o comando `docker exec -it dpsql bash`.
2. Acessar o banco: `mysql -u deliveryprint -p deliveryprint`. Senha: `deliveryprint`.
3. Os patches que são aplicados no início do projeto criam alguns registros no banco que não são mais necessários. Executar os seguintes comandos para apagá-los:
```sql
DELETE FROM tbarquivo;
DELETE FROM tbitem;
DELETE FROM tbpedido;
DELETE FROM tbcliente;
DELETE FROM tbendereco;
DELETE FROM tbusuariopadrao;
DELETE FROM tbpdftemp;
```

### Teste do fluxo de pedidos
1. Acessar o endereço https://localhost:3000.
   - Se o browser acusar que a conexão não é segura, adicionar uma exceção clicando em Advanced > Add Exception... > Confirm Security Exception no Firefox ou Advanced > Proceed to localhost no Chrome
2. Fazer um pedido normalmente como um cliente comum. Para fazer um teste com cartão de crédito, utilizar os seguintes dados:
   - Número: 4111 1111 1111 1111
   - Nome e Sobrenome
   - Código de verificação: 158
   - Validade: 01/21
3. Criar um usuário admin:
   - Criar um usuário normalmente pelo site.
   - Acessar o banco de dados e alterar a coluna “is_adm” desse cliente para o valor 1.
4. Acessar a tela do admin https://localhost:3000/adm/login e fazer login com o usuário criado.
5. Verificar se o pedido que foi feito está listado e se os dados estão corretos.


**Importante**: Para evitar problemas de caching em navegadores, durante o desenvolvimento, recomenda-se desativar o cache na janela de debug (rede) do navegador.
