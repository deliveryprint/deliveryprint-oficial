<?php

namespace IntecPhp;

use IntecPhp\View\Layout;

return [
    [
        'pattern' => '/impressao-apostila/amp',
        'callback' => function () {
            $layout = new Layout();
            $layout
                ->setCanonical('/impressao-apostila')
                ->setMetaDescription('Impressão de apostila barata online com qualidade e agilidade é na DeliveryPrint. Orçamento em segundos, pagamento facilitado e frete para todo país. Reduza seus custos com impressão, temos uma solução rápida de impressão e com bom atendimento para você.')
                ->appendTitle('Impressão de Apostila', ' | ')
                ->setLayout('layout-amp')
                ->render('produto/amp-template-produto', [
                    'title'     => 'Impressão de Apostilas', // = msg_b
                    'subtitle'  => 'Leve o conhecimento na mochila', //msg_c
                    'breadc'    => 'Apostilas', //msg_a
                    'img_url'   => '/img/Impressao-de-apostila.jpg',
                    'li_es'     => [
                        'Imprima apostilas para sala de aula ou estudo em casa',
                        'Impressão e encadernação em um só lugar',
                        'Colorida ou preto e branco, em tamanho A4',
                    ],
                    'price' => '7,00',
                    'unit'  => 'encadernação',
                    'a_0'   => '',
                    'a_1'   => '',
                    'a_2'   => '',
                    'a_3'   => '',
                    'a_4'   => true,
                    'a_5'   => '',
                    'text'  => 'Apostila para estudo? Apostila para treinamento? Impressão de apostila barata? É com a DeliveryPrint! A sua Gráfica Digital Online! Somos especialistas em impressão de apostila, colorida ou preto e branco, com encadernação em espiral, wire-o e muito mais! E o melhor: faça tudo no conforto de sua casa! Peça um orçamento em nosso site, e-mail ou whatsapp e receba sua apostila aonde for! Entregamos para todo o Brasil! Trabalhamos com diferentes tipos de papel, A5, A4, A3, sulfite, couchê e de encadernação, nunca foi tão fácil imprimir! Não deixe de fazer o seu pedido ou solicitar seu orçamento! Preço baixo e qualidade é com a DeliveryPrint, seu serviço de impressão online!'
                ]);
        },
    ],
    [
        'pattern' => '/impressao-colorida/amp',
        'callback' => function () {
            $layout = new Layout();
            $layout
                ->setCanonical('/impressao-colorida')
                ->setMetaDescription('Impressão colorida barata Online com qualidade é na DeliveryPrint. Orçamento em segundos, pagamento facilitado e frete para todo país. Peça sua Impressão online em Colorido já, é Simples, Rápido e Seguro.')
                ->appendTitle('Impressão Colorida', ' | ')
                ->setLayout('layout-amp')
                ->render('produto/amp-template-produto', [
                    'title'     => 'Impressão Colorida', // = msg_b
                    'subtitle'  => 'Cause impacto no primeiro contato', //msg_c
                    'breadc'    => 'Impressão Colorida', //msg_a
                    'img_url'   => '/img/Impressao-colorida.jpg',
                    'li_es'     => [
                        'Impressione a todos',
                        'Escolha o Melhor Acabamento',
                        'Alta Qualidade de Impressão',
                    ],
                    'price' => '0,43',
                    'unit'  => 'página', //unidade ou página.
                    'a_0'   => '',
                    'a_1'   => '',
                    'a_2'   => '',
                    'a_3'   => '',
                    'a_4'   => true,
                    'a_5'    => '',
                    'text'  => 'Aqui na DeliveryPrint, seu serviço de impressão online, você pode confiar que sua impressão colorida sairá do modo como você precisa! Trabalhamos com as melhores impressoras do mercado em gráficas digitais. Imprimir colorido é é imprimir online na DeliveryPrint! Do conforto de sua casa, seja para impressão de apostilas, TCC’s, materiais de estudo ou treinamento, faremos sua impressão colorida do modo que você quer, com rapidez e alta qualidade, com entregas para todo o Brasil e entregas no mesmo dia para a região metropolitana de São Paulo! Pensou em impressão online, impressão colorida? Pensou na DeliveryPrint! A sua Gráfica Digital Online. Peça um orçamento!'
                ]);
        },
    ],
    [
        'pattern' => '/encadernacao-wire-o/amp',
        'callback' => function () {
            $layout = new Layout();
            $layout
                ->setCanonical('/encadernacao-wire-o')
                ->setMetaDescription('Encadernação em Wire-o com Impressão a laser de Alta Qualidade é na DeliveryPrint. Orçamento em segundos, pagamento facilitado e frete para todo país.')
                ->appendTitle('Encadernação em Wire-o', ' | ')
                ->setLayout('layout-amp')
                ->render('produto/amp-template-produto', [
                    'title'     => 'Encadernação em Wire-o', // = msg_b
                    'subtitle'  => 'Acabamento Premium para Impressionar', //msg_c
                    'breadc'    => 'Encadernação Wire-o', //msg_a
                    'img_url'   => '/img/Encadernação em Wire-o.png',
                    'li_es'     => [
                        'Impressão em Preto e Branco ou Colorido',
                        'Para Apresentações, Catálogos, Mostruário e muito mais',
                        'Impressione com a Alta Qualidade',
                    ],
                    'price' => '12,00',
                    'unit'  => 'unidade', //unidade ou página.
                    'a_0'   => '',
                    'a_1'   => '',
                    'a_2'   => '',
                    'a_3'   => '',
                    'a_4'   => true,
                    'a_5'   => '',
                    'text'  => 'Na DeliveryPrint, a sua gráfica digital online, temos os melhores preços para encadernação em wire-o do mercado. Ideal para mostruários, catálogos e apresentações, indicamos a encadernação wire-o como uma opção de baixo custo com acabamento diferenciado. Para todas as suas necessidades, oferecemos encadernação em A4, A3 e A5, em diferentes tipos de papel e de gramaturas. Não perca tempo! Quer imprimir mostruário, catálogo ou apresentação? Seja impressão em preto e branco ou impressão colorida, a DeliveryPrint resolve para você! Impressão de qualidade, preço baixo e retirada ou entrega rápida. DeliveryPrint, o seu serviço de impressão online com Nota Máxima no Google!'
                ]);
        },
    ],
    [
        'pattern' => '/impressao-barata/amp',
        'callback' => function () {
            $layout = new Layout();
            $layout
                ->setCanonical('/impressao-barata')
                ->setMetaDescription('Impressão Barata, várias opções de acabamentos e de Alta Qualidade é na DeliveryPrint. Orçamento em segundos, pagamento facilitado e frete para todo país.')
                ->appendTitle('Impressão Barata', ' | ')
                ->setLayout('layout-amp')
                ->render('produto/amp-template-produto', [
                    'title'     => 'Impressão Barata', // = msg_b
                    'subtitle'  => 'Alta qualidade por um Preço Baixo', //msg_c
                    'breadc'    => 'Impressão Barata', //msg_a
                    'img_url'   => '/img/Impressão Barata.jpg',
                    'li_es'     => [
                        'Impressão em Preto e Branco ou Colorido',
                        'Alta Qualidade em Impressão a Laser',
                        'Diversas opções de Acabamento',
                    ],
                    'price' => '0,11',
                    'unit'  => 'página', //unidade ou página.
                    'a_0'   => '',
                    'a_1'   => '',
                    'a_2'   => '',
                    'a_3'   => '',
                    'a_4'   => true,
                    'a_5'   => '',
                    'text'  => ''
                ]);
        },
    ],
    [
        'pattern' => '/encadernacao-espiral/amp',
        'callback' => function () {
            $layout = new Layout();
            $layout
                ->setCanonical('/encadernacao-espiral')
                ->setMetaDescription('Encadernação em Espiral para suas Impressões com Alta Qualidade é na DeliveryPrint. Orçamento em segundos, pagamento facilitado e frete para todo país.')
                ->appendTitle('Encadernação em Espiral', ' | ')
                ->setLayout('layout-amp')
                ->render('produto/amp-template-produto', [
                    'title'     => 'Encadernação em Espiral', // = msg_b
                    'subtitle'  => 'Acabamento que Impressiona', //msg_c
                    'breadc'    => 'Encadernação em Espiral', //msg_a
                    'img_url'   => '/img/Encadernação em Espiral.jpg',
                    'li_es'     => [
                        'Apostilas para Treinamentos, Cursos, Estudos',
                        'Para seu Negócio e todas necessidades',
                        'Agilidade na Entrega',
                    ],
                    'price' => '5,11',
                    'unit'  => 'unidade', //unidade ou página.
                    'a_0'   => '',
                    'a_1'   => '',
                    'a_2'   => '',
                    'a_3'   => '',
                    'a_4'   => true,
                    'a_5'   => '',
                    'text'  => 'Na DeliveryPrint, a sua gráfica digital online, temos os melhores preços para encadernação em espiral do mercado. Ideal para materiais de estudo, treinamento e cursos, trabalhamos com preços e condições diferenciadas para empresas e pessoas jurídicas, em geral. Para todas as suas necessidades, oferecemos encadernação em A4, A3 e A5, em diferentes tipos de papel e de gramaturas. Não perca tempo! Quer imprimir apostila? Precisa estudar e se dedicar? Precisa de impressão de apostila? Sua empresa fará um treinamento? A DeliveryPrint resolve para você! Impressão de qualidade, preço baixo e retirada ou entrega rápida. DeliveryPrint, o seu serviço de impressão online com Nota Máxima no Google!'
                ]);
        },
    ],
    [
        'pattern' => '/encadernacao-capa-dura/amp',
        'callback' => function () {
            $layout = new Layout();
            $layout
                ->setCanonical('/encadernacao-capa-dura')
                ->setMetaDescription('Encadernação em Capa Dura barato e com Alta Qualidade é na DeliveryPrint. Orçamento em segundos, pagamento facilitado e frete para todo país.')
                ->appendTitle('Encadernação em Capa Dura', ' | ')
                ->setLayout('layout-amp')
                ->render('produto/amp-template-produto', [
                    'title'     => 'Encadernação em Capa Dura', // = msg_b
                    'subtitle'  => 'Encadernação de Qualidade e Resistência', //msg_c
                    'breadc'    => 'Encadernação em Capa Dura', //msg_a
                    'img_url'   => '/img/Encadernação em Capa Dura.png',
                    'li_es'     => [
                        'Impressão em Preto e Branco e Colorido',
                        'Acabamento da Encadernação em <i>Hot Stamping</i>',
                        'Várias opções de Entrega',
                    ],
                    'price' => '81,73',
                    'unit'  => 'unidade', //unidade ou página.
                    'a_0'   => '',
                    'a_1'   => '',
                    'a_2'   => '',
                    'a_3'   => '',
                    'a_4'   => true,
                    'a_5'   => '',
                    'text'  => 'A encadernação em Capa Dura é perfeita para impressão de TCC (trabalho de conclusão de curso) e teses, em geral. Também é amplamente usada para livros-conta e outros documentos de contabilidade. Durabilidade, Qualidade e Resistência, além de uma apresentação superior e diferenciada, são sinônimos de Capa Dura. Seja para imprimir colorido ou imprimir em preto e branco, atendemos todas as suas demandas de Capa Dura, em tamanho A4 e A3 e diferentes tipos de papel, com preço baixo e entrega rápida, além de qualidade superior aos serviços de impressão, online ou físico. Não perca tempo: peça seu orçamento na DeliveryPrint, sua Gráfica Digital Online com nota máxima no Google.'
                ]);
        },
    ],
    [
        'pattern' => '/impressao-certificado/amp',
        'callback' => function () {
            $layout = new Layout();
            $layout
                ->setCanonical('/impressao-certificado')
                ->setMetaDescription('Impressão de Certificados com entrega ágil e de Alta Qualidade é na DeliveryPrint. Orçamento em segundos, pagamento facilitado e frete para todo país.')
                ->appendTitle('Impressão de Certificados', ' | ')
                ->setLayout('layout-amp')
                ->render('produto/amp-template-produto', [
                    'title'     => 'Impressão de Certificado', // = msg_b
                    'subtitle'  => 'Exponha seu Sucesso', //msg_c
                    'breadc'    => 'Impressão de Certificado', //msg_a
                    'img_url'   => '/img/Impressão-de-certificado.jpg',
                    'li_es'     => [
                        'Impressão em Preto e Branco ou Colorido',
                        'Papel Couché de Alta Qualidade',
                        'Entrega ágil e Personalizada',
                    ],
                    'price' => '1,06',
                    'unit'  => 'página', //unidade ou página.
                    'a_0'   => '',
                    'a_1'   => '',
                    'a_2'   => '',
                    'a_3'   => '',
                    'a_4'   => true,
                    'a_5'   => '',
                    'text'  => 'Na DeliveryPrint, sua Gráfica Digital Online, seu certificado tem a qualidade que seu sucesso merece. Seja impressão em preto e branco, seja impressão colorida, imprimir na DeliveryPrint é qualidade na certa! Impressão em papel couchê, em alta alvura, em diferentes gramaturas, de acordo com a sua necessidade. Temos preços especiais, os preços mais baixos do mercado, para Pessoas Jurídicas, além de entrega rápida e personalizada para todo o Brasil! A sua empresa precisa imprimir certificados de sucesso de seus funcionários ou clientes? Pensou em certificado, pensou na DeliveryPrint, o seu serviço de impressão online.'
                ]);
        },
    ],
    [
        'pattern' => '/impressao-couche/amp',
        'callback' => function () {
            $layout = new Layout();
            $layout
                ->setCanonical('/impressao-couche')
                ->setMetaDescription('Impressão em Papel Couché em A4 ou A3 com Alta Qualidade é na DeliveryPrint. Orçamento em segundos, pagamento facilitado e frete para todo país.')
                ->appendTitle('Impressão em Papel Couché', ' | ')
                ->setLayout('layout-amp')
                ->render('produto/amp-template-produto', [
                    'title'     => 'Impressão em Papel Couché', // = msg_b
                    'subtitle'  => 'Impressão de Alta Resolução e Elegância', //msg_c
                    'breadc'    => 'Impressão em Papel Couché', //msg_a
                    'img_url'   => '/img/Impressão Papel Couche.jpg',
                    'li_es'     => [
                        'Fidelidade nas Cores da sua Impressão',
                        'Excelência no Acabamento',
                        'Entregamos para todo Brasil com agilidade',
                    ],
                    'price' => '0,25',
                    'unit'  => 'página', //unidade ou página.
                    'a_0'   => '',
                    'a_1'   => '',
                    'a_2'   => '',
                    'a_3'   => '',
                    'a_4'   => true,
                    'a_5'   => '',
                    'text'  => 'Na DeliveryPrint o serviço de impressão em papel couchê tem alta resolução e fidelidade em suas cores. A Impressão em couchê é indicada para serviços de panfletos, fotos, imagens, cardápios, catálogos, cartazes e muito mais. Nós trabalhamos com serviço de impressão e encadernação, com papel couchê em alta alvura, de diferentes gramaturas, tais como 115g, 170g até 300g e de tipo brilho e tipo fosco. Trabalhamos com diferentes tamanhos de papel e personalizações do pedido da maneira que o cliente necessita! Faça seu orçamento conosco, em nosso site, e-mail ou whatsapp. Impressão Online é com a DeliveryPrint: a sua Gráfica Digital Online.'
                ]);
        },
    ],
    [
        'pattern' => '/impressao-empresas/amp',
        'callback' => function () {
            $layout = new Layout();
            $layout
                ->setCanonical('/impressao-empresas')
                ->setMetaDescription('Serviço de Impressão para sua Empresa com toda comodidade sem ocupar espaço em seu Escritório. Impressão a laser de Alta qualidade,Orçamento em segundos, pagamento facilitado e frete para todo país.')
                ->appendTitle('Impressão para Empresas', ' | ')
                ->setLayout('layout-amp')
                ->render('produto/amp-template-produto', [
                    'title'     => 'Impressão para Empresas', // = msg_b
                    'subtitle'  => 'Comodidade, Qualidade e Preço Baixo', //msg_c
                    'breadc'    => 'Impressão Empresas', //msg_a
                    'img_url'   => '/img/Impressão_para_Empresas.jpg',
                    'li_es'     => [
                        'Impressão em Preto e Branco ou Colorido',
                        'Você pede, nós Produzimo e Entregamos',
                        'Qualidade e Sigilo',
                    ],
                    'price' => '0,11',
                    'unit'  => 'página', //unidade ou página.
                    'a_0'   => '',
                    'a_1'   => '',
                    'a_2'   => '',
                    'a_3'   => '',
                    'a_4'   => true,
                    'a_5'   => '',
                    'text'  => 'Sua empresa não precisa mais passar sufoco com gráficas de péssima qualidade ou impressoras emperrando! Na DeliveryPrint, sua Gráfica Digital Online, a qualidade de impressão é praxe, o preço baixo é costume e a entrega rápida é necessidade! Impressão em preto e branco ou colorida, com alta resolução, em diferentes tipos e tamanhos de papel, com atendimento personalizado e opções diferenciadas para empresas e pessoas jurídicas, com opções de faturamento, entrega expressa e sigilosidade garantida de seus materiais. Não perca tempo! Imprima com qualidade diferenciada com a DeliveryPrint, o serviço de impressão online com Nota Máxima no Google Avaliações!'
                ]);
        },
    ],
    [
        'pattern' => '/copiadora-faria-lima/amp',
        'callback' => function () {
            $layout = new Layout();
            $layout
                ->setCanonical('/copiadora-faria-lima')
                ->setMetaDescription('Serviço de Impressão e Encadernação na Faria Lima com Alta Qualidade e bom preço é na DeliveryPrint. Orçamento em segundos, pagamento facilitado e frete para todo país. Faça seu Pedido sem sair de onde está e Economize. Melhor Preço da Região, vem conferir!')
                ->appendTitle('Copiadora Faria Lima', ' | ')
                ->setLayout('layout-amp')
                ->render('produto/amp-template-produto', [
                    'title'     => 'Copiadora na Faria Lima', // = msg_b
                    'subtitle'  => 'Sua Copiadora Online na Faria Lima', //msg_c
                    'breadc'    => 'Copiadora na Faria Lima', //msg_a
                    'img_url'   => '/img/Copiadora Faria Lima.jpg',
                    'li_es'     => [
                        'Diversas Opções em Impressão',
                        'Encadernamos seu material da forma que você precisa',
                        'Entregamos na Faria Lima e em todo Brasil',
                    ],
                    'price' => '0,11',
                    'unit'  => 'página', //unidade ou página.
                    'a_0'   => '',
                    'a_1'   => '',
                    'a_2'   => '',
                    'a_3'   => '',
                    'a_4'   => true,
                    'a_5'   => '',
                    'text'  => 'Está a procura de gráfica digital e copiadora na Faria Lima e região? Conheça a DeliveryPrint, o serviço de impressão online com nota máxima no Google Avaliações. Faça seu pedido online, com poucos cliques em nosso site, diretamente de sua empresa ou de sua casa e receba até no mesmo dia. Preços e condições especiais para empresas e pessoas jurídicas. Peça seu orçamento pelo e-mail ou whatsapp e se surpreenda com a facilidade. Impressão colorida ou impressão em preto e branco com qualidade e preço baixo garantidos! Imprimir colorido ou imprimir em preto e branco nunca foi tão fácil! Diversos tipos de papéis, acabamentos e produtos. DeliveryPrint: a sua Gráfica Digital Online!'
                ]);
        },
    ],
    [
        'pattern' => '/copiadora-vila-madalena/amp',
        'callback' => function () {
            $layout = new Layout();
            $layout
                ->setCanonical('/copiadora-vila-madalena')
                ->setMetaDescription('Serviço de Impressão e Encadernação na Vila Madalena com Alta Qualidade e bom preço é na DeliveryPrint. Orçamento em segundos, pagamento facilitado e frete para todo país. Faça seu Pedido sem sair de onde está e Economize. Melhor Preço da Vila Madalena!')
                ->appendTitle('Copiadora na Vila Madalena', ' | ')
                ->setLayout('layout-amp')
                ->render('produto/amp-template-produto', [
                    'title'     => 'Copiadora na Vila Madalena', // = msg_b
                    'subtitle'  => 'Você não precisa mais se Deslocar para Imprimir', //msg_c
                    'breadc'    => 'Copiadora na Vila Madalena', //msg_a
                    'img_url'   => '/img/Impressão Barata.jpg',
                    'li_es'     => [
                        'Impressão em Preto e Branco ou Colorido',
                        'Papel Sulfite, Couché, Carta, Ofício e Reciclado',
                        'Preço Baixo e Entrega para Todo lugar do Brasil',
                    ],
                    'price' => '0,11',
                    'unit'  => 'página', //unidade ou página.
                    'a_0'   => '',
                    'a_1'   => '',
                    'a_2'   => '',
                    'a_3'   => '',
                    'a_4'   => true,
                    'a_5'   => '',
                    'text'  => 'Procura por gráfica digital e copiadora na Vila Madalena e zona oeste de São Paulo? Faça seu pedido sem sair de casa ou de sua empresa! Com a DeliveryPrint, o seu serviço de impressão online, você pode fazer seu pedido de impressão colorida ou impressão em preto e branco com poucos cliques. Funcionamento completamente online! E mais: entregas até no mesmo dia! Não perca tempo, faça seu pedido ou peça seu orçamento por e-mail ou whatsapp. Valores e condições diferenciadas para empresas. Diversos produtos gráficos, diversos tipos de papel e de acabamento. Apostilas, cartões de visita, plotagens, papel couchê, produtos variados e de alta qualidade. Imprimir online é com a DeliveryPrint!'
                ]);
        },
    ],
    [
        'pattern' => '/copiadora-santo-amaro/amp',
        'callback' => function () {
            $layout = new Layout();
            $layout
                ->setCanonical('/copiadora-santo-amaro')
                ->setMetaDescription('Serviço de Impressão e Encadernação em Santo Amaro com Alta Qualidade e bom preço é na DeliveryPrint. Orçamento em segundos, pagamento facilitado e frete para todo país. Faça seu Pedido sem sair de onde está e Economize. Melhor Preço em Santo Amaro, confira!')
                ->appendTitle('Copiadora em Santo Amaro', ' | ')
                ->setLayout('layout-amp')
                ->render('produto/amp-template-produto', [
                    'title'     => 'Copiadora em Santo Amaro', // = msg_b
                    'subtitle'  => 'Você não precisa mais se Deslocar para Imprimir', //msg_c
                    'breadc'    => 'Copiadora em Santo Amaro', //msg_a
                    'img_url'   => '/img/Impressão Barata.jpg',
                    'li_es'     => [
                        'Impressão em Preto e Branco ou Colorido',
                        'Papel Sulfite, Couché, Carta, Ofício e Reciclado',
                        'Preço Baixo e Entrega para Todo lugar do Brasil',
                    ],
                    'price' => '0,11',
                    'unit'  => 'página', //unidade ou página.
                    'a_0'   => '',
                    'a_1'   => '',
                    'a_2'   => '',
                    'a_3'   => '',
                    'a_4'   => true,
                    'a_5'   => '',
                    'text'  => 'Procurando copiadora ou gráfica digital em Santo Amaro e região? Na Zona Sul de São Paulo, pensou em impressão, pensou na DeliveryPrint, o seu serviço de impressão online! Qualidade de impressão colorida e impressão em preto e branco, com os melhores preços do mercado! Não perca tempo e peça o seu orçamento por e-mail e whatsapp, ou faça o seu pedido em nosso site. Diversos tipos de papel, couchê, sulfite, em diferentes gramaturas e tipos de acabamento, como espiral e wire-o. Preços e condições diferenciados para empresas. Imprimir online e receber em casa até no mesmo dia: apenas na DeliveryPrint, a sua Gráfica Digital Online com nota máxima no Google Avaliações!'
                ]);
        },
    ],
    [
        'pattern' => '/copiadora-avenida-paulista/amp',
        'callback' => function () {
            $layout = new Layout();
            $layout
                ->setCanonical('/copiadora-avenida-paulista')
                ->setMetaDescription('Serviço de Impressão e Encadernação na Avenida Paulista com Alta Qualidade e bom preço é na DeliveryPrint. Orçamento em segundos, pagamento facilitado e frete para todo país. Faça seu Pedido sem sair de onde está e Economize. Melhor Preço na região da Avenida Paulista!')
                ->appendTitle('Copiadora na Avenida Paulista', ' | ')
                ->setLayout('layout-amp')
                ->render('produto/amp-template-produto', [
                    'title'     => 'Copiadora na Avenida Paulista', // = msg_b
                    'subtitle'  => 'Impressão de Qualidade perto de você!', //msg_c
                    'breadc'    => 'Copiadora na Av. Paulista', //msg_a
                    'img_url'   => '/img/Copiadora Av Paulista.jpg',
                    'li_es'     => [
                        'Impressão em A3, A4, A5 e muito mais',
                        'Colorido ou Preto e Branco, em Alta Resolução',
                        'Entrega Rápida e Preço Diferenciado',
                    ],
                    'price' => '0,11',
                    'unit'  => 'página', //unidade ou página.
                    'a_0'   => '',
                    'a_1'   => '',
                    'a_2'   => '',
                    'a_3'   => '',
                    'a_4'   => true,
                    'a_5'   => '',
                    'text'  => 'Você ou sua empresa estão cansados de gráficas e copiadoras com preços abusivos e péssima qualidade de impressão? Faça seu pedido com a DeliveryPrint e se surpreenda! Entregamos até no mesmo dia para a Avenida Paulista e região central de São Paulo. Impressão em preto e branco ou impressão colorida de alta qualidade e preço baixo. Imprimir online nunca foi tão fácil! Trabalhamos com preços e condições diferenciadas para empresas e pessoas jurídicas em geral. Diversos tipos de acabamento e de papel. Impressão online é com a DeliveryPrint, sua Gráfica Digital Online com nota máxima no Google Avaliações!'
                ]);
        },
    ],
    [
        'pattern' => '/copiadora-vila-olimpia/amp',
        'callback' => function () {
            $layout = new Layout();
            $layout
                ->setCanonical('/copiadora-vila-olimpia')
                ->setMetaDescription('Serviço de Impressão e Encadernação na Região da Vila Olímpia com Alta Qualidade e bom preço é na DeliveryPrint. Orçamento em segundos, pagamento facilitado e frete para todo país. Faça seu Pedido sem sair de onde está e Economize. Melhor Preço da Vila Olímpia!')
                ->appendTitle('Copiadora na Vila Olímpia', ' | ')
                ->setLayout('layout-amp')
                ->render('produto/amp-template-produto', [
                    'title'     => 'Copiadora na Vila Olímpia', // = msg_b
                    'subtitle'  => 'Você não precisa mais andar até uma Copiadora', //msg_c
                    'breadc'    => 'Copiadora na Vila Olímpia', //msg_a
                    'img_url'   => '/img/Copiadora Vila Olimpia.jpg',
                    'li_es'     => [
                        'Opções em Impressão',
                        'Opções em Encadernações',
                        'Entregamos onde e quando precisar',
                    ],
                    'price' => '0,11',
                    'unit'  => 'página', //unidade ou página.
                    'a_0'   => '',
                    'a_1'   => '',
                    'a_2'   => '',
                    'a_3'   => '',
                    'a_4'   => true,
                    'a_5'   => '',
                    'text'  => 'Procurando Gráfica Digital e Copiadora na Vila Olímpia? Impressão colorida e impressão em preto e branco em alta qualidade e com preço baixo? Na DeliveryPrint você tem tudo isso e muito mais! Peça online de sua casa ou de sua empresa e receba até no mesmo dia na região da Vila Olímpia. Entregamos para todo o Brasil! Condições e valores diferenciados para empresas e PJ’s. Faça seu pedido! Diversos tipos de papéis, acabamentos e serviços gráficos em geral: apostilas, materiais de treinamento, cartões de visita, panfletos, papel couchê, papel timbrado e muito mais! Gráfica Digital Online é com a DeliveryPrint, seu serviço de impressão online para todos os momentos!'
                ]);
        },
    ],
    [
        'pattern' => '/copiadora-vila-mariana/amp',
        'callback' => function () {
            $layout = new Layout();
            $layout
                ->setCanonical('/copiadora-vila-mariana')
                ->setMetaDescription('Serviço de Impressão e Encadernação na Vila Mariana com Alta Qualidade e bom preço é na DeliveryPrint. Orçamento em segundos, pagamento facilitado e frete para todo país. Faça seu Pedido sem sair de onde está e Economize. Melhor Preço da Vila Mariana e Região!')
                ->appendTitle('Copiadora Vila Mariana', ' | ')
                ->setLayout('layout-amp')
                ->render('produto/amp-template-produto', [
                    'title'     => 'Copiadora na Vila Mariana', // = msg_b
                    'subtitle'  => 'A Copiadora que vem até você!', //msg_c
                    'breadc'    => 'Copiadora na Vila Mariana', //msg_a
                    'img_url'   => '/img/Copiadora Vila Mariana.jpg',
                    'li_es'     => [
                        'Diferentes opções de Impressão',
                        'Várias opções em Papéis',
                        'Entrega Rápida e Preço Bom',
                    ],
                    'price' => '0,11',
                    'unit'  => 'página', //unidade ou página.
                    'a_0'   => '',
                    'a_1'   => '',
                    'a_2'   => '',
                    'a_3'   => '',
                    'a_4'   => true,
                    'a_5'   => '',
                    'text'  => 'Procurando copiadora ou gráfica digital na Vila Mariana e Zona Sul de São Paulo? Com a DeliveryPrint, seu serviço de impressão online, você ou sua empresa pode confiar que seu material será impresso do modo como você precisa. Trabalhamos com os melhores preços do mercado e materiais e impressoras de altíssima qualidade. Impressão em preto e branco ou impressão colorida em diversos tipos de papel e nos mais variados acabamentos. Não perca tempo e peça seu orçamento ou faça seu pedido em nosso site! Preços e condições diferenciadas para empresas, com entregas até no mesmo dia para a Vila Mariana. Imprimir online nunca foi tão fácil: peça com a DeliveryPrint e se surpreenda!'
                ]);
        },
    ],
    [
        'pattern' => '/copiadora-pinheiros/amp',
        'callback' => function () {
            $layout = new Layout();
            $layout
                ->setCanonical('/copiadora-pinheiros')
                ->setMetaDescription('Serviço de Impressão e Encadernação em Pinheiros com Alta Qualidade e bom preço é na DeliveryPrint. Orçamento em segundos, pagamento facilitado e frete para todo país. Faça seu Pedido sem sair de onde está e Economize. Melhor Preço em Pinheiros e região!')
                ->appendTitle('Copiadora em Pinheiros', ' | ')
                ->setLayout('layout-amp')
                ->render('produto/amp-template-produto', [
                    'title'     => 'Copiadora em Pinheiros', // = msg_b
                    'subtitle'  => 'Faça sua Impressão sem sair de onde está!', //msg_c
                    'breadc'    => 'Copiadora em Pinheiros', //msg_a
                    'img_url'   => '/img/Copiadora Pinheiros.jpg',
                    'li_es'     => [
                        'Impressão em Diversos tipos de Papel',
                        'Quanto maior o nº de páginas, maior o desconto',
                        'Entrega Ágil para suas necessidades',
                    ],
                    'price' => '0,11',
                    'unit'  => 'página', //unidade ou página.
                    'a_0'   => '',
                    'a_1'   => '',
                    'a_2'   => '',
                    'a_3'   => '',
                    'a_4'   => true,
                    'a_5'   => '',
                    'text'  => 'Pensou em copiadora e gráfica digital em Pinheiros, pensou na DeliveryPrint, o seu serviço de impressão online com entrega expressa até para o mesmo dia! Impressão colorida ou impressão em preto e branco com qualidade profissional e preço baixo comprovados! Peça seu orçamento por e-mail ou whatsapp ou faça seu pedido em nosso site. Imprimir online nunca foi tão fácil! Trabalhamos com diversos tipos de papel e de acabamento, com os melhores preços do mercado. Chega de passar sufoco com gráficas que não cumprem prazos e de péssima qualidade. Gráfica Digital e copiadora em Pinheiros e na Zona Oeste é com a DeliveryPrint!'
                ]);
        },
    ],
    [
        'pattern' => '/copiadora-moema/amp',
        'callback' => function () {
            $layout = new Layout();
            $layout
                ->setCanonical('/copiadora-moema')
                ->setMetaDescription('Serviço de Impressão e Encadernação em Moema com Alta Qualidade e bom preço é na DeliveryPrint. Orçamento em segundos, pagamento facilitado e frete para todo país. Faça seu Pedido sem sair de onde está e Economize. Melhor Preço em Moema, faça seu orçamento!')
                ->appendTitle('Copiadora em Moema', ' | ')
                ->setLayout('layout-amp')
                ->render('produto/amp-template-produto', [
                    'title'     => 'Copiadora em Moema', // = msg_b
                    'subtitle'  => 'Você não precisa mais se Deslocar para Imprimir', //msg_c
                    'breadc'    => 'Copiadora em Moema', //msg_a
                    'img_url'   => '/img/Impressão Barata.jpg',
                    'li_es'     => [
                        'Impressão em Preto e Branco ou Colorido',
                        'Papel Sulfite, Couché, Carta, Ofício e Reciclado',
                        'Preço Baixo e Entrega para Todo lugar do Brasil',
                    ],
                    'price' => '0,11',
                    'unit'  => 'página', //unidade ou página.
                    'a_0'   => '',
                    'a_1'   => '',
                    'a_2'   => '',
                    'a_3'   => '',
                    'a_4'   => true,
                    'a_5'   => '',
                    'text'  => 'Gráfica Digital e Copiadora em Moema é com a DeliveryPrint, o seu serviço de impressão online com qualidade e preço baixo garantidos. Entregas para todo o Brasil. Receba até no mesmo dia em Moema. Peça seu orçamento por whatsapp ou e-mail ou faça seu pedido em nosso site. Preços e condições diferenciadas para empresas e PJs. Impressão colorida ou impressão em preto e branco, diversos tipos de acabamento, de papéis e de serviços gráficos no geral: apostilas, materiais de estudo, panfletos, flyers, cartões de visita e muito mais! Imprimir online é com a DeliveryPrint. Faça seu pedido e se surpreenda: impressão online, com entrega em Moema, apenas com a DeliveryPrint!'
                ]);
        },
    ],
    [
        'pattern' => '/impressao-documentos/amp',
        'callback' => function () {
            $layout = new Layout();
            $layout
                ->setCanonical('/impressao-documentos')
                ->setMetaDescription('Impressão de Documentos e Várias opções de Acabamentos  é na DeliveryPrint. Orçamento em segundos, pagamento facilitado e frete para todo país. Alta qualidade aliada ao atendimento Personalizado')
                ->appendTitle('Impressão de Documentos', ' | ')
                ->setLayout('layout-amp')
                ->render('produto/amp-template-produto', [
                    'title'     => 'Impressão de Documentos', // = msg_b
                    'subtitle'  => 'Sigilo e Qualidade para quando você mais Precisa', //msg_c
                    'breadc'    => 'Impressão de Documentos', //msg_a
                    'img_url'   => '/img/Impressão_de_Documentos.jpg',
                    'li_es'     => [
                        'Impressão em Preto e Branco ou Colorido',
                        'Grampeado, Separado ou Encadernado',
                        'Segurança e Alta Qualidade',
                    ],
                    'price' => '0,20',
                    'unit'  => 'página', //unidade ou página.
                    'a_0'   => '',
                    'a_1'   => '',
                    'a_2'   => '',
                    'a_3'   => '',
                    'a_4'   => true,
                    'a_5'    => '',
                    'text'  => 'Imprimir em colorido ou imprimir em preto e branco nunca foi tão fácil! Apenas na DeliveryPrint, a sua Gráfica Digital Online, você pode confiar que a impressão de documentos de seu negócio sairá com a melhor qualidade do mercado, com preço baixo e entrega rápida. Oferecemos condições diferenciadas para empresas e pessoas jurídicas, com opções de faturamento e sigilosidade do material para impressão. Documentos grampeados, separados ou encadernados. Fazemos do jeito que você e sua empresa necessitam. Não perca tempo: peça seu orçamento pelo nosso site, e-mail ou Whatsapp. Conheça a DeliveryPrint, o serviço de impressão online com a melhor avaliação no Google!'
                ]);
        },
    ],
    [
        'pattern' => '/impressao-arquivos/amp',
        'callback' => function () {
            $layout = new Layout();
            $layout
                ->setCanonical('/impressao-arquivos')
                ->setMetaDescription('Impressão de Arquivos de forma ágil e recebendo em casa na hora que você escolher é na DeliveryPrint. Orçamento em segundos, pagamento facilitado e frete para todo país. Alta qualidade de Impressão e Preços abaixo do Mercado')
                ->appendTitle('Impressão de Arquivos', ' | ')
                ->setLayout('layout-amp')
                ->render('produto/amp-template-produto', [
                    'title'     => 'Impressão de Arquivos', // = msg_b
                    'subtitle'  => 'Anexe seus PDF e deixe o resto com a gente', //msg_c
                    'breadc'    => 'Impressão de Arquivos', //msg_a
                    'img_url'   => '/img/Impressão-de-Arquivos.jpg',
                    'li_es'     => [
                        'Impressão em Preto e Branco ou Colorido',
                        'Diversas opções de Acabamentos',
                        'Alta qualidade e Entrega ágil',
                    ],
                    'price' => '0,11',
                    'unit'  => 'página', //unidade ou página.
                    'a_0'   => '',
                    'a_1'   => '',
                    'a_2'   => '',
                    'a_3'   => '',
                    'a_4'   => true,
                    'a_5'   => '',
                    'text'  => 'Imprima seus arquivos com a DeliveryPrint, a sua Gráfica Digital Online com preço baixo e alta qualidade garantidas pelos nossos usuários. Impressão em preto e branco e impressão colorida, em diversos tipos e tamanhos de papel, acabamentos e encadernações variadas do modo que você quer. Preços e condições diferenciadas para empresas e pessoas jurídicas em geral. Imprimir online ficou fácil! Faça seu pedido sem sair de casa ou de sua empresa e receba até no mesmo dia! Entregas para todo o Brasil! Na DeliveryPrint, você pode confiar que sua impressão sairá do modo que você precisa, sem se preocupar com prazos e qualidade de impressão! Conheça nossos serviços e se surpreenda!'
                ]);
        },
    ],
    [
        'pattern' => '/impressao-encadernacao/amp',
        'callback' => function () {
            $layout = new Layout();
            $layout
                ->setCanonical('/impressao-encadernacao')
                ->setMetaDescription('Impressão a laser com alta resolução e Diversas opções de Encadernação é só na DeliveryPrint. Orçamento em segundos, pagamento facilitado e frete para todo país. Peça Impressão Online')
                ->appendTitle('Impressão e Encadernação', ' | ')
                ->setLayout('layout-amp')
                ->render('produto/amp-template-produto', [
                    'title'     => 'Impressão e Encadernação', // = msg_b
                    'subtitle'  => 'A Solução para todas as suas necessidades', //msg_c
                    'breadc'    => 'Impressão e Encadernação', //msg_a
                    'img_url'   => '/img/Impressão_e_Encadernação.jpg',
                    'li_es'     => [
                        'Impressão em Preto e Branco ou Colorido',
                        'Espiral, Wire-o, Capa Dura e Grampeado',
                        'Impressione com a Alta Qualidade',
                    ],
                    'price' => '5,11',
                    'unit'  => 'unidade', //unidade ou página.
                    'a_0'   => '',
                    'a_1'   => '',
                    'a_2'   => '',
                    'a_3'   => '',
                    'a_4'   => true,
                    'a_5'   => '',
                    'text'  => 'Precisando de serviços de impressão e encadernação? Conheça a DeliveryPrint, a sua Gráfica Digital Online, e nunca mais saia de casa para imprimir seus trabalhos! Peça online e receba até no mesmo dia! Entregas para todo o Brasil! Impressão colorida e impressão em preto e branco com alta qualidade e preço baixo! Condições e valores diferenciados para empresas! Estamos prontos para a sua demanda: apostilas de estudos, apostilas de treinamento, concurso e muito mais! Conheça nossos serviços gráficos e se surpreenda com a agilidade e qualidade. Imprimir online ficou fácil e barato. Diversos tipos de encadernação e de acabamento. DeliveryPrint: impressão do seu jeito!'
                ]);
        },
    ],
    [
        'pattern' => '/copiadora-cidade-jardim/amp',
        'callback' => function () {
            $layout = new Layout();
            $layout
                ->setCanonical('/copiadora-cidade-jardim')
                ->setMetaDescription('Serviço de Impressão e Encadernação na Cidade Jardim com Alta Qualidade e bom preço é na DeliveryPrint. Orçamento em segundos, pagamento facilitado e frete para todo país. Faça seu Pedido sem sair de onde está e Economize! Melhores preços e Prazos da Cidade Jardim!')
                ->appendTitle('Copiadora Cidade Jardim', ' | ')
                ->setLayout('layout-amp')
                ->render('produto/amp-template-produto', [
                    'title'     => 'Copiadora na Cidade Jardim', // = msg_b
                    'subtitle'  => 'Copiadora Online para suas necessidades', //msg_c
                    'breadc'    => 'Copiadora na Cidade Jardim', //msg_a
                    'img_url'   => '/img/Copiadora Cidade Jardim.jpg',
                    'li_es'     => [
                        'Opções em Impressões e Encadernações',
                        'Diversas opções de Entrega e Retirada',
                        'Preço que cai quanto mais você imprime!',
                    ],
                    'price' => '0,11',
                    'unit'  => 'página', //unidade ou página.
                    'a_0'   => '',
                    'a_1'   => '',
                    'a_2'   => '',
                    'a_3'   => '',
                    'a_4'   => true,
                    'a_5'   => '',
                    'text'  => 'Procurando copiadora e gráfica digital de qualidade e com preço baixo? Que faça sua impressão rapidamente? Com diversos tipos de acabamento, papéis e opções diferenciadas para empresas e pessoas jurídicas? Peça com a DeliveryPrint, o seu serviço de impressão online com nota máxima no Google Avaliações, e se surpreenda! Você não precisa sair de sua casa ou de sua empresa, nós fazemos todo o serviço para você e entregamos até no mesmo dia na Cidade Jardim e região, com entregas também para todo o Brasil! Impressão colorida ou impressão em preto e branco, nos mais variados papéis! Fazemos do jeito que você precisa! Peça seu orçamento ou faça o seu pedido em nosso site!'
                ]);
        },
    ],
    [
        'pattern' => '/copiadora-morumbi/amp',
        'callback' => function () {
            $layout = new Layout();
            $layout
                ->setCanonical('/copiadora-morumbi')
                ->setMetaDescription('Serviço de Impressão e Encadernação no Morumbi com Alta Qualidade e bom preço é na DeliveryPrint. Orçamento em segundos, pagamento facilitado e frete para todo país. Faça seu Pedido sem sair de onde está e Economize! Temos o Melhor preço e comodidade da Região!')
                ->appendTitle('Copiadora Morumbi', ' | ')
                ->setLayout('layout-amp')
                ->render('produto/amp-template-produto', [
                    'title'     => 'Copiadora no Morumbi', // = msg_b
                    'subtitle'  => 'Você não precisa mais se Deslocar para Imprimir', //msg_c
                    'breadc'    => 'Copiadora no Morumbi', //msg_a
                    'img_url'   => '/img/Impressão Barata.jpg',
                    'li_es'     => [
                        'Impressão em Preto e Branco ou Colorido',
                        'Papel Sulfite, Couché, Carta, Ofício e Reciclado',
                        'Preço Baixo e Entrega para Todo lugar do Brasil',
                    ],
                    'price' => '0,11',
                    'unit'  => 'página', //unidade ou página.
                    'a_0'   => '',
                    'a_1'   => '',
                    'a_2'   => '',
                    'a_3'   => '',
                    'a_4'   => true,
                    'a_5'   => '',
                    'text'  => 'Serviços de gráfica digital e copiadora no Morumbi e Zona Sul de São Paulo é com a DeliveryPrint, a sua gráfica digital online! Imprima online direto de sua casa ou de sua empresa e receba até no mesmo dia na região do Morumbi! Entregas para todo o Brasil! Valores e condições diferenciadas para empresas e PJs. Trabalhamos com diversos tipos de papéis - sulfite, couché, carta, ofício, reciclado -, diversos tipos de acabamento e encadernação e os mais variados serviços gráficos: apostilas, materiais de treinamento, plotagem, cartões de visita, panfletos e mais! Impressão colorida e impressão em preto e branco com qualidade e preço baixo! Impressão online é com a DeliveryPrint! Nos conheça!'
                ]);
        },
    ],
    [
        'pattern' => '/servico-impressao/amp',
        'callback' => function () {
            $layout = new Layout();
            $layout
                ->setCanonical('/servico-impressao')
                ->setMetaDescription('Serviço de Impressão com Impressão a laser de Alta Qualidade é na DeliveryPrint. Orçamento em segundos, pagamento facilitado e frete para todo país. Melhores Preços e Prazos!')
                ->appendTitle('Serviço de Impressão', ' | ')
                ->setLayout('layout-amp')
                ->render('produto/amp-template-produto', [
                    'title'     => 'Serviço de Impressão', // = msg_b
                    'subtitle'  => 'Impressão de Qualidade com Entrega Rápida', //msg_c
                    'breadc'    => 'Serviço de Impressão', //msg_a
                    'img_url'   => '/img/Impressao-colorida.jpg',
                    'li_es'     => [
                        'Impressão em Preto e Branco ou Colorido',
                        'Diversos tipos de Papel',
                        'Impressione com a Alta Qualidade',
                    ],
                    'price' => '0,11',
                    'unit'  => 'unidade', //unidade ou página.
                    'a_0'   => '',
                    'a_1'   => '',
                    'a_2'   => '',
                    'a_3'   => '',
                    'a_4'   => true,
                    'a_5'   => '',
                    'text'  => 'Serviço de impressão online com qualidade e preço baixo garantidos é na DeliveryPrint: a sua Gráfica Digital Online com nota máxima no Google Avaliações. Trabalhamos com diversos tipos de produtos, papéis e acabamentos. Não perca tempo e peça o seu orçamento! Imprimir em preto e branco ou imprimir colorido nunca foi tão fácil! Do conforto de sua casa ou diretamente de sua empresa, faça seu pedido e receba até no mesmo dia. Entregamos para todo o Brasil! Valores diferenciados para empresas e pessoas jurídicas em geral! Entre em contato com nosso time de atendimento e se surpreenda com a facilidade: DeliveryPrint, o serviço de impressão online para todas as ocasiões!'
                ]);
        },
    ],
    [
        'pattern' => '/impressao-alta-qualidade/amp',
        'callback' => function () {
            $layout = new Layout();
            $layout
                ->setCanonical('/impressao-alta-qualidade')
                ->setMetaDescription('Alta Qualidade em Impressão é na DeliveryPrint. Orçamento em segundos, pagamento facilitado e frete para todo país. Diversos tipos de Papel, fazemos encadernação com ótimos preço!')
                ->appendTitle('Impressão de Alta Qualidade', ' | ')
                ->setLayout('layout-amp')
                ->render('produto/amp-template-produto', [
                    'title'     => 'Impressão de Alta Qualidade', // = msg_b
                    'subtitle'  => 'Impressão que Atende até os Mais Críticos', //msg_c
                    'breadc'    => 'Impressão de Alta Qualidade', //msg_a
                    'img_url'   => '/img/Impressão Alta Qualidade.jpg',
                    'li_es'     => [
                        'Impressão Colorida e em Preto e Branco',
                        'Maiores resoluções do Mercado',
                        'Receba seu pedido ainda hoje',
                    ],
                    'price' => '0,11',
                    'unit'  => 'unidade', //unidade ou página.
                    'a_0'   => '',
                    'a_1'   => '',
                    'a_2'   => '',
                    'a_3'   => '',
                    'a_4'   => true,
                    'a_5'   => '',
                    'text'  => 'Impressão de alta qualidade e preço baixo é com a DeliveryPrint, a sua gráfica digital online! A melhor resolução e o melhor preço do mercado! Impressão digital a laser com fidelidade de cores! Oferecemos os mais variados serviços gráficos! Impressão colorida e impressão em preto e branco, nos mais variados papéis, tamanhos e acabamentos. Fazemos o pedido do modo que você quer! Valores e condições diferenciadas para empresas e PJs. Peça online e receba até no mesmo dia! Entregas para todo o Brasil! Imprimir online ficou fácil: com a DeliveryPrint você não precisa se preocupar com qualidade e prazo de entrega! Impressão online e gráfica digital é conosco! Peça seu orçamento!'
                ]);
        },
    ],
    [
        'pattern' => '/copiadora-campinas/amp',
        'callback' => function () {
            $layout = new Layout();
            $layout
                ->setCanonical('/copiadora-campinas')
                ->setMetaDescription('Serviço de Impressão e Encadernação na Cidade de Campinas com Alta Qualidade e bom preço é na DeliveryPrint. Faça seu Pedido totalmente Online. Receba seus Impressos de forma Rápida por um preço baixo.')
                ->appendTitle('Copiadora Campinas', ' | ')
                ->setLayout('layout-amp')
                ->render('produto/amp-template-produto', [
                    'title'     => 'Copiadora em Campinas', // = msg_b
                    'subtitle'  => 'Você não precisa se Deslocar para Imprimir', //msg_c
                    'breadc'    => 'Copiadora em Campinas', //msg_a
                    'img_url'   => '/img/Impressão Barata.jpg',
                    'li_es'     => [
                        'Impressão em Preto e Branco ou Colorido',
                        'Papel Sulfite, Couché, Carta, Ofício e Reciclado',
                        'Preço Baixo e Entrega para Todo Lugar do Brasil',
                    ],
                    'price'  => '0,11',
                    'unit'   => 'página', //unidade ou página.
                    'a_0'    => '',
                    'a_1'    => '',
                    'a_2'    => '',
                    'a_3'    => '',
                    'a_4'    => true,
                    'a_5'    => '',
                    'text'  => 'A procura de gráfica digital e copiadora em Campinas, Valinhos e região? Peça direto de sua casa ou empresa com a DeliveryPrint: o serviço de impressão online que entrega para todo o Brasil e em 2 dias úteis* para Campinas e região! Imprimir preto e branco e imprimir colorido com preço baixo e qualidade nunca foi tão fácil! Diversos tipos de papéis, de acabamentos, encadernação, serviços gráficos em geral: apostilas, materiais de treinamento e estudo, panfletos, plotagem e mais. Impressão colorida e impressão em preto e branco nas melhores impressoras, nos mais variados tamanhos. Peça conosco e se surpreenda! Pensou em impressão online, pensou na DeliveryPrint!'
            ]);
        },
    ],
    [
        'pattern' => '/copiadora-curitiba/amp',
        'callback' => function () {
            $layout = new Layout();
            $layout
                ->setCanonical('/copiadora-curitiba')
                ->setMetaDescription('Serviço de Impressão e Encadernação na Cidade de Curitiba com Alta Qualidade e bom preço é na DeliveryPrint. Faça seu pedido de Impressão e Encadernação por nosso site de forma simples e segura. Ótimos preços, entrega ágil, confira!')
                ->appendTitle('Copiadora Curitiba', ' | ')
                ->setLayout('layout-amp')
                ->render('produto/amp-template-produto', [
                    'title'     => 'Copiadora em Curitiba', // = msg_b
                    'subtitle'  => 'Você não precisa se Deslocar para Imprimir', //msg_c
                    'breadc'    => 'Copiadora em Curitiba', //msg_a
                    'img_url'   => '/img/Copiadora_Curitiba.jpg',
                    'li_es'     => [
                        'Impressão em Preto e Branco ou Colorido',
                        'Papel Sulfite, Couché, Carta, Ofício e Reciclado',
                        'Preço Baixo e Entrega para Todo lugar do Brasil',
                    ],
                    'price'  => '0,11',
                    'unit'   => 'página', //unidade ou página.
                    'a_0'    => '',
                    'a_1'    => '',
                    'a_2'    => '',
                    'a_3'    => '',
                    'a_4'    => true,
                    'a_5'    => '',
                    'text'  => 'Serviços de gráfica digital e copiadora em Curitiba é com a DeliveryPrint, o seu serviço de impressão online com nota máxima no Google! Faça seu pedido do conforto de sua casa ou de sua empresa! Receba rapidamente, em qualquer lugar do Brasil! Condições diferenciadas para empresas e PJ’s. Preço baixo e qualidade garantida em impressão colorida, impressão em preto e branco, com diversos tipos de acabamento e de papéis. Apostilas, panfletos, material de estudo e treinamento, flyers, cartão de visita e muito mais! Peça com a DeliveryPrint e se surpreenda: receba em pouco tempo em Curitiba e qualquer lugar no Paraná! Imprimir online nunca foi tão fácil! Conheça a DeliveryPrint!'
            ]);
        },
    ],
    [
        'pattern' => '/copiadora-belo-horizonte/amp',
        'callback' => function () {
            $layout = new Layout();
            $layout
                ->setCanonical('/copiadora-belo-horizonte')
                ->setMetaDescription('Serviço de Impressão e Encadernação na Cidade de Belo Horizonte e Região com Alta Qualidade e bom preço é na DeliveryPrint. Não precisa se deslocar, basta entrar em nosso site e fazer seu pedido de Impressão e Encadernação!')
                ->appendTitle('Copiadora Belo Horizonte', ' | ')
                ->setLayout('layout-amp')
                ->render('produto/amp-template-produto', [
                    'title'     => 'Copiadora em Belo Horizonte', // = msg_b
                    'subtitle'  => 'Você não precisa se Deslocar para Imprimir', //msg_c
                    'breadc'    => 'Copiadora em Belo Horizonte', //msg_a
                    'img_url'   => '/img/Copiadora Belo Horizonte.jpg',
                    'li_es'     => [
                        'Impressão em Preto e Branco ou Colorido',
                        'Papel Sulfite, Couché, Carta, Ofício e Reciclado',
                        'Preço Baixo e Entrega para Todo lugar do Brasil',
                    ],
                    'price'  => '0,11',
                    'unit'   => 'página', //unidade ou página.
                    'a_0'    => '',
                    'a_1'    => '',
                    'a_2'    => '',
                    'a_3'    => '',
                    'a_4'    => true,
                    'a_5'    => '',
                    'text'  => 'Procurar serviços de copiadora e gráfica digital em Belo Horizonte nunca foi tão fácil! Com a DeliveryPrint, a sua Gráfica Digital Online, você não precisa sair de sua casa ou de sua empresa para fazer a sua impressão! É isso mesmo, peça online e receba diretamente no endereço escolhido, em todo o Brasil! Impressão colorida ou impressão em preto e branco com qualidade e preço baixo garantidos, em diversos tipos de papel e acabamento, com opções diferenciadas de valores para empresas e pessoas jurídicas em geral! Não perca tempo e peça seu orçamento por whatsapp e e-mail ou faça seu pedido em nosso site. Imprimir colorido e preto e branco é na DeliveryPrint: nota máxima no Google Avaliações!'
            ]);
        },
    ],
    [
        'pattern' => '/copiadora-rio-de-janeiro/amp',
        'callback' => function () {
            $layout = new Layout();
            $layout
                ->setCanonical('/copiadora-rio-de-janeiro')
                ->setMetaDescription('Serviço de Impressão e Encadernação no Rio de Janeiro com Alta Qualidade e bom preço é na DeliveryPrint. Orçamento em segundos, pagamento facilitado e frete para todo país. Peça de onde estiver, entregamos para você com agilidade!')
                ->appendTitle('Copiadora Rio de Janeiro', ' | ')
                ->setLayout('layout-amp')
                ->render('produto/amp-template-produto', [
                    'title'     => 'Copiadora no Rio de Janeiro', // = msg_b
                    'subtitle'  => 'A melhor Impressão do RJ', //msg_c
                    'breadc'    => 'Copiadora no Rio de Janeiro', //msg_a
                    'img_url'   => '/img/Copiadora Rio de Janeiro.jpg',
                    'li_es'     => [
                        'Impressão de PDF, Apostilas, Relatórios, Material de Estudo e afins',
                        'Encadernação em Espiral, Wire-o e Capa Dura',
                        'Entrega Personalizada para sua Demanda',
                    ],
                    'price'  => '0,11',
                    'unit'   => 'página', //unidade ou página.
                    'a_0'    => '',
                    'a_1'    => '',
                    'a_2'    => '',
                    'a_3'    => '',
                    'a_4'    => true,
                    'a_5'    => '',
                    'text'  => 'Com a DeliveryPrint, seu serviço de impressão online, você não precisará mais se preocupar com impressões de baixa qualidade. Trabalhamos com as melhores máquinas do mercado, impressão colorida e impressão em preto e branco de alta qualidade e preço baixo. Condições e valores diferenciados para pessoas jurídicas em geral. Serviços de copiadora e gráfica digital, como apostilas, relatórios, materiais de estudo e treinamento, cartões de visita e afins. Peça de sua casa ou empresa e receba em qualquer local do Rio de Janeiro e do Brasil em pouco tempo. Faça seu orçamento e se surpreenda. Copiadora e Gráfica Digital no Rio de Janeiro é com a DeliveryPrint! Imprima conosco!'
            ]);
        },
    ],
    [
        'pattern' => '/impressao-material-estudo/amp',
        'callback' => function () {
            $layout = new Layout();
            $layout
                ->setCanonical('/impressao-material-estudo')
                ->setMetaDescription('Impressão a laser de Alta Qualidade é na DeliveryPrint. Imprima seus arquivos e Estude melhor! Orçamento em segundos, pagamento facilitado e frete para todo país. Seus materiais de Estudo com qualidade e Entrega ágil para onde você quiser!')
                ->appendTitle('Impressão para Estudo', ' | ')
                ->setLayout('layout-amp')
                ->render('produto/amp-template-produto', [
                    'title'     => 'Impressão de Material de Estudo', // = msg_b
                    'subtitle'  => 'Melhore o Rendimento de seus Estudos', //msg_c
                    'breadc'    => 'Material de Estudo', //msg_a
                    'img_url'   => '/img/Impressao Material de Estudo.jpg',
                    'li_es'     => [
                        'Impressão e Encadernação',
                        'Preço bom com Entrega Rápida',
                        'Facilidade de Leitura e em Anotações',
                    ],
                    'price'  => '0,11',
                    'unit'   => 'unidade', //unidade ou página.
                    'a_0'    => '',
                    'a_1'    => '',
                    'a_2'    => '',
                    'a_3'    => '',
                    'a_4'    => true,
                    'a_5'    => '',
                    'text'  => ''
            ]);
        },
    ],
    [
        'pattern' => '/impressao-concursos/amp',
        'callback' => function () {
            $layout = new Layout();
            $layout
                ->setCanonical('/impressao-concursos')
                ->setMetaDescription('Impressão a laser de Alta Qualidade é na DeliveryPrint. Orçamento em segundos, pagamento facilitado e frete para todo país. Imprima seu material para concursos, apostila de concursos e tudo mais, estude melhor com papeis impressos e apostilas de qualidade!')
                ->appendTitle('Impressão para Concurseiros', ' | ')
                ->setLayout('layout-amp')
                ->render('produto/amp-template-produto', [
                    'title'     => 'Impressão para Concursos', // = msg_b
                    'subtitle'  => 'Muito mais fácil para Estudar e fazer suas Marcações', //msg_c
                    'breadc'    => 'Impressão para Concursos', //msg_a
                    'img_url'   => '/img/Impressão Concursos.jpg',
                    'li_es'     => [
                        'Impressão em Preto e Branco',
                        'Impressão Colorida',
                        'Entrega Rápida e Preço Bom',
                    ],
                    'price'  => '0,11',
                    'unit'   => 'unidade', //unidade ou página.
                    'a_0'    => '',
                    'a_1'    => '',
                    'a_2'    => '',
                    'a_3'    => '',
                    'a_4'    => true,
                    'a_5'    => '',
                    'text'   => 'Material de estudo para concurso é com a DeliveryPrint, a Gráfica Digital Online com nota máxima no Google Avaliações. Peça direto de sua casa, sem precisar interromper seus estudos, e receba até no mesmo dia! Entregas para todo o Brasil! Diversos tipos de papel, acabamentos, encadernações e de tamanhos! Faça seu pedido em nosso site ou peça um orçamento por e-mail ou Whastapp e se surpreenda com a facilidade! Impressão em preto e branco e impressão colorida com qualidade e preço baixo. Imprimir online ficou fácil - e barato! Apostilas para concursos é com a DeliveryPrint! Imprimir em preto e branco ou imprimir colorido com a confiança de que está em boas mãos! Nos conheça!'
            ]);
        },
    ],
    [
        'pattern' => '/impressao-a4/amp',
        'callback' => function () {
            $layout = new Layout();
            $layout
                ->setCanonical('/impressao-a4')
                ->setMetaDescription('Impressão a laser em A4 com Alta Qualidade é na DeliveryPrint. Orçamento em segundos, pagamento facilitado e frete para todo país.Trabalhamos com várias opções de Acabamento para sua Impressão em A4, que pode ser em Preto e branco e Colorido!')
                ->appendTitle('Impressão em A4', ' | ')
                ->setLayout('layout-amp')
                ->render('produto/amp-template-produto', [
                    'title'     => 'Impressão em A4', // = msg_b
                    'subtitle'  => 'Qualidade no Papel mais famoso do Brasil', //msg_c
                    'breadc'    => 'Impressão em A4', //msg_a
                    'img_url'   => '/img/Copiadora.jpg',
                    'li_es'     => [
                        'Impressão em Preto e Branco ou Colorido',
                        'Diversas opções de acabamentos',
                        'Impressione com a Alta Qualidade',
                    ],
                    'price'  => '0,11',
                    'unit'   => 'unidade', //unidade ou página.
                    'a_0'    => '',
                    'a_1'    => '',
                    'a_2'    => '',
                    'a_3'    => '',
                    'a_4'    => true,
                    'a_5'    => '',
                    'text'  => 'Apenas na DeliveryPrint, a sua Gráfica Digital Online, você pode confiar que sua impressão em A4 sairá do jeito que você precisa. Quer imprimir em preto e branco, imprimir colorido? Trabalhamos com a melhor qualidade do mercado! A impressão em A4 é indicada para provas, TCCs, materiais de estudo e treinamento! Trabalhamos com diferentes tipos de papel, como sulfite e couchê, em diferentes gramaturas e com diferentes tipos de acabamento: grampeamento, encadernação wire-o, espiral e Capa Dura, sempre com o melhor preço de impressão do mercado, online ou físico. Fazemos sua impressão em A4 do jeito que você precisa! Imprimir colorido? Imprimir em preto e branco? É com a DeliveryPrint, o seu serviço de impressão online! Peça seu orçamento!'
            ]);
        },
    ],
    [
        'pattern' => '/impressao-a5/amp',
        'callback' => function () {
            $layout = new Layout();
            $layout
                ->setCanonical('/impressao-a5')
                ->setMetaDescription('Impressão a laser em A5 com Alta Qualidade é na DeliveryPrint. Orçamento em segundos, pagamento facilitado e frete para todo país. Imprima em A5 e receba onde quiser sem precisar sair de casa!')
                ->appendTitle('Impressão em A5', ' | ')
                ->setLayout('layout-amp')
                ->render('produto/amp-template-produto', [
                    'title'     => 'Impressão em A5', // = msg_b
                    'subtitle'  => 'A solução chegou!', //msg_c
                    'breadc'    => 'Impressão em A5', //msg_a
                    'img_url'   => '/img/Copiadora.jpg',
                    'li_es'     => [
                        'Impressão em Preto e Branco ou Colorido',
                        'Entrega ágil',
                        'Impressione com a Alta Qualidade',
                    ],
                    'price'  => '0,12',
                    'unit'   => 'página', //unidade ou página.
                    'a_0'    => '',
                    'a_1'    => '',
                    'a_2'    => '',
                    'a_3'    => '',
                    'a_4'    => '',
                    'a_5'    => '',
                    'text'  => 'Apenas na DeliveryPrint, a sua Gráfica Digital Online, você pode confiar que sua impressão em A5 sairá do jeito que você precisa. Trabalhamos com a melhor qualidade do mercado! A impressão em A5 é indicada para flyers, panfletos, materiais de treinamento e agendas! Trabalhamos com diferentes tipos de papel, como sulfite e couchê, em diferentes gramaturas e com diferentes tipos de acabamento: grampeamento, encadernação wire-o, espiral, com corte e refile, sempre com o melhor preço de impressão do mercado, online ou físico. Fazemos sua impressão em A5 do jeito que você precisa! Imprimir colorido? Imprimir em preto e branco? É com a DeliveryPrint, o seu serviço de impressão online! Peça seu orçamento!'
            ]);
        },
    ],
    [
        'pattern' => '/impressao-wingoo/amp',
        'callback' => function () {
            $layout = new Layout();
            $layout
                ->setCanonical('/impressao-wingoo')
                ->setMetaDescription('Impressão a laser em A3, A4 e A5 com Alta Qualidade é na DeliveryPrint. Orçamento em segundos, pagamento facilitado e frete para todo país. Somos Parceiros da Wingoo, faça sua impressão com desconto!')
                ->appendTitle('Impressão Wingoo', ' | ')
                ->setLayout('layout-amp')
                ->render('produto/amp-template-produto', [
                    'title'     => 'Impressão para Alunos Wingoo', // = msg_b
                    'subtitle'  => 'A solução chegou!', //msg_c
                    'breadc'    => 'Impressão Parceiro Wingoo', //msg_a
                    'img_url'   => '/img/Copiadora.jpg',
                    'li_es'     => [
                        'Impressão em Preto e Branco ou Colorido',
                        'Entrega ágil',
                        'Encadernações, várias opções de papel e Preço sem Igual',
                    ],
                    'price'  => '0,08',
                    'unit'   => 'página', //unidade ou página.
                    'a_0'    => '',
                    'a_1'    => '',
                    'a_2'    => '',
                    'a_3'    => '',
                    'a_4'    => true,
                    'a_5'    => '',
                    'text'  => ''
            ]);
        },
    ],
    [
        'pattern' => '/copiadora/amp',
        'callback' => function () {
            $layout = new Layout();
            $layout
                ->setCanonical('/copiadora')
                ->setMetaDescription('Copiadora para Impressão com Alta Qualidade é na DeliveryPrint. Orçamento em segundos, pagamento facilitado e frete para todo país. Somos uma espécie de Copiadora Online, um serviço de Impressão e Encadernação Online!')
                ->appendTitle('Copiadora', ' | ')
                ->setLayout('layout-amp')
                ->render('produto/amp-template-produto', [
                    'title'     => 'Copiadora', // = msg_b
                    'subtitle'  => 'Faça suas Impressões e Encadernações em poucos Cliques', //msg_c
                    'breadc'    => 'Copiadora Online', //msg_a
                    'img_url'   => '/img/Copiadora.jpg',
                    'li_es'     => [
                        'Impressão em Preto e Branco ou Colorido',
                        'Preço Baixo e Entrega Rápida',
                        'Diversas opções de Encadernações',
                    ],
                    'price'  => '0,11',
                    'unit'   => 'página', //unidade ou página.
                    'a_0'    => '',
                    'a_1'    => '',
                    'a_2'    => '',
                    'a_3'    => '',
                    'a_4'    => true,
                    'a_5'    => '',
                    'text'  => 'Gráfica Digital e Copiadora é na DeliveryPrint, seu serviço de impressão online! Faça seu pedido sem sair de casa, com apenas alguns cliques. Funcionamento 100% online. Entregas expressas até no mesmo dia e para todo o Brasil! Preços e condições especiais para empresas e pessoas jurídicas. Impressão em preto e branco e impressão colorida com qualidade e preço baixo comprovados! Diversos serviços gráficos, encadernação, apostilas, flyers e panfletos, plotagem, os mais variados papéis e acabamentos! Peça seu orçamento via e-mail e whatsapp ou faça seu pedido em nosso site! Imprimir online ficou fácil! Imprimir colorido e imprimir em preto e branco com segurança: só na DeliveryPrint!'
            ]);
        },
    ],
    [
        'pattern' => '/impressao-provas/amp',
        'callback' => function () {
            $layout = new Layout();
            $layout
                ->setCanonical('/impressao-provas')
                ->setMetaDescription('Impressão de Provas a laser em com Alta Qualidade é na DeliveryPrint. Orçamento em segundos, pagamento facilitado e frete para todo país. Faça Impressão de suas Provas, avaliações e receba com agilidade sem gastar muito!')
                ->appendTitle('Impressão de Provas', ' | ')
                ->setLayout('layout-amp')
                ->render('produto/amp-template-produto', [
                    'title'     => 'Impressão de Provas', // = msg_b
                    'subtitle'  => 'Agilidade, Sigilo e Preço Baixo', //msg_c
                    'breadc'    => 'Impressão de Provas', //msg_a
                    'img_url'   => '/img/Copiadora.jpg',
                    'li_es'     => [
                        'Impressão em Preto e Branco ou Colorido',
                        'Faturamento para Empresas e PJ',
                        'Impressione com a Alta Qualidade',
                    ],
                    'price'  => '0,11',
                    'unit'   => 'página', //unidade ou página.
                    'a_0'    => '',
                    'a_1'    => '',
                    'a_2'    => '',
                    'a_3'    => '',
                    'a_4'    => true,
                    'a_5'    => '',
                    'text'   => 'Impressão de provas para aplicação ou estudo; dados variáveis e gestão de informações educacionais é com a DeliveryPrint, sua Gráfica Digital Online. Segurança e sigilosidade das informações para aplicação de provas e materiais de estudo. Preços e condições diferenciadas para empresas e pessoas jurídicas em geral. Impressão colorida e impressão em preto e branco com qualidade e preço baixo, nos mais variados tipos de papel, tamanhos e acabamentos! Peça online e receba até no mesmo dia! Entregamos para todo o Brasil! Imprimir online com segurança, fidelidade ao material e aos prazos! Conheça a DeliveryPrint, o serviço de impressão online que será seu parceiro para todas as horas!'
            ]);
        },
    ],
    [
        'pattern' => '/avaliacoes/amp',
        'callback' => function () {
            $layout = new Layout();
            $layout
                ->setCanonical('/avaliacoes')
                ->setMetaDescription('Impressão de Avaliações a laser em com Alta Qualidade é na DeliveryPrint. Orçamento em segundos, pagamento facilitado e frete para todo país. Faça Impressão de suas Avaliações, provas e receba com agilidade sem gastar muito!')
                ->appendTitle('Impressão de Avaliações', ' | ')
                ->setLayout('layout-amp')
                ->render('produto/amp-template-produto', [
                    'title'     => 'Impressão de Avaliações', // = msg_b
                    'subtitle'  => 'Agilidade, Segurança e Preço Baixo', //msg_c
                    'breadc'    => 'Impressão de Avaliações', //msg_a
                    'img_url'   => '/img/Copiadora.jpg',
                    'li_es'     => [
                        'Impressão em Preto e Branco ou Colorido',
                        'Faturamento para Empresas e PJ',
                        'Impressione com a Alta Qualidade',
                    ],
                    'price'  => '0,11',
                    'unit'   => 'página', //unidade ou página.
                    'a_0'    => '',
                    'a_1'    => '',
                    'a_2'    => '',
                    'a_3'    => '',
                    'a_4'    => true,
                    'a_5'    => '',
                    'text'  => 'Impressão de dados variáveis e de gestão de informações educacionais é com a DeliveryPrint, a sua Gráfica Digital Online! Oferecemos a segurança e confidencialidade de suas informações e qualidade de impressão comprovada, além do preço baixo e condições diferenciadas de faturamento e entrega expressa para pessoas jurídicas e empresas. Imprimir colorido ou imprimir em preto e branco nunca foi tão fácil. Peça seu orçamento agora mesmo e se impressione com a facilidade e agilidade de nossos serviços. Pensou em imprimir avaliações e dados variáveis? Pensou na DeliveryPrint, o seu serviço de impressão online com a melhor avaliação no Google!'
            ]);
        },
    ],
    [
        'pattern' => '/papelaria/amp',
        'callback' => function () {
            $layout = new Layout();
            $layout
                ->setCanonical('/papelaria')
                ->setMetaDescription('Serviço de Impressão e Encadernação com Alta Qualidade é na DeliveryPrint. Orçamento em segundos, pagamento facilitado e frete para todo país. Somos uma espécie de Papelaria Online, um serviço de Impressão e Encadernação Online!')
                ->appendTitle('Impressão em Papelaria', ' | ')
                ->setLayout('layout-amp')
                ->render('produto/amp-template-produto', [
                    'title'     => 'Impressão em Papelaria', // = msg_b
                    'subtitle'  => 'Faça suas Impressões e Encadernações em poucos Cliques', //msg_c
                    'breadc'    => 'Impressão em Papelaria', //msg_a
                    'img_url'   => '/img/Copiadora.jpg',
                    'li_es'     => [
                        'Impressão em Preto e Branco ou Colorido',
                        'Preço Baixo e Entrega Rápida',
                        'Diversas opções de Encadernações',
                    ],
                    'price'  => '0,11',
                    'unit'   => 'página', //unidade ou página.
                    'a_0'    => '',
                    'a_1'    => '',
                    'a_2'    => '',
                    'a_3'    => '',
                    'a_4'    => true,
                    'a_5'    => '',
                    'text'  => 'Serviço de impressão online com preço baixo e qualidade garantida é com a DeliveryPrint, a sua Gráfica Digital com nota máxima no Google. Faça seu pedido de impressão colorida ou impressão em preto e branco pelo nosso site, e-mail ou Whatsapp. Não perca tempo e nem dinheiro: entregas expressas para a cidade de São Paulo até no mesmo dia e entregas para todo o Brasil com rapidez e agilidade. Trabalhamos com valores diferenciados para empresas e pessoas jurídicas em geral, com opções diferenciadas de pagamento. Imprimir online nunca foi tão fácil! Diversos tipos de papéis, tamanhos e de acabamentos! Peça com a DeliveryPrint, o melhor serviço de impressão online e físico!'
            ]);
        },
    ],
    [
        'pattern' => '/impressao-gabaritos/amp',
        'callback' => function () {
            $layout = new Layout();
            $layout
                ->setCanonical('/impressao-gabaritos')
                ->setMetaDescription('Impressão de gabaritos  a laser com Alta Qualidade é na DeliveryPrint. Orçamento em segundos, pagamento facilitado e frete para todo país. Somos um serviço de Impressão totalmente online!')
                ->appendTitle('Impressão de Gabaritos', ' | ')
                ->setLayout('layout-amp')
                ->render('produto/amp-template-produto', [
                    'title'     => 'Impressão de Gabaritos', // = msg_b
                    'subtitle'  => 'A solução para suas Impressões chegou!', //msg_c
                    'breadc'    => 'Impressão de Gabaritos', //msg_a
                    'img_url'   => '/img/Copiadora.jpg',
                    'li_es'     => [
                        'Impressão em Preto e Branco ou Colorido',
                        'Entrega ágil e Segurança',
                        'Preços Diferenciados para Empresas e PJ',
                    ],
                    'price'  => '0,11',
                    'unit'   => 'página', //unidade ou página.
                    'a_0'    => '',
                    'a_1'    => '',
                    'a_2'    => '',
                    'a_3'    => '',
                    'a_4'    => true,
                    'a_5'    => '',
                    'text'  => 'Impressão de dados variáveis e de gestão de informações educacionais é com a DeliveryPrint, a sua Gráfica Digital Online! Oferecemos a segurança e confidencialidade de suas informações e qualidade de impressão comprovada, além do preço baixo e condições diferenciadas de faturamento e entrega expressa para pessoas jurídicas e empresas. Imprimir colorido ou imprimir em preto e branco nunca foi tão fácil. Peça seu orçamento agora mesmo e se impressione com a facilidade e agilidade de nossos serviços. Pensou em imprimir gabaritos e dados variáveis? Pensou na DeliveryPrint, o seu serviço de impressão online com a melhor avaliação no Google!'
            ]);
        },
    ],
    [
        'pattern' => '/grafica-online/amp',
        'callback' => function () {
            $layout = new Layout();
            $layout
                ->setCanonical('/grafica-online')
                ->setMetaDescription('Fazemos serviços de Impressão e Encadernação de seus Arquivos em PDF e entregamos para todo Brasil com Agilidade e Segurança. Faça seu Pedido em Segundos e aproveite nossos preços baixos com Alta qualidade em Impressão!')
                ->appendTitle('Gráfica para Impressão', ' | ')
                ->setLayout('layout-amp')
                ->render('produto/amp-template-produto', [
                    'title'     => 'Impressão em Gráfica Online', // = msg_b
                    'subtitle'  => 'Serviços de Impressão e Encadernação Online', //msg_c
                    'breadc'    => 'Impressão em Gráfica Online', //msg_a
                    'img_url'   => '/img/Gráfica Online Impressão.jpg',
                    'li_es'     => [
                        'Impressão em Preto e Branco ou Colorido',
                        'Opções em Acabamentos',
                        'Pagamentos Facilitado',
                    ],
                    'price'  => '0,11',
                    'unit'   => 'página', //unidade ou página.
                    'a_0'    => '',
                    'a_1'    => '',
                    'a_2'    => '',
                    'a_3'    => '',
                    'a_4'    => true,
                    'a_5'    => '',
                    'text'  => ''
            ]);
        },
    ],
    [
        'pattern' => '/tcc/amp',
        'callback' => function () {
            $layout = new Layout();
            $layout
                ->setCanonical('/tcc')
                ->setMetaDescription('Impressão de seu TCC em Capa dura ou Espiral, preto e branco ou colorido, com Entrega Rápida para todo o Brasil. Faça seu Pedido de forma Rápida e Simples em nosso site pagando um preço baixo na Impressão!')
                ->appendTitle('Impressão de TCC', ' | ')
                ->setLayout('layout-amp')
                ->render('produto/amp-template-produto', [
                    'title'     => 'Impressão de TCC', // = msg_b
                    'subtitle'  => 'Alta qualidade para sua Aprovação!', //msg_c
                    'breadc'    => 'Impressão de TCC', //msg_a
                    'img_url'   => '/img/Impressão de TCC.jpg',
                    'li_es'     => [
                        'Impressão em Preto e Branco ou Colorido',
                        'Opções de Cores para o Acabamento',
                        'Faça seu Pedido em poucos segundos',
                    ],
                    'price'  => '0,11',
                    'unit'   => 'página', //unidade ou página.
                    'a_0'    => '',
                    'a_1'    => '',
                    'a_2'    => '',
                    'a_3'    => '',
                    'a_4'    => true,
                    'a_5'    => '',
                    'text'  => ''
            ]);
        },
    ],
    [
        'pattern' => '/monografia/amp',
        'callback' => function () {
            $layout = new Layout();
            $layout
                ->setCanonical('/monografia')
                ->setMetaDescription('Imprimimos sua Monografia com alta qualidade, preço justo e entrega rápida. Faça o pedido por nosso site em menos de 2min e escolha o melhor dia para receber um trabalho de alta qualidade!')
                ->appendTitle('Impressão de Monografia', ' | ')
                ->setLayout('layout-amp')
                ->render('produto/amp-template-produto', [
                    'title'     => 'Impressão de Monografia', // = msg_b
                    'subtitle'  => 'Qualidade e Agilidade no seu momento mais Importante', //msg_c
                    'breadc'    => 'Impressão de Monografia', //msg_a
                    'img_url'   => '/img/Monografia.jpg',
                    'li_es'     => [
                        'Impressão em Preto e Branco ou Colorido',
                        'Várias opções de Cores na Capa Dura',
                        'Entrega para todo Brasil',
                    ],
                    'price'  => '0,11',
                    'unit'   => 'página', //unidade ou página.
                    'a_0'    => '',
                    'a_1'    => '',
                    'a_2'    => '',
                    'a_3'    => '',
                    'a_4'    => true,
                    'a_5'    => '',
                    'text'  => 'Terminou o seu TCC ou sua Monografia e não sabe aonde imprimir com qualidade? Seus problemas acabaram. Apenas na DeliveryPrint, a sua Gráfica Digital Online, você pode confiar que a impressão já tem nota máxima garantida! Imprimir em preto e branco ou imprimir em colorido nunca foi tão fácil. Peça em casa e receba aonde quiser, com entregas expressas para todo o Brasil. Trabalhamos com diversos tipos de papel e acabamentos, como espiral, wire-o e Capa Dura, perfeita para TCCs e monografias em geral. Faça seu orçamento agora mesmo e impressione sua banca com a melhor impressão online do mercado! DeliveryPrint: 5,0 estrelas no Google! Nota 10 em toda banca!'
            ]);
        },
    ],
    [
        'pattern' => '/impressao-papel-couche/amp',
        'callback' => function () {
            $layout = new Layout();
            $layout
                ->setCanonical('/impressao-papel-couche')
                ->setMetaDescription('Impressão em Papel Couché em Alta Resolução com agilidade e preço justo. Orçamento facilitado, faça todas as etapas de pedido em nosso site. Fazemos impressão e Encadernação e entregamos para todo Brasil!')
                ->appendTitle('Impressão em Papel Couché', ' | ')
                ->setLayout('layout-amp')
                ->render('produto/amp-template-produto', [
                    'title'     => 'Impressão em Papel Couché', // = msg_b
                    'subtitle'  => 'Alta Resolução para todos os Momentos', //msg_c
                    'breadc'    => 'Impressão em Papel Couché', //msg_a
                    'img_url'   => '/img/Papel Couché.jpg',
                    'li_es'     => [
                        'Impressão em Preto e Branco ou Colorido',
                        'Papel mais Resistente',
                        'Diversas Gramaturas em A3, A4 e A5',
                    ],
                    'price'  => '0,64',
                    'unit'   => 'página', //unidade ou página.
                    'a_0'    => '',
                    'a_1'    => '',
                    'a_2'    => '',
                    'a_3'    => '',
                    'a_4'    => true,
                    'a_5'    => '',
                    'text'  => ''
            ]);
        },
    ],
    [
        'pattern' => '/impressao-preto-branco/amp',
        'callback' => function () {
            $layout = new Layout();
            $layout
                ->setCanonical('/impressao-preto-branco')
                ->setMetaDescription('Impressão em Preto e Branco com os Melhores preços do Mercado! Temos diversas opções de papéis para todas suas necessidades. Faça o pedido em nosso site em poucos segundos, entregamos para o Brasil todo!')
                ->appendTitle('Impressão em Preto e Branco', ' | ')
                ->setLayout('layout-amp')
                ->render('produto/amp-template-produto', [
                    'title'     => 'Impressão em Preto e Branco', // = msg_b
                    'subtitle'  => 'A impressão que agrada todo mundo', //msg_c
                    'breadc'    => 'Impressão em Preto e Branco', //msg_a
                    'img_url'   => '/img/Preto e Branco.jpg',
                    'li_es'     => [
                        'Preço baixo e Alta Resolução',
                        'Opções em Encadernações e Acabamentos',
                        'Entrega para todo o Brasil',
                    ],
                    'price'  => '0,19',
                    'unit'   => 'página', //unidade ou página.
                    'a_0'    => '',
                    'a_1'    => '',
                    'a_2'    => '',
                    'a_3'    => '',
                    'a_4'    => true,
                    'a_5'    => '',
                    'text'  => 'Está precisando imprimir em preto e branco? Imprimir barato? E com qualidade? Não perca tempo! Conheça a DeliveryPrint: sua Gráfica Digital Online! Trabalhamos com as melhores impressoras do mercado, sua impressão em preto e branco em alta qualidade, com preço baixo e entrega rápida para todo o Brasil! Nunca foi tão fácil imprimir: impressão online, do conforto de sua casa. Imprimir em A5, A4 ou A3? Papel Sulfite ou Papel Couchê? Imprimir apostilas de estudo e treinamento? Peça seu orçamento em nosso site, e-mail ou whatsapp e teste nossos serviços! Você não irá se arrepender! DeliveryPrint: seu serviço de impressão online!'
            ]);
        },
    ],
    [
        'pattern' => '/papel-timbrado/amp',
        'callback' => function () {
            $layout = new Layout();
            $layout
                ->setCanonical('/papel-timbrado')
                ->setMetaDescription('Impressão de Papel Timbrado com Alta Qualidade é na DeliveryPrint. Orçamento em segundos, pagamento facilitado e frete para todo país. Serviço Online e com atendimento personalizado!')
                ->appendTitle('Impressão de Papel Timbrado', ' | ')
                ->setLayout('layout-amp')
                ->render('produto/amp-template-produto', [
                    'title'     => 'Impressão de Papel Timbrado', // = msg_b
                    'subtitle'  => 'Faça suas Impressões e Encadernações em poucos Cliques', //msg_c
                    'breadc'    => 'Impressão de Papel Timbrado', //msg_a
                    'img_url'   => '/img/Copiadora.jpg',
                    'li_es'     => [
                        'Impressão em Preto e Branco ou Colorido',
                        'Preço Baixo e Entrega Rápida',
                        'Opções de Faturamento para Empresas',
                    ],
                    'price'  => '0,11',
                    'unit'   => 'página', //unidade ou página.
                    'a_0'    => '',
                    'a_1'    => '',
                    'a_2'    => '',
                    'a_3'    => '',
                    'a_4'    => true,
                    'a_5'    => '',
                    'text'   => 'Impressione seus clientes com materiais diferenciados, com a sua marca e seu negócio em evidência! Impressão online de alta qualidade. Papel timbrado é na DeliveryPrint, a sua Gráfica Digital Online! Oferecemos serviços de impressão colorida e impressão em preto e branco, com preço baixo garantido! Valores e condições diferenciadas para empresas e pessoas jurídicas em geral. Compre online e receba até no mesmo dia, ou retire quando quiser. Entregas para todo o Brasil. Imprimir online ficou fácil e barato! Conheça nossos serviços gráficos e se surpreenda: diversos tipos de papel, de acabamentos, encadernações e tamanhos! DeliveryPrint: impressão online com segurança e agilidade!'
            ]);
        },
    ],
    [
        'pattern' => '/impressao-delivery/amp',
        'callback' => function () {
            $layout = new Layout();
            $layout
                ->setCanonical('/impressao-delivery')
                ->setMetaDescription('Serviço de Impressão e Encadernação Delivery com Alta Qualidade é na DeliveryPrint. Orçamento em segundos, pagamento facilitado e frete para todo país. Temos várias opções de Delivery, até para o mesmo dia!')
                ->appendTitle('Impressão Delivery', ' | ')
                ->setLayout('layout-amp')
                ->render('produto/amp-template-produto', [
                    'title'     => 'Impressão Delivery', // = msg_b
                    'subtitle'  => 'Faça Aqui suas Impressões e Receba em Casa', //msg_c
                    'breadc'    => 'Impressão Delivery', //msg_a
                    'img_url'   => '/img/Copiadora.jpg',
                    'li_es'     => [
                        'Impressão em Preto e Branco ou Colorido',
                        'Preço Baixo e Entrega Rápida',
                        'Diversas opções de Encadernações',
                    ],
                    'price'  => '0,11',
                    'unit'   => 'página', //unidade ou página.
                    'a_0'    => '',
                    'a_1'    => '',
                    'a_2'    => '',
                    'a_3'    => '',
                    'a_4'    => true,
                    'a_5'    => '',
                    'text'  => 'Faça seu pedido de impressão online e sem sair de casa! Nós fazemos todo o trabalho para você e para sua empresa! E melhor, receba até no mesmo dia! Com a DeliveryPrint, nunca foi tão fácil realizar o seu pedido de impressão. Imprimir em preto e branco ou colorido com apenas alguns cliques. Diversos tipos de acabamento, de tamanhos e de papéis. Impressão profissional com qualidade garantida e preço baixo para sua comodidade. Peça seu orçamento, é fácil e rápido. Entre em contato por e-mail, whatsapp ou faça seu pedido em nosso site. Não perca tempo! Qualidade de impressão é na DeliveryPrint, a sua Gráfica Digital Online com avaliação máxima no Google!'
            ]);
        },
    ],
    [
        'pattern' => '/grafica/amp',
        'callback' => function () {
            $layout = new Layout();
            $layout
                ->setCanonical('/grafica')
                ->setMetaDescription('Impressão e Encadernação com Alta Qualidade é na DeliveryPrint. Orçamento em segundos, pagamento facilitado e frete para todo país. Somos uma espécie de Gráfica Online, porém somos especializados em serviço de Impressão e Encadernação Online em A4, A3, A5, Papel Carta, Reciclado e Ofício.')
                ->appendTitle('Gráfica para Impressão', ' | ')
                ->setLayout('layout-amp')
                ->render('produto/amp-template-produto', [
                    'title'     => 'Gráfica para Impressão', // = msg_b
                    'subtitle'  => 'Faça suas Impressões e Encadernações gastando Pouco', //msg_c
                    'breadc'    => 'Gráfica para Impressão', //msg_a
                    'img_url'   => '/img/Copiadora.jpg',
                    'li_es'     => [
                        'Impressão em Preto e Branco ou Colorido',
                        'Preço Baixo e Entrega Rápida',
                        'Diversas opções de Encadernações',
                    ],
                    'price'  => '0,11',
                    'unit'   => 'página', //unidade ou página.
                    'a_0'    => '',
                    'a_1'    => '',
                    'a_2'    => '',
                    'a_3'    => '',
                    'a_4'    => true,
                    'a_5'    => '',
                    'text'  => 'Serviços de Gráfica Digital é com a DeliveryPrint, o seu serviço de impressão online com nota máxima no Google Avaliações. Preço baixo e qualidade garantida em todos os serviços gráficos que você ou sua empresa precisam. Condições diferenciadas e especiais para empresas e pessoas jurídicas em geral. Não deixe de fazer seu orçamento! Trabalhamos com os mais variados tipos de papel, acabamentos e produtos gráficos. Produção de cartões de visita, banners, flyers, apostilas de treinamento, estudo e muito mais! Peça seu orçamento: impressão colorida ou impressão em preto e branco com preço baixo e entrega expressa até no mesmo dia! DeliveryPrint: sua gráfica digital online!'
            ]);
        },
    ],
    [
        'pattern' => '/copiadora-online/amp',
        'callback' => function () {
            $layout = new Layout();
            $layout
                ->setCanonical('/copiadora-online')
                ->setMetaDescription('A melhor Copiadora Online , Serviço de Impressão e Encadernação com Alta Qualidade é na DeliveryPrint. Orçamento em segundos, pagamento facilitado e frete para todo país.')
                ->appendTitle('Copiadora Online', ' | ')
                ->setLayout('layout-amp')
                ->render('produto/amp-template-produto', [
                    'title'     => 'Copiadora Online', // = msg_b
                    'subtitle'  => 'Faça suas Impressões e Encadernações em poucos Cliques', //msg_c
                    'breadc'    => 'Copiadora Online', //msg_a
                    'img_url'   => '/img/Copiadora.jpg',
                    'li_es'     => [
                        'Impressão em Preto e Branco ou Colorido',
                        'Preço Baixo e Entrega Rápida',
                        'Diversas opções de Encadernações',
                    ],
                    'price'  => '0,11',
                    'unit'   => 'página', //unidade ou página.
                    'a_0'    => '',
                    'a_1'    => '',
                    'a_2'    => '',
                    'a_3'    => '',
                    'a_4'    => true,
                    'a_5'    => '',
                    'text'  => ''
            ]);
        },
    ],
    [
        'pattern' => '/copiadora-sp/amp',
        'callback' => function () {
            $layout = new Layout();
            $layout
                ->setCanonical('/copiadora-sp')
                ->setMetaDescription('A melhor opção em Impressão! A DeliveryPrint é um serviço online de Impressão e Encadernação com Entregas para todo Brasil. Trabalhamos com Alta Resolução e bons Preços.')
                ->appendTitle('Copiadora SP', ' | ')
                ->setLayout('layout-amp')
                ->render('produto/amp-template-produto', [
                    'title'     => 'Copiadora em SP', // = msg_b
                    'subtitle'  => 'Sua Copiadora Online em São Paulo', //msg_c
                    'breadc'    => 'Copiadora SP', //msg_a
                    'img_url'   => '/img/Copiadora SP.jpg',
                    'li_es'     => [
                        'Impressão em alta Qualidade',
                        'Opções em Encadernações',
                        'Peça Online e Receba em casa',
                    ],
                    'price'  => '0,11',
                    'unit'   => 'página', //unidade ou página.
                    'a_0'    => '',
                    'a_1'    => '',
                    'a_2'    => '',
                    'a_3'    => '',
                    'a_4'    => true,
                    'a_5'    => '',
                    'text'  => 'Já pode parar de procurar! Gráfica Digital e Copiadora em São Paulo é com a DeliveryPrint, o seu serviço de impressão online! Peça de sua casa ou de sua empresa e receba até no mesmo dia! Entregas para todas as regiões de São Paulo, na cidade, em todo o Estado e em todo o Brasil! Preços e condições diferenciadas para empresas e PJ’s. Impressão colorida e impressão em preto e branco é com a DeliveryPrint! Imprimir online nunca foi tão fácil! Diversos tipos de serviços, os mais variados tipos de papéis e de acabamentos. Panfletos, apostilas, materiais de estudo e treinamento, cartões de visitas e plotagem. Faça seu pedido e se surpreenda: DeliveryPrint, a sua Gráfica Digital Online!'
            ]);
        },
    ],
    [
        'pattern' => '/copiadora-vergueiro/amp',
        'callback' => function () {
            $layout = new Layout();
            $layout
                ->setCanonical('/copiadora-vergueiro')
                ->setMetaDescription('Soluções em Impressão e Encadernação na Região da Vergueiro é com a DeliveryPrint. Somos um serviço Online com entrega rápida e ótimos preços.')
                ->appendTitle('Copiadora Vergueiro', ' | ')
                ->setLayout('layout-amp')
                ->render('produto/amp-template-produto', [
                    'title'     => 'Copiadora Vergueiro', // = msg_b
                    'subtitle'  => 'Impressão e Encadernação com Agilidade', //msg_c
                    'breadc'    => 'Copiadora Vergueiro', //msg_a
                    'img_url'   => '/img/Copiadora Vergueiro.jpg',
                    'li_es'     => [
                        'Impressão em Preto e Branco ou Colorido',
                        'Tamanhos A3, A4, A5 e mais',
                        'Pedidos totalmente Online',
                    ],
                    'price'  => '0,11',
                    'unit'   => 'página', //unidade ou página.
                    'a_0'    => '',
                    'a_1'    => '',
                    'a_2'    => '',
                    'a_3'    => '',
                    'a_4'    => true,
                    'a_5'    => '',
                    'text'  => 'Procurando copiadora e gráfica digital no Metrô Vergueiro e região? Peça na DeliveryPrint: o serviço de impressão online com qualidade e preço baixo comprovados! Impressão colorida e impressão em preto e branco com os melhores preços do mercado. Máquinas a laser prontas para te atender, com entregas até no mesmo dia e para todo o Brasil! Peça seu orçamento pelo site, whatsapp ou faça seu pedido em nosso site! Atendimento e condições diferenciadas para empresas e pessoas jurídicas. Imprimir online nunca foi tão fácil! Na DeliveryPrint nós fazemos todo o trabalho para você! Chega de impressões falhadas: gráfica na Vergueiro e região é com a DeliveryPrint!'
            ]);
        },
    ],
    [
        'pattern' => '/copiadora-bela-vista/amp',
        'callback' => function () {
            $layout = new Layout();
            $layout
                ->setCanonical('/copiadora-bela-vista')
                ->setMetaDescription('Sua melhor opção em Copiadora! Somos um serviço de Impressão e Encadernação onde você faz seu Pedido totalmente Online por um Preço Baixo e Entrega Rápida.')
                ->appendTitle('Copiadora Bela Vista', ' | ')
                ->setLayout('layout-amp')
                ->render('produto/amp-template-produto', [
                    'title'     => 'Copiadora Bela Vista', // = msg_b
                    'subtitle'  => 'Sua Copiadora Online', //msg_c
                    'breadc'    => 'Copiadora Bela Vista', //msg_a
                    'img_url'   => '/img/Gráfica Online Impressão.jpg',
                    'li_es'     => [
                        'Diversas Opções em Impressão',
                        'Impressão e Encadernação em poucos cliques',
                        'Peça Online e Receba onde quiser',
                    ],
                    'price'  => '0,11',
                    'unit'   => 'página', //unidade ou página.
                    'a_0'    => '',
                    'a_1'    => '',
                    'a_2'    => '',
                    'a_3'    => '',
                    'a_4'    => true,
                    'a_5'    => '',
                    'text'  => 'Impressão online com preço baixo e qualidade é na DeliveryPrint, sua Gráfica Digital Online! Faça seu pedido em nosso site ou peça seu orçamento pelo nosso e-mail ou Whatsapp e receba até no mesmo dia no bairro da Bela Vista e na região Central de São Paulo. Impressão colorida ou impressão em preto e branco com a melhor definição do mercado. Valores diferenciados para empresas e pessoas jurídicas em geral. Não perca tempo e peça seu orçamento. Imprimir online nunca foi tão fácil! Apenas na DeliveryPrint, o serviço de impressão online com nota máxima no Google Avaliações, você pode confiar que sua demanda será atendida com qualidade e confiança!'
            ]);
        },
    ],
    [
        'pattern' => '/copiadora-republica/amp',
        'callback' => function () {
            $layout = new Layout();
            $layout
                ->setCanonical('/copiadora-republica')
                ->setMetaDescription('Sua Copiadora no coração de São Paulo. Nós da DeliveryPrint trabalhamos com alta qualidade em Impressão e Encadernação. Faça seu Pedido Online e Receba Hoje.')
                ->appendTitle('Copiadora República', ' | ')
                ->setLayout('layout-amp')
                ->render('produto/amp-template-produto', [
                    'title'     => 'Copiadora República', // = msg_b
                    'subtitle'  => 'Imprima e Receba onde estiver!', //msg_c
                    'breadc'    => 'Copiadora República', //msg_a
                    'img_url'   => '/img/Copiadora República.jpg',
                    'li_es'     => [
                        'Impressões',
                        'Encadernações',
                        'Atendimento Personalizado e Rápido',
                    ],
                    'price'  => '0,11',
                    'unit'   => 'página', //unidade ou página.
                    'a_0'    => '',
                    'a_1'    => '',
                    'a_2'    => '',
                    'a_3'    => '',
                    'a_4'    => true,
                    'a_5'    => '',
                    'text'  => 'Seus problemas com gráficas acabaram! Se procura gráfica digital e copiadora na República e no Centro de São Paulo, você já achou! A DeliveryPrint é uma gráfica digital online que oferece serviços de impressão em preto e branco e impressão colorida com máxima qualidade e preço baixo! Faça seu pedido sem sair de casa ou de sua empresa e receba até no mesmo dia na República e Centro de São Paulo. Impressão online, processo simples e eficaz. Diversos tipos de papéis, acabamentos, encadernações e serviços gráficos. Imprimir online ficou fácil! Condições especiais para empresas e PJ’s. Peça com a DeliveryPrint, o serviço de impressão online com cinco estrelas no Google!'
            ]);
        },
    ],
    [
        'pattern' => '/copiadora-itaim-bibi/amp',
        'callback' => function () {
            $layout = new Layout();
            $layout
                ->setCanonical('/copiadora-itaim-bibi')
                ->setMetaDescription('Copiadora Online com Entrega Express e Preço baixo. Somos um serviço de Impressão e Encadernação Rápidos e Digital, a DeliveryPrint está pronta para te atender.')
                ->appendTitle('Copiadora Itaim Bibi', ' | ')
                ->setLayout('layout-amp')
                ->render('produto/amp-template-produto', [
                    'title'     => 'Copiadora  Itaim Bibi', // = msg_b
                    'subtitle'  => 'A comodidade de uma Copiadora Online', //msg_c
                    'breadc'    => 'Copiadora  Itaim Bibi', //msg_a
                    'img_url'   => '/img/Copiadora Itaim Bibi.jpg',
                    'li_es'     => [
                        'Impressão em Pb e Color com Alta Resolução',
                        'Ótimas opções em Acabamentos',
                        'Atendimento Rápido e Entrega',
                    ],
                    'price'  => '0,11',
                    'unit'   => 'página', //unidade ou página.
                    'a_0'    => '',
                    'a_1'    => '',
                    'a_2'    => '',
                    'a_3'    => '',
                    'a_4'    => true,
                    'a_5'    => '',
                    'text'  => 'Qualidade de impressão? Preço baixo? Gráfica Digital e Copiadora no Itaim Bibi e região? A DeliveryPrint, seu serviço de impressão online, tem! Peça direto de sua casa ou de sua empresa e receba até no mesmo dia! Entregas expressas para o Itaim Bibi e entregas rápidas para todo o Brasil! Imprimir online nunca foi tão fácil, faça seu orçamento e se surpreenda. Condições Diferenciadas para empresas e PJs. Impressão colorida e em preto e branco em diversos tipos de papel, os mais variados acabamentos e encadernações, serviços gráficos em geral. Conheça a Gráfica Digital Online com nota máxima no Google Avaliações: na DeliveryPrint, você pode confiar e relaxar! Faça seu pedido!'
            ]);
        },
    ],
    [
        'pattern' => '/copiadora-santa-cruz/amp',
        'callback' => function () {
            $layout = new Layout();
            $layout
                ->setCanonical('/copiadora-santa-cruz')
                ->setMetaDescription('A melhor Resolução em impressão por um preço justo. É isso que a DeliveryPrint trabalha para entregar. Impressão e Encadernação com entrega para todo Brasil.')
                ->appendTitle('Copiadora Santa Cruz', ' | ')
                ->setLayout('layout-amp')
                ->render('produto/amp-template-produto', [
                    'title'     => 'Copiadora Santa Cruz', // = msg_b
                    'subtitle'  => 'Impressão e Encadernação Delivery', //msg_c
                    'breadc'    => 'Copiadora Santa Cruz', //msg_a
                    'img_url'   => '/img/Copiadora Santa Cruz.jpg',
                    'li_es'     => [
                        'Impressão de Alta Qualidade',
                        'Opções em Encadernação',
                        'Entrega Até para o mesmo Dia',
                    ],
                    'price'  => '0,11',
                    'unit'   => 'página', //unidade ou página.
                    'a_0'    => '',
                    'a_1'    => '',
                    'a_2'    => '',
                    'a_3'    => '',
                    'a_4'    => true,
                    'a_5'    => '',
                    'text'  => 'Pensou em copiadora e gráfica digital na região do Santa Cruz, pensou na DeliveryPrint, o seu serviço de impressão online com máxima avaliação no Google! Impressão colorida e impressão em preto e branco com qualidade e preço baixo comprovados! Diversos tipos de papel, serviços e acabamentos. Entregas até no mesmo dia para a região do metrô Santa Cruz. Faça seu pedido diretamente de sua casa ou empresa e nós entregamos para todo o Brasil! Faça seu orçamento em nosso site, e-mail ou whatsapp. Imprimir online nunca foi tão fácil! Valores e condições diferenciados para pessoas jurídicas. Pensou em impressão, pensou na DeliveryPrint, sua Gráfica Digital Online!'
            ]);
        },
    ],
    [
        'pattern' => '/copiadora-liberdade/amp',
        'callback' => function () {
            $layout = new Layout();
            $layout
                ->setCanonical('/copiadora-liberdade')
                ->setMetaDescription('A Copiadora para Impressão e Encadernação que o centro de São Paulo precisa. A DeliveryPrint faz impressão por bons preços em alta qualidade e entrega para todo Brasil.')
                ->appendTitle('Copiadora Liberdade', ' | ')
                ->setLayout('layout-amp')
                ->render('produto/amp-template-produto', [
                    'title'     => 'Copiadora Liberdade', // = msg_b
                    'subtitle'  => 'Serviço de Impressão do Futuro chegou', //msg_c
                    'breadc'    => 'Copiadora Liberdade', //msg_a
                    'img_url'   => '/img/Copiadora Liberdade.jpg',
                    'li_es'     => [
                        'Em Colorido ou em Preto e Branco',
                        'Atendimento Online Personalizado',
                        'Diversas opções de Encadernações',
                    ],
                    'price'  => '0,11',
                    'unit'   => 'página', //unidade ou página.
                    'a_0'    => '',
                    'a_1'    => '',
                    'a_2'    => '',
                    'a_3'    => '',
                    'a_4'    => true,
                    'a_5'    => '',
                    'text'  => 'Liberte-se das gráficas de péssima qualidade. Procurando gráfica digital e copiadora no bairro da Liberdade e no Centro de São Paulo? Conheça a DeliveryPrint, o serviço de impressão online que realiza entregas expressas até no mesmo dia! Faça seu pedido de sua casa ou empresa, online, com apenas alguns cliques! Impressão em preto e branco e impressão colorida com preço baixo e qualidade garantida! Diversos tipos de encadernações, acabamentos e de papéis. Peça seu orçamento e se surpreenda com a facilidade! Imprimir online nunca foi tão fácil! Peça com a DeliveryPrint e se surpreenda, a Gráfica Digital Online de confiança e agilidade!'
            ]);
        },
    ],
    [
        'pattern' => '/copiadora-mooca/amp',
        'callback' => function () {
            $layout = new Layout();
            $layout
                ->setCanonical('/copiadora-mooca')
                ->setMetaDescription('Impressão e Encadernação com Alta Resolução e Acabamento de Qualidade com entrega para toda Zona Leste de São Paulo é só na DeliveryPrint.')
                ->appendTitle('Copiadora Mooca', ' | ')
                ->setLayout('layout-amp')
                ->render('produto/amp-template-produto', [
                    'title'     => 'Copiadora Mooca', // = msg_b
                    'subtitle'  => 'Impressão e Encadernação do seu jeito', //msg_c
                    'breadc'    => 'Copiadora Mooca', //msg_a
                    'img_url'   => '/img/Copiadora Mooca.jpg',
                    'li_es'     => [
                        'Impressão em Preto e Branco ou Colorido',
                        'Melhores opções em preços e Entregas',
                        'Atendimento Online',
                    ],
                    'price'  => '0,11',
                    'unit'   => 'página', //unidade ou página.
                    'a_0'    => '',
                    'a_1'    => '',
                    'a_2'    => '',
                    'a_3'    => '',
                    'a_4'    => true,
                    'a_5'    => '',
                    'text'  => 'Copiadora e Gráfica Digital na Mooca e Zona Leste? Conheça a DeliveryPrint: o serviço de impressão online com entregas e retiradas até no mesmo dia! Imprimir online nunca foi tão fácil! Faça seu pedido em nosso site ou faça seu orçamento por e-mail ou whatsapp. Preços e condições diferenciadas para empresas e pessoas jurídicas. Impressão colorida e impressão em preto e branco com qualidade e os melhores preços do mercado! Entregas para todo o Brasil! Imprimir online do conforto de sua casa e com a segurança de que seu pedido será feito do modo como você precisa! Copiadora na Mooca é com a DeliveryPrint: a Gráfica Digital Online com nota máxima no Google Avaliações!'
            ]);
        },
    ],
    [
        'pattern' => '/copiadora-consolacao/amp',
        'callback' => function () {
            $layout = new Layout();
            $layout
                ->setCanonical('/copiadora-consolacao')
                ->setMetaDescription('Copiadora que entrega na região da consolação com qualidade e agilidade. Impressão, Encadernação, dobras e atendimento personalizado. DeliveryPrint é o Serviço de Impressão On demand que você precisa.')
                ->appendTitle('Copiadora Consolação', ' | ')
                ->setLayout('layout-amp')
                ->render('produto/amp-template-produto', [
                    'title'     => 'Copiadora Consolação', // = msg_b
                    'subtitle'  => 'Impressão barata ao seu alcance', //msg_c
                    'breadc'    => 'Copiadora Consolação', //msg_a
                    'img_url'   => '/img/Copiadora Consolação.jpg',
                    'li_es'     => [
                        'Preto e Branco e Colorido',
                        'Entrega Rápida',
                        'Atendimento Rápido e de Qualidade',
                    ],
                    'price'  => '0,11',
                    'unit'   => 'página', //unidade ou página.
                    'a_0'    => '',
                    'a_1'    => '',
                    'a_2'    => '',
                    'a_3'    => '',
                    'a_4'    => true,
                    'a_5'    => '',
                    'text'  => 'Gráfica Digital e Copiadora na Consolação é com a DeliveryPrint, seu serviço de impressão online! Preços baixos e qualidade impecável. Impressão em preto e branco e impressão colorida, diversos tipos de acabamento, de papéis e de serviços gráficos. Peça direto de sua casa ou de sua empresa e receba até no mesmo dia na Consolação e Centro de São Paulo. Preços e condições diferenciadas para empresas e PJ’s. Imprimir online ficou fácil. Chega de dor de cabeça com péssima qualidade de impressão e prazos gigantescos. Serviços de impressão e encadernação rápidas. Na DeliveryPrint, a gráfica digital online com nota máxima no Google, você pode confiar que sua impressão sairá do modo como deve ser!'
            ]);
        },
    ],
    [
        'pattern' => '/copiadora-barra-funda/amp',
        'callback' => function () {
            $layout = new Layout();
            $layout
                ->setCanonical('/copiadora-barra-funda')
                ->setMetaDescription('Impressão, Encadernação e Entrega para Barra Funda e todo Brasil é na DeliveryPrint. Fazemos Impressão de Alta Qualidade com bons preços e Entrega Diferenciada.')
                ->appendTitle('Copiadora Barra Funda', ' | ')
                ->setLayout('layout-amp')
                ->render('produto/amp-template-produto', [
                    'title'     => 'Copiadora Barra Funda', // = msg_b
                    'subtitle'  => 'Impressão Barata com Entrega Garantida', //msg_c
                    'breadc'    => 'Copiadora Barra Funda', //msg_a
                    'img_url'   => '/img/Copiadora Barra Funda.jpg',
                    'li_es'     => [
                        'Impressão e Encadernação',
                        'Delivery para todo lugar',
                        'Desconto no 1º Pedido',
                    ],
                    'price'  => '0,11',
                    'unit'   => 'página', //unidade ou página.
                    'a_0'    => '',
                    'a_1'    => '',
                    'a_2'    => '',
                    'a_3'    => '',
                    'a_4'    => true,
                    'a_5'    => '',
                    'text'  => 'Precisando de copiadora e gráfica digital na Barra Funda e Zona Oeste de São Paulo? Conheça a DeliveryPrint: a gráfica digital online com nota máxima no google avaliações! Impressão colorida e impressão em preto e branco de alta qualidade, com preço baixo comprovado! Faça seu pedido direto de casa ou de sua empresa e receba até no mesmo dia! Entregas para todo o Brasil, condições e valores diferenciados para pessoas jurídicas. Imprimir online nunca foi tão fácil! Oferecemos os mais variados serviços gráficos, em diferentes tipos de papel e de acabamento. Impressão online é com a DeliveryPrint! Imprimir na Barra Funda ficou fácil! Faça seu pedido e se surpreenda!'
            ]);
        },
    ],
    [
        'pattern' => '/impressao-nf/amp',
        'callback' => function () {
            $layout = new Layout();
            $layout
                ->setCanonical('/impressao-nf')
                ->setMetaDescription('Serviço de Impressão de Notas Fiscais para Empresas e Empresários. Imprimimos e entregamos dentro de sua necessidade pelo melhor preço. A DeliveryPrint é um serviço de Impressão e Encadernação Online.')
                ->appendTitle('Impressão de Nota Fiscal', ' | ')
                ->setLayout('layout-amp')
                ->render('produto/amp-template-produto', [
                    'title'     => 'Impressão de Nota Fiscal', // = msg_b
                    'subtitle'  => 'A melhor opção para Terceirizar Impressão', //msg_c
                    'breadc'    => 'Impressão de Nota Fiscal', //msg_a
                    'img_url'   => '/img/Nota Fiscal Impressa.jpg',
                    'li_es'     => [
                        'Impressão com baixo Custo',
                        'Entrega Rápida',
                        'Opções de Faturamento para Empresas',
                    ],
                    'price'  => '0,11',
                    'unit'   => 'página', //unidade ou página.
                    'a_0'    => '',
                    'a_1'    => '',
                    'a_2'    => '',
                    'a_3'    => '',
                    'a_4'    => true,
                    'a_5'    => '',
                    'text'  => 'Imprimir online nunca foi tão fácil! Com a DeliveryPrint, a sua Gráfica Digital Online, com apenas alguns cliques você realiza todo o seu pedido, sem sair de sua casa ou de sua empresa. Impressão de Notas Fiscais, dados variáveis e malas diretas para todo o Brasil! Preço baixo e qualidade garantida. Respeito ao prazo de entrega e condições diferenciadas para empresas e pessoas físicas em geral. Seja impressão em preto e branco ou impressão colorida, experimente nossos serviços: faça seu pedido pelo nosso site ou solicite um orçamento pelo e-mail ou Whatsapp. Conheça nossos serviços! DeliveryPrint: o serviço de impressão online com nota máxima no Google Avaliações!'
            ]);
        },
    ],
    [
        'pattern' => '/impressao-boletos/amp',
        'callback' => function () {
            $layout = new Layout();
            $layout
                ->setCanonical('/impressao-boletos')
                ->setMetaDescription('Serviço de Impressão de Boletos com Entregas Personalizadas é na DeliveryPrint. Somos um serviço de Impressão e Encadernação Profissional de alta qualidade, entregamos para todo Brasil.')
                ->appendTitle('Impressão de Boletos', ' | ')
                ->setLayout('layout-amp')
                ->render('produto/amp-template-produto', [
                    'title'     => 'Impressão de Boletos', // = msg_b
                    'subtitle'  => 'Serviço de Impressão e Entrega para seu Negócio', //msg_c
                    'breadc'    => 'Impressão de Boletos', //msg_a
                    'img_url'   => '/img/Impressão de Boletos.jpg',
                    'li_es'     => [
                        'Impressão em Preto e Branco ou Colorido',
                        'Impressão Profissional',
                        'Entregas sob demanda',
                    ],
                    'price'  => '0,11',
                    'unit'   => 'página', //unidade ou página.
                    'a_0'    => '',
                    'a_1'    => '',
                    'a_2'    => '',
                    'a_3'    => '',
                    'a_4'    => true,
                    'a_5'    => '',
                    'text'   => 'Precisa imprimir boletos para o seu negócio? Enviar para clientes? Conheça a DeliveryPrint, a Gráfica Digital Online de alta qualidade e preço baixo garantido por nossos usuários! Faça seu pedido em nosso site ou peça seu orçamento por e-mail ou whatsapp e se surpreenda com a facilidade! Valores e condições diferenciadas para empresas e pessoas jurídicas: faturamento, atendimento e entregas personalizadas! Entregamos até no mesmo dia e para todo o Brasil! Dados variáveis e malas diretas! Impressão colorida e impressão em preto e branco, diversos tipos de acabamento e de papéis. Imprimir online ficou fácil: utilize nossos serviços de impressão e não se arrependa: preço baixo e qualidade, respeito aos prazos!'
            ]);
        },
    ],
    [
        'pattern' => '/impressao-processos/amp',
        'callback' => function () {
            $layout = new Layout();
            $layout
                ->setCanonical('/impressao-processos')
                ->setMetaDescription('Solução de Impressão para escritórios e Advogados. Fazemos Impressão e Encadernação, temos entregas até para o mesmo dia. DeliveryPrint é sinônimo de Qualidade e Preços justo.')
                ->appendTitle('Impressão de Processos', ' | ')
                ->setLayout('layout-amp')
                ->render('produto/amp-template-produto', [
                    'title'     => 'Impressão de Processos', // = msg_b
                    'subtitle'  => 'Impressão para Advogados e Escritórios', //msg_c
                    'breadc'    => 'Impressão de Processos', //msg_a
                    'img_url'   => '/img/Processos.jpg',
                    'li_es'     => [
                        'Impressão em Preto e Branco ou Colorido',
                        'Entrega sob demanda',
                        'Preços Especiais para Empresas',
                    ],
                    'price'  => '0,11',
                    'unit'   => 'página', //unidade ou página.
                    'a_0'    => '',
                    'a_1'    => '',
                    'a_2'    => '',
                    'a_3'    => '',
                    'a_4'    => true,
                    'a_5'    => '',
                    'text'  => 'Chega de impressoras emperrando, toners explodindo e bagunça no escritório com papéis aos montes. Na DeliveryPrint, sua Gráfica Digital Online, seu escritório ou sua empresa não precisam mais se preocupar com nada disso. Fazemos todo o trabalho e organização de documentos para sua empresa, com os menores preços do mercado em impressão em preto e branco e impressão colorida, com condições diferenciadas para empresas. Pedidos e orçamentos 100% online, com entregas expressas até no mesmo dia! Entregamos para todo o Brasil, na porta de sua empresa ou de seus parceiros comerciais. Peça seu orçamento na DeliveryPrint, o serviço de impressão online com nota máxima no Google!'
            ]);
        },
    ],
    [
        'pattern' => '/impressao-treinamentos/amp',
        'callback' => function () {
            $layout = new Layout();
            $layout
                ->setCanonical('/impressao-treinamentos')
                ->setMetaDescription('A melhor solução para Impressão de Apostilas de Treinamento. Impressão e Encadernação com entrega para você ou sua Empresa. A DeliveryPrint é um serviço de Impressão Profissional com ótimos preços.')
                ->appendTitle('Impressão de Treinamentos', ' | ')
                ->setLayout('layout-amp')
                ->render('produto/amp-template-produto', [
                    'title'     => 'Impressão para Treinamentos', // = msg_b
                    'subtitle'  => 'A Solução de Impressão que sua Empresa Precisava', //msg_c
                    'breadc'    => 'Impressão para Treinamentos', //msg_a
                    'img_url'   => '/img/Impressão de Treinamentos.jpg',
                    'li_es'     => [
                        'Impressão em Preto e Branco ou Colorido',
                        'Pedidos Faturados para Empresas',
                        'Opções de Entrega para sua Necessidade',
                    ],
                    'price'  => '0,11',
                    'unit'   => 'página', //unidade ou página.
                    'a_0'    => '',
                    'a_1'    => '',
                    'a_2'    => '',
                    'a_3'    => '',
                    'a_4'    => true,
                    'a_5'    => '',
                    'text'  => 'Apostilas e materiais de treinamento em geral com preço baixo e qualidade comprovada. Na DeliveryPrint, a sua gráfica digital online, sua empresa não precisará se preocupar com impressões falhadas e atrasos no prazo de entrega. Impressão em preto e branco e colorida, com diferentes tipos de acabamento, ideais para apostilas de treinamento, com entrega expressa e opções diferenciadas para empresas. Encadernações em espiral e wire-o para que seu material fique com qualidade profissional! Não perca tempo, peça seu orçamento de impressão online na DeliveryPrint, o serviço de impressão online com nota máxima no Google Avaliações.'
            ]);
        },
    ],
    [
        'pattern' => '/copiadora-shopping/amp',
        'callback' => function () {
            $layout = new Layout();
            $layout
                ->setCanonical('/copiadora-shopping')
                ->setMetaDescription('Serviço de impressão com Entrega Rápida, fidelidade de cores, preço baixo e atendimento exclusivo é na DeliveryPrint. Entregamos Impressão em Shoppings e em todo Brasil.')
                ->appendTitle('Copiadora Shopping', ' | ')
                ->setLayout('layout-amp')
                ->render('produto/amp-template-produto', [
                    'title'     => 'Copiadora Shopping', // = msg_b
                    'subtitle'  => 'Você não precisa mais ir ao Shopping para Imprimir', //msg_c
                    'breadc'    => 'Copiadora Shopping', //msg_a
                    'img_url'   => '/img/Copiadora Shopping.jpg',
                    'li_es'     => [
                        'Impressão em Vários Tamanhos de Papel',
                        'Mais barato do que no Shopping',
                        'Entregamos para você!',
                    ],
                    'price'  => '0,11',
                    'unit'   => 'página', //unidade ou página.
                    'a_0'    => '',
                    'a_1'    => '',
                    'a_2'    => '',
                    'a_3'    => '',
                    'a_4'    => true,
                    'a_5'    => '',
                    'text'  => 'Impressão online com qualidade e preço baixo. Mais barato do que no Shopping! Você não precisa mais ir ao shopping para procurar gráfica digital e copiadora! Com a DeliveryPrint, faça seu pedido online direto de sua casa e receba até no mesmo dia! Entregamos para todo o Brasil! Impressão colorida e impressão em preto e branco, com diversos tipos de acabamento, de tamanhos e de papéis. Serviço gráfico do modo como você precisa! Faça seu pedido de impressão online em nosso site, whatsapp ou e-mail com extrema facilidade. Imprimir colorido e imprimir em preto e branco ficou fácil: peça com a DeliveryPrint, sua Gráfica Digital Online e não se arrependa!'
            ]);
        },
    ],
    [
        'pattern' => '/copiadora-paulista/amp',
        'callback' => function () {
            $layout = new Layout();
            $layout
                ->setCanonical('/copiadora-paulista')
                ->setMetaDescription('Somos um Serviço de Impressão e Encadernação em São Paulo que entrega para todo Brasil. Precisa de Impressão de Qualidade e Preço Justo? Contem com a DeliveryPrint.')
                ->appendTitle('Copiadora Paulista', ' | ')
                ->setLayout('layout-amp')
                ->render('produto/amp-template-produto', [
                    'title'     => 'Copiadora Paulista', // = msg_b
                    'subtitle'  => 'A Copiadora do Coração de São Paulo', //msg_c
                    'breadc'    => 'Copiadora Paulista', //msg_a
                    'img_url'   => '/img/Copiadora Paulista.jpg',
                    'li_es'     => [
                        'Impressão em Cores e Preto e Branco',
                        'Tamanhos A3, A4, A5 e mais',
                        'Entregamos onde você está',
                    ],
                    'price'  => '0,11',
                    'unit'   => 'página', //unidade ou página.
                    'a_0'    => '',
                    'a_1'    => '',
                    'a_2'    => '',
                    'a_3'    => '',
                    'a_4'    => true,
                    'a_5'    => '',
                    'text'  => 'Está a procura de Gráfica Digital e Copiadora na Paulista e Centro de São Paulo? Com a DeliveryPrint, seu serviço de impressão online, sua procura acabou! Gráfica Digital Online com preço baixo e qualidade comprovada! Faça seu pedido em nosso site ou peça seu orçamento via e-mail ou whatsapp e se surpreenda com a facilidade! Peça em casa ou em sua empresa e receba até no mesmo dia! Entregas para todo o Brasil! Impressão colorida ou impressão em preto e branco, funcionamento online e atendimento qualificado. Diversos tipos de papéis, de acabamentos e de serviços gráficos: apostilas, flyers, cartões de visita, materiais de estudo e treinamento. Peça com a DeliveryPrint! Imprimir online ficou fácil!'
            ]);
        },
    ],
    [
        'pattern' => '/copiadora-itaquera/amp',
        'callback' => function () {
            $layout = new Layout();
            $layout
                ->setCanonical('/copiadora-itaquera')
                ->setMetaDescription('Copiadora em São Paulo com entrega em Itaquera de forma Rápida sem perder a qualidade. Impressão em diversos tamanhos com bons preços é na DeliveryPrint.')
                ->appendTitle('Copiadora Itaquera', ' | ')
                ->setLayout('layout-amp')
                ->render('produto/amp-template-produto', [
                    'title'     => 'Copiadora Itaquera', // = msg_b
                    'subtitle'  => 'Impressão barata na Zona Leste', //msg_c
                    'breadc'    => 'Copiadora Itaquera', //msg_a
                    'img_url'   => '/img/Copiadora Itaquera.jpg',
                    'li_es'     => [
                        'Impressão em Preto e Branco ou Colorido',
                        'Peça de onde estiver, receba Rápido',
                        'Diversas opções de papéis',
                    ],
                    'price'  => '0,11',
                    'unit'   => 'página', //unidade ou página.
                    'a_0'    => '',
                    'a_1'    => '',
                    'a_2'    => '',
                    'a_3'    => '',
                    'a_4'    => true,
                    'a_5'    => '',
                    'text'  => 'Procurando copiadora e gráfica digital em Itaquera e Zona Leste de São Paulo? Peça na DeliveryPrint, o seu serviço de impressão online, e receba rapidamente direto em sua casa ou em sua empresa! Entregamos para todo o Brasil! Temos os mais variados serviços gráficos, impressão colorida e impressão em preto e branco de alta qualidade, com preço baixo garantido. Imprimir online ficou fácil! Faça seu pedido por e-mail, whatsapp ou pelo nosso site e se surpreenda! Pagamento online e entrega expressa! Diversos tipos de papel e acabamento! Impressão em Itaquera é impressão na DeliveryPrint, a Gráfica Digital Online com nota máxima no Google Avaliações!'
            ]);
        },
    ],
    [
        'pattern' => '/copiadora-tatuape/amp',
        'callback' => function () {
            $layout = new Layout();
            $layout
                ->setCanonical('/copiadora-tatuape')
                ->setMetaDescription('A melhor Copiadora para o Tatuapé, Impressão e Encadernação com Qualidade é na DeliveryPrint. Faça seu pedido de forma rápida em nosso site e receba ainda hoje.')
                ->appendTitle('Copiadora Tatuapé', ' | ')
                ->setLayout('layout-amp')
                ->render('produto/amp-template-produto', [
                    'title'     => 'Copiadora Tatuapé', // = msg_b
                    'subtitle'  => 'Serviços de Impressão e Encadernação Delivery', //msg_c
                    'breadc'    => 'Copiadora Tatuapé', //msg_a
                    'img_url'   => '/img/Copiadora Tatuapé.jpg',
                    'li_es'     => [
                        'Impressões em PB ou Color',
                        'Desconto no 1º pedido',
                        'Encadernações e Atendimento Personalizado',
                    ],
                    'price'  => '0,11',
                    'unit'   => 'página', //unidade ou página.
                    'a_0'    => '',
                    'a_1'    => '',
                    'a_2'    => '',
                    'a_3'    => '',
                    'a_4'    => true,
                    'a_5'    => '',
                    'text'  => 'Copiadora e Gráfica Digital no Tatuapé e Zona Leste de São Paulo é com a DeliveryPrint! Faça seu pedido online e receba até no mesmo dia, do modo como você precisa! Impressão colorida e impressão em preto e branco com qualidade e preço justo garantidos. Peça pelo whatsapp, e-mail ou site e se surpreenda com a facilidade. Entregas para todo o Brasil, valores e condições diferenciadas para empresas e pessoas jurídicas. Diversos tipos de papel e de acabamento. Imprimir online com a DeliveryPrint é o modo mais fácil e barato de garantir seu serviço gráfico com qualidade no Tatuapé. Impressão de apostilas, relatórios, materiais de estudo e treinamento, cartões de visita, produtos gráficos em geral! Conheça!'
            ]);
        },
    ],
    [
        'pattern' => '/grafica-digital-online/amp',
        'callback' => function () {
            $layout = new Layout();
            $layout
                ->setCanonical('/grafica-digital-online')
                ->setMetaDescription('A DeliveryPrint faz o serviço de uma gráfica digital Online com baixo custo e Entrega Rápida. Impressão em maquinário Profissional, Entrega ágil, pedido feito totalmente online de forma rápida.')
                ->appendTitle('Gráfica Digital Online', ' | ')
                ->setLayout('layout-amp')
                ->render('produto/amp-template-produto', [
                    'title'     => 'Gráfica Digital Online', // = msg_b
                    'subtitle'  => 'O Serviço de Impressão do futuro chegou', //msg_c
                    'breadc'    => 'Gráfica Digital Online', //msg_a
                    'img_url'   => '/img/Gráfica Digital Online.jpg',
                    'li_es'     => [
                        'Impressão Profissional',
                        'Encadernação de Qualidade',
                        'Bons Preços e Entrega Ágil',
                    ],
                    'price'  => '0,11',
                    'unit'   => 'página', //unidade ou página.
                    'a_0'    => '',
                    'a_1'    => '',
                    'a_2'    => '',
                    'a_3'    => '',
                    'a_4'    => true,
                    'a_5'    => '',
                    'text'   => 'Gráfica Digital Online é com a DeliveryPrint: o serviço de impressão online com nota máxima no Google Avaliações. Oferecemos os mais variados serviços gráficos, com qualidade e preço baixo comprovado por nossos clientes! Faça seu pedido online e receba até no mesmo dia em sua casa ou empresa! Entregas para todo o Brasil! Valores e condições diferenciadas para empresas e pessoas jurídicas em geral. Impressão em preto e branco e impressão colorida com os melhores preços do mercado. Diversos tipos de papel, de tamanhos, de acabamentos e de encadernações. Peça seu orçamento ou faça seu pedido no site e se surpreenda com a facilidade. DeliveryPrint: imprimir online ficou fácil!'
            ]);
        },
    ],
    [
        'pattern' => '/copiadora-jk/amp',
        'callback' => function () {
            $layout = new Layout();
            $layout
                ->setCanonical('/copiadora-jk')
                ->setMetaDescription('Copiadora Online que entrega rápido no Itaim, JK, Vila olimpia e região. A DeliveryPrint é um serviço de impressão e Encadernação Online com preços baixos e entrega para todo Brasil.')
                ->appendTitle('Copiadora JK', ' | ')
                ->setLayout('layout-amp')
                ->render('produto/amp-template-produto', [
                    'title'     => 'Copiadora na JK', // = msg_b
                    'subtitle'  => 'O Serviço de Impressão na palma de sua mão', //msg_c
                    'breadc'    => 'Copiadora na JK', //msg_a
                    'img_url'   => '/img/Copiadora JK.jpg',
                    'li_es'     => [
                        'Impressão',
                        'Encadernação',
                        'Desconto no 1º pedido',
                    ],
                    'price'  => '0,11',
                    'unit'   => 'página', //unidade ou página.
                    'a_0'    => '',
                    'a_1'    => '',
                    'a_2'    => '',
                    'a_3'    => '',
                    'a_4'    => true,
                    'a_5'    => '',
                    'text'  => 'Copiadora e gráfica digital na JK e região do Itaim com qualidade e preço baixo comprovados é com a DeliveryPrint, a sua Gráfica Digital Online! Faça seu pedido sem sair de sua empresa ou de sua casa. Trabalhamos com valores e condições diferenciadas para pessoas jurídicas e empresas. Amplo portfólio de serviços gráficos, cartões de visita, apostilas, materiais de estudo e treinamento, impressão colorida e impressão em preto e branco, em diversos tipos de papel e de acabamentos. Entregas até no mesmo dia para a JK e região do Itaim. Preço baixo, qualidade e agilidade. Imprimir online nunca foi tão fácil. Na DeliveryPrint, nós fazemos todo o trabalho para você, sem dor de cabeça.'
            ]);
        },
    ],
    [
        'pattern' => '/grafica-express/amp',
        'callback' => function () {
            $layout = new Layout();
            $layout
                ->setCanonical('/grafica-express')
                ->setMetaDescription('O Melhor serviço de Impressão Digital de toda internet. A DeliveryPrint imprime, encaderna e entrega o que você precisa por o valor que você pode pagar.')
                ->appendTitle('Gráfica Express', ' | ')
                ->setLayout('layout-amp')
                ->render('produto/amp-template-produto', [
                    'title'     => 'Gráfica Express', // = msg_b
                    'subtitle'  => 'Impressão Profissional ao seu Alcance', //msg_c
                    'breadc'    => 'Gráfica Express', //msg_a
                    'img_url'   => '/img/Gráfica Express.jpg',
                    'li_es'     => [
                        'Impressão Digital Profissional',
                        'Serviço de Plotagem',
                        'Diversas opções de Encadernações',
                    ],
                    'price'  => '0,11',
                    'unit'   => 'página', //unidade ou página.
                    'a_0'    => '',
                    'a_1'    => '',
                    'a_2'    => '',
                    'a_3'    => '',
                    'a_4'    => true,
                    'a_5'    => '',
                    'text'  => 'Serviço de impressão online com qualidade e preço baixo é na DeliveryPrint. Realizamos entregas até no mesmo dia para a cidade de São Paulo e entregas rápidas para todo o Brasil. Está precisando de impressão urgente? Imprimir em preto e branco ou colorida? Peça seu orçamento ou faça o seu pedido em nosso site. Apenas na DeliveryPrint, você pode confiar que sua impressão sairá do jeito que precisa. Trabalhamos com diversos tipos e tamanhos de papel, como o A5, A4 e A3, papel sulfite e couchê, e também de acabamentos, como encadernação em espiral e wire-o. Não perça tempo, conheça a DeliveryPrint, a Gráfica Digital Online com nota máxima no Google!'
            ]);
        },
    ],
    [
        'pattern' => '/copiadora-lapa/amp',
        'callback' => function () {
            $layout = new Layout();
            $layout
                ->setCanonical('/copiadora-lapa')
                ->setMetaDescription('Serviço de Impressão, Encadernação, Plotagem e muito mais. A DeliveryPrint é um serviço de Impressão que entrega na região da Lapa e de toda São Paulo. Faça o pedido de maneira fácil e barata em nosso site.')
                ->appendTitle('Copiadora Lapa', ' | ')
                ->setLayout('layout-amp')
                ->render('produto/amp-template-produto', [
                    'title'     => 'Copiadora na Lapa', // = msg_b
                    'subtitle'  => 'Impressão e Encadernação na Região mais Tradicional de SP', //msg_c
                    'breadc'    => 'Copiadora na Lapa', //msg_a
                    'img_url'   => '/img/Copiadora Lapa.jpg',
                    'li_es'     => [
                        'Vários tipos de Impressão',
                        'Encadernação em Espiral, Wire-o, Capa Dura e Grampeado',
                        'Entregamos rápido',
                    ],
                    'price'  => '0,11',
                    'unit'   => 'página', //unidade ou página.
                    'a_0'    => '',
                    'a_1'    => '',
                    'a_2'    => '',
                    'a_3'    => '',
                    'a_4'    => true,
                    'a_5'    => '',
                    'text'  => 'Gráfica Digital e Copiadora na Lapa e Zona Oeste de São Paulo é com a DeliveryPrint: seu serviço de impressão online! Entregas expressas até no mesmo dia para a Lapa! Impressão em preto e branco e impressão colorida de alta qualidade e preço baixo! Peça online, direto de sua casa ou de sua empresa, sem precisar se preocupar. Condições diferenciadas para empresas e PJs. Faça seu pedido em nosso site, e-mail ou Whatsapp. Imprimir colorido e imprimir em preto e branco em diversos tipos de papel, com acabamento e encadernação de qualidade, apenas na DeliveryPrint! Serviços gráficos variados: cartões de visita, panfletos, plotagens e muito mais! Gráfica Digital Online é com a DeliveryPrint!'
            ]);
        },
    ],
    [
        'pattern' => '/grafica-profissional/amp',
        'callback' => function () {
            $layout = new Layout();
            $layout
                ->setCanonical('/grafica-profissional')
                ->setMetaDescription('A DeliveryPrint é um serviço Rápido e Online de Impressão e Encadernação. Faça o pedido por nosso site rapidamente, escolha uma opção de entrega e é só aguardar. Preços baixos, atendimento personalizado é aqui.')
                ->appendTitle('Gráfica Profissional', ' | ')
                ->setLayout('layout-amp')
                ->render('produto/amp-template-produto', [
                    'title'     => 'Gráfica Profissional', // = msg_b
                    'subtitle'  => 'Atendimento e Impressão Profissionais para você', //msg_c
                    'breadc'    => 'Gráfica Profissional', //msg_a
                    'img_url'   => '/img/Gráfica Profissional.jpg',
                    'li_es'     => [
                        'Impressão em Preto e Branco ou Colorido',
                        'Atendimento Personalizado',
                        'Entregamos para você',
                    ],
                    'price'  => '0,11',
                    'unit'   => 'página', //unidade ou página.
                    'a_0'    => '',
                    'a_1'    => '',
                    'a_2'    => '',
                    'a_3'    => '',
                    'a_4'    => true,
                    'a_5'    => '',
                    'text'  => 'Serviços de Gráfica Digital sem dor de cabeça e com profissionalismo é com a DeliveryPrint, o seu serviço de impressão online com nota máxima no Google Avaliações. Preço baixo e qualidade garantida em todos os serviços gráficos que você ou sua empresa precisam. Condições diferenciadas e especiais para empresas e pessoas jurídicas em geral. Não deixe de fazer seu orçamento! Trabalhamos com os mais variados tipos de papel, acabamentos e produtos gráficos. Produção de cartões de visita, banners, flyers, apostilas de treinamento, estudo e muito mais! Peça seu orçamento: impressão colorida ou impressão em preto e branco com preço baixo e entrega expressa até no mesmo dia!'
            ]);
        },
    ],
    [
        'pattern' => '/grafica-24h/amp',
        'callback' => function () {
            $layout = new Layout();
            $layout
                ->setCanonical('/grafica-24h')
                ->setMetaDescription('Serviço de Impressão e Encadernação que recebe pedidos 24h, 7 dias por semana. Faça seu pedido de Impressão pelo site da DeliveryPrint e receba rápido.')
                ->appendTitle('Gráfica 24h', ' | ')
                ->setLayout('layout-amp')
                ->render('produto/amp-template-produto', [
                    'title'     => 'Gráfica 24h', // = msg_b
                    'subtitle'  => 'Serviço de Impressão para toda hora', //msg_c
                    'breadc'    => 'Gráfica 24h', //msg_a
                    'img_url'   => '/img/Gráfica 24h.jpg',
                    'li_es'     => [
                        'Faça seu pedido de Impressão 24h',
                        'Facilidade em Orçar e Comprar',
                        'Entrega Ágil em dias úteis',
                    ],
                    'price'  => '0,11',
                    'unit'   => 'página', //unidade ou página.
                    'a_0'    => '',
                    'a_1'    => '',
                    'a_2'    => '',
                    'a_3'    => '',
                    'a_4'    => true,
                    'a_5'    => '',
                    'text'  => 'Surgiu demanda de serviço gráfico na calada da noite? Não se preocupe! O sistema da DeliveryPrint, a sua gráfica digital online, funciona 24h por dia! Imprimir online ficou fácil e rápido! Entre em nosso site, realize o seu pedido e receba até no mesmo dia! Impressão em preto e branco e impressão colorida com qualidade profissional e preço que cabe no seu bolso! Trabalhamos com diversos tipos de papel de acabamento! Imprimir em preto e branco e imprimir colorido a qualquer hora do dia só com a DeliveryPrint: o serviço de impressão online com as melhores avaliações no Google! Faça seu pedido e se surpreenda com a facilidade! Valores e condições diferenciadas para empresas! Nos conheça!'
            ]);
        },
    ],
    [
        'pattern' => '/grafica-rapida/amp',
        'callback' => function () {
            $layout = new Layout();
            $layout
                ->setCanonical('/grafica-rapida')
                ->setMetaDescription('A DeliveryPrint tem soluções em impressão e Encadernação, somos uma espécie de Gráfica Rápida, porém totalmente online para facilitar ainda mais sua vida. Peça e receba ainda hoje sua impressão.')
                ->appendTitle('Gráfica Rápida', ' | ')
                ->setLayout('layout-amp')
                ->render('produto/amp-template-produto', [
                    'title'     => 'Gráfica Rápida', // = msg_b
                    'subtitle'  => 'A Gráfica Rápida que é Online', //msg_c
                    'breadc'    => 'Gráfica Rápida', //msg_a
                    'img_url'   => '/img/Gráfica Rápida.jpg',
                    'li_es'     => [
                        'Impressão a Laser de Alta Qualidade',
                        'Acabamento Profissional',
                        'Entregas Rápidas e para Todo Brasil',
                    ],
                    'price'  => '0,11',
                    'unit'   => 'página', //unidade ou página.
                    'a_0'    => '',
                    'a_1'    => '',
                    'a_2'    => '',
                    'a_3'    => '',
                    'a_4'    => true,
                    'a_5'    => '',
                    'text'  => 'Achar gráficas de qualidade está ficando cada vez mais difícil. Preços abusivos, prazos gigantescos e péssima qualidade de impressão são as marcas da maioria das gráficas. Pois conheça a DeliveryPrint, o serviço de impressão online que deixa essas gráficas comendo toner - e poeira. Peça direto do conforto de sua casa ou sem sair do escritório (valores especiais para pessoas jurídicas) e receba suas impressões até no mesmo dia! Entregas expressas para todo o país: produz e despacha no mesmo dia! Imprimir em preto e branco e imprimir colorido ficou fácil - e barato! Diversos tipos de papéis, de encadernações, acabamentos e serviços gráficos em geral. Impressão em preto e branco e impressão colorida a laser de alta qualidade é na DeliveryPrint!'
            ]);
        },
    ],
    [
        'pattern' => '/grafica-sp/amp',
        'callback' => function () {
            $layout = new Layout();
            $layout
                ->setCanonical('/grafica-sp')
                ->setMetaDescription('A DeliveryPrint funciona como uma Gráfica para serviços de Impressão e Encadernação, porém somos totalmente Online. Faça o pedido por nosso site rapidamente, receba os impressos em SP ou em qualquer lugar do Brasil.')
                ->appendTitle('Gráfica SP', ' | ')
                ->setLayout('layout-amp')
                ->render('produto/amp-template-produto', [
                    'title'     => 'Gráfica SP', // = msg_b
                    'subtitle'  => 'Gráfica para impressão e Encadernação em SP', //msg_c
                    'breadc'    => 'Gráfica SP', //msg_a
                    'img_url'   => '/img/Gráfica SP.jpg',
                    'li_es'     => [
                        'Impressões em Colorido e Preto e Branco',
                        'Desconto no 1º Pedido',
                        'Faça seu pedido Online Rapidamente',
                    ],
                    'price'  => '0,11',
                    'unit'   => 'página', //unidade ou página.
                    'a_0'    => '',
                    'a_1'    => '',
                    'a_2'    => '',
                    'a_3'    => '',
                    'a_4'    => true,
                    'a_5'    => '',
                    'text'  => 'Pensou em impressão barata, pensou na DeliveryPrint! O serviço de impressão online que entrega para todas as cidades de SP e para todo o Brasil! Impressão colorida e impressão em preto e branco de alta qualidade, com preço baixo garantido. Chega de correr atrás de gráficas e esperar dias e dias pelo seu serviço. Na DeliveryPrint você faz seu pedido do conforto de sua casa e recebe em pouco tempo. Nós fazemos todo o trabalho para você! Imprimir em preto e branco e imprimir colorido, em diversos tipos de papel e de acabamentos, com preço justo que cabe no seu bolso! Gráfica Digital Online é com a DeliveryPrint!'
            ]);
        },
    ],
    [
        'pattern' => '/grafica-rj/amp',
        'callback' => function () {
            $layout = new Layout();
            $layout
                ->setCanonical('/grafica-rj')
                ->setMetaDescription('Impressão e Encadernação com entrega no Rio de Janeiro e no Brasil todo é só na DeliveryPrint! Serviço de Impressão de alta qualidade por um preço justo e entrega inclusa.')
                ->appendTitle('Gráfica RJ', ' | ')
                ->setLayout('layout-amp')
                ->render('produto/amp-template-produto', [
                    'title'     => 'Gráfica RJ', // = msg_b
                    'subtitle'  => 'Gráfica digital da cidade Maravilhosa', //msg_c
                    'breadc'    => 'Gráfica RJ', //msg_a
                    'img_url'   => '/img/Gráfica RJ.jpg',
                    'li_es'     => [
                        'Impressão e Encadernação',
                        'Atendimento Personalizado',
                        'Entrega Ágil',
                    ],
                    'price'  => '0,11',
                    'unit'   => 'página', //unidade ou página.
                    'a_0'    => '',
                    'a_1'    => '',
                    'a_2'    => '',
                    'a_3'    => '',
                    'a_4'    => true,
                    'a_5'    => '',
                    'text'  => 'Serviços de Gráfica Digital e Copiadora no Rio de Janeiro é com a DeliveryPrint: a sua Gráfica Digital Online! Peça diretamente de sua casa e receba em qualquer lugar do estado e da cidade do Rio de Janeiro! Entregamos para todo o Brasil com prazo mínimo! Impressão colorida e impressão em preto e branco com preço baixo e alta qualidade garantidos! Faça seu pedido sem sair de casa: entre em nosso site ou peça seu orçamento por e-mail ou whatsapp. Não perca tempo e conheça a qualidade de nossos produtos! Atendentes qualificados para satisfazer a sua demanda! Imprimir online ficou fácil: diversos tipos de papéis, diversos tipos de acabamento, seja para imprimir colorido ou imprimir em preto e branco! Nos conheça e se surpreenda com a facilidade!'
            ]);
        },
    ],
    [
        'pattern' => '/grafica-vila-olimpia/amp',
        'callback' => function () {
            $layout = new Layout();
            $layout
                ->setCanonical('/grafica-vila-olimpia')
                ->setMetaDescription('A DeliveryPrint é a sua melhor opção em Impressão e Encadernação. Entregamos na Vila Olimpia e em toda São Paulo de forma ágil e com alta qualidade. Faça seu pedido em nosso site de maneira fácil e receba ainda hoje!')
                ->appendTitle('Gráfica na Vila Olímpia', ' | ')
                ->setLayout('layout-amp')
                ->render('produto/amp-template-produto', [
                    'title'     => 'Gráfica na Vila Olímpia', // = msg_b
                    'subtitle'  => 'Serviço de Impressão e Encadernação feito para você', //msg_c
                    'breadc'    => 'Gráfica na Vila Olímpia', //msg_a
                    'img_url'   => '/img/Gráfica Vila Olímpia.jpg',
                    'li_es'     => [
                        'Escolha como deseja sua Impressão',
                        'Peça por nosso site, é simples',
                        'Ótimos preços e Entrega Rápida',
                    ],
                    'price'  => '0,11',
                    'unit'   => 'página', //unidade ou página.
                    'a_0'    => '',
                    'a_1'    => '',
                    'a_2'    => '',
                    'a_3'    => '',
                    'a_4'    => true,
                    'a_5'    => '',
                    'text'  => 'Já pode parar de procurar gráfica digital e copiadora na Vila Olímpia e região! Com a DeliveryPrint, a sua Gráfica Digital Online, você não precisará mais correr atrás de gráficas na Vila Olímpia. Com funcionamento 100% online, compre de casa e receba até no mesmo dia! Impressão colorida e impressão em preto e branco em alta qualidade e com o preço lá embaixo! Preços e condições diferenciadas para empresas e pessoas jurídicas em geral. Imprimir online ficou fácil: seja imprimir colorido ou imprimir em preto e branco, na DeliveryPrint fazemos do jeito que você precisa. Diversos tipos de papel - couchê, sulfite, A3, A4, A4 - e de acabamento - espiral, wire-o e muito mais! Faça seu pedido!'
            ]);
        },
    ],
    [
        'pattern' => '/grafica-faria-lima/amp',
        'callback' => function () {
            $layout = new Layout();
            $layout
                ->setCanonical('/grafica-faria-lima')
                ->setMetaDescription('Serviços de Impressão, encadernação e entrega. Somos uma gráfica digital. A DeliveryPrint fez com que fazer pedidos de impressão sejam mais fáceis, rápidos e online. Impressão de Qualidade é aqui!')
                ->appendTitle('Gráfica Faria Lima', ' | ')
                ->setLayout('layout-amp')
                ->render('produto/amp-template-produto', [
                    'title'     => 'Gráfica na Faria Lima', // = msg_b
                    'subtitle'  => 'Impressão Online Delivery para você', //msg_c
                    'breadc'    => 'Gráfica na Faria Lima', //msg_a
                    'img_url'   => '/img/Gráfica na Faria Lima.jpg',
                    'li_es'     => [
                        'Impressões',
                        'Encadernações',
                        'Entregamos onde você quiser',
                    ],
                    'price'  => '0,11',
                    'unit'   => 'página', //unidade ou página.
                    'a_0'    => '',
                    'a_1'    => '',
                    'a_2'    => '',
                    'a_3'    => '',
                    'a_4'    => true,
                    'a_5'    => '',
                    'text'  => ''
            ]);
        },
    ],
    [
        'pattern' => '/grafica-pinheiros/amp',
        'callback' => function () {
            $layout = new Layout();
            $layout
                ->setCanonical('/grafica-pinheiros')
                ->setMetaDescription('Entregamos impressão e Encadernações em Pinheiros, somos uma Gráfica Digital com Delivery para todo Brasil. Compre no site da DeliveryPrint suas apostilas e demais arquivos impressos.')
                ->appendTitle('Gráfica Pinheiros', ' | ')
                ->setLayout('layout-amp')
                ->render('produto/amp-template-produto', [
                    'title'     => 'Gráfica em Pinheiros', // = msg_b
                    'subtitle'  => 'A Gráfica Digital Online de Pinheiros', //msg_c
                    'breadc'    => 'Gráfica em Pinheiros', //msg_a
                    'img_url'   => '/img/Gráfica em Pinheiros.jpg',
                    'li_es'     => [
                        'Impressão e Encadernaçao',
                        'Entregas Rápidas',
                        'Atendimento feito para você',
                    ],
                    'price'  => '0,11',
                    'unit'   => 'página', //unidade ou página.
                    'a_0'    => '',
                    'a_1'    => '',
                    'a_2'    => '',
                    'a_3'    => '',
                    'a_4'    => true,
                    'a_5'    => '',
                    'text'  => ''
            ]);
        },
    ],
    [
        'pattern' => '/grafica-jardins/amp',
        'callback' => function () {
            $layout = new Layout();
            $layout
                ->setCanonical('/grafica-jardins')
                ->setMetaDescription('Gráfica no bairro do Jardins em São Paulo, entregamos para toda Capital Impressos, Encadernações e materiais impressos em papel. A DeliveryPrint é um serviço de Impressão On Demand feito para você.')
                ->appendTitle('Gráfica Jardins', ' | ')
                ->setLayout('layout-amp')
                ->render('produto/amp-template-produto', [
                    'title'     => 'Gráfica Jardins', // = msg_b
                    'subtitle'  => 'O Serviço de Impressão Delivery de São Paulo', //msg_c
                    'breadc'    => 'Gráfica Jardins', //msg_a
                    'img_url'   => '/img/Gráfica Jardins.jpg',
                    'li_es'     => [
                        'Impressão em Preto e Branco ou Colorido',
                        'Delivery Rápido',
                        'Preços justos e Impressão de Qualidade',
                    ],
                    'price'  => '0,11',
                    'unit'   => 'página', //unidade ou página.
                    'a_0'    => '',
                    'a_1'    => '',
                    'a_2'    => '',
                    'a_3'    => '',
                    'a_4'    => true,
                    'a_5'    => '',
                    'text'  => 'Achar gráficas de qualidade nos Jardins está ficando cada vez mais difícil. Preços abusivos, prazos gigantescos e péssima qualidade de impressão são as marcas da maioria das gráficas. Pois conheça a DeliveryPrint, o serviço de impressão online que deixa essas gráficas comendo toner - e poeira. Peça direto do conforto de sua casa ou sem sair do escritório (valores especiais para pessoas jurídicas) e receba suas impressões até no mesmo dia! Imprimir em preto e branco e imprimir colorido ficou fácil - e barato! Diversos tipos de papéis, de encadernações, acabamentos e serviços gráficos em geral. Impressão em preto e branco e impressão colorida a laser de alta qualidade é na DeliveryPrint!'
            ]);
        },
    ],
    [
        'pattern' => '/grafica-centro/amp',
        'callback' => function () {
            $layout = new Layout();
            $layout
                ->setCanonical('/grafica-centro')
                ->setMetaDescription('Impressão e Encadernação é na DeliveryPrint, preço baixo, entrega para todo brasil e acabamento profissional. Peça por nosso site, compre suas impressões de maneira fácil e rápido.')
                ->appendTitle('Gráfica Centro', ' | ')
                ->setLayout('layout-amp')
                ->render('produto/amp-template-produto', [
                    'title'     => 'Gráfica Centro', // = msg_b
                    'subtitle'  => 'A Impressão que te Impressiona', //msg_c
                    'breadc'    => 'Gráfica Centro', //msg_a
                    'img_url'   => '/img/Gráfica Centro.jpg',
                    'li_es'     => [
                        'Preto e Branco ou Colorido',
                        'Impressão a Laser Profissional',
                        'Várias opções em Papéis',
                    ],
                    'price'  => '0,11',
                    'unit'   => 'página', //unidade ou página.
                    'a_0'    => '',
                    'a_1'    => '',
                    'a_2'    => '',
                    'a_3'    => '',
                    'a_4'    => true,
                    'a_5'    => '',
                    'text'  => 'Chega de passar sufoco e esperar horas em gráficas de péssima qualidade no Centro de São Paulo. Com a DeliveryPrint você não precisa mais se preocupar: fazemos todo o trabalho para você! Faça seu pedido direto de sua casa ou de sua empresa e receba até no mesmo dia no Centro de SP. Impressão colorida e impressão em preto e branco com preço baixo e altíssima qualidade, comprovada pelo nossos clientes. Temos diversos tipos de papéis, de acabamentos e de serviços gráficos em geral. Peça seu orçamento por whatsapp e e-mail ou faça seu pedido em nosso site. imprimir colorido ou imprimir em preto e branco ficou fácil: impressão online onde você estiver! Gráfica Digital Online é com a DeliveryPrint!'
            ]);
        },
    ],
    [
        'pattern' => '/grafica-vila-mariana/amp',
        'callback' => function () {
            $layout = new Layout();
            $layout
                ->setCanonical('/grafica-vila-mariana')
                ->setMetaDescription('Impressão Colorida, Impressão em preto e Branco, Impressão em papel sulfite e Couché com alta qualidade, entrega ágil e atendimento de ponta é na DeliveryPrint. Faça seu pedido em nosso sistema, é fácil.')
                ->appendTitle('Gráfica Vila Mariana', ' | ')
                ->setLayout('layout-amp')
                ->render('produto/amp-template-produto', [
                    'title'     => 'Gráfica Vila Mariana', // = msg_b
                    'subtitle'  => 'A melhor impressão é a que fica', //msg_c
                    'breadc'    => 'Gráfica Vila Mariana', //msg_a
                    'img_url'   => '/img/Gráfica Vila Mariana.jpg',
                    'li_es'     => [
                        'Serviços de Impressão e Encadernação',
                        'Bons preços, atendimento Rápido',
                        'Entregas para todo Brasil',
                    ],
                    'price'  => '0,11',
                    'unit'   => 'página', //unidade ou página.
                    'a_0'    => '',
                    'a_1'    => '',
                    'a_2'    => '',
                    'a_3'    => '',
                    'a_4'    => true,
                    'a_5'    => '',
                    'text'  => 'Achar gráficas de qualidade na Vila Mariana está ficando cada vez mais difícil. Preços abusivos, prazos gigantescos e péssima qualidade de impressão são as marcas da maioria das gráficas. Pois conheça a DeliveryPrint, o serviço de impressão online que deixa essas gráficas comendo toner - e poeira. Peça direto do conforto de sua casa ou sem sair do escritório (valores especiais para pessoas jurídicas) e receba suas impressões até no mesmo dia! Imprimir em preto e branco e imprimir colorido ficou fácil - e barato! Diversos tipos de papéis, de encadernações, acabamentos e serviços gráficos em geral. Impressão em preto e branco e impressão colorida a laser de alta qualidade é na DeliveryPrint!'
            ]);
        },
    ],
    [
        'pattern' => '/grafica-itaim-bibi/amp',
        'callback' => function () {
            $layout = new Layout();
            $layout
                ->setCanonical('/grafica-itaim-bibi')
                ->setMetaDescription('Impressão de apostilas, apresentações, material de estudo, documentos e muito mais é só na DeliveryPrint. Somos um serviço de Impressão onde você faz seu pedido pela internet em poucos minutos e pode receber até no mesmo dia.')
                ->appendTitle('Gráfica Itaim Bibi', ' | ')
                ->setLayout('layout-amp')
                ->render('produto/amp-template-produto', [
                    'title'     => 'Gráfica Itaim Bibi', // = msg_b
                    'subtitle'  => 'Impressões com Delivery', //msg_c
                    'breadc'    => 'Gráfica Itaim Bibi', //msg_a
                    'img_url'   => '/img/Gráfica Itaim Bibi.jpg',
                    'li_es'     => [
                        'Maquinário Profissional',
                        'Atendimento Personalizado',
                        'Melhores Preços',
                    ],
                    'price'  => '0,11',
                    'unit'   => 'página', //unidade ou página.
                    'a_0'    => '',
                    'a_1'    => '',
                    'a_2'    => '',
                    'a_3'    => '',
                    'a_4'    => true,
                    'a_5'    => '',
                    'text'  => 'Procurando gráfica no Itaim Bibi, Vila Olímpia e região? Pode parar de procurar! Na DeliveryPrint, sua gráfica digital online, você ou sua empresa não precisam mais se preocupar em ir até a gráfica e esperar dias e mais dias para ter seu serviço em mãos! Imprimir online ficou fácil - e barato: peça direto de sua casa ou de sua empresa e receba seu serviço até no mesmo dia! Impressão colorida e impressão em preto e branco de alta qualidade, com preço baixo garantido pelo nossos clientes! Diversos tipos de acabamentos, de papéis e de serviços gráficos em geral. Imprimir online ficou fácil: peça seu orçamento pelo whatsapp e e-mail ou faça seu pedido em nosso site! Gráfica Digital é na DeliveryPrint!'
            ]);
        },
    ],
    [
        'pattern' => '/impressao-rapida/amp',
        'callback' => function () {
            $layout = new Layout();
            $layout
                ->setCanonical('/impressao-rapida')
                ->setMetaDescription('Impressão e Entrega Rápida é na DeliveryPrint. Serviços de impressão e encadernação online, com entrega expressa para o mesmo dia! Peça seu orçamento agora mesmo. Impressão preto e branco e colorida é na DeliveryPrint!')
                ->appendTitle('Impressão Rápida', ' | ')
                ->setLayout('layout-amp')
                ->render('produto/amp-template-produto', [
                    'title'     => 'Impressão e Entrega Rápida', // = msg_b
                    'subtitle'  => 'Imprima Online e receba até no mesmo dia!', //msg_c
                    'breadc'    => 'Impressão Rápida', //msg_a
                    'img_url'   => '/img/Impressão Rápida.jpg',
                    'li_es'     => [
                        'Impressão rápida em alta definição',
                        'Preço baixo e entrega expressa!',
                        'Diversas opções de papéis e acabamento',
                    ],
                    'price'  => '0,11',
                    'unit'   => 'página', //unidade ou página.
                    'a_0'    => '',
                    'a_1'    => '',
                    'a_2'    => '',
                    'a_3'    => '',
                    'a_4'    => true,
                    'a_5'    => '',
                    'text'  => ''
            ]);
        },
    ],
    [
        'pattern' => '/copiadora-se/amp',
        'callback' => function () {
            $layout = new Layout();
            $layout
                ->setCanonical('/copiadora-se')
                ->setMetaDescription('Na DeliveryPrint você efetua seu pedido de impressão de maneira prática e rápida. Impressão colorida e em preto e branco, diversos tipos de papel e acabamento. Qualidade e preço baixo! Entrega expressa no mesmo dia para a região central de São Paulo!')
                ->appendTitle('Copiadora Praça da Sé', ' | ')
                ->setLayout('layout-amp')
                ->render('produto/amp-template-produto', [
                    'title'     => 'Gráfica Digital e Copiadora - Praça da Sé', // = msg_b
                    'subtitle'  => 'Faça seu pedido rapidamente e receba até no mesmo dia!', //msg_c
                    'breadc'    => 'Copiadora Praça da Sé', //msg_a
                    'img_url'   => '/img/Copiadora Sé.jpg',
                    'li_es'     => [
                        'Impressão de alta qualidade!',
                        'Os melhores preços para impressão colorida e preto e branco!',
                        'Variadas opções de papéis e acabamento!',
                    ],
                    'price'  => '0,11',
                    'unit'   => 'página', //unidade ou página.
                    'a_0'    => '',
                    'a_1'    => '',
                    'a_2'    => '',
                    'a_3'    => '',
                    'a_4'    => true,
                    'a_5'    => '',
                    'text'  => 'Gráfica Digital e Copiadora na Praça da Sé e região do Centro de São Paulo é com a DeliveryPrint, a sua Gráfica Digital Online! Peça direto de sua casa ou de sua empresa e receba até no mesmo dia! Impressão colorida e impressão em preto e branco com qualidade e preço baixo garantidos. Preços e condições diferenciadas para empresas e pessoas jurídicas em geral! Diversos tipos de acabamento e de papéis. Imprimir colorido e imprimir em preto e branco ficou fácil - e barato! Peça seu orçamento por e-mail e whatsapp ou faça seu pedido direto em nosso site! Imprimir online é com a DeliveryPrint: o seu serviço de impressão online com nota máxima no Google Avaliações! Nos conheça!'
            ]);
        },
    ],
    [
        'pattern' => '/impressao-dissertacao/amp',
        'callback' => function () {
            $layout = new Layout();
            $layout
                ->setCanonical('/impressao-dissertacao')
                ->setMetaDescription('Chega de dor de cabeça! Imprima sua dissertação na DeliveryPrint, o seu serviço de impressão online. Qualidade comprovada, preço baixo e entrega expressa! Peça seu orçamento! Em preto e branco ou colorido, imprimir na DeliveryPrint é aprovação na certa!')
                ->appendTitle('Impressão de Dissertação', ' | ')
                ->setLayout('layout-amp')
                ->render('produto/amp-template-produto', [
                    'title'     => 'Impressão de Dissertação', // = msg_b
                    'subtitle'  => 'Impressione sua banca com a melhor impressão do mercado!', //msg_c
                    'breadc'    => 'Impressão Dissertação', //msg_a
                    'img_url'   => '/img/Impressão de Dissertação.jpg',
                    'li_es'     => [
                        'Qualidade e preço baixo comprovados!',
                        'Entrega expressa e atendimento diferenciado!',
                        'Diversas opções de papéis e encadernações',
                    ],
                    'price'  => '0,11',
                    'unit'   => 'página', //unidade ou página.
                    'a_0'    => '',
                    'a_1'    => '',
                    'a_2'    => '',
                    'a_3'    => '',
                    'a_4'    => true,
                    'a_5'    => '',
                    'text'  => ''
            ]);
        },
    ],
    [
        'pattern' => '/copiadora-sumare/amp',
        'callback' => function () {
            $layout = new Layout();
            $layout
                ->setCanonical('/copiadora-sumare')
                ->setMetaDescription('Sua melhor opção para serviços de Impressão e Encadernação. Não perca tempo andando até uma gráfica! Orçamento em segundos, pagamento facilitado e frete para todo país.')
                ->appendTitle('Copiadora Sumaré', ' | ')
                ->setLayout('layout-amp')
                ->render('produto/amp-template-produto', [
                    'title'     => 'Copiadora Sumaré', // = msg_b
                    'subtitle'  => 'Faça sua Impressão em apenas 4 passos rápidos', //msg_c
                    'breadc'    => 'Copiadora Sumaré', //msg_a
                    'img_url'   => '/img/Copiadora Sumaré.jpg',
                    'li_es'     => [
                        'Impressão em A5, A4, A3, A2 e muito mais',
                        'Entrega até no mesmo dia*',
                        'Espiral, Wire-o, Capa Dura, Grampeado e solto',
                    ],
                    'price'  => '0,11',
                    'unit'   => 'página', //unidade ou página.
                    'a_0'    => '',
                    'a_1'    => '',
                    'a_2'    => '',
                    'a_3'    => '',
                    'a_4'    => true,
                    'a_5'    => '',
                    'text'  => 'A procura de serviços de copiadora e Gráfica Digital na Sumaré e região da Zona Oeste de São Paulo? Já pode parar de procurar! Conheça a DeliveryPrint, a Gráfica Digital Online com a melhor avaliação no Google, e se surpreenda! Impressão colorida e impressão em preto e branco de alta qualidade e preço baixo garantido! Trabalhamos com valores e condições diferenciadas para empresas e pessoas jurídicas. Faça seu pedido direto de sua casa ou de sua empresa e receba ou retire até no mesmo dia. Diversos tipos de papel - A5, A4, A3, plotagem em grandes tamanhos - e de acabamentos! Peça seu orçamento por e-mail e whatsapp ou faça seu pedido direto em nosso site! Impressão online é na DP!'
            ]);
        },
    ],
    [
        'pattern' => '/copiadora-usp/amp',
        'callback' => function () {
            $layout = new Layout();
            $layout
                ->setCanonical('/copiadora-usp')
                ->setMetaDescription('Solução em Impressão e Encadernação para alunos da USP. Você não precisa mais pegar fila na xerox, peça online. Orçamento em segundos, pagamento facilitado e frete para todo país.')
                ->appendTitle('Copiadora para USP', ' | ')
                ->setLayout('layout-amp')
                ->render('produto/amp-template-produto', [
                    'title'     => 'Copiadora para USP', // = msg_b
                    'subtitle'  => 'Impressão com desconto para Universitários', //msg_c
                    'breadc'    => 'Copiadora USP', //msg_a
                    'img_url'   => '/img/Copiadora USP.jpg',
                    'li_es'     => [
                        'Impressão em Preto e Branco ou Colorido',
                        'Entregamos para você',
                        'Para descontos universitários chamem no CHAT',
                    ],
                    'price'  => '0,11',
                    'unit'   => 'página', //unidade ou página.
                    'a_0'    => '',
                    'a_1'    => '',
                    'a_2'    => '',
                    'a_3'    => '',
                    'a_4'    => true,
                    'a_5'    => '',
                    'text'  => 'Não perca mais tempo em filas para imprimir seus materiais de estudo. Você, estudante, está precisando de impressão para seus estudos, seus trabalhos acadêmicos ou de seu TCC? Conheça a DeliveryPrint, o serviço de impressão online que tem a confiança dos universitários de todo o Brasil! Peça diretamente de sua casa e receba até no mesmo dia, sem precisar parar de estudar para ir até a gráfica. Imprimir em preto e branco e imprimir colorido nunca foi tão fácil! Diversos tipos de papel e de acabamentos para seus materiais e trabalhos, inclusive Capa Dura tipo TCC. Se precisa imprimir na USP ou na Cidade Universitária já sabe: é na DeliveryPrint, o serviço de impressão e gráfica online da sua graduação!'
            ]);
        },
    ],
    [
        'pattern' => '/copiadora-unifesp/amp',
        'callback' => function () {
            $layout = new Layout();
            $layout
                ->setCanonical('/copiadora-unifesp')
                ->setMetaDescription('Está na UNIFESP e precisa imprimir e gastar pouco? Somos o melhor serviço de impressão Online. Orçamento em segundos, pagamento facilitado e frete para todo país.')
                ->appendTitle('Copiadora para UNIFESP', ' | ')
                ->setLayout('layout-amp')
                ->render('produto/amp-template-produto', [
                    'title'     => 'Copiadora para UNIFESP', // = msg_b
                    'subtitle'  => 'Impressão que cabe em seu Bolso', //msg_c
                    'breadc'    => 'Copiadora UNIFESP', //msg_a
                    'img_url'   => '/img/Copiadora Unifesp.jpg',
                    'li_es'     => [
                        'Impressão com desconto para Universitários',
                        'Preço Baixo e Entrega Rápida',
                        'Diversas opções de Encadernações',
                    ],
                    'price'  => '0,11',
                    'unit'   => 'página', //unidade ou página.
                    'a_0'    => '',
                    'a_1'    => '',
                    'a_2'    => '',
                    'a_3'    => '',
                    'a_4'    => true,
                    'a_5'    => '',
                    'text'  => 'É universitário e cansou de estudar pelo computador? Conheça a DeliveryPrint, a gráfica digital online reconhecida por impressão de qualidade e preço baixo! Descontos e preços especiais para universitários. Se você estuda na UNIFESP e precisa de impressão, entre em contato conosco! Impressão colorida ou impressão em preto e branco, com diversos tipos de encadernações e acabamentos. Qualidade que cabe no seu bolso! Compre direto de sua casa e receba ou retire até no mesmo dia! Imprimir online ficou fácil! Imprima online seus materiais de estudo e não se arrependa! Descontos exclusivos para universitários e estudantes. Peça seu orçamento por e-mail ou whatsapp ou faça seu pedido em nosso site!'
            ]);
        },
    ],
    [
        'pattern' => '/copiadora-sao-bento/amp',
        'callback' => function () {
            $layout = new Layout();
            $layout
                ->setCanonical('/copiadora-sao-bento')
                ->setMetaDescription('Seu serviço de Impressão no centro de São Paulo. Impressões baratas com pedidos online e entrega rápida. DeliveryPrint é a sua Copiadora Online.')
                ->appendTitle('Copiadora São Bento', ' | ')
                ->setLayout('layout-amp')
                ->render('produto/amp-template-produto', [
                    'title'     => 'Copiadora São Bento', // = msg_b
                    'subtitle'  => 'A Copiadora Online do centro', //msg_c
                    'breadc'    => 'Copiadora São Bento', //msg_a
                    'img_url'   => '/img/Copiadora São Bento.jpg',
                    'li_es'     => [
                        'Serviço de Impressão',
                        'Serviço de Encadernação e Acabamentos',
                        'Preços Baixos e Entrega Rápida',
                    ],
                    'price'  => '0,11',
                    'unit'   => 'página', //unidade ou página.
                    'a_0'    => '',
                    'a_1'    => '',
                    'a_2'    => '',
                    'a_3'    => '',
                    'a_4'    => true,
                    'a_5'    => '',
                    'text'  => 'Está a procura de copiadora, gráfica digital e serviço de impressão na região da São Bento e do Centro de São Paulo? Pois pode parar de procurar! Na DeliveryPrint, seu serviço de impressão online, você encontra tudo isso e muito mais! Faça seu pedido online e receba onde precisar, até no mesmo dia! Impressão em preto e branco e impressão colorida, diversos tipos de acabamento e de papéis, em alta qualidade e com preço baixo garantido por nossos clientes! Imprimir colorido e imprimir em preto e branco ficou fácil: faça seu pedido em nosso site ou peça seu orçamento por e-mail e Whatsapp e se surpreenda com a facilidade e atendimento profissional que entende a sua demanda! Conheça nosso trabalho!'
            ]);
        },
    ],
    [
        'pattern' => '/copiadora-perdizes/amp',
        'callback' => function () {
            $layout = new Layout();
            $layout
                ->setCanonical('/copiadora-perdizes')
                ->setMetaDescription('Os melhores preços de impressão você só encontra na DeliveryPrint. Fazemos impressão em alta qualidade e acabamentos profissionais com entrega ágil em todo Brasil.')
                ->appendTitle('Copiadora Perdizes', ' | ')
                ->setLayout('layout-amp')
                ->render('produto/amp-template-produto', [
                    'title'     => 'Copiadora Perdizes', // = msg_b
                    'subtitle'  => 'Impressão e Encadernação sem sair de casa', //msg_c
                    'breadc'    => 'Copiadora Perdizes', //msg_a
                    'img_url'   => '/img/Copiadora Perdizes.jpg',
                    'li_es'     => [
                        'Serviço de Impressão, Encadernação e Acabamentos',
                        'Preço Baixo e Entrega Rápida',
                        'Atendimento Incrível para você',
                    ],
                    'price'  => '0,11',
                    'unit'   => 'página', //unidade ou página.
                    'a_0'    => '',
                    'a_1'    => '',
                    'a_2'    => '',
                    'a_3'    => '',
                    'a_4'    => true,
                    'a_5'    => '',
                    'text'  => 'Você já pode parar de procurar copiadora e gráfica digital em Perdizes e na Zona Oeste de São Paulo! Com a DeliveryPrint, a sua gráfica digital online, você não precisa mais se preocupar para imprimir seus materiais. Impressão em preto e branco e impressão colorida em alta qualidade, com preço baixo garantido! Preços e condições diferenciadas para empresas. Compre em casa e receba até no mesmo dia! Diversos tipos de papel e de acabamento para você imprimir colorido ou imprimir em preto e branco com qualidade profissional! Não perca tempo, conheça nossos serviços, peça seu orçamento por e-mail ou whatsapp ou faça seu pedido diretamente em nosso site! DeliveryPrint: seu serviço de impressão online!'
            ]);
        },
    ],
    [
        'pattern' => '/copiadora-higienopolis/amp',
        'callback' => function () {
            $layout = new Layout();
            $layout
                ->setCanonical('/copiadora-higienopolis')
                ->setMetaDescription('Serviço de Impressão e Encadernação com entrega na Av Angélica e toda São Paulo. Faça seu pedido pela internet, não perca tempo passando PDF para o PenDrive.')
                ->appendTitle('Copiadora Higienopolis', ' | ')
                ->setLayout('layout-amp')
                ->render('produto/amp-template-produto', [
                    'title'     => 'Copiadora Higienopolis', // = msg_b
                    'subtitle'  => 'O Serviço de Impressão para todas as horas', //msg_c
                    'breadc'    => 'Copiadora Higienopolis', //msg_a
                    'img_url'   => '/img/Copiadora Higienopolis.jpg',
                    'li_es'     => [
                        'Impressões e Acabamentos',
                        'Melhores Preços por página',
                        'Diversas opções de Encadernações',
                    ],
                    'price'  => '0,11',
                    'unit'   => 'página', //unidade ou página.
                    'a_0'    => '',
                    'a_1'    => '',
                    'a_2'    => '',
                    'a_3'    => '',
                    'a_4'    => true,
                    'a_5'    => '',
                    'text'  => 'Precisando de impressão na região de Higienópolis, Vila Buarque, Pacaembu e Perdizes? Serviço de impressão e gráfica digital na região Centro-Oeste de São Paulo é com a DeliveryPrint: imprimir online ficou fácil - e barato! Impressão em preto e branco e impressão colorida de alta qualidade com preço baixo garantido! Peça diretamente de sua casa ou de sua empresa e receba até no mesmo dia! Diversos tipos de papel, de acabamentos e de serviços gráficos em geral. Imprimir colorido ou imprimir em preto e branco sem sair de casa. Conheça nossos serviços e se surpreenda: na DeliveryPrint imprimir online ficou fácil! Faça seu pedido em nosso site ou peça um orçamento pelo e-mail ou whatsapp!'
            ]);
        },
    ],
    [
        'pattern' => '/copiadora-pacaembu/amp',
        'callback' => function () {
            $layout = new Layout();
            $layout
                ->setCanonical('/copiadora-pacaembu')
                ->setMetaDescription('Impressão e encadernação no bairro do pacaembu e arredores com ótimos preços, qualidade comprovada e entrega rápida. DeliveryPrint é a sua Copiadora Online.')
                ->appendTitle('Copiadora Pacaembu', ' | ')
                ->setLayout('layout-amp')
                ->render('produto/amp-template-produto', [
                    'title'     => 'Copiadora Pacaembu', // = msg_b
                    'subtitle'  => 'Para que ligar se você pode imprimir online?', //msg_c
                    'breadc'    => 'Copiadora Pacaembu', //msg_a
                    'img_url'   => '/img/Copiadora Pacaembu.jpg',
                    'li_es'     => [
                        'Impressão em Preto e Branco ou Colorido',
                        'Encaderne em Espiral, Wire-o ou Capa Dura',
                        'Entregamos para você, rápido e barato',
                    ],
                    'price'  => '0,11',
                    'unit'   => 'página', //unidade ou página.
                    'a_0'    => '',
                    'a_1'    => '',
                    'a_2'    => '',
                    'a_3'    => '',
                    'a_4'    => true,
                    'a_5'    => '',
                    'text'  => 'Precisando de copiadora e gráfica digital na região do Pacaembu e da Zona Oeste de São Paulo? Conheça a DeliveryPrint: gráfica digital online de alta qualidade e preço baixo comprovados por nossos clientes! Compre online e receba onde quiser, até no mesmo dia! Impressão colorida e impressão em preto e branco, em diversos tipos de papéis e de acabamentos. Encaderne em espiral, wire-o ou capa dura. Trabalhamos com preços e condições diferenciadas para empresas e pessoas jurídicas em geral. Imprimir online ficou fácil e barato. Faça seu pedido em nosso site ou peça seu orçamento por e-mail e whatsapp e se surpreenda com a qualidade e facilidade da DeliveryPrint, seu serviço de impressão online!'
            ]);
        },
    ],
    [
        'pattern' => '/copiadora-saude/amp',
        'callback' => function () {
            $layout = new Layout();
            $layout
                ->setCanonical('/copiadora-saude')
                ->setMetaDescription('Impressão de alta qualidade, encadernação de ótimo acabamento. Orçamento em segundos, pagamento facilitado e frete para todo país. DeliveryPrint é a sua Impressora Online.')
                ->appendTitle('Copiadora Saúde', ' | ')
                ->setLayout('layout-amp')
                ->render('produto/amp-template-produto', [
                    'title'     => 'Copiadora Saúde', // = msg_b
                    'subtitle'  => 'A Impressora do Futuro está aqui e é Online', //msg_c
                    'breadc'    => 'Copiadora Saúde', //msg_a
                    'img_url'   => '/img/Copiadora Saúde.jpg',
                    'li_es'     => [
                        'Preto e Branco ou Colorido',
                        'Várias opções em Papéis e gramaturas',
                        'Entregas Rápidas e Retiradas Gratuitas',
                    ],
                    'price'  => '0,11',
                    'unit'   => 'página', //unidade ou página.
                    'a_0'    => '',
                    'a_1'    => '',
                    'a_2'    => '',
                    'a_3'    => '',
                    'a_4'    => true,
                    'a_5'    => '',
                    'text'  => 'Serviços de copiadora, gráfica digital e impressão é com a DeliveryPrint: seu serviço de impressão online de alta qualidade e preço baixo garantidos! Imprimir em preto e branco e imprimir colorido ficou fácil: peça direto do conforto de sua casa ou sem sair do escritório e receba até no mesmo dia na região da Saúde e Zona Sul de São Paulo! Deixe o trabalho difícil para nós: chega de ir atrás de gráficas duvidosas na rua! Trabalhamos com diversos tipos de papel, de acabamentos, encadernações e serviços gráficos em geral. Peça seu orçamento via E-mail e Whatsapp ou faça seu pedido em nosso site e se surpreenda com a facilidade e qualidade de nossos serviços. Atendimento especializado e qualidade de impressão: imprima online conosco!'
            ]);
        },
    ],
    [
        'pattern' => '/copiadora-bras/amp',
        'callback' => function () {
            $layout = new Layout();
            $layout
                ->setCanonical('/copiadora-bras')
                ->setMetaDescription('A melhor Copiadora Online da Região do Brás , Serviço de Impressão e Encadernação com Alta Qualidade é na DeliveryPrint. Orçamento em segundos, pagamento facilitado e frete incluso para todo país.')
                ->appendTitle('Copiadora Brás', ' | ')
                ->setLayout('layout-amp')
                ->render('produto/amp-template-produto', [
                    'title'     => 'Copiadora Brás', // = msg_b
                    'subtitle'  => 'Impressão Online com Entrega', //msg_c
                    'breadc'    => 'Copiadora Brás', //msg_a
                    'img_url'   => '/img/Copiadora Brás.jpg',
                    'li_es'     => [
                        'Serviço de Impressão',
                        'Ótimos Preços',
                        'Alta Resolução em sua Impressão',
                    ],
                    'price'  => '0,11',
                    'unit'   => 'página', //unidade ou página.
                    'a_0'    => '',
                    'a_1'    => '',
                    'a_2'    => '',
                    'a_3'    => '',
                    'a_4'    => true,
                    'a_5'    => '',
                    'text'  => 'Preço baixo e alta qualidade de impressão a laser é com a DeliveryPrint: o seu serviço de impressão online! Serviço de impressão, gráfica digital e copiadora no Brás, Parque Dom Pedro II e região da Zona Leste de São Paulo é aqui mesmo! Chega de bater perna atrás de gráficas de péssima qualidade, com preços abusivos e prazos gigantescos. Com a DeliveryPrint você não precisa mais se preocupar: faça seu pedido direto de sua casa ou empresa e receba até no mesmo dia! Valores diferenciados para PJ! Diversos tipos de papéis, de acabamentos e de serviços gráficos em geral. Imprimir colorido e imprimir em preto e branco com facilidade e confiança: apenas na DeliveryPrint! Peça conosco!'
            ]);
        },
    ],
    [
        'pattern' => '/amp',
        'callback' => function () {
            $layout = new Layout();
            $layout
                ->setCanonical('/')
                ->appendTitle("Impressão colorida e encadernação online barata", " | ")
                ->setLayout('layout-amp-home')
                ->render('ws/home-amp');
        },
    ],
    [
        'pattern' => '/como-funciona/amp',
        'callback' => function () {
            $layout = new Layout();
            $layout
                ->setCanonical('como-funciona')
                ->appendTitle("Como Funciona", " | ")
                ->setLayout('layout-amp-home')
                ->render('ws/how-it-works-amp');
        },
    ],
    [
        'pattern' => '/ajuda/faq/amp',
        'callback' => function () {
            $layout = new Layout();
            $layout
                ->setCanonical('ajuda/faq')
                ->appendTitle("Dúvidas", " | ")
                ->setLayout('layout-amp-home')
                ->render('ws/faq-amp');
        },
    ],
    [
        'pattern' => '/pedido-empresas/amp',
        'callback' => function () {
            $layout = new Layout();
            $layout
                ->setCanonical('pedido-empresas')
                ->appendTitle("Corporate", " | ")
                ->setLayout('layout-amp-home')
                ->render('ws/corporate-amp');
        },
    ],
];
