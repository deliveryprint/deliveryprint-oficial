<?php

namespace IntecPhp;

use IntecPhp\View\Layout;

return [
    [
        'pattern' => '/',
        'callback' => function () {
            $layout = new Layout();
            $layout->setMetaDescription('Peça sua Impressão online com delivery rápido! Escolha sua impressão colorida ou pb de maneira ágil em nosso site e receba ainda hoje seus impressos com alta qualidade');
            $layout
            ->setAmp('/amp')
            ->addStylesheet('/css/home.min.css')
            ->addScript('/js/home.min.js')
            ->appendTitle("Impressão online delivery", " | ")
            ->render('ws/home');
        },
    ],
    [
        'pattern' => '/(impressao-online|orcamento)',
        'callback' => function () {
            $layout = new Layout();
            $layout->setMetaDescription('Impressão Online barata e de qualidade é na DeliveryPrint. Orçamento em segundos, pagamento facilitado e frete para todo país. Agora você não precisa mais se deslocar para imprimir e Encadernar! Faça sua Impressão Online Rápido.');
            $layout
                ->addScript('/js/request-order.min.js')
                ->addStylesheet('/css/request-order.min.css')
                ->setCanonical('/impressao-online')
                ->appendTitle('Impressão Online', ' | ')
                ->render('ws/request-order');
        },
    ],
    [
        'pattern' => '/quem-somos',
        'callback' => function () {
            $layout = new Layout();
            $layout
                ->setCanonical('/quem-somos')
                ->render('ws/about-us');
        },
    ],
    [
        'pattern' => '/como-funciona',
        'callback' => function () {
            $layout = new Layout();
            $layout->setMetaDescription('Você sabe como funciona imprimir na DeliveryPrint? é muito fácil pedir sua impressão com delivery, é mais barato do que ir até uma gráfica de rua e o atendimento é pessoal mesmo');
            $layout
                ->appendTitle("Como Funciona", " | ")
                ->addStylesheet('/css/how-it-works.min.css')
                ->addScript('/js/how-it-works.min.js')
                ->setCanonical('/como-funciona')
                ->render('ws/how-it-works');
        },
    ],
    [
        'pattern' => '/ajuda/faq',
        'callback' => function () {
            $layout = new Layout();
            $layout->setMetaDescription('Tem dúvidas de onde imprimir seu material? Aqui explicamos como fazemos a impressão, entrega, quais materiais usamos e muito mais. Faça sua impressão e encadernação online!');
            $layout
                ->appendTitle("Dúvidas", " | ")
                ->addStylesheet('/css/faq.min.css')
                ->addScript('/js/faq.min.js')
                ->setCanonical('/ajuda/faq')
                ->render('ws/faq');
        },
    ],
    [
        'pattern' => '/pedido-empresas',
        'callback' => function () {
            $layout = new Layout();
            $layout->setMetaDescription('Sua empresa precisa imprimir? Temos um serviço de impressão ágil, barato, de qualidade e entregamos em todo Brasil seus impressos. Não perca tempo ligando para gráficas, peça online');
            $layout
                ->appendTitle("Para sua empresa", " | ")
                ->addStylesheet('/css/corporate.min.css')
                ->addScript('/js/corporate.min.js')
                ->setCanonical('/pedido-empresas')
                ->render('ws/corporate');
        },
    ],
    [
        'pattern' => '/produtos',
        'callback' => function () {
            $layout = new Layout();
            $layout
                ->setMetaDescription('Produtos: fazemos diversos serviços de Impressão em papeis A3, A4 e A5 com opções de Encadernação. Somos um serviço de Impressão Online com entrega para todo o Brasil de forma ágil e fácil.')
                ->appendTitle("Produtos", " | ")
                ->addScript('js/products.min.js')
                ->addStylesheet('/css/products.min.css')
                ->setCanonical('/produtos')
                ->render('ws/products');
        },
    ],
    [
        'pattern' => '/carrinho',
        'callback' => function () {
            $layout = new Layout();
            $layout->setMetaDescription('Peça sua impressão de maneira rápida e receba ainda hoje seu pedido. Somos uma copiadora online com serviço de delivery. Imprima barato e com alta qualidade');
            $layout->appendTitle('Carrinho');
            $layout
                ->setCanonical('/carrinho')
                ->addStylesheet('/css/cart-shopping.min.css')
                ->addScript('/js/shopping-cart.min.js')
                ->render('ws/shopping-cart');
        },
    ],
    [
        'pattern' => '/recuperar-senha',
        'callback' => function () {
            $layout = new Layout();
            $layout
                ->setCanonical('/recuperar-senha')
                ->addScript('/js/recover-pass.min.js')
                ->render('ws/recover-senha');
        },
    ],
    [
        'pattern' => '/verificar-email',
        'callback' => function () {
            $layout = new Layout();
            $layout
                ->setCanonical('/verificar-email')
                ->addScript('/js/verify-email.min.js')
                ->render('ws/verify-email');
        },
    ],
    [
        'pattern' => '/verificacao-necessaria',
        'callback' => function () {
            $layout = new Layout();
            $layout
                ->setCanonical('/verificacao-necessaria')
                ->addScript('/js/require-verification.min.js')
                ->render('ws/require-verification');
        },
    ],
    [
        'pattern' => '/(carrinho/pagamentos|segundo-pagamento)',
        'middlewares' => [
            Middleware\AuthenticationMiddleware::class . ':isAuthenticated',
        ],
        'callback' => function () {
            $layout = new Layout();
            $layout
                ->setCanonical('/carrinho/pagamentos')
                ->addStylesheet('/css/cart-shopping-payments.min.css')
                ->addScript('/js/shopping-cart-payments.min.js')
                ->addScript('https://js.iugu.com/v2')
                ->render('ws/shopping-cart-payments');
        },
    ],
    [
        'pattern' => '/dados-cadastrais',
        'middlewares' => [
            Middleware\AuthenticationMiddleware::class . ':isAuthenticated',
        ],
        'callback' => function () {
            $layout = new Layout();
            $layout
                ->setMetaRobots('noindex')
                ->setCanonical('/dados-cadastrais')
                ->addStylesheet('/css/user-registers.min.css')
                ->addScript('/js/user-registers.min.js')
                ->render('ws/user-registers');
        },
    ],
    [
        'pattern' => '/meus-pedidos',
        'callback' => function () {
            $layout = new Layout();
            $layout
                ->setMetaRobots('noindex')
                ->setCanonical('/meus-pedidos')
                ->addStylesheet('/css/my-requests.min.css')
                ->addScript('/js/my-requests.min.js')
                ->render('ws/my-requests');
        },
    ],
    [
        'pattern' => '/indicacoes',
        'callback' => function () {
            $layout = new Layout();
            $layout
                ->setMetaRobots('noindex')
                ->setCanonical('/indicacoes')
                ->addStylesheet('/css/my-invitations.min.css')
                ->addScript('/js/my-invitations.min.js')
                ->render('ws/my-invitations');
        },
    ],
    [
        'pattern' => '/(pedido-finalizado|segundo-pagamento-finalizado)',
        'middlewares' => [
            Middleware\AuthenticationMiddleware::class . ':isAuthenticated',
        ],
        'callback' => function () {
            $layout = new Layout();
            $layout
                ->setCanonical('/pedido-finalizado')
                ->addStylesheet('/css/payment-confirm.min.css')
                ->addScript('https://apis.google.com/js/platform.js')
                ->addScript('/js/payment-confirm.min.js')
                ->render('ws/payment-confirm');
        },
    ],
    [
        'pattern' => '/mapa-do-site',
        'callback' => function () {
            $layout = new Layout();
            $layout
                ->setMetaDescription('Mapa do site. Serviços de Impressão: Impressão colorida, Preto e Branco, Encadernação em Espiral, Capa Dura e Wire-o. Opções em Papel: Papel Sulfite e couché em A3, A4, A5, papel Carta, Reciclado e Ofício. Pedidos Online e Entregas para todo Brasil.')
                ->setCanonical('/mapa-do-site')
                ->addStylesheet('/css/site-map.min.css')
                ->addScript('/js/site-map.min.js')
                ->appendTitle("Mapa do Site", " | ")
                ->render('ws/site-map');
        },
    ],
    [
        'pattern' => '/orcamento-para-impressao',
        'callback' => function () {
            $layout = new Layout();
            $layout
                ->setMetaDescription('Faça seu orçamento de impressão Online em nosso site. Respondemos em instantes com nosso melhor preço e entrega. Somos um serviço de impressão e Encadernação Online, entregamos para Todo Brasil.')
                ->setCanonical('/orcamento-para-impressao')
                ->addStylesheet('/css/orcamento.min.css')
                ->addScript('/js/orcamento.min.js')
                ->appendTitle("Orçamento para Impressão", " | ")
                ->render('ws/orcamento');
        },
    ],
    [
        'pattern' => '/indique-a-deliveryprint',
        'callback' => function () {
            $layout = new Layout();
            $layout
                ->setMetaDescription('Participe de nosso programa de descontos e tenha sua impressão com o custo reduzido. É fácil participar, a pessoa indicada ganha desconto para imprimir e você também, é infinito!')
                ->setCanonical('/indique-a-deliveryprint')
                ->addStylesheet('/css/invite.min.css')
                ->addScript('/js/invite.min.js')
                ->appendTitle("Indique a DP", " | ")
                ->render('ws/invite');
        },
    ],
];
