<?php


return array_merge_recursive(
    require_once 'app/config/adm-routes.php',
    require_once 'app/config/api-routes.php',
    require_once 'app/config/ws-routes.php',
    require_once 'app/config/landing-page-routes.php',
    require_once 'app/config/product-routes.php',
    require_once 'app/config/landing-routes.php',
    require_once 'app/config/amp-routes.php'
);
