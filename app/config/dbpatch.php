<?php

$settings = require_once 'app/config/settings.php';

if(file_exists('app/config/settings.local.php')) {
    $settings = array_replace_recursive($settings, require 'app/config/settings.local.php');
}

$db = $settings['db_patch'];

$dbPatchConfig = [
    'limit'             => 10,
    'default_branch'    => 'default',
    'patch_directory'   => 'app/data/database',
    'patch_prefix'      => 'patch',
    'color'             => true,
    'dump_before_update'=> false,
    'dump_directory'    => '',
    # database settings
    'db' => [
        'adapter' => 'pdo_mysql',
        'params' => [
            'host'       => $db['host'],
            'dbname'     => $db['db_name'],
            'username'   => $db['db_user'],
            'password'   => $db['db_pass'],
            'port'       => $db['db_port'],
            'charset'    => 'utf8mb4',
            'bin_dir'    => '',
        ],
    ]
];
