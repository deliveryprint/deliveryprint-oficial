<?php

namespace IntecPhp;

use IntecPhp\View\Layout;

return [
    [
        'pattern' => '/impressao-online/0101',
        'callback' => function () {
            $layout = new Layout();
            $layout
                ->setLayout('layout-lp-impressao-online')
                ->addStylesheet('/css/landing-page-layout.min.css')
                ->appendTitle('Impressão Online', ' | ')
                ->render('landing-pages/impressao-online/0101');
        },
    ],
    [
        'pattern' => '/impressao-redes-sociais',
        'callback' => function () {
            $layout = new Layout();
            $layout
                ->setLayout('layout-landing-page')
                ->addStylesheet('/css/landing-page-layout.min.css')
                ->appendTitle('Impressão Online', ' | ')
                ->render('landing-pages/impressao-redes-sociais/index');
        },
    ],
    [
        'pattern' => '/impressora-online-barata',
        'callback' => function () {
            $layout = new Layout();
            $layout->setMetaDescription('Imprimir aqui é melhor e mas barato do que ter uma impressora! Não perca tempo comprando toner, nós imprimimos e entregamos.');
            $layout
                ->setLayout('layout-landing-page-campanha')
                ->addStylesheet('/css/landing-page-campanha.min.css')
                ->addStylesheet('/css/landing-page-impressora-online-barata.min.css')
                ->addScript('/js/impressora-online-barata.min.js')
                ->appendTitle('Impressora Barata', ' | ')
                ->render('landing-pages/impressora-online-barata/index');
        },
    ],
    [
        'pattern' => '/impressao-barata-entrega',
        'callback' => function () {
            $layout = new Layout();
            $layout->setMetaDescription('Imprimir aqui é melhor e mas barato do que ter uma impressora! Não perca tempo comprando toner, nós imprimimos e entregamos.');
            $layout
                ->setLayout('layout-landing-page-campanha')
                ->addStylesheet('/css/landing-page-campanha.min.css')
                ->addStylesheet('/css/landing-page-impressao-barata-entrega.min.css')
                ->addScript('/js/impressao-barata-entrega.min.js')
                ->appendTitle('Impressão Online', ' | ')
                ->render('landing-pages/impressao-barata-entrega/index');
        },
    ],
];
