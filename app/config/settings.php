<?php

return [
    'display_errors' => true,
    'db' => [
        'host' => getenv('DB_HOST'),
        'db_name' => getenv('DB_NAME'),
        'db_user' => getenv('DB_USER'),
        'db_pass' => getenv('DB_PASS'),
        'charset' => 'utf8mb4'
    ],
    'db_patch' => [
        'host' => 'localhost',
        'db_name' => 'deliveryprint',
        'db_user' => 'root',
        'db_pass' => 'root',
        'db_port' => null,
        'charset' => 'utf8mb4'
    ],
    'pheanstalk' => [
        'host' => getenv('BK_HOST'),
        'port' => 11300
    ],
    'mail' => [
        'tube_name' => 'delivery_email',
        'credentials' => [
            'smtp_server' => '',
            'smtp_port' => '',
            'ssl' => 'tls',
            'auth_email' => '',
            'auth_pass' => '',
        ],
        'message' => [
            'subject_prefix' => 'Delivery ',
            'default_from' => 'suporte@deliveryprint.com.br',
            'default_from_name' => 'DeliveryPrint',
            'default_bcc' => false,
        ],
        'enable_status' => [
            'canceled' => true,
            'production' => true,
            'dispatch' => true,
            'delivery' => true,
            'waiting_withdrawal' => false,
        ],
    ],
    'mailgun' => [
        'api_key' => '75cc8426e566709be997403258645554-a5d1a068-491483cd',
        'domain'  => 'sandboxedbb06c7326b4ea6b0c3edcc72f02e18.mailgun.org',
    ],
    'shipping' => [
        'from_zip' => '05409012',
        'express_zips' => [
            [
                1000000, 1099999
            ],
            [
                1200000, 1299999
            ],
            [
                1300000, 1399999
            ],
            [
                1400000, 1499999
            ],
            [
                1500000, 1599999
            ],
            [
                4000000, 4099999
            ],
            [
                4100000, 4199999
            ],
            [
                4200000, 4299999
            ],
            [
                4500000, 4599999
            ],
            [
                5000000, 5099999
            ],
            [
                5400000, 5499999
            ],
            [
                5600000, 5699999
            ],
        ],
        'express_delivery' => [
            'db_values' => true,
            'page_limits' => [
                'limit1' => 750,
                'limit2' => 2000,
                'limit3' => 5000,
                'limit4' => 10000,
                'limit5' => 10000
            ],
            'express_free_treshold' => 150,
            'express_free_enabled' => false,
            'express1_endtime' => [
                'hour' => 13,
                'minute' => 0
            ],
            'express1_price' => 39,
            'express1_delivery_time' => [
                'hour' => 18,
                'minute' => 0,
            ],
            'express2_price' => 35,
            'express2_delivery_time' => [
                'hour' => 12,
                'minute' => 0,
            ],
            'express3_price' => 22,
            'express3_delivery_time' => [
                'hour' => 18,
                'minute' => 0,
            ],
            'express4_price' => 18,
            'express4_additional_price' => 25,
            'express4_delivery_time' => [
                'hour' => 18,
                'minute' => 0,
            ],
            'express5_enabled' => false,
            'express5_endtime' => [
                'hour' => 11,
                'minute' => 0
            ],
            'express5_price' => 18,
            'express5_delivery_time' => [
                'hour' => 16,
                'minute' => 0,
            ],
            'sedex1_enabled' => true,
            'sedex1_additional_price' => 10,
            'sedex1_extra_days' => 0,
            'sedex1_delivery_time' => [
                'hour' => 18,
                'minute' => 0,
            ],
            'sedex2_enabled' => true,
            'sedex2_additional_price' => 5,
            'sedex2_extra_days' => 1,
            'sedex2_delivery_time' => [
                'hour' => 18,
                'minute' => 0,
            ],
            'pac1_enabled' => false,
            'pac1_additional_price' => 2,
            'pac1_extra_days' => 0,
            'pac1_delivery_time' => [
                'hour' => 18,
                'minute' => 0,
            ],
            'pac2_enabled' => false,
            'pac2_additional_price' => 0,
            'pac2_extra_days' => 1,
            'pac2_delivery_time' => [
                'hour' => 18,
                'minute' => 0,
            ],
        ],
        'express_withdrawal' => [
            'db_values' => true,
            'page_limits' => [
                'limit1' => 500,
                'limit2' => 3000,
                'limit3' => 1000,
            ],
            'express1_endtime' => [
                'hour' => 15,
                'minute' => 0
            ],
            // 'express1_price' => 35,
            'express1_price' => 0,
            'express1_withdrawal_time' => [
                'hour' => 17,
                'minute' => 0,
            ],
            // 'express2_price' => 25,
            'express2_price' => 0,
            'express2_withdrawal_time' => [
                'hour' => 12,
                'minute' => 0,
            ],
            // 'express2_price' => 25,
            'express3_price' => 0,
            'express3_withdrawal_time' => [
                'hour' => 12,
                'minute' => 0,
            ],
            // 'normal1_price' => 6,
            'normal1_price' => 0,
            'normal1_withdrawal_time' => [
                'hour' => 12,
                'minute' => 0,
            ],
            'normal2_price' => 0,
            'normal2_withdrawal_time' => [
                'hour' => 12,
                'minute' => 0,
            ],
        ]
    ],
    'pdf' => [],
    'payment' => [
        'iuguToken'     => 'e938bc56c6260175bf9ceeb6da145698',
        'iuguAccountId' => 'DA6FDE4427634FF09308B1EFB780BA9D',
        'iuguCardTest'  => true,
        'auth_trigger'  => '792470975DB5BA97EFDB342AAC71DAE63C244835FF4A7339E7444290BD797868'
    ],
    'notaFiscal' => [
        'empresaId' => '645DBC9B-A132-45A4-B71D-7A70E2F30300',
        'apiKey' => 'YTJkMDYxZjItZGY5My00ZjRjLTgwYmMtODAwZWI4ZTEwMzAw'
    ],
    'storage' => [
        'type' => 'local',
        'save' => '/pdfCount',
        'download' => '/download/',
        'download_all' => '/downloadAll/',
        'gCloudBucket' => 'deliveryprint-2',
        'gCloudFolder' => 'dev',
        'localFolder' => 'app/data/print'
    ],
    'jwt' => [
        'app_secret' => 'very_secret_much_woow',
        'token_expires' => 15552000 // 30 min
    ],
    'session' => [
        'cookie_name' => 'APPcf83e1357eefb8bdf1542',
        'cookie_expires' => 15552000 // 30 min
    ],
    'mailchimp' => [
        'token' => '69cf65458adafd4d32cd976c7136a599-us16',
        'lists' => [
            'test_local' => '1e9507f5e7',
            'sub_home'   => '34083336de'
        ]
    ],
    'sms' => [
        'totalVoiceToken' => '28f663183bd82511fe2b1008550e2f0b'
    ],
    'recaptcha' => [
        'use_captcha' => true
    ]
];
