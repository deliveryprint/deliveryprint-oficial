<?php

namespace IntecPhp;

use IntecPhp\View\Layout;

return [
    [
        'pattern' => '/impressao-apostila-enem',
        'callback' => function () {
            $layout = new Layout();
            $layout
                ->setCanonical('/impressao-apostila-enem')
                ->setMetaDescription('Peça sua Apostila do ENEM Impressa por um Preço baixo e receba de forma Ágil em sua casa. Estude melhor, faça simulados, teste seu conhecimento! Impressão de Qualidade por um preço baixo é na DeliveryPrint.')
                ->appendTitle('Apostila ENEM', ' | ')
                ->addStylesheet('/css/impressao-apostila.min.css')
                ->addScript('/js/impressao-apostila.min.js')
                ->addScript('/js/lp-product.min.js')
                ->render('produto/template-produto-lp', [
                    'title'     => 'Impressão de Apostila "Coletânea ENEM"', // = msg_e
                    'subtitle'  => 'Apostila ENEM', //msg_a
                    'breadc'    => 'Apostila ENEM Completa', //msg_g
                    'img_url'   => '/img/Apostila ENEM.jpg', //h
                    'alt'       => 'Apostila ENEM',
                    'subject'   => 'Impressão Apostila ENEM',
                    'li_es'     => [
                        'Apostila com Provas impressas do ENEM de 2009 até 2017', //b
                        'Faça simulados e melhore seu resultado', //c
                        'Entregamos em até 3 dias úteis' //d
                    ],
                    'button'    => 'PEDIR ORÇAMENTO RÁPIDO', //f
                    'text'      => 'Estudando para o ENEM e para vestibulares? Cansado de estudar direto da tela do computador? Imprima com a DeliveryPrint! Impressão de qualidade que cabe no seu bolso. Peça online, direto de casa, sem interromper seus estudos e receba até no mesmo dia! Entregas para todo o Brasil! Impressão em preto e branco e impressão colorida, com diversos tipos de acabamento e de encadernações. Materiais de estudo com qualidade e preço baixo! Imprimir online ficou fácil! Temos coletâneas prontas de provas do ENEM, Unicamp, Unesp e USP desde 2010! Não perca tempo e faça seu orçamento: gráfica digital online e impressão online é com a DeliveryPrint!'
                ]);
        },
    ],
    [
        'pattern' => '/impressao-simulados-vestibular',
        'callback' => function () {
            $layout = new Layout();
            $layout
                ->setCanonical('/impressao-simulados-vestibular')
                ->setMetaDescription('Peça sua Apostila com Provas dos maiores Vestibulares do Brasil Impressa por um Preço baixo e receba de forma Ágil em sua casa. Estude melhor, faça simulados, teste seu conhecimento! Impressão de Qualidade por um preço baixo é na DeliveryPrint.')
                ->appendTitle('Apostila Simulados Vestibular', ' | ')
                ->addStylesheet('/css/impressao-apostila.min.css')
                ->addScript('/js/impressao-apostila.min.js')
                ->addScript('/js/lp-product.min.js')
                ->render('produto/template-produto-lp', [
                    'title'     => 'Apostila de Provas da USP,UNESP e UNICAMP', //
                    'subtitle'  => 'Apostila de Simulados para Vestibular', //
                    'breadc'    => 'Apostila Simulados Vestibular', //
                    'img_url'   => '/img/Apostila ENEM.jpg',
                    'alt'       => 'Apostila Simulados',
                    'subject'   => 'Impressão Apostila Simulados',
                    'li_es'     => [
                        'Contém Provas de 2010 até 2017 para USP, UNESP e UNICAMP',
                        'Faça simulados e melhore seu resultado',
                        'Entregamos em até 3 dias úteis'
                    ],
                    'button'    => 'PEDIR ORÇAMENTO RÁPIDO', //f
                    'text'  => 'Compilamos todas as provas que você, estudante, precisa para mandar bem nos vestibulares da USP, UNESP e UNICAMP e alcançar seus sonhos com mais facilidade! Estudar para a reta final dos vestibulares nunca foi tão fácil: peça sua apostila de simulados de vestibular do conforto de sua casa, sem se preocupar em interromper seus estudos e se dirigir até a gráfica. Realizamos entregas até no mesmo dia! Peça seu orçamento! Impressão em preto e branco e impressão colorida com os menores preços do mercado. Imprima com a DeliveryPrint e se impressione! Seu serviço de impressão online e gráfica digital com nota máxima no Google! Alcance seus sonhos! Força na peruca!'
                ]);
        },
    ],
    [
        'pattern' => '/impressao-para-empresas',
        'callback' => function () {
            $layout = new Layout();
            $layout
                ->setCanonical('/impressao-para-empresas')
                ->setMetaDescription('Serviço de Impressão e Encadernação para Empresas, autônomos e Pessoa Jurídica. Redução de Custo, Entrega ágil, Alta qualidade de Impressão a Laser. Tudo isso fazendo pedidos Online, sem precisar se locomover, isso tudo apenas na DeliveryPrint.')
                ->appendTitle('Serviço para sua empresa', ' | ')
                ->addStylesheet('/css/impressao-apostila.min.css')
                ->addScript('/js/impressao-apostila.min.js')
                ->addScript('/js/lp-product.min.js')
                ->render('produto/template-produto-lp', [
                    'title'     => 'Impressão para Empresas', // = msg_e
                    'subtitle'  => 'Serviço de Impressão para Empresas', //msg_a
                    'breadc'    => 'Impressão para Empresas', //msg_g
                    'img_url'   => '/img/Impressão_de_Documentos.jpg', //h
                    'alt'       => 'Impressão para Empresas', //H
                    'subject'   => 'Contato B2B de Impressão', //assunto do email
                    'li_es'     => [
                        'Impressão em A3, A4, A5, Papel Carta, Ofício e mais', //b
                        'Impressão, Encadernação, Grampear e Entrega', //c
                        'Redução de seus Custos e aumento na Qualidade' //d
                    ],
                    'button'    => 'PEDIR CONTATO RÁPIDO', //f
                    'text'  => 'Sua empresa não precisa mais passar sufoco com gráficas de péssima qualidade ou impressoras emperrando! Na DeliveryPrint, sua Gráfica Digital Online, a qualidade de impressão é praxe, o preço baixo é costume e a entrega rápida é necessidade! Impressão em preto e branco ou colorida, com alta resolução, em diferentes tipos e tamanhos de papel, com atendimento personalizado e opções diferenciadas para empresas e pessoas jurídicas, entrega expressa e sigilosidade garantida de seus materiais. Não perca tempo! Imprima com qualidade diferenciada com a DeliveryPrint, o serviço de impressão online com Nota Máxima no Google Avaliações!'
                ]);
        },
    ],
    [
        'pattern' => '/impressao-simulado-unicamp',
        'callback' => function () {
            $layout = new Layout();
            $layout
                ->setCanonical('/impressao-simulado-unicamp')
                ->setMetaDescription('Peça sua Apostila com Simulados da UNICAMP Impressa por um Preço baixo e receba de forma Ágil em sua casa. Estude melhor, faça simulados, teste seu conhecimento! Impressão de Qualidade por um preço baixo é na DeliveryPrint.')
                ->appendTitle('Apostila UNICAMP', ' | ')
                ->addStylesheet('/css/impressao-apostila.min.css')
                ->addScript('/js/impressao-apostila.min.js')
                ->addScript('/js/lp-product.min.js')
                ->render('produto/template-produto-lp', [
                    'title'     => 'Impressão de Apostila "Simulados UNICAMP"', // = msg_e
                    'subtitle'  => 'Apostila de Simulados UNICAMP', //msg_a
                    'breadc'    => 'Simulados UNICAMP Completo', //msg_g
                    'img_url'   => '/img/Apostila ENEM.jpg', //h
                    'alt'       => 'Simulados UNICAMP', //H
                    'subject'   => 'Impressão Apostila UNICAMP', //assunto do email
                    'li_es'     => [
                        'Apostila com Provas impressas da UNICAMP de 2010 até 2018', //b
                        'Faça simulados e melhore seu resultado', //c
                        'Entregamos em até 3 dias úteis' //d
                    ],
                    'button'    => 'PEDIR ORÇAMENTO RÁPIDO', //f
                    'text'      => 'Não pare de estudar para ir até a gráfica! Peça online com a DeliveryPrint, o seu serviço de impressão online e continue estudando enquanto nós fazemos o trabalho para você! Receba até no mesmo dia! Entregas para todo o Brasil! Apostilas e compilados dos maiores vestibulares do Brasil! Simulados e apostilas da Unicamp desde 2010, já prontas, impressos em alta qualidade, encadernados e com preço baixo! Impressão colorida e impressão em preto e branco, diversos tipos de acabamento, de papéis e de tamanhos, para o seu material de estudo ficar do jeito que você precisa. Gráfica Digital Online é com a DeliveryPrint: te ajudando a conquistar os seus sonhos!'
                ]);
        },
    ],
    [
        'pattern' => '/impressao-simulados-fuvest',
        'callback' => function () {
            $layout = new Layout();
            $layout
                ->setCanonical('/impressao-simulados-fuvest')
                ->setMetaDescription('Peça sua Apostila de Simulados FUVEST Impressa por um Preço baixo e receba de forma Ágil em sua casa. Estude melhor, faça simulados, teste seu conhecimento! Impressão de Qualidade por um preço baixo é na DeliveryPrint.')
                ->appendTitle('Apostila FUVEST', ' | ')
                ->addStylesheet('/css/impressao-apostila.min.css')
                ->addScript('/js/impressao-apostila.min.js')
                ->addScript('/js/lp-product.min.js')
                ->render('produto/template-produto-lp', [
                    'title'     => 'Impressão de Apostila "Simulados FUVEST"', // = msg_e
                    'subtitle'  => 'Apostila de Simulados FUVEST', //msg_a
                    'breadc'    => 'Apostila Simulados FUVEST', //msg_g
                    'img_url'   => '/img/Apostila ENEM.jpg', //h
                    'alt'       => 'Simulados FUVEST', //H
                    'subject'   => 'Impressão Apostila FUVEST', //assunto do email
                    'li_es'     => [
                        'Apostila com Provas impressas da FUVEST de 2010 até 2018', //b
                        'Faça simulados e melhore seu resultado', //c
                        'Entregamos em até 3 dias úteis' //d
                    ],
                    'button'    => 'PEDIR ORÇAMENTO RÁPIDO', //f
                    'text'  => 'Compilamos todas as provas que você, estudante, precisa para mandar bem no vestibular da FUVEST, o mais competitivo do Brasil, e alcançar seus sonhos com mais facilidade! Estudar para a reta final dos vestibulares nunca foi tão fácil: peça sua apostila de simulados de vestibular do conforto de sua casa, sem se preocupar em interromper seus estudos e se dirigir até a gráfica. Realizamos entregas até no mesmo dia! Peça seu orçamento! Impressão em preto e branco e impressão colorida com os menores preços do mercado. Imprima com a DeliveryPrint e se impressione! Seu serviço de impressão online e gráfica digital com nota máxima no Google! Alcance seus sonhos! Força na peruca!'
                ]);
        },
    ],
    [
        'pattern' => '/impressao-simulado-unesp',
        'callback' => function () {
            $layout = new Layout();
            $layout
                ->setCanonical('/impressao-simulado-unesp')
                ->setMetaDescription('Peça sua Apostila de Simulados UNESP Impressa por um Preço baixo e receba de forma Ágil em sua casa. Estude melhor, faça simulados, teste seu conhecimento! Impressão de Qualidade por um preço baixo é na DeliveryPrint.')
                ->appendTitle('Apostila UNESP', ' | ')
                ->addStylesheet('/css/impressao-apostila.min.css')
                ->addScript('/js/impressao-apostila.min.js')
                ->addScript('/js/lp-product.min.js')
                ->render('produto/template-produto-lp', [
                    'title'     => 'Impressão de Apostila "Simulados UNESP"', // = msg_e
                    'subtitle'  => 'Apostila de Simulados UNESP', //msg_a
                    'breadc'    => 'Apostila Simulados UNESP', //msg_g
                    'img_url'   => '/img/Apostila ENEM.jpg', //h
                    'alt'       => 'Simulados UNESP', //H
                    'subject'   => 'Impressão Apostila UNESP', //assunto do email
                    'li_es'     => [
                        'Apostila com Provas impressas da UNESP de 2010 até 2018', //b
                        'Faça simulados e melhore seu resultado', //c
                        'Entregamos em até 3 dias úteis' //d
                    ],
                    'button'    => 'PEDIR ORÇAMENTO RÁPIDO', //f
                    'text'  => 'Compilamos todas as provas que você, estudante, precisa para mandar bem no vestibular da UNESP e alcançar seus sonhos com mais facilidade! Estudar para a reta final dos vestibulares nunca foi tão fácil: peça sua apostila de simulados de vestibular do conforto de sua casa, sem se preocupar em interromper seus estudos e se dirigir até a gráfica. Realizamos entregas até no mesmo dia! Peça seu orçamento! Impressão em preto e branco e impressão colorida com os menores preços do mercado. Imprima com a DeliveryPrint e se impressione! Seu serviço de impressão online e gráfica digital com nota máxima no Google! Alcance seus sonhos! Força na peruca!'
                ]);
        },
    ],
    [
        'pattern' => '/impressao-papel-timbrado',
        'callback' => function () {
            $layout = new Layout();
            $layout
                ->setCanonical('/impressao-papel-timbrado')
                ->setMetaDescription('Imprima Papel Timbrado na DeliveryPrint. Preço baixo com Alta qualidade e Entrega ágil, basta pedir em nosso site em poucos minutos! DeliveryPrint é um serviço de Impressão completo para todos os usos.')
                ->appendTitle('Papel Timbrado', ' | ')
                ->addStylesheet('/css/impressao-apostila.min.css')
                ->addScript('/js/impressao-apostila.min.js')
                ->addScript('/js/lp-product.min.js')
                ->render('produto/template-produto-lp', [
                    'title'     => 'Orçamento de Papel Timbrado', // = msg_e
                    'subtitle'  => 'Impressão de Papel Timbrado', //msg_a
                    'breadc'    => 'Papel Timbrado', //msg_g
                    'img_url'   => '/img/Impressão Barata.jpg', //h
                    'alt'       => 'Papel Timbrado', //H
                    'subject'   => 'Orçamento para Impressão Papel Timbrado', //assunto do email
                    'li_es'     => [
                        'Preço Baixo e Entrega Rápida', //b
                        'Impressão de Qualidade', //c
                    ],
                    'button'    => 'PEDIR ORÇAMENTO RÁPIDO', //f
                    'text'  => 'Imprima os documentos oficiais de sua empresa com a DeliveryPrint, a sua gráfica digital online! Preço baixo e impressão colorida ou impressão em preto e branco com qualidade garantida! Imprimir papel timbrado nunca foi tão fácil! Trabalhamos com diferentes tipos de papel e com condições diferenciadas para empresas: entrega expressa e preço baixo! Não deixe de fazer seu orçamento, se tiver dúvidas, entre em contato conosco via e-mail ou Whatsapp. Pensou em imprimir colorido? Pensou em imprimir em preto e branco? Pensou em qualidade e preço baixo? Pensou na DeliveryPrint, o serviço de impressão online com a melhor avaliação no Google!'
                ]);
        },
    ],
    [
        'pattern' => '/impressao-livreto',
        'callback' => function () {
            $layout = new Layout();
            $layout
                ->setCanonical('/impressao-livreto')
                ->setMetaDescription('Faça Impressão em Livreto, economize sem perder a qualidade, Impressos por um Preço baixo com entrega de forma Ágil em sua casa. Livreto e outros tipos de Impressão de Qualidade é na DeliveryPrint.')
                ->appendTitle('Impressão Livreto', ' | ')
                ->addStylesheet('/css/impressao-apostila.min.css')
                ->addScript('/js/impressao-apostila.min.js')
                ->addScript('/js/lp-product.min.js')
                ->render('produto/template-produto-lp', [
                    'title'     => 'Orçamento de Impressão em Livreto', // = msg_e
                    'subtitle'  => 'Impressão em Livreto', //msg_a
                    'breadc'    => 'Impressão Livreto', //msg_g
                    'img_url'   => '/img/Impressão Livreto.jpg', //h
                    'alt'       => 'Impressão Livreto', //H
                    'subject'   => 'Orçamento de Impressão em Livreto', //assunto do email
                    'li_es'     => [
                        'Economia no valor da Impressão sem perder a Qualidade', //b
                        'Orçamento Rápido com Desconto no 1º Pedido', //c
                        'Entregamos em até 3 dias úteis*' //d
                    ],
                    'button'    => 'PEDIR ORÇAMENTO RÁPIDO', //f
                    'text'  => 'Ideal para materiais de estudo, treinamento e uso pessoal, a impressão em livreto é indicada para redução nos custos de impressão, sem perda de qualidade, com um acabamento diferenciado de acordo com o gosto do cliente. Trabalhamos com diversos tipos de papel e de acabamentos, encadernações em espiral, wire-o ou customizadas de acordo com sua necessidade. Seja impressão colorida ou impressão em preto e branco, garantimos o preço baixo e a qualidade comprovada pelos nossos clientes. Imprimir online nunca foi tão fácil! Peça seu orçamento e conheça a DeliveryPrint, a sua Gráfica Digital Online com nota máxima no Google. Pensou em impressão online, pensou DeliveryPrint!'
                ]);
        },
    ],
    [
        'pattern' => '/impressao-avaliacoes',
        'callback' => function () {
            $layout = new Layout();
            $layout
                ->setCanonical('/impressao-avaliacoes')
                ->setMetaDescription('Impressão de Avaliações, Simulados e muito mais com preço baixo e Entrega Inclusa é só na DeliveryPrint. Somos um serviço de Impressão Profissional de Alta qualidade e Agilidade. Peça por nosso site, é rápido, simples e online.')
                ->appendTitle('Impressão de Avaliações', ' | ')
                ->addStylesheet('/css/impressao-apostila.min.css')
                ->addScript('/js/impressao-apostila.min.js')
                ->addScript('/js/lp-product.min.js')
                ->render('produto/template-produto-lp', [
                    'title'     => 'Orçamento de Impressão de Avaliações e Material de Estudo', // = msg_e
                    'subtitle'  => 'Impressão de Avaliações', //msg_a
                    'breadc'    => 'Impressão de avaliações', //msg_g
                    'img_url'   => '/img/Dados Variaveis.jpg', //h
                    'alt'       => 'Avaliações', //H
                    'subject'   => 'Impressão de Avaliações', //assunto do email
                    'li_es'     => [
                        'Impressão de Alta qualidade por um preço baixo', //b
                        'Impressão e Entrega', //c
                        'Atendimento Personalizado' //d
                    ],
                    'button'    => 'PEDIR ORÇAMENTO RÁPIDO', //f
                    'text'  => ''
                ]);
        },
    ],
    [
        'pattern' => '/impressao-apostila-concursos',
        'callback' => function () {
            $layout = new Layout();
            $layout
                ->setCanonical('/impressao-apostila-concursos')
                ->setMetaDescription('Impressão de apostila, material de estudo e muito mais para estudar para Concursos com preço baixo e Entrega Inclusa é só na DeliveryPrint. Somos um serviço de Impressão Profissional de Alta qualidade e Agilidade. Peça por nosso site, é rápido, simples e online.')
                ->appendTitle('Impressão de Apostilas Concursos', ' | ')
                ->addStylesheet('/css/impressao-apostila.min.css')
                ->addScript('/js/impressao-apostila.min.js')
                ->addScript('/js/lp-product.min.js')
                ->render('produto/template-produto-lp', [
                    'title'     => 'Orçamento de Impressão de Apostilas para Concursos', // = msg_e
                    'subtitle'  => 'Impressão de Apostila para Concursos', //msg_a
                    'breadc'    => 'Impressão de Apostila para Concursos', //msg_g
                    'img_url'   => '/img/Dados Variaveis.jpg', //h
                    'alt'       => 'Concursos', //H
                    'subject'   => 'Impressão de Apostila Concursos', //assunto do email
                    'li_es'     => [
                        'Impressão de Alta qualidade por um preço baixo', //b
                        'Impressão e Entrega', //c
                        'Atendimento Personalizado' //d
                    ],
                    'button'    => 'PEDIR ORÇAMENTO RÁPIDO', //f
                    'text'  => ''
                ]);
        },
    ],
    //landing page plottagem
    [
        'pattern' => '/orcamento-plotagem',
        'callback' => function () {
            $layout = new Layout();
            $layout
                ->setCanonical('/orcamento-plotagem')
                ->appendTitle('Orçamento de Plotagem')
                ->addScript('js/lp-plottagem.min.js')
                ->addStylesheet('/css/impressao-apostila.min.css')
                ->render('produto/template-lp-plot1', [
                    'img_url'   => '/img/Imagem Impressora.jpg',
                    'subject' => 'Orçamento de Plotagem',
                    'text'  => ''
                ]);
        },
    ],
    [
        'pattern' => '/plotagem-01',
        'callback' => function () {
            $layout = new Layout();
            $layout
                ->setCanonical('/plotagem-01')
                ->appendTitle('Orçamento de Plotagem')
                ->addScript('js/lp-plottagem.min.js')
                ->addStylesheet('/css/impressao-apostila.min.css')
                ->render('produto/template-lp-plot2', [
                    'img_url'   => '/img/Planta Arquitetura.jpg',
                    'subject' => 'Orçamento de Plotagem',
                    'li_es'     => [
                        'Orçamento respondido em Instantes',
                        'Fechamento do Pedido totalmente Online',
                        'Entregamos onde você quiser',
                        'Atendimento Personalizado'
                    ],
                    'text'  => ''
                ]);
        },
    ],
    [
        'pattern' => '/orcamento-plotagem-01',
        'callback' => function () {
            $layout = new Layout();
            $layout
                ->setCanonical('/orcamento-plotagem-01')
                ->appendTitle('Orçamento de Plotagem')
                ->addScript('js/lp-plottagem.min.js')
                ->addStylesheet('/css/impressao-apostila.min.css')
                ->render('produto/template-lp-plot2', [
                    'img_url'   => '/img/Imagem Impressora.jpg',
                    'subject' => 'Orçamento de Plotagem',
                    'li_es'     => [
                        'Orçamento respondido em Instantes',
                        'Fechamento do Pedido totalmente Online',
                        'Entregamos onde você quiser',
                        'Atendimento Personalizado'
                    ],
                    'text'  => ''
                ]);
        },
    ],
    [
        'pattern' => '/impressao-papel-verge',
        'callback' => function () {
            $layout = new Layout();
            $layout
                ->setLayout('layout')
                ->setMetaDescription('Serviço de impressão em Papel verge e muitos outros tipos de papel nos tamanhos A3, A4 e A5. A DeliveryPrint é mais que uma Copiadora Online, somos a Impressora do Futuro que entrega para todo Brasil.')
                ->setCanonical('/impressao-papel-verge')
                ->appendTitle('Impressão em Papel Vergê', ' | ')
                ->addScript('js/lp-email.min.js')
                ->render('produto/lp-papel-verge');
        },
    ],
    [
        'pattern' => '/plastificacao',
        'callback' => function () {
            $layout = new Layout();
            $layout
                ->setLayout('layout')
                ->setMetaDescription('Serviço de impressão, Plastificação e Encadernação nos tamanhos A3, A4 e A5. A DeliveryPrint é mais que uma Copiadora Online, somos a Impressora do Futuro que entrega para todo Brasil.')
                ->setCanonical('/plastificacao')
                ->appendTitle('Impressão e Plastificação', ' | ')
                ->addScript('js/lp-email.min.js')
                ->render('produto/lp-plastificacao');
        },
    ],
];
