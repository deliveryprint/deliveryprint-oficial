<?php

namespace IntecPhp;

use IntecPhp\View\Layout;

return [
    [
        'pattern' => '/adm/pedidos',
        'middlewares' => [
            Middleware\AuthenticationMiddleware::class . ':isAdminAuthenticated',
        ],
        'callback' => function () {
            $layout = new Layout();
            $layout
                ->setMetaRobots('noindex')
                ->setLayout('layout-admin')
                ->addStylesheet('/css/la-requests.min.css')
                ->addStylesheet('/css/la-requests-track-modal.min.css')
                ->addScript('/js/la-request.min.js')
                ->render('la/requests');
        },
    ],
    [
        'pattern' => '/adm/cupom',
        'middlewares' => [
            Middleware\AuthenticationMiddleware::class . ':isAdminAuthenticated',
        ],
        'callback' => function () {
            $layout = new Layout();
            $layout
                ->setMetaRobots('noindex')
                ->setLayout('layout-admin')
                ->addStylesheet('/css/la-coupon.min.css')
                ->addScript('/js/la-coupon.min.js')
                ->render('la/coupon');
        },
    ],
    [
        'pattern' => '/adm/login',
        'callback' => function () {
            $layout = new Layout();
            $layout
                ->setMetaRobots('noindex')
                ->setLayout('layout-login')
                ->addStylesheet('/css/la-login.min.css')
                ->render('la/login');
        },
    ],
    [
        'pattern' => '/adm/faixa-precos',
        'middlewares' => [
            Middleware\AuthenticationMiddleware::class . ':isAdminAuthenticated',
        ],
        'callback' => function () {
            $layout = new Layout();
            $layout
                ->setMetaRobots('noindex')
                ->setLayout('layout-admin')
                ->addStylesheet('/css/la-prices.min.css')
                ->addScript('/js/la-prices.min.js')
                ->render('la/prices');
        },
    ],
    [
        'pattern' => '/adm/usuarios',
        'middlewares' => [
            Middleware\AuthenticationMiddleware::class . ':isAdminAuthenticated',
        ],
        'callback' => function () {
            $layout = new Layout();
            $layout
                ->setMetaRobots('noindex')
                ->setLayout('layout-admin')
                ->addStylesheet('/css/la-users.min.css')
                ->addScript('/js/la-users.min.js')
                ->render('la/users');
        },
    ],
    [
        'pattern' => '/adm/relatorios',
        'middlewares' => [
            Middleware\AuthenticationMiddleware::class . ':isAdminAuthenticated',
        ],
        'callback' => function () {
            $layout = new Layout();
            $layout
                ->setLayout('layout-admin')
                ->addStylesheet('/css/la-reports.min.css')
                ->addScript('/js/la-reports.min.js')
                ->render('la/reports');

        },
    ],
    [
        'pattern' => '/adm/sms',
        'middlewares' => [
            Middleware\AuthenticationMiddleware::class . ':isAdminAuthenticated',
        ],
        'callback' => function () {
            $layout = new Layout();
            $layout
                ->setLayout('layout-admin')
                ->addScript('/js/la-sms.min.js')
                ->addStylesheet('/css/la-sms.min.css')
                ->render('la/sms');
        },
    ],
    [
        'pattern' => '/adm/atualizar-custos',
        'middlewares' => [
            Middleware\AuthenticationMiddleware::class . ':isAdminAuthenticated',
        ],
        'callback' => function () {
            $layout = new Layout();
            $layout
                ->setLayout('layout-admin')
                ->addScript('/js/la-custos.min.js')
                ->addStylesheet('/css/la-custos.min.css')
                ->render('la/custos');
        },
    ],
    [
        'pattern' => '/adm/modalemails',
        'middlewares' => [
            Middleware\AuthenticationMiddleware::class . ':isAdminAuthenticated',
        ],
        'callback' => function () {
            $layout = new Layout();
            $layout
                ->setMetaRobots('noindex')
                ->setLayout('layout-admin')
                ->addStylesheet('/css/la-modal-emails.min.css')
                ->addScript('/js/la-modal-emails.min.js')
                ->render('la/modal-emails');
        },
    ],
    [
        'pattern' => '/adm/configuracoes',
        'middlewares' => [
            Middleware\AuthenticationMiddleware::class . ':isAdminAuthenticated',
        ],
        'callback' => function () {
            $layout = new Layout();
            $layout
                ->setMetaRobots('noindex')
                ->setLayout('layout-admin')
                ->addStylesheet('/css/la-configuracoes.min.css')
                ->addScript('/js/la-configuracoes.min.js')
                ->render('la/configuracoes');
        },
    ],
    [
        'pattern' => '/adm/entregas-rapidas',
        'middlewares' => [
            Middleware\AuthenticationMiddleware::class . ':isAdminAuthenticated',
        ],
        'callback' => function () {
            $layout = new Layout();
            $layout
                ->setMetaRobots('noindex')
                ->setLayout('layout-admin')
                ->addStylesheet('/css/la-configuracoes.min.css')
                ->addScript('/js/la-entregas-rapidas.min.js')
                ->render('la/entregas-rapidas');
        },
    ],
    // [
    //     'pattern' => '/adm/exibir-relatorio',
    //     'middlewares' => [
    //         Middleware\AuthenticationMiddleware::class . ':isAdminAuthenticated',
    //     ],
    //     'callback' => function () {
    //         $layout = new Layout();
    //         $layout
    //             ->setLayout('layout-admin')
    //             ->addStylesheet('/css/la-reports.min.css')
    //             ->addScript('/js/view-report.min.js')
    //             ->render('la/view-report');
    //     },
    // ],
];
