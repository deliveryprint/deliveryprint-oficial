<?php

use Pimple\Container as PimpleContainer;

$dependencies = new PimpleContainer();
$dependencies['settings'] = $settings;

use Tx\Mailer;
use Pheanstalk\Pheanstalk;

// Entity
use IntecPhp\Entity\TbPedido;
use IntecPhp\Entity\TbCliente;
use IntecPhp\Entity\TbFrete;
use IntecPhp\Entity\TbEntrega;
use IntecPhp\Entity\TbEndereco;
use IntecPhp\Entity\TbItem;
use IntecPhp\Entity\TbArquivo;
use IntecPhp\Entity\TbCor;
use IntecPhp\Entity\TbLado;
use IntecPhp\Entity\TbPapel;
use IntecPhp\Entity\TbAcabamento;
use IntecPhp\Entity\TbFaixa;
use IntecPhp\Entity\TbFeriado;
use IntecPhp\Entity\TbRetirada;
use IntecPhp\Entity\TbCupomGeral;
use IntecPhp\Entity\TbCupom;
use IntecPhp\Entity\TbColaborador;
use IntecPhp\Entity\PasswordRecovery as PasswordRecoveryEntity;
use IntecPhp\Entity\CsvEntity;
use IntecPhp\Entity\OrderReportEntity;
use IntecPhp\Entity\SmsEntity;
use IntecPhp\Entity\PrintCostEntity;
use IntecPhp\Entity\FinishCostEntity;
use IntecPhp\Entity\TbEmailMkt;
use IntecPhp\Entity\TbSettings;
use IntecPhp\Entity\TbValorFrete;
use IntecPhp\Entity\TbIp;
use IntecPhp\Entity\TbDetalhesAcabamento;
use IntecPhp\Entity\TbEmpresa;
use IntecPhp\Entity\TbShippingPrices;

// Model
use IntecPhp\Model\Account;
use IntecPhp\Model\DbHandler;
use IntecPhp\Model\User;
use IntecPhp\Model\ShippingSpecs;
use IntecPhp\Model\Order;
use IntecPhp\Model\OrderPartial;
use IntecPhp\Model\PDF;
use IntecPhp\Model\Customer;
use IntecPhp\Model\CustomerAddress;
use IntecPhp\Model\Payment;
use IntecPhp\Model\Shipping;
use IntecPhp\Model\Withdrawal;
use IntecPhp\Model\BusinessDay;
use IntecPhp\Model\PriceCalculator;
use IntecPhp\Model\Coupon;
use IntecPhp\Model\ShoppingCart;
use IntecPhp\Model\Item;
use IntecPhp\Model\NF;
use IntecPhp\Model\PasswordRecovery;
use IntecPhp\Model\Csv;
use IntecPhp\Model\SalesReport;
use IntecPhp\Model\Sms;
use IntecPhp\Model\Costs;
use IntecPhp\Model\EmailMkt;
use IntecPhp\Model\Ip;
use IntecPhp\Model\Invitation;
use IntecPhp\Model\Company;
use IntecPhp\Model\ShippingPrices;

// Service
use IntecPhp\Service\Shipping as ShippingCorreios;
use IntecPhp\Service\AddressFinder;
use IntecPhp\Service\PaymentService;
use IntecPhp\Service\NFService;
use IntecPhp\Service\JwtWrapper;
use IntecPhp\Service\Cookie;
use IntecPhp\Service\AuthAccount;
use IntecPhp\Service\MailChimpService;
use TotalVoice\Client as TotalVoiceClient;
use IntecPhp\Service\SmsService;
use IntecPhp\Service\VerifyEmail;

// Worker
use IntecPhp\Worker\EmailWorker;
use IntecPhp\Worker\MailgunWorker;
use IntecPhp\Worker\RemoveFilesWorker;
use IntecPhp\Worker\RemoveFilesNotRegisteredWorker;
use IntecPhp\Worker\NotifyPendingPaymentWorker;

// Controller
use IntecPhp\Controller\UserController;
use IntecPhp\Controller\ShippingController;
use IntecPhp\Controller\WithdrawalController;
use IntecPhp\Controller\LoginController;
use IntecPhp\Controller\OrderController;
use IntecPhp\Controller\PartialOrderController;
use IntecPhp\Controller\PDFController;
use IntecPhp\Controller\CustomerController;
use IntecPhp\Controller\PaymentController;
use IntecPhp\Controller\CouponController;
use IntecPhp\Controller\ShoppingCartController;
use IntecPhp\Controller\ItemController;
use IntecPhp\Controller\NFController;
use IntecPhp\Controller\CouponManagerController;
use IntecPhp\Controller\BudgetController;
use IntecPhp\Controller\MailChimpController;
use IntecPhp\Controller\CsvController;
use IntecPhp\Controller\SalesReportController;
use IntecPhp\Controller\SmsController;
use IntecPhp\Controller\CostsController;
use IntecPhp\Controller\EmailMktController;
use IntecPhp\Controller\SettingsController;
use IntecPhp\Controller\ContactController;
use IntecPhp\Controller\PricesController;
use IntecPhp\Controller\InvitationController;
use IntecPhp\Controller\CompanyController;
use IntecPhp\Controller\ShippingPricesController;

// Middleware
use IntecPhp\Middleware\AuthenticationMiddleware;
use IntecPhp\Middleware\HttpMiddleware;
use IntecPhp\Middleware\IuguTriggerMiddleware;

// View
use IntecPhp\View\Layout;

// ----------------------  Base
$dependencies[PDO::class] = function ($c) {
    $db = $c['settings']['db'];

    return new PDO(
        'mysql:host=' . $db['host'] . ';dbname=' . $db['db_name'] . ';charset=' . $db['charset'],
        $db['db_user'],
        $db['db_pass'],
        [
            PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
            PDO::ATTR_PERSISTENT => false,
            PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC
        ]
    );
};

$dependencies[Mailer::class] = $dependencies->factory(function ($c) {
    $mail = $c['settings']['mail']['credentials'];
    $txMailer = new Mailer();
    $txMailer
        ->setServer($mail['smtp_server'], $mail['smtp_port'], $mail['ssl'])
        ->setAuth($mail['auth_email'], $mail['auth_pass']);

    return $txMailer;
});

$dependencies[Pheanstalk::class] = function ($c) {
    $settings = $c['settings']['pheanstalk'];
    return new Pheanstalk($settings['host'], $settings['port']);
};
// ----------------------  /Base

// ----------------------  Entities
$dependencies[TbPedido::class] = function ($c) {
    $tbPedido = $c[DbHandler::class];
    return new TbPedido($tbPedido);
};

$dependencies[TbCliente::class] = function ($c) {
    $db = $c[DbHandler::class];
    return new TbCliente($db);
};

$dependencies[TbFrete::class] = function ($c) {
    $dbHandler = $c[DbHandler::class];
    return new TbFrete($dbHandler);
};

$dependencies[TbEntrega::class] = function ($c) {
    $dbHandler = $c[DbHandler::class];
    return new TbEntrega($dbHandler);
};

$dependencies[TbEndereco::class] = function ($c) {
    $dbHandler = $c[DbHandler::class];
    return new TbEndereco($dbHandler);
};

$dependencies[TbItem::class] = function ($c) {
    $dbHandler = $c[DbHandler::class];
    return new TbItem($dbHandler);
};

$dependencies[TbArquivo::class] = function ($c) {
    $dbHandler = $c[DbHandler::class];
    return new TbArquivo($dbHandler);
};

$dependencies[TbCor::class] = function ($c) {
    $db = $c[DbHandler::class];
    return new TbCor($db);
};
$dependencies[TbLado::class] = function ($c) {
    $db = $c[DbHandler::class];
    return new TbLado($db);
};
$dependencies[TbPapel::class] = function ($c) {
    $db = $c[DbHandler::class];
    return new TbPapel($db);
};

$dependencies[TbAcabamento::class] = function ($c) {
    $db = $c[DbHandler::class];
    return new TbAcabamento($db);
};

$dependencies[TbFaixa::class] = function ($c) {
    $db = $c[DbHandler::class];
    return new TbFaixa($db);
};

$dependencies[TbFeriado::class] = function ($c) {
    $db = $c[DbHandler::class];
    return new TbFeriado($db);
};

$dependencies[TbRetirada::class] = function ($c) {
    $db = $c[DbHandler::class];
    return new TbRetirada($db);
};
$dependencies[TbCupomGeral::class] = function ($c) {
    $db = $c[DbHandler::class];
    return new TbCupomGeral($db);
};

$dependencies[TbCupom::class] = function ($c) {
    $db = $c[DbHandler::class];
    return new TbCupom($db);
};

$dependencies[TbColaborador::class] = function ($c) {
    $db = $c[DbHandler::class];
    return new TbColaborador($db);
};

$dependencies[PasswordRecoveryEntity::class] = function ($c) {
    $db = $c[DbHandler::class];
    return new PasswordRecoveryEntity($db);
};

$dependencies[CsvEntity::class] = function ($c) {
    $db = $c[DbHandler::class];
    return new CsvEntity($db);
};

$dependencies[OrderReportEntity::class] = function ($c) {
    $db = $c[DbHandler::class];
    return new OrderReportEntity($db);
};

$dependencies[SmsEntity::class] = function ($c) {
    $db = $c[DbHandler::class];
    return new SmsEntity($db);
};

$dependencies[PrintCostEntity::class] = function ($c) {
    $db = $c[DbHandler::class];
    return new PrintCostEntity($db);
};

$dependencies[FinishCostEntity::class] = function ($c) {
    $db = $c[DbHandler::class];
    return new FinishCostEntity($db);
};

$dependencies[TbEmailMkt::class] = function ($c) {
    $db = $c[DbHandler::class];
    return new TbEmailMkt($db);
};

$dependencies[TbSettings::class] = function ($c) {
    $db = $c[DbHandler::class];
    return new TbSettings($db);
};

$dependencies[TbValorFrete::class] = function ($c) {
    $db = $c[DbHandler::class];
    return new TbValorFrete($db);
};

$dependencies[TbIp::class] = function ($c) {
    $db = $c[DbHandler::class];
    return new TbIp($db);
};

$dependencies[TbDetalhesAcabamento::class] = function ($c) {
    $db = $c[DbHandler::class];
    return new TbDetalhesAcabamento($db);
};

$dependencies[TbEmpresa::class] = function ($c) {
    $db = $c[DbHandler::class];
    return new TbEmpresa($db);
};

$dependencies[TbShippingPrices::class] = function ($c) {
    $db = $c[DbHandler::class];
    return new TbShippingPrices($db);
};
// ----------------------  /Entities

// ----------------------  Models
$dependencies[DbHandler::class] = function ($c) {
    $pdo = $c[PDO::class];
    return new DbHandler($pdo);
};

$dependencies[User::class] = function ($c) {
    $userE = $c[TbPedido::class];
    return new User($userE);
};

$dependencies[ShippingSpecs::class] = function ($c) {
    $tbPapel = $c[TbPapel::class];
    return new ShippingSpecs($tbPapel);
};

$dependencies[BusinessDay::class] = function ($c) {
    $holiday = $c[TbFeriado::class];
    return new BusinessDay($holiday);
};

$dependencies[Withdrawal::class] = function ($c) {
    $tbAcabamento = $c[TbAcabamento::class];
    $bday = $c[BusinessDay::class];
    $withdrawalConfig = $c['settings']['shipping']['express_withdrawal'];
    $tbSettings = $c[TbSettings::class];
    $shippingPrices = $c[ShippingPrices::class];
    return new Withdrawal($bday, $tbAcabamento, $withdrawalConfig, $tbSettings, $shippingPrices);
};

$dependencies[Shipping::class] = function ($c) {
    $shippingCorreios = $c[ShippingCorreios::class];
    $expressZips = $c['settings']['shipping']['express_zips'];
    $expressDelivery = $c['settings']['shipping']['express_delivery'];
    $tbAcabamento = $c[TbAcabamento::class];
    $bday = $c[BusinessDay::class];
    $specs = $c[ShippingSpecs::class];
    $tbSettings = $c[TbSettings::class];
    $tbValorFrete = $c[TbValorFrete::class];
    $shippingPrices = $c[ShippingPrices::class];
    return new Shipping(
        $specs,
        $shippingCorreios,
        $expressZips,
        $expressDelivery,
        $bday,
        $tbAcabamento,
        $tbSettings,
        $tbValorFrete,
        $shippingPrices
    );
};

$dependencies[PDF::class] = function ($c) {
    $storage = $c['settings']['storage'];
    return new PDF(
        $storage
    );
};

$dependencies[Coupon::class] = function ($c) {
    $tbCupom = $c[TbCupom::class];
    $tbCliente = $c[TbCliente::class];
    return new Coupon($tbCupom, $tbCliente);
};

$dependencies[Order::class] = function ($c) {
    $tbEntrega       = $c[TbEntrega::class];
    $tbRetirada      = $c[TbRetirada::class];
    $tbFrete         = $c[TbFrete::class];
    $tbEndereco      = $c[TbEndereco::class];
    $tbPedido        = $c[TbPedido::class];
    $tbItem          = $c[TbItem::class];
    $tbArquivo       = $c[TbArquivo::class];
    $tbCliente       = $c[TbCliente::class];
    $tbDetalhes      = $c[TbDetalhesAcabamento::class];
    $addrFinder      = $c[AddressFinder::class];
    $priceCalculator = $c[PriceCalculator::class];
    $shipping        = $c[Shipping::class];
    $withdrawal      = $c[Withdrawal::class];
    $item            = $c[Item::class];
    return new Order($tbEntrega, $tbRetirada, $tbFrete, $tbEndereco, $tbPedido, $tbItem, $tbArquivo, $tbCliente, $addrFinder, $priceCalculator, $shipping, $withdrawal, $item, $tbDetalhes);
};

$dependencies[OrderPartial::class] = function ($c) {
    $tcor = $c[TbCor::class];
    $tlado = $c[TbLado::class];
    $tpapel = $c[TbPapel::class];
    $tacabamento = $c[TbAcabamento::class];
    $tfaixa = $c[TbFaixa::class];
    $tbCupomGeral = $c[TbCupomGeral::class];
    return new OrderPartial($tcor, $tlado, $tpapel, $tacabamento, $tfaixa, $tbCupomGeral);
};

$dependencies[CustomerAddress::class] = function ($c) {
    $addrEnt = $c[TbEndereco::class];
    return new CustomerAddress($addrEnt);
};

$dependencies[Customer::class] = function ($c) {
    $custEnt = $c[TbCliente::class];
    return new Customer($custEnt);
};

$dependencies[Payment::class] = function ($c) {
    $payService = $c[PaymentService::class];
    $item       = $c[Item::class];
    $tbCliente  = $c[TbCliente::class];
    $tbpedido   = $c[TbPedido::class];
    return new Payment($payService, $item, $tbCliente, $tbpedido);
};

$dependencies[PriceCalculator::class] = function ($c) {
    $tbFaixa = $c[TbFaixa::class];
    $tbPapel = $c[TbPapel::class];
    $tbAcabamento = $c[TbAcabamento::class];
    $tbCupomGeral = $c[TbCupomGeral::class];
    $tbCupom = $c[TbCupom::class];
    return new PriceCalculator($tbFaixa, $tbPapel, $tbAcabamento, $tbCupomGeral, $tbCupom);
};

$dependencies[ShoppingCart::class] = function ($c) {
    $tbPedido = $c[TbPedido::class];
    $tbItem = $c[TbItem::class];
    $tbEntrega = $c[TbEntrega::class];
    $tbArquivo = $c[TbArquivo::class];
    $tbFrete = $c[TbFrete::class];
    $tbRetirada = $c[TbRetirada::class];
    $tbCliente = $c[TbCliente::class];
    $pdf = $c[PDF::class];

    return new ShoppingCart($tbPedido, $tbItem, $tbEntrega, $tbArquivo, $tbFrete, $tbRetirada, $tbCliente, $pdf);
};

$dependencies[Item::class] = function ($c) {
    $tbItem = $c[TbItem::class];
    $tbArquivo = $c[TbArquivo::class];
    $tbCupom = $c[TbCupom::class];
    $tbPedido = $c[TbPedido::class];
    return new Item($tbItem, $tbArquivo, $tbCupom, $tbPedido);
};

$dependencies[NF::class] = function ($c) {
    $nf = $c[NFService::class];
    $tbPedido = $c[TbPedido::class];
    $item         = $c[Item::class];
    return new NF($nf, $tbPedido, $item);
};

$dependencies[PasswordRecovery::class] = function ($c) {
    $passEnt = $c[PasswordRecoveryEntity::class];
    return new PasswordRecovery($passEnt);
};

$dependencies[Csv::class] = function ($c) {
    $csvEntity = $c[CsvEntity::class];
    $tbFaixa   = $c[TbFaixa::class];
    return new Csv($csvEntity, $tbFaixa);
};

$dependencies[SalesReport::class] = function ($c) {
    $orderEntity = $c[OrderReportEntity::class];
    return new SalesReport($orderEntity);
};

$dependencies[Sms::class] = function ($c) {
    $smsEntity = $c[smsEntity::class];
    return new Sms($smsEntity);
};

$dependencies[Costs::class] = function ($c) {
    $printCostEntity  = $c[PrintCostEntity::class];
    $finishCostEntity = $c[FinishCostEntity::class];
    return new Costs($printCostEntity, $finishCostEntity);
};

$dependencies[EmailMkt::class] = function ($c) {
    $tbEmailMkt = $c[TbEmailMkt::class];
    return new EmailMkt($tbEmailMkt);
};

$dependencies[Ip::class] = function ($c) {
    $tbIp = $c[TbIp::class];
    return new Ip($tbIp);
};

$dependencies[Invitation::class] = function ($c) {
    $tbCupom = $c[TbCupom::class];
    $tbCliente = $c[TbCliente::class];
    return new Invitation($tbCupom, $tbCliente);
};

$dependencies[Company::class] = function ($c) {
    $tbEmpresa = $c[TbEmpresa::class];
    return new Company($tbEmpresa);
};

$dependencies[ShippingPrices::class] = function ($c) {
    $tbShippingPrices = $c[TbShippingPrices::class];
    return new ShippingPrices($tbShippingPrices);
};
// ----------------------  /Models

// ----------------------  Services
$dependencies[ShippingCorreios::class] = function ($c) {
    $shippingData = $c['settings']['shipping'];
    return new ShippingCorreios($shippingData['from_zip']);
};

$dependencies[AddressFinder::class] = function ($c) {
    return new AddressFinder();
};

$dependencies[PaymentService::class] = function ($c) {
    $iuguToken     = $c['settings']['payment']['iuguToken'];
    $iuguAccountId = $c['settings']['payment']['iuguAccountId'];
    return new PaymentService($iuguToken, $iuguAccountId);
};

$dependencies[NFService::class] = function ($c) {
    $empresaId = $c['settings']['notaFiscal']['empresaId'];
    $apiKey = $c['settings']['notaFiscal']['apiKey'];
    return new NFService($empresaId, $apiKey);
};

$dependencies[Cookie::class] = function ($c) {
    $cookieSettings = $c['settings']['session'];
    return new Cookie($cookieSettings['cookie_name'], $cookieSettings['cookie_expires']);
};

$dependencies[JwtWrapper::class] = function ($c) {
    $jwtSettings = $c['settings']['jwt'];
    return new JwtWrapper($jwtSettings['app_secret'], $jwtSettings['token_expires']);
};

$dependencies[AuthAccount::class] = function ($c) {
    $jwt = $c[JwtWrapper::class];
    $sessionCookie = $c[Cookie::class];
    return new AuthAccount($jwt, $sessionCookie);
};

$dependencies[MailChimpService::class] = function ($c) {
    $mailChimpToken = $c['settings']['mailchimp']['token'];
    return new MailChimpService($mailChimpToken);
};

$dependencies[TotalVoiceClient::class] = function ($c) {
    $token = $c['settings']['sms']['totalVoiceToken'];
    return new TotalVoiceClient($token);
};

$dependencies[SmsService::class] = function ($c) {
    $tvClient = $c[TotalVoiceClient::class];
    return new SmsService($tvClient);
};

$dependencies[VerifyEmail::class] = function ($c) {
    return new VerifyEmail();
};
// ----------------------  /Services

// ----------------------  Workers
$dependencies[EmailWorker::class] = $dependencies->factory(function ($c) {
    $messageConfig = $c['settings']['mail']['message'];
    $mailer = $c[Mailer::class];
    return new EmailWorker($mailer, $messageConfig);
});

$dependencies[MailgunWorker::class] = function ($c) {
    $apiKey        = $c['settings']['mailgun']['api_key'];
    $domain        = $c['settings']['mailgun']['domain'];
    $messageConfig = $c['settings']['mail']['message'];

    return new MailgunWorker($apiKey, $domain, $messageConfig);
};

$dependencies[RemoveFilesWorker::class] = function ($c) {
    $order = $c[Order::class];
    $pdf   = $c[PDF::class];
    return new RemoveFilesWorker($order, $pdf);
};

$dependencies[RemoveFilesNotRegisteredWorker::class] = function ($c) {
    $order = $c[Order::class];
    $pdf   = $c[PDF::class];
    return new RemoveFilesNotRegisteredWorker($order, $pdf);
};

$dependencies[NotifyPendingPaymentWorker::class] = function ($c) {
    $order = $c[Order::class];
    $emailProducer = $c[Pheanstalk::class];
    return new NotifyPendingPaymentWorker($order, $emailProducer);
};
// ----------------------  /Workers

// ----------------------  Controllers
$dependencies[UserController::class] = function ($c) {
    $userM = $c[User::class];
    $authAccount = $c[AuthAccount::class];
    return new UserController($authAccount, $userM);
};

$dependencies[WithdrawalController::class] = function ($c) {
    $withdrawal = $c[Withdrawal::class];
    return new WithdrawalController($withdrawal);
};

$dependencies[ShippingController::class] = function ($c) {
    $shipping = $c[Shipping::class];
    $addrFinder = $c[AddressFinder::class];
    $priceCalculator = $c[PriceCalculator::class];
    $authAccount = $c[AuthAccount::class];
    return new ShippingController($shipping, $addrFinder, $priceCalculator, $authAccount);
};

$dependencies[LoginController::class] = function ($c) {
    $authAccount = $c[AuthAccount::class];
    $tbCliente = $c[TbCliente::class];
    $pass = $c[PasswordRecovery::class];
    $order = $c[Order::class];
    $emailProducer = $c[Pheanstalk::class];
    $emailProducer->useTube($c['settings']['mail']['tube_name']);
    $verifyEmail = $c[VerifyEmail::class];
    $verifyEmail->setEmailFrom($c['settings']['mail']['message']['default_from']);
    $verifyEmail->setConnectionTimeout(5);
    return new LoginController($authAccount, $tbCliente, $pass, $order, $emailProducer, $verifyEmail);
};

$dependencies[PDFController::class] = function ($c) {
    $pdf = $c[PDF::class];
    return new PDFController($pdf);
};

$dependencies[OrderController::class] = function ($c) {
    $order = $c[Order::class];
    $customer = $c[Customer::class];
    $authAccount = $c[AuthAccount::class];
    $emailProducer = $c[Pheanstalk::class];
    $emailProducer->useTube($c['settings']['mail']['tube_name']);
    $pdf = $c[PDF::class];
    return new OrderController($authAccount, $order, $customer, $emailProducer, $pdf);
};

$dependencies[PartialOrderController::class] = function ($c) {
    $pOrder = $c[OrderPartial::class];
    $settings = $c['settings']['storage'];
    return new PartialOrderController($pOrder, $settings['save']);
};

$dependencies[CustomerController::class] = function ($c) {
    $cus = $c[Customer::class];
    $cusAddr = $c[CustomerAddress::class];
    $authAccount = $c[AuthAccount::class];
    return new CustomerController($authAccount, $cus, $cusAddr);
};

$dependencies[PaymentController::class] = function ($c) {
    $payment  = $c[Payment::class];
    $customer = $c[Customer::class];
    $emailProducer = $c[Pheanstalk::class];
    $emailProducer->useTube($c['settings']['mail']['tube_name']);
    $order = $c[Order::class];
    $item = $c[Item::class];
    $tbPedido = $c[TbPedido::class];
    $authAccount = $c[AuthAccount::class];
    $ip = $c[Ip::class];
    $invitation = $c[Invitation::class];
    $useCaptcha = $c['settings']['recaptcha']['use_captcha'];
    return new PaymentController($authAccount, $payment, $customer, $emailProducer, $order, $item, $tbPedido, $ip, $invitation, $useCaptcha);
};

$dependencies[CouponController::class] = function ($c) {
    $coupon = $c[Coupon::class];
    $order = $c[Order::class];
    $authAccount = $c[AuthAccount::class];
    $tbCoupon = $c[TbCupom::class];
    return new CouponController($authAccount, $coupon, $order, $tbCoupon);
};

$dependencies[ShoppingCartController::class] = function ($c) {
    $scart         = $c[ShoppingCart::class];
    $iuguAccountId = $c['settings']['payment']['iuguAccountId'];
    $iuguCardTest  = $c['settings']['payment']['iuguCardTest'];
    $order = $c[Order::class];
    $authAccount = $c[AuthAccount::class];
    return new ShoppingCartController($authAccount, $scart, $iuguAccountId, $iuguCardTest, $order);
};

$dependencies[ItemController::class] = function ($c) {
    $item        = $c[Item::class];
    $order       = $c[Order::class];
    $storage     = $c['settings']['storage'];
    $emailEnable = $c['settings']['mail']['enable_status'];
    $producer    = $c[Pheanstalk::class]->useTube($c['settings']['mail']['tube_name']);
    $smsService  = $c[SmsService::class];
    $sms         = $c[Sms::class];
    $pdf         = $c[PDF::class];
    return new ItemController(
        $item,
        $order,
        $storage['download_all'],
        $emailEnable,
        $producer,
        $smsService,
        $sms,
        $pdf
    );
};

$dependencies[NFController::class] = function ($c) {
    $nf = $c[NF::class];
    return new NFController($nf);
};

$dependencies[CouponManagerController::class] = function ($c) {
    $coupon = $c[Coupon::class];
    return new CouponManagerController($coupon);
};

$dependencies[BudgetController::class] = function ($c) {
    $emailProducer = $c[Pheanstalk::class];
    $emailProducer->useTube($c['settings']['mail']['tube_name']);
    return new BudgetController($emailProducer);
};

$dependencies[MailChimpController::class] = function ($c) {
    $mChimpService = $c[MailChimpService::class];
    $lists         = $c['settings']['mailchimp']['lists'];
    return new MailChimpController($mChimpService, $lists);
};

$dependencies[CsvController::class] = function ($c) {
    $csv = $c[Csv::class];
    return new CsvController($csv);
};

$dependencies[SalesReportController::class] = function ($c) {
    $salesReport = $c[SalesReport::class];
    return new SalesReportController($salesReport);
};

$dependencies[SmsController::class] = function ($c) {
    $sms = $c[Sms::class];
    return new SmsController($sms);
};

$dependencies[CostsController::class] = function ($c) {
    $costs = $c[Costs::class];
    return new CostsController($costs);
};

$dependencies[EmailMktController::class] = function ($c) {
    $emailMkt = $c[EmailMkt::class];
    return new EmailMktController($emailMkt);
};

$dependencies[SettingsController::class] = function ($c) {
    $settingsEnt = $c[TbSettings::class];
    return new SettingsController($settingsEnt);
};

$dependencies[ContactController::class] = function ($c) {
    $emailProducer = $c[Pheanstalk::class];
    $emailProducer->useTube($c['settings']['mail']['tube_name']);
    return new ContactController($emailProducer);
};

$dependencies[PricesController::class] = function ($c) {
    $tbFaixa = $c[TbFaixa::class];
    return new PricesController($tbFaixa);
};

$dependencies[InvitationController::class] = function ($c) {
    $authAccount = $c[AuthAccount::class];
    $invitation = $c[Invitation::class];
    return new InvitationController($authAccount, $invitation);
};

$dependencies[CompanyController::class] = function ($c) {
    $company = $c[Company::class];
    $authAccount = $c[AuthAccount::class];
    $emailProducer = $c[Pheanstalk::class];
    $emailProducer->useTube($c['settings']['mail']['tube_name']);
    return new CompanyController($authAccount, $company, $emailProducer);
};

$dependencies[ShippingPricesController::class] = function ($c) {
    $shippingPrices = $c[ShippingPrices::class];
    return new ShippingPricesController($shippingPrices);
};
// ----------------------  /Controllers

// ----------------------  Middlewares
$dependencies[AuthenticationMiddleware::class] = function ($c) {
    $layout = $c[Layout::class];
    $auth = $c[AuthAccount::class];
    $isLoggedIn = $auth->get('id') != false;
    $isAdmin = $auth->get('isAdmin') != false;
    return new AuthenticationMiddleware($layout, $isLoggedIn, $isAdmin);
};

$dependencies[HttpMiddleware::class] = function ($c) {
    $layout = $c[Layout::class];
    return new HttpMiddleware($layout, $c['settings']['display_errors']);
};

$dependencies[IuguTriggerMiddleware::class] = function ($c) {
    $authToken = $c['settings']['payment']['auth_trigger'];
    return new IuguTriggerMiddleware($authToken);
};
// ----------------------  /Middlewares

// ----------------------  Views
$dependencies[Layout::class] = function ($c) {
    return new Layout();
};
