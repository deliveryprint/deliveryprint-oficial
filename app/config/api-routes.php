<?php

namespace IntecPhp;

return [
    [
        'pattern' => '/myOrders',
        'callback' => Controller\UserController::class . ':myOrders',
    ],
    [
        'pattern' => '/criar-conta',
        'callback' => Controller\LoginController::class . ':createAccount',

    ],
    [
        'pattern' => '/changePassword',
        'middlewares' => [
            Middleware\AuthenticationMiddleware::class . ':isAuthenticated',
        ],
        'callback' => Controller\LoginController::class . ':changePassword',

    ],
    [
        'pattern' => '/salvar-empresa',
        'callback' => Controller\CompanyController::class . ':saveCompany',

    ],
    [
        'pattern' => '/pdfCount',
        'callback' => Controller\PDFController::class . ':getNumberOfPages',
    ],
    [
        'pattern' => '/makeOrder',
        'callback' => Controller\OrderController::class . ':saveOrderInfo',
    ],
    [
        'pattern' => '/applyOrderDiscount',
        'callback' => Controller\OrderController::class . ':applyOrderDiscount',
    ],
    [
        'pattern' => '/getInfoOrder',
        'middlewares' => [
            Middleware\AuthenticationMiddleware::class . ':isAuthenticated',
        ],
        'callback' => Controller\OrderController::class . ':getInfoOrder'
    ],
    [
        'pattern' => '/saveUserCompleteAddress',
        'middlewares' => [
            Middleware\AuthenticationMiddleware::class . ':isAuthenticated',
        ],
        'callback' => Controller\OrderController::class . ':saveUserCompleteAddress'
    ],
    [
        'pattern' => '/saveUserBasicInfo',
        'middlewares' => [
            Middleware\AuthenticationMiddleware::class . ':isAuthenticated',
        ],
        'callback' => Controller\OrderController::class . ':saveUserBasicInfo'
    ],
    [
        'pattern' => '/getPartialOrderData',
        'callback' => Controller\PartialOrderController::class . ':loadPartials',
    ],
    [
        'pattern' => '/getCustomerData',
        'middlewares' => [
            Middleware\AuthenticationMiddleware::class . ':isAuthenticated',
        ],
        'callback' => Controller\CustomerController::class . ':getInfo',
    ],
    [
        'pattern' => '/checkCaptcha',
        'middlewares' => [
            Middleware\AuthenticationMiddleware::class . ':isAuthenticated',
        ],
        'callback' => Controller\PaymentController::class . ':checkCaptcha',
    ],
    [
        'pattern' => '/updateCustomerData',
        'middlewares' => [
            Middleware\AuthenticationMiddleware::class . ':isAuthenticated',
        ],
        'callback' => Controller\CustomerController::class . ':updateInfo',
    ],
    [
        'pattern' => '/updateCustomerAddress',
        'middlewares' => [
            Middleware\AuthenticationMiddleware::class . ':isAuthenticated',
        ],
        'callback' => Controller\CustomerController::class . ':updateAddrInfo',
    ],
    [
        'pattern' => '/addCustomerAddress',
        'middlewares' => [
            Middleware\AuthenticationMiddleware::class . ':isAuthenticated',
        ],
        'callback' => Controller\CustomerController::class . ':addAddrInfo',
    ],
    [
        'pattern' => '/getCartData',
        'callback' => Controller\ShoppingCartController::class . ':getCartData'
    ],
    [
        'pattern' => '/getPaymentData',
        'callback' => Controller\ShoppingCartController::class . ':getPaymentData'
    ],
    [
        'pattern' => '/hasDeliveryName',
        'callback' => Controller\ShoppingCartController::class . ':hasDeliveryName'
    ],
    [
        'pattern' => '/removeShoppingCartItem',
        'callback' => Controller\ItemController::class . ':deleteItem'
    ],
    [
        'pattern' => '/getNumberItemsInCart',
        'callback' => Controller\ShoppingCartController::class . ':getNumberItemsInCart'
    ],
    [
        'pattern' => '/getItemObservation',
        'callback' => Controller\ItemController::class . ':getItemObservation'
    ],
    [
        'pattern' => '/saveItemObservation',
        'callback' => Controller\ItemController::class . ':saveItemObservation'
    ],
    [
        'pattern' => '/creditCardPayment',
        'middlewares' => [
            Middleware\AuthenticationMiddleware::class . ':isAuthenticated',
        ],
        'callback' => Controller\PaymentController::class . ':creditCardPayment'
    ],
    [
        'pattern' => '/bankSlipPayment',
        'middlewares' => [
            Middleware\AuthenticationMiddleware::class . ':isAuthenticated',
        ],
        'callback' => Controller\PaymentController::class . ':bankSlipPayment'
    ],
    [
        'pattern' => '/promotionPayment',
        'middlewares' => [
            Middleware\AuthenticationMiddleware::class . ':isAuthenticated',
        ],
        'callback' => Controller\PaymentController::class . ':promotionPayment'
    ],
    [
        'pattern' => '/getOrderFormatted',
        'middlewares' => [
            Middleware\AuthenticationMiddleware::class . ':isAuthenticated',
        ],
        'callback' => Controller\PaymentController::class . ':getFormattedECommerceInfo'
    ],
    [
        'pattern' => '/updateOrderStatus/a099885d6c873a4b24ae4db764163a6d',
        'middlewares' => [
            Middleware\IuguTriggerMiddleware::class
        ],
        'callback' => Controller\PaymentController::class . ':updateOrderStatusAfterPayment'
    ],
    [
        'pattern' => '/getBankSlip',
        'middlewares' => [
            Middleware\AuthenticationMiddleware::class . ':isAuthenticated',
        ],
        'callback' => Controller\PaymentController::class . ':getSessionBankSlip'
    ],
    [
        'pattern' => '/paySelectedOrder',
        'middlewares' => [
            Middleware\AuthenticationMiddleware::class . ':isAuthenticated',
        ],
        'callback' => Controller\PaymentController::class . ':paySelectedOrder'
    ],
    [
        'pattern' => '/getFreightTypes',
        'callback' => Controller\ShippingController::class . ':getFreightTypes'
    ],
    [
        'pattern' => '/getAddress',
        'callback' => Controller\ShippingController::class . ':getAddressFromCep'
    ],
    [
        'pattern' => '/getWithdrawalTypes',
        'callback' => Controller\WithdrawalController::class . ':getWithdrawalTypes'
    ],
    [
        'pattern' => '/getAllInvitationCoupons',
        'middlewares' => [
            Middleware\AuthenticationMiddleware::class . ':isAuthenticated',
        ],
        'callback' => Controller\InvitationController::class . ':getAllInvitationCoupons'
    ],
    [
        'pattern' => '/getAllInvitationCouponsByUser',
        'middlewares' => [
            Middleware\AuthenticationMiddleware::class . ':isAuthenticated',
        ],
        'callback' => Controller\InvitationController::class . ':getAllInvitationCouponsByUser'
    ],
    [
        'pattern' => '/getInviteCodeByUser',
        'middlewares' => [
            Middleware\AuthenticationMiddleware::class . ':isAuthenticated',
        ],
        'callback' => Controller\InvitationController::class . ':getInviteCodeByUser'
    ],
    [
        'pattern' => '/getInviteCouponsUsed',
        'middlewares' => [
            Middleware\AuthenticationMiddleware::class . ':isAdminAuthenticated',
        ],
        'callback' => Controller\SalesReportController::class . ':getInviteCouponsUsed'
    ],
    [
        'pattern' => '/checkCoupon',
        'callback' => Controller\CouponController::class . ':checkCoupon'
    ],
    [
        'pattern' => '/validateCoupon',
        'callback' => Controller\CouponController::class . ':validateCoupon'
    ],
    [
        'pattern' => '/saveEmail',
        'callback' => Controller\EmailMktController::class . ':saveEmail'
    ],
    [
        'pattern' => '/getAllEmails',
        'middlewares' => [
            Middleware\AuthenticationMiddleware::class . ':isAdminAuthenticated',
        ],
        'callback' => Controller\EmailMktController::class . ':getAllEmails'
    ],
    [
        'pattern' => '/getCartEmails',
        'middlewares' => [
            Middleware\AuthenticationMiddleware::class . ':isAdminAuthenticated',
        ],
        'callback' => Controller\EmailMktController::class . ':getShoppingCartDiscountsByPeriod'
    ],
    [
        'pattern' => '/getSettings',
        'callback' => Controller\SettingsController::class . ':getSettings'
    ],
    [
        'pattern' => '/updateSettings',
        'callback' => Controller\SettingsController::class . ':updateSettings'
    ],
    [
        'pattern' => '/loginAdm',
        'callback' => Controller\LoginController::class . ':loginAdm',
    ],
    [
        'pattern' => '/getAllItems',
        'callback' => Controller\ItemController::class . ':getAllItems'
    ],
    [
        'pattern' => '/documento/([a-f0-9]{64})',
        'callback' => Controller\PDFController::class . ':showDocument',
    ],
    [
        'pattern' => '/getItemDetails/([0-9]+)',
        'callback' => Controller\ItemController::class . ':getDetails'
    ],
    [
        'pattern' => '/order/changeStatus',
        'callback' => Controller\OrderController::class . ':changeStatus'
    ],
    [
        'pattern' => '/makeNF/8aa07ca01ec88893e3948da003a10821',
        'callback' => Controller\NFController::class . ':makeNF'
    ],
    [
        'pattern' => '/requestRecovery',
        'callback' => Controller\LoginController::class . ':passwordRecovery'
    ],
    [
        'pattern' => '/changePasswordFromRecovery',
        'callback' => Controller\LoginController::class . ':changePasswordFromRecovery',
    ],
    [
        'pattern' => '/verifyEmail',
        'callback' => Controller\LoginController::class . ':verifyEmail',
    ],
    [
        'pattern' => '/checkVerification',
        'callback' => Controller\LoginController::class . ':checkVerification',
    ],
    [
        'pattern' => '/resendVerificationEmail',
        'callback' => Controller\LoginController::class . ':resendEmail',
    ],
    [
        'pattern' => '/saveObs',
        'callback' => Controller\OrderController::class . ':changeObs'
    ],
    [
        'pattern' => '/getNewOrder',
        'callback' => Controller\OrderController::class . ':getNewOrder'
    ],
    [
        'pattern' => '/saveAdmObs',
        'callback' => Controller\ItemController::class . ':updateAdmObservation'
    ],
    [
        'pattern' => '/emailDeConfirmacao',
        'callback' => Controller\OrderController::class . ':sendEmail'
    ],
    [
        'pattern' => '/emailFreteLento',
        'callback' => Controller\OrderController::class . ':sendOrderEmail'
    ],
    [
        'pattern' => '/emailContato',
        'callback' => Controller\ContactController::class . ':sendContactEmail'
    ],
    [
        'pattern' => '/emailCampanhaEntrega',
        'callback' => Controller\ContactController::class . ':sendCampaignEmail'
    ],
    [
        'pattern' => '/item/changeStatus',
        'callback' => Controller\ItemController::class . ':changeItemStatus'
    ],
    [
        'pattern' => '/item/changeFileStatus',
        'callback' => Controller\ItemController::class . ':changeFileStatus'
    ],
    [
        'pattern' => '/item/updateTrackingCode',
        'middlewares' => [
            Middleware\AuthenticationMiddleware::class . ':isAdminAuthenticated',
        ],
        'callback' => Controller\ItemController::class . ':updateTrackingCode'
    ],
    [
        'pattern' => '/downloadAll/([0-9]+)',
        'middlewares' => [
            Middleware\AuthenticationMiddleware::class . ':isAdminAuthenticated',
        ],
        'callback' => Controller\ItemController::class . ':downloadAll'
    ],
    [
        'pattern' => '/download/([a-f0-9]{64})',
        'middlewares' => [
            Middleware\AuthenticationMiddleware::class . ':isAdminAuthenticated',
        ],
        'callback' => Controller\ItemController::class . ':download'
    ],
    [
        'pattern' => '/addCoupon',
        'middlewares' => [
            Middleware\AuthenticationMiddleware::class . ':isAdminAuthenticated',
        ],
        'callback' => Controller\CouponManagerController::class . ':addCoupon'
    ],
    [
        'pattern' => '/findCoupons',
        'middlewares' => [
            Middleware\AuthenticationMiddleware::class . ':isAdminAuthenticated',
        ],
        'callback' => Controller\CouponManagerController::class . ':find'
    ],
    [
        'pattern' => '/removeCoupon',
        'middlewares' => [
            Middleware\AuthenticationMiddleware::class . ':isAdminAuthenticated',
        ],
        'callback' => Controller\CouponManagerController::class . ':removeCoupon'
    ],
    [
        'pattern' => '/deactiveCoupon',
        'middlewares' => [
            Middleware\AuthenticationMiddleware::class . ':isAdminAuthenticated',
        ],
        'callback' => Controller\CouponManagerController::class . ':deactiveCoupon'
    ],
    [
        'pattern' => '/activeCoupon',
        'middlewares' => [
            Middleware\AuthenticationMiddleware::class . ':isAdminAuthenticated',
        ],
        'callback' => Controller\CouponManagerController::class . ':activeCoupon'
    ],
    [
        'pattern' => '/couponDetails',
        'middlewares' => [
            Middleware\AuthenticationMiddleware::class . ':isAdminAuthenticated',
        ],
        'callback' => Controller\CouponManagerController::class . ':couponDetails'
    ],
    [
        'pattern' => '/budget/request',
        'callback' => Controller\BudgetController::class . ':requestBudget'
    ],
    [
        'pattern' => '/subscribeEmailToList',
        'callback' => Controller\MailChimpController::class . ':subscribeEmailToList'
    ],
    [
        'pattern' => '/plottBudget/request',
        'callback' => Controller\BudgetController::class . ':plottBudgetRequest'
    ],
    [
        'pattern' => '/budget/sendEmail',
        'callback' => Controller\BudgetController::class . ':sendEmail'
    ],
    [
        'pattern' => '/saveCsv',
        'middlewares' => [
            Middleware\AuthenticationMiddleware::class . ':isAdminAuthenticated',
        ],
        'callback' => Controller\CsvController::class . ':save'
    ],
    [
        'pattern' => '/changePrices',
        'middlewares' => [
            Middleware\AuthenticationMiddleware::class . ':isAdminAuthenticated',
        ],
        'callback' => Controller\CsvController::class . ':changePrices'
    ],
    [
        'pattern' => '/listCsv',
        'middlewares' => [
            Middleware\AuthenticationMiddleware::class . ':isAdminAuthenticated',
        ],
        'callback' => Controller\CsvController::class . ':listCsv'
    ],
    [
        'pattern' => '/getAllRanges',
        'middlewares' => [
            Middleware\AuthenticationMiddleware::class . ':isAdminAuthenticated',
        ],
        'callback' => Controller\PricesController::class . ':getAllWithPaper'
    ],
    [
        'pattern' => '/updateRangePrices',
        'middlewares' => [
            Middleware\AuthenticationMiddleware::class . ':isAdminAuthenticated',
        ],
        'callback' => Controller\PricesController::class . ':updatePriceRanges'
    ],
    [
        'pattern' => '/getPaperPrices',
        'callback' => Controller\PricesController::class . ':getPaperPrices'
    ],
    [
        'pattern' => '/listUsers',
        'middlewares' => [
            Middleware\AuthenticationMiddleware::class . ':isAdminAuthenticated',
        ],
        'callback' => Controller\CustomerController::class . ':listUsers'
    ],
    [
        'pattern' => '/createUsersCsv',
        'middlewares' => [
            Middleware\AuthenticationMiddleware::class . ':isAdminAuthenticated',
        ],
        'callback' => Controller\CustomerController::class . ':createUsersCsv'
    ],
    [
        'pattern' => '/downloadCsv',
        'middlewares' => [
            Middleware\AuthenticationMiddleware::class . ':isAdminAuthenticated',
        ],
        'callback' => Controller\CsvController::class . ':downloadTempCsv'
    ],
    [
        'pattern' => '/createCouponsCsv',
        'middlewares' => [
            Middleware\AuthenticationMiddleware::class . ':isAdminAuthenticated',
        ],
        'callback' => Controller\CouponManagerController::class . ':createCouponsCsv'
    ],
    [
        'pattern' => '/createEmailsCsv',
        'middlewares' => [
            Middleware\AuthenticationMiddleware::class . ':isAdminAuthenticated',
        ],
        'callback' => Controller\EmailMktController::class . ':createEmailsCsv'
    ],
    [
        'pattern' => '/generateSalesReport',
        'middlewares' => [
            Middleware\AuthenticationMiddleware::class . ':isAdminAuthenticated',
        ],
        'callback' => Controller\SalesReportController::class . ':generateSalesReport'
    ],
    [
        'pattern' => '/getColorsData',
        'middlewares' => [
            Middleware\AuthenticationMiddleware::class . ':isAdminAuthenticated',
        ],
        'callback' => Controller\SalesReportController::class . ':getColorsData'
    ],
    [
        'pattern' => '/getSalesData',
        'middlewares' => [
            Middleware\AuthenticationMiddleware::class . ':isAdminAuthenticated',
        ],
        'callback' => Controller\SalesReportController::class . ':getSalesData'
    ],
    [
        'pattern' => '/getCouponsData',
        'middlewares' => [
            Middleware\AuthenticationMiddleware::class . ':isAdminAuthenticated',
        ],
        'callback' => Controller\SalesReportController::class . ':getCouponsData'
    ],
    [
        'pattern' => '/getFinishesData',
        'middlewares' => [
            Middleware\AuthenticationMiddleware::class . ':isAdminAuthenticated',
        ],
        'callback' => Controller\SalesReportController::class . ':getFinishesData'
    ],
    [
        'pattern' => '/getDeliveryTypesData',
        'middlewares' => [
            Middleware\AuthenticationMiddleware::class . ':isAdminAuthenticated',
        ],
        'callback' => Controller\SalesReportController::class . ':getDeliveryTypesData'
    ],
    [
        'pattern' => '/getRangesData',
        'middlewares' => [
            Middleware\AuthenticationMiddleware::class . ':isAdminAuthenticated',
        ],
        'callback' => Controller\SalesReportController::class . ':getRangesData'
    ],
    [
        'pattern' => '/getPeriodProfit',
        'middlewares' => [
            Middleware\AuthenticationMiddleware::class . ':isAdminAuthenticated',
        ],
        'callback' => Controller\SalesReportController::class . ':getPeriodProfit'
    ],
    [
        'pattern' => '/getAverageTicket',
        'middlewares' => [
            Middleware\AuthenticationMiddleware::class . ':isAdminAuthenticated',
        ],
        'callback' => Controller\SalesReportController::class . ':getAverageTicket'
    ],
    [
        'pattern' => '/getCustomerSales',
        'middlewares' => [
            Middleware\AuthenticationMiddleware::class . ':isAdminAuthenticated',
        ],
        'callback' => Controller\SalesReportController::class . ':getCustomerSales'
    ],
    [
        'pattern' => '/generateSalesReportCsv',
        'middlewares' => [
            Middleware\AuthenticationMiddleware::class . ':isAdminAuthenticated',
        ],
        'callback' => Controller\SalesReportController::class . ':generateSalesReportCsv'
    ],
    [
        'pattern' => '/generateLtvTable',
        'middlewares' => [
            Middleware\AuthenticationMiddleware::class . ':isAdminAuthenticated',
        ],
        'callback' => Controller\SalesReportController::class . ':generateLtvTable'
    ],
    [
        'pattern' => '/prepareCsv',
        'middlewares' => [
            Middleware\AuthenticationMiddleware::class . ':isAdminAuthenticated',
        ],
        'callback' => Controller\CsvController::class . ':prepareCsvForDownload'
    ],
    [
        'pattern' => '/smsReply',
        'callback' => Controller\SmsController::class . ':saveReply'
    ],
    [
        'pattern' => '/listSms',
        'middlewares' => [
            Middleware\AuthenticationMiddleware::class . ':isAdminAuthenticated',
        ],
        'callback' => Controller\SmsController::class . ':listSms'
    ],
    [
        'pattern' => '/createSmsCsv',
        'middlewares' => [
            Middleware\AuthenticationMiddleware::class . ':isAdminAuthenticated',
        ],
        'callback' => Controller\SmsController::class . ':createSmsCsv'
    ],
    [
        'pattern' => '/getAllCosts',
        'middlewares' => [
            Middleware\AuthenticationMiddleware::class . ':isAdminAuthenticated',
        ],
        'callback' => Controller\CostsController::class . ':getAllCosts'
    ],
    [
        'pattern' => '/updatePrintCosts',
        'middlewares' => [
            Middleware\AuthenticationMiddleware::class . ':isAdminAuthenticated',
        ],
        'callback' => Controller\CostsController::class . ':updatePrintCosts'
    ],
    [
        'pattern' => '/updateFinishCosts',
        'middlewares' => [
            Middleware\AuthenticationMiddleware::class . ':isAdminAuthenticated',
        ],
        'callback' => Controller\CostsController::class . ':updateFinishCosts'
    ],
    [
        'pattern' => '/getShippingPrices',
        'middlewares' => [
            Middleware\AuthenticationMiddleware::class . ':isAdminAuthenticated',
        ],
        'callback' => Controller\ShippingPricesController::class . ':getShippingPrices'
    ],
    [
        'pattern' => '/updateDeliveryShippingPrices',
        'middlewares' => [
            Middleware\AuthenticationMiddleware::class . ':isAdminAuthenticated',
        ],
        'callback' => Controller\ShippingPricesController::class . ':updateDeliveryShippingPrices'
    ],
    [
        'pattern' => '/updateWithdrawShippingPrices',
        'middlewares' => [
            Middleware\AuthenticationMiddleware::class . ':isAdminAuthenticated',
        ],
        'callback' => Controller\ShippingPricesController::class . ':updateWithdrawShippingPrices'
    ],
];
