<?php
chdir(dirname(dirname(__DIR__)));
include './vendor/autoload.php';

$settings = require_once 'app/config/settings.php';

if (file_exists('app/config/settings.local.php')) {
    $settings = array_replace_recursive($settings, require 'app/config/settings.local.php');
}

require 'app/config/dependencies.php';

use IntecPhp\Worker\RemoveFilesWorker;

try {
    $worker = $dependencies[RemoveFilesWorker::class];
    $worker->execute();
} catch (\Throwable $e) {
    print $e->getMessage() . "\n";
}
