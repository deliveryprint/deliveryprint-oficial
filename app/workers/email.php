<?php
chdir(dirname(dirname(__DIR__)));
include './vendor/autoload.php';

$settings = require_once 'app/config/settings.php';

if(file_exists('app/config/settings.local.php')) {
    $settings = array_replace_recursive($settings, require 'app/config/settings.local.php');
}

require 'app/config/dependencies.php';

use Pheanstalk\Pheanstalk;
use IntecPhp\Worker\EmailWorker;

$queue = $dependencies[Pheanstalk::class];
$queue
    ->watch($dependencies['settings']['mail']['tube_name'])
    ->ignore('default');
do {
    try {
        $job = $queue->reserve();
        $data = json_decode($job->getData(), true);
        $worker = $dependencies[EmailWorker::class];
        $worker->execute($data);
        $queue->delete($job);
    } catch (\Throwable $e) {
        print $e->getMessage() . "\n";
        $queue->delete($job);
    }
} while (true);
