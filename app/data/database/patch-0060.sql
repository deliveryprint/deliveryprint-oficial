DROP TABLE IF EXISTS `tbsettings`;
CREATE TABLE `tbsettings`  (
  `idSetting` int(11) NOT NULL AUTO_INCREMENT,
  `deliveryExtraBDays` INT(11) NOT NULL DEFAULT 0,
  `withdrawalExtraBDays` INT(11) NOT NULL DEFAULT 0,
  `dExpress1ExtraBDays` INT(11) NOT NULL DEFAULT 0,
  `dExpress2ExtraBDays` INT(11) NOT NULL DEFAULT 0,
  `dExpress3ExtraBDays` INT(11) NOT NULL DEFAULT 0,
  `dExpress4ExtraBDays` INT(11) NOT NULL DEFAULT 0,
  `dSedex1ExtraBDays` INT(11) NOT NULL DEFAULT 0,
  `dSedex2ExtraBDays` INT(11) NOT NULL DEFAULT 0,
  `dPac1ExtraBDays` INT(11) NOT NULL DEFAULT 0,
  `dPac2ExtraBDays` INT(11) NOT NULL DEFAULT 0,
  `wExpress1ExtraBDays` INT(11) NOT NULL DEFAULT 0,
  `wExpress2ExtraBDays` INT(11) NOT NULL DEFAULT 0,
  `wNormal1ExtraBDays` INT(11) NOT NULL DEFAULT 0,
  `wNormal2ExtraBDays` INT(11) NOT NULL DEFAULT 0,
  PRIMARY KEY (`idSetting`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

INSERT INTO `tbsettings` () VALUES ();
