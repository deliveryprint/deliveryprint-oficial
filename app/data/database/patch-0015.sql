
alter table tbpapel
    add column peso double default 0,
    add column largura double default 0,
    add column comprimento double default 0,
    add column espessura double default 0;

update tbpapel set peso=0.00467775, largura=21, comprimento=29.7, espessura=0.01  where idPapel=1;
update tbpapel set peso=0.0056133, largura=21, comprimento=29.7, espessura=0.01  where idPapel=2;
update tbpapel set peso=0.00717254999, largura=21, comprimento=29.7, espessura=0.01  where idPapel=3;
update tbpapel set peso=0.0106029, largura=21, comprimento=29.7, espessura=0.01  where idPapel=4;
update tbpapel set peso=0.005346, largura=21.6, comprimento=33, espessura=0.01  where idPapel=5;
update tbpapel set peso=0.00467775, largura=21, comprimento=29.7, espessura=0.01  where idPapel=6;
update tbpapel set peso=0.0045198, largura=21.6, comprimento=27.9, espessura=0.01  where idPapel=7;
update tbpapel set peso=0.0093555, largura=42, comprimento=29.7, espessura=0.01  where idPapel=8;
update tbpapel set peso=0.018711, largura=42, comprimento=29.7, espessura=0.01  where idPapel=9;
update tbpapel set peso=0.002331, largura=21, comprimento=14.8, espessura=0.01  where idPapel=10;
