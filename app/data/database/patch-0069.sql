DROP TABLE IF EXISTS `tbdetalhesacabamento`;
CREATE TABLE `tbdetalhesacabamento`  (
  `idDetalhes` INTEGER  NOT NULL AUTO_INCREMENT,
  `corDaCapa` VARCHAR(255) DEFAULT NULL,
  `corDaLetra` VARCHAR(255) DEFAULT NULL,
  `textoCapa` VARCHAR(255) DEFAULT NULL,
  `textoLombada` VARCHAR(255) DEFAULT NULL,
  PRIMARY KEY (`idDetalhes`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

alter table `tbitem` add `tbDetalhesAcabamento_idDetalhes` int(11) NULL DEFAULT NULL;
alter table `tbitem` add CONSTRAINT `fk_tbDetalhesAcabamento` FOREIGN KEY (`tbDetalhesAcabamento_idDetalhes`) REFERENCES `tbdetalhesacabamento` (`idDetalhes`);
