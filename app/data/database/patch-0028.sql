

create table password_recovery (
    id int auto_increment primary key,
    email varchar(100) not null,
    hash_code varchar(256) not null,
    active boolean default 1
);

alter table password_recovery add constraint unique_hash unique(hash_code);
