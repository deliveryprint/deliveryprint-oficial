alter table tbpapel add column paper_order smallint default null;

update tbpapel set paper_order=1 where idPapel=1;
update tbpapel set paper_order=2 where idPapel=2;
update tbpapel set paper_order=3 where idPapel=3;
update tbpapel set paper_order=4 where idPapel=4;
update tbpapel set paper_order=5 where idPapel=10;
update tbpapel set paper_order=6 where idPapel=8;
update tbpapel set paper_order=7 where idPapel=9;
update tbpapel set paper_order=8 where idPapel=5;
update tbpapel set paper_order=9 where idPapel=6;
update tbpapel set paper_order=10 where idPapel=7;

update tbpapel set descricaoPapel='Papel Sulfite A4 75g' where idPapel=1;
update tbpapel set descricaoPapel='Papel Sulfite A4 90g' where idPapel=2;
update tbpapel set descricaoPapel='Papel Couché A4 115g' where idPapel=3;
update tbpapel set descricaoPapel='Papel Couché A4 170g' where idPapel=4;
update tbpapel set descricaoPapel='Papel Ofício 75g' where idPapel=5;
update tbpapel set descricaoPapel='Papel Eco A4 75g' where idPapel=6;
update tbpapel set descricaoPapel='Papel Carta 75g' where idPapel=7;
update tbpapel set descricaoPapel='Papel Sulfite A3 75g' where idPapel=8;
update tbpapel set descricaoPapel='Papel Couché A3 150g' where idPapel=9;
update tbpapel set descricaoPapel='Papel Sulfite A5 75g' where idPapel=10;
