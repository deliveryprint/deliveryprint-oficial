DROP TABLE IF EXISTS `tbshippingprices`;
CREATE TABLE `tbshippingprices`  (
    `idShippingPrice` INT AUTO_INCREMENT PRIMARY KEY,
    `dExpress1Price` FLOAT NOT NULL DEFAULT 39.0,
    `dExpress2Price` FLOAT NOT NULL DEFAULT 35.0,
    `dExpress3Price` FLOAT NOT NULL DEFAULT 22.0,
    `dExpress4Price` FLOAT NOT NULL DEFAULT 18.0,
    `dExpress4AdditionalPrice` FLOAT NOT NULL DEFAULT 25.0,
    `dExpress5Price` FLOAT NOT NULL DEFAULT 18.0,
    `wExpress1Price` FLOAT NOT NULL DEFAULT 0.0,
    `wExpress2Price` FLOAT NOT NULL DEFAULT 0.0
) ENGINE = InnoDB;

INSERT INTO `tbshippingprices` (`idShippingPrice`) VALUES (1);
