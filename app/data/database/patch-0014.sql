alter table tbcliente drop column rgCliente;
alter table tbpedido drop column custoPedido;
alter table tbpedido drop column confirmacaoPagamento;
alter table tbpedido drop column tbEntrega_idEntrega;
alter table tbpedido add column dataPagto datetime default null;
alter table tbpedido drop column statusPagamento;
alter table tbpedido add column statusPagamento varchar(50);
alter table tbcliente drop column cpfCliente;
alter table tbcliente add column cpfCliente varchar(18) default null;
