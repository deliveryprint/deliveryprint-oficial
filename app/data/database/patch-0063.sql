alter table `tbpedido` add `discountValue` float default null;
alter table `tbpedido` add `discountType` ENUM('PERCENT', 'ABS') default null;
alter table `tbpedido` add `tbEmailMkt_idEmailMkt` int null;
alter table `tbpedido` add constraint `fk_p_tbEmailMkt` foreign key (`tbEmailMkt_idEmailMkt`) REFERENCES `tbemailmkt` (`idEmailMkt`);
