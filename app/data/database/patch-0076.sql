-- Altera as descrições dos papéis Papel Couché A4 115g, Papel Couché A4 170g, Papel Sulfite A5 75g
UPDATE tbpapel SET descricaoPapel = 'Papel Glossy A4 135g' WHERE idPapel = 3; -- Papel Couché A4 115g
UPDATE tbpapel SET descricaoPapel = 'Papel Glossy A4 220g' WHERE idPapel = 4; -- Papel Couché A4 170g
UPDATE tbpapel SET descricaoPapel = 'Papel Matte A4 230g' WHERE idPapel = 10; -- Papel Sulfite A5 75g
