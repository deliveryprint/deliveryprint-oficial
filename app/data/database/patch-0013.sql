alter table tbitem drop column obsMomentoSalvamento;
alter table tbitem add column obsMomentoSalvamento timestamp default current_timestamp;
alter table tbitem add statusItem int;
alter table tbitem add descTotal double not null;
alter table tbitem add qdeAcabamentos int;

CREATE TABLE IF NOT EXISTS tbcupom (
  idDesc INT AUTO_INCREMENT primary key,
  value FLOAT NOT NULL,
  validade DATE NOT NULL,
  codigo VARCHAR(25),
  status BOOLEAN DEFAULT 0,
  type ENUM('PERCENT', 'ABS') NOT NULL DEFAULT 'PERCENT',
  comment VARCHAR(256),
  cupom_limit INT,
  cupom_counter INT,
  cupom_user_limit INT
) ENGINE = InnoDB;

CREATE TABLE IF NOT EXISTS tbcupomGeral (
  idDescGeral INT AUTO_INCREMENT primary key,
  value FLOAT NOT NULL,
  status BOOLEAN DEFAULT 0,
  comment VARCHAR(256)
) ENGINE = InnoDB;

alter table tbitem add tbcupom_idDesc int;
alter table tbitem add constraint tbitem_x_tbcupom foreign key(tbcupom_idDesc) references tbcupom(idDesc);
alter table tbitem add tbcupomGeral_idDescGeral int;
alter table tbitem add constraint tbitem_x_tbcupomGeral foreign key(tbcupomGeral_idDescGeral) references tbcupomGeral(idDescGeral);
alter table tbitem add borda enum('CURTA', 'LONGA') after qdeAcabamentos;
