create table tbferiado (
    idFeriado int auto_increment primary key,
    fdate date default null
);


insert into tbferiado values(null, '2018-01-01');
insert into tbferiado values(null, '2018-01-25');
insert into tbferiado values(null, '2018-02-12');
insert into tbferiado values(null, '2018-02-13');
insert into tbferiado values(null, '2018-02-14');
insert into tbferiado values(null, '2018-03-30');
insert into tbferiado values(null, '2018-04-21');
insert into tbferiado values(null, '2018-05-01');
insert into tbferiado values(null, '2018-05-31');
insert into tbferiado values(null, '2018-07-09');
insert into tbferiado values(null, '2018-09-07');
insert into tbferiado values(null, '2018-10-12');
insert into tbferiado values(null, '2018-10-15');
insert into tbferiado values(null, '2018-10-28');
insert into tbferiado values(null, '2018-11-02');
insert into tbferiado values(null, '2018-11-15');
insert into tbferiado values(null, '2018-11-20');
insert into tbferiado values(null, '2018-11-25');


insert into tbferiado values(null, '2019-01-01');
insert into tbferiado values(null, '2019-01-25');
insert into tbferiado values(null, '2019-03-04');
insert into tbferiado values(null, '2019-03-05');
insert into tbferiado values(null, '2019-03-06');
insert into tbferiado values(null, '2019-04-19');
insert into tbferiado values(null, '2019-05-01');
insert into tbferiado values(null, '2019-06-20');
insert into tbferiado values(null, '2019-07-09');
insert into tbferiado values(null, '2019-09-07');
insert into tbferiado values(null, '2019-10-12');
insert into tbferiado values(null, '2019-10-15');
insert into tbferiado values(null, '2019-10-28');
insert into tbferiado values(null, '2019-11-02');
insert into tbferiado values(null, '2019-11-15');
insert into tbferiado values(null, '2019-11-20');
insert into tbferiado values(null, '2019-12-25');
