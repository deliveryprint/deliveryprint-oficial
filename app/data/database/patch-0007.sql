insert into tbpapel values (10, 'Papel Pequeno (A5 75g)', 0, 0, '75.svg');

alter table tbpapel drop column custoPapel, drop column valorPapel, add column precoPapel float not null;

drop table tbcolorido;
drop table tbfaixa;

CREATE TABLE IF NOT EXISTS tbfaixa (
  idFaixa INT AUTO_INCREMENT primary key,
  de INT NOT NULL,
  ate INT NOT NULL,
  preco FLOAT NOT NULL,
  idPapel INT NOT NULL,
  hasColor boolean default false,
  FOREIGN KEY (idPapel) REFERENCES tbpapel (idPapel)
) ENGINE = InnoDB;


-- preto e branco
insert into tbfaixa values

-- Papel Normal (A4 75g)
(null, 1, 25, 1, 1, false),
(null, 26, 100, 0.5, 1, false),
(null, 101, 300, 0.25, 1, false),
(null, 301, 600, 0.20, 1, false),
(null, 601, 1000, 0.17, 1, false),
(null, 1001, 3000, 0.14, 1, false),
(null, 3001, 6000, 0.12, 1, false),
(null, 6001, 1000000, 0.10, 1, false),

-- Papel Premium (A4 90g)
(null, 1, 25, 1, 2, false),
(null, 26, 100, 0.5, 2, false),
(null, 101, 300, 0.25, 2, false),
(null, 301, 600, 0.20, 2, false),
(null, 601, 1000, 0.17, 2, false),
(null, 1001, 3000, 0.14, 2, false),
(null, 3001, 6000, 0.12, 2, false),
(null, 6001, 1000000, 0.10, 2, false),

-- Papel Grosso 1 (A4 115g)
(null, 1, 25, 1, 3, false),
(null, 26, 100, 0.5, 3, false),
(null, 101, 300, 0.25, 3, false),
(null, 301, 600, 0.20, 3, false),
(null, 601, 1000, 0.17, 3, false),
(null, 1001, 3000, 0.14, 3, false),
(null, 3001, 6000, 0.12, 3, false),
(null, 6001, 1000000, 0.10, 3, false),

-- Papel Grosso 2 (A4 170g)
(null, 1, 25, 1, 4, false),
(null, 26, 100, 0.5, 4, false),
(null, 101, 300, 0.25, 4, false),
(null, 301, 600, 0.20, 4, false),
(null, 601, 1000, 0.17, 4, false),
(null, 1001, 3000, 0.14, 4, false),
(null, 3001, 6000, 0.12, 4, false),
(null, 6001, 1000000, 0.10, 4, false),

-- Papel Ofício (A4 75g)
(null, 1, 25, 1, 5, false),
(null, 26, 100, 0.5, 5, false),
(null, 101, 300, 0.25, 5, false),
(null, 301, 600, 0.20, 5, false),
(null, 601, 1000, 0.17, 5, false),
(null, 1001, 3000, 0.14, 5, false),
(null, 3001, 6000, 0.12, 5, false),
(null, 6001, 1000000, 0.10, 5, false),

-- Papel Reciclado (A4 75g)
(null, 1, 25, 1, 6, false),
(null, 26, 100, 0.5, 6, false),
(null, 101, 300, 0.25, 6, false),
(null, 301, 600, 0.20, 6, false),
(null, 601, 1000, 0.17, 6, false),
(null, 1001, 3000, 0.14, 6, false),
(null, 3001, 6000, 0.12, 6, false),
(null, 6001, 1000000, 0.10, 6, false),

-- Papel Carta (A4 75g)
(null, 1, 25, 1, 7, false),
(null, 26, 100, 0.5, 7, false),
(null, 101, 300, 0.25, 7, false),
(null, 301, 600, 0.20, 7, false),
(null, 601, 1000, 0.17, 7, false),
(null, 1001, 3000, 0.14, 7, false),
(null, 3001, 6000, 0.12, 7, false),
(null, 6001, 1000000, 0.10, 7, false),

-- Tamanho Grande (A3 75g)
(null, 1, 25, 4, 8, false),
(null, 26, 100, 2, 8, false),
(null, 101, 300, 1.4, 8, false),
(null, 301, 600, 1, 8, false),
(null, 601, 1000, 0.8, 8, false),
(null, 1001, 3000, 0.64, 8, false),
(null, 3001, 6000, 0.56, 8, false),
(null, 6001, 1000000, 0.48, 8, false),

-- Papel Grosso 3 (A3 150g)
(null, 1, 25, 4, 9, false),
(null, 26, 100, 2, 9, false),
(null, 101, 300, 1, 9, false),
(null, 301, 600, 0.8, 9, false),
(null, 601, 1000, 0.68, 9, false),
(null, 1001, 3000, 0.56, 9, false),
(null, 3001, 6000, 0.48, 9, false),
(null, 6001, 1000000, 0.40, 9, false),

--  Papel Pequeno (A5 75g)
(null, 1, 25, 1, 10, false),
(null, 26, 100, 0.5, 10, false),
(null, 101, 300, 0.35, 10, false),
(null, 301, 600, 0.25, 10, false),
(null, 601, 1000, 0.20, 10, false),
(null, 1001, 3000, 0.16, 10, false),
(null, 3001, 6000, 0.14, 10, false),
(null, 6001, 1000000, 0.12, 10, false)

;


-- colorido
insert into tbfaixa values

(null, 1, 25, 2.5, 1, true),
(null, 26, 50, 1.5, 1, true),
(null, 51, 100, 1.25, 1, true),
(null, 101, 300, 1, 1, true),
(null, 301, 600, 0.85, 1, true),
(null, 601, 1000, 0.6, 1, true),
(null, 1001, 3000, 0.45, 1, true),
(null, 3001, 100000, 0.4, 1, true),

(null, 1, 25, 2.75, 2, true),
(null, 26, 50, 1.65, 2, true),
(null, 51, 100, 1.38, 2, true),
(null, 101, 300, 1.1, 2, true),
(null, 301, 600, 0.94, 2, true),
(null, 601, 1000, 0.66, 2, true),
(null, 1001, 3000, 0.50, 2, true),
(null, 3001, 100000, 0.44, 2, true),

(null, 1, 25, 3.75, 3, true),
(null, 26, 50, 2.25, 3, true),
(null, 51, 100, 1.88, 3, true),
(null, 101, 300, 1.5, 3, true),
(null, 301, 600, 1.28, 3, true),
(null, 601, 1000, 0.9, 3, true),
(null, 1001, 3000, 0.68, 3, true),
(null, 3001, 100000, 0.6, 3, true),

(null, 1, 25, 5, 4, true),
(null, 26, 50, 3, 4, true),
(null, 51, 100, 2.5, 4, true),
(null, 101, 300, 2, 4, true),
(null, 301, 600, 1.7, 4, true),
(null, 601, 1000, 1.2, 4, true),
(null, 1001, 3000, 0.9, 4, true),
(null, 3001, 100000, 0.8, 4, true),

(null, 1, 25, 3.75, 5, true),
(null, 26, 50, 2.25, 5, true),
(null, 51, 100, 1.88, 5, true),
(null, 101, 300, 1.5, 5, true),
(null, 301, 600, 1.28, 5, true),
(null, 601, 1000, 0.9, 5, true),
(null, 1001, 3000, 0.68, 5, true),
(null, 3001, 100000, 0.6, 5, true),

(null, 1, 25, 3.75, 6, true),
(null, 26, 50, 2.25, 6, true),
(null, 51, 100, 1.88, 6, true),
(null, 101, 300, 1.5, 6, true),
(null, 301, 600, 1.28, 6, true),
(null, 601, 1000, 0.9, 6, true),
(null, 1001, 3000, 0.68, 6, true),
(null, 3001, 100000, 0.6, 6, true),

(null, 1, 25, 2.5, 7, true),
(null, 26, 50, 1.5, 7, true),
(null, 51, 100, 1.25, 7, true),
(null, 101, 300, 1, 7, true),
(null, 301, 600, 0.85, 7, true),
(null, 601, 1000, 0.6, 7, true),
(null, 1001, 3000, 0.45, 7, true),
(null, 3001, 100000, 0.4, 7, true),

(null, 1, 25, 10, 8, true),
(null, 26, 50, 3, 8, true),
(null, 51, 100, 2.5, 8, true),
(null, 101, 300, 2, 8, true),
(null, 301, 600, 1.7, 8, true),
(null, 601, 1000, 1.2, 8, true),
(null, 1001, 3000, 0.9, 8, true),
(null, 3001, 100000, 0.8, 8, true),

(null, 1, 25, 10, 9, true),
(null, 26, 50, 3, 9, true),
(null, 51, 100, 2.5, 9, true),
(null, 101, 300, 2, 9, true),
(null, 301, 600, 1.7, 9, true),
(null, 601, 1000, 1.2, 9, true),
(null, 1001, 3000, 0.9, 9, true),
(null, 3001, 100000, 0.8, 9, true),

(null, 1, 25, 2.5, 10, true),
(null, 26, 50, 1.5, 10, true),
(null, 51, 100, 1.25, 10, true),
(null, 101, 300, 1, 10, true),
(null, 301, 600, 0.85, 10, true),
(null, 601, 1000, 0.6, 10, true),
(null, 1001, 3000, 0.45, 10, true),
(null, 3001, 100000, 0.4, 10, true)
;


update tbpapel set precoPapel = 0.03 where idPapel=1;
update tbpapel set precoPapel = 0.04 where idPapel=2;
update tbpapel set precoPapel = 0.15 where idPapel=3;
update tbpapel set precoPapel = 0.26 where idPapel=4;
update tbpapel set precoPapel = 0.14 where idPapel=5;
update tbpapel set precoPapel = 0.26 where idPapel=6;
update tbpapel set precoPapel = 0.26 where idPapel=7;
update tbpapel set precoPapel = 0.14 where idPapel=8;
update tbpapel set precoPapel = 0.34 where idPapel=9;
update tbpapel set precoPapel = 0.03 where idPapel=10;


update tbacabamento set valorAcabamento=0.5 where idAcabamento=2;
update tbacabamento set valorAcabamento=5 where idAcabamento=3;
update tbacabamento set valorAcabamento=85 where idAcabamento=4;
