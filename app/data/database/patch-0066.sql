DROP TABLE IF EXISTS `tbip`;
CREATE TABLE `tbip`  (
  `idIp` INTEGER  NOT NULL AUTO_INCREMENT,
  `userIp` VARCHAR(255) NOT NULL,
  `remoteAddress` VARCHAR(255) DEFAULT NULL,
  `httpCfConnectingIp` VARCHAR(255) DEFAULT NULL,
  `tries` INTEGER  DEFAULT 1,
  `totalTries` INTEGER  DEFAULT 1,
  `lastTime` datetime(0) NOT NULL,
  `tbPedido_idPedido` INTEGER  DEFAULT NULL,
  PRIMARY KEY (`idIp`) USING BTREE,
  CONSTRAINT `fk_tbIp_tbPedido` FOREIGN KEY (`tbPedido_idPedido`) REFERENCES `tbpedido` (`idPedido`)
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;
