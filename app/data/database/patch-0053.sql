CREATE TABLE IF NOT EXISTS custo_acabamento (
    id INT AUTO_INCREMENT PRIMARY KEY,
    tbAcabamento_idAcabamento INT NOT NULL,
    valor FLOAT NOT NULL,
    FOREIGN KEY (tbAcabamento_idAcabamento) REFERENCES tbacabamento (idAcabamento)
) ENGINE = InnoDB;

INSERT INTO custo_acabamento(id, tbAcabamento_idAcabamento, valor) VALUES
(1, 1, 0),
(2, 2, 0.15),
(3, 3, 0.45),
(4, 4, 75),
(5, 5, 150),
(6, 6, 1.60);

CREATE TABLE IF NOT EXISTS custo_impressao (
    id INT AUTO_INCREMENT PRIMARY KEY,
    tbPapel_idPapel INT NOT NULL,
    tbCor_idCor INT NOT NULL,
    tbLado_idLado INT NOT NULL,
    valor FLOAT NOT NULL,
    FOREIGN KEY (tbPapel_idPapel) REFERENCES tbpapel (idPapel),
    FOREIGN KEY (tbCor_idCor) REFERENCES tbcor (idCor),
    FOREIGN KEY (tbLado_idLado) REFERENCES tblado (idLado)
) ENGINE = InnoDB;

INSERT INTO custo_impressao(id, tbPapel_idPapel, tbCor_idCor, tbLado_idLado, valor) VALUES
(1, 1, 1, 1, 0.048), -- Papel Sulfite A4 75g // Preto e Branco // Frente e Verso
(2, 2, 1, 1, 0.055), -- Papel Sulfite A4 90g // Preto e Branco // Frente e Verso
(3, 3, 1, 1, 0.085), -- Papel Couché A4 115g // Preto e Branco // Frente e Verso
(4, 4, 1, 1, 0.105), -- Papel Couché A4 170g // Preto e Branco // Frente e Verso
(5, 5, 1, 1, 0.09),  -- Papel Ofício 75g // Preto e Branco // Frente e Verso
(6, 6, 1, 1, 0.055), -- Papel Eco A4 75g // Preto e Branco // Frente e Verso
(7, 7, 1, 1, 0.057), -- Papel Carta 75g // Preto e Branco // Frente e Verso
(8, 8, 1, 1, 0.245), -- Papel Sulfite A3 75g // Preto e Branco // Frente e Verso
(9, 9, 1, 1, 0.23), -- Papel Couché A3 150g // Preto e Branco // Frente e Verso
(10, 10, 1, 1, 0.06), -- Papel Sulfite A5 75g // Preto e Branco // Frente e Verso

(11, 1, 2, 1, 0.268), -- Papel Sulfite A4 75g // Colorido // Frente e Verso
(12, 2, 2, 1, 0.275), -- Papel Sulfite A4 90g // Colorido // Frente e Verso
(13, 3, 2, 1, 0.305), -- Papel Couché A4 115g // Colorido // Frente e Verso
(14, 4, 2, 1, 0.325), -- Papel Couché A4 170g // Colorido // Frente e Verso
(15, 5, 2, 1, 0.53), -- Papel Ofício 75g // Colorido // Frente e Verso
(16, 6, 2, 1, 0.275), -- Papel Eco A4 75g // Colorido // Frente e Verso
(17, 7, 2, 1, 0.277), -- Papel Carta 75g // Colorido // Frente e Verso
(18, 8, 2, 1, 0.685), -- Papel Sulfite A3 75g // Colorido // Frente e Verso
(19, 9, 2, 1, 0.67), -- Papel Couché A3 150g // Colorido  // Frente e Verso
(20, 10, 2, 1, 0.28), -- Papel Sulfite A5 75g // Colorido // Frente e Verso

(21, 1, 1, 2, 0.066), -- Papel Sulfite A4 75g // Preto e Branco  // Só Frente
(22, 2, 1, 2, 0.08), -- Papel Sulfite A4 90g // Preto e Branco  // Só Frente
(23, 3, 1, 2, 0.14), -- Papel Couché A4 115g // Preto e Branco  // Só Frente
(24, 4, 1, 2, 0.18), -- Papel Couché A4 170g // Preto e Branco // Só Frente
(25, 5, 1, 2, 0.12), -- Papel Ofício 75g // Preto e Branco // Só Frente
(26, 6, 1, 2, 0.08), -- Papel Eco A4 75g // Preto e Branco // Só Frente
(27, 7, 1, 2, 0.084), -- Papel Carta 75g // Preto e Branco // Só Frente
(28, 8, 1, 2, 0.43), -- Papel Sulfite A3 75g // Preto e Branco // Só Frente
(29, 9, 1, 2, 0.4), -- Papel Couché A3 150g // Preto e Branco // Só Frente
(30, 10, 1, 2, 0.09), -- Papel Sulfite A5 75g // Preto e Branco // Só Frente

(31, 1, 2, 2, 0.286), -- Papel Sulfite A4 75g // Colorido // Só Frente
(32, 2, 2, 2, 0.3), -- Papel Sulfite A4 90g // Colorido // Só Frente
(33, 3, 2, 2, 0.36), -- Papel Couché A4 115g // Colorido // Só Frente
(34, 4, 2, 2, 0.4), -- Papel Couché A4 170g // Colorido // Só Frente
(35, 5, 2, 2, 0.56), -- Papel Ofício 75g // Colorido // Só Frente
(36, 6, 2, 2, 0.3), -- Papel Eco A4 75g // Colorido // Só Frente
(37, 7, 2, 2, 0.304), -- Papel Carta 75g // Colorido // Só Frente
(38, 8, 2, 2, 0.87), -- Papel Sulfite A3 75g // Colorido // Só Frente
(39, 9, 2, 2, 0.84), -- Papel Couché A3 150g // Colorido // Só Frente
(40, 10, 2, 2, 0.31); -- Papel Sulfite A5 75g // Colorido // Só Frente
