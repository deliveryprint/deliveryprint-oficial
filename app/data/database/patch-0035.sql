
alter table tbpedido add nomeEntrega varchar(255), add telEntrega varchar(15);

alter table tbentrega
    add column dispatch_date datetime default null,
    add column finish_date datetime default null,
    add column start_date datetime default null;

