alter table tbentrega drop foreign key fk_tbEntrega_tbFrete1;
alter table tbentrega drop column tbFrete_idFrete;

alter table tbfrete add column id_entrega int default null, add constraint fk_entrega foreign key(id_entrega) references tbentrega(idEntrega);
