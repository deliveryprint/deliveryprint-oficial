-- tbendereco
alter table tbendereco add categoria varchar(45) default null;

-- tbretirada
create table tbretirada (idRetirada int(11) primary key, prazo datetime,  valor float);

-- tbfrete
alter table tbfrete add nomeServico varchar(45) DEFAULT NULL;

-- tbentrega
alter table tbentrega add tipoEntrega int(11);
alter table tbentrega add tbRetirada_idRetirada int(11);
alter table tbentrega add constraint tbRetirada_x_tbEntrega foreign key(tbRetirada_idRetirada) references tbretirada(idRetirada);
