-- Adiciona férias de final de ano em tbferiado (2019 e começo de 2020)
insert into tbferiado (fdate) values ('2019-12-20');
insert into tbferiado (fdate) values ('2019-12-21');
insert into tbferiado (fdate) values ('2019-12-22');
insert into tbferiado (fdate) values ('2019-12-23');
insert into tbferiado (fdate) values ('2019-12-24');
insert into tbferiado (fdate) values ('2019-12-26');
insert into tbferiado (fdate) values ('2019-12-27');
insert into tbferiado (fdate) values ('2019-12-28');
insert into tbferiado (fdate) values ('2019-12-29');
insert into tbferiado (fdate) values ('2019-12-30');
insert into tbferiado (fdate) values ('2019-12-31');
insert into tbferiado (fdate) values ('2020-01-01');
insert into tbferiado (fdate) values ('2020-01-02');
