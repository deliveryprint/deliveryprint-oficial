alter table tbitem modify column borda enum('CURTA', 'LONGA', 'Vertical', 'Horizontal');
update tbitem set borda = 'Horizontal' where borda = 'LONGA';
update tbitem set borda = 'Vertical' where borda = 'CURTA';
alter table tbitem modify column borda enum('Vertical', 'Horizontal');
