alter table tbcupom
    add column commission_email varchar(255) default '',
    add column commission_percent smallint default 0;

alter table tbcupom
    add column startdate datetime default now();

alter table tbcupom
    add column total_discount float default 0,
    add column total_sales float default 0;
