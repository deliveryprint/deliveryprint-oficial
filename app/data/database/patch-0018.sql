alter table tbentrega drop foreign key tbRetirada_x_tbEntrega;
alter table tbretirada modify column idRetirada int not null auto_increment;
alter table tbentrega add constraint tbRetirada_x_tbEntrega foreign key(tbRetirada_idRetirada) references tbretirada(idRetirada);
