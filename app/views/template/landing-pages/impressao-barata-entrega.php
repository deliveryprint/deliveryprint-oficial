<img class="d-none" src="/img/logo.png" alt="DeliveryPrint impressora online">

<!--PRELOADER-->
<div id="wrap">
    <section id="main">
        <img class="main-logo" src="/img/deliveryprint_laranja.png" alt="Delivery Print" />
        <div class="container">
            <div class="row">
                <div class="col-md-6" id="col-left">
                    <h1 style="color: rgb(255, 255, 255); font-size: 44px; margin-bottom: 30px; margin-top: 0px; text-align: center">
                        <strong><span style="font-size: 44px">SERVIÇO DE IMPRESSÃO COM ENTREGA RÁPIDA</span></strong>
                    </h1>
                    <h3 class="subt" style="margin-top: 0;">
                        <span style="font-size: 20px;">
                            O que fazemos:
                            <br><br>
                        </span>
                    </h3>
                    <ul class="checked item-list-left mb-5">
                        <li id="item1" data-selector=".item-list-right li, .item-list-left li, .item-list-center li, .item-list-border li" class=""><i class="icon big-icon icon-check icofont-check-circled" style="color: rgb(225, 237, 245); font-size: 40px; opacity: 1;" data-selector=".icon"></i>
                            <h3 data-selector="h4" style="margin-bottom: -39px; font-size: 18px; color: rgb(255, 255, 255); margin-top: 0px;" class="">Impressão em preto e branco <strong>por R$0,18/página</strong>. Impressão em colorido <strong>por R$0,65/página</strong></h3>
                        </li>
                        <li id="item2" data-selector=".item-list-right li, .item-list-left li, .item-list-center li, .item-list-border li" class=""><i class="icon big-icon icon-check icofont-check-circled" style="color: rgb(225, 237, 245); font-size: 40px; opacity: 1;" data-selector=".icon"></i>
                            <h3 data-selector="h4" style="margin-bottom: -39px; font-size: 18px; color: rgb(255, 255, 255); margin-top: 0px;" class="">Faça o pedido online, <strong>nós entregamos onde estiver!</strong></h3>
                        </li>
                        <li id="item3" data-selector=".item-list-right li, .item-list-left li, .item-list-center li, .item-list-border li" class=""><i class="icon big-icon icon-check icofont-check-circled" style="color: rgb(225, 237, 245); font-size: 40px; opacity: 1;" data-selector=".icon"></i>
                            <h4 data-selector="h4" style="margin-bottom: -39px; font-size: 18px; color: rgb(255, 255, 255); margin-top: 0px;" class=""><strong>Fazemos encadernações</strong>, dobras e grampos</h4>
                        </li>
                        <li id="item4" data-selector=".item-list-right li, .item-list-left li, .item-list-center li, .item-list-border li" class=""><i class="icon big-icon icon-check icofont-check-circled" style="color: rgb(225, 237, 245); font-size: 40px; opacity: 1;" data-selector=".icon"></i>
                            <h4 data-selector="h4" style="margin-bottom: -39px; font-size: 18px; color: rgb(255, 255, 255); margin-top: 0px;" class=""><strong>Pode confiar</strong>, olhe nossa nota no Facebook e no Google, funcionamos online desde 2016!</h4>
                        </li>
                    </ul>
                </div>
                <div class="col-md-6">
                    <form id="contact-form">
                        <h3 class="text-center">
                            <strong>Peça seu orçamento abaixo! Iremos responder rapidamente, com nosso melhor preço.</strong>
                        </h3>
                        <div class="form-group">
                            <label for="name">Nome</label>
                            <input id="name" class="form-control" name="name" placeholder="Nome" type="text">
                        </div>
                        <div class="form-group">
                            <label for="email">E-mail principal</label>
                            <input id="email" class="form-control" name="email" placeholder="Deixe aqui seu e-mail" type="text">
                        </div>
                        <div class="form-group">
                            <label for="pages">Quantidade de Páginas</label>
                            <input id="pages" class="form-control" name="pages" type="text">
                        </div>
                        <div class="form-group-radio">
                            <div class="form-check">
                                <input class="form-check-input" type="radio" name="color" id="color1" value="Preto e Branco" checked>
                                <label class="form-check-label" for="color1">
                                    Preto e Branco
                                </label>
                            </div>
                            <div class="form-check">
                                <input class="form-check-input" type="radio" name="color" id="color2" value="Colorido">
                                <label class="form-check-label" for="color2">
                                    Colorido
                                </label>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="city">Cidade de entrega</label>
                            <input id="city" class="form-control" name="city" type="text">
                        </div>
                        <button id="btn-contato" type="button" class="btn btn-block btn-lg btn-success wow shake" data-wow-duration="1s" data-wow-delay="1s">
                            PEDIR ORÇAMENTO COM DESCONTO
                        </button>
                    </form>
                </div>
            </div>
        </div>
    </section>

    <section id="intro-line" style="background-color: rgb(255, 255, 255); padding-top: 80px; padding-bottom: 50px; background-image: none; border-bottom: 0px none rgb(85, 85, 85); background-repeat: repeat; background-size: auto; background-attachment: scroll;" data-selector="section">
        <div class="container">
            <div class="row align-items-center">
                <div class="col-md-3 text-center"><i class="mb-4 big-icon icon sep-bottom icofont-printer"></i>
                    <h1 class="title" data-selector="h1" style="color: rgb(255, 255, 255); font-size: 30px; margin-bottom: 60px; margin-top: 0px; background-color: rgba(0, 0, 0, 0);">
                        <span style="color: rgb(0, 0, 0);"><strong>Mais de 10.000</strong> pedidos feitos</span></h1>
                </div>
                <div class="col-md-3 text-center"><i class="mb-2 big-icon icon sep-bottom icofont-fast-delivery" style="font-size: 85px;"></i>
                    <h1 class="title" data-selector="h1" style="color: rgb(255, 255, 255); font-size: 30px; margin-bottom: 60px; margin-top: 0px; background-color: rgba(0, 0, 0, 0);">
                        <strong><span style="color: rgb(0, 0, 0);">Entregamos em todo
                                Brasil</span><strong></strong></strong></h1>
                </div>
                <div class="col-md-3 text-center"><i class="mb-4 big-icon icon sep-bottom icofont-star"></i>
                    <h1 class="title" data-selector="h1" style="color: rgb(255, 255, 255); font-size: 30px; margin-bottom: 60px; margin-top: 0px; background-color: rgba(0, 0, 0, 0);">
                        <strong><span style="color: rgb(0, 0, 0);">Avaliação 5 estrelas no Google!</span></strong></h1>
                </div>
                <div class="col-md-3 text-center">
                    <a id="scrollBtn" class="btn btn-lg" href="#contact-form">
                        <i class="icon icon-check icofont-check-circled" data-selector=".icon" style="font-size: 20px; opacity: 1;"></i>
                        Quero fazer meu orçamento
                    </a>
                </div>
            </div>
        </div>
    </section>

    <section id="section-resultado" style="background-color: rgb(255, 255, 255); padding-top: 150px; padding-bottom: 150px; border-bottom: 0px none rgb(85, 85, 85); background-repeat: repeat; background-size: auto; background-attachment: scroll; background-image: none;">
        <div class="container">
            <div class="row">
                <div class="col-md-12 text-center" style="margin-top: 2%;" data-selector=".col-md-12"><img class="screen wow bounce" alt="screen" src="img/img-55398-20170308112152.png" data-selector="img" style="border-radius: 0px; border-color: rgb(255, 255, 255); border-style: none; border-width: 1px; width: 86px; height: auto; visibility: visible; animation-name: bounce;" data-wow-duration=".4" data-wow-delay=".6"></div>
                <div class="col-md-12 text-center" data-selector=".col-md-12">
                    <h2 data-selector="h2" style="font-size: 44px; margin-top: 45px; font-family: Raleway; margin-bottom: 150px; color: rgb(34, 34, 34);" class=""><strong>OBRIGADO!<br><br><br></strong>Em breve nosso time entrará em contato e
                        mostrará nossas opções de impressão com desconto.<br><br><br>Buscaremos
                        a melhor solução para sua demanda de impressão, nós queremos que você
                        abandone as impressoras e deixe o trabalho duro com a gente.</h2>
                </div>
            </div>
            <div class="row">
                <div class="col-md-8 col-md-offset-2" data-selector=".col-md-8">
                    <div class="col-md-6" data-selector=".col-md-6"></div>
                    <div class="col-md-6" data-selector=".col-md-6"></div>
                </div>
            </div>
        </div>
    </section>
</div>
