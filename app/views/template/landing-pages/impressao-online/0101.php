<nav class="navbar navbar-expand-lg">
    <img src="/img/deliveryprint_branco.png" alt="DeliveryPrint - Impressão Online" />
</nav>

<div class="jumbotron jumbotron-fluid text-center px-2">
    <h1 class="titulo d-none d-sm-block">Impressão Online</h1>
    <h1 class="titulo d-sm-none">Impressão Barata</h1>
    <h2 class="lead d-none d-sm-block">Surpreenda com a impressão online de melhor resolução do mercado!</h2>
    <ul class="lead d-block d-sm-none m-0 p-0" style="list-style-position: inside;">
        <li>Impressão até R$0,17/página</li>
        <li>Fazemos encadernações e afins</li>
        <li>Entregamos em sua casa</li>
        <li>Parcelamos em até 3x sem juros*</li>
    </ul>
    <p class="lead d-block d-sm-none">
        Peça seu orçamento, respondemos rápido! <br>
        Impressão a laser de alta qualidade
    </p>

    <div class="d-none d-sm-block">
        <p class="cta">
            <form action="/impressao-online">
                <button class="button button--nina button--border-thin button--round-s button-prices" data-text="VER PREÇOS" type="submit">
                    <span>V</span>
                    <span>E</span>
                    <span>R</span>&nbsp;
                    <span>P</span>
                    <span>R</span>
                    <span>E</span>
                    <span>Ç</span>
                    <span>O</span>
                    <span>S</span>
                </button>
            </form>
        </p>
    </div>

    <div class="d-block d-sm-none">
        <p class="cta">
            <div>
                <button class="button button--nina button--border-thin button--round-s budget-whatsapp button-prices" data-text="ORÇAMENTO COM DESCONTO" onclick="gtag_report_conversion(); sendWhatsAppMessage();">
                    <span>O</span>
                    <span>R</span>
                    <span>Ç</span>
                    <span>A</span>
                    <span>M</span>
                    <span>E</span>
                    <span>N</span>
                    <span>T</span>
                    <span>O</span>
                </button>
            </div>
        </p>
    </div>
    <footer class="align-content-md-center display_mobile">
        <p class="telefone">
            <a href="tel:+55113807-7562">
                <i class="fas fa-phone"></i>Ligar
            </a> |
            <a href="https://api.whatsapp.com/send?phone=5511992812284&text=Fale%20agora%20com%20nosso%20suporte!">
                <i class="fab fa-whatsapp"></i>Orçamento
            </a>
        </p>

    </footer>
    <footer class="align-content-md-center display_site">
        <p class="telefone">
            <i class="fas fa-phone"></i>
            <span>(11)</span> 3807-7562 |
            <i class="fab fa-whatsapp"></i>
            <span>(11)</span> 9.9281-2284
        </p>
    </footer>
</div>
