<img class="d-none" src="/img/logo.png" alt="DeliveryPrint impressora online">

<!--PRELOADER-->
<div id="wrap">
    <section id="main">
        <div class="container">
            <div class="row">
                <div class="col-md-6" id="col-left">
                    <h1 style="color: rgb(255, 255, 255); font-size: 44px; margin-bottom: 30px; margin-top: 0px;">
                        <strong><span style="font-size: 30px;">Impressão mais barata com entrega!</span></strong>
                    </h1>
                    <h2 style="color: rgb(255, 255, 255); font-size: 26px; margin-bottom: 0px; margin-top: 20px;">
                        <strong><span style="font-size: 18px;">Você não precisa mais ter uma impressora!</span></strong>
                    </h2>
                    <h3 style="margin-top: 0;">
                        <span style="font-size: 20px;">
                            Nós criamos um serviço de impressão onde você pede pela internet e recebe onde quiser tudo pronto.
                            <br><strong>Legal, mas e o preço?</strong> Sim, vale mais a pena do que ter uma impressora e
                            <strong>irei te provar</strong> abaixo:
                            <br><br><br>
                        </span>
                    </h3>
                    <ul class="checked item-list-left">
                        <li id="item1" data-selector=".item-list-right li, .item-list-left li, .item-list-center li, .item-list-border li" class=""><i class="icon big-icon icon-check icofont-check-circled" style="color: rgb(225, 237, 245); font-size: 40px; opacity: 1;" data-selector=".icon"></i>
                            <h2 data-selector="h4" style="margin-bottom: -39px; font-size: 18px; color: rgb(255, 255, 255); margin-top: 0px;" class="">Iremos imprimir, em <strong>alta qualidade.</strong> Você não terá trabalho!</h4>
                        </li>
                        <li id="item2" data-selector=".item-list-right li, .item-list-left li, .item-list-center li, .item-list-border li" class=""><i class="icon big-icon icon-check icofont-check-circled" style="color: rgb(225, 237, 245); font-size: 40px; opacity: 1;" data-selector=".icon"></i>
                            <h3 data-selector="h4" style="margin-bottom: -39px; font-size: 18px; color: rgb(255, 255, 255); margin-top: 0px;" class=""><strong>Chega de dor de cabeça</strong> com toner, manutenção, papel e afins.</h4>
                        </li>
                        <li id="item3" data-selector=".item-list-right li, .item-list-left li, .item-list-center li, .item-list-border li" class=""><i class="icon big-icon icon-check icofont-check-circled" style="color: rgb(225, 237, 245); font-size: 40px; opacity: 1;" data-selector=".icon"></i>
                            <h4 data-selector="h4" style="margin-bottom: -39px; font-size: 18px; color: rgb(255, 255, 255); margin-top: 0px;" class=""><strong>Iremos te entregar</strong>, você não precisa nem se mexer.</h4>
                        </li>
                        <li id="item4" data-selector=".item-list-right li, .item-list-left li, .item-list-center li, .item-list-border li" class=""><i class="icon big-icon icon-check icofont-check-circled" style="color: rgb(225, 237, 245); font-size: 40px; opacity: 1;" data-selector=".icon"></i>
                            <h4 data-selector="h4" style="margin-bottom: -39px; font-size: 18px; color: rgb(255, 255, 255); margin-top: 0px;" class="">Estamos no mercado desde Nov/2016, veja nosso Facebook e <strong>avaliações no Google</strong></h4>
                        </li>
                        <li id="item5" data-selector=".item-list-right li, .item-list-left li, .item-list-center li, .item-list-border li" class=""><i class="icon big-icon icon-check icofont-check-circled" style="color: rgb(225, 237, 245); font-size: 40px; opacity: 1;" data-selector=".icon"></i>
                            <h4 data-selector="h4" style="margin-bottom: -39px; font-size: 18px; color: rgb(255, 255, 255); margin-top: 0px;" class="">Temos opções de planos mensais ou tabela de preço avulsa com <strong>desconto</strong></h4>
                        </li>
                    </ul>
                </div>
                <div class="col-md-6">
                    <form id="contact-form">
                        <h3 class="text-center">
                            <strong>Deixe seus dados e iremos entrar em contato rapidamente para
                                entender sua demanda e te oferecer nossos valores de impressão:</strong>
                        </h3>
                        <div class="form-group">
                            <label for="name">Nome</label>
                            <input id="name" class="form-control" name="name" placeholder="Nome completo" type="text">
                        </div>
                        <div class="form-group">
                            <label for="email">E-mail principal</label>
                            <input id="email" class="form-control" name="email" placeholder="Deixe aqui seu e-mail" type="text">
                        </div>
                        <div class="form-group">
                            <label for="phone">Telefone</label>
                            <input id="phone" class="form-control" name="phone" placeholder="(XX) XXXXX-XXXX" type="text">
                        </div>
                        <button id="btn-contato" type="button" class="btn btn-block btn-lg btn-success wow shake" data-wow-duration="1s" data-wow-delay="1s">
                            Pedir contato <strong>AGORA!</strong>
                        </button>
                    </form>
                </div>
            </div>
        </div>
    </section>

    <section id="section-processo" class="section-processo" style="padding-top: 100px; padding-bottom: 100px;" data-selector="section">
        <div class="w-container">
            <div class="row w-row">
                <div class="w-col w-col-6">
                    <div class="div-card">
                        <h3 class="heading-2" data-selector="h3">ETAPA 1</h3>
                        <p class="paragraph editContent" data-selector="p">Você
                            irá se cadastrar no formulário acima e nós receberemos um aviso para
                            entrar em contato com você. Com isso saberemos que tem uma demanda de
                            impressão e que precisa de um bom preço!</p>
                    </div>
                </div>
                <div class="w-col w-col-6 w-hidden-small w-hidden-tiny"></div>
            </div>
            <div class="row w-row">
                <div class="w-clearfix w-col w-col-6"><img class="guia w-hidden-small w-hidden-tiny" src="img/img-606662-20170818141857.jpg" data-selector="img" width="150"></div>
                <div class="w-col w-col-6">
                    <div class="div-card">
                        <h3 class="heading-2" data-selector="h3">ETAPA 2</h3>
                        <p class="paragraph editContent" data-selector="p">Um
                            de nossos Experts irá entrar em contato com você para entender sua
                            demanda e já iremos te passar de antemão 1 opção de pacote mensal e uma
                            opção de tabela avulsa com valores por página reduzidos.</p>
                    </div>
                </div>
            </div>
            <div class="row w-row">
                <div class="w-col w-col-6">
                    <div class="div-card">
                        <h3 class="heading-2" data-selector="h3">ETAPA FINAL</h3>
                        <p class="paragraph editContent" data-selector="p">Juntos
                            iremos elaborar uma solução adequada para sua demanda de impressão. Nós
                            queremos que você tenha o melhor custo benefício em imprimir com nossa
                            empresa.</p>
                    </div>
                </div>
                <div class="w-clearfix w-col w-col-6"><img class="guia guia-direita w-hidden-small w-hidden-tiny" src="img/img-606662-20170818141857.jpg" data-selector="img" width="150"></div>
            </div><a id="scrollBtn" class="btn btn-lg btn-primary button-authority btn-center w--current" href="#contact-form" style="white-space: normal; margin-top: 24px; margin-right: auto; margin-left: auto; color: rgb(255, 255, 255); border: 0px none rgb(255, 255, 255); border-radius: 25px; font-size: 16px; background-color: rgb(0, 255, 0);"><span style="font-size: 11px;"><strong><span style="font-size: 14px;">QUERO
                            TESTAR!</span></strong></span></a>
        </div>
    </section>

    <section id="section-resultado" style="background-color: rgb(255, 255, 255); padding-top: 150px; padding-bottom: 150px; border-bottom: 0px none rgb(85, 85, 85); background-repeat: repeat; background-size: auto; background-attachment: scroll; background-image: none;">
        <div class="container">
            <div class="row">
                <div class="col-md-12 text-center" style="margin-top: 2%;" data-selector=".col-md-12"><img class="screen wow bounce" alt="screen" src="img/img-55398-20170308112152.png" data-selector="img" style="border-radius: 0px; border-color: rgb(255, 255, 255); border-style: none; border-width: 1px; width: 86px; height: auto; visibility: visible; animation-name: bounce;" data-wow-duration=".4" data-wow-delay=".6"></div>
                <div class="col-md-12 text-center" data-selector=".col-md-12">
                    <h2 data-selector="h2" style="font-size: 44px; margin-top: 45px; font-family: Raleway; margin-bottom: 150px; color: rgb(34, 34, 34);" class=""><strong>OBRIGADO!<br><br><br></strong>Em breve nosso time entrará em contato e
                        mostrará nossas opções de impressão com desconto.<br><br><br>Buscaremos
                        a melhor solução para sua demanda de impressão, nós queremos que você
                        abandone as impressoras e deixe o trabalho duro com a gente.</h2>
                </div>
            </div>
            <div class="row">
                <div class="col-md-8 col-md-offset-2" data-selector=".col-md-8">
                    <div class="col-md-6" data-selector=".col-md-6"></div>
                    <div class="col-md-6" data-selector=".col-md-6"></div>
                </div>
            </div>
        </div>
    </section>
</div>
