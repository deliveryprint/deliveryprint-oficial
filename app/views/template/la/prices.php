<div id="pricesApp">

    <h2 class="text-orange mt-5">
        <i class="fa fa-usd mr-3"></i> Atualização dos Preços
    </h2>

    <ul class="nav nav-tabs">
        <li class="nav-item">
            <a class="nav-link active" data-toggle="tab" href="#form" role="tab">Formulário</a>
        </li>
        <li class="nav-item">
            <a class="nav-link" data-toggle="tab" href="#table" role="tab">CSV</a>
        </li>
    </ul>
    <div class="tab-content">
        <div class="tab-pane fade show active" id="form" role="tabpanel">
            <prices-form></prices-form>
        </div>
        <div class="tab-pane fade" id="table" role="tabpanel">
            <csv-table></csv-table>
        </div>
    </div>

</div>
