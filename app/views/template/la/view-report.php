<div class="container">
    <div class="row" id="filters">
        <div class="col-md-9 mt-3">
            <h3 class="default-text text-orange">
                <i class="fa fa-mobile mr-3"></i>Lista de Respostas de SMS
            </h3>
        </div>
        <div class="col-md-3">
            <div class="clearfix filter-by-date date-fixed">
                <div class="col-md-12 p-0">
                    <div class="data-filter-title">
                        <i class="fa fa-calendar"></i>
                        <b>
                            Período
                        </b>
                    </div>
                </div>
                <div class="date-text" data-toggle="collapse" data-target="#inputsDiv">
                    {{startPeriod | formatDatePt}} até {{endPeriod | formatDatePt}}
                    <i class="fa fa-angle-down collapsed" data-toggle="collapse" data-target="#inputsDiv"></i>
                </div>
                <div class="collapse small-padding" id="inputsDiv">
                    <div class="type-div">
                        <span class="type">Tipo</span>
                        <div class="btn-group">
                            <button type="button" class="text-sm btn btn-grey-outlined dropdown-toggle default-text text-orange"
                                data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                {{periodType}}
                            </button>
                            <ul class="dropdown-menu">
                                <li>
                                    <a href="#" v-on:click="setPeriodType('Pagamento')">Data do Pagamento</a>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class="period">Período</div>
                    <div class="input-period">
                        <input name="start-period" class="form-control" v-mask="'##/##'" v-model="startPeriod">
                        <span class="input-divider"> a </span>
                        <input name="end-period" class="form-control" v-mask="'##/##'" v-model="endPeriod">
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="row" id="reportsTable">
        <div class="row">
            <div class="col-12">
                <p class="text-center">Quantidade de Páginas compradas neste Período</p>
            </div>
            <div class="col-3">
                <p class='text-center'>Tipo</p>
                <p v-for="(type, id) in paper_type" :key="id">
                    {{type}}
                </p>
            </div>
            <div class="col-3">
                <p class='text-center'>Preto e Branco</p>
                <p class='text-center'>{{colors.pb_sulfite_a4_75g}}</p>
                <p class='text-center'>{{colors.pb_sulfite_a4_90g}}</p>
                <p class='text-center'>{{colors.pb_couche_a4_115g}}</p>
                <p class='text-center'>{{colors.pb_couche_a4_170g}}</p>
                <p class='text-center'>{{colors.pb_oficio_75g}}</p>
                <p class='text-center'>{{colors.pb_eco_a4_75g}}</p>
                <p class='text-center'>{{colors.pb_carta_75g}}</p>
                <p class='text-center'>{{colors.pb_sulfite_a3_75g}}</p>
                <p class='text-center'>{{colors.pb_couche_a3_150g}}</p>
                <p class='text-center'>{{colors.pb_sulfite_a5_75g}}</p>
            </div>
            <div class="col-3">
                <p class='text-center'>Colorido</p>
                <p class='text-center'>{{colors.c_sulfite_a4_75g}}</p>
                <p class='text-center'>{{colors.c_sulfite_a4_90g}}</p>
                <p class='text-center'>{{colors.c_couche_a4_115g}}</p>
                <p class='text-center'>{{colors.c_couche_a4_170g}}</p>
                <p class='text-center'>{{colors.c_oficio_75g}}</p>
                <p class='text-center'>{{colors.c_eco_a4_75g}}</p>
                <p class='text-center'>{{colors.c_carta_75g}}</p>
                <p class='text-center'>{{colors.c_sulfite_a3_75g}}</p>
                <p class='text-center'>{{colors.c_couche_a3_150g}}</p>
                <p class='text-center'>{{colors.c_sulfite_a5_75g}}</p>
            </div>
            <div class="col-3">
                <p class='text-center'>Total de Páginas</p>
                <p class='text-center'>{{colors.pb_sulfite_a4_75g + colors.c_sulfite_a4_75g}}</p>
                <p class='text-center'>{{colors.pb_sulfite_a4_90g + colors.c_sulfite_a4_90g}}</p>
                <p class='text-center'>{{colors.pb_couche_a4_115g + colors.c_couche_a4_115g}}</p>
                <p class='text-center'>{{colors.pb_couche_a4_170g + colors.c_couche_a4_170g}}</p>
                <p class='text-center'>{{colors.pb_oficio_75g + colors.c_oficio_75g}}</p>
                <p class='text-center'>{{colors.pb_eco_a4_75g + colors.c_eco_a4_75g}}</p>
                <p class='text-center'>{{colors.pb_carta_75g + colors.c_carta_75g}}</p>
                <p class='text-center'>{{colors.pb_sulfite_a3_75g + colors.c_sulfite_a3_75g}}</p>
                <p class='text-center'>{{colors.pb_couche_a3_150g + colors.c_couche_a3_150g}}</p>
                <p class='text-center'>{{colors.pb_sulfite_a5_75g + colors.c_sulfite_a5_75g}}</p>
            </div>
        </div>
        <div class="row mt-3">
            <div class="col-6">
                <div class="row">
                    <div class="col-12">
                        <p class="text-center">Quantidade de Acabamentos compradas neste período</p>
                    </div>
                    <div class="col-6">
                        <p class="text-center">Tipo</p>
                        <p v-for="(acab, id) in finish_type" :key="id">
                            {{acab}}
                        </p>
                        <p>Total</p>
                    </div>
                    <div class="col-6">
                        <p class="text-center">Qtd</p>
                        <p class='text-center'>{{finishes.solta}}</p>
                        <p class='text-center'>{{finishes.grampeado}}</p>
                        <p class='text-center'>{{finishes.espiral}}</p>
                        <p class='text-center'>{{finishes.capa_dura}}</p>
                        <p class='text-center'>{{finishes.capa_dura_a3}}</p>
                        <p class='text-center'>{{finishes.wire_o}}</p>
                        <p class='text-center'>{{finishes.solta + finishes.grampeado + finishes.espiral +
                            finishes.capa_dura + finishes.capa_dura_a3 + finishes.wire_o}}</p>
                    </div>
                </div>
            </div>
            <div class="col-6">
                <div class="row">
                    <div class="col-12">
                        <p class="text-center">Resumo do Periodo</p>
                    </div>
                    <div class="col-4">
                        <p class="text-center">Tipo</p>
                        <p>Preto e Branco</p>
                        <p>Colorido</p>
                        <p>Acabamentos</p>
                        <p>Total Faturado</p>
                    </div>
                    <div class="col-4">
                        <p class="text-center">Qtd de Pag Comprada</p>
                        <p class="text-center">{{period_summary.black_white.num_pages}}</p>
                        <p class="text-center">{{period_summary.colorful.num_pages}}</p>
                        <p class="text-center">{{period_summary.finishes.total}}</p>

                    </div>
                    <div class="col-4">
                        <p class="text-center">Valor Total</p>
                        <p class="text-center">R$ {{period_summary.black_white.value | moneyFormatter}}</p>
                        <p class="text-center">R$ {{period_summary.colorful.value | moneyFormatter}}</p>
                        <p class="text-center">R$ {{period_summary.finishes.value | moneyFormatter}}</p>
                        <p class="text-center">R$ {{period_summary.total_value | moneyFormatter}}</p>
                    </div>
                </div>
            </div>
        </div>
        <div class="row mt-5">
            <div class="col-6">
                <div class="row">
                    <div class="col-12">
                        <p class="text-center">Média de Páginas Compradas por dia</p>
                    </div>
                    <div class="col-6">
                        <p class="text-center">Tipo</p>
                        <p>Preto e Branco</p>
                        <p>Colorido</p>
                        <p>Total</p>
                    </div>
                    <div class="col-6">
                        <p class="text-center">Qtd</p>
                        <p class="text-center">{{average_pages.average_black_white}}</p>
                        <p class="text-center">{{average_pages.average_colorful}}</p>
                        <p class="text-center">{{average_pages.average_overall}}</p>
                    </div>
                </div>
            </div>
            <div class="col-6">
                <div class="row">
                    <div class="col-12">
                        <p class="text-center">Ticket Médio de Faturamento diário</p>
                    </div>
                    <div class="col-4">
                        <p class="text-center">Tipo</p>
                        <p>Faturamento</p>
                    </div>
                    <div class="col-4">
                        <p class="text-center">Qtd de Ped no Periodo</p>
                        <p class="text-center">{{average_ticket.total_orders}}</p>
                    </div>
                    <div class="col-4">
                        <p class="text-center">Ticket Médio</p>
                        <p class="text-center">R$ {{average_ticket.billing | moneyFormatter}}</p>
                    </div>
                </div>
            </div>
        </div>
        <div class="row mt-3">
            <div class="col-6">
                <div class="row">
                    <div class="col-12">
                        <p class="text-center">Faturamento x Cancelado</p>
                    </div>
                    <div class="col-6">
                        <p class="text-center">Tipo</p>
                        <p>Compras Realizadas</p>
                        <p>Pedidos Cancelados</p>
                    </div>
                    <div class="col-6">
                        <p class="text-center">Valor</p>
                        <p class="text-center">R$ {{paid_value | moneyFormatter}}</p>
                        <p class="text-center">R$ {{canceled_value | moneyFormatter}}</p>
                    </div>
                </div>
            </div>
            <div class="col-6">
                <div class="row">
                    <div class="col-12">
                        <p class="text-center">Resultado do Período</p>
                    </div>
                    <div class="col-4">
                        <p class="text-center">Faturado</p>
                        <p class="text-center">R$ {{billing_result.billing | moneyFormatter}}</p>
                    </div>
                    <div class="col-4">
                        <p class="text-center">Custo</p>
                        <p class="text-center">R$ {{billing_result.costs | moneyFormatter}}</p>
                    </div>
                    <div class="col-4">
                        <p class="text-center">Resultado</p>
                        <p class="text-center">R$ {{billing_result.result | moneyFormatter}}</p>
                    </div>
                </div>
            </div>
        </div>
        <div class="row mt-3">
            <div class="col-12">
                <div class="row mb-3">
                    <p class="text-center">Relatório por Faixas</p>
                </div>
                <div class="row">
                    <table class="col-12 text-center table">
                        <thead class="font-weight-bold">
                            <tr>
                                <td>Id Faixa</td>
                                <td>Tipo de Papel</td>
                                <td>Cor</td>
                                <td>Qtd de Pág</td>
                                <td>Nº Itens</td>
                                <td>Faturamento</td>
                                <td>Custo</td>
                                <td>Resultado</td>
                            </tr>
                        </thead>
                        <tbody>
                            <tr v-for="(faixa, id) in price_range" :key="id">
                                <td class="text-center">{{id}}</td>
                                <td class="text-center">{{faixa.paper_type}}</td>
                                <td class="text-center">{{faixa.color}}</td>
                                <td class="text-center">{{faixa.num_pages}}</td>
                                <td class="text-center">{{faixa.num_items}}</td>
                                <td class="text-center">R$ {{faixa.items_price | moneyFormatter}}</td>
                                <td class="text-center">R$ {{faixa.items_cost | moneyFormatter}}</td>
                                <td class="text-center">R$ {{faixa.result | moneyFormatter}}</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
