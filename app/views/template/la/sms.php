<div class="container">
    <div class="row" id="filters">
        <div class="col-md-6 mt-3">
            <h3 class="default-text text-orange">
                <i class="fa fa-mobile mr-3"></i>Lista de Respostas de SMS
            </h3>
        </div>
        <div class="col-md-2 offset-md-1 d-flex alignt-items-center justify-content-center">
            <p class="text-orange btn" @click="downloadCsv()">
                <i class="fa fa-floppy-o mr-2"></i>Exportar
            </p>
        </div>
        <div class="col-md-3">
            <div class="clearfix filter-by-date date-fixed">
                <div class="col-md-12 p-0">
                    <div class="data-filter-title">
                        <i class="fa fa-calendar"></i>
                        <b>
                            Período
                        </b>
                    </div>
                </div>
                <div class="date-text" data-toggle="collapse" data-target="#inputsDiv">
                    {{startPeriod | formatDatePt}} até {{endPeriod | formatDatePt}}
                    <i class="fa fa-angle-down collapsed" data-toggle="collapse" data-target="#inputsDiv"></i>
                </div>
                <div class="collapse small-padding" id="inputsDiv">
                    <div class="type-div">
                        <span class="type">Tipo</span>
                        <div class="btn-group">
                            <button type="button" class="text-sm btn btn-grey-outlined dropdown-toggle default-text text-orange"
                                data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                {{periodType}}
                            </button>
                            <ul class="dropdown-menu">
                                <li>
                                    <a href="#" v-on:click="setPeriodType('Envio')">Envio do SMS</a>
                                </li>
                                <li>
                                    <a href="#" v-on:click="setPeriodType('Resposta')">Recebimento da Resposta</a>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class="period">Período</div>
                    <div class="input-period">
                        <input name="start-period" class="form-control" v-mask="'##/##'" v-model="startPeriod">
                        <span class="input-divider"> a </span>
                        <input name="end-period" class="form-control" v-mask="'##/##'" v-model="endPeriod">
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="row mt-5" id="smsTable">
        <table class="col-12 text-center">
            <thead class="font-weight-bold">
                <tr>
                    <td>Id do Pedido</td>
                    <td>Nome do Cliente</td>
                    <td>Nº do Celular</td>
                    <td>Data do Envio</td>
                    <td>Data da Resposta</td>
                    <td>Resposta Recebida</td>
                </tr>
            </thead>
            <tbody>
                <tr v-for="(sms, id) in listSms" :key="id">
                    <td class="text-center">{{sms.id_pedido}}</td>
                    <td class="text-center">{{checkField(sms.nomeCliente)}}</td>
                    <td class="text-center">{{sms.celular}}</td>
                    <td class="text-center">{{sms.data_envio}}</td>
                    <td class="text-center">{{sms.data_resposta}}</td>
                    <td class="text-center">{{sms.resposta}}</td>
                </tr>
            </tbody>
        </table>
    </div>
</div>
