<div id="modalEmailsApp">

    <h2 class="text-orange mt-5">
        <i class="fa fa-envelope mr-3"></i> Lista de Emails do Modal de 10% OFF
    </h2>

    <div class="row" id="filters">
        <div class="col-md-5"></div>
        <div class="col-md-4">
            <a href="#" v-on:click="createEmailsCsv()" v-if="activeTab == 1">
                <i class="fa fa-save"></i> Exportar
            </a>
            <a href="#" v-on:click="createShoppingCartCsv()" v-else>
                <i class="fa fa-save"></i> Exportar
            </a>
        </div>
        <div class="col-md-2 offset-md-1">
            <div id="mainFilters" class="clearfix filter-by-date date-fixed">
                <div class="col-md-12 p-0">
                    <div class="data-filter-title">
                        <i class="fa fa-calendar"></i>
                        <b>
                            Período
                        </b>
                    </div>
                </div>
                <div class="date-text" data-toggle="collapse" data-target="#inputsDiv">
                    {{startPeriod | formatDatePt}} até {{endPeriod | formatDatePt}}
                    <i class="fa fa-angle-down collapsed" data-toggle="collapse" data-target="#inputsDiv"></i>
                </div>
                <div class="collapse small-padding" id="inputsDiv">
                    <div class="period">Período</div>
                    <div class="input-period">
                        <input name="start-period" class="form-control" v-mask="'##/##'" v-model="startPeriod">
                        <span class="input-divider"> a </span>
                        <input name="end-period" class="form-control" v-mask="'##/##'" v-model="endPeriod">
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div id="emailsTable" class="col-md-12 p-0">


        <ul class="nav nav-tabs" role="tablist">
            <li class="nav-item">
                <a class="nav-link active" data-toggle="tab" href="#impressao-online" role="tab" @click="setActiveTab(1)">Impressão Online</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" data-toggle="tab" href="#carrinho" role="tab" @click="setActiveTab(2)">Carrinho</a>
            </li>
        </ul>
        <div class="tab-content">
            <div class="tab-pane fade show active" id="impressao-online" role="tabpanel">
                <div class="table-all-data bs-component live-less-editor-hovercontainer pt-0">
                    <table class="table table-striped table-hover">
                        <thead>
                            <tr class="text-center">
                                <th class="sticky-header">Dia da Inscrição</th>
                                <th class="sticky-header">Email</th>
                                <th class="sticky-header">Comprou?</th>
                                <th class="sticky-header">Valor do Pedido</th>
                            </tr>
                        </thead>
                        <tbody class="text-center">
                            <template v-for="(email, idx) in emails" v-if="email.origem == 'impressao-online'">
                                <tr>
                                    <td>{{ email.dataInscricao | datetimeFormatter }}</td>
                                    <td>{{ email.email }}</td>
                                    <td>{{ email.statusItem != null ? email.statusItem : "Não" }}</td>
                                    <td>{{ email.valorItem | moneyFormatter }}</td>
                                </tr>
                            </template>
                        </tbody>
                    </table>
                </div>
            </div>
            <div class="tab-pane fade" id="carrinho" role="tabpanel">
                <div class="table-all-data bs-component live-less-editor-hovercontainer pt-0">
                    <table class="table table-striped table-hover">
                        <thead>
                            <tr class="text-center">
                                <th class="sticky-header">Dia da Inscrição</th>
                                <th class="sticky-header">Email</th>
                                <th class="sticky-header">Comprou?</th>
                                <th class="sticky-header">Valor do Pedido</th>
                                <th class="sticky-header">Desconto</th>
                                <th class="sticky-header">Valor Total</th>
                            </tr>
                        </thead>
                        <tbody class="text-center">
                            <template v-for="(email, idx) in shoppingCartEmails">
                                <tr>
                                    <td>{{ email.dataInscricao | datetimeFormatter }}</td>
                                    <td>{{ email.email }}</td>
                                    <td>{{ email.statusPedido != null ? email.statusPedido : "Não" }}</td>
                                    <td>{{ email.valorPedido | moneyFormatter }}</td>
                                    <td>{{ email.discount | moneyFormatter }}</td>
                                    <td>{{ email.order_value | moneyFormatter }}</td>
                                </tr>
                            </template>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
