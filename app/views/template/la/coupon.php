<div id="couponApp">

    <h2 class="text-orange mt-5">
        <i class="fa fa-plus mr-3"></i> Cadastrar Cupom
    </h2>

    <form action="" method="post" enctype="multipart/form-data" class="addCoupon" id="addCoupon">

        <div class="row text-sm">
            <div class="col-8">
                <div class="row">

                    <div class="col-12">
                        <div class="row">
                            <label class="text-orange font-weight-bold col-3"> Digite o Código do Cupom
                                <br />
                                <input class="bg-transparent border-solid border-sm border-radius-md" type="text" placeholder="Ex.: #DP2018" v-model="code" />
                            </label>
                            <label class="text-orange font-weight-bold col-3"> Validade
                                <br />
                                <input class="form-control col-4 p-0" type="text" v-mask="'##/##'" placeholder="DD/MM" v-model="startdate" />
                                <input class="form-control col-4 p-0" type="text" v-mask="'##/##'" placeholder="DD/MM" v-model="expiration_date" />
                            </label>
                            <label class="text-orange font-weight-bold col-3"> Limite de uso geral
                                <br />
                                <input class="bg-transparent border-solid border-sm border-radius-md" v-mask="['#', '##', '###', '####', '#####', '######']" type="tel" placeholder="Ex.: 10" v-model="cupom_limit" />
                            </label>
                            <label class="text-orange font-weight-bold col-3"> Limite de uso por usuário
                                <br />
                                <input class="bg-transparent border-solid border-sm border-radius-md" v-mask="['#', '##', '###', '####', '#####', '######']" type="tel" placeholder="Ex.: 1" v-model="cupom_user_limit" />
                            </label>
                        </div>
                    </div>
                    <div class="col-12">

                        <div class="row">
                            <label class="text-orange font-weight-bold col-3"> Desconto aplicado
                                <br />
                                <input class="bg-transparent border-solid border-sm border-radius-md" type="tel" placeholder="" v-mask="['#', '##', '###', '####', '#####']" v-model="value" />
                            </label>
                            <label class="text-orange font-weight-bold px-0 col-3"> Tipo
                                <br />
                                <label class="radio-btn mx-0 text-grey font-weight-lighter">
                                    <input type="radio" value="ABS" name="type" v-model="type"> Em R$
                                    <span class="checkmark"></span>
                                </label>
                                <label class="radio-btn mx-0 text-grey font-weight-lighter">
                                    <input type="radio" name="type" value="PERCENT" v-model="type"> Em %
                                    <span class="checkmark"></span>
                                </label>
                            </label>
                            <label class="text-orange font-weight-bold col-4"> E-mail
                                <br />
                                <input class="bg-transparent border-solid border-sm border-radius-md" type="text" placeholder="" v-model="email" />
                            </label>
                            <label class="text-orange font-weight-bold col-2"> Comissão (em R$)
                                <br />
                                <input class="bg-transparent border-solid border-sm border-radius-md" type="tel" placeholder="" v-mask="['#', '##', '###', '####', '#####']" v-model="commission_percent" />
                            </label>
                        </div>

                    </div>
                </div>
            </div>
            <div class="col-4">
                <label class="text-orange font-weight-bold"> Observações / Comentário
                    <br />
                    <textarea class=" border-solid border-sm border-radius-md bg-transparent" v-model="comment"></textarea>
            </div>

        </div>

        <div class="row">
            <div class="col-12 d-flex justify-content-end">
                <button type="button" class="btn-b5 btn-orange border-0 border-radius-md" v-on:click="saveCoupon()">CADASTRAR</button>
            </div>
        </div>

    </form>

    <h2 class="text-orange mt-5">
        <i class="fa fa-tags mr-3"></i> Cupons de Desconto
    </h2>

    <div class="row  text-sm" id="coupon-filter">
        <div class="col-6">
            <div class="row text-orange">
                <label class="col-3"> Status
                    <button type="button" class="text-sm text-grey btn py-0 bg-transparent  dropdown-toggle default-text text-orange text-left" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <span v-if="status == 1">Publicado</span>
                        <span v-else-if="status == 0">Não Publicado</span>
                        <span v-else>Ambos</span>
                    </button>
                    <ul class="dropdown-menu">
                        <li>
                            <a href="#" v-on:click="status = 1">Publicado</a>
                        </li>
                        <li>
                            <a href="#" v-on:click="status = 0">Não Publicado</a>
                        </li>
                        <li>
                            <a href="#" v-on:click="status = 2">Ambos</a>
                        </li>
                    </ul>
                </label>
                <label class="col-3">Código / E-mail
                    <input class="bg-transparent border-solid border-sm border-radius-md" v-model="codeOrEmail" type="text">
                </label>
                <label class="col-3"> Contém E-mail?
                    <button type="button" class="text-sm text-grey btn py-0 bg-transparent  dropdown-toggle default-text text-orange text-left" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <span v-if="hasEmail == 1">Sim</span>
                        <span v-else-if="hasEmail == 0">Não</span>
                        <span v-else>Sim/Não</span>
                    </button>
                    <ul class="dropdown-menu">
                        <li>
                            <a href="#" v-on:click="hasEmail = 2">Sim/Não</a>
                        </li>
                        <li>
                            <a href="#" v-on:click="hasEmail = 1">Sim</a>
                        </li>
                        <li>
                            <a href="#" v-on:click="hasEmail = 0">Não</a>
                        </li>
                    </ul>
                </label>
                <label class="col-2">
                    <br />
                    <a href="#" v-on:click="downloadCsv()">
                        <i class="fa fa-save"></i> Exportar
                    </a>
                </label>
            </div>
        </div>
        <div class='col-6 text-right'>
            <br />
            <a href="#" class="text-grey font-weight-bold" data-toggle="modal" data-target="#remove-mdl">- EXCLUIR SELECIONADOS </a>
        </div>

    </div>
    <!-- END FILTER -->

    <!-- COUPON-TABLE -->
    <div class="row my-5" id="coupon-table">
        <table class="col-12 text-center">
            <thead class="font-weight-bold">
                <tr>
                    <td></td>
                    <td class="pl-3">
                        Status
                    </td>
                    <td>Código</td>
                    <td>Desconto</td>
                    <td>Validade</td>
                    <td>Uso</td>
                    <td class="bg-yel py-3">Valor total de desconto</td>
                    <td class="bg-greeny px-5">Vendas</td>
                    <td>Atribuido à:</td>
                    <td>Observação</td>
                </tr>
            </thead>
            <tbody>
                <template v-for="(c, id) in coupons">
                    <tr>
                        <td class="bg-transparent">
                            <label class="checkbox-btn">
                                <input type="checkbox" name="che" class="table-check" v-on:click="toggleLine(c.idDesc)" v-bind:checked="c.idDesc in expanded">
                                <span class="checkmark check-success"></span>
                            </label>
                        </td>
                        <td class="pl-3">
                            <i class="fa" v-bind:class="{'fa-pause-circle': c.status == 0, 'fa-check-circle': c.status == 1}" v-on:click="changeStatus(id, c.status)"></i>
                        </td>
                        <td>{{c.codigo}}</td>
                        <td v-if="c.type === 'ABS'">{{c.value | moneyFormatterWithZeroAndCurrency }}</td>
                        <td v-else>{{c.value | percentFormatter}}</td>
                        <td>de {{c.startdate | dateMFormatter }} até {{c.validade | dateMFormatter}}</td>
                        <td>{{c.cupom_counter | formatNumber }} de {{c.cupom_limit}}</td>
                        <td class="bg-yel">{{c.total_discount_items | moneyFormatterWithZeroAndCurrency}}</td>
                        <td class="bg-greeny">{{c.sales_delivery | moneyFormatterWithZeroAndCurrency}}</td>
                        <td>{{c.commission_email}}</td>
                        <td>{{c.comment}}</td>
                    </tr>
                    <template v-if="c.idDesc in expanded">
                        <tr class="">
                            <td class="bg-transparent"></td>
                            <td colspan="12">
                                <div class="my-4 row font-weight-bold justify-content-between align-content-center">

                                    <div class="col-3 row border-right border-solid">
                                        <div class="col-6">
                                            <label class="text-orange">Total de páginas
                                                <br />
                                                <span class="mt-2 default-text h2 font-weight-lighter bg-orange text-white">
                                                    {{expanded[c.idDesc].total_pages}}
                                                </span>
                                            </label>
                                        </div>
                                        <div class="col-6">
                                            <label class="text-orange">Total de vendas
                                                <br />
                                                <span class="mt-2 text-lg font-weight-lighter  bg-green text-white">
                                                    {{ c.total_sales | moneyFormatterWithZeroAndCurrency }}
                                                </span>
                                                <span class=" font-weight-lighter ">
                                                    <br />(sem frete)
                                                    <br />Comissão: {{ ( (c.total_sales * c.commission_percent) / 100) | moneyFormatterWithZeroAndCurrency
                                                    }}
                                                </span>
                                            </label>
                                        </div>
                                    </div>

                                    <div class="col-6 row border-right border-solid">
                                        <div class="col-3">
                                            <label class="text-orange">Cor
                                                <br />
                                                <span class="mt-2 text-grey font-weight-lighter">
                                                    <b>P&amp;B</b> - {{expanded[c.idDesc].pb}}</span>
                                                <br />
                                                <span class="text-grey font-weight-lighter">
                                                    <b>Colorido</b> - {{expanded[c.idDesc].color}}</span>
                                            </label>
                                        </div>
                                        <div class="col-3">
                                            <label class="text-orange">Acabamento
                                                <br>
                                                <span class="mt-2 text-grey font-weight-lighter" v-if="expanded[c.idDesc].espiral > 0">
                                                    <b>Espiral</b> - {{expanded[c.idDesc].espiral}}
                                                    <br>
                                                </span>
                                                <span class="text-grey font-weight-lighter" v-if="expanded[c.idDesc].wire_o > 0">
                                                    <b>Wire-o</b> - {{expanded[c.idDesc].wire_o}}
                                                    <br>
                                                </span>
                                                <span class="text-grey font-weight-lighter" v-if="expanded[c.idDesc].grampeado > 0">
                                                    <b>Grampeado</b> - {{expanded[c.idDesc].grampeado}}
                                                    <br>
                                                </span>
                                                <span class="text-grey font-weight-lighter" v-if="expanded[c.idDesc].folha_solta > 0">
                                                    <b>Folha Solta</b> - {{expanded[c.idDesc].folha_solta}}
                                                    <br>
                                                </span>
                                                <span class="text-grey font-weight-lighter" v-if="expanded[c.idDesc].capa_dura_a3 > 0">
                                                    <b>Capa Dura A3</b> - {{expanded[c.idDesc].capa_dura_a3}}
                                                    <br>
                                                </span>
                                                <span class="text-grey font-weight-lighter" v-if="expanded[c.idDesc].capa_dura_a4 > 0">
                                                    <b>Capa Dura A4</b> - {{expanded[c.idDesc].capa_dura_a4}}
                                                    <br>
                                                </span>
                                            </label>
                                        </div>
                                        <div class="col-4">
                                            <label class="text-orange">Cidade
                                                <br />
                                                <span class="mt-2 text-grey font-weight-lighter"> {{expanded[c.idDesc].cities.join(', ')}}</span>
                                            </label>
                                        </div>
                                        <div class="col-2">
                                            <label class="text-orange">Estado
                                                <br />
                                                <span class="mt-2 text-grey font-weight-lighter"> {{expanded[c.idDesc].state.join(', ')}}</span>
                                            </label>
                                        </div>
                                    </div>

                                    <div class="col-3 row">
                                        <div class="col-6">
                                            <label class="text-orange">Primeira venda
                                                <br />
                                                <span class="mt-2 text-grey text-hg font-weight-lighter">
                                                    {{expanded[c.idDesc].first_use | dateFormatterY2}}
                                                </span>
                                            </label>
                                        </div>
                                        <div class="col-6">
                                            <label class="text-orange">Última venda
                                                <br />
                                                <span class="mt-2 text-grey text-hg font-weight-lighter">
                                                    {{expanded[c.idDesc].last_use | dateFormatterY2}}
                                                </span>
                                            </label>
                                        </div>
                                    </div>
                                </div>
                            </td>
                        </tr>
                    </template>
                </template>

            </tbody>

        </table>

        <div class="modal fade" id="coupon-status" tabindex="-1" role="dialog" aria-labelledby="modalAcessoTitle" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered" role="document">
                <div class="modal-content border-0 border-radius-sm">
                    <div class="modal-header border-0">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">

                        <div class="row">
                            <div class="col-12">

                                <form>
                                    <label class="col-12 text-orange font-weight-bold col-3"> Validade
                                        <br />
                                        <input class="form-control col-4" type="text" v-mask="'##/##'" v-model="startdate">
                                        <input class="form-control col-4" type="text" v-mask="'##/##'" v-model="validade">
                                    </label>
                                    <br />

                                    <button class="btn btn-b3 btn-orange-outlined border-0 close order-1" type="button" data-dismiss="modal">CANCELAR</button>
                                    <button class="btn btn-b3 btn-orange" type="button" v-on:click="activeCoupon()">CONFIRMAR</button>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- MODAL -->
        <div class="modal fade" id="remove-mdl" tabindex="-1" role="dialog" aria-labelledby="modalAcessoTitle" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered" role="document">
                <div class="modal-content border-0 border-radius-sm">
                    <div class="modal-header border-0">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">

                        <div class="row">
                            <div class="col-12 text-left">
                                <span class="text-orange text-grey font-weight-lighter" v-if="currentSelected.length">
                                    Deseja excluir os cupons selecinoados?
                                </span>
                                <span class="text-orange text-grey font-weight-lighter" v-else>
                                    Selecione cupons primeiro!
                                </span>
                                <br />
                                <span class="text-orange text-grey font-weight-bold" v-for="(c, index) in expanded">
                                    #{{getCouponName(index)}}
                                    <br />
                                </span>

                            </div>
                        </div>
                        <div class="row justify-content-end" v-if="currentSelected.length">
                            <div class="col-3 order-2">
                                <button class="btn btn-b3 btn-block btn-orange-outlined border-0 close px-4" type="button" v-on:click="removeCoupons()" data-dismiss="modal">
                                    SIM
                                </button>
                            </div>
                            <div class="col-3 order-1">
                                <button class="btn btn-b3 btn-block btn-orange px-4" type="button" data-dismiss="modal">NÃO</button>
                            </div>
                        </div>

                        <div class="row justify-content-end" v-else>
                            <div class="col-6 order-2">
                                <button class="btn btn-b3 btn-orange-outlined border-0 close px-4" type="button" data-dismiss="modal">
                                    VOLTAR
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
