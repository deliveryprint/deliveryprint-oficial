<div id="costsApp">

    <h2 class="text-orange mt-5">
        <i class="fa fa-exchange mr-3"></i> Custos de Produção
    </h2>

    <div class="row mt-5" id="costsTable">
        <div class="col-12">
            <div class="row">
                <div class="col-8">
                    <h3 class="text-orange">Custos de Papel</h3>
                </div>
                <div class="col-4 text-center">
                    <button type="button" class="btn btn-success" @click="updatePrintCosts">Aplicar Alterações</button>
                </div>
            </div>
            <div class="row mt-3">
                <table class="col-12 text-center">
                    <thead class="font-weight-bold">
                        <tr>
                            <td>Id</td>
                            <td>Tipo de Papel</td>
                            <td>Cor</td>
                            <td>Lado</td>
                            <td>Valor</td>
                        </tr>
                    </thead>
                    <tbody>
                        <tr v-for="(item, id) in listPaper" :key="id">
                            <td class="text-center">{{item.id}}</td>
                            <td class="text-center">{{item.paper}}</td>
                            <td class="text-center">{{item.color}}</td>
                            <td class="text-center">{{item.side}}</td>
                            <td class="text-center">
                                R$<input type="number" class="form-control text-center" min="0" v-model="item.value">
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
        <div class="col-12 mt-5">
            <div class="row">
                <div class="col-8">
                    <h3 class="text-orange">Custos de Acabamento</h3>
                </div>
                <div class="col-4 text-center">
                    <button type="button" class="btn btn-success" @click="updateFinishCosts">Aplicar Alterações</button>
                </div>
            </div>
            <div class="row justify-content-center mt-3">
                <table class="col-8 text-center">
                    <thead class="font-weight-bold">
                        <tr>
                            <td>Id</td>
                            <td>Tipo de Acabamento</td>
                            <td>Valor</td>
                        </tr>
                    </thead>
                    <tbody>
                        <tr v-for="(item, id) in listFinish" :key="id">
                            <td class="text-center">{{item.id}}</td>
                            <td class="text-center">{{item.finish}}</td>
                            <td class="text-center">
                                R$<input type="number" class="form-control text-center" min="0" v-model="item.value">
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
