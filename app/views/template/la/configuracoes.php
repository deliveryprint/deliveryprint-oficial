<div id="settingsApp">

    <div class="row">
        <div class="col-12">
            <h2 class="table-title mt-5">
                <i class="fa fa-envelope mr-3"></i> Dias extras nas entregas e retiradas
            </h2>

            <p>Preencha os campos abaixo para adicionar dias úteis extras aos cálculos de entregas e retiradas.</p>
            <ul class="mb-4">
                <li>O valor 0 não causa nenhuma alteração nos cálculos.</li>
                <li>Os campos marcados como "Geral" afetam todas as opções de etrega ou retirada.</li>
                <li>As configurações específicas sobrescrevem as configurações gerais.</li>
            </ul>
        </div>
    </div>

    <div class="row">

        <div class="col-md-6">
            <div class="card mb-4">
                <div class="card-body">

                    <h3 class="card-title">Entregas</h3>

                    <div class="form-group row">
                        <label class="col-sm-4 col-form-label">Geral</label>
                        <div class="col-sm-8">
                            <input type="number" class="form-control" v-model="settings.deliveryExtraBDays">
                        </div>
                    </div>

                    <div class="form-group row">
                        <label class="col-sm-4 col-form-label">Expressa 1</label>
                        <div class="col-sm-8">
                            <input type="number" class="form-control" v-model="settings.dExpress1ExtraBDays">
                        </div>
                    </div>

                    <div class="form-group row">
                        <label class="col-sm-4 col-form-label">Expressa 2</label>
                        <div class="col-sm-8">
                            <input type="number" class="form-control" v-model="settings.dExpress2ExtraBDays">
                        </div>
                    </div>

                    <div class="form-group row">
                        <label class="col-sm-4 col-form-label">Expressa 3</label>
                        <div class="col-sm-8">
                            <input type="number" class="form-control" v-model="settings.dExpress3ExtraBDays">
                        </div>
                    </div>

                    <div class="form-group row">
                        <label class="col-sm-4 col-form-label">Expressa 4</label>
                        <div class="col-sm-8">
                            <input type="number" class="form-control" v-model="settings.dExpress4ExtraBDays">
                        </div>
                    </div>

                    <div class="form-group row">
                        <label class="col-sm-4 col-form-label">Sedex 1</label>
                        <div class="col-sm-8">
                            <input type="number" class="form-control" v-model="settings.dSedex1ExtraBDays">
                        </div>
                    </div>

                    <div class="form-group row">
                        <label class="col-sm-4 col-form-label">Sedex 2</label>
                        <div class="col-sm-8">
                            <input type="number" class="form-control" v-model="settings.dSedex2ExtraBDays">
                        </div>
                    </div>

                    <div class="form-group row">
                        <label class="col-sm-4 col-form-label">Pac 1</label>
                        <div class="col-sm-8">
                            <input type="number" class="form-control" v-model="settings.dPac1ExtraBDays">
                        </div>
                    </div>

                    <div class="form-group row">
                        <label class="col-sm-4 col-form-label">Pac 2</label>
                        <div class="col-sm-8">
                            <input type="number" class="form-control" v-model="settings.dPac2ExtraBDays">
                        </div>
                    </div>

                </div>
            </div>
        </div>

        <div class="col-md-6">
            <div class="card mb-4">
                <div class="card-body">

                    <h3 class="card-title">Retiradas</h3>

                    <div class="form-group row">
                        <label class="col-sm-4 col-form-label">Geral</label>
                        <div class="col-sm-8">
                            <input type="number" class="form-control" v-model="settings.withdrawalExtraBDays">
                        </div>
                    </div>

                    <div class="form-group row">
                        <label class="col-sm-4 col-form-label">Expressa 1</label>
                        <div class="col-sm-8">
                            <input type="number" class="form-control" v-model="settings.wExpress1ExtraBDays">
                        </div>
                    </div>

                    <div class="form-group row">
                        <label class="col-sm-4 col-form-label">Expressa 2</label>
                        <div class="col-sm-8">
                            <input type="number" class="form-control" v-model="settings.wExpress2ExtraBDays">
                        </div>
                    </div>

                    <div class="form-group row">
                        <label class="col-sm-4 col-form-label">Normal 1</label>
                        <div class="col-sm-8">
                            <input type="number" class="form-control" v-model="settings.wNormal1ExtraBDays">
                        </div>
                    </div>

                    <div class="form-group row">
                        <label class="col-sm-4 col-form-label">Normal 2</label>
                        <div class="col-sm-8">
                            <input type="number" class="form-control" v-model="settings.wNormal2ExtraBDays">
                        </div>
                    </div>

                </div>
            </div>

            <button class="btn btn-success" type="button" @click="applyChanges()">Aplicar alterações</button>
        </div>

    </div>
</div>
