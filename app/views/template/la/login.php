<div class="container text-center">
    <div class="row">
        <div class="col-10 offset-1 col-sm-6 col-lg-4 offset-sm-3 offset-lg-4 ">
            <h3 class="text-orange pb-5">Acessar</h3>
            <form class="row px-3 form-horizontal intec-ajax-form" id="loginAdm" method="post" action="/loginAdm">
                <input type="text" class="form-control col-12 mb-4" placeholder="E-mail" name="email"><br/>
                <input type="password" class="form-control col-12 mb-4" placeholder="Senha" name="password"><br/>
                <a href="#" class="default-text text-sm text-orange font-weight-bold col-12 mb-4 text-left px-0">Esqueci a senha</a>
                <button type="submit" class="btn btn-b3 btn-b4-xs text-center btn-orange mx-auto">PROSSEGUIR</button>
            </form>

        </div>
    </div>
</div>
