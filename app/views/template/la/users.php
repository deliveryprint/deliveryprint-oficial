<div id="usersApp">

    <h2 class="text-orange mt-5">
        <i class="fa fa-users mr-3"></i> Lista de Usuários
    </h2>

    <div class="row" id="filters">
        <div class="col-md-6 mt-3"></div>
        <div class="col-md-2 offset-md-1 d-flex align-items-center justify-content-center">
            <p class="text-orange btn" @click="downloadCsv()">
                <i class="fa fa-floppy-o mr-2"></i>Exportar
            </p>
        </div>
        <div class="col-md-3">
            <div class="clearfix filter-by-date date-fixed">
                <div class="col-md-12 p-0">
                    <div class="data-filter-title">
                        <i class="fa fa-calendar"></i>
                        <b>
                            Período
                        </b>
                    </div>
                </div>
                <div class="date-text" data-toggle="collapse" data-target="#inputsDiv">
                    {{startPeriod | formatDatePt}} até {{endPeriod | formatDatePt}}
                    <i class="fa fa-angle-down collapsed" data-toggle="collapse" data-target="#inputsDiv"></i>
                </div>
                <div class="collapse small-padding" id="inputsDiv">
                    <div class="type-div">
                        <span class="type">Tipo</span>
                        <div class="btn-group">
                            <button type="button" class="text-sm btn btn-grey-outlined dropdown-toggle default-text text-orange" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                {{periodType}}
                            </button>
                            <ul class="dropdown-menu">
                                <li>
                                    <a href="#" v-on:click="setPeriodType('Conta')">Criação da Conta</a>
                                </li>
                                <li>
                                    <a href="#" v-on:click="setPeriodType('Pedido')">Ultimo Pedido</a>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class="period">Período</div>
                    <div class="input-period">
                        <input name="start-period" class="form-control" v-mask="'##/##'" v-model="startPeriod">
                        <span class="input-divider"> a </span>
                        <input name="end-period" class="form-control" v-mask="'##/##'" v-model="endPeriod">
                    </div>
                    <!-- <div class="">
                        <div class="text-center apply">
                            <button type="button" class="btn btn-block btn-b3 border-radius-lg btn btn-orange border-0 border-solid"
                            >Aplicar</button>
                        </div>
                    </div> -->
                </div>
            </div>
        </div>
    </div>
    <div class="row mt-5" id="usersTable">
        <table class="col-12 text-center">
            <thead class="font-weight-bold">
                <tr>
                    <td class="w-25">Nome</td>
                    <td class="w-25">Email</td>
                    <td class="w-20">Telefone</td>
                    <td class="w-15">Data de Criação</td>
                    <td class="w-15">Última Compra</td>
                </tr>
            </thead>
            <tbody>
                <tr v-for="(usr, id) in listUsers" :key="id">
                    <td class="text-center">{{checkField(usr.nomeCliente)}}</td>
                    <td class="text-center">{{usr.emailCliente}}</td>
                    <td class="text-center">{{checkField(usr.telCliente)}}</td>
                    <td class="text-center">{{usr.dtIngresso}}</td>
                    <td class="text-center">{{checkField(usr.dataPedido)}}</td>
                </tr>
            </tbody>
        </table>
    </div>
</div>
