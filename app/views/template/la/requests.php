<div id="ordersApp">

    <div class="row">
        <div class="col-12">
            <h2 class="text-orange mt-5">
                <i class="fa fa-print mr-3"></i> Pedidos
            </h2>

            <div class="col-12">
                <div class="row" id="filters">
                    <div class="col-md-9">
                        <h3 class="table-title">Itens</h3>
                        <div class="col-md-12 p-0">
                            <div id="mainFilters" class="bs-component live-less-editor-hovercontainer">
                                <div class="bg-white white-placeholder"></div>
                                <table class="table  filter-by-data ">
                                    <thead>
                                        <tr class="title-column">
                                            <th class="pedido-la">
                                                <i class="fa fa-unsorted"></i>
                                                <span>ID Item</span>
                                            </th>
                                            <th class="pedido-la">
                                                <i class="fa fa-user"></i>
                                                <span>Nome ou e-mail</span>
                                            </th>
                                            <th class="pedido-la">
                                                <i class="fa fa-truck"></i>
                                                <span>Tipo Entrega</span>
                                            </th>
                                            <th class="pedido-la">
                                                <i class="fa fa-money"></i>
                                                <span>Status Pgto</span>
                                            </th>
                                            <th class="pedido-la">
                                                <i class="fa fa-bullseye"></i>
                                                <!-- fa fa-crosshairs-->
                                                <span>Status Item</span>
                                            </th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td>
                                                <input name="end-period" class="form-control filter-input" v-model="idItem">
                                            </td>
                                            <td>
                                                <input name="end-period" class="form-control filter-input" v-model="nome">
                                            </td>
                                            <td>
                                                <select class="chosen-select form-control chosen-input" multiple data-placeholder="Escolha as opções" v-model="tipoEntrega">
                                                    <option></option>
                                                    <option value="w1">Retirada Expressa 1</option>
                                                    <option value="w2">Retirada Expressa 2</option>
                                                    <option value="f1">Entrega Expressa 1</option>
                                                    <option value="f2">Entrega Expressa 2</option>
                                                    <option value="f3">Entrega Expressa 3</option>
                                                    <option value="f4">Entrega Expressa 4</option>
                                                    <option value="f5">Entrega Sedex 1</option>
                                                    <option value="f6">Entrega Sedex 2</option>
                                                    <option value="f7">Entrega PAC 1</option>
                                                    <option value="f8">Entrega PAC 2</option>
                                                    <option value="w3">Retirada Normal 1</option>
                                                    <option value="w4">Retirada Normal 2</option>
                                                </select>
                                            </td>
                                            <td>
                                                <select class="chosen-select-1 form-control chosen-input" multiple data-placeholder="Escolha as opções" v-model="statusPgto">
                                                    <option value="Pendente">Pendente</option>
                                                    <option value="Pago">Pago</option>
                                                    <option value="Cancelado">Cancelado</option>
                                                </select>
                                            </td>
                                            <td>
                                                <select class="chosen-select-2 form-control chosen-input" multiple data-placeholder="Escolha as opções" v-model="statusItem">
                                                    <option></option>
                                                    <option v-for="st in statusArray" v-bind:value="st">{{st}}</option>
                                                </select>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                            <h3 class="table-title">
                                <i class="fa fa-motorcycle"></i>Entregas & Retiradas Rápidas
                            </h3>
                        </div>
                    </div>
                    <div class="col-md-2 offset-md-1">
                        <div class="clearfix filter-by-date date-fixed">
                            <div class="col-md-12 p-0">
                                <div class="data-filter-title">
                                    <i class="fa fa-calendar"></i>
                                    <b>
                                        Período
                                    </b>
                                </div>
                            </div>
                            <div class="date-text" data-toggle="collapse" data-target="#inputsDiv">
                                {{startPeriod | formatDatePt}} até {{endPeriod | formatDatePt}}
                                <i class="fa fa-angle-down collapsed" data-toggle="collapse" data-target="#inputsDiv"></i>
                            </div>
                            <div class="collapse small-padding" id="inputsDiv">
                                <div class="type-div">
                                    <span class="type">Tipo</span>
                                    <div class="btn-group">
                                        <button type="button" class="text-sm btn btn-grey-outlined dropdown-toggle default-text text-orange" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                            {{periodType}}
                                        </button>
                                        <ul class="dropdown-menu">
                                            <li>
                                                <a href="#" v-on:click="setPeriodType('Pedido')">Pedido</a>
                                            </li>
                                            <li>
                                                <a href="#" v-on:click="setPeriodType('Entrega')">Entrega</a>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="period">Período</div>
                                <div class="input-period">
                                    <input name="start-period" class="form-control" v-mask="'##/##'" v-model="startPeriod">
                                    <span class="input-divider"> a </span>
                                    <input name="end-period" class="form-control" v-mask="'##/##'" v-model="endPeriod">
                                </div>
                                <!-- <div class="">
                        <div class="text-center apply">
                            <button type="button" class="btn btn-block btn-b3 border-radius-lg btn btn-orange border-0 border-solid"
                            >Aplicar</button>
                        </div>
                    </div> -->
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-12 p-0">
                <div class="table-all-data bs-component live-less-editor-hovercontainer" id="ninetyTable">
                    <table class="table-delivery table table-striped table-hover">
                        <?php require 'app/views/partial/la-request-table.php'; ?>
                    </table>
                </div>
            </div>

            <div class="col-12 p-0">
                <div class="table-all-data bs-component live-less-editor-hovercontainer" id="deliveryTable">
                    <table class="table table-striped table-hover">
                        <?php require 'app/views/partial/la-request-table.php'; ?>
                    </table>
                </div>
            </div>
        </div>

    </div>
</div>

<div class="modal fade text-area-modal" id="modalObservacoes" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h3 class="modal-title default-text text-orange font-weight-lighter" id="exampleModalLongTitle">
                    <Strong>Adicionar Nota</strong>
                </h3>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body pt-0">
                <div class="form-group m-0" v-if="">
                    <textarea class="form-control " rows="5" id="clientOrientation" name="clientOrientation" placeholder="Campo destinado para observações" v-model="observacao"></textarea>
                    <div class="float-right">
                        <button type="button" class="btn btn-b3 btn-b6-xs border-radius-lg btn btn-orange border border-solid" v-on:click="saveObs">
                            SALVAR
                        </button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="modalRastreio" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h3 class="modal-title f-24 text-orange default font-weight-bold" id="exampleModalLongTitle">
                    <strong>Esse pedido contém código de rastreio?</strong>
                </h3>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body text-left">
                <div class="row">
                    <div class="col-10 ">
                        <p class="text-orange f-18 text-left">Insira o Código:</p>
                        <div class="form-group">
                            <input class="form-control validate w-100" v-model="$v.code.$model" maxlength="13" type="text" />
                        </div>
                        <div class="input-validator bg-white text-red py-0" v-if="!$v.code.minLength">
                            O código deve possuir 13 caracteres
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer justify-content-center">
                <button type="button" :disabled="code.length > 0" data-dismiss="modal" class="btn btn-b3 btn-b6-xs border-radius-lg btn btn-white border border-solid mr-2 mb-3">
                    NÃO TEM
                </button>
                <button type="button" :disabled="code.length != 13" class="btn btn-b3 btn-b6-xs border-radius-lg btn btn-blue border border-solid ml-2 mb-3" @click="saveTrackingCode()">
                    ENVIAR CÓDIGO
                </button>
            </div>
        </div>
    </div>
</div>
