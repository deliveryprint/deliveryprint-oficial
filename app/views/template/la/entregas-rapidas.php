<div id="shippingPricesApp">

    <div class="row">
        <div class="col-12">
            <h2 class="table-title mt-5">
                <i class="fa fa-dollar mr-3"></i> Preços das Entregas e Retiradas Expressas
            </h2>
        </div>
    </div>

    <div class="row">

        <div class="col-md-6">
            <div class="card mb-4">
                <div class="card-body">

                    <h3 class="card-title">Entregas Expressas</h3>

                    <div class="form-group row">
                        <label class="col-sm-4 col-form-label">Expressa 1</label>
                        <div class="col-sm-8">
                            <input type="number" class="form-control" v-model="prices.dExpress1Price">
                        </div>
                    </div>

                    <div class="form-group row">
                        <label class="col-sm-4 col-form-label">Expressa 2</label>
                        <div class="col-sm-8">
                            <input type="number" class="form-control" v-model="prices.dExpress2Price">
                        </div>
                    </div>

                    <div class="form-group row">
                        <label class="col-sm-4 col-form-label">Expressa 3</label>
                        <div class="col-sm-8">
                            <input type="number" class="form-control" v-model="prices.dExpress3Price">
                        </div>
                    </div>

                    <div class="form-group row">
                        <label class="col-sm-4 col-form-label">Expressa 4</label>
                        <div class="col-sm-8">
                            <input type="number" class="form-control" v-model="prices.dExpress4Price">
                        </div>
                    </div>

                    <div class="form-group row">
                        <label class="col-sm-4 col-form-label">Expressa 4 Preço Adicional</label>
                        <div class="col-sm-8">
                            <input type="number" class="form-control" v-model="prices.dExpress4AdditionalPrice">
                        </div>
                    </div>

                    <div class="form-group row">
                        <label class="col-sm-4 col-form-label">Expressa 5</label>
                        <div class="col-sm-8">
                            <input type="number" class="form-control" v-model="prices.dExpress5Price">
                        </div>
                    </div>
                </div>
            </div>

            <button class="btn btn-success" type="button" @click="updateDeliveryPrices()">Aplicar alterações</button>
        </div>

        <div class="col-md-6">
            <div class="card mb-4">
                <div class="card-body">

                    <h3 class="card-title">Retiradas Expressas</h3>

                    <div class="form-group row">
                        <label class="col-sm-4 col-form-label">Expressa 1</label>
                        <div class="col-sm-8">
                            <input type="number" class="form-control" v-model="prices.wExpress1Price">
                        </div>
                    </div>

                    <div class="form-group row">
                        <label class="col-sm-4 col-form-label">Expressa 2</label>
                        <div class="col-sm-8">
                            <input type="number" class="form-control" v-model="prices.wExpress2Price">
                        </div>
                    </div>
                </div>
            </div>

            <button class="btn btn-success" type="button" @click="updateWithdrawalPrices()">Aplicar alterações</button>
        </div>

    </div>
</div>
