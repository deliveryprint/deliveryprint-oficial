<div id="reportsApp">

    <h2 class="text-orange mt-5">
        <i class="fa fa-file mr-3"></i> Relatórios do Sistema
    </h2>


    <div class="row mt-4 mb-3" id="filters">
        <div class="col-md-7">
            <div class="row">
                <p class="text-orange btn text-hg mx-0" @click="downloadCsv()">
                    <i class="fa fa-floppy-o mx-2"></i>Exportar
                </p>
                <p class="text-grey text-sm ml-0 mt-3">
                    *Defina o Período de dias para "Exportar" os dados.
                </p>
            </div>
            <div class="row">
                <p class="text-orange btn text-hg mx-0" @click="downloadLtvTable()">
                    <i class="fa fa-floppy-o mx-2"></i>LTV
                </p>
            </div>
        </div>
        <div class="col-md-4 offset-md-1">
            <filters-app :filters-value="filtersValue" @searchbtncicked="getAllData($event)"></filters-app>
        </div>
    </div>

    <section class="row">
        <div class="col-sm-6 col-md-4 col-lg-3 col-xl-2">
            <spotlight-box background="secondary">
                <template v-slot:header>
                    Link de indicação
                </template>
                {{ invitationCoupons.length }}
            </spotlight-box>
        </div>

        <div class="col-sm-6 col-md-4 col-lg-3 col-xl-2">
            <spotlight-box background="success">
                <template v-slot:header>
                    Ticket Médio
                </template>
                R$ {{ averageTicket | moneyFormatter}}
            </spotlight-box>
        </div>
    </section>

    <section class="d-flex flex-column flex-md-row">

        <div class="flex-fill">
            <admin-box title="Vendas no período">
                <ul class="nav nav-tabs">
                    <li class="nav-item">
                        <a class="nav-link active" data-toggle="tab" href="#sales">Valor total de vendas</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" data-toggle="tab" href="#sales2">Total de vendas</a>
                    </li>
                </ul>

                <div class="tab-content">
                    <div class="tab-pane fade show active" id="sales" role="tabpanel">
                        <line-chart :chart-data="salesChartData" :options="salesChartOptions" :height="salesChartHeight"></line-chart>
                    </div>
                    <div class="tab-pane fade" id="sales2" role="tabpanel">
                        <line-chart :chart-data="salesChart2Data" :height="salesChartHeight"></line-chart>
                    </div>
                </div>
            </admin-box>
        </div>

        <div class="d-flex flex-column flex-sm-row flex-md-column justify-content-between mx-3">
            <spotlight-box background="primary">
                <template v-slot:header>
                    Faturamento
                </template>
                R$ {{billingResult.billing | moneyFormatter}}
            </spotlight-box>

            <spotlight-box background="danger">
                <template v-slot:header>
                    Custos de Produção
                </template>
                R$ {{billingResult.costs | moneyFormatter}}
            </spotlight-box>

            <spotlight-box background="success">
                <template v-slot:header>
                    Resultado Operacional
                </template>
                R$ {{billingResult.result | moneyFormatter}}
            </spotlight-box>
        </div>

    </section>

    <section class="row">

        <div class="col-md-6">
            <div class="row">

                <div class="col-md-12">
                    <admin-box title="Clientes novos x recorrentes">
                        <pie-chart :chart-data="customerSalesChartData" :height="chartHeight"></pie-chart>
                    </admin-box>
                </div>

                <div class="col-md-12">
                    <admin-box title="Cupons mais utilizados">
                        <data-table :headers="couponsTotalHeaders" :items="couponsTotalItems" small>
                            <template v-slot:items="props">
                                <th>{{ props.item.codigo }}</th>
                                <td class="text-right">{{ props.item.qtd }}</td>
                            </template>
                        </data-table>
                    </admin-box>
                </div>

                <div class="col-md-12">
                    <admin-box title="Tipos de Entrega">
                        <data-table :headers="deliveryTypesHeaders" :items="deliveryTypesItems" small>
                            <template v-slot:items="props">
                                <th>{{ props.item.type }}</th>
                                <td class="text-right">{{ props.item.total }}</td>
                            </template>
                        </data-table>
                    </admin-box>
                </div>

            </div>
        </div>

        <div class="col-md-6">
            <div class="row">

                <div class="col-md-12">
                    <admin-box title="Acabamentos">
                        <data-table :headers="finishesHeaders" :items="finishesItems" small>
                            <template v-slot:items="props">
                                <th>{{ props.item.type }}</th>
                                <td class="text-right">{{ props.item.quantity }}</td>
                            </template>
                        </data-table>
                    </admin-box>
                </div>

                <div class="col-md-12">
                    <admin-box title="Valor Médio por Página">
                        <div class="row">
                            <div class="col-sm-6">
                                <spotlight-box background="secondary">
                                    <template v-slot:header>
                                        Preto e Branco
                                    </template>
                                    R$ {{ (totalPricePb / totalPagesPb) | moneyFormatter }}
                                </spotlight-box>
                            </div>
                            <div class="col-sm-6">
                                <spotlight-box background="info">
                                    <template v-slot:header>
                                        Colorido
                                    </template>
                                    R$ {{ (totalPriceColor / totalPagesColor) | moneyFormatter }}
                                </spotlight-box>
                            </div>
                        </div>
                    </admin-box>
                </div>

                <div class="col-md-12">
                    <admin-box title="Total de páginas">
                        <data-table :headers="pagesTotalHeaders" :items="pagesTotalItems" small>
                            <template v-slot:items="props">
                                <th>{{ props.item.name }}</th>
                                <td class="text-right">{{ props.item.bwTotal }}</td>
                                <td class="text-right">{{ props.item.cTotal }}</td>
                                <td class="text-right">{{ props.item.total }}</td>
                            </template>
                        </data-table>
                    </admin-box>
                </div>

                <div class="col-md-12">
                    <admin-box title="Entregas x Retiradas">
                        <ul class="nav nav-tabs">
                            <li class="nav-item">
                                <a class="nav-link active" data-toggle="tab" href="#freights">Comparação</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" data-toggle="tab" href="#deliveries">Entregas</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" data-toggle="tab" href="#withdrawals">Retiradas</a>
                            </li>
                        </ul>

                        <div class="tab-content">
                            <div class="tab-pane fade show active" id="freights" role="tabpanel">
                                <pie-chart :chart-data="freightChartData" :height="chartHeight"></pie-chart>
                            </div>
                            <div class="tab-pane fade" id="deliveries" role="tabpanel">
                                <pie-chart :chart-data="deliveryChartData" :height="chartHeight"></pie-chart>
                            </div>
                            <div class="tab-pane fade" id="withdrawals" role="tabpanel">
                                <pie-chart :chart-data="withdrawalChartData" :height="chartHeight"></pie-chart>
                            </div>
                        </div>
                    </admin-box>
                </div>

            </div>
        </div>
    </section>

    <section class="row">
        <div class="col-md-12">
            <admin-box title="Faixas de Páginas">
                <ul class="nav nav-tabs">
                    <li class="nav-item">
                        <a class="nav-link active" data-toggle="tab" href="#rangeTable">Tabela</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" data-toggle="tab" href="#rangeGraph">Gráfico</a>
                    </li>
                </ul>

                <div class="tab-content">
                    <div class="tab-pane fade show active" id="rangeTable" role="tabpanel">
                        <data-table :headers="rangesHeaders" :items="rangesItems" small>
                            <template v-slot:items="props">
                                <th>{{ props.item.id }}</th>
                                <td>{{ props.item.paperType }}</td>
                                <td>{{ props.item.color }}</td>
                                <td>{{ props.item.numPages }}</td>
                                <td class="text-right">{{ props.item.numItems }}</td>
                                <td class="text-right">{{ props.item.earnings }}</td>
                                <td class="text-right">{{ props.item.cost }}</td>
                                <td class="text-right">{{ props.item.result }}</td>
                            </template>
                        </data-table>
                    </div>
                    <div class="tab-pane fade" id="rangeGraph" role="tabpanel">
                        <bar-chart :chart-data="rangeChartData" :options="rangeChartOptions" :height="salesChartHeight"></bar-chart>
                    </div>
                </div>
            </admin-box>
        </div>
    </section>

    <page-loader :show="loadingAll"></page-loader>
</div>
