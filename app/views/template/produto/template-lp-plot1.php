<div class="container my-md-5 pb-md-5 lp" id="lp">
    <div class="row mb-3">
        <div class="col-4 offset-md-4 d-flex justify-content-center">
            <img class="logo p-0 " src="/img/logo.png" alt="logo">
        </div>
        <div class="col-4">
            <div class="float-right">
                <span class="default-text text-orange text-md">
                    <b>Faça seu Orçamento:</b>
                    <br>
                </span>
                <span class="default-text text-orange text-sm pt-2">
                    <i class="fa fa-whatsapp"></i> (11)99281-2284 | <i class="fa fa-phone"></i> (11)3807-7562
                    <br />
                </span>
                <span class="default-text text-sm text-orange text-md pt-2">
                    <b>
                        <i class="fa fa-envelope-o d-none d-sm-inline"></i> suporte@deliveryprint.com.br</b>
                    <br />
                </span>
            </div>
        </div>
    </div>
    <div class="row mt-5">
        <div class="col-md-6 col-12 order-2 order-md-1">
            <h1 class="text-orange h2">
                Peça seu Orçamento de <br>Plotagem Rapidamente!
            </h1>

            <div class="img-responsive img-plot" alt="" style="background-image: url('<?php echo $img_url ?>')">
            </div>

        </div>
        <div class="col-md-6 details order-md-1 d-flex text-lg mt-md-0 mb-5 mb-md-0">
            <div class="col-12 m-0 ">
                <form id="email_form">
                    <label class="text-orange">Nome
                        <br/>
                        <input class="form-control form-plot" v-model="user_name" type="text" />
                    </label>
                    <br/>
                    <label class="text-orange my-3">Email
                        <br/>
                        <input v-model="user_email" class="form-control form-plot" type="email" />
                    </label>
                    <br/>
                    <label class="text-orange">Escreva como você quer sua Impressão:
                        <br/><br/>
                        <textarea class="form-control form-plot" rows="5" id="textArea" v-model="description" type="text"></textarea>
                    </label>
                    <br/>
                    <button type="button" @click="subject = '<?php echo $subject ?>'; sendEmail()" class="btn btn-b4 btn-info bg-orange border-0 mt-3 float-sm-right">PEDIR ORÇAMENTO</button>
                </form>
            </div>
        </div>
    </div>
</div>

<?php
    require 'app/views/partial/how_it_works-bar.php';
    require 'app/views/partial/link_building_bar.php';
?>

<div class="container my-3" id="commentBar">
    <div class="row">
        <div class="col-12 text-center">
            <h3 class="text-orange">
                FAÇA SUA IMPRESSÃO NA DELIVERYPRINT
            </h3>
        </div>
        <div class="col-12">
            <p class="text-grey font-weight-bold text-justify mx-5">
                <?php
                    if (empty($text)):
                ?>

                Pensou em impressão barata, pensou na DeliveryPrint, a sua Gráfica Digital Online. Impressão em preto e
                branco e impressão colorida com os menores preços do mercado, online e físico! Imprimir com qualidade
                garantida, do conforto de sua casa, você pode fazer o seu pedido de impressão online e receber na sua
                casa, até no mesmo dia! Entregas para todo o Brasil! Trabalhamos com diversas opções de papel e
                acabamento, encadernação em espiral, wire-o e capa dura para TCC. Papel Sulfite e Couchê, nos tamanhos
                A5, A4 e A3. Não perca tempo: imprimir com qualidade e preço baixo, é na DeliveryPrint: a impressora
                online com 5,0 estrelas no Google Avaliações!

                <?php
                    else:

                        echo $text;

                    endif;
                ?>
            </p>
        </div>
    </div>
</div>
