<div class="container" id="emailDiv">
    <div class="row">
        <div class="col">
            <h1 class="text-orange my-3 f-28 f-sm-36">IMPRESSÃO EM PAPEL VERGÊ</h1>
            <input type="text" class="d-none" ref="subject_input" value="Orçamento para impressão em PAPEL VERGE">
            <input type="text" class="d-none" ref="email_type" value="budget-request-description">
        </div>
    </div>
    <div class="row mb-3 border-bottom">
        <div class="col-12 col-md-6">
            <div class="row mb-2">
                <div class="col-12 text-orange d-flex align-items-center justify-content-center justify-content-md-start">
                    <i class="fa fa-envelope-open f-40 p-3"></i>
                    <h3 class="f-18 d-inline font-weight-normal mb-0 default-text" style="min-width: 120px;">Convites</h3>
                </div>
                <div class="col-12 text-orange d-flex align-items-center justify-content-center justify-content-md-start">
                    <i class="fa fa-files-o f-40 p-3"></i>
                    <h3 class="f-18 d-inline font-weight-normal mb-0 default-text" style="min-width: 120px;">Apresentações</h3>
                </div>
                <div class="col-12 text-orange d-flex align-items-center justify-content-center justify-content-md-start">
                    <i class="fa fa-file-text-o f-40 p-3"></i>
                    <h3 class="f-18 d-inline font-weight-normal mb-0 default-text" style="min-width: 120px;">Curriculum</h3>
                </div>
                <div class="col-12 text-orange d-flex align-items-center justify-content-center justify-content-md-start">
                    <i class="fa fa-th-list f-40 p-3"></i>
                    <h3 class="f-18 d-inline font-weight-normal mb-0 default-text" style="min-width: 120px;">Cardápios</h3>
                </div>
                <div class="col-12 text-orange d-flex align-items-center justify-content-center justify-content-md-start">
                    <i class="fa fa-graduation-cap f-40 p-3"></i>
                    <h3 class="f-18 d-inline font-weight-normal mb-0 default-text" style="min-width: 120px;">TCC</h3>
                </div>
            </div>
            <div class="row">
                <div class="col-12 mb-4">
                    <p class="text-center text-orange f-20 f-sm-24 mb-3 font-weight-bold"> Trabalhamos com:</p>
                    <h2 class="text-grey f-16 f-sm-18 font-weight-normal default-text text-center mb-2">Papel Vergê A4
                        80g</h2>
                    <h2 class="text-grey f-16 f-sm-18 font-weight-normal default-text text-center">Papel Vergê A4 180g</h2>
                </div>
            </div>
        </div>
        <div class="col-12 col-md-6">
            <h4 class="text-orange f-24 f-sm-28 mb-2">Peça seu Orçamento, é rápido!</h4>
            <div class="form" action="" id="email_form">
                <div class="form-group">
                    <input class="form-control" type="text" name="nomeCliente" placeholder="Nome" v-model="user_name">
                </div>
                <div class="form-group">
                    <input class="form-control" type="text" name="email" placeholder="Email" v-model="user_email">
                </div>
                <div class="form-group">
                    <input class="form-control mb-3" type="tel" v-mask="['(##)####-####', '(##)#####-####']" name="telephone" placeholder="Telefone" v-model="user_tel">
                </div>
                <textarea class="form-control p-2 mb-3" rows="5" name="clientText"
                placeholder="Escreva aqui o que deseja imprimir e a quantidade de folhas" v-model="description"></textarea>

                <button type="button" class="btn btn-b3 btn-b6-xs border-radius-lg btn btn-orange border border-solid d-block ml-auto mb-2" @click="sendEmail()">
                    ENVIAR
                </button>

            </div>

            <div class="text-center">

                <p class="text-orange f-20 f-sm-24 mx-auto d-inline-block">
                    Nossos telefones:
                </p>
                <p class="text-orange f-24 mx-auto d-inline-block">
                    <i class="fa fa-whatsapp p-2"> </i>
                    <span class="f-16 f-sm-18"> (11) 99281-2284</span>
                </p>
                <p class="text-orange f-24 mx-auto d-inline-block">
                    <i class="fa fa-phone p-2"> </i>
                    <span class="f-16 f-sm-18"> (11) 3807-7562</span>
                </p>
            </div>
        </div>
    </div>
</div>

<?php require 'app/views/partial/comments.php'; ?>
