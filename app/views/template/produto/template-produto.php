<div class="container" id="orderApp">
    <div class="row">
        <div class="container col-sm-12" id="container">
            <div class="row">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item">
                            <a href="/">Home</a>
                        </li>
                        <li class="breadcrumb-item font-weight-bold">
                            <a href="/produtos">Serviços</a>
                        </li>
                        <li class="breadcrumb-item active" aria-current="page">
                            <?php echo $breadc ?>
                        </li>
                    </ol>
                </nav>
            </div>
            <h1 class="text-orange h2 mt-4 d-md-none">
                <?php echo $title ?>
            </h1>
        </div>
    </div>
</div>

<div class="container mb-5 pb-md-5">
    <div class="row">
        <div class="col-md-6 col-12">
            <div class="img-responsive img-a" alt="<?php echo $title ?>"
                style="background-image: url('<?php echo $img_url ?>')">
            </div>
            <div class="text-orange paper_type row text-center align-items-baseline">
                <?php
                    if (isset($a_0) && $a_0):
                ?>
                <div class="p-2 a0 bg-white col-auto ml-2 mb-0 default-title  font-weight-bold">
                    <a href="/impressao-online">A0</a>
                </div>
                <?php endif;
                    if (isset($a_1) && $a_1):
                ?>
                <div class="p-2 a1 bg-white col-auto ml-2 mb-0 default-title  font-weight-bold">
                    <a href="/impressao-online">A1</a>
                </div>
                <?php endif;
                    if (isset($a_2) && $a_2):
                ?>
                <div class="p-2 a2 bg-white col-auto ml-2 mb-0 default-title  font-weight-bold">
                    <a href="/impressao-online">A2</a>
                </div>
                <?php endif;
                    if (isset($a_3) && $a_3):
                ?>
                <div class="p-2 a3 bg-white col-auto ml-2 mb-0 default-title  font-weight-bold">
                    <a href="/impressao-online">A3</a>
                </div>
                <?php endif;
                    if (isset($a_4) && $a_4):
                ?>
                <div class="p-2 a4 bg-white col-auto ml-2 mb-0 default-title  font-weight-bold">
                    <a href="/impressao-online">A4</a>
                </div>
                <?php endif;
                    if (isset($a_5) && $a_5):
                    ?>
                <div class="p-2 a5 bg-white col-auto ml-2 mb-0 default-title  font-weight-bold">
                    <a href="/impressao-online">A5</a>
                </div>
                <?php endif; ?>
            </div>

        </div>
        <div class="col-md-6 details d-flex text-lg mt-5 mt-md-0">
            <div>
                <h1 class="text-orange h2 d-none d-md-block">
                    <?php echo $title ?>
                </h1>
                <span class="text-grey font-weight-bold">
                    <?php echo $subtitle ?>
                </span>
                <ul class="no-decoration text-grey font-weight-lighter mt-3">
                    <?php
                        foreach ($li_es as $phrase):
                    ?>
                    <li>
                        - <?php echo $phrase ?>
                    </li>
                    <?php
                        endforeach;
                    ?>
                </ul>
            </div>
            <div class="row text-center text-md-left mt-4 mt-md-0">
                <div class="col-12 m-0">
                    <!-- <p class="default-title scale-2 text-hg text-orange mb-3">
                        Preço mínimo de até
                        <span class="h2">R$
                            <?php echo $price ?>
                        </span>
                        cada
                        <?php echo $unit ?>
                        <span class="cursor-pointer" data-toggle="modal" data-target="#priceWarningModal">
                            <i class="fa fa-question-circle text-blue f-20" aria-hidden="true"></i>
                        </span>
                    </p> -->
                </div>
                <div class="col-12 m-0">
                    <a href="/impressao-online">
                        <button class="btn btn-b4 btn-info bg-blue border-0">
                            <i class="fa fa-shopping-cart mr-2"></i>CONFIGURAR E COMPRAR
                        </button>
                    </a>
                </div>
            </div>
        </div>
    </div>
    <div id="coupon" hidden><?php echo $coupon ?>
    </div>
</div>

<?php

    require 'app/views/partial/how_it_works-bar.php';

    require 'app/views/partial/link_building_bar.php';

    require 'app/views/partial/modal-price-warning.php';

?>

<div class="container my-3" id="commentBar">
    <div class="row">
        <div class="col-12 text-center">
            <h3 class="text-orange">
                FAÇA SUA IMPRESSÃO NA DELIVERYPRINT
            </h3>
        </div>
        <div class="col-12">
            <p class="text-grey font-weight-bold text-justify mx-5">
                <?php
                    if (empty($text)):
                ?>

                Pensou em impressão barata, pensou na DeliveryPrint, a sua Gráfica Digital Online. Impressão em preto e
                branco e impressão colorida com os menores preços do mercado, online e físico! Imprimir com qualidade
                garantida, do conforto de sua casa, você pode fazer o seu pedido de impressão online e receber na sua
                casa, até no mesmo dia! Entregas para todo o Brasil! Trabalhamos com diversas opções de papel e
                acabamento, encadernação em espiral, wire-o e capa dura para TCC. Papel Sulfite e Couchê, nos tamanhos
                A5, A4 e A3. Não perca tempo: imprimir com qualidade e preço baixo, é na DeliveryPrint: a impressora
                online com 5,0 estrelas no Google Avaliações!

                <?php
                    else:

                        echo $text;

                    endif;
                ?>
            </p>
        </div>
    </div>
</div>
