<div class="container my-md-5 pb-md-5 lp" id="lp">
    <div class="row mb-3">
        <div class="col-9">
            <img class="logo p-0 " src="/img/logo.png" alt="logo">
        </div>
        <div class="col-3">
            <div class="float-right">
                <span class="default-text text-orange text-md">
                    <b>Faça seu Orçamento:</b>
                    <br>
                </span>
                <span class="default-text text-orange text-sm pt-2">
                    <i class="fa fa-whatsapp"></i> (11)99281-2284 | <i class="fa fa-phone"></i> (11)3807-7562
                    <br />
                </span>
                <span class="default-text text-sm text-orange text-md pt-2">
                    <b>
                        <i class="fa fa-envelope-o d-none d-sm-inline"></i> suporte@deliveryprint.com.br</b>
                    <br />
                </span>
            </div>
        </div>
    </div>
    <div class="row mt-5">
        <div class="col-md-6 col-12 order-2 order-md-1">
            <h1 class="text-orange h2">
                Nossos Diferenciais:
            </h1>

            <ul class="no-decoration text-black font-weight-lighter ul-plot my-3">
                <?php
                    foreach ($li_es as $phrase):
                ?>
                <li>
                    <i class="fa fa-check"></i>
                    <?php echo $phrase ?>
                </li>
                <?php
                    endforeach;
                ?>
            </ul>

            <div class="img-responsive img-plot" alt="" style="background-image: url('<?php echo $img_url ?>')">
            </div>

        </div>
        <div class="col-md-6 details order-md-1 d-flex text-lg mt-md-0 mb-5 mb-md-0">
            <div class="col-12 m-0 ">
                <h2 class="text-orange">
                    Peça seu Orçamento de Plotagem!
                </h2>
                <form id="email_form">
                    <label class="text-orange">Nome
                        <br />
                        <input class="form-control form-plot" v-model="user_name" type="text" />
                    </label>
                    <br />
                    <label class="text-orange my-3">Email
                        <br />
                        <input v-model="user_email" class="form-control form-plot" type="email" />
                    </label>
                    <br />
                    <label class="text-orange">Escreva como você quer sua Impressão:
                        <br /><br />
                        <textarea class="form-control form-plot" rows="5" id="textArea" v-model="description" type="text"></textarea>
                    </label>
                    <br />
                    <button type="button" @click="subject = '<?php echo $subject ?>'; sendEmail()"
                        class="btn btn-b4 btn-info bg-orange border-0 mt-3 float-sm-right">PEDIR ORÇAMENTO</button>
                </form>
            </div>
        </div>
    </div>
</div>

<?php require 'app/views/partial/how_it_works-bar.php';
