
<div class="container my-md-5 pb-md-5 lp" id="lp">
    <div class="row">
        <div class="col-md-6 col-12 order-2 order-md-1">
        <span></span>
            <div class="img-responsive img-lp" alt="<?php echo $alt ?>" style="background-image: url('<?php echo $img_url ?>')">
            </div>
            <div class="mt-3 text-lg">

                <span class="text-orange font-weight-bold h2">
                    <?php echo $subtitle ?>
                </span>
                <ul class="no-decoration text-black font-weight-lighter mt-3">
                    <?php
                        foreach($li_es as $phrase):
                    ?>
                        <li>
                            -
                            <?php echo $phrase ?>
                        </li>
                        <?php
                        endforeach;
                    ?>
                </ul>
            </div>
        </div>
        <div class="col-md-6 details order-md-1 d-flex text-lg mt-md-0 mb-5 mb-md-0">
            <div class="col-12 m-0 ">
                <h1 class="text-orange h2 ">
                    <?php echo $title ?>
                </h1>
                <div class="col-12 mx-0 mt-3">
                    <form id="email_form">
                        <label class="text-orange">Nome
                            <br/>
                            <input class="form-control" v-model="user_name" type="text" />
                        </label>
                        <br/>
                        <label class="text-orange my-3">Email
                            <br/>
                            <input v-model="user_email" class="form-control" type="email" />
                        </label>
                        <br/>
                        <label class="text-orange">Telefone
                            <br/>
                            <input v-model="user_tel" v-mask="['(##) #### ####', '(##) # #### ####']" class="form-control" type="text" />
                        </label>
                        <br/>
                        <button type="button" @click="subject = '<?php echo $subject ?>'; sendEmail()" class="btn btn-b4 btn-info bg-blue border-0 mt-3 float-sm-right"><?php echo $button ?></button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<?php
    require 'app/views/partial/how_it_works-bar.php';

    require 'app/views/partial/link_building_bar.php';

?>

<div class="container my-3" id="commentBar">
    <div class="row">
        <div class="col-12 text-center">
            <h3 class="text-orange">
                FAÇA SUA IMPRESSÃO NA DELIVERYPRINT
            </h3>
        </div>
        <div class="col-12">
            <p class="text-grey font-weight-bold text-justify mx-5">
                <?php
                    if (empty($text)):
                ?>

                Pensou em impressão barata, pensou na DeliveryPrint, a sua Gráfica Digital Online. Impressão em preto e
                branco e impressão colorida com os menores preços do mercado, online e físico! Imprimir com qualidade
                garantida, do conforto de sua casa, você pode fazer o seu pedido de impressão online e receber na sua
                casa, até no mesmo dia! Entregas para todo o Brasil! Trabalhamos com diversas opções de papel e
                acabamento, encadernação em espiral, wire-o e capa dura para TCC. Papel Sulfite e Couchê, nos tamanhos
                A5, A4 e A3. Não perca tempo: imprimir com qualidade e preço baixo, é na DeliveryPrint: a impressora
                online com 5,0 estrelas no Google Avaliações!

                <?php
                    else:

                        echo $text;

                    endif;
                ?>
            </p>
        </div>
    </div>
</div>
