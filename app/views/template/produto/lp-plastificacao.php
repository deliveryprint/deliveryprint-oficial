<div class="container" id="emailDiv">
    <div class="row">
        <div class="col">
            <h1 class="text-orange my-3 f-28 f-sm-36">FAÇA SUA PLASTIFICAÇÃO AQUI!</h1>
            <input type="text" class="d-none" ref="subject_input" value="Orçamento de Plastificação | DeliveryPrint">
            <input type="text" class="d-none" ref="email_type" value="budget-request-description">
        </div>
    </div>
    <div class="row mb-5 border-bottom">
        <div class="col-12 col-md-6">
            <div class="row mb-5">
                <div class="col-xs-12 col-sm-6 text-orange d-flex align-items-center justify-content-center justify-content-md-start">
                    <i class="fa fa-th-list f-40 p-3 d-block"></i>
                    <h2 class="f-18 d-inline font-weight-normal mb-0 default-text" style="min-width: 120px;">Cardápios</h2>
                </div>
                <div class="col-xs-12 col-sm-6 text-orange d-flex align-items-center justify-content-center justify-content-md-start">
                    <i class="fa fa-files-o f-40 p-3"></i>
                    <h2 class="f-18 d-inline font-weight-normal mb-0 default-text" style="min-width: 120px;">Certificações</h2>
                </div>
                <div class="col-xs-12 col-sm-6 text-orange d-flex align-items-center justify-content-center justify-content-md-start">
                    <i class="fa fa-file-text-o f-40 p-3"></i>
                    <h2 class="f-18 d-inline font-weight-normal mb-0 default-text" style="min-width: 120px;">Documentos</h2>
                </div>
                <div class="col-xs-12 col-sm-6 text-orange d-flex align-items-center justify-content-center justify-content-md-start">
                    <i class="fa fa-area-chart f-40 p-3"></i>
                    <h2 class="f-18 d-inline font-weight-normal mb-0 default-text" style="min-width: 120px;">Cartazes</h2>
                </div>
            </div>
            <div class="row">
                <div class="col-12 mb-4 mt-0 mt-sm-5">
                    <h3 class="text-center text-orange f-20 f-sm-24 mb-1 font-weight-bold"> Faça seu orçamento pelo
                        Whatsapp</h3>
                    <p class="text-orange f-40 text-center">
                        <i class="fa fa-whatsapp text-green-whatsapp p-2"> </i>
                        <span class="f-16 f-sm-18"> (11) 9.9281-2284</span>
                    </p>
                </div>
            </div>
        </div>
        <div class="col-12 col-md-6 mb-5">
            <h4 class="text-orange f-24 f-sm-28 mb-2">Peça seu Orçamento, é rápido!</h4>
            <div class="form" action="" id="email_form">
                <div class="form-group">
                    <input class="form-control" type="text" name="nomeCliente" placeholder="Nome" v-model="user_name">
                </div>
                <div class="form-group">
                    <input class="form-control" type="text" name="email" placeholder="Email" v-model="user_email">
                </div>
                <div class="form-group">
                    <input class="form-control mb-3" type="tel" v-mask="['(##)####-####', '(##)#####-####']" name="telephone"
                        placeholder="Telefone" v-model="user_tel">
                </div>
                <textarea class="form-control p-2 mb-3" rows="5" name="clientText" placeholder="Escreva aqui o que deseja imprimir e a quantidade de folhas"
                    v-model="description"></textarea>

                <button type="button" class="btn btn-b3 btn-b6-xs border-radius-lg btn btn-orange border border-solid d-block ml-auto mb-2"
                    @click="sendEmail()">
                    ENVIAR
                </button>

            </div>

        </div>
    </div>
</div>

<?php require 'app/views/partial/comments.php';
