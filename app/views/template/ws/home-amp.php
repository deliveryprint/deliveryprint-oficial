<section class="bg-solicitation text-center pb-5">
    <div class="container" id="main-div">
        <h1 class="text-white pt-2 pt-sm-5 pb-3 ">Impressão e <br class="d-block d-sm-none">Encadernação Online</h1>
        <a href="/impressao-online" class="btn btn-orange mb-1 mt-1 float-none btn-b2" style="text-decoration: none; border-color: #fff">Ver preços</a>
        <div class="mt-5 jumbo-solicitation">
            <button class="btn text-white h3 d-sm-inline-block mr-4 border-0 btn-b6-xs bg-transparent text-uppercase" id="jumbo-btn1">
                <i class="fa fa-usd" aria-hidden="true"></i>
                Melhores Preços
            </button>
            <button class="btn text-white h3 d-sm-inline-block mr-4 border-0 btn-b6-xs bg-transparent text-uppercase" id="jumbo-btn2">
                <i class="fa fa-truck" aria-hidden="true"></i>
                Entregamos Rápido
            </button>
            <button class="btn text-white h3 d-sm-inline-block mr-4 border-0 btn-b6-xs bg-transparent text-uppercase" id="jumbo-btn3">
                <i class="fa fa-shield" aria-hidden="true"></i>
                Qualidade Superior
            </button>
        </div>
    </div>
</section>

<section class="d-flex justify-content-center flex-wrap solicitation-items">
    <div class="card-bg card-bg-c1 mr-4 text-left mt-0">
        <a href="/impressao-apostila" class="float-none">
            <div class="card-container">
                <p class="card-txt text-white d-inline-block p-2 h4" style="padding: .5rem">Mais vendido</p>
                <h2 class="card-txt text-white d-inline-block p-2 h2" style="padding: .5rem">Imprimir Apostila</h2>
            </div>
        </a>
    </div>
    <div class="card-bg card-bg-c2 mr-4 text-left">
        <a href="/impressao-preto-branco/amp" class="float-none">
            <div class="card-container">
                <p class="card-txt text-white d-inline-block p-2 h4" style="padding: .5rem">Custo x Benefício</p>
                <p class="card-txt text-white d-inline-block p-2 h2" style="padding: .5rem; width: 100%;">Impressão em PB A4</p>
            </div>
        </a>
    </div>
    <div class="card-bg card-bg-c3 mr-4 text-left">
        <a href="/impressao-colorida" class="float-none">
            <div class="card-container">
                <p class="card-txt text-white d-inline-block p-2 h4" style="padding: .5rem">Menor preço!</p>
                <h3 class="card-txt text-white d-inline-block p-2 h2" style="padding: .5rem">Impressão Colorida</h3>
            </div>
        </a>
    </div>
</section>

<!-- BARRA VERMELHA -->

<h3 class="text-center text-orange mb-4 mt-0 hide-m h2">Processo Simples e Online</h3>

<div id="how_it_works" class="bg-smoother-og text-white text-center mt-1">
    <div class="row font-weight-lighter mr-0">
        <div class="col-12 col-md-6 mk bg-orange"></div>
        <div class="col-md-6 col-12 mk bg-orange  clip chevron b"></div>
        <div class="col-md-1 offset-half d-none d-md-block"></div>

        <div class="col-md-3 col-12 bg-orange">
            <div class="col-12 col-hg-6 py-5" id="pos_left">
                <i class="fa fa-sliders fa-5x  mt-5 mt-md-0"></i>
                <p class="h3" style="width: 100%">Configuração</p>
                <p class="px-2 center" style="font-weight: 10">
                    Escolha entre impressão colorida ou P&amp;B, tipo de papel, tamanho e acabamento.
                </p>
            </div>
        </div>

        <div class="col-md-4 col-lg-3 py-md-5 col-12 bg-smooth-og clip chevron b center">
            <i class="fa fa-cloud-upload fa-5x"></i>
            <p class="h3 text-center" style="width: 100%">Upload</p>
            <p class="px-2 center" style="font-weight: 10">
                Faça upload dos arquivos a serem impressos ou informe o número de páginas.
            </p>
        </div>

        <div class="col-12 col-md-2 py-5 bg-smoother-og">
            <div class="col-12 col-hg-6" id="pos_right">
                <i class="fa fa-truck fa-5x fa-flip-horizontal"></i>
                <p class="h3" style="width: 100%">Receba em Casa</p>
                <p class="px-2 pb-3 center" style="font-weight: 10">
                    Pronto! Agora é só esperar chegar no endereço que você escolheu ;)
                </p>
            </div>
        </div>
        <div class="col-md-1 d-none d-md-block"></div>
    </div>
</div>

<!-- COMMENTS -->

<div class="container  mt-3">
    <div class="row pt-3">

        <!-- nw avaliations -->
        <div class="col-12" id="nw_avaliations">
            <h2 class="text-orange text-center pb-3">A primeira impressão é a que fica!</h2>

            <div class="row text-center">

                <div class="col-6 col-md-3  text-yellow offset-md-3">
                    <span class="h1 default-text font-weight-lighter font-italic" style="font-weight: 100">5,0</span><br>
                    <div class="stars ">
                        <i class="fa fa-star"></i>
                        <i class="fa fa-star"></i>
                        <i class="fa fa-star"></i>
                        <i class="fa fa-star"></i>
                        <i class="fa fa-star"></i>
                    </div>
                    <span class="text-sm text-orange font-italic">Classificação no Google</span>
                </div>

                <div class="col-6 col-md-3  text-yellow">
                    <span class="h1 default-text font-weight-lighter font-italic" style="font-weight: 100">4,8</span><br>
                    <div class="stars ">
                        <i class="fa fa-star"></i>
                        <i class="fa fa-star"></i>
                        <i class="fa fa-star"></i>
                        <i class="fa fa-star"></i>
                        <i class="fa fa-star-half-o"></i>
                    </div>
                    <span class="text-sm text-orange font-italic">Classificação no Facebook</span>
                </div>

            </div>
        </div>
        <!-- end avaliations -->

        <!-- comments -->
        <div class="col-12 text-center mb-5" id="customer_comments">

            <div class="row">
                <div class="col-6"></div>
                <div class="col-6"></div>
            </div>

            <div class="row text-grey mt-5">
                <div class="col-12 col-md-4">
                    <p class="font-weight-bold text-orange text-hg pb-4">Thomas Novaes</p>
                    <p class="font-italic text-md">"Muito ágil e de ótima qualidade, fui chamado para receber a encomenda no exato momento em que rastreava
                        o pedido, ainda tinham 2 dias de prazo. Ficou exatamente da forma que queria. Continuem assim, estão
                        de parabéns."</p>
                    <div class="stars text-orange p-4">
                        <i class="fa fa-star"></i>
                        <i class="fa fa-star"></i>
                        <i class="fa fa-star"></i>
                        <i class="fa fa-star"></i>
                        <i class="fa fa-star"></i>
                    </div>

                    </span>
                </div>
                <div class="col-12 col-md-4">
                    <p class="font-weight-bold text-orange text-hg pb-4">Luisa de Sousa</p>
                    <p class="font-italic text-md">"Todas as experiências foram ótimas. Impressão de alta qualidade, entrega rápida, serviço eficiente,
                        super prático e o preço é bom. Estou muito satisfeita. Muito obrigada pelas entregas. #Recomendo!"</p>
                    <div class="stars text-orange p-4">
                        <i class="fa fa-star"></i>
                        <i class="fa fa-star"></i>
                        <i class="fa fa-star"></i>
                        <i class="fa fa-star"></i>
                        <i class="fa fa-star"></i>
                    </div>
                </div>
                <div class="col-12 col-md-4">
                    <p class="font-weight-bold text-orange text-hg pb-4">Adriana Borges</p>
                    <p class="font-italic text-md">"Serviço de excelente qualidade, funcionários prontos para o melhor atendimento ao cliente, só tenho
                        o que agradecer a essa empresa, que me socorreu em uma urgência, e só me surpreendeu em todos os
                        quesitos! Super recomendo."</p>
                    <div class="stars text-orange p-4">
                        <i class="fa fa-star"></i>
                        <i class="fa fa-star"></i>
                        <i class="fa fa-star"></i>
                        <i class="fa fa-star"></i>
                        <i class="fa fa-star"></i>
                    </div>
                </div>
            </div>
        </div>
        <!-- end comments -->
    </div>
</div>
