<div id="verify-app" class="container text-center">
    <div class="row">
        <div class="col-10 offset-1 col-sm-6 col-lg-4 offset-sm-3 offset-lg-4 ">
            <h3 class="text-orange pb-5 mt-4">Verificação de E-mail</h3>

            <div class="page-loader-single" v-if="loading">
                <div class="sk-fading-circle">
                    <div class="sk-circle1 sk-circle"></div>
                    <div class="sk-circle2 sk-circle"></div>
                    <div class="sk-circle3 sk-circle"></div>
                    <div class="sk-circle4 sk-circle"></div>
                    <div class="sk-circle5 sk-circle"></div>
                    <div class="sk-circle6 sk-circle"></div>
                    <div class="sk-circle7 sk-circle"></div>
                    <div class="sk-circle8 sk-circle"></div>
                    <div class="sk-circle9 sk-circle"></div>
                    <div class="sk-circle10 sk-circle"></div>
                    <div class="sk-circle11 sk-circle"></div>
                    <div class="sk-circle12 sk-circle"></div>
                </div>
            </div>

            <div v-if="!loading && !error">
                <p class="mb-3">E-mail verificado com sucesso! Bem-vindo à Delivery Print!</p>
                <a href="/impressao-online" class="btn btn-orange border-radius-md mb-3">Faça um pedido</a>
                <p class="mb-3">ou</p>
                <a href="/carrinho" class="btn btn-orange border-radius-md mb-3">Vá para o carrinho</a>
            </div>

            <div v-if="!loading && error">
                <p>{{ errorMsg }}</p>
            </div>

        </div>
    </div>
</div>
