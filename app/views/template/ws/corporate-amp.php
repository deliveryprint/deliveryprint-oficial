<section class="corporate-main">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-body">
                        <h1 class="text-orange">Peça suas impressões de maneira fácil!</h1>
                        <h2 class="text-orange">Você já tem muita coisa para se preocupar. Deixe suas impressões com a gente, é a nossa especialidade!</h2>
                        <div class="clearfix">
                            <div class="main-btn-1"><a href="/impressao-online" class="btn btn-b3 btn-primary">Iniciar Orçamento</a></div>
                            <div class="main-btn-2"><a href="/ajuda/faq" class="btn btn-b3 btn-primary">Tirar Dúvidas</a></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="features bg-lightgrey pt-5">
    <div class="container">
        <div class="row">
            <div class="col-md-4">
                <div class="card mb-5">
                    <div class="card-body text-center p-3" style="width: auto;">
                        <img class="feature-img mb-3" src="/img/icone-desconto.png" alt="Descontos exclusivos para empresas na DeliveryPrint">
                        <p class="feature-text-1 mb-3">Descontos Exclusivos</p>
                        <h4 class="text-orange feature-text-2 mb-3">15%</h4>
                        <h3 class="feature-text-3 mx-auto" style="max-width: 230px">
                            <strong>mais barato</strong> do que comprar pelo site comum
                        </h3>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="card mb-5">
                    <div class="card-body text-center p-3" style="width: auto;">
                        <img class="feature-img mb-3" src="/img/icone-delivery.png" alt="Frete grátis na DeliveryPrint">
                        <p class="feature-text-1 mb-3">Entregas Personalizadas</p>
                        <h4 class="text-orange feature-text-2 mb-3">FRETE GRÁTIS</h4>
                        <h3 class="feature-text-3 mx-auto" style="max-width: 190px">
                            para pedidos a partir de <strong>R$ 150,00</strong> em SP
                        </h3>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="card mb-5">
                    <div class="card-body text-center p-3" style="width: auto;">
                        <img class="feature-img mb-3" src="/img/icone-chat.png" alt="Atendimento humano e eficiente na DeliveryPrint">
                        <p class="feature-text-1 mb-3">Atendimento Humano</p>
                        <h4 class="text-orange feature-text-2 mb-3">EFICIÊNCIA</h4>
                        <h3 class="feature-text-3 mx-auto" style="max-width: 250px">
                            e <strong>agilidade comprovadas</strong>. Veja nossas avaliações no Google!
                        </h3>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="container">
    <?php require 'app/views/partial/comments.php'; ?>
</section>

<style>
    .card {
        position: relative;
        display: flex;
        flex-direction: column;
        min-width: 0;
        word-wrap: break-word;
        background-color: #fff;
        background-clip: border-box;
        border: 1px solid rgba(0, 0, 0, 0.125);
        border-radius: 0.25rem;
    }

    .card-body {
        flex: 1 1 auto;
        margin-bottom: 10px;
        margin-top: 10px;
        margin-left: 15px;
    }

    .btn {
        display: inline-block;
        font-weight: 400;
        text-align: center;
        white-space: nowrap;
        vertical-align: middle;
        user-select: none;
        border: 1px solid transparent;
        padding: 0.375rem 0.75rem;
        font-size: 1rem;
        line-height: 1.5;
        border-radius: 0.25rem;
        transition: color 0.15s ease-in-out, background-color 0.15s ease-in-out, border-color 0.15s ease-in-out, box-shadow 0.15s ease-in-out;
    }

    .btn-primary {
        color: #fff;
        background-color: #007bff;
        border-color: #007bff;
    }

    .bg-lightgrey {
        background-color: #f3f3f3;
    }

    .mb-3 {
        margin-bottom: 1rem !important;
    }

    .p-3 {
        padding: 1rem !important;
    }



    .corporate-main {
        min-height: 526px;
        background: url('/img/coffee-break.jpg');
        background-size: cover;
        background-position: center;
        padding-top: 40px;
        padding-bottom: 40px;
    }

    .corporate-main .card {
        max-width: 600px;
    }

    .corporate-main .card .card-body {
        padding: 40px;
        width: auto;
    }

    .corporate-main .card .card-body h1,
    .corporate-main .card .card-body h2 {
        font-size: 32px;
        margin-bottom: 50px;
    }

    .corporate-main .card .card-body .btn-b3 {
        font-size: 22px;
        line-height: 22px;
        border-radius: 12px;
        text-transform: uppercase;
        padding: 22px 20px;
        min-width: auto;
        height: auto;
    }

    .corporate-main .card .card-body .btn-b3:hover,
    .corporate-main .card .card-body .btn-b3:active,
    .corporate-main .card .card-body .btn-b3:focus {
        background-color: #9DCB7D;
        border-color: #9DCB7D;
    }

    .corporate-main .card .card-body .main-btn-1 {
        float: left;
    }

    .corporate-main .card .card-body .main-btn-2 {
        float: right;
    }

    @media (max-width: 575px) {
        .corporate-main {
            min-height: auto;
            padding-top: 20px;
            padding-bottom: 20px;
        }

        .corporate-main .card-body {
            text-align: center;
            padding: 20px;
        }

        .corporate-main .card-body h1,
        .corporate-main .card-body h2 {
            font-size: 26px;
            margin-bottom: 30px;
        }

        .corporate-main .card-body .btn-b3 {
            font-size: 18px;
            line-height: 18px;
            padding: 18px 16px;
        }

        .corporate-main .card-body .main-btn-1 {
            margin-bottom: 15px;
        }

        .corporate-main .card-body .main-btn-1,
        .corporate-main .card-body .main-btn-2 {
            float: none;
            text-align: center;
        }

        .corporate-main .card-body .main-btn-1 a,
        .corporate-main .card-body .main-btn-2 a {
            float: none;
        }
    }

    .features .card {
        border-radius: 20px;
    }

    .feature-img {
        height: 65px;
    }

    .feature-text-1 {
        font-family: 'Economica', sans-serif !important;
        font-size: 24px;
        text-transform: uppercase;
    }

    .feature-text-2 {
        font-size: 32px;
    }

    .feature-text-3 {
        font-size: 24px;
        font-weight: normal;
        margin-bottom: 0;
    }
</style>
