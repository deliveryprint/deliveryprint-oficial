<div class="container">
    <div class="row">
        <div class="container col-sm-12" id="container">
            <div class="row">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item">
                            <a href="/">Home</a>
                        </li>
                        <li class="breadcrumb-item active" aria-current="page">Faq</li>
                    </ol>
                </nav>
            </div>
        </div>
    </div>

    <div class="faq-container" id="faq">

        <faq></faq>

        <!-- <button class="btn btn-b1 btn-orange d-block mx-auto" @click="openChat">Ainda tem dúvidas? Clique aqui!</button> -->

    </div>

    <?php require 'app/views/partial/comments.php'; ?>

</div>
