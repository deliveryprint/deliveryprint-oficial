<div class="container hiw-container">

    <section class="mb-5 text-center">
        <h1 class="text-center text-orange hiw-h1">Como funciona nosso serviço de Impressão?</h1>

        <div class="row">
            <div class="col-md-4">
                <div class="img-step-by-step">
                    <div class="step-number">1</div>
                    <amp-img src="/img/pagina_de_pedido.png" alt="Página de pedidos" class="img-fluid mb-3" width="242" height="232" layout="responsive"></amp-img>
                </div>
                <p class="text-hiw mb-3">Aqui você escolhe como quer sua impressão, se terá algum acabamento e anexa os arquivos em PDF que deseja imprimir</p>
            </div>
            <div class="col-md-4">
                <div class="img-step-by-step">
                    <div class="step-number">2</div>
                    <amp-img src="/img/frete.png" alt="Página de pagamentos" class="img-fluid mb-3" width="242" height="232" layout="responsive"></amp-img>
                </div>
                <p class="text-hiw mb-3">Em seguida irá definir qual opção de entrega será a melhor para seu pedido. Cada opção de entrega tem seu preço estipulado de acordo com agilidade, distancia e configuração de seu pedido de impressão.</p>
            </div>
            <div class="col-md-4">
                <div class="img-step-by-step">
                    <div class="step-number">3</div>
                    <amp-img src="/img/pgto.png" alt="Escolha do frete" class="img-fluid mb-3" width="242" height="232" layout="responsive"></amp-img>
                </div>
                <p class="text-hiw mb-3">No fim, você poderá finalizar seu pedido de impressão escolhendo pagar em boleto bancário a vista ou no seu cartão de crédito, com toda segurança da IUGU. Ai o resto é com a gente, iremos imprimir e entregar para você até a data escolhida.</p>
            </div>
        </div>

        <a href="/impressao-online" class="btn btn-orange btn-hiw">Fazer Orçamento</a>
    </section>

    <section class="my-5">
        <div class="row align-items-center">
            <div class="col-md-6">
                <h2 class="text-center text-orange my-3">E por que Imprimir na DeliveryPrint?</h2>
                <p class="text-center text-hiw mb-3">Além de toda segurança em seu pedido, nosso sistema proporciona que você tenha mais vantagens como:</p>
                <ul class="mb-5">
                    <li><p class="text-hiw mb-1">- Temos serviço de entrega expresso</p></li>
                    <li><p class="text-hiw mb-1">- Atendimento pessoal, rápido e sincero</p></li>
                    <li><p class="text-hiw mb-1">- Aqui você pode pagar no seu cartão em até 3x acima de R$100</p></li>
                    <li><p class="text-hiw mb-1">- Trabalhamos apenas com máquinas profissionais</p></li>
                    <li><p class="text-hiw mb-1">- Preço abaixo da média, mesmo sendo de ótima qualidade</p></li>
                    <li><p class="text-hiw mb-1">- Se errarmos, imprimimos novamente seu trabalho.</p></li>
                </ul>
                <div class="text-center mb-3">
                    <a href="/impressao-online" class="btn btn-orange btn-hiw">Fazer Pedido</a>
                </div>
            </div>
            <div class="col-md-6">
                <amp-img src="/img/imagem_impressora.jpg" class="img-fluid" alt="Imagem impressora" width="540" height="480" layout="responsive"></amp-img>
            </div>
        </div>
    </section>

    <?php require 'app/views/partial/comments.php'; ?>

</div>

<style>
a {
    float: none !important;
}

.text-hiw {
    font-size: 20px;
    font-family: "Economica" !important;
}

.btn-hiw {
    font-weight: bold;
    font-size: 24px;
    min-width: 220px;
    height: 50px;
}

.img-step-by-step {
    position: relative;
}

.img-step-by-step .step-number {
    position: absolute;
    top: 10px;
    left: 10px;
    height: 44px;
    width: 44px;
    border-radius: 22px;
    background-color: #fc4a1a;
    color: rgb(255, 255, 255);
    font-size: 28px;
    line-height: 44px;
    font-weight: bold;
}

.img-fluid {
    max-width: 100%
}

.mb-3 {
    margin-bottom: 1rem;
}

@media (min-width: 1200px) {
    .img-step-by-step img {
        max-height: 232px;
    }
}


ul {
    float: none;
    list-style-type: none;
}

ul li {
    float: none;
    color: #807F7F;
}

.hiw-h1, h3 {
    font-size: 24px;
}

@media (min-width: 560px) {
    .hiw-h1, h3 {
        font-size: 36px;
    }
}

</style>
