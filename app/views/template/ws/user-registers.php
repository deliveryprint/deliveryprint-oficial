<section id="breadcrumb">
    <div class="container">
        <h4 class="text-orange accordion-mob">Minha Conta</h4>
        <div class="row">
            <nav aria-label="breadcrumb" style="font-size: 14px;">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item active">
                        <a href="#">Home</a>
                    </li>
                    <li class="breadcrumb-item active" aria-current="page">Minha Conta</li>
                    <li class="breadcrumb-item" aria-current="page">Dados Cadastrais</li>
                </ol>
            </nav>
        </div>
    </div>
</section>

<section id="forms">
    <div class="container">
        <div class="col-md-12">
            <div class="row">
                <div class="col-md-8 tab-desk">
                    <div class="row">
                        <h3 class="title-forms">Dados Cadastrais</h3>
                    </div>
                    <div class="row tablist">
                        <ul class="nav nav-tabs" role="tablist">
                            <li class="nav-item abas">
                                <a class="nav-link navl active" data-toggle="tab" href="#dados" role="tab" aria-controls="dados" aria-selected="true">
                                    <i class="ifa fa fa-user"></i>Dados Pessoais </a>
                            </li>
                            <li class="nav-item abas">
                                <a class="nav-link navl" data-toggle="tab" href="#address" role="tab" aria-controls="enderecos" aria-selected="false">
                                    <i class="ifa fa fa-map-marker"></i>Endereços</a>
                            </li>
                            <li class="nav-item abas">
                                <a class="nav-link navl" data-toggle="tab" href="#alterar-senha" role="tab" aria-controls="alterar-senha" aria-selected="false">
                                    <i class=" ifa fa fa-unlock-alt"></i>Alterar Senha</a>
                            </li>
                        </ul>
                    </div>
                    <div class="tab-content">

                        <!--Tab Dados Cadastrais-->
                        <div id="dados" class="tab-pane in active">
                            <div class="col-md-8 no-gutters">
                                <form method="post" action="">
                                    <div class="form-group">
                                        <input class="form-control" type="text" name="nomeCliente" v-model="user.name" placeholder="Nome">
                                    </div>
                                    <div class="form-group">
                                        <input class="form-control" type="email" name="emailCliente" v-model="user.email" placeholder="E-mail">
                                    </div>
                                    <div class="form-group">
                                        <input class="form-control" type="text" name="dtNascimento" v-mask="'##/##/####'" v-model="user.birth" placeholder="Data de Nascimento">
                                    </div>
                                    <div class="form-group">
                                        <input class="form-control mask-cpf-cnpj" type="text" name="cpfCliente" v-mask="['###.###.###-##', '##.###.###/####-##']" v-model="user.cpf" placeholder="CPF/CNPJ">
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6 phone-cell">
                                            <div class="form-group">
                                                <input class="form-control" type="text" name="telCliente" v-mask="['(##) ####-####', '(##) #####-####']" v-model="user.tel"
                                                    placeholder="Telefone (Opcional)">
                                            </div>
                                        </div>
                                        <div class="col-md-6 phone-cell">
                                            <div class="form-group">
                                                <input class="form-control mask-phone" type="text" name="celCliente" v-mask="['(##) ####-####', '(##) #####-####']" v-model="user.cel"
                                                    placeholder="Celular">
                                            </div>
                                        </div>
                                    </div>
                                    <button class="btn btn-b3 btn-orange border-0" type="button" value="SALVAR" v-on:click="updateCustomerData()"> SALVAR </button>
                                </form>
                            </div>
                        </div>
                        <!--FIM Tab Dados Cadastrais-->
                        <!--Tab Endereços-->
                        <div id="address" class="tab-pane fade">
                            <div class="col-md-8 no-gutters">
                                <form>
                                    <div class="col-md-12 no-gutters" v-if="address.length < 2">
                                        <div class="checkbox">
                                            <label class="add-address radio-btn" v-on:change="newAddress()" >
                                                <input class="form-check-input" name="gridRadios" type="radio" v-model="curAddrIdx">
                                                <b v-bind:class="{'text-orange': curAddrIdx < 0}">Adicionar Endereço</b>
                                            </label>
                                        </div>
                                    </div>
                                    <div class="col-md-12 no-gutters" v-for="(addr, index) in address">
                                        <div class="checkbox">
                                            <label class="text-grey radio-btn" v-on:change="setCurAddr(index)">
                                                <input class="form-check-input" name="gridRadios" type="radio" v-model="curAddrIdx">
                                                <span class="edit-address-line"  v-bind:class="{'text-orange': curAddrIdx === index}">
                                                    {{addr.street}}, {{addr.number}}
                                                    <br> {{addr.district}}, {{addr.city}} {{addr.uf}} – Brasil.
                                                </span>
                                                <span class="edit-text" v-bind:class="{'text-orange': curAddrIdx === index}">EDITAR</span>
                                            </label>
                                        </div>
                                    </div>
                                </form>
                                <div class="col-md-12">
                                    <form class="" method="post" action="">
                                        <div class="form-group">
                                            <input class="form-control" type="text" name="cep" v-mask="'#####-###'" v-on:keyup="searchZip()" v-model="curAddress.zip"
                                                placeholder="CEP">
                                        </div>
                                        <div class="form-group">
                                            <input class="form-control" type="text" name="logradouro" v-model="curAddress.street" placeholder="Rua">
                                        </div>

                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <input class="form-control" type="number" name="numero" v-model="curAddress.number" min="1" placeholder="Número do endereço">
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <input class="form-control" type="text" name="complemento" v-model="curAddress.compl" placeholder="Complemento">
                                                </div>
                                            </div>
                                        </div>



                                        <div class="form-group">
                                            <input class="form-control" type="text" name="bairro" v-model="curAddress.district" placeholder="Bairro">
                                        </div>
                                        <div class="form-group">
                                            <input class="form-control" type="text" name="localidade" v-model="curAddress.city" placeholder="Cidade">
                                        </div>
                                        <div class="form-group">
                                            <select class="form-control" name="uf" v-model="curAddress.uf" required>
                                                <option value="" disabled selected>Estado</option>
                                                <option value="AC">Acre</option>
                                                <option value="AL">Alagoas</option>
                                                <option value="AP">Amapá</option>
                                                <option value="AM">Amazonas</option>
                                                <option value="BA">Bahia</option>
                                                <option value="CE">Ceará</option>
                                                <option value="DF">Distrito Federal</option>
                                                <option value="ES">Espírito Santo</option>
                                                <option value="GO">Goiás</option>
                                                <option value="MA">Maranhão</option>
                                                <option value="MT">Mato Grosso</option>
                                                <option value="MS">Mato Grosso do Sul</option>
                                                <option value="MG">Minas Gerais</option>
                                                <option value="PA">Pará</option>
                                                <option value="PB">Paraíba</option>
                                                <option value="PR">Paraná</option>
                                                <option value="PE">Pernambuco</option>
                                                <option value="PI">Piauí</option>
                                                <option value="RJ">Rio de Janeiro</option>
                                                <option value="RN">Rio Grande do Norte</option>
                                                <option value="RS">Rio Grande do Sul</option>
                                                <option value="RO">Rondônia</option>
                                                <option value="RR">Roraima</option>
                                                <option value="SC">Santa Catarina</option>
                                                <option value="SP">São Paulo</option>
                                                <option value="SE">Sergipe</option>
                                                <option value="TO">Tocantins</option>
                                            </select>
                                        </div>
                                        <button class="btn btn-b3 btn-orange border-0" value="SALVAR" type="button" v-on:click="saveAddress()"> Salvar </button>
                                    </form>
                                </div>
                            </div>
                        </div>
                        <!--FIM Tab Endereços-->
                        <!--Tab Alterar Senha-->
                        <div id="alterar-senha" class="tab-pane fade">
                            <div class="col-md-8">
                                <form method="post" action="">
                                    <div class="form-group row">
                                        <div class="col-10">
                                            <label for="example-email-input" style="color: #FC4A1A;"><b>Digite sua senha atual</b></label>
                                            <input class="form-control atual-password" type="password" name="atual_password" v-model="password.currentPassword">
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <div class="col-10">
                                            <label for="example-email-input" style="color: #FC4A1A;"><b>Digite sua nova senha</b></label>
                                            <input class="form-control new-password" type="password" name="new_password" v-model="password.newPassword">
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <div class="col-10">
                                            <label for="example-email-input" style="color: #FC4A1A;"><b>Confirme a nova senha</b></label>
                                            <input class="form-control conf-password" type="password" name="conf_password" v-model="password.newPasswordAgain">
                                        </div>
                                    </div>
                                    <div class="alert alert-danger alert-new-senha d-none"></div>
                                    <button class="btn btn-b3 btn-orange border-0" value="SALVAR" type="button" v-on:click="changePassword()"> Salvar </button>
                                </form>
                            </div>
                        </div>
                        <!--FIMTab Alterar Senha-->
                    </div>
                </div>
                <div id="accordion" class="accordion-mob col-sm-12">
                    <div class="card">
                        <div class="card-header" id="headingOne">
                            <h5 class="mb-0">
                                <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseOne" aria-expanded="false" aria-controls="collapseTwo">
                                    <i class="ifa fa fa-user"></i>Dados Pessoais <i class="ifa fa fa-angle-down"></i>
                                </button>
                            </h5>
                        </div>
                        <div id="collapseOne" class="collapse text-center" aria-labelledby="headingOne" data-parent="#accordion">
                            <div class="card-body text-center">
                                <form method="post" action="">
                                    <div class="form-group">
                                        <input class="form-control" type="text" name="nomeCliente" v-model="user.name" placeholder="Nome">
                                    </div>
                                    <div class="form-group">
                                        <input class="form-control" type="email" name="emailCliente" v-model="user.email" placeholder="E-mail">
                                    </div>
                                    <div class="form-group">
                                        <input class="form-control" type="text" name="dtNascimento" v-mask="'##/##/####'" v-model="user.birth" placeholder="Data de Nascimento">
                                    </div>
                                    <div class="form-group">
                                        <input class="form-control mask-cpf-cnpj" type="text" name="cpfCliente" v-mask="['###.###.###-##', '##.###.###/####-##']" v-model="user.cpf" placeholder="CPF/CNPJ">
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6 phone-cell">
                                            <div class="form-group">
                                                <input class="form-control" type="text" name="telCliente" v-mask="['(##) ####-####', '(##) #####-####']" v-model="user.tel"
                                                    placeholder="Telefone (Opcional)">
                                            </div>
                                        </div>
                                        <div class="col-md-6 phone-cell">
                                            <div class="form-group">
                                                <input class="form-control mask-phone" type="text" name="celCliente" v-mask="['(##) ####-####', '(##) #####-####']" v-model="user.cel"
                                                    placeholder="Celular">
                                            </div>
                                        </div>
                                    </div>
                                    <button class="btn btn-b6 btn-orange border-0" type="button" value="SALVAR" v-on:click="updateCustomerData()"> SALVAR </button>
                                </form>
                            </div>
                        </div>
                    </div>

                    <div class="card">
                        <div class="card-header" id="headingTwo">
                        <h5 class="mb-0">
                            <button class="btn btn-link collapsed text-orange" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                                <i class="ifa fa fa-map-marker"></i>Endereços<i class="ifa fa fa-angle-down as"></i>
                            </button>
                        </h5>
                        </div>
                        <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordion">
                            <div class="card-body">
                                <h4 class="text-orange text-left">Endereço principal</h4>
                                <div class="col-md-12 no-gutters text-center">
                                        <form class="text-left">

                                            <div class="col-md-12 no-gutters" v-if="address.length < 2">
                                                <div class="checkbox">
                                                    <label class="add-address radio-btn" v-on:change="newAddress()" >
                                                        <input class="form-check-input" name="gridRadios" type="radio" v-model="curAddrIdx">
                                                        <b v-bind:class="{'text-orange': curAddrIdx < 0}">Adicionar Endereço</b>
                                                    </label>
                                                </div>
                                            </div>
                                            <div class="col-md-12 no-gutters" v-for="(addr, index) in address">
                                                <div class="checkbox pl-3">
                                                    <label class="text-grey radio-btn row" v-on:change="setCurAddr(index)">
                                                        <input class="form-check-input" name="gridRadios" type="radio" v-model="curAddrIdx">
                                                        <span class="edit-address-line"  v-bind:class="{'text-orange': curAddrIdx == index}">
                                                            {{addr.street}}, {{addr.number}}
                                                            <br> {{addr.district}}, {{addr.city}} {{addr.uf}} – Brasil.
                                                        </span>
                                                        <span class="edit-text" v-bind:class="{'text-orange': curAddrIdx == index}">EDITAR</span>
                                                    </label>
                                                </div>
                                            </div>
                                        </form>
                                        <div class="col-md-12">
                                            <form class="" method="post" action="">
                                                <div class="form-group">
                                                    <input class="form-control" type="text" name="cep" v-mask="'#####-###'" v-on:keyup="searchZip()" v-model="curAddress.zip"
                                                        placeholder="CEP">
                                                </div>
                                                <div class="form-group">
                                                    <input class="form-control" type="text" name="logradouro" v-model="curAddress.street" placeholder="Rua">
                                                </div>

                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <input class="form-control" type="text" name="numero" v-model="curAddress.number" placeholder="Número do endereço">
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <input class="form-control" type="text" name="complemento" v-model="curAddress.compl" placeholder="Complemento">
                                                        </div>
                                                    </div>
                                                </div>



                                                <div class="form-group">
                                                    <input class="form-control" type="text" name="bairro" v-model="curAddress.district" placeholder="Bairro">
                                                </div>
                                                <div class="form-group">
                                                    <input class="form-control" type="text" name="localidade" v-model="curAddress.city" placeholder="Cidade">
                                                </div>
                                                <div class="form-group">
                                                    <select class="form-control" name="uf" v-model="curAddress.uf" required>
                                                        <option value="" disabled selected>Estado</option>
                                                        <option value="AC">Acre</option>
                                                        <option value="AL">Alagoas</option>
                                                        <option value="AP">Amapá</option>
                                                        <option value="AM">Amazonas</option>
                                                        <option value="BA">Bahia</option>
                                                        <option value="CE">Ceará</option>
                                                        <option value="DF">Distrito Federal</option>
                                                        <option value="ES">Espírito Santo</option>
                                                        <option value="GO">Goiás</option>
                                                        <option value="MA">Maranhão</option>
                                                        <option value="MT">Mato Grosso</option>
                                                        <option value="MS">Mato Grosso do Sul</option>
                                                        <option value="MG">Minas Gerais</option>
                                                        <option value="PA">Pará</option>
                                                        <option value="PB">Paraíba</option>
                                                        <option value="PR">Paraná</option>
                                                        <option value="PE">Pernambuco</option>
                                                        <option value="PI">Piauí</option>
                                                        <option value="RJ">Rio de Janeiro</option>
                                                        <option value="RN">Rio Grande do Norte</option>
                                                        <option value="RS">Rio Grande do Sul</option>
                                                        <option value="RO">Rondônia</option>
                                                        <option value="RR">Roraima</option>
                                                        <option value="SC">Santa Catarina</option>
                                                        <option value="SP">São Paulo</option>
                                                        <option value="SE">Sergipe</option>
                                                        <option value="TO">Tocantins</option>
                                                    </select>
                                                </div>
                                                <button class="btn btn-b6 btn-orange border-0" value="SALVAR" type="button" v-on:click="saveAddress()"> Salvar </button>
                                            </form>
                                        </div>
                                    </div>
                            </div>
                        </div>
                    </div>

                    <div class="card">
                        <div class="card-header" id="headingThree">
                            <span class="panel-icon icon-arrow-bottom text-gray"></span>
                            <h5 class="mb-0">
                                <button class="btn btn-link collapsed text-orange" data-toggle="collapse" data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                                    <i class=" ifa fa fa-unlock-alt"></i>Alterar Senha <i class="ifa fa fa-angle-down"></i>
                                </button>
                            </h5>
                        </div>
                        <div id="collapseThree" class="collapse" aria-labelledby="headingThree" data-parent="#accordion">
                            <div class="card-body">
                                <form method="post" action="">

                                    <div class="form-group row">
                                        <input class="form-control atual-password" type="password" placeholder="Digite sua senha atual" name="atual_password" v-model="password.currentPassword">
                                    </div>

                                    <div class="form-group row">
                                        <input class="form-control new-password" type="password" placeholder="Digite sua nova senha" name="new_password" v-model="password.newPassword">
                                    </div>

                                    <div class="form-group row">
                                        <input class="form-control conf-password" type="password" placeholder="Confirme a nova senha" name="conf_password" v-model="password.newPasswordAgain">
                                    </div>

                                    <div class="alert alert-danger alert-new-senha d-none"></div>

                                    <button class="btn btn-b6 btn-orange border-0" value="SALVAR" type="button" v-on:click="changePassword()"> Salvar </button>

                                </form>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-4 col-sm-12 d-flex d-sm-block flex-wrap justify-content-center">
                    <account-menu current-page="dados-cadastrais"></account-menu>
                </div>
            </div>
        </div>

    </div>
</section>
