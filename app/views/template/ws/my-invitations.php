<section id="breadcrumb">
    <div class="container">
        <div class="row">
            <nav aria-label="breadcrumb" style="font-size: 14px;">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item">
                        <a href="#">Home</a>
                    </li>
                    <li class="breadcrumb-item active" aria-current="page">Indicações</li>
                </ol>
            </nav>
        </div>
    </div>
</section>

<section class="container">
    <div id="myInvitationsApp" class="row">
        <div class="col-12 col-md-8">
            <p class="mb-3">
                Indique a DeliverPrint para seus amigos! Quando eles fizerem pedidos utilizando seu código pessoal eles ganham um desconto
                e você ganha cupons para utilizar nas suas próximas compras.
            </p>
            <p class="mb-3">
                Clique <a href="/indique-a-deliveryprint">aqui</a> para gerar seu código.
            </p>
            <p class="mb-3">
                Veja abaixo cupons que você já acumulou:
            </p>
            <div class="table-responsive" v-if="coupons.length > 0">
                <table class="table table-hover">
                    <thead>
                        <tr>
                            <th scope="col">Valor</th>
                            <th scope="col">Código</th>
                            <th scope="col">Expira em</th>
                            <th scope="col">Utilizado?</th>
                            <th scope="col"></th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr v-for="(coupon, idx) in coupons" :key="idx">
                            <th scope="row" v-if="coupon.type === 'ABS'">{{coupon.value | moneyFormatterWithZeroAndCurrency }}</th>
                            <th scope="row" v-else>{{coupon.value | percentFormatter}}</th>
                            <td>{{ coupon.codigo }}</td>
                            <td>{{ coupon.validade }}</td>
                            <td>{{ coupon.used ? 'Sim' : 'Não' }}</td>
                            <td><a :href="'/impressao-online?cupom=' + coupon.codigo" v-if="!coupon.used">Usar cupom</a></td>
                        </tr>
                    </tbody>
                </table>
            </div>
            <div v-else>
                <p class="lead text-center pt-4">Você ainda não tem nenhum cupom</p>
            </div>
        </div>
        <div class="col-12 col-md-4">
            <account-menu current-page="indicacoes"></account-menu>
        </div>
    </div>
</section>
