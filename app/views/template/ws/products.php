<div class="container" id="orderApp">
    <div class="row">
        <div class="container col-sm-12" id="container">
            <div class="row">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item">
                            <a href="/">Home</a>
                        </li>
                        <li class="breadcrumb-item active" aria-current="page">Serviços</li>
                    </ol>
                </nav>
            </div>
        </div>
    </div>
</div>

<div class="container my-md-5 pb-md-5 prod" id="prod">
    <div class="row">
        <div class="col-12">
            <h1 class="text-orange h2">
                O que fazemos:
            </h1>
        </div>

        <div class="col-12">
            <div class="row text-center">
                <div class="col-lg-3 col-md-4 col-6 py-3">
                    <a href="/impressao-colorida">
                        <img class="img-fluid img-thumbnail img-product border-0" src="/img/products/Impressão Colorida.jpg" alt="Impressão Colorida">
                        <h2 class="h3">Impressão Colorida</h2>
                    </a>
                </div>

                <div class="col-lg-3 col-md-4 col-6 py-3">
                    <a href="/impressao-preto-branco">
                        <img class="img-fluid img-thumbnail img-product border-0" src="/img/products/Impressão em Preto e Branco.jpg" alt="Impressão em Preto e Branco">
                        <h2 class="h3">Impressão em Preto e Branco</h2>
                    </a>
                </div>

                <!-- <div class="col-lg-3 col-md-4 col-6 py-3">
                    <a href="/impressao-couche">
                        <img class="img-fluid img-thumbnail img-product border-0" src="/img/products/Impressão em Papel Couche.jpg" alt="Impressão em Papel Couche">
                        <h2 class="h3">Impressão em Papel Couché</h2>
                    </a>
                </div> -->

                <div class="col-lg-3 col-md-4 col-6 py-3">
                    <a href="/impressao-certificado">
                        <img class="img-fluid img-thumbnail img-product border-0" src="/img/products/Impressão de Certificado.jpg" alt="Impressão de Certificado">
                        <h2 class="h3">Impressão de Certificado</h2>
                    </a>
                </div>

                <div class="col-lg-3 col-md-4 col-6 py-3">
                    <a href="/impressao-apostila">
                        <img class="img-fluid img-thumbnail img-product border-0" src="/img/products/Impressão de Apostila.jpg" alt="Impressão de Apostila">
                        <h1 class="h3">Impressão de Apostilas</h1>
                    </a>
                </div>

                <div class="col-lg-3 col-md-4 col-6 py-3">
                    <a href="/impressao-papel-timbrado">
                        <img class="img-fluid img-thumbnail img-product border-0" src="/img/products/Impressão de Papel Timbrado.jpg" alt="Impressão de Papel Timbrado">
                        <h2 class="h3">Impressão de Papel Timbrado</h2>
                    </a>
                </div>

                <div class="col-lg-3 col-md-4 col-6 py-3">
                    <a href="/avaliacoes">
                        <img class="img-fluid img-thumbnail img-product border-0" src="/img/products/Impressão de Avaliações.jpg" alt="Impressão de Avaliações">
                        <h2 class="h3">Impressão de Avaliações</h2>
                    </a>
                </div>

                <div class="col-lg-3 col-md-4 col-6 py-3">
                    <a href="/impressao-gabaritos">
                        <img class="img-fluid img-thumbnail img-product border-0" src="/img/products/Impressão de Gabarito.jpg" alt="Impressão de Gabarito">
                        <h2 class="h3">Impressão de Gabaritos</h2>
                    </a>
                </div>

                <div class="col-lg-3 col-md-4 col-6 py-3">
                    <a href="/impressao-documentos">
                        <img class="img-fluid img-thumbnail img-product border-0" src="/img/products/Impressão de Documentos.jpg" alt="Impressão de Documentos">
                        <h2 class="h3">Impressão de Documentos</h2>
                    </a>
                </div>

                <div class="col-lg-3 col-md-4 col-6 py-3">
                    <a href="/impressao-a4">
                        <img class="img-fluid img-thumbnail img-product border-0" src="/img/products/Impressão em A4.jpg" alt="Impressão em A4">
                        <h2 class="h3">Impressão em A4</h2>
                    </a>
                </div>

                <!-- <div class="col-lg-3 col-md-4 col-6 py-3">
                    <a href="/impressao-a5">
                        <img class="img-fluid img-thumbnail img-product border-0" src="/img/products/Impressão em A5.jpg" alt="Impressão em A5">
                        <h2 class="h3">Impressão em A5</h2>
                    </a>
                </div> -->

                <div class="col-lg-3 col-md-4 col-6 py-3">
                    <a href="/encadernacao-espiral">
                        <img class="img-fluid img-thumbnail img-product border-0" src="/img/products/Encadernação Espiral.jpg" alt="Encadernação Espiral">
                        <h2 class="h3">Encadernação em Espiral</h2>
                    </a>
                </div>

                <div class="col-lg-3 col-md-4 col-6 py-3">
                    <a href="/encadernacao-wire-o">
                        <img class="img-fluid img-thumbnail img-product border-0" src="/img/products/Encadernação em Wire-o.jpg" alt="Encadernação em Wire-o">
                        <h2 class="h3">Encadernação em Wire-o</h2>
                    </a>
                </div>

                <!-- <div class="col-lg-3 col-md-4 col-6 py-3">
                    <a href="/encadernacao-capa-dura">
                        <img class="img-fluid img-thumbnail img-product border-0" src="/img/products/Encadernação em Capa Dura.jpg" alt="Encadernação em Capa Dura">
                        <h2 class="h3">Encadernação em Capa Dura</h2>
                    </a>
                </div> -->

                <div class="col-lg-3 col-md-4 col-6 py-3">
                    <a href="/impressao-empresas">
                        <img class="img-fluid img-thumbnail img-product border-0" src="/img/products/Impressão para Empresas.jpg" alt="Impressão para Empresas">
                        <h2 class="h3">Impressão para Empresas</h2>
                    </a>
                </div>
            </div>
        </div>
    </div>
</div>

<?php require 'app/views/partial/how_it_works-bar.php'; ?>
