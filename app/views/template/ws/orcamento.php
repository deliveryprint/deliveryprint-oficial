<div class="container-fluid pb-md-5 orc" id="orc">
    <div class="row">
        <div class="col-md-6 col-12 order-2 order-md-1 bg-orange">
            <div class="row mt-5 justify-content-center">
                <h1 class="text-white h2">
                    PRECISA IMPRIMIR?
                </h1>
            </div>
            <div class="row justify-content-center">
                <p class="text-white h2 text-center">
                    TEMOS BOM PREÇO E ENTREGA ÁGIL!
                </p>
            </div>
            <div class="row pt-3">
                <div class="col-4 d-flex align-items-center justify-content-end">
                    <img class="img-fluid img-orc" src="/img/icon-printer.png" alt="">
                </div>
                <div class="col-8 d-flex align-items-center">
                    <p class="text-white h3">
                        Somos um serviço de Impressão e<br>
                        Encadernação com entrega para todo Brasil.<br>
                        Usamos Impressoras profissionais das melhores<br>
                        marcas do Mercado.
                    </p>
                </div>
            </div>
            <div class="row pt-3">
                <div class="col-4 d-flex align-items-center justify-content-end">
                    <img class="img-fluid img-orc" src="/img/icon-doc.png" alt="">
                </div>
                <div class="col-8">
                    <h3 class="text-white d-flex align-items-center">
                        Imprimimos mais de 1.824.000 páginas nesse<br>
                        ano de 2018
                    </h3>
                </div>
            </div>
            <div class="row pt-3 mb-5">
                <div class="col-4 d-flex align-items-center justify-content-end">
                    <img class="img-fluid img-orc" src="/img/icon-replly.png" alt="">
                </div>
                <div class="col-8 d-flex align-items-center">
                    <p class="text-white h3">
                        Orçamento Respondido de forma rápida e <br>
                        Personalizada, pagamento online por Cartão ou <br>
                        Boleto.
                    </p>
                </div>
            </div>
        </div>
        <div class="col-md-6 col-12 order-2 order-md-1 bg-lightgrey">
            <div class="row mt-5 justify-content-center">
                <h2 class="text-orange">
                    FAÇA SEU ORÇAMENTO AGORA
                </h2>
            </div>
            <div class="row">
                <div class="col-12 text-center align-center">
                    <form id="email_form">
                        <div class="form-group">
                            <label>
                                <p class="h3 mb-1 text-left">NOME:</p>
                                <input class="form-control" v-model="user_name" type="text" />
                            </label>
                        </div>
                        <div class="form-group">
                            <label>
                                <p class="h3 mb-1 text-left">EMAIL:</p>
                                <input v-model="user_email" class="form-control" type="email" />
                            </label>
                        </div>
                        <div class="form-group">
                            <label>
                                <p class="h3 mb-1 text-left">O que você quer imprimir?</p>
                                <textarea class="form-control" rows="5" id="textArea" v-model="description" type="text"></textarea>
                            </label>
                        </div>
                        <button type="button" @click="sendEmail()" class="btn btn-b4 btn-info bg-orange border-0 mt-3">PEDIR
                            ORÇAMENTO</button>

                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<?php require 'app/views/partial/how_it_works-bar.php'; ?>
