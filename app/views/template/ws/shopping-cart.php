<div id="shopping-items" v-cloak>
    <div class="container conteudo-carrinho">
        <div class="row breadcrumb-carrinho">
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item">
                        <a href="/">Home</a>
                    </li>
                    <li class="breadcrumb-item active" aria-current="page">
                        <a href="/impressao-online">Impressão Online</a>
                    </li>
                    <li class="breadcrumb-item active" aria-current="page">
                        Carrinho
                    </li>
                </ol>
            </nav>
        </div>
        <div class="row" v-if="items.length > 0">
            <div class="col-lg-6 order-2 order-sm-1">
                <order-summary :order="pedido" :items="items" :loading="loadingCartData" mode="cart"></order-summary>
            </div>
            <div class="col-lg-6 order-1 order-sm-2">

                <shopping-cart-address-form :endereco="endereco" :cliente="cliente" :incomplete-addr="incompleteAddr" :has-delivery="hasDelivery" :has-withdrawal="hasWithdrawal"></shopping-cart-address-form>

                <div class="text-center justify-content-end align-items-end">
                    <button class="btn btn-orange border-0 btn-b3 btn-b4-xs mb-3 margin-hg-bottom" :disabled="items.length == 0" value="Prosseguir" @click="goToPaymentPage">IR PARA PAGAMENTO</button>
                </div>
            </div>
        </div>
        <div class="d-flex justify-content-center align-items-center p-5" v-else>
            <div class="text-center">
                <p class="lead mb-3">Seu carrinho está vazio</p>
                <a href="/impressao-online" class="btn btn-b6 btn-orange-outlined border-radius-lg" style="float: none">Fazer um pedido</a>
            </div>
        </div>
    </div>
    <div class="container text-center d-sm-none" v-if="items.length > 0">
        <button class="btn btn-orange-outlined border-md margin-md-top btn-b3 btn-b4-xs b" value="Novo Item" onclick="location.href='/impressao-online'">
            NOVO ITEM </button>
    </div>

    <page-loader :show="loading"></page-loader>
</div>

<div id="modalSaida">
    <modal-saida-carrinho></modal-saida-carrinho>
</div>

<!-- Modal anexo-->
<div class="modal fade bd-example-modal-lg" id="anexoModal" tabindex="-1" role="dialog" aria-labelledby="anexoModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h3 class="modal-title default-text text-orange" id="anexoModalLabel">Anexar Arquivo PDF</h3><br>
                <span class="font-weight-normal text-orange border-0 txt-encardenacao">AVISO (para mais de um arquivo) <br>Os arquivos são impressos e encadernados (se houver encadernação) na ordem em que forem anexados aqui.</span>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form enctype="multipart/form-data">
                    <div class="file-loading">
                        <input id="file-0a" type="file" name="file" multiple accept="application/pdf">
                    </div>
                    <br>
                </form>
                <div class="modal-footer p-0">
                    <div class="input_fields_wrap"></div>
                    <button class="btn btn-secondary btn-success d-none" id="btnteste">teste</button>
                    <button class="btn btn-b3 btn-orange border-radius-md" id="btno" data-dismiss="modal" data-toggle="collapse" data-target="#collapseSix">Prosseguir</button>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="modalObservacoes" tabindex="-1" role="dialog" aria-hidden="true">
    <!--MODAL ORIENTACOES -->
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h3 class="modal-title default-text text-orange font-weight-lighter" id="exampleModalLongTitle">
                    <Strong>Orientações</strong> - Item # {{ itemId }}
                </h3>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body pt-0">
                <form>
                    <div class="form-group m-0" v-if="!isOrientationSaved">
                        <textarea class="form-control " rows="5" id="clientOrientation" name="clientOrientation" placeholder="Campo destinado para observações" v-model="comment" maxlength="254"></textarea>
                        <div class="modal-footer p-0">
                            <button type="button" class="btn btn-b3 btn-b6-xs border-radius-lg btn btn-orange border border-solid" v-on:click="saveOrientation">SALVAR</button>
                        </div>
                    </div>
                    <div class="margin-sm" v-else>
                        <p class="date">{{ date }}</p>
                        <p class="orientation">{{ comment }}</p>
                        <div class="modal-footer p-0">
                            <button type="button" class="btn btn-b3 btn-b5-xs border-radius-md btn btn-orange-outlined border-md border-solid" v-on:click="editOrientation">Editar</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
