<div class="container" id="container">
    <div class="row">
        <nav aria-label="breadcrumb">
            <ol class="breadcrumb">
                <li class="breadcrumb-item">
                    <a href="/">Home</a>
                </li>
                <li class="breadcrumb-item active" aria-current="page">
                    <a href="/impressao-online">Impressão Online</a>
                </li>
                <li class="breadcrumb-item active" aria-current="page">
                    <a href="/carrinho">
                        Carrinho
                    </a>
                </li>
                <li class="breadcrumb-item active" aria-current="page">
                    Pagamento
                </li>
            </ol>
        </nav>
    </div>

    <div class="row" id="checkout" v-cloak>
        <div class="lateral col-xs-12 col-md-12 col-lg-6">

            <order-summary :order="order" :items="items" :loading="loading" mode="payment"></order-summary>

        </div>
        <div class="lateral col-xs-12 order-1 order-md-2 col-lg-6 float-left">
            <div class="row row-title">
                <h3 class="text-orange margin-md-left margin-sm-botton-2">
                    <i class="fa fa-money margin-sm-right" aria-hidden="true"></i>Forma de Pagamento</h3>
            </div>
            <form>
                <div class="form-check chkboleto" style="width: 100%">
                    <label class="form-check-label radio-btn" for="retirada">
                        <input class="form-check-input inpchk" type="radio" name="gridRadios" id="retirada" value="option1" style="margin-top: 30px;" v-model="paymentType">
                        <span class="checkmark margin-md-top"></span>
                        <div class="bandeiras flag">
                            <i class="fa fa-barcode fa-6x" style="margin-left: 10px;">
                            </i>
                            <span class="paymnt-type default-text text-lg font-weight-bold display-inline margin-md-top">
                                <div class="boleto">
                                    Boleto
                                </div>
                            </span>
                        </div>
                    </label>
                </div>
                <div class="div-ipts-boleto margin-hg-left margin-sm-top pl-md-3" v-show="paymentType == 'option1'">
                    <div class="ipts-control">
                        <label class="text-orange">
                            <i class="fa fa-user" aria-hidden="true"></i> Nome Completo
                            <input class="ipts-boleto form-control" type="text" name="" v-model="user.fullName" placeholder="Insira Nome">
                        </label>
                    </div>

                    <div class="ipts-control">
                        <label class="text-orange">
                            <i class="fa fa-id-card" aria-hidden="true"></i> CPF ou CNPJ
                            <input class="ipts-boleto form-control" type="text" name="" v-mask="['###.###.###-##', '##.###.###/####-##']" v-model="user.cpf_cnpj" placeholder="Insira CPF ou CNPJ">
                        </label>
                    </div>
                    <br>
                    <span class="ipts-control d-none">
                        <i class="fa fa-user" aria-hidden="true"></i>
                        <input class="ipts-boleto form-control" type="text" name="" v-model="user.email" placeholder="Email">
                    </span>
                    <span class="ipts-control d-none">
                        <i class="fa fa-map-marker" aria-hidden="true"></i>
                        <input class="ipts-boleto form-control" type="text" name="" v-mask="'#####-###'" v-model="user.zipCode" placeholder="CEP">
                    </span>
                    <br>
                    <span class="ipts-control d-none">
                        <i class="fa fa-hashtag" aria-hidden="true"></i>
                        <input class="ipts-boleto form-control" type="text" name="" v-model="user.number" placeholder="Número do endereço">
                    </span>
                    </br>
                    <span class="ipts-control" v-bind:class="{'d-none': !incompleteAddr}">
                        <i class="fa fa-map-marker" aria-hidden="true"></i>
                        <input class="ipts-boleto form-control" type="text" name="" v-model="user.street" placeholder="Logradouro">
                    </span>
                    <span class="ipts-control " v-bind:class="{'d-none': !incompleteAddr}">
                        <i class="fa fa-hashtag" aria-hidden="true"></i>
                        <input class="ipts-boleto form-control" type="text" name="" v-model="user.district" placeholder="Bairro">
                    </span>
                    <!-- </br> -->
                    <span class="ipts-control d-none">
                        <i class="fa fa-map-marker" aria-hidden="true"></i>
                        <input class="ipts-boleto form-control" type="text" name="" v-model="user.city" placeholder="Cidade">
                    </span>
                    <span class="ipts-control  d-none">
                        <i class="fa fa-hashtag" aria-hidden="true"></i>
                        <input class="ipts-boleto form-control" type="text" name="" v-model="user.state" placeholder="UF">
                    </span>
                </div>

                <div class="form-check chkcartao margin-md-bottom" style="width: 100%">
                    <label class="form-check-label radio-btn" for="entrega">
                        <div class="frete-retirada">
                            <input class="form-check-input inpchk" type="radio" name="gridRadios" id="entrega" value="option2" v-model="paymentType">
                            <span class="checkmark margin-top"></span>
                            <div style="float: left; margin-left: 10px;" class="bandeiras flag">
                                <i class="fa fa-cc-visa fa-2x margin-xxs-right margin-xxs-bottom"></i>
                                <i class="fa fa-cc-mastercard fa-2x"></i>
                                <br>
                                <i class="fa fa-cc-amex fa-2x margin-xxs-right"></i>
                                <i class="fa fa-cc-discover fa-2x"></i>
                            </div>
                            <span class="bandeiras flag paymnt-type margin-top default-text  text-lg font-weight-bold">
                                <div class="cartao">
                                    Cartão de Crédito
                                </div>
                            </span>
                    </label>
                </div>
        </div>

        <div class="div-ipts-cartao margin-hg-left margin-md-top pl-md-3 mb-4" :class="{'d-none': paymentType != 'option2'}">
            <!-- Em uso -->
            <div class="ipts-control" v-if="vfinal >= 100.00">
                <label class="text-orange">
                    <i class="fa fa-credit-card" aria-hidden="true"></i> Parcelas
                    <select class="ipts-boleto form-control" v-model="installments">
                        <option value="1" selected>à vista {{vfinal | moneyFormatter}}</option>
                        <option v-for="opt in generateInstallmentsOpt" :value="opt.value">{{opt.text}}</option>
                    </select>
                </label>
            </div>

            <div class="ipts-control">
                <label class="text-orange">
                    <i class="fa fa-hashtag" aria-hidden="true"></i> Número do cartão
                    <input class="ipts-boleto form-control validate" :class="status($v.card.number)" type="text" v-mask="['#### #### #### ###', '#### #### #### ####']" v-model="$v.card.number.$model" placeholder="Insira Número">
                    <div class="input-validator" v-if="!$v.card.number.minLength">
                        Número de cartão inválido
                    </div>

                </label>
            </div>

            <div class="ipts-control">
                <label class="text-orange">
                    <i class="fa fa-user" aria-hidden="true"></i> Titular do cartão

                    <input class="ipts-boleto form-control validate" :class="status($v.card.fullName)" type="text" name="" v-model="$v.card.fullName.$model" placeholder="Insira Nome">
                    <div class="input-validator" v-if="!$v.card.fullName.checkName">
                        Digite o nome conforme o cartão
                    </div>

                </label>
            </div>

            <div class="ipts-control">
                <div class="row">
                    <div class="col-6">
                        <label class="text-orange">
                            <i class="fa fa-calendar" aria-hidden="true"></i> Validade
                            <input class="ipts-boleto form-control validate" :class="status($v.card.expiration)" type="text" name="" v-mask=" '##/##' " v-model="$v.card.expiration.$model" placeholder="Insira Validade">
                            <div class="input-validator" v-if="!$v.card.expiration.checkDate">
                                Data inválida
                            </div>
                        </label>
                    </div>
                    <div class="col-6">
                        <label class="text-orange">
                            <i class="fa fa-unlock-alt pr-2 d-inline-block d-sm-none" aria-hidden="true"></i> CVV
                            <i class="fa fa-info-circle text-blue pl-2"></i>
                            <input class="ipts-boleto form-control validate" :class="status($v.card.verificationValue)" type="text" v-model="$v.card.verificationValue.$model" v-mask="['###', '####']" placeholder="Insira CVV">
                            <div class="input-validator" v-if="!$v.card.verificationValue.minLength">
                                O CVV possui no mínimo 3 dígitos
                            </div>
                        </label>
                    </div>
                </div>
            </div>
            <br>
            <!-- Não esta sendo mostrado -->
            <span class="ipts-control d-none">
                <i class="fa fa-user" aria-hidden="true"></i>
                <input class="ipts-boleto form-control" type="text" name="" v-model="user.email" placeholder="Email">
            </span>
            <span class="ipts-control d-none">
                <i class="fa fa-id-card margin-xxs-top" aria-hidden="true"></i>
                <input class="ipts-boleto form-control" type="text" v-mask="['###.###.###-##', '##.###.###/####-##']" v-model="user.cpf_cnpj" placeholder="CPF ou CNPJ">
            </span>
            </br>
            <span class="ipts-control d-none">
                <i class="fa fa-map-marker" aria-hidden="true"></i>
                <input class="ipts-boleto form-control" type="text" name="" v-mask="'#####-###'" v-model="user.zipCode" placeholder="CEP">
            </span>
            <span class="ipts-control d-none">
                <i class="fa fa-hashtag" aria-hidden="true"></i>
                <input class="ipts-boleto form-control" type="text" name="" id="ipt-icon" v-model="user.number" placeholder="Número do endereço">
            </span>
            </br>
            <span class="ipts-control" v-bind:class="{'d-none': !incompleteAddr}">
                <i class="fa fa-map-marker" aria-hidden="true"></i>
                <input class="ipts-boleto form-control" type="text" name="" v-model="user.street" placeholder="Logradouro">
            </span>
            <span class="ipts-control" v-bind:class="{'d-none': !incompleteAddr}">
                <i class="fa fa-hashtag" aria-hidden="true"></i>
                <input class="ipts-boleto form-control" type="text" name="" v-model="user.district" placeholder="Bairro">
            </span>
            </br>
            <span class="ipts-control d-none">
                <i class="fa fa-map-marker" aria-hidden="true"></i>
                <input class="ipts-boleto form-control" type="text" name="" v-model="user.city" placeholder="Cidade">
            </span>
            <span class="ipts-control d-none">
                <i class="fa fa-hashtag" aria-hidden="true"></i>
                <input class="ipts-boleto form-control" type="text" name="" v-model="user.state" placeholder="UF">
            </span>
        </div>

        </form>

        <div id="recaptcha" class="g-recaptcha mb-4" :data-sitekey="reCaptchaId" v-show="reCaptchaEnable"></div>

        <div class="text-center">
            <button class="btn btn-orange border-0 btn-b3 btn-b4-xs b" value="Proseguir" v-on:click="createPayment()" v-bind:disabled="blockProceed">FINALIZAR PEDIDO</button>
        </div>
        <div class="dtermos margin-md-left margin-md-top font-weight-lighter">
            <p class="pgmto d-none d-sm-block">Pagamento com tecnologia e segurança</p>
            <img class="iugu d-none d-sm-block" src="/img/iugu.png" alt="iugu">
            <p class="termo">*Ao continuar você declara que aceita os Termos de Uso do Site:
                <span class="termos" data-toggle="modal" data-target="#modalTermos"> Termos de Uso DeliveryPrint</span>
            </p>
        </div>
        <div class="row margin-md-top float-md-right">
            <div class="col-6 text-center  d-inline-block d-sm-none">
                <button class="btn btn-orange-outlined border-md btn-b3 btn-b4-xs b" value="Novo Item" onclick="location.href='/impressao-online'">
                    NOVO ITEM </button>
            </div>
        </div>
    </div>

    <page-loader :show="loading"></page-loader>

</div>
</div>

<div class="modal fade payment-blocked-modal" id="paymentBlockedModal" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h3 class="modal-title default-text anexoModalLabel text-orange">Aviso Importante</h3>
            </div>
            <div class="modal-body">

                <p class="text-left mb-3">
                    Por motivos de segurança, pedimos que você entre em contato conosco pelo e-mail
                    <strong>suporte@deliveryprint.com.br</strong> para concluir seu pedido.
                </p>

                <p class="text-left">
                    Não se preocupe, todos os seus dados estão salvos com a gente. O código do seu pedido
                    é <strong>{{ id }}</strong>
                </p>

                <button type="button" class="btn btn-b3 btn-orange border-radius-md" @click="closeModal()">Entendido</button>
            </div>
        </div>
    </div>
</div>
