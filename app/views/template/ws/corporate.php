<section class="corporate-main d-flex align-items-center" id="companyApp">
    <div class="container-fluid">
        <div class="card">
            <div class="card-body">
                <h1>Peça suas impressões de maneira fácil!</h1>
                <h2>Você já tem muita coisa para se preocupar. Deixe suas impressões com a gente, é a nossa especialidade!</h2>
                <div class="clearfix">
                    <div class="main-btn-1">
                        <a href="#" class="btn btn-b3 btn-primary d-none d-md-block" @click="openModal">Orçamento com desconto</a>
                        <a href="#" class="btn btn-b3 btn-outline-light d-md-none w-100 mx-auto" @click="openModal">Orçamento com desconto</a>
                    </div>
                    <div class="main-btn-2">
                        <a href="/ajuda/faq" class="btn btn-b3 btn-primary d-none d-md-block">Dúvidas frequentes</a>
                        <a href="/ajuda/faq" class="btn btn-b3 btn-outline-light d-md-none w-100 mx-auto">Dúvidas frequentes</a>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <page-loader :show="loading"></page-loader>

    <div id="companyModal" class="modal" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header text-center">
                    <h3 class="modal-title default-text text-orange">PREENCHA O CADASTRO PARA LIBERAR A PÁGINA EXCLUSIVA DE DESCONTO</h3>
                </div>
                <div class="modal-body">
                    <form id="companyForm" @submit.prevent="submitCompany" novalidate>
                        <div class="w-100">
                            <div id="nameGroup" class="form-group mb-5">
                                <input type="name" class="form-control" placeholder="seu nome..." v-model="name" required>
                                <div class="invalid-feedback">Insira seu nome</div>
                            </div>

                            <div id="phoneGroup" class="form-group mb-5">
                                <input type="phone" class="form-control" placeholder="Telefone" v-mask="['(##) ####-####', '(##) #####-####']" v-model="phone" required>
                                <div class="invalid-feedback">Insira seu telefone</div>
                            </div>

                            <div id="emailGroup" class="form-group mb-5">
                                <input type="email" class="form-control" placeholder="email corporativo" v-model="email" required>
                                <div class="invalid-feedback">Insira seu e-mail corporativo</div>
                            </div>

                            <button type="button" class="btn btn-b3 btn-orange d-block m-auto" @click="submitCompany()" v-if="!loading">CONTINUAR</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="features bg-lightgrey pt-5">
    <div class="container">
        <div class="row">
            <div class="col-md-4">
                <div class="card mb-5">
                    <div class="card-body text-center p-3" style="width: auto;">
                        <img class="feature-img mb-3" src="/img/icone-desconto.png" alt="Descontos exclusivos para empresas na DeliveryPrint">
                        <p class="feature-text-1 mb-3">Descontos Exclusivos</p>
                        <h4 class="text-orange feature-text-2 mb-3">15%</h4>
                        <h3 class="feature-text-3 mx-auto" style="max-width: 230px">
                            <strong>mais barato</strong> do que comprar pelo site comum
                        </h3>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="card mb-5">
                    <div class="card-body text-center p-3" style="width: auto;">
                        <img class="feature-img mb-3" src="/img/icone-delivery.png" alt="Frete grátis na DeliveryPrint">
                        <p class="feature-text-1 mb-3">Entregas Personalizadas</p>
                        <h4 class="text-orange feature-text-2 mb-3">ENTREGAS RÁPIDAS</h4>
                        <h3 class="feature-text-3 mx-auto" style="max-width: 190px">Enviamos pedidos para o Brasil inteiro!</h3>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="card mb-5">
                    <div class="card-body text-center p-3" style="width: auto;">
                        <img class="feature-img mb-3" src="/img/icone-chat.png" alt="Atendimento humano e eficiente na DeliveryPrint">
                        <p class="feature-text-1 mb-3">Atendimento Humano</p>
                        <h4 class="text-orange feature-text-2 mb-3">EFICIÊNCIA</h4>
                        <h3 class="feature-text-3 mx-auto" style="max-width: 250px">
                            e <strong>agilidade comprovadas</strong>. Veja nossas avaliações no Google!
                        </h3>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="container">
    <?php require 'app/views/partial/comments.php'; ?>
</section>
