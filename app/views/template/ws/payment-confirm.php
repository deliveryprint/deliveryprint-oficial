<section id="breadcrumb">
    <div class="container">
        <div class="row">
            <nav aria-label="breadcrumb" class="text-grey text-sm">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item">
                        <a href="#">Home</a>
                    </li>
                    <li class="breadcrumb-item">
                        <a href="#">Pedidos</a>
                    </li>
                    <li class="breadcrumb-item">
                        <a href="#">Carrinho</a>
                    </li>
                    <li class="breadcrumb-item">
                        <a href="#">Pagamento</a>
                    </li>
                    <li class="breadcrumb-item active" aria-current="page">Confirmação</li>
                </ol>
            </nav>
        </div>
    </div>
</section>

<section>
    <div class="container">
        <div class="row">
            <div class="col-md-7">
                <div id="confirm-dialog" v-cloak>
                    <!-- payment dialog credit card -->

                    <div class="row bg-green align-items-center text-white py-4 mx-auto d-none" v-bind:class="{ 'd-flex': !isBoleto }">
                        <div class="col-1">
                            <i class="fa fa-check-circle fa-2x mt-sm-2 mt-2"></i>
                        </div>
                        <div class="col-10 pr-0">
                            <p class="text-lg font-weight-bold">Pagamento realizado com sucesso!</p>
                            <p class="font-weight-lighter text-sm">Uma confirmação será enviada para seu e-mail.</p>
                        </div>
                    </div>
                    <!-- end dialog -->
                    <!-- payment dialog boleto -->

                    <div class="row bg-blue align-items-center text-white py-4 mx-auto d-none" v-bind:class="{ 'd-flex': isBoleto }">
                        <div class="col-1">
                            <i class="fa fa-check-circle fa-2x mt-3"></i>
                        </div>
                        <div class="col-10 pr-0">

                            <p class="text-lg font-weight-bold">Boleto Gerado</p>
                            <p>
                                <span class="font-weight-lighter text-sm">Seu boleto foi enviado para o email</span>
                                <span class="font-weight-bold text-sm">{{email}}</span>

                            </p>

                        </div>
                    </div>
                    <!-- end dialog -->
                    <!-- order details -->
                    <div class="row bg-lightgrey text-orange font-weight-bold mt-3 py-3 mx-auto">
                        <div class="col-6 pr-0">
                            <div class="row">
                                <div class="col-2 pr-0">
                                    <i class="fa fa-truck fa-2x mt-sm-3 mt-1"></i>
                                </div>
                                <div class="col-10 pr-0 pl-4">

                                    <span class="text-md">{{isWithdrawal ? 'Retirada' : 'Entrega'}} prevista:</span>
                                    <br>
                                    <span id="deliveryDate" class="text-md">{{forecast}}</span>
                                </div>
                            </div>
                        </div>
                        <div class="col-6 pr-0 pl-0">
                            <!-- Credit card dialog -->

                            <div class="row d-none" v-bind:class="{ 'd-flex': (!isBoleto && !isPromotion) }">
                                <div class="col-2 px-0">
                                    <i class="fa fa-credit-card fa-2x mt-sm-3"></i>
                                </div>
                                <div class="col-10 px-0">
                                    <span class="text-md">Método de Pagto:</span>
                                    <br>
                                    <span id="payMethod" class="text-sm font-weight-lighter text-grey">Cartão de Crédito</span>
                                    <br>
                                    <span id="cardNumber" class="text-sm font-weight-lighter text-grey">{{cartao.bandeira ? cartao.bandeira.toUpperCase() : ""}} **** **** **** {{cartao.finalNum}}</span>
                                </div>
                            </div>
                            <!-- Boleto dialog -->

                            <div class="row d-none" v-bind:class="{ 'd-flex': isBoleto }">
                                <div class="col-12 mt-2 mt-sm-1 text-right">
                                    <button v-on:click="displayBankSlip" class="btn btn-b4 btn-b5-xs btn-orange mr-3 border-0">VISUALIZAR BOLETO</button>
                                </div>
                            </div>
                            <!-- end dialogs -->
                        </div>
                    </div>
                </div>
                <!-- end details -->
                <h3 class="text-orange mt-5 mb-0">Resumo do(s) pedido(s)</h3>
                <!-- order items -->
                <div class="card" id="itemList" v-cloak>
                    <!-- card starts -->
                    <div class="item-lista row bg-lightergrey padding-xs mx-0 margin-xs-top" v-for="(item, itIdx) in items">
                        <div class="card-header-itens col-12 bg-transparent">
                            <h4 class="itens card-title default-text mb-3 bg-transparent">Item #<span class="">{{item.pretty_id}}</span>
                                <span class="text-grey font-weight-lighter text-sm d-block d-sm-inline-block">{{ pedido.data | dateFormatter }}</span>
                            </h4>
                        </div>


                        <ul class="list-group col-7">
                            <li class="list-group-item">Cor
                                <span class="itens-info text-orange">{{ item.cor }}</span>
                            </li>
                            <li class="list-group-item">Lado
                                <span class="itens-info text-orange">{{ item.lado }}</span>
                            </li>
                            <li class="list-group-item">Papel
                                <span class="itens-info text-orange">{{ item.papel }}</span>
                            </li>
                            <li class="list-group-item">Acabamento
                                <span class="itens-info text-orange">{{ item.acabamento }}</span>
                            </li>
                            <li class="list-group-item">Páginas
                                <span class="itens-info text-orange">{{ countPages(itIdx) }}</span>
                            </li>
                        </ul>
                    </div>
                    <!-- end card -->
                </div>
                <!-- end items -->
            </div>
            <div id="accountMenuApp" class="col-md-4">
                <div class="bg-blue text-center p-3 mb-4">
                    <p class="mb-3 text-white lead">Indique a DeliveryPrint para seus amigos e ganhe descontos nas próximas compras!</p>
                    <a href="/indique-a-deliveryprint" class="btn btn-b3 btn-orange border-0" style="line-height: 1.5">Clique aqui e saiba como</a>
                </div>
                <account-menu></account-menu>
            </div>
        </div>
        <!-- end row -->
    </div>
</section>
