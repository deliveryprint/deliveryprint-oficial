<section id="breadcrumb">
    <div class="container">
        <div class="row">
            <nav aria-label="breadcrumb" style="font-size: 14px;">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item">
                        <a href="#">Home</a>
                    </li>
                    <li class="breadcrumb-item active" aria-current="page">Meus Pedidos</li>
                </ol>
            </nav>
        </div>
    </div>
</section>

<section class="container" id="verificationWarning" v-cloak>
    <div class="alert alert-primary mb-4" role="alert" v-if="needVerification">
        <b>Importante:</b> Seu e-mail ainda não foi verificado. Clique <a href="verificacao-necessaria">aqui</a> para fazer a verificação.
    </div>
</section>

<section id="forms">
    <div class="container">
        <div class="col-md-12">
            <div id="orders" class="row">
                <div class="col-12 col-lg-8">
                    <div class="row">
                        <div class="col-md-12">
                            <h3 class="title-forms">Meus Pedidos</h3>
                            <a href="/impressao-online">
                                <button class="btn btn-b6 border-radius-md btn btn-orange-outlined d-none d-md-inline-block border-md border-solid novo-pedido">Novo Pedido</button>
                            </a>
                        </div>
                    </div>
                    <order-list
                        :orders="orders"
                        @details-btn-clicked="showModalInfo($event)"
                        @tracking-btn-clicked="showModalTracking($event)"
                        @payment-btn-clicked="payOrder($event)">
                    </order-list>
                </div>
                <div class="col-md-4">
                    <account-menu current-page="meus-pedidos"></account-menu>
                </div>
            </div>
        </div>
    </div>
</section>

<!-- Modal -->
<div class="modal fade" id="detailsModal" tabindex="-1" role="dialog" aria-hidden="true" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">

                <h3 class="modal-title default-text rowpedidos text-orange" id="exampleModalLongTitle">Pedido
                    #{{currentOrder}}</h3>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body item-lista">
                <div class="itens bg-transparent text-left" v-for="item in items">

                    <ul class="itens-pedido float-none p text-grey padding-sm-bottom">
                        <h4 class="text-orange default-text">Item #{{ item.pretty_id }}</h4>
                        <li>Cor:
                            <span class="text-orange"> {{ item.descricaoCor }} </span>
                        </li>
                        <li>Lado:
                            <span class="text-orange">{{ item.descricaoLado }}</span>
                        </li>
                        <li>Papel:
                            <span class="text-orange">{{ item.descricaoPapel }}</span>
                        </li>
                        <li>Acabamento:
                            <span class="text-orange">{{ item.descricaoAcabamento }}</span>
                        </li>
                        <li>Encadernações:
                            <span class="text-orange">{{ item.encadernacoes }}</span>
                        </li>
                        <li>Nº de Arquivos:
                            <span class="text-orange">{{ item.arquivos }}</span>
                        </li>
                        <li>Borda:
                            <span class="text-orange">{{item.borda}}</span>
                        </li>
                        <li>Nº de Páginas:
                            <span class="text-orange">{{ item.numberPages }}</span>
                        </li>
                        <li>Observação:
                            <span class="text-orange">{{ item.obsCliente }}</span>
                        </li>
                    </ul>
                </div>
                <h3 class="default-text text-orange">Previsão de {{ items[0].isWithdrawal ? 'retirada' : 'entrega'}}: {{items[0].delivery_date}} </h3>
            </div>
            <div class="modal-footer">
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="itemsTrackingModal" tabindex="-1" role="dialog" aria-hidden="true" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">

                <h3 class="modal-title default-text rowpedidos text-orange" id="exampleModalLongTitle">Rastreio de Itens </h3>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body item-lista pt-0">
                <div class="itens bg-transparent text-left pt-1 border-bottom mb-3" v-for="item in items">
                    <ul class="itens-pedido float-none p text-grey padding-sm-bottom">
                        <p class="text-orange default-text f-16 mb-3 font-weight-bold">Item #{{ item.pretty_id }}</p>
                        <li class="f-14 mb-3 float-none">Rastreio:
                            <span class="text-orange"> {{ item.tracking_code }} </span>
                        </li>
                        <li class="f-14 mb-3 float-none">Status:
                            <span class="text-orange"> {{ item.statusItem }} </span>
                        </li>
                        <button class="btn btn-orange btn-b5 font-weight-bold" @click="trackItem(item.tracking_code)">RASTREAR ITEM</button>
                    </ul>
                </div>
            </div>
            <div class="modal-footer">
            </div>
        </div>
    </div>

    <div class="d-none">
        <form id="trackingForm" action="https://www2.correios.com.br/sistemas/rastreamento/ctrl/ctrlRastreamento.cfm" method="post" target="_blank">
            <input id="objetos" maxlength="13" name="objetos" size="13" type="text" :value="currentCode" />
        </form>
    </div>
</div>
