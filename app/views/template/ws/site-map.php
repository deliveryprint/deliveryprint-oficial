<div class="container" id="orderApp">
    <div class="row">
        <div class="container col-sm-12" id="container">
            <div class="row">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item">
                            <a href="/">Home</a>
                        </li>
                        <li class="breadcrumb-item active" aria-current="page">Mapa do Site</li>
                    </ol>
                </nav>
            </div>
        </div>
    </div>
</div>

<div class="container pb-md-5 map" id="map">
    <div class="row">
        <div class="col-12">
            <h3 class="text-orange h2">
                Mapa do Site
            </h3>
        </div>

        <div class="col-12">
            <h2 class="text-orange h3">
                Locais Atendidos
            </h2>

            <div class="row category-container ml-2 ml-sm-0">
                <ul>
                    <li>
                        <a href="/copiadora-pinheiros">
                            <h2><i class="fa fa-angle-right"></i> Copiadora Pinheiros</h2>
                        </a>
                    </li>
                    <li>
                        <a href="/copiadora-vila-mariana">
                            <h2><i class="fa fa-angle-right"></i> Copiadora Vila Mariana</h2>
                        </a>
                    </li>
                    <li>
                        <a href="/copiadora-vila-madalena">
                            <h2><i class="fa fa-angle-right"></i> Copiadora Vila Madalena</h2>
                        </a>
                    </li>
                    <li>
                        <a href="/copiadora-vila-olimpia">
                            <h2><i class="fa fa-angle-right"></i> Copiadora Vila Olímpia</h2>
                        </a>
                    </li>
                    <li>
                        <a href="/copiadora-avenida-paulista">
                            <h2><i class="fa fa-angle-right"></i> Copiadora Av. Paulista</h2>
                        </a>
                    </li>
                    <li>
                        <a href="/copiadora-faria-lima">
                            <h2><i class="fa fa-angle-right"></i> Copiadora Faria Lima</h2>
                        </a>
                    </li>
                    <li>
                        <a href="/copiadora-morumbi">
                            <h2><i class="fa fa-angle-right"></i> Copiadora Morumbi</h2>
                        </a>
                    </li>
                    <li>
                        <a href="/copiadora-santo-amaro">
                            <h2><i class="fa fa-angle-right"></i> Copiadora Santo Amaro</h2>
                        </a>
                    </li>
                    <li>
                        <a href="/copiadora-cidade-jardim">
                            <h2><i class="fa fa-angle-right"></i> Copiadora Cidade Jardim</h2>
                        </a>
                    </li>
                    <li>
                        <a href="/copiadora-moema">
                            <h2><i class="fa fa-angle-right"></i> Copiadora Moema</h2>
                        </a>
                    </li>
                    <li>
                        <a href="/copiadora-campinas">
                            <h2><i class="fa fa-angle-right"></i> Copiadora Campinas</h2>
                        </a>
                    </li>
                    <li>
                        <a href="/copiadora-belo-horizonte">
                            <h2><i class="fa fa-angle-right"></i> Copiadora Belo Horizonte</h2>
                        </a>
                    </li>
                    <li>
                        <a href="/copiadora-rio-de-janeiro">
                            <h2><i class="fa fa-angle-right"></i> Copiadora Rio de Janeiro</h2>
                        </a>
                    </li>
                    <li>
                        <a href="/copiadora-curitiba">
                            <h2><i class="fa fa-angle-right"></i> Copiadora Curitiba</h2>
                        </a>
                    </li>
                    <li>
                        <a href="/copiadora-lapa">
                            <h2><i class="fa fa-angle-right"></i> Copiadora Lapa</h2>
                        </a>
                    </li>
                    <li>
                        <a href="/copiadora-jk">
                            <h2><i class="fa fa-angle-right"></i> Copiadora JK</h2>
                        </a>
                    </li>
                    <li>
                        <a href="/copiadora-barra-funda">
                            <h2><i class="fa fa-angle-right"></i> Copiadora Barra Funda</h2>
                        </a>
                    </li>
                    <li>
                        <a href="/copiadora-paulista">
                            <h2><i class="fa fa-angle-right"></i> Copiadora Paulista</h2>
                        </a>
                    </li>
                    <li>
                        <a href="/copiadora-shopping">
                            <h2><i class="fa fa-angle-right"></i> Copiadora Shopping</h2>
                        </a>
                    </li>
                    <li>
                        <a href="/copiadora-tatuape">
                            <h2><i class="fa fa-angle-right"></i> Copiadora Tatuapé</h2>
                        </a>
                    </li>
                    <li>
                        <a href="/copiadora-itaquera">
                            <h2><i class="fa fa-angle-right"></i> Copiadora Itaquera</h2>
                        </a>
                    </li>
                    <li>
                        <a href="/copiadora-consolacao">
                            <h2><i class="fa fa-angle-right"></i> Copiadora Consolação</h2>
                        </a>
                    </li>
                    <li>
                        <a href="/copiadora-liberdade">
                            <h2><i class="fa fa-angle-right"></i> Copiadora Liberdade</h2>
                        </a>
                    </li>
                    <li>
                        <a href="/copiadora-santa-cruz">
                            <h2><i class="fa fa-angle-right"></i> Copiadora Santa Cruz</h2>
                        </a>
                    </li>
                    <li>
                        <a href="/copiadora-mooca">
                            <h2><i class="fa fa-angle-right"></i> Copiadora Mooca</h2>
                        </a>
                    </li>
                    <li>
                        <a href="/copiadora-itaim-bibi">
                            <h2><i class="fa fa-angle-right"></i> Copiadora Itaim Bibi</h2>
                        </a>
                    </li>
                    <li>
                        <a href="/copiadora-republica">
                            <h2><i class="fa fa-angle-right"></i> Copiadora República</h2>
                        </a>
                    </li>
                    <li>
                        <a href="/copiadora-bela-vista">
                            <h2><i class="fa fa-angle-right"></i> Copiadora Bela Vista</h2>
                        </a>
                    </li>
                    <li>
                        <a href="/copiadora-vergueiro">
                            <h2><i class="fa fa-angle-right"></i> Copiadora Vergueiro</h2>
                        </a>
                    </li>
                    <li>
                        <a href="/copiadora-sp">
                            <h2><i class="fa fa-angle-right"></i> Copiadora SP</h2>
                        </a>
                    </li>
                    <li>
                        <a href="/copiadora-bras">
                            <h2><i class="fa fa-angle-right"></i> Copiadora Brás</h2>
                        </a>
                    </li>
                    <li>
                        <a href="/copiadora-perdizes">
                            <h2><i class="fa fa-angle-right"></i> Copiadora Perdizes</h2>
                        </a>
                    </li>
                    <li>
                        <a href="/copiadora-sumare">
                            <h2><i class="fa fa-angle-right"></i> Copiadora Sumaré</h2>
                        </a>
                    </li>
                    <li>
                        <a href="/copiadora-usp">
                            <h2><i class="fa fa-angle-right"></i> Copiadora USP</h2>
                        </a>
                    </li>
                    <li>
                        <a href="/copiadora-sao-bento">
                            <h2><i class="fa fa-angle-right"></i> Copiadora São Bento</h2>
                        </a>
                    </li>
                    <li>
                        <a href="/copiadora-unifesp">
                            <h2><i class="fa fa-angle-right"></i> Copiadora UNIFESP</h2>
                        </a>
                    </li>
                    <li>
                        <a href="/copiadora-pacaembu">
                            <h2><i class="fa fa-angle-right"></i> Copiadora Pacaembu</h2>
                        </a>
                    </li>
                    <li>
                        <a href="/copiadora-higienopolis">
                            <h2><i class="fa fa-angle-right"></i> Copiadora Higienopolis</h2>
                        </a>
                    </li>
                    <li>
                        <a href="/copiadora-saude">
                            <h2><i class="fa fa-angle-right"></i> Copiadora Saúde</h2>
                        </a>
                    </li>
                    <li>
                        <a href="/copiadora-se">
                            <h2><i class="fa fa-angle-right"></i> Copiadora Sé</h2>
                        </a>
                    </li>
                    <li>
                        <a href="/grafica-itaim-bibi">
                            <h2><i class="fa fa-angle-right"></i> Gráfica Itaim Bibi</h2>
                        </a>
                    </li>
                    <li>
                        <a href="/grafica-vila-mariana">
                            <h2><i class="fa fa-angle-right"></i> Gráfica Vila Mariana</h2>
                        </a>
                    </li>
                    <li>
                        <a href="/grafica-pinheiros">
                            <h2><i class="fa fa-angle-right"></i> Gráfica Pinheiros</h2>
                        </a>
                    </li>
                    <li>
                        <a href="/grafica-faria-lima">
                            <h2><i class="fa fa-angle-right"></i> Gráfica Faria Lima</h2>
                        </a>
                    </li>
                    <li>
                        <a href="/grafica-centro">
                            <h2><i class="fa fa-angle-right"></i> Gráfica Centro</h2>
                        </a>
                    </li>
                    <li>
                        <a href="/grafica-jardins">
                            <h2><i class="fa fa-angle-right"></i> Gráfica Jardins</h2>
                        </a>
                    </li>
                    <li>
                        <a href="/grafica-vila-olimpia">
                            <h2><i class="fa fa-angle-right"></i> Gráfica Vila Olímpia</h2>
                        </a>
                    </li>
                    <li>
                        <a href="/grafica-rj">
                            <h2><i class="fa fa-angle-right"></i> Gráfica RJ</h2>
                        </a>
                    </li>
                    <li>
                        <a href="/grafica-sp">
                            <h2><i class="fa fa-angle-right"></i> Gráfica SP</h2>
                        </a>
                    </li>
                    <li>
                        <a href="/grafica-rapida">
                            <h2><i class="fa fa-angle-right"></i> Gráfica Rápida</h2>
                        </a>
                    </li>
                    <li>
                        <a href="/grafica-24h">
                            <h2><i class="fa fa-angle-right"></i> Gráfica 24h</h2>
                        </a>
                    </li>
                    <li>
                        <a href="/copiadora">
                            <h2><i class="fa fa-angle-right"></i> Copiadora para todo Brasil</h2>
                        </a>
                    </li>
                </ul>
            </div>
        </div>
        <div class="col-12 pt-3">
            <h1 class="text-orange h3">
                Serviços de Impressão
            </h1>

            <div class="row category-container ml-2 ml-sm-0">
                <ul>
                    <li>
                        <a href="/impressao-colorida">
                            <h2><i class="fa fa-angle-right"></i> Impressão Colorida</h2>
                        </a>
                    </li>
                    <li>
                        <a href="/impressao-preto-branco">
                            <h2><i class="fa fa-angle-right"></i> Impressão Preto e Branco</h2>
                        </a>
                    </li>
                    <li>
                        <a href="/impressao-barata">
                            <h2><i class="fa fa-angle-right"></i> Impressão Barata</h2>
                        </a>
                    </li>
                    <li>
                        <a href="/impressao-online">
                            <h2><i class="fa fa-angle-right"></i> Impressão Online</h2>
                        </a>
                    </li>
                    <li>
                        <a href="/impressao-empresas">
                            <h2><i class="fa fa-angle-right"></i> Impressão para Empresas</h2>
                        </a>
                    </li>
                    <li>
                        <a href="/servico-impressao">
                            <h2><i class="fa fa-angle-right"></i> Serviço de Impressão</h2>
                        </a>
                    </li>
                    <li>
                        <a href="/impressao-alta-qualidade">
                            <h2><i class="fa fa-angle-right"></i> Impressão em Alta
                                Qualidade</h2>
                        </a>
                    </li>
                    <li>
                        <a href="/impressao-material-estudo">
                            <h2><i class="fa fa-angle-right"></i> Impressão de Material de
                                Estudo</h2>
                        </a>
                    </li>
                    <li>
                        <a href="/impressao-concursos">
                            <h2><i class="fa fa-angle-right"></i> Impressão para Concursos</h2>
                        </a>
                    </li>
                    <li>
                        <a href="/avaliacoes">
                            <h2><i class="fa fa-angle-right"></i> Impressão de Avaliações</h2>
                        </a>
                    </li>
                    <li>
                        <a href="/impressao-provas">
                            <h2><i class="fa fa-angle-right"></i> Impressão de Provas</h2>
                        </a>
                    </li>
                    <li>
                        <a href="/impressao-gabaritos">
                            <h2><i class="fa fa-angle-right"></i> Impressão de Gabaritos</h2>
                        </a>
                    </li>
                    <li>
                        <a href="/impressao-certificado">
                            <h2><i class="fa fa-angle-right"></i> Impressão de Certificados</h2>
                        </a>
                    </li>
                    <li>
                        <a href="/impressao-papel-timbrado">
                            <h2><i class="fa fa-angle-right"></i> Impressão de Papel
                                Timbrado</h2>
                        </a>
                    </li>
                    <li>
                        <a href="/impressao-apostila">
                            <h2><i class="fa fa-angle-right"></i> Impressão de Apostilas</h2>
                        </a>
                    </li>
                    <li>
                        <a href="/impressao-couche">
                            <h2><i class="fa fa-angle-right"></i> Impressão em Couché</h2>
                        </a>
                    </li>
                    <li>
                        <a href="/impressao-a4">
                            <h2><i class="fa fa-angle-right"></i> Impressão em A4</h2>
                        </a>
                    </li>
                    <li>
                        <a href="/impressao-a5">
                            <h2><i class="fa fa-angle-right"></i> Impressão em A5</h2>
                        </a>
                    </li>
                    <li>
                        <a href="/encadernacao-espiral">
                            <h2><i class="fa fa-angle-right"></i> Encadernação em Espiral</h2>
                        </a>
                    </li>
                    <li>
                        <a href="/encadernacao-capa-dura">
                            <h2><i class="fa fa-angle-right"></i> Encadernação em Capa Dura</h2>
                        </a>
                    </li>
                    <li>
                        <a href="/encadernacao-wire-o">
                            <h2><i class="fa fa-angle-right"></i> Encadernação em Wire-o</h2>
                        </a>
                    </li>
                    <li>
                        <a href="/produtos">
                            <h2><i class="fa fa-angle-right"></i> Produtos</h2>
                        </a>
                    </li>
                    <li>
                        <a href="/grafica-digital-online">
                            <h2><i class="fa fa-angle-right"></i> Gráfica Digital</h2>
                        </a>
                    </li>
                    <li>
                        <a href="/grafica-profissional">
                            <h2><i class="fa fa-angle-right"></i> Gráfica Profissional</h2>
                        </a>
                    </li>
                    <li>
                        <a href="/grafica-express">
                            <h2><i class="fa fa-angle-right"></i> Gráfica Express</h2>
                        </a>
                    </li>
                    <li>
                        <a href="/impressao-nf">
                            <h2><i class="fa fa-angle-right"></i> Impressão de Nota Fiscal</h2>
                        </a>
                    </li>
                    <li>
                        <a href="/impressao-boletos">
                            <h2><i class="fa fa-angle-right"></i> Impressão de Boletos</h2>
                        </a>
                    </li>
                    <li>
                        <a href="/impressao-processos">
                            <h2><i class="fa fa-angle-right"></i> Impressão de Processos</h2>
                        </a>
                    </li>
                    <li>
                        <a href="/impressao-treinamentos">
                            <h2><i class="fa fa-angle-right"></i> Impressão para Treinamentos</h2>
                        </a>
                    </li>
                    <li>
                        <a href="/grafica">
                            <h2><i class="fa fa-angle-right"></i> Gráfica</h2>
                        </a>
                    </li>
                    <li>
                        <a href="/impressao-delivery">
                            <h2><i class="fa fa-angle-right"></i> Impressão Delivery</h2>
                        </a>
                    </li>
                    <li>
                        <a href="/impressao-livreto">
                            <h2><i class="fa fa-angle-right"></i> Impressão Livreto</h2>
                        </a>
                    </li>
                    <li>
                        <a href="/impressao-arquivos">
                            <h2><i class="fa fa-angle-right"></i> Impressão de Arquivos</h2>
                        </a>
                    </li>
                    <li>
                        <a href="/impressao-documentos">
                            <h2><i class="fa fa-angle-right"></i> Impressão de Documentos</h2>
                        </a>
                    </li>
                    <li>
                        <a href="/monografia">
                            <h2><i class="fa fa-angle-right"></i> Impressão de Monografia</h2>
                        </a>
                    </li>
                    <li>
                        <a href="/tcc">
                            <h2><i class="fa fa-angle-right"></i> Impressão de TCC</h2>
                        </a>
                    </li>
                    <li>
                        <a href="/impressao-simulados-fuvest">
                            <h2><i class="fa fa-angle-right"></i> Simulados FUVEST</h2>
                        </a>
                    </li>
                    <li>
                        <a href="/impressao-apostila-enem">
                            <h2><i class="fa fa-angle-right"></i> Simulado ENEM</h2>
                        </a>
                    </li>
                    <li>
                        <a href="/impressao-simulados-vestibular">
                            <h2><i class="fa fa-angle-right"></i> Simulados Vestibulares</h2>
                        </a>
                    </li>
                    <li>
                        <a href="/papel-timbrado">
                            <h2><i class="fa fa-angle-right"></i> Papel Timbrado Barato</h2>
                        </a>
                    </li>
                    <li>
                        <a href="/impressao-simulado-unicamp">
                            <h2><i class="fa fa-angle-right"></i> Simulado UNICAMP</h2>
                        </a>
                    </li>
                    <li>
                        <a href="/papelaria">
                            <h2><i class="fa fa-angle-right"></i> Papelaria para Impressão</h2>
                        </a>
                    </li>
                    <li>
                        <a href="/impressao-simulado-unesp">
                            <h2><i class="fa fa-angle-right"></i> Simulado UNESP</h2>
                        </a>
                    </li>
                    <li>
                        <a href="/impressao-encadernacao">
                            <h2><i class="fa fa-angle-right"></i> Impressão e Encadernação</h2>
                        </a>
                    </li>
                    <li>
                        <a href="/impressao-para-empresas">
                            <h2><i class="fa fa-angle-right"></i> Impressão para Escritórios</h2>
                        </a>
                    </li>
                    <li>
                        <a href="/impressao-dissertacao">
                            <h2><i class="fa fa-angle-right"></i> Impressão de Dissertação</h2>
                        </a>
                    </li>
                    <li>
                        <a href="/impressao-rapida">
                            <h2><i class="fa fa-angle-right"></i> Impressão e Entrega Rápida</h2>
                        </a>
                    </li>
                    <li>
                        <a href="/plastificacao">
                            <h2><i class="fa fa-angle-right"></i> Plastificação</h2>
                        </a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</div>

<?php require 'app/views/partial/how_it_works-bar.php';
