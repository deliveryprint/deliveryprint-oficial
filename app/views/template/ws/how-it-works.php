<div class="container">
    <div class="row">
        <div class="container col-sm-12" id="container">
            <div class="row">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item">
                            <a href="/">Home</a>
                        </li>
                        <li class="breadcrumb-item active" aria-current="page">Como Funciona</li>
                    </ol>
                </nav>
            </div>
        </div>
    </div>
</div>

<div class="container">
    <section class="mb-5 text-center">
        <h1 class="text-center text-orange hiw-h1">Como funciona nosso serviço de Impressão?</h1>

        <div class="row">
            <div class="col-md-4">
                <div class="img-step-by-step">
                    <div class="step-number">1</div>
                    <img src="/img/pagina_de_pedido.png" alt="Página de pedidos" class="img-fluid mb-3">
                </div>
                <p class="text-hiw mb-3">Aqui você escolhe como quer sua impressão, se terá algum acabamento e anexa os arquivos em PDF que deseja imprimir</p>
            </div>
            <div class="col-md-4">
                <div class="img-step-by-step">
                    <div class="step-number">2</div>
                    <img src="/img/frete.png" alt="Página de pagamentos" class="img-fluid mb-3">
                </div>
                <p class="text-hiw mb-3">Em seguida irá definir qual opção de entrega será a melhor para seu pedido. Cada opção de entrega tem seu preço estipulado de acordo com agilidade, distancia e configuração de seu pedido de impressão.</p>
            </div>
            <div class="col-md-4">
                <div class="img-step-by-step">
                    <div class="step-number">3</div>
                    <img src="/img/pgto.png" alt="Escolha do frete" class="img-fluid mb-3">
                </div>
                <p class="text-hiw mb-3">No fim, você poderá finalizar seu pedido de impressão escolhendo pagar em boleto bancário a vista ou no seu cartão de crédito, com toda segurança da IUGU. Ai o resto é com a gente, iremos imprimir e entregar para você até a data escolhida.</p>
            </div>
        </div>

        <button class="btn btn-b1 btn-orange" id="btn-hiw-coupon" value="">Orçamento com 10% desconto</button>
    </section>
</div>

<?php require 'app/views/partial/questions_section.php'; ?>

<div class="container">
    <section class="my-5">
        <div class="row align-items-center">
            <div class="col-md-6">
                <h2 class="text-center text-orange my-3">E por que Imprimir na DeliveryPrint?</h2>
                <p class="text-center text-hiw mb-3">Além de toda segurança em seu pedido, nosso sistema proporciona que você tenha mais vantagens como:</p>
                <ul class="mb-5">
                    <li>
                        <p class="text-hiw mb-1">- Temos serviço de entrega expresso</p>
                    </li>
                    <li>
                        <p class="text-hiw mb-1">- Atendimento pessoal, rápido e sincero</p>
                    </li>
                    <li>
                        <p class="text-hiw mb-1">- Aqui você pode pagar no seu cartão em até 3x acima de R$100</p>
                    </li>
                    <li>
                        <p class="text-hiw mb-1">- Trabalhamos apenas com máquinas profissionais</p>
                    </li>
                    <li>
                        <p class="text-hiw mb-1">- Preço abaixo da média, mesmo sendo de ótima qualidade</p>
                    </li>
                    <li>
                        <p class="text-hiw mb-1">- Se errarmos, imprimimos novamente seu trabalho.</p>
                    </li>
                </ul>
                <div class="text-center mb-3">
                    <a href="/impressao-online" class="btn btn-orange btn-hiw">Fazer Pedido</a>
                </div>
            </div>
            <div class="col-md-6">
                <img src="/img/imagem_impressora.jpg" class="img-fluid" alt="Imagem impressora">
            </div>
        </div>
    </section>
</div>

<div class="container">
    <?php require 'app/views/partial/comments.php'; ?>
</div>
