<div class="container text-center">
    <div class="row">
        <div class="col-10 offset-1 col-sm-6 col-lg-4 offset-sm-3 offset-lg-4 ">
            <h3 class="text-orange pb-5 mt-4">Recuperar Senha</h3>
            <form class="row px-3 form-horizontal intec-ajax-form" id="recoverPass" method="post" action="/changePasswordFromRecovery">
                <input type="text" class="form-control col-12 mb-4 d-none" placeholder="Código"  id='hash' name="hash"><br/>
                <input type="password" class="form-control col-12 mb-4" placeholder="Nova Senha" name="password"><br/>
                <button type="submit" class="btn btn-b3 btn-b4-xs text-center btn-orange btn-block">PROSSEGUIR</button>
            </form>

        </div>
    </div>
</div>
