<section class="home-cover text-center">
    <div class="container" id="main-div">
        <h1 class="text-white">Impressão e <br class="d-block d-sm-none">Encadernação Online</h1>
        <a href="/impressao-online" class="btn btn-orange btn-b2">FAZER ORÇAMENTO</a>
        <div class="d-flex flex-wrap justify-content-center flex-column flex-sm-row">
            <button class="btn btn-b6 text-white text-uppercase bg-transparent border-0 mb-3" @click="setCouponCookie('MELHORESPRECOS2019')">
                <i class="fa fa-usd" aria-hidden="true"></i>
                Melhores Preços
            </button>
            <button class="btn btn-b6 text-white text-uppercase bg-transparent border-0 mb-3" onclick='location.href="/impressao-online"'>
                <i class="fa fa-truck" aria-hidden="true"></i>
                Entregamos Rápido
            </button>
            <button class="btn btn-b6 text-white text-uppercase bg-transparent border-0 mb-3" onclick='location.href="/impressao-online"'>
                <i class="fa fa-shield" aria-hidden="true"></i>
                Qualidade Superior
            </button>
        </div>
    </div>
</section>

<section class="product-cards d-flex justify-content-center flex-column flex-sm-row py-2">
    <div class="card-bg card-bg-c1 mb-2 mx-2">
        <a href="/impressao-apostila">
            <div class="card-container">
                <p class="card-txt text-white d-inline-block p-2 h4">Mais vendido</p><br>
                <h2 class="card-txt text-white d-inline-block p-2 h2">Imprimir Apostila</h2>
            </div>
        </a>
    </div>
    <div class="card-bg card-bg-c2 mb-2 mx-2">
        <a href="/impressao-preto-branco">
            <div class="card-container">
                <p class="card-txt text-white d-inline-block p-2 h4">Custo x Benefício</p><br>
                <p class="card-txt text-white d-inline-block p-2 h2">Impressão em PB A4</p>
            </div>
        </a>
    </div>
    <div class="card-bg card-bg-c3 mb-2 mx-2">
        <a href="/impressao-colorida">
            <div class="card-container">
                <p class="card-txt text-white d-inline-block p-2 h4">Menor preço!</p><br>
                <h3 class="card-txt text-white d-inline-block p-2 h2">Impressão Colorida</h3>
            </div>
        </a>
    </div>
</section>

<?php require 'app/views/partial/how_it_works-bar.php'; ?>

<?php require 'app/views/partial/comments.php'; ?>

<?php require 'app/views/partial/questions_section.php'; ?>

<section class="services-section container text-center">
    <h2 class="services-title text-orange mb-5">Fazemos diversos serviços de impressão</h2>
    <div class="row">
        <div class="col-12 col-md-6">
            <a href="/pedido-empresas">
                <picture>
                    <source srcset="img/home/small-home-services1.webp" type="image/webp">
                    <source srcset="img/home/small-home-services1.jpg" type="image/jpeg">
                    <img class="img-fluid mb-3" src="img/home/small-home-services1.jpg" alt="Impressão para Empresas">
                </picture>
                <h2>Impressão para Empresas</h2>
            </a>
        </div>
        <div class="col-12 col-md-6">
            <a href="impressao-preto-branco">
                <picture>
                    <source srcset="img/home/small-home-services2.webp" type="image/webp">
                    <source srcset="img/home/small-home-services2.jpg" type="image/jpeg">
                    <img class="img-fluid mb-3" src="img/home/small-home-services2.jpg" alt="Impressão PB por até R$0,19">
                </picture>
                <h2>Impressão PB por até R$0,20</h2>
            </a>
        </div>
        <!-- <div class="col-12 col-md-4">
            <a href="impressao-couche">
                <picture>
                    <source srcset="img/home/small-home-services3.webp" type="image/webp">
                    <source srcset="img/home/small-home-services3.jpg" type="image/jpeg">
                    <img class="img-fluid mb-3" src="img/home/small-home-services3.jpg" alt="Impressão em Papel Couchê">
                </picture>
                <h2>Impressão em Papel Couchê</h2>
            </a>
        </div> -->
    </div>
</section>

<?php require 'app/views/partial/discount-bar.php'; ?>
