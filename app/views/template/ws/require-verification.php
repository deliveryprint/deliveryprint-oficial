<div id="verification-app" class="container text-center">
    <div class="row" v-if="!loading">
        <div class="col-10 offset-1 col-sm-6 col-lg-4 offset-sm-3 offset-lg-4 ">
            <h3 class="text-orange pb-5 mt-4">Verificação de E-mail</h3>

            <p class="mb-3">
                Para continuar, precisamos que você confirme seu endereço de e-mail.
                É bem simples! Nós já te enviamos um e-mail com as instruções.
            </p>

            <button type="button" class="btn btn-b3 btn-orange border-radius-md" @click="requestResend()">
                Reenviar e-mail de confirmação
            </button>

        </div>
    </div>
</div>
