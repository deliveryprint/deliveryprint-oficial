<div class="container" id="orderApp" v-cloak>
    <transition name="fade"><div class="overlay" :style="{ zIndex: overlayZIndex }" v-if="overlay"></div></transition>
    <div class="row">
        <div class="container col-sm-12" id="container">
            <div class="row">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item">
                            <a href="/">Home</a>
                        </li>
                        <li class="breadcrumb-item active" aria-current="page">Impressão Online</li>
                    </ol>
                </nav>
            </div>
        </div>
    </div>
    <div class="row">
        <div id="alerta" class="alert col-sm-12  alert-danger" style="display: none">
            <strong>Atenção!</strong> Verifique se todas as informações estão preenchidas e tente novamente.
        </div>
    </div>
    <div class="row">
        <div class="informacoes col-sm-9 mb-3">
            <div id="accordion">
                <div class="card">
                    <div class="card-header" id="headingOne" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                        <h1 class="btn btn-link collapsed default-text mb-0">
                            <i class="fa fa-tint"></i> &nbsp; Escolha a cor de sua Impressão
                        </h1>
                    </div>
                    <div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#accordion">
                        <div class="d-flex flex-column flex-sm-row">
                            <div class="clearfix">
                                <div class="card float-left" v-for="(color, index) in colors">
                                    <div class="card-body">
                                        <button type="button" class="btn btn-opcoes btn-cor icone" data-toggle="collapse" v-bind:class="{color: color.hasColor, active: curColor.id == color.id}" v-on:click="updateColor(index);">
                                            <img class="card-img-top" v-bind:src="'img/' + color.icon">
                                            <br>
                                            <div class="footer-card">
                                                <span class="card-text">
                                                    <b>{{color.description}}</b>
                                                </span>
                                                <p class="card-text text-center" v-if="color.id == 1">
                                                    de R${{priceBkMin | moneyFormatter}} até R${{priceBkMax | moneyFormatter}}/pág
                                                </p>
                                                <p class="card-text text-center" v-else>
                                                    de R${{priceColorMin | moneyFormatter}} até R${{priceColorMax | moneyFormatter}}/pág
                                                </p>
                                            </div>
                                        </button>
                                    </div>
                                </div>
                            </div>
                            <div class="flex-fill d-flex justify-content-center">
                                <div class="tutorial-step-container align-self-center">
                                    <transition name="slide-fade">
                                        <div class="tutorial-step tutorial-step-1" v-if="showStep1">
                                            <p class="tutorial-p">Nesta página você escolherá como quer que sua impressão seja feita.</p>
                                            <p class="tutorial-p">No final da página iremos te dar o prazo e o preço de seu pedido!</p>
                                            <div class="tutorial-btn-container text-center clearfix">
                                                <button class="btn btn-outline-light tutorial-btn" @click="closeStep">Entendi!</button>
                                            </div>
                                            <div class="tutorial-arrow"></div>
                                        </div>
                                    </transition>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header" id="headingTwo" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                        <h2 class="btn btn-link collapsed default-text mb-0 pl-0">
                            <i class="fa fa-object-group"></i> &nbsp; Escolha o lado que será impresso
                        </h2>
                    </div>
                    <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordion">
                        <div class="card float-left" v-for="(side, index) in sides">
                            <div class="card-body col-sm-12">
                                <button type="button" v-bind:class="{active: curSide.id == side.id}" class="btn btn-opcoes btn-lado btn-frente icone" data-toggle="collapse" v-on:click="updateSide(index)">
                                    <img class="card-img-top" v-bind:src="'img/' + side.icon" v-bind:alt="side.description">
                                    <br>
                                    <div class="footer-card">
                                        <span class="card-text">
                                            <b>{{side.description}}</b>
                                        </span>
                                        <p class="card-text text-center info-adicional" v-if="!side.value">
                                            Sem Adicional
                                            <br>&nbsp;
                                            <br>
                                        </p>
                                        <p class="card-text text-center info-adicional" v-else>
                                            Adicional
                                            <br>R$ {{side.value | moneyFormatter}}/pág.
                                        </p>
                                    </div>
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header" id="headingThree" data-toggle="collapse" data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                        <h2 class="btn btn-link collapsed default-text mb-0 pl-0">
                            <i class="fa fa-file-text-o"></i> &nbsp; Escolha o papel
                        </h2>
                    </div>
                    <!--inicio id="collapseThree" class="collapse"-->
                    <div id="collapseThree" class="collapse" aria-labelledby="headingThree" data-parent="#accordion">
                        <div class="card float-left" v-for="(pt, index) in visiblePaperTypes">
                            <div class="box-header text-center" v-if="pt.description == 'Papel Sulfite A4 75g'">
                                <p class="header-title">Menor Preço</p>
                            </div>
                            <div class="mt-3" v-else>
                            </div>
                            <div class="card-body">
                                <button v-if=
                                "!(curSide.id == 1 && (pt.description == 'Papel Glossy A4 135g' ||
                                                    pt.description == 'Papel Glossy A4 220g' ||
                                                    pt.description == 'Papel Matte A4 230g'))"
                                type="button" v-bind:class="{active: curPaper.id === pt.id}" class="btn btn-opcoes btn-paper icone" v-on:click="updatePageType(index)" data-toggle="collapse">
                                    <img class="card-img-top" v-bind:src="'img/' + pt.icon" v-bind:alt="pt.description">
                                    <br>
                                    <div class="footer-card">
                                        <span class="card-text paper">
                                            <b>{{pt.description}}</b>
                                            <br>
                                            <b v-if="pt.description == 'Papel Sulfite A4 75g'">(Comum)</b>
                                            <b v-if="pt.description == 'Papel Sulfite A4 90g'">(Alta qualidade)</b>
                                            <b v-if="pt.description == 'Papel Glossy A4 135g'">(Papel BRILHO)</b>
                                            <b v-if="pt.description == 'Papel Glossy A4 220g'">(Papel BRILHO)</b>
                                            <b v-if="pt.description == 'Papel Couché A3 150g'">(Papel BRILHO)</b>
                                            <b v-if="pt.description == 'Papel Matte A4 230g'">(Papel FOSCO)</b>
                                        </span>
                                        <p class="card-text text-center info-adicional" v-if="!pt.value">
                                            Sem Adicional
                                            <br>&nbsp;
                                            <br>
                                        </p>
                                        <p class="card-text text-center info-adicional" v-else>
                                            Adicional
                                            <br>R$ {{pt.value | moneyFormatter}}/pág.
                                        </p>
                                        <div>
                                </button>
                            </div>
                        </div>
                        <div class="card float-left">
                            <div class="card-body">
                                <button class="btn-toggle btn btn-mais-opcoes icone" v-if="visiblePaperTypes.length < 4" v-on:click="toggleVisiblePaperTypes()">
                                    <img class="card-img-top" src="img/mais.svg">
                                    <p class="card-text text-center" style="margin-top: -40px;"> Clique aqui para mais Opções</p>
                                </button>
                                <button class="btn-toggle btn btn-mais-opcoes icone" v-if="visiblePaperTypes.length > 3" v-on:click="toggleVisiblePaperTypes()">
                                    <img class="card-img-top" src="img/menos.svg">
                                    <p class="card-text text-center" style="margin-top: -40px;"> Menos Opções</p>
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header" id="headingFour" data-toggle="collapse" data-target="#collapseFour" aria-expanded="false" aria-controls="collapseFour">
                        <h2 class="btn btn-link collapsed default-text mb-0 pl-0">
                            <i class="fa fa-adjust"></i> Escolha o Acabamento
                        </h2>
                    </div>
                    <div id="collapseFour" class="collapse" aria-labelledby="headingFour" data-parent="#accordion">
                        <div class="card float-left" v-for="(f, index) in visibleFinishingTypes">
                            <div class="card-body">
                                <button type="button" v-bind:class="{active: curFinishing.id === f.id}" class="btn btn-opcoes btn-finishing icone" data-toggle="collapse" v-on:click="updateFinishing(index)">
                                    <img class="card-img-top" v-bind:src="'img/' + f.icon" v-bind:alt="f.description">
                                    <br>
                                    <div class="footer-card">
                                        <span class="card-text finishing">
                                            <b>{{f.description}}</b><br>
                                            <b v-if="f.description == 'Encadernar Capa Dura A4'">(Ideal para TCC)</b>
                                        </span>
                                        <p class="card-text text-center info-adicional" v-if="!f.value">
                                            Sem Adicional
                                            <br>&nbsp;
                                            <br>
                                        </p>
                                        <p class="card-text text-center info-adicional" v-else>
                                            Adicional
                                            <br>R$ {{f.value | moneyFormatter}}/pág.
                                        </p>
                                        <div>
                                </button>
                            </div>
                        </div>
                        <div class="card float-left" style="display:inline">
                            <div class="card-body">
                                <button class="btn-toggle btn btn-mais-opcoes icone" v-if="visibleFinishingTypes.length > 3" v-on:click="toggleVisibleFinishingTypes()">
                                    <img class="card-img-top" src="img/mais.svg">
                                    <p class="card-text text-center" style="margin-top: -30px;"> Clique aqui para mais Opções</p>
                                </button>
                                <button class="btn-toggle btn btn-mais-opcoes icone" v-if="visibleFinishingTypes.length > 3" v-on:click="toggleVisibleFinishingTypes()">
                                    <i class="fa fa-minus-square fa-4x fa-lg" style="color:#FC4A1A;"></i>
                                    <p class="card-text text-center" style="margin-top: 10px;"> Menos Opções</p>
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
                <!--div card-->
                <div class="card" id="card-anexar">
                    <div class="card-header" id="headingFive" data-toggle="collapse" data-target="#collapseFive" aria-expanded="false" aria-controls="collapseFive">
                        <h2 class="btn btn-link default-text mb-0 pl-0">
                            <i class="fa fa-paperclip"></i> &nbsp; Anexar Arquivo PDF
                        </h2>
                    </div>
                    <div id="collapseFive" class="collapse" aria-labelledby="headingFive" data-parent="#accordion">
                        <div class="d-flex flex-column flex-sm-row">
                            <div class="clearfix">
                                <div class="card float-left">
                                    <div class="card-body">
                                        <button v-bind:class="{active: isAttachment}" class="btn btn-anexar btn-opcoes icone" data-toggle="modal" v-on:click="isFileEnabled">
                                            <img class="card-img-top " src="/img/anexo.svg" alt="frente e verso">
                                            <br>
                                            <div class="footer-card">
                                                <p class="card-text text-center">
                                                    <b>Anexar arquivo(s)</b>
                                                    <br>Para contagem
                                                    <br>automática de páginas.</p>
                                                <div>
                                        </button>
                                    </div>
                                </div>
                                <!-- <div class="card-separator d-none d-sm-block">OU</div> -->
                                <div class="card float-left">
                                    <div class="card-body">
                                        <button id="manualInput" v-bind:class="{active: isManual}" class="btn btn-opcoes icone" data-toggle="modal" v-on:click="isManualEnabled">
                                            <img class="card-img-top" src="/img/calculadora.svg" alt="frente e verso">
                                            <div class="footer-card">
                                                <p class="card-text text-center">
                                                    <b>Informar Nº de Páginas</b>
                                                    <br>Informar manualmente
                                                    <br> o número de páginas.</p>
                                            </div>
                                        </button>
                                    </div>
                                </div>
                            </div>
                            <div class="flex-fill d-flex justify-content-center">
                                <div class="tutorial-step-container align-self-center">
                                    <transition name="slide-fade">
                                        <div class="tutorial-step tutorial-step-2" v-if="showStep2">
                                            <p class="tutorial-p">Aqui você anexa seu PDF para o site calcular a quantidade de páginas, pode inserir quantos arquivos precisar.</p>
                                            <p class="tutorial-p">Ou</p>
                                            <p class="tutorial-p">Pode também informar manualmente quantas páginas quer imprimir.</p>
                                            <div class="tutorial-btn-container clearfix text-center">
                                                <button class="btn btn-outline-light tutorial-btn" @click="closeStep">Entendi!</button>
                                            </div>
                                            <div class="tutorial-arrow"></div>
                                        </div>
                                    </transition>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!--fim div card-->
                <!--div card-->
                <div class="card">
                    <div class="card-header" id="headingSix" data-toggle="collapse" data-target="#collapseSix" aria-expanded="false" aria-controls="collapseSix">
                        <h3 class="btn btn-link default-text mb-0 pl-0">
                            <i class="fa fa-truck"></i> &nbsp; Frete e Previsão de Entrega
                        </h3>
                    </div>

                    <div class="text-right d-md-none">
                        <button class="btn btn-cupom-mob border-0 text-success" data-toggle="modal" data-target="#modalCupom">
                            <i class="fa fa-percent"></i> Cupom de Desconto</button>
                    </div>
                </div>

            </div>
            <!-- END ACCORDION -->
            <!---inicio div id="collapseSix" -->
            <div id="collapseSix" class="collapse" aria-labelledby="headingSix" data-parent="#accordion">
                <div class="row" style="margin-top: 10px;">
                    <div class="col-md-12">
                        <div class="frete-retirada">
                            <div class="form-check">
                                <label class="form-check-label radio-btn" v-bind:class="{active: delivery == 1}">
                                    <input class="form-check-input" type="radio" name="gridRadios" value="1" v-model="delivery" v-on:change="changeFreight()"> Entrega:
                                    <span class="checkmark"></span>
                                </label>
                            </div>
                            <!-- <div class="form-check ml-1 discount-fix">
                                <label class="form-check-label radio-btn" v-bind:class="{active: delivery == 0}">
                                    <input class="form-check-input" type="radio" name="gridRadios" value="0" v-model="delivery" v-on:change="changeFreight()">
                                    <span>Retirada:</span>
                                    <span class="text-success f-14 d-block d-sm-inline-block discount-span ml-1"> 5% DESCONTO</span>
                                    <span class="checkmark"></span>
                                </label>
                            </div> -->
                        </div>
                    </div>
                </div>
                <div class="row" v-if="delivery == 1">
                    <div class="col-md-12">
                        <p class="lblCep ml-0">Digite seu CEP:</p>
                        <div class="row">
                            <input class="form-control input-frete" type="text" name="cep" v-mask="'#####-###'" placeholder="00000-000" v-model="zip" v-on:keyup="calculateShippingNoDelay()">
                            <img class="spinner-loader mt-3" src="/img/spinner-loader.gif" v-bind:class="{'d-none': !calculatingZip}">
                            <p class="address-line" v-if="zipResult && delivery == 1">{{addressLine1}}
                                <br>{{addressLine2}}</p>
                        </div>
                    </div>
                </div>

                <div class="form-retirada data-retirada mb-4" v-if="delivery == 0">
                    <p class="text-retirada">Endereço para retirada:</p>
                    <p class="textEndereco"> Rua Oscar Freire 2617, cj 410,
                        <br>Pinheiros, CEP 05409-012
                        <br>São Paulo SP - Brasil.</p>
                    <p class="text-retirada-mtr">Próximo ao metrô Sumaré - Linha 2 Verde</p>
                </div>

                <div class="row" :class="{'d-none': !loadingFreights}">
                    <div class="col text-center">
                        <img class="spinner-loader mt-5" src="/img/spinner-loader.gif" style="width: 45px; height: 45px;">
                    </div>
                </div>

                <freight-box-list :freights="availableDeliveryTypes" :is-delivery="true" :cur-price-no-freight="curPriceNoFreight" :cur-price-no-freight-no-finishing="curPriceNoFreigtNoFinishing" :total-pages="totalPages" v-if="shippingResult && delivery == 1" @freight-clicked="isDisabled = true, setDeliveryType($event)">
                </freight-box-list>

                <freight-box-list :freights="availableWithdrawalTypes" :is-delivery="false" :cur-price-no-freight="curPriceNoFreight" :cur-price-no-freight-no-finishing="curPriceNoFreigtNoFinishing" :total-pages="totalPages" v-if="delivery == 0" @freight-clicked="isDisabled = true, setWithdrawalType($event)">
                </freight-box-list>
            </div>

        </div>
        <!-- end collapse six -->
        <div class="div-resumo col-md-3">
            <div class="card resumo-lateral">
                <div class="card-header-resumo resumo-hide-mob">
                    <h5 class="card-title">Resumo</h5>
                    <ul class="list-group">
                        <li class="list-group-item text-darkgrey">Cor
                            <span class="selected-iitem text-grey">{{curColor.description}}</span>
                        </li>
                        <li class="list-group-item text-darkgrey">Lado
                            <span class="selected-iitem text-grey">{{curSide.description}}</span>
                        </li>
                        <li class="list-group-item text-darkgrey">Papel
                            <span class="selected-iitem text-grey">{{curPaper.description}}</span>
                        </li>
                        <li class="list-group-item text-darkgrey">Acabamento
                            <span class="selected-iitem text-grey">{{curFinishing.description}}</span>
                        </li>
                        <li class="list-group-item text-darkgrey" @click="isFileEnabled" style="cursor: pointer">Nº de Páginas
                            <span class="selected-iitem text-grey">{{totalPages || ''}}</span>
                        </li>
                    </ul>
                </div>
                <div class="card-header-resumo transparent resumo-hide-mob">
                    <div class="row justify-content-right cupom-desconto m-0 mt-3">
                        <div class="cupom text-orange">
                            Inserir Cupom de Desconto
                        </div>
                        <div class="row m-0">
                            <i class="fa fa-times-circle text-orange mt-3 mr-2" v-if="wrongCoupon" v-on:click="removeCoupon()"></i>
                            <i class="fa fa-check-circle text-success mt-3 mr-2" v-if="hasCoupon()"></i>
                            <input type="text" v-model="coupon" maxlength="25" class="form-control" v-bind:class="{'input-success': hasCoupon(), 'input-warning': wrongCoupon}" v-bind:readonly="hasCoupon()">
                            <i class="fa fa-times-circle mt-3 mr-2" @mouseover="mouseOver = true" @mouseleave="mouseOver = false" :class="mouseOver ? 'text-orange' : 'text-grey'" v-if="hasCoupon()" @click="removeCoupon()"></i>
                            <button class="btn-aplicar" v-if="!hasCoupon()" v-on:click="addCoupon()">
                                APLICAR
                            </button>
                        </div>
                    </div>
                    <div class="row text-center" id="messagePopover">
                        <!-- <button id="submit-car" class="btn btn-b3 btn-block submit-car ml-3" v-bind:disabled="isNotComplete" v-bind:class='{"btn-orange": !isNotComplete, "btn-grey": isNotComplete}'
                            v-on:click="toShoppingCart('direita')">
                            ADICIONAR AO CARRINHO
                        </button> -->
                        <button id="submit-car" class="btn btn-b3 btn-block submit-car ml-3 btn-grey" v-bind:disabled="!showFreight" v-on:click="showDeliveryWarning()">
                            ADICIONAR AO CARRINHO
                        </button>
                    </div>
                    <div id="alerta" class="alert col-sm-12  alert-danger" style="display: none">
                        <strong>Atenção!</strong> Verifique se todas as informações estão preenchidas e tente novamente.
                    </div>

                </div>
            </div>
        </div>
    </div>
    <div class="row margin-md-top margin-hg-bottom">
        <div class="col-sm-9 text-right">
            <!-- <button class="btn btn-b3 btn-b4-xs mx-auto btn-left-margin submit-car" id='addCar' v-bind:disabled="isNotComplete" v-bind:class='{"btn-orange": !isNotComplete, "btn-grey": isNotComplete}'
                v-on:click="toShoppingCart('esquerda')"> ADICIONAR AO CARRINHO </button> -->
            <button class="btn btn-b3 btn-b4-xs mx-auto btn-left-margin submit-car btn-grey" id='addCar' v-bind:disabled="!showFreight" v-on:click="showDeliveryWarning()"> ADICIONAR AO CARRINHO </button>
        </div>
    </div>
    <div class="modal fade" id="modalCupom" tabindex="-1" role="dialog" aria-labelledby="modalCupom" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <div class="col-sm-12 text-left">
                        <h3 class="text-orange">Inserir Cupom de Desconto</h3>
                    </div>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>

                </div>
                <div class="modal-body text-left body-cupom">
                    <div class="text-left">
                        <i class="fa fa-times-circle text-orange mt-3" v-if="wrongCoupon" v-on:click="removeCoupon()"></i>
                        <i class="fa fa-check-circle text-success mt-3 mr-0" v-if="hasCoupon()"></i>
                        <input type="text" v-model="coupon" maxlength="25" class="form-control" v-bind:class="{'text-success': hasCoupon(), 'text-orange': wrongCoupon}" v-bind:readonly="hasCoupon()">
                    </div>
                </div>
                <div class="modal-footer">
                    <div class="col-sm-12 text-right">
                        <button type="button" class="btn btn-b5 border-radius-md btn btn-orange border-md border-0" v-if="!hasCoupon()" v-on:click="addCoupon()">Prosseguir</button>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <page-loader :show="addingToCart"></page-loader>

</div>

<div class="modal fade bd-example-modal-lg file-info-modal" id="fileInfoApp" tabindex="-1" role="dialog" aria-labelledby="infoModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content md-width md-top">
            <div class="modal-header">
                <h3 class="modal-title default-text text-orange" id="infoModalLabel">Informe o total de páginas</h3>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body col-sm-12 clone">
                <div class="informacoes_file ml-3" v-for="(f, index) in files">
                    <div class="label font-weight-normal text-grey control-label margin-md-top">
                        Quantas páginas tem o arquivo?
                        <br>
                        <div class="input-number margin-xxs-top" v-if="f">
                            <input type="text" class="text-left" v-mask="['#', '##', '###', '####', '#####']" v-model="f.numPages" value="1" v-on:input="calcTotalPages()">
                            <div class="incrase-number" v-on:click="increaseNumPages(f)">
                                <div class="incrase-button"></div>
                            </div>
                            <div class="decrase-number" v-on:click="decreaseNumPages(f)">
                                <div class="incrase-button">
                                </div>
                            </div>
                        </div>
                        <!-- <input class="form-control input-width" placeholder="1" step="1" min="1" type="number" v-model="f.numPages" v-on:input="calcTotalPages()"
                            v-on:click="calcTotalPages()" /> -->
                    </div>
                    <div class="label font-weight-normal text-grey control-label margin-md-top">Quantas cópias do arquivo {{index + 1}}?
                        <br>
                        <div class="row ml-0">
                            <div class="input-number margin-xxs-top">
                                <input type="text" class="text-left" v-mask="['#', '##', '###', '####']" value="1" v-model="f.numCopies" v-on:input="calcTotalPages()">
                                <div class="incrase-number" v-on:click="increaseNumCopies(f)">
                                    <div class="incrase-button"></div>
                                </div>
                                <div class="decrase-number" v-on:click="decreaseNumCopies(f)">
                                    <div class="incrase-button">
                                    </div>
                                </div>
                            </div>
                            <div class="col-6 mt-3" v-if='index > 0'>
                                <a class="text-orange btn p-0" @click='removeItem(index)'>Excluir item</a>
                            </div>
                        </div>
                        <!-- <input class="form-control input-width" min="1" step="1" type="number" placeholder="1" v-model="f.numCopies" v-on:input="calcTotalPages()"
                            v-on:click="calcTotalPages()" /> -->
                    </div>
                    <hr>
                </div>

                <div class="informacoes_file">
                    <label class="label col-12 text-left font-weight-normal text-grey margin-md-top pl-0 ml-0">Total de páginas: &nbsp;
                        <span class="font-weight-normal text-orange border-0"> {{fiTotalPages}} página(s)</span>
                    </label>
                </div>
            </div>
            <div class="modal-footer">
                <button id="novo" class="btn btn-link d-none d-sm-block" v-on:click="newItem()">NOVO ITEM</button>
                <button id="btnseguir-nPages" class="btn btn-b3 btn-orange" data-dismiss="modal" v-on:click="infoNext()"> Prosseguir</button>
            </div>
        </div>
    </div>
</div>

<!-- Modal anexo-->
<div class="modal fade bd-example-modal-lg anexoModal" id="anexoModal" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h3 class="modal-title default-text anexoModalLabel">Anexar Arquivo PDF</h3><br>
                <span class="font-weight-normal text-orange border-0 txt-encardenacao">AVISO (para mais de um arquivo) <br>Os arquivos são impressos e encadernados (se houver encadernação) na ordem em que forem anexados aqui.</span>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <table class="d-sm-none" id="fileNames" v-if="files.length > 0">
                    <tr class="default-text">
                        <th>Nº.</th>
                        <th>Arquivo</th>
                        <th>Págs.</th>
                        <th></th>
                    </tr>
                    <tr v-for="(f, index) in files">
                        <td>
                            <span class="default-text text-grey">{{ index + 1 }}</span>
                        </td>
                        <td>
                            <span class="default-text text-grey" v-if="f">{{ f.userFileName }}</span>
                        </td>
                        <td>
                            <span class="default-text text-grey" v-if="f">{{ f.numPages }}</span>
                        </td>
                        <td style="vertical-align: top">
                            <span class="default-text text-grey" v-on:click="trigRemove(index)">
                                <i class="fa fa-trash fa-2x"></i>
                            </span>
                        </td>
                    </tr>
                </table>
                <br />
                <form id="formOrder" enctype="multipart/form-data">
                    <div class="file-loading">
                        <input id="file-0a" type="file" name="file[]" multiple accept=".pdf">
                    </div>
                    <br>
                </form>
                <div id='copies' class="row text-left text-blue row-attachment-copies padding-md-bottom">
                    <h3 class="default-text text-left text-orange col-sm-12 mb-2" id="n-impress">Escolha o número de cópias
                        <i id="anexoPdfPopOver"></i>
                    </h3>
                    <h6 class="text-grey mb-2 col-12">Insira o numero de vezes que você quer imprimir esse arquivo.</h6>
                    <br />
                    <div class="col-auto  margin-sm-bottom" v-for="(f, index) in files">
                        Nº de cópias do
                        <br> arquivo {{index + 1}}
                        <div class="input-number margin-xxs-top" v-if="f">
                            <input type="text" class="text-left" v-mask="['#', '##', '###', '####']" v-model="f.numCopies" v-on:input="calcTotalPages()">
                            <div class="incrase-number d-none d-sm-block" v-on:click="increaseNumCopies(f)">
                                <div class="incrase-button"></div>
                            </div>
                            <div class="decrase-number d-none d-sm-block" v-on:click="decreaseNumCopies(f)">
                                <div class="incrase-button">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="modal-footer p-0">
                    <div class="input_fields_wrap"></div>
                    <button class="submit-car btn btn-b3 btn-orange border-radius-md" id="btno" data-dismiss="modal" v-on:click="attachmentNext()" v-bind:disabled="!haveFiles()">Prosseguir</button>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Modal anexo 2-->
<div class="modal fade bd-example-modal-lg anexoModal" id="anexoModal2" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h3 class="modal-title default-text anexoModalLabel">Anexar Arquivo PDF</h3><br>
                <span class="font-weight-normal text-orange border-0 txt-encardenacao">AVISO (para mais de um arquivo) <br>Os arquivos são impressos e encadernados (se houver encadernação) na ordem em que forem anexados aqui.</span>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form id="formAtt2" enctype="multipart/form-data">
                    <div class="file-loading">
                        <input id="file-0b" type="file" name="file[]" multiple accept="application/pdf">
                    </div>
                    <br>
                </form>
                <div id='copies2' class="row text-left text-blue row-attachment-copies padding-md-bottom">
                    <h3 class="default-text text-left text-orange col-sm-12" id="n-impress"></h3>
                    <br />
                    <div class="col-auto col-md-2 margin-sm-bottom" v-for="(f, index) in getFiles()">
                        Arquivo {{index + 1}}:
                        <br> <span v-if="f"> {{f.numPages}} Página(s) </span>
                        <br> <span v-if="f"> {{f.numCopies}} Cópia(s) </span>
                    </div>
                </div>
                <div class="modal-footer p-0">
                    <div class="input_fields_wrap"></div>
                    <button class="submit-car btn btn-b3 btn-orange border-radius-md" id="btno2" v-on:click="tryToSave()">Prosseguir</button>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="modalEncadernacao" tabindex="-1" role="dialog" aria-labelledby="infoModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content md-width md-top">
            <div class="modal-header">
                <h3 class="modal-title default-text text-orange" id="infoModalLabel">Características da Encadernação</h3>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body col-sm-12 clone pl-md-5 pl-sm-3 finishing-details">

                <div class="etapa-enca">
                    <p class="text-grey">(Etapa {{ step }} de {{ totalSteps }})</p>
                </div>

                <div class="border-type row" v-if="step == 1">
                    <div class="card">
                        <div class="card-body">
                            <button data-toggle="modal" data-target="#" class="btn btn-opcoes icom ic1 icone border-radius-lg" v-bind:class="{'active': border == 'Borda Vertical'}" v-on:click="setBorder('Borda Vertical')">
                                <img src="/img/borda_curta.svg" alt="frente e verso" class="card-img-top">
                                <br>
                                <div class="footer-card">
                                    <p class="card-text text-center">
                                        <b>Lado Maior</b>
                                    </p>
                                </div>
                            </button>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-body">
                            <button data-toggle="modal" data-target="#" v-on:click="setBorder('Borda Horizontal')" class="btn btn-opcoes icom ic2 icone border-radius-lg" v-bind:class="{'active': border == 'Borda Horizontal'}">
                                <img src="/img/borda_longa.svg" alt="frente e verso" class="card-img-top">
                                <br>
                                <div class="footer-card">
                                    <p class="card-text text-center">
                                        <b>Lado Menor</b>
                                    </p>
                                </div>
                            </button>
                        </div>
                    </div>
                </div>

                <div class="border-quantity" v-if="step == 2">
                    <div class="label text-left font-weight-normal text-grey margin-md-top pl-0">
                        <span class="font-weight-normal text-orange border-0 txt-encardenacao">AVISO <br>Aqui você informa a quantidade de Encadernações que seu pedido terá. <br>EX: irei anexar 3 PDFs, cada um será uma apostila diferente, então terei que informar 3 encadernações agora no seletor abaixo:</span><br><br>
                        <span id="pop" class="font-weight-normal text-orange border-0 txt-encardenacao">Quantas encadernações você precisa?</span>
                        <span id="finishingModalButton" class="cursor-pointer" data-toggle="modal" data-target="#finishingInfoModal">
                            <i class="fa fa-question-circle text-blue f-20" aria-hidden="true"></i>
                        </span>
                        <br>
                        <div class="input-number margin-xxs-top">
                            <input type="text" class="input-encadernacao text-left orange-txt" v-mask="['#', '##', '###', '####']" v-model="total" name="qtdEncadernacoes">
                            <div class="incrase-number" v-on:click="inc()">
                                <div class="incrase-button orange-txt"></div>
                            </div>
                            <div class="decrase-number" v-on:click="dec()">
                                <div class="incrase-button orange-txt"></div>
                            </div>
                        </div>
                        <p class="text-orange pt-3">{{observation}}</p>
                    </div>
                </div>

                <div class="details-cor-da-capa" v-if="step == 3">
                    <h3 class="text-orange">Escolha a cor de sua capa dura</h3>
                    <div class="form-check" v-for="(opt, idx) in corDaCapaOptions">
                        <input class="form-check-input" type="radio" name="corDaCapaRadio" :id="'corDaCapaRadio' + idx" :value="idx" v-model="corDaCapa">
                        <label class="form-check-label" :for="'corDaCapaRadio' + idx">
                            {{ opt }}
                        </label>
                    </div>
                </div>

                <div class="details-cor-da-letra" v-if="step == 4">
                    <h3 class="text-orange">Qual a cor da letra na capa?</h3>
                    <div class="form-check" v-for="(opt, idx) in corDaLetraOptions">
                        <input class="form-check-input" type="radio" name="corDaLetraRadio" :id="'corDaLetraRadio' + idx" :value="idx" v-model="corDaLetra">
                        <label class="form-check-label" :for="'corDaLetraRadio' + idx">
                            {{ opt }}
                        </label>
                    </div>
                </div>

                <div class="details-texto-capa" v-if="step == 5">
                    <h3 class="text-orange">O que será escrito na capa de seu TCC?</h3>
                    <div class="form-check" v-for="(opt, idx) in textoCapaOptions">
                        <input class="form-check-input" type="radio" name="textoCapaRadio" :id="'textoCapaRadio' + idx" :value="idx" v-model="textoCapa">
                        <label class="form-check-label" :for="'textoCapaRadio' + idx">
                            {{ opt }}
                        </label>
                    </div>
                </div>

                <div class="details-texto-lombada" v-if="step == 6">
                    <h3 class="text-orange">O que será escrito na lombada do TCC?</h3>
                    <div class="form-check" v-for="(opt, idx) in textoLombadaOptions">
                        <input class="form-check-input" type="radio" name="textoLombadaRadio" :id="'textoLombadaRadio' + idx" :value="idx" v-model="textoLombada">
                        <label class="form-check-label" :for="'textoLombadaRadio' + idx">
                            {{ opt }}
                        </label>
                    </div>
                </div>

            </div>
            <div class="modal-footer" v-if="step != 1">
                <div class="col-3 text-left">
                    <a class="text-orange btn" @click='previousStep'>VOLTAR</a>
                </div>
                <div class="col-9 text-right">
                    <button id="btnseguir-nPages" v-if="step != totalSteps" class="btn btn-b3 btn-orange" v-on:click="nextStep">
                        Prosseguir
                    </button>
                    <button v-if="step == totalSteps" class="btn btn-b3 btn-orange" v-on:click="setBinding()">
                        Finalizar Edição
                    </button>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade text-area-modal" id="modalObservacoes" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered " role="document">
        <div class="modal-content ">
            <div class="modal-header">
                <h3 class="modal-title default-text text-orange font-weight-lighter" id="exampleModalLongTitle">
                    <Strong>Adicionar Observação</strong>
                </h3>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body  pt-0">
                <div class="form-group m-0" v-if="!isSaved">
                    <textarea class="form-control " rows="5" id="clientOrientation" name="clientOrientation" placeholder="Campo destinado para observações" v-model="placeholder"></textarea>
                    <div class="float-right">
                        <button type="button" class="btn btn-b3 btn-b6-xs border-radius-lg btn btn-orange border border-solid" v-on:click="isSaved = true; observation = placeholder">SALVAR</button>
                    </div>
                </div>
                <div class="form-group m-0" v-else>
                    <p class="observation-text">{{ observation }}</p>
                    <div class="float-right">
                        <button type="button" class="btn btn-b3 btn-b6-xs border-radius-lg btn btn-orange border border-solid" v-on:click="isSaved = false">EDITAR</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" tabindex="-1" id="incompleteMsgApp" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h3 class="modal-title default-text text-orange">Atenção</h3>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <p class="text-grey">{{msg}}</p>
            </div>
            <div class="modal-footer">
                <button data-dismiss="modal" class="btn btn-success btn-b3">OK</button>
            </div>
        </div>
    </div>
</div>

<div class="modal" tabindex="-1" id="spinnerModal" role="dialog">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="spinnerDiv mx-auto">
            <div class="spinner">
                <i class="fa fa-spinner fa-pulse fa-3x fa-fw text-white mb-2 mt-sm-2"></i>
            </div>
            <p class="text-white default-title font-weight-bold mt-3 mb-sm-1 mt-sm-1">Aplicando desconto...</p>
        </div>
    </div>
</div>

<div class="modal left-modal" id="modalDesconto" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered my-0" role="document">
        <div class="modal-content my-0 bg-orange">
            <div class="modal-header">
                <button type="button text-white" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body  text-white d-flex justify-content-center align-items-center flex-column">
                <p class="default-title mx-2 mb-3 mb-md-0">
                    <b> TÁ CARO?! </b>

                    <br>
                    <br>

                    Insira seu e-mail ao lado e aplique o desconto de <b> 10% </b> na hora!
                </p>

                <form id="discountModalForm" class="discount-modal-form d-flex align-items-center" v-on:submit.prevent="applyDiscount" novalidate>
                    <div class="w-100">

                        <div id="emailDescontoFormGroup" class="form-group">
                            <label for="emailDesconto" class="default-title">Insira seu email e ganhe até 10% de desconto</label>
                            <input type="email" class="form-control" id="emailDesconto" name="emailDesconto" placeholder="Insira seu e-mail aqui..." v-model="email" required>
                            <div class="invalid-feedback">
                                Insira seu e-mail para liberar o desconto
                            </div>
                        </div>

                        <button type="button" class="btn btn-primary btn-discount d-block m-auto" @click="applyDiscount()" v-if="!loading">
                            APLICAR DESCONTO DE 10%
                        </button>
                        <div class="discount-modal-loading" v-if="loading">
                            <i class="fa fa-spinner fa-pulse"></i>
                        </div>

                    </div>
                </form>

                <div class="arrow-right position-absolute"></div>

            </div>
        </div>
    </div>
</div>

<div class="modal fade delayed-freights-modal" id="delayedFreightsModal" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h3 class="modal-title default-text anexoModalLabel">Aviso Importante</h3>
            </div>
            <div class="modal-body">

                <p class="modal-text">
                    Nossa integração com a transportadora está lenta hoje, por isso ainda não conseguimos calcular os prazos de entrega.
                    Preencha abaixo o seu e-mail que nós entraremos em contato com você e fecharemos seu pedido por lá.
                </p>

                <form id="delayedFreightsForm" class="delayed-freights-form form-inline" v-on:submit.prevent="sendEmail" novalidate>

                    <div class="form-group">
                        <div class="input-group">
                            <input type="email" class="form-control" id="delayedFreightEmail" placeholder="Insira seu melhor e-mail" v-model="email">
                            <div class="input-group-append">
                                <button type="button" class="btn btn-b3 btn-orange border-radius-md" @click="sendEmail()">ENVIAR</button>
                            </div>
                        </div>
                        <div class="invalid-feedback">
                            Insira seu e-mail
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<div class="modal fade finishing-info-modal" id="finishingInfoModal" tabindex="-1" role="dialog" aria-hidden="true" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header pb-3" style="border-radius: 1px;">
                <h2 class="modal-title text-white">Aviso</h2>
                <button type="button" class="close text-white" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <p class="text-left">Aqui você informa a quantidade de Encadernações que seu pedido terá.</p>
                <p class="text-left">EX: irei anexar 3 PDFs, cada um será uma apostila diferente, então terei que escolher 3 encadernações agora</p>
            </div>
        </div>
    </div>
</div>
