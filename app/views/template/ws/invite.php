<section id="breadcrumb">
    <div class="container">
        <div class="row">
            <nav aria-label="breadcrumb" style="font-size: 14px;">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item">
                        <a href="#">Home</a>
                    </li>
                    <li class="breadcrumb-item active" aria-current="page">Indique a DeliveryPrint</li>
                </ol>
            </nav>
        </div>
    </div>
</section>

<section class="container">
    <div id="invitationApp">
        <h2>CAMPANHA DE DESCONTOS</h2>

        <div class="invite-main d-flex flex-column text-center mx-auto">
            <h1 class="text-orange">INDIQUE A DELIVERYPRINT!</h1>

            <h3>Indicando você acumula descontos para as próximas compras. Quem
                utilizar a indicação leva R$15 de desconto em um primeiro pedido.</h3>

            <div v-if="logged" class="logged-div">
                <p class="text-orange mb-3">Compartilhe o link abaixo em suas redes sociais:</p>
                <div class="input-group-container d-block mx-auto">
                    <div class="input-group">
                        <input id="inviteUrlInput" type="text" class="form-control" v-model="inviteUrl">
                        <div class="input-group-append">
                            <button class="btn btn-primary" type="button" @click="copyUrl">COPIAR LINK</button>
                        </div>
                    </div>
                </div>

                <social-sharing :url="'deliveryprint.com.br'" title="Imprima na DeliveryPrint!" quote="Imprima na DeliveryPrint!" description="Impressão online e barata, com entrega!" hashtags="deliveryprint" inline-template>
                    <div>
                        <network network="facebook">
                            <button class="btn btn-b3 btn-orange">Compartilhar no Facebook</button>
                        </network>
                    </div>
                </social-sharing>

            </div>
            <div v-else class="not-logged-div">
                <button class="btn btn-b3 btn-orange" @click="tryToLogin">Faça login para continuar</button>
            </div>
        </div>

        <div class="row">
            <div class="col-sm-6">
                <h4 class="text-orange">COMO FUNCIONA O PROGRAMA?</h4>

                <p class="mb-3">Você usará o link informado a cima para
                    <strong>compartilhar em suas redes sociais</strong> (facebook,
                    instagram, linkedin...) ou até mesmo por whatsapp,
                    email, como você achar que é a melhor forma de
                    <strong>mostrar para seus contatos nossos serviços</strong> de
                    impressão com delivery.</p>

                <p class="mb-3">Quem clicar nesse link será direcionado(a) para
                    nosso site, na página onde irá fazer o orçamento de
                    impressão <strong>com R$15 de desconto</strong> no primeiro
                    pedido.</p>

                <p class="mb-3">Assim que a pessoa concluir o 1o pedido utilizando
                    seu link, <strong>você ganhará R$10 de desconto</strong> em seu
                    próximo pedido. E sim, é <strong>sem limite</strong>, quanto mais
                    pessoas usarem o seu link mais compras de
                    <strong>impressões com descontos você terá</strong>!</p>
            </div>
            <div class="col-sm-6">
                <h4 class="text-orange">QUAIS SÃO AS VANTAGENS?</h4>

                <p class="mb-3"><strong>Você poderá acumular infinitas compras</strong>
                    com desconto de R$10 em nosso site.</p>

                <p class="mb-3">Além disso estará <strong>presenteando</strong> um
                    contato seu com <strong>desconto de R$15</strong> na
                    primeira compra e um novo serviço de
                    <strong>impressão prático, ágil e de confiança</strong>.</p>

                <p class="mb-3">Lembre-se, a <strong>DeliveryPrint</strong> pode imprimir
                    para você, para sua empresa, para seus
                    amigos, filhos, alunos e muito mais. Eles
                    ganham, <strong>você ganha</strong> e nós ganhamos
                    podendo cada dia criar mais soluções
                    para você.</p>
            </div>
        </div>
    </div>
</section>
