<section>
    <div class="container">
        <div class="row">
        <div class="col-12 d-flex justify-content-center">
            <img src="/img/logo.png" alt="logo" class="logo my-5">
        </div>
            <div class="col-12 text-orange text-center">
                <h1 class="err_404">404</h2>
                <p class="default-title font-weight-lighter not_found">Página não encontrada.</p>
            </div>
            <div class="col-12 d-flex justify-content-center mt-5">
            <a href="/impressao-online"><button class="btn btn-b8 btn-b5-xs btn-orange mx-3">VER PREÇOS</button></a>
            <a href="/"><button class="btn btn-b8 btn-b5-xs btn-orange-outlined mx-3 border-lg">IR PARA A HOME</button></a>
            </div>
        </div>
    </div>
</section>
