<div class="container mt-3" id="linkBuilding">
    <div class="row">
        <div class="col-md-12 text-center">
            <h3 class="text-orange">
                VOCÊ TAMBÉM PODE PRECISAR IMPRIMIR
            </h3>
        </div>
    </div>
    <div class="row my-4">
        <div class="col-12 col-md-4">
            <div class="row pb-2 justify-content-center">
                <p class="text-orange text-lg">Opções em Impressão</p>
            </div>
            <div class="row pb-2 text-center justify-content-center">
                <ul class="pl-0">
                    <li>
                        <a href="/impressao-barata">Impressão Barata</a>
                    </li>
                    <li>
                        <a href="/impressao-colorida">Impressão Colorida</a>
                    </li>
                    <li>
                        <a href="/impressao-preto-branco">Impressão em Preto e Branco</a>
                    </li>
                    <li>
                        <a href="/impressao-apostila">Impressão de Apostila</a>
                    </li>
                    <li v-if="op">
                        <a href="/impressao-papel-couche">Impressão em Papel Couché</a>
                    </li>
                    <li v-if="op">
                        <a href="/impressao-a4">Impressão em A4</a>
                    </li>
                    <li v-if="op">
                        <a href="/impressao-a5">Impressão em A5</a>
                    </li>
                    <li v-if="op">
                        <a href="/tcc">Impressão de TCC</a>
                    </li>
                    <li v-if="op">
                        <a href="/monografia">Impressão de Monografia</a>
                    </li>
                    <li v-if="op">
                        <a href="/encadernacao-capa-dura">Encadernação em Capa Dura</a>
                    </li>
                    <li v-if="op">
                        <a href="/encadernacao-espiral">Encadernação Espiral</a>
                    </li>
                    <li v-if="!op">
                        <a class="cursor-pointer" @click="op = true">Ver todos</a>
                    </li>
                    <li v-if="op">
                        <a class="cursor-pointer" @click="op = false">Ver menos</a>
                    </li>
                </ul>
            </div>
        </div>
        <div class="col-12 col-md-4 text-center">
            <div class="row pb-2 justify-content-center">
                <p class="text-orange text-lg">Locais também atendidos</p>
            </div>
            <div class="row pb-2 text-center justify-content-center">
                <ul class="pl-0">
                    <li>
                        <a href="/copiadora-pinheiros">Copiadora Pinheiros</a>
                    </li>
                    <li>
                        <a href="/copiadora-faria-lima">Copiadora Faria Lima</a>
                    </li>
                    <li>
                        <a href="/copiadora-vila-olimpia">Copiadora Vila Olímpia</a>
                    </li>
                    <li>
                        <a href="/copiadora-vila-mariana">Copiadora Vila Mariana</a>
                    </li>
                    <li v-if="lo">
                        <a href="/copiadora-consolacao">Copiadora Consolação</a>
                    </li>
                    <li v-if="lo">
                        <a href="/copiadora-vila-madalena">Copiadora Vila Madalena</a>
                    </li>
                    <li v-if="lo">
                        <a href="/copiadora-itaim-bibi">Copiadora Itaim Bibi</a>
                    </li>
                    <li v-if="lo">
                        <a href="/copiadora-barra-funda">Copiadora Barra Funda</a>
                    </li>
                    <li v-if="lo">
                        <a href="/copiadora-itaquera">Copiadora Itaquera</a>
                    </li>
                    <li v-if="lo">
                        <a href="/copiadora-tatuape">Copiadora Tatuapé</a>
                    </li>
                    <li v-if="lo">
                        <a href="/copiadora-mooca">Copiadora Mooca</a>
                    </li>
                    <li v-if="lo">
                        <a href="/copiadora-online">Copiadora Online</a>
                    </li>
                    <li v-if="!lo">
                        <a class="cursor-pointer" @click="lo = true">Ver todos</a>
                    </li>
                    <li v-if="lo">
                        <a class="cursor-pointer" @click="lo = false">Ver menos</a>
                    </li>
                </ul>
            </div>
        </div>
        <div class="col-12 col-md-4 text-center">
            <div class="row pb-2 justify-content-center">
                <p class="text-orange text-lg">Gráfica para Impressão</p>
            </div>
            <div class="row pb-2 text-center justify-content-center">
                <ul class="pl-0">
                    <li>
                        <a href="/grafica-online">Gráfica Online</a>
                    </li>
                    <li>
                        <a href="/grafica-digital-online">Gráfica Digital</a>
                    </li>
                    <li>
                        <a href="/grafica-rapida">Gráfica Rápida</a>
                    </li>
                    <li>
                        <a href="/grafica-24h">Gráfica 24h</a>
                    </li>
                    <li v-if="gr">
                        <a href="/grafica-pinheiros">Gráfica Pinheiros</a>
                    </li>
                    <li v-if="gr">
                        <a href="/grafica-faria-lima">Gráfica Faria Lima</a>
                    </li>
                    <li v-if="gr">
                        <a href="/grafica-vila-olimpia">Gráfica Vila Olímpia</a>
                    </li>
                    <li v-if="gr">
                        <a href="/grafica-vila-mariana">Gráfica Vila Mariana</a>
                    </li>
                    <li v-if="gr">
                        <a href="/grafica-itaim-bibi">Gráfica Itaim Bibi</a>
                    </li>
                    <li v-if="gr">
                        <a href="/grafica-centro">Gráfica Centro</a>
                    </li>
                    <li v-if="gr">
                        <a href="/grafica-sp">Gráfica Paulista</a>
                    </li>
                    <li v-if="!gr">
                        <a class="cursor-pointer" @click="gr = true">Ver todos</a>
                    </li>
                    <li v-if="gr">
                        <a class="cursor-pointer" @click="gr = false">Ver menos</a>
                    </li>
                </ul>
            </div>
        </div>
        <!-- <div class="col-4 col-md-12 text-center">
            <div class="row">
                <div class="col-12 col-sm-4 pb-2">
                    <p class="text-orange text-lg">Opções em Impressão</p>
                </div>
                <div class="col-12 col-sm-4 pb-2">
                    <p class="text-orange text-lg">Locais também atendidos</p>
                </div>
                <div class="col-12 col-sm-4 pb-2">
                    <p class="text-orange text-lg">Gráfica para Impressão</p>
                </div>
            </div>
        </div>
        <div class="col-8 col-md-12 text-center">
            <div class="row">
                <div class="col-12 col-sm-4 pb-2">
                    <ul>
                        <li>
                            <a href="/impressao-barata">Impressão Barata</a>
                        </li>
                        <li>
                            <a href="/impressao-colorida">Impressão Colorida</a>
                        </li>
                        <li>
                            <a href="/impressao-preto-branco">Impressão em Preto e Branco</a>
                        </li>
                        <li>
                            <a href="/impressao-apostila">Impressão de Apostila</a>
                        </li>
                        <li v-if="op">
                            <a href="/impressao-papel-couche">Impressão em Papel Couché</a>
                        </li>
                        <li v-if="op">
                            <a href="/impressao-a4">Impressão em A4</a>
                        </li>
                        <li v-if="op">
                            <a href="/impressao-a5">Impressão em A5</a>
                        </li>
                        <li v-if="op">
                            <a href="/tcc">Impressão de TCC</a>
                        </li>
                        <li v-if="op">
                            <a href="/monografia">Impressão de Monografia</a>
                        </li>
                        <li v-if="op">
                            <a href="/encadernacao-capa-dura">Encadernação em Capa Dura</a>
                        </li>
                        <li v-if="op">
                            <a href="/encadernacao-espiral">Encadernação Espiral</a>
                        </li>
                        <li v-if="!op">
                            <a class="cursor-pointer" @click="op = true">Ver todos</a>
                        </li>
                        <li v-if="op">
                            <a class="cursor-pointer" @click="op = false">Ver menos</a>
                        </li>
                    </ul>
                </div>
                <div class="col-12 col-sm-4 pb-2">
                    <ul>
                        <li>
                            <a href="/copiadora-pinheiros">Copiadora Pinheiros</a>
                        </li>
                        <li>
                            <a href="/copiadora-faria-lima">Copiadora Faria Lima</a>
                        </li>
                        <li>
                            <a href="/copiadora-vila-olimpia">Copiadora Vila Olímpia</a>
                        </li>
                        <li>
                            <a href="/copiadora-vila-mariana">Copiadora Vila Mariana</a>
                        </li>
                        <li v-if="lo">
                            <a href="/copiadora-consolacao">Copiadora Consolação</a>
                        </li>
                        <li v-if="lo">
                            <a href="/copiadora-vila-madalena">Copiadora Vila Madalena</a>
                        </li>
                        <li v-if="lo">
                            <a href="/copiadora-itaim-bibi">Copiadora Itaim Bibi</a>
                        </li>
                        <li v-if="lo">
                            <a href="/copiadora-barra-funda">Copiadora Barra Funda</a>
                        </li>
                        <li v-if="lo">
                            <a href="/copiadora-itaquera">Copiadora Itaquera</a>
                        </li>
                        <li v-if="lo">
                            <a href="/copiadora-tatuape">Copiadora Tatuapé</a>
                        </li>
                        <li v-if="lo">
                            <a href="/copiadora-mooca">Copiadora Mooca</a>
                        </li>
                        <li v-if="lo">
                            <a href="/copiadora-online">Copiadora Online</a>
                        </li>
                        <li v-if="!lo">
                            <a class="cursor-pointer" @click="lo = true">Ver todos</a>
                        </li>
                        <li v-if="lo">
                            <a class="cursor-pointer" @click="lo = false">Ver menos</a>
                        </li>
                    </ul>
                </div>
                <div class="col-12 col-sm-4 pb-2">
                    <ul>
                        <li>
                            <a href="/grafica-online">Gráfica Online</a>
                        </li>
                        <li>
                            <a href="/grafica-digital-online">Gráfica Digital</a>
                        </li>
                        <li>
                            <a href="/grafica-rapida">Gráfica Rápida</a>
                        </li>
                        <li>
                            <a href="/grafica-24h">Gráfica 24h</a>
                        </li>
                        <li v-if="gr">
                            <a href="/grafica-pinheiros">Gráfica Pinheiros</a>
                        </li>
                        <li v-if="gr">
                            <a href="/grafica-faria-lima">Gráfica Faria Lima</a>
                        </li>
                        <li v-if="gr">
                            <a href="/grafica-vila-olimpia">Gráfica Vila Olímpia</a>
                        </li>
                        <li v-if="gr">
                            <a href="/grafica-vila-mariana">Gráfica Vila Mariana</a>
                        </li>
                        <li v-if="gr">
                            <a href="/grafica-berrini">Gráfica Berrini</a>
                        </li>
                        <li v-if="gr">
                            <a href="/grafica-itaim-bibi">Gráfica Itaim Bibi</a>
                        </li>
                        <li v-if="gr">
                            <a href="/grafica-centro">Gráfica Centro</a>
                        </li>
                        <li v-if="gr">
                            <a href="/grafica-sp">Gráfica Paulista</a>
                        </li>
                        <li v-if="!gr">
                            <a class="cursor-pointer" @click="gr = true">Ver todos</a>
                        </li>
                        <li v-if="gr">
                            <a class="cursor-pointer" @click="gr = false">Ver menos</a>
                        </li>
                    </ul>
                </div>
            </div>
        </div> -->
    </div>
</div>
