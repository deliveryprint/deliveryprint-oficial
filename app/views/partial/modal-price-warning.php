<div class="modal fade" id="priceWarningModal" tabindex="-1" role="dialog" aria-hidden="true" data-backdrop="static"
    data-keyboard="false">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header bg-orange pb-3" style="border-radius: 1px;">
                <h2 class="modal-title text-white">Aviso</h2>
                <button type="button" class="close text-white" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <p class="text-orange text-left">O valor apresentado na página é um exemplo de impressão com alto volume de páginas impressas. Nosso menor valor é na impressão em preto e branco no sulfite A4 que é de R$0,11/página e ele é alcançado em pedidos superiores a 10.000 páginas. Sendo um número de páginas menor que esse os valores podem variar, por exemplo, de R$0,11 até R$0,35/página na impressão em PB no papel sulfite A4 75g.</p><br>
                <p class="text-orange text-left">Nossos valores variam de acordo com a quantidade de páginas a serem impressas.</p><br>
                <p class="text-orange text-left">Ou seja, quanto maior for o número de páginas menor o preço unitário dela fica, isso se aplica para todas opções de papel e de cor.</p>
            </div>
        </div>
    </div>
</div>
