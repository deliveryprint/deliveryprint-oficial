<footer class="bg-lightgrey font-weight-lighter pb-5 mb-5">
    <div class="row justify-content-center">
        <div class="contato col-sm-3 col-xl-3 mr-sm-2">
            <div class="ft-info mt-4">
                <div class="pedido float-left">
                    <span class="default-text text-orange text-sm"><b></b>
                        <br>
                    </span>
                    <span class="default-text text-orange text-md">
                        <b>
                            <i class="fa fa-phone"></i> (11)3807-7562</b>
                        <br />
                    </span>
                    <span class="default-text text-orange text-md">
                        <b>
                            <i class="fa fa-whatsapp"></i> (11) 99281-2284</b>
                        <br />
                    </span>
                    <span class="default-text text-sm text-orange text-md">
                        <b>
                         suporte@deliveryprint.com.br</b>
                        <br />
                    </span>
                </div>
                <div class="ft-social social float-right">
                    <b>
                        <p class="default-text text-orange text-sm">Siga-nos</p>
                    </b>
                    <a href="https://www.facebook.com/deliveryprintapp/" target="_blank">
                        <b><i class="fa fa-facebook-square text-orange text-hg"></i></b>
                    </a>
                    <a href="https://www.instagram.com/deliveryprintapp/" target="_blank">
                        <b><i class="fa fa-instagram text-orange text-hg"></i></b>
                    </a>
                </div>
                <div class="float-right mt-2 d-block d-md-none text-right">
                    <a href="/" class="default-text text-sm text-orange">Página Inicial</a>
                    <br>
                    <a href="/produtos" class="default-text text-sm text-orange">Serviços</a>
                    <br>
                    <a href="/mapa-do-site" class="default-text text-sm text-orange">Mapa do Site</a>
                </div>
            </div>
        </div>
    </div>
</footer>
