<!-- BARRA VERMELHA -->

<h3 class="text-center text-orange mb-4 mt-0 hide-m h2">Processo Simples e Online</h3>

<div id="how_it_works" class="bg-smoother-og text-white text-center mt-1">
    <div class="row font-weight-lighter mr-0">
        <div class="col-12 col-md-6 mk bg-orange"></div>
        <div class="col-md-6 col-12 mk bg-smoother-orange  clip chevron b"></div>
        <div class="col-md-1 offset-half d-none d-md-block"></div>

        <div class="col-md-3 col-12 bg-orange">
            <div class="col-12 col-hg-6 py-5" id="pos_left">
                <i class="fa fa-sliders fa-5x  mt-5 mt-md-0"></i>
                <p class="h3">Configuração</p>
                <p class="px-2 mx-auto center">
                    Escolha a cor de sua impressão, papel que deseja e se precisa de encadernação
                </p>
            </div>
        </div>

        <div class="col-md-4 col-lg-3 py-md-5 col-12 bg-smooth-og clip chevron b center">
            <i class="fa fa-cloud-upload fa-5x"></i>
            <p class="h3 text-center">Upload</p>
            <p class="px-2 mx-auto center">
                Faça upload dos arquivos a serem impressos ou informe o número de páginas.
            </p>

            <a id="btn-hiw-primary" href="/impressao-online" class="btn btn-b3 btn-primary mt-4 d-none d-md-inline-block">FAZER ORÇAMENTO</a>
        </div>

        <div class="col-12 col-md-2 py-5 bg-smoother-og">
            <div class="col-12 col-hg-6" id="pos_right">
                <i class="fa fa-truck fa-5x fa-flip-horizontal"></i>
                <p class="h3">Receba em Casa</p>
                <p class="px-2 pb-3 mx-auto center">
                    Pronto! Agora é só esperar chegar no endereço que você escolheu ;)
                </p>
            </div>
        </div>
        <div class="col-md-1 d-none d-md-block"></div>
    </div>
</div>
