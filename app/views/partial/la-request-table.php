<thead>
    <tr class="text-center">
        <th class="sticky-header"></th>
        <th class="sticky-header">ID</th>
        <th class="sticky-header">Nº de Páginas</th>
        <th class="sticky-header">Tipo de Entrega</th>
        <th class="green sticky-header">Iniciar Até</th>
        <th class="red sticky-header">Finalizar Até</th>
        <th class="egg sticky-header">Despachar Até</th>
        <th class="sticky-header">Status Pgto</th>
        <th class="sticky-header">Status Item</th>
        <th class="sticky-header">Data Pedido</th>
        <th class="sticky-header">Data Entrega</th>
        <th class="sticky-header">Observações</th>
    </tr>
</thead>
<tbody class="text-center">
    <template v-for="(item, idx) in items">
        <tr v-bind:class='{
            "bg-em-producao": item.statusItem == "Em Produção",
            "bg-acabamento": item.statusItem == "Acabamento",
            "bg-falta-despachar": item.statusItem == "Falta Despachar"
        }'>
            <td>
                <label class="checkbox-btn check-success float-right">
                    <input v-on:click="toggleLine(item.idItem)" type="checkbox" class="table-check" v-bind:checked="item.idItem in expanded">
                    <span class="checkmark toggle"></span>
                </label>
            </td>
            <td>{{ item.pretty_id }}</td>
            <td>{{ item.num_folhas }}</td>
            <td>{{ (item.delivery.tbRetirada_idRetirada ? ('Retirada ' + item.delivery.tipo_retirada) : ('Entrega ' + item.delivery.tipo_frete))
                | filterDelivery }}
            </td>
            <td>{{ item.delivery.start_date | datetimeFormatter }}</td>
            <td>{{ item.delivery.finish_date | datetimeFormatter }}</td>
            <td>{{ item.delivery.dispatch_date | datetimeFormatter }}</td>
            <td>{{ item.statusPagamento }}</td>
            <td>
                <span class="label" v-bind:class='getCorrectClass(item.statusItem)'>{{ item.statusItem }}</span>
            </td>
            <td>{{ item.dataPedido | dateFormatter }}</td>
            <td>{{ (item.delivery.tbRetirada_idRetirada ? item.delivery.prazo_retirada : item.delivery.prazo_frete) | dateFormatter
                }}
            </td>
            <td class="text-left">
                <span class="addNote" data-toggle="modal" data-target="#modalObservacoes" v-on:click="changeObs(idx)">
                    <i class="fa fa-pencil-square-o text-hg margin-xs-right" aria-hidden="true"></i>
                    <span class="font-weight-normal">{{ item.obsAdmin ? item.obsAdmin : 'Adicionar nota' }}</span>
                </span>
            </td>
        </tr>
        <?php require 'app/views/partial/la-expanded-row.php'; ?>
    </template>
</tbody>
