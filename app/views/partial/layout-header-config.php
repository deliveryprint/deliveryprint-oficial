<title><?php echo $this->title; ?></title>
<meta name="viewport" content="width=device-width, initial-scale=1">

<meta name="keywords" content="" />
<meta name="description" content="<?php echo $this->metaDescription; ?>" />
<meta name="author" content="">
<meta name="robots" content="<?php echo $this->metaRobots; ?>"/>

<!-- Facebook -->
<meta prefix="og: http://ogp.me/ns#" property="og:title" content="DeliveryPrint" />
<meta prefix="og: http://ogp.me/ns#" property="og:image:width" content="450" />
<meta prefix="og: http://ogp.me/ns#" property="og:image:height" content="300" />
<meta prefix="og: http://ogp.me/ns#" property="og:image"
    content="<?php echo \IntecPhp\Model\Config::getDomain('/img/logo.png');?>"/>
<meta prefix="og: http://ogp.me/ns#" property="og:url" content="<?php echo $this->metaDescription; ?>" />
<meta prefix="og: http://ogp.me/ns#" property="og:site_name" content="" />
<meta prefix="og: http://ogp.me/ns#" property="og:description" content="" />
<meta prefix="og: http://ogp.me/ns#" property="og:type" content="website" />

<!-- Twitter -->
<meta name="twitter:card" content="">
<meta name="twitter:url" content="<?php echo \IntecPhp\Model\Config::getDomain(); ?>">
<meta name="twitter:title" content="DeliveryPrint">
<meta name="twitter:description" content="<?php echo $this->metaDescription; ?>">
<meta name="twitter:image" content="<?php echo \IntecPhp\Model\Config::getDomain('/img/logo.png');?>">

<link rel="apple-touch-icon" sizes="180x180" href="/apple-touch-icon.png">
<link rel="icon" type="image/png" sizes="32x32" href="/favicon-32x32.png">
<link rel="icon" type="image/png" sizes="16x16" href="/favicon-16x16.png">
<link rel="icon" type="image/png" sizes="16x16" href="/img/favicon.png">
<link rel="manifest" href="/manifest.json">

<meta name="theme-color" content="#ffffff">
<link rel="canonical" href="https://<?php echo \IntecPhp\Model\Config::getDomain($this->canonical) ?>" />

<!-- Link AMP -->
<?php if (isset($this->amp)): ?>
    <link rel="amphtml" href="https://<?php echo \IntecPhp\Model\Config::getDomain($this->amp) ?>" />
<?php endif;
?>
