<div id="sidenavApp">
    <div class="sidenav bg-light" :class="{ active: active }">
        <ul class="nav flex-column" id="main-m">
            <li class="nav-item">
                <a class="nav-link" href="/meus-pedidos">
                    <i class="fa fa-shopping-basket" style="font-size: 14px"></i>
                    Meus Pedidos
                </a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="/dados-cadastrais">
                    <i class="fa fa-user"></i>
                    Meus Dados
                </a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="/indicacoes">
                    <i class="fa fa-user"></i>
                    Indicações
                </a>
            </li>
            <li class="nav-item">
                <a id="btn-sair-m" class="nav-link" href="#">
                    <i class="fa fa-sign-out"></i>
                    Sair
                </a>
            </li>
        </ul>

        <button id="btn-acessar-m" type="button" class="btn btn-b6 btn-orange-outlined border-md" data-toggle="modal" data-target="#modalAcesso">Entrar</button>

        <hr>

        <ul class="nav flex-column">
            <li class="nav-item">
                <a class="nav-link" href="/impressao-online">Fazer Pedido</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="/pedido-empresas">Pedido Corporativo</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="/como-funciona">Como Funciona?</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="/produtos">Serviços</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="https://blog.deliveryprint.com.br/">Blog</a>
            </li>
            <!-- <li class="nav-item">
                <a class="nav-link" href="https://blog.deliveryprint.com.br/um-pouco-sobre-nos/">Quem é a DeliveryPrint?</a>
            </li> -->
            <li class="nav-item">
                <a class="nav-link" href="/ajuda/faq">Dúvidas Frequentes</a>
            </li>
        </ul>
        <hr>
        <p>suporte@deliveryprint.com.br</p>
        <p><i class="fa fa-phone mr-2"></i> (11) 3807-7562</p>
        <p><i class="fa fa-whatsapp mr-2"></i> (11) 99281-2284</p>
    </div>
    <div class="sidenav-mask" :class="{ active: active }" @click="toggleSidenav"></div>
</div>


<header>
    <nav class="navbar navbar-expand-lg navbar-light bg-light">
        <div class="container-fluid">

            <!-- Side menu toggle button -->
            <button class="navbar-toggler" type="button">
                <i class="fa fa-bars"></i>
            </button>

            <!-- Company logo -->
            <a class="navbar-brand" href="/">
                <!-- <img class="d-none d-xl-block" src="/img/logo.png" width="240" height="75" alt="logo"> -->
                <img class="d-none d-lg-block" src="/img/logo.png" width="180" height="56" alt="logo">
                <img class="d-lg-none" src="/img/logo.png" width="98" alt="logo">
            </a>

            <!-- Main navigation bar -->
            <ul class="navbar-nav d-none d-lg-flex">
                <li class="nav-item">
                    <a class="nav-link" href="/impressao-online">Fazer Pedido</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="/pedido-empresas">Pedido Corporativo</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="/como-funciona">Como Funciona?</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link d-none d-xl-block" href="/produtos">Serviços</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link d-none d-xl-block" href="https://blog.deliveryprint.com.br/">Blog</a>
                </li>
                <li class="nav-item dropdown">
                    <a href="#" id="contactDropdownLink" class="nav-link dropdown-toggle" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Contato</a>
                    <div class="dropdown-menu" aria-labelledby="contactDropdownLink">
                        <!-- <a class="dropdown-item" href="https://blog.deliveryprint.com.br/um-pouco-sobre-nos/">Quem é a DeliveryPrint?</a> -->
                        <a class="dropdown-item" href="/ajuda/faq">Dúvidas Frequentes</a>
                        <div class="dropdown-divider"></div>
                        <span class="dropdown-item-text">suporte@deliveryprint.com.br</span>
                        <span class="dropdown-item-text"><i class="fa fa-phone mr-2"></i> (11) 3807-7562</span>
                        <span class="dropdown-item-text"><i class="fa fa-whatsapp mr-2"></i> (11) 99281-2284</span>
                    </div>
                </li>
            </ul>

            <div class="d-flex flex-row">
                <!-- Login and My Account buttons -->
                <div id="loginButtonContainer" class="d-none d-lg-block mr-4">
                    <div class="dropdown d-none d-lg-block">
                        <button id="main" class="btn btn-b6 text-orange font-weight-bold bg-transparent dropdown-toggle" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            MINHA CONTA
                        </button>
                        <div class="dropdown-menu border-radius-sm shadow border-0" aria-labelledby="main">
                            <a class="dropdown-item font-weight-lighter text-orange" href="/meus-pedidos">
                                <i class="fa fa-shopping-basket"></i>Meus Pedidos</a>
                            <a class="dropdown-item font-weight-lighter text-orange" href="/dados-cadastrais">
                                <i class="fa fa-user"></i> Meus Dados</a>
                            <a class="dropdown-item font-weight-lighter text-orange" href="/indicacoes">
                                <i class="fa fa-ticket"></i> Indicacoes</a>
                            <a class="dropdown-item font-weight-lighter text-orange" href="#" id="btn-sair">
                                <i class="fa fa-sign-out"></i>Sair</a>
                        </div>
                    </div>
                    <button id="btn-acessar" type="button" class="btn btn-b6 btn-orange-outlined border-md" data-toggle="modal" data-target="#modalAcesso">Entrar</button>
                </div>

                <!-- Whatsapp button -->
                <a id="whatsapp-chat" class="whatsapp-chat-btn d-lg-none" href="https://api.whatsapp.com/send?phone=5511992812284&text=Fale%20agora%20com%20nosso%20suporte!">
                    <i class="fa fa-whatsapp"></i>
                </a>

                <!-- Shopping cart button -->
                <a id="cart-header" class="cart-btn" href="/carrinho">
                    <i class="fa fa-shopping-cart"></i>
                    <span id="cartInfo">(0)</span>
                </a>
            </div>
        </div>
    </nav>
</header>

<?php $this->appendPartial('modal-acesso'); ?>
