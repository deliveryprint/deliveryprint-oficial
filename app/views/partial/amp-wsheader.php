<header>
    <div class="menu row shadow-sm no-gutters">
        <div class="container">
            <div class="d-flex row justify-content-end mt-2" id="contacts">
                <span class="text-sm text-orange">
                    <i class="fa fa-whatsapp mr-2"></i>(11) 99281-2284 </span>
                <span class="text-sm text-orange mx-2"> | </span>
                <span class="text-sm text-orange">
                    <i class="fa fa-phone mr-2"></i>(11) 3807-7562</span>
            </div>
            <div class="navbar-header row align-items-center justify-content-center">
                <div class="p-0">
                    <a class="navbar-brand animate logo py-3" href="/">
                        <amp-img alt="logo" src="/img/logo.png" width="98" height="35"> </amp-img>
                    </a>
                </div>
            </div>
        </div>
    </div>
</header>
