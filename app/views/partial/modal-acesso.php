<div class="modal fade" id="modalAcesso" tabindex="-1" role="dialog" aria-labelledby="modalAcessoTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content border-0 border-radius-sm">
            <div class="modal-header border-0">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div id="acessoModalLoading" class="d-none">
                    <div class="page-loader-single mb-3">
                        <div class="sk-fading-circle">
                            <div class="sk-circle1 sk-circle"></div>
                            <div class="sk-circle2 sk-circle"></div>
                            <div class="sk-circle3 sk-circle"></div>
                            <div class="sk-circle4 sk-circle"></div>
                            <div class="sk-circle5 sk-circle"></div>
                            <div class="sk-circle6 sk-circle"></div>
                            <div class="sk-circle7 sk-circle"></div>
                            <div class="sk-circle8 sk-circle"></div>
                            <div class="sk-circle9 sk-circle"></div>
                            <div class="sk-circle10 sk-circle"></div>
                            <div class="sk-circle11 sk-circle"></div>
                            <div class="sk-circle12 sk-circle"></div>
                        </div>
                    </div>
                    <p class="text-orange mb-5">Por favor, aguarde...</p>
                </div>

                <div id="loginForm">
                    <div class="tenho" role="tabpanel">
                        <div class="row row-title container col-sm-12">
                            <p class="titulo-modal-acesso default-text h3">Criar Acesso / Logar</p>
                            <div class="alert col-sm-12  alert-danger alertcad" style="display: none">
                                <strong>Atenção!</strong> Verifique se todas as informações estão preenchidas e tente novamente.
                            </div>
                        </div>
                        <form class="form-horizontal intec-ajax-form" id="createAccountForm" method="POST" action="/criar-conta">
                            <div class="form-group row">
                                <div class="col-12">
                                    <input id="emailcad" class="form-control input-focus" type="email" required name="email" placeholder="E-mail">
                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="col-12">
                                    <input id="senhacad" class="form-control input-focus" type="password" required name="password" placeholder="Criar senha">
                                </div>
                            </div>
                            <button type="button" id="recoverPass" class="btn btn-link p-0 font-weight-bold default-text">Esqueci minha senha</button><br>
                            <button id="cadastrar" type="submit" class="btn btn-b3 btn-orange border-radius default-title" value="Prosseguir"> PROSSEGUIR </button>
                        </form>
                    </div>
                    <div class="recuperar-senha" style="display:none">
                        <div class="row row-title container col-sm-12">
                            <p class="titulo-modal-acesso default-text h3">Recuperar Senha</p>
                            <div class="alert col-sm-12  alert-danger alertcad" style="display: none">
                                <strong>Atenção!</strong> Verifique se todas as informações estão preenchidas e tente novamente.
                            </div>
                        </div>

                        <form class="form-horizontal intec-ajax-form" id="recoverForm" method="post" action="/requestRecovery">
                            <div class="form-group row">
                                <div class="col-12">
                                    <input class="form-control input-focus" type="email" required name="email" value="" placeholder="E-mail">
                                </div>
                            </div>
                            <button type="button" class="btn btn-link p-0 font-weight-bold default-text ntenho"><span class="text-acesso font-weight-normal">Não possui acesso? </span>Criar Conta</button><br>
                            <button id="enviEmail" type="submit" class="btn btn-b3 btn-orange border-radius default-title" value="SALVAR" style="margin-top: 25px"> ENVIAR EMAIL </button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
