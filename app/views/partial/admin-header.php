<header>
    <div class="container">
        <div class="navbar navbar-smak navbar-fixed-top default-text text-md text-orange p-0" id="navbar" role="navigation">
            <div class="col-sm-12 p-0">
                <div class="float-left  ">
                    <a class="navbar-brand animate float-left" href="/"><img class="logo" src="/img/logo.png" alt="logo"></a>
                </div>
                <div class=" info float-right margin-1-right">
                <button id="btn-sair-adm" class="btn btn-b6 border-radius-md btn btn-orange-outlined border-md border-solid">
                    <i class="fa fa-sign-out"></i> SAIR
                </button>
                </div>
            </div>
        </div>
    </div>
</header>
