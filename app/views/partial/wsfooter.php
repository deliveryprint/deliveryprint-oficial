<footer class="bg-lightgrey font-weight-lighter">
    <div class="row justify-content-center">
        <div class="contato col-sm-3 col-xl-3 mr-sm-2">
            <p class="h3 default-title text-orange hidden-xs">Contatos</p>
            <p class="default-text text-grey text-sm hidden-xs">Entre em contato conosco através dos dados abaixo<br> ou por nossas Redes Sociais.
            </p>
            <div class="ft-info mt-4">
                <div class="pedido float-left">
                    <!--<span class="default-text text-orange text-sm">Pedidos também por-->
                    <!--    <br>-->
                    <!--</span>-->
                    <span class="default-text text-orange text-md">
                        <b>
                            <i class="fa fa-phone"></i> (11) 3807-7562</b>
                        <br/>
                    </span>
                    <span class="default-text text-orange text-md">
                        <b>
                            <i class="fa fa-whatsapp"></i> (11) 99281-2284</b>
                        <br/>
                    </span>
                    <span class="default-text text-sm text-orange text-md">
                        <b>
                            <i class="fa fa-envelope-o d-none d-sm-inline"></i> suporte@deliveryprint.com.br</b>
                        <br/>
                    </span>
                </div>
                <div class="ft-social social float-right" style="margin-top: 0;">
                    <p class="default-text text-orange text-sm">Siga-nos</p>

                    <a href="https://www.facebook.com/deliveryprintapp/" target="_blank">
                        <i class="fa fa-facebook-square text-hg"></i>
                    </a>
                    <a href="https://twitter.com/deliveryprintbr" target="_blank">
                        <i class="fa fa-twitter text-hg"></i>
                    </a>
                    <a href="https://www.instagram.com/deliveryprintapp/" target="_blank">
                        <i class="fa fa-instagram text-hg"></i>
                    </a>
                    <a href="https://blog.deliveryprint.com.br" class="d-block text-default font-weight-bold">Blog</a>
                </div>
                <div class="float-right mt-2 d-block d-md-none">
                    <b>
                        <a href="/mapa-do-site" class="default-text text-sm text-orange text-md">Mapa do Site</a>
                    </b>
                </div>
            </div>
            <br/>
            <br/>
            <!-- <p class="default-text text-orange text-sm pb-3 hidden-xs">Endereço</p>
            <p class="default-text text-sm text-grey hidden-xs">Rua Oscar Freire 2617, cj 410,
                <br> Pinheiros, CEP 05409-012, São Paulo SP - Brasil.</p>
            <br> -->

        </div>
        <div class="pagamento col-sm-2 ml-5 pl-4 hidden-xs">
            <p class="h3 default-title text-orange">Formas de Pagamento</p>
            <div class="default-text text-sm text-grey row">
                <div class="col-12 col-lg-4">
                    <p class="bandeiras pt-2">Boleto</p>
                    <i class="fa fa-barcode fa-5x"></i>
                </div>
                <div class="bandeiras col-12 col-lg-8">
                    <p class="pb-0">
                        Cartão de crédito
                        <br>
                        (em até 3x a cima de R$100)
                    </p>
                    <i class="fa fa-cc-visa fa-2x"></i>
                    <i class="fa fa-cc-mastercard fa-2x"></i>
                    <br>
                    <i class="fa fa-cc-amex fa-2x"></i>
                    <i class="fa fa-cc-discover fa-2x"></i>
                </div>
            </div>
            <div class="ambiente">
                <p class=" h3 default-title text-orange">Ambiente Seguro</p>
                <img class="iugu" src="/img/iugu.png" alt="iugu">
            </div>
        </div>
        <div class="pagamento col-sm-3 hidden-xs">
            <p class="h3 default-title text-orange">A DeliveryPrint</p>
            <p class="font-weight-lighter text-grey">
                A DeliveryPrint é uma copiadora online com serviço de entrega rápida para todo Brasil.
                Fazemos serviço de impressão em preto e branco e colorido nos mais variados tipos de
                papel que podem ser consultados na página de orçamento. Somos uma Startup que funciona
                desde nov/2016, já atendemos por nosso site mais de 14.000 clientes. Em nosso Facebook
                e conta no Google você pode conferir as avaliações de alguns de nossos clientes. Venha
                imprimir com a gente, não precisa mais ir até uma gráfica!
            </p>
            <br/>
            <!-- <p class="default-text text-sm pb-3 text-grey hidden-xs">
                Copyright © 2018 Plataformas de Tecnologia em Comercio e Serviços,
                <br/> CNPJ 30.070.065/0001-74 | Todos os direitos reservados
                <br/>
                <a href="#" class="text-orange font-weight-bold" data-toggle="modal" data-target="#modalTermos">Termos de Uso</a>
                <a href="/mapa-do-site" class="text-orange font-weight-bold pl-3">Mapa do Site</a>
            </p> -->
            <p class="default-text text-sm pb-3 text-grey hidden-xs">
                Copyright © 2018 - DELIVERYPRINT SERVIÇOS DE IMPRESSÃO
                <br/> CNPJ 05.582.740/0001-05 | Todos os direitos reservados
                <br/>
                <a href="#" class="text-orange font-weight-bold" data-toggle="modal" data-target="#modalTermos">Termos de Uso</a>
                <a href="/mapa-do-site" class="text-orange font-weight-bold pl-3">Mapa do Site</a>
            </p>
        </div>
    </div>
</footer>

<!--Modal Termos de Uso-->
<div class="modal fade" id="modalTermos" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <p class="modal-title default-text text-orange font-weight-lighter h3" id="exampleModalLongTitle">
                    <Strong>Termos de Serviço</strong>
                </p>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body pt-0">
                <?php require "app/views/partial/user-termos.php"; ?>
                <div class="modal-footer p-0">
                    <button type="button" class="btn btn-b3 btn-b6-xs border-radius-lg btn btn-orange border border-solid" data-dismiss="modal">ACEITO OS TERMOS</button>
                </div>
            </div>
        </div>
    </div>
</div>
