<!DOCTYPE html>
<html lang="pt-br">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <?php require_once 'app/views/partial/layout-header-config.php'; ?>
    <link type="text/css" rel="stylesheet" href="/css/app.min.css" />
    <?php foreach ($this->stylesheets as $href) : ?>
        <link href="<?php echo $href; ?>" rel="stylesheet" type="text/css">
    <?php endforeach; ?>
    <link href="https://fonts.googleapis.com/css?family=Roboto+Slab|PT+Sans|Fira+Mono" rel="stylesheet">
    <link href='https://fonts.googleapis.com/css?family=Roboto:100,300,400,500,700,900|Material+Icons' rel="stylesheet">
</head>

<body>

    <nav class="navbar navbar-dark bg-dark fixed-top p-0 shadow">
        <a class="navbar-brand p-3 mr-0" href="/">Deliveryprint Admin</a>
        <button class="navbar-toggler" type="button">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="flex-grow-1">
            <button id="btn-sair-adm" class="btn btn-b6 border-radius-md btn-orange-outlined border-md border-solid float-right mr-3">
                <i class="fa fa-sign-out"></i> SAIR
            </button>
        </div>
    </nav>

    <div class="container-fluid">
        <aside class="main-sidebar shadow">
            <nav class="sidebar">
                <ul class="nav flex-column">
                    <li class="nav-item">
                        <a class="nav-link active" href="/adm/pedidos">
                            <i class="fa fa-print"></i> Pedidos
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link active" href="/adm/cupom">
                            <i class="fa fa-tags"></i> Cupons
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link active" href="/adm/usuarios">
                            <i class="fa fa-users"></i> Usuários
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link active" href="/adm/faixa-precos">
                            <i class="fa fa-dollar"></i> Preços
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link active" href="/adm/relatorios">
                            <i class="fa fa-file"></i> Relatórios
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link active" href="/adm/atualizar-custos">
                            <i class="fa fa-exchange"></i> Custos
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link active" href="/adm/modalemails">
                            <i class="fa fa-envelope"></i> E-mails do Modal
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link active" href="/adm/entregas-rapidas">
                            <i class="fa fa-dollar"></i> Entregas Rápidas
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link active" href="/adm/configuracoes">
                            <i class="fa fa-cog"></i> Configurações
                        </a>
                    </li>
                </ul>
            </nav>
        </aside>

        <main class="content-wrapper">
            <?php
            require_once 'app/views/template/' . $page . '.php';

            foreach ($this->appendPartial as $p) {
                require_once 'app/views/partial/' . $p . '.php';
            }
            ?>
        </main>
    </div>

    <script type='text/javascript' src="/js/app-adm.min.js"></script>
    <?php foreach ($this->scripts as $path) : ?>
        <script type='text/javascript' src="<?php echo $path; ?>"></script>
    <?php endforeach; ?>
</body>

</html>
