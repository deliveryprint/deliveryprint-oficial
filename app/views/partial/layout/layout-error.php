<!DOCTYPE html>
<html lang="pt-br">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <?php require_once 'app/views/partial/layout-header-config.php'; ?>
    <link type="text/css" rel="stylesheet" href="/css/app.min.css" />
    <?php foreach ($this->stylesheets as $href): ?>
    <link href="<?php echo $href; ?>" rel="stylesheet" type="text/css">
    <?php endforeach; ?>

    <?php if(\IntecPhp\Model\Config::getDomain() == "deliveryprint.com.br") : ?>
        <!-- Hotjar Tracking Code for http://deliveryprint.com.br/ -->
        <script>
            (function(h, o, t, j, a, r) {
                h.hj = h.hj || function() {
                    (h.hj.q = h.hj.q || []).push(arguments)
                };
                h._hjSettings = {
                    hjid: 937579,
                    hjsv: 6
                };
                a = o.getElementsByTagName('head')[0];
                r = o.createElement('script');
                r.async = 1;
                r.src = t + h._hjSettings.hjid + j + h._hjSettings.hjsv;
                a.appendChild(r);
            })(window, document, 'https://static.hotjar.com/c/hotjar-', '.js?sv=');
        </script>
    <?php endif; ?>

    <link rel="apple-touch-icon" sizes="180x180" href="/apple-touch-icon.png">
    <link rel="icon" type="image/png" sizes="32x32" href="/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="16x16" href="/favicon-16x16.png">
    <link rel="manifest" href="/site.webmanifest">
    <link rel="mask-icon" href="/safari-pinned-tab.svg" color="#5bbad5">
    <meta name="msapplication-TileColor" content="#da532c">
    <meta name="theme-color" content="#ffffff">
</head>

<body>
    <div id="wrapper">

        <div id="page-container-wrapper">
            <?php require_once 'app/views/template/' . $page . '.php'; ?>
            <div id="page-content-wrapper-backdrop"></div>
        </div>

        <script type='text/javascript' src="/js/app.min.js"></script>
        <?php foreach ($this->scripts as $path): ?>
                <script type='text/javascript' src="<?php echo $path; ?>"></script>
            <?php endforeach; ?>
        <link href="https://fonts.googleapis.com/css?family=Open+Sans:300" rel="stylesheet">
    <?php if(\IntecPhp\Model\Config::getDomain() == "deliveryprint.com.br") : ?>
        <script async src="https://www.googletagmanager.com/gtag/js?id=UA-85986051-1"></script> <script> window.dataLayer = window.dataLayer || []; function gtag(){dataLayer.push(arguments);} gtag('js', new Date()); gtag('config', 'UA-85986051-1'); </script>
    <?php endif; ?>
</body>

</html>
