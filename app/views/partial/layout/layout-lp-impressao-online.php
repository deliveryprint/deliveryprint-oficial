<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title><?php echo $this->title; ?></title>

    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Roboto+Slab|PT+Sans|Fira+Mono" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Economica" rel="stylesheet">

    <!-- Font Awesome -->
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.10/css/all.css" integrity="sha384-+d0P83n9kaQMCwj8F4RJB66tzIwOKmrdb46+porD/OvrJ+37WqIM7UoBtwHO6Nlg" crossorigin="anonymous">

    <!-- Folhas de estilo da página -->
    <?php foreach ($this->stylesheets as $href) : ?>
        <link href="<?php echo $href; ?>" rel="stylesheet" type="text/css">
    <?php endforeach; ?>

    <!-- dataLayer object for Google Tag Manager -->
    <script>
        window.dataLayer = [];
    </script>

    <!-- Google Tag Manager -->
    <script>
        (function(w, d, s, l, i) {
            w[l] = w[l] || [];
            w[l].push({
                'gtm.start': new Date().getTime(),
                event: 'gtm.js'
            });
            var f = d.getElementsByTagName(s)[0],
                j = d.createElement(s),
                dl = l != 'dataLayer' ? '&l=' + l : '';
            j.async = true;
            j.src =
                'https://www.googletagmanager.com/gtm.js?id=' + i + dl;
            f.parentNode.insertBefore(j, f);
        })(window, document, 'script', 'dataLayer', 'GTM-57DWK9H');
    </script>
    <!-- End Google Tag Manager -->

    <!-- Funções da página -->
    <script>
        // Snippet de código para disparar o evento personalizado do Google Tag Manager de click no botão do Whatsapp
        function gtag_report_conversion(url) {
            window.dataLayer.push({
                'event': 'whatsappButtonClicked'
            });
        }

        function sendWhatsAppMessage() {
            fbq('track', 'Contact');
            window.location.href = "https://api.whatsapp.com/send?phone=5511992812284&text=Fale%20agora%20com%20nosso%20suporte!";
        }
    </script>
</head>

<body>

    <?php
    require_once 'app/views/template/' . $page . '.php';

    foreach ($this->appendPartial as $p) {
        require_once 'app/views/partial/' . $p . '.php';
    }
    ?>

</body>

</html>
