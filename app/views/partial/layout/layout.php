<!DOCTYPE html>
<html lang="pt-br" class="no-js">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <?php require_once 'app/views/partial/layout-header-config.php'; ?>
    <link type="text/css" rel="stylesheet" href="/css/app.min.css" />

    <?php foreach ($this->stylesheets as $href) : ?>
        <link href="<?php echo $href; ?>" rel="stylesheet" type="text/css">
    <?php endforeach; ?>

    <?php if (\IntecPhp\Model\Config::getDomain() == "deliveryprint.com.br") : ?>
        <!-- dataLayer object for Google Tag Manager -->
        <script>
            window.dataLayer = [];
        </script>

        <!-- Google Tag Manager -->
        <script>
            (function(w, d, s, l, i) {
                w[l] = w[l] || [];
                w[l].push({
                    'gtm.start': new Date().getTime(),
                    event: 'gtm.js'
                });
                var f = d.getElementsByTagName(s)[0],
                    j = d.createElement(s),
                    dl = l != 'dataLayer' ? '&l=' + l : '';
                j.async = true;
                j.src =
                    'https://www.googletagmanager.com/gtm.js?id=' + i + dl;
                f.parentNode.insertBefore(j, f);
            })(window, document, 'script', 'dataLayer', 'GTM-57DWK9H');
        </script>
        <!-- End Google Tag Manager -->
    <?php endif; ?>

    <!-- reCaptcha onLoad callback -->
    <script type="text/javascript">
        var onloadCallback = function() {
            if (document.getElementById('recaptcha')) {
                grecaptcha.render('recaptcha');
            }
        };
    </script>

    <!-- BEGIN JIVOSITE CODE -->
    <!-- <script type='text/javascript'>
        (function() {
            var widget_id = 'J4m5Mqt84P';
            var d = document;
            var w = window;

            function l() {
                var s = document.createElement('script');
                s.type = 'text/javascript';
                s.async = true;
                s.src = '//code.jivosite.com/script/widget/' + widget_id;
                var ss = document.getElementsByTagName('script')[0];
                ss.parentNode.insertBefore(s, ss);
            }
            if (d.readyState == 'complete') {
                l();
            } else {
                if (w.attachEvent) {
                    w.attachEvent('onload', l);
                } else {
                    w.addEventListener('load', l, false);
                }
            }
        })();
    </script> -->
    <!-- END JIVOSITE CODE -->

    <link rel="apple-touch-icon" sizes="180x180" href="/apple-touch-icon.png">
    <link rel="icon" type="image/png" sizes="32x32" href="/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="16x16" href="/favicon-16x16.png">
    <link rel="manifest" href="/site.webmanifest">
    <link rel="mask-icon" href="/safari-pinned-tab.svg" color="#5bbad5">
    <meta name="msapplication-TileColor" content="#da532c">
    <meta name="theme-color" content="#ffffff">
</head>

<body>

    <?php if (\IntecPhp\Model\Config::getDomain() == "deliveryprint.com.br") : ?>
        <!-- Google Tag Manager (noscript) -->
        <noscript>
            <iframe src="https://www.googletagmanager.com/ns.html?id=GTM-57DWK9H" height="0" width="0" style="display:none;visibility:hidden"></iframe>
        </noscript>
        <!-- End Google Tag Manager (noscript) -->
    <?php endif; ?>

    <a href="https://api.whatsapp.com/send?phone=5511992812284&text=Fale%20agora%20com%20nosso%20suporte!" class="whatsapp-support" target="_blank"><i class="fa fa-whatsapp whatsapp-icon-top"></i></a>

    <!-- <div class="jivo-btn-container" id="jivoContainer">
        <div class="jivo-btn jivo-online-btn jivo-btn-light" onclick="jivo_api.open();" style="font-family: Arial, Arial;font-size: 17px;background-color: #FF5722;border-radius: 20px;-moz-border-radius: 20px;-webkit-border-radius: 20px;height: 31px;line-height: 31px;padding: 0 15px 0 15px;font-weight: normal;font-style: normal">
            <div class="spinner">
                <div class="bounce1"></div>
                <div class="bounce2"></div>
                <div class="bounce3"></div>
            </div>
            <div class="jivo-btn-content">
                <div class="jivo-btn-icon" style="background-image: url(//static.jivosite.com/button/chat_light.png);"></div>Posso
                te ajudar?
            </div>
        </div>
        <div class="jivo-btn jivo-offline-btn jivo-btn-light" onclick="jivo_api.open();" style="font-family: Arial, Arial;font-size: 17px;background-color: #FF5722;border-radius: 20px;-moz-border-radius: 20px;-webkit-border-radius: 20px;height: 31px;line-height: 31px;padding: 0 15px 0 15px;display: none;font-weight: normal;font-style: normal">
            <div class="spinner">
                <div class="bounce1"></div>
                <div class="bounce2"></div>
                <div class="bounce3"></div>
            </div>
            <div class="jivo-btn-content">
                <div class="jivo-btn-icon" style="background-image: url(//static.jivosite.com/button/mail_light.png);"></div>Deixe
                aqui sua dúvida
            </div>
        </div>
    </div> -->

    <?php
    require_once 'app/views/partial/wsheader.php';
    ?>

    <div class="pageMain">
        <?php
        require_once 'app/views/template/' . $page . '.php';
        ?>
    </div>

    <?php
    require_once 'app/views/partial/wsfooter.php';

    foreach ($this->appendPartial as $p) {
        require_once 'app/views/partial/' . $p . '.php';
    }
    ?>

    <script src="https://code.jquery.com/jquery-3.4.1.min.js" integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo=" crossorigin="anonymous"></script>

    <!-- <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script> -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>

    <script src="https://cdn.jsdelivr.net/npm/vue"></script>

    <script type='text/javascript' src="/js/modernizr-output.js"></script>
    <script type='text/javascript' src="/js/app.min.js"></script>
    <?php foreach ($this->scripts as $path) : ?>
        <script type='text/javascript' src="<?php echo $path; ?>"></script>
    <?php endforeach; ?>
    <script type="application/ld+json">
        {
            "@context": "http://schema.org",
            "@type": "OfficeEquipmentStore",
            "name": "Delivery Print",
            "image": "https://deliveryprint.com.br/img/logo_square.png",
            "telephone": "(11)3807-7562",
            "url": "https://deliveryprint.com.br/",
            "email": "suporte@deliveryprint.com.br",
            "address": {
                "@type": "PostalAddress",
                "streetAddress": "Rua Oscar Freire 2617, cj 410",
                "addressLocality": "São Paulo",
                "addressRegion": "SP",
                "addressCountry": "Brasil",
                "postalCode": "05409-012"
            },
            "openingHours": [
                "Mo-Fri 08:00-19:00 "
            ],
            "aggregateRating": {
                "@type": "AggregateRating",
                "ratingValue": "5",
                "reviewCount": "1000"
            },
            "paymentAccepted": "Visa, Master Card, Boleto",
            "priceRange": "$"
        }
    </script>

    <?php if (\IntecPhp\Model\Config::getDomain() == "deliveryprint.com.br") : ?>
        <!-- reCaptcha code -->
        <script src="https://www.google.com/recaptcha/api.js?hl=pt-BR&onload=onloadCallback&render=explicit" async defer></script>

        <!-- Código de acompanhamento do RD Station -->
        <script type="text/javascript" async src="https://d335luupugsy2.cloudfront.net/js/loader-scripts/2ab25b20-a735-4877-a674-4d61197a4f14-loader.js"></script>
    <?php endif; ?>

</body>

</html>
