<!doctype html>
<html amp lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <script async src="https://cdn.ampproject.org/v0.js"></script>
    <?php require_once 'app/views/partial/layout-header-config.php'; ?>
    <link href="https://fonts.googleapis.com/css?family=Roboto+Slab|PT+Sans|Fira+Mono" rel="stylesheet">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Economica">
    <link rel="apple-touch-icon" sizes="180x180" href="/apple-touch-icon.png">
    <link rel="icon" type="image/png" sizes="32x32" href="/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="16x16" href="/favicon-16x16.png">
    <link rel="manifest" href="/site.webmanifest">
    <link rel="mask-icon" href="/safari-pinned-tab.svg" color="#5bbad5">
    <meta name="msapplication-TileColor" content="#da532c">
    <meta name="theme-color" content="#ffffff">
    <script async custom-element="amp-sidebar" src="https://cdn.ampproject.org/v0/amp-sidebar-0.1.js"></script>
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css" rel="stylesheet">

    <script type="application/ld+json">
        {
            "@context": "http://schema.org",
            "@type": "OfficeEquipmentStore",
            "name": "Delivery Print",
            "image" : "https://deliveryprint.com.br/img/logo_square.png",
            "telephone" : "(11)3807-7562",
            "url": "https://deliveryprint.com.br/",
            "email": "suporte@deliveryprint.com.br",
            "address" : {
                "@type" : "PostalAddress",
                "streetAddress" : "Rua Oscar Freire 2617, cj 410",
                "addressLocality" : "São Paulo",
                "addressRegion" : "SP",
                "addressCountry" : "Brasil",
                "postalCode" : "05409-012"
            },
            "openingHours": [
                "Mo-Fri 08:00-19:00 "
            ],
            "aggregateRating": {
                "@type": "AggregateRating",
                "ratingValue": "5",
                "reviewCount": "1000"
            },
            "paymentAccepted": "Visa, Master Card, Boleto",
            "priceRange": "$"
        }
    </script>
    <style amp-boilerplate>
        body {
            -webkit-animation: -amp-start 8s steps(1, end) 0s 1 normal both;
            -moz-animation: -amp-start 8s steps(1, end) 0s 1 normal both;
            -ms-animation: -amp-start 8s steps(1, end) 0s 1 normal both;
            animation: -amp-start 8s steps(1, end) 0s 1 normal both
        }

        @-webkit-keyframes -amp-start {
            from {
                visibility: hidden
            }

            to {
                visibility: visible
            }
        }

        @-moz-keyframes -amp-start {
            from {
                visibility: hidden
            }

            to {
                visibility: visible
            }
        }

        @-ms-keyframes -amp-start {
            from {
                visibility: hidden
            }

            to {
                visibility: visible
            }
        }

        @-o-keyframes -amp-start {
            from {
                visibility: hidden
            }

            to {
                visibility: visible
            }
        }

        @keyframes -amp-start {
            from {
                visibility: hidden
            }

            to {
                visibility: visible
            }
        }
    </style><noscript>
        <style amp-boilerplate>
            body {
                -webkit-animation: none;
                -moz-animation: none;
                -ms-animation: none;
                animation: none
            }
        </style>
    </noscript>
    <style amp-custom>
        .btn:focus,.btn:hover,a{text-decoration:none}.mt-0,.my-0,dl,ol,p,ul{margin-top:0}.img-a,.img-lp,.img-plot{position:relative;background-size:cover}#linkBuilding ul li,.breadcrumb{list-style:none}:root{--blue:#007bff;--indigo:#6610f2;--purple:#6f42c1;--pink:#e83e8c;--red:#dc3545;--orange:#fd7e14;--yellow:#ffc107;--green:#28a745;--teal:#20c997;--cyan:#17a2b8;--white:#fff;--gray:#6c757d;--gray-dark:#343a40;--primary:#007bff;--secondary:#6c757d;--success:#28a745;--info:#17a2b8;--warning:#ffc107;--danger:#dc3545;--light:#f8f9fa;--dark:#343a40;--breakpoint-xs:0;--breakpoint-sm:576px;--breakpoint-md:768px;--breakpoint-lg:992px;--breakpoint-xl:1200px;--font-family-sans-serif:-apple-system,BlinkMacSystemFont,"Segoe UI",Roboto,"Helvetica Neue",Arial,sans-serif,"Apple Color Emoji","Segoe UI Emoji","Segoe UI Symbol","Noto Color Emoji";--font-family-monospace:SFMono-Regular,Menlo,Monaco,Consolas,"Liberation Mono","Courier New",monospace}*,::after,::before{box-sizing:border-box}html{font-family:sans-serif;line-height:1.15;-webkit-text-size-adjust:100%;-ms-text-size-adjust:100%;-ms-overflow-style:scrollbar;-webkit-tap-highlight-color:transparent}body{margin:0;font-size:1rem;font-weight:400;line-height:1.5;color:#212529;text-align:left;background-color:#fff;overflow-x:hidden}h1,h2,h3,h4,h5,h6{margin-top:0;margin-bottom:.5rem}dl,ol,p,ul{margin-bottom:1rem}ol ol,ol ul,ul ol,ul ul{margin-bottom:0}a{color:#007bff;background-color:transparent;-webkit-text-decoration-skip:objects}.h1,.h2,.h3,.h4,.h5,.h6,h1,h2,h3,h4,h5,h6{margin-bottom:.5rem;font-family:inherit;font-weight:500;line-height:1.2;color:inherit}.btn,.text-simple{font-weight:400}.h1,h1{font-size:2.5rem}.h2,h2{font-size:24px}.h3,h3{font-size:1.75rem}.h4,h4{font-size:1.5rem}.h5,h5{font-size:1.25rem}.btn,.h6,h6{font-size:1rem}.container{width:100%;padding-right:15px;padding-left:15px;margin-right:auto;margin-left:auto}@media (min-width:576px){.container{max-width:540px}}@media (min-width:768px){.container{max-width:720px}}@media (min-width:992px){.container{max-width:960px}}@media (min-width:1200px){.container{max-width:1140px}}.row{display:-ms-flexbox;display:flex;-ms-flex-wrap:wrap;flex-wrap:wrap;margin-right:-15px;margin-left:-15px}.col,.col-1,.col-10,.col-11,.col-12,.col-2,.col-3,.col-4,.col-5,.col-6,.col-7,.col-8,.col-9,.col-auto,.col-lg,.col-lg-1,.col-lg-10,.col-lg-11,.col-lg-12,.col-lg-2,.col-lg-3,.col-lg-4,.col-lg-5,.col-lg-6,.col-lg-7,.col-lg-8,.col-lg-9,.col-lg-auto,.col-md,.col-md-1,.col-md-10,.col-md-11,.col-md-12,.col-md-2,.col-md-3,.col-md-4,.col-md-5,.col-md-6,.col-md-7,.col-md-8,.col-md-9,.col-md-auto,.col-sm,.col-sm-1,.col-sm-10,.col-sm-11,.col-sm-12,.col-sm-2,.col-sm-3,.col-sm-4,.col-sm-5,.col-sm-6,.col-sm-7,.col-sm-8,.col-sm-9,.col-sm-auto,.col-xl,.col-xl-1,.col-xl-10,.col-xl-11,.col-xl-12,.col-xl-2,.col-xl-3,.col-xl-4,.col-xl-5,.col-xl-6,.col-xl-7,.col-xl-8,.col-xl-9,.col-xl-auto{position:relative;width:100%;min-height:1px;padding-right:15px;padding-left:15px}.col{-ms-flex-preferred-size:0;flex-basis:0;-ms-flex-positive:1;flex-grow:1;max-width:100%}.col-auto{-ms-flex:0 0 auto;flex:0 0 auto;width:auto;max-width:none}.col-1{-ms-flex:0 0 8.333333%;flex:0 0 8.333333%;max-width:8.333333%}.col-2{-ms-flex:0 0 16.666667%;flex:0 0 16.666667%;max-width:16.666667%}.col-3{-ms-flex:0 0 25%;flex:0 0 25%;max-width:25%}.col-4{-ms-flex:0 0 33.333333%;flex:0 0 33.333333%;max-width:33.333333%}.col-5{-ms-flex:0 0 41.666667%;flex:0 0 41.666667%;max-width:41.666667%}.col-6{-ms-flex:0 0 50%;flex:0 0 50%;max-width:50%}.col-7{-ms-flex:0 0 58.333333%;flex:0 0 58.333333%;max-width:58.333333%}.col-8{-ms-flex:0 0 66.666667%;flex:0 0 66.666667%;max-width:66.666667%}.col-9{-ms-flex:0 0 75%;flex:0 0 75%;max-width:75%}.col-10{-ms-flex:0 0 83.333333%;flex:0 0 83.333333%;max-width:83.333333%}.col-11{-ms-flex:0 0 91.666667%;flex:0 0 91.666667%;max-width:91.666667%}.col-12{-ms-flex:0 0 100%;flex:0 0 100%;max-width:100%}@media (min-width:576px){.col-sm{-ms-flex-preferred-size:0;flex-basis:0;-ms-flex-positive:1;flex-grow:1;max-width:100%}.col-sm-auto{-ms-flex:0 0 auto;flex:0 0 auto;width:auto;max-width:none}.col-sm-1{-ms-flex:0 0 8.333333%;flex:0 0 8.333333%;max-width:8.333333%}.col-sm-2{-ms-flex:0 0 16.666667%;flex:0 0 16.666667%;max-width:16.666667%}.col-sm-3{-ms-flex:0 0 25%;flex:0 0 25%;max-width:25%}.col-sm-4{-ms-flex:0 0 33.333333%;flex:0 0 33.333333%;max-width:33.333333%}.col-sm-5{-ms-flex:0 0 41.666667%;flex:0 0 41.666667%;max-width:41.666667%}.col-sm-6{-ms-flex:0 0 50%;flex:0 0 50%;max-width:50%}.col-sm-7{-ms-flex:0 0 58.333333%;flex:0 0 58.333333%;max-width:58.333333%}.col-sm-8{-ms-flex:0 0 66.666667%;flex:0 0 66.666667%;max-width:66.666667%}.col-sm-9{-ms-flex:0 0 75%;flex:0 0 75%;max-width:75%}.col-sm-10{-ms-flex:0 0 83.333333%;flex:0 0 83.333333%;max-width:83.333333%}.col-sm-11{-ms-flex:0 0 91.666667%;flex:0 0 91.666667%;max-width:91.666667%}.col-sm-12{-ms-flex:0 0 100%;flex:0 0 100%;max-width:100%}}@media (min-width:768px){.col-md{-ms-flex-preferred-size:0;flex-basis:0;-ms-flex-positive:1;flex-grow:1;max-width:100%}.col-md-auto{-ms-flex:0 0 auto;flex:0 0 auto;width:auto;max-width:none}.col-md-1{-ms-flex:0 0 8.333333%;flex:0 0 8.333333%;max-width:8.333333%}.col-md-2{-ms-flex:0 0 16.666667%;flex:0 0 16.666667%;max-width:16.666667%}.col-md-3{-ms-flex:0 0 25%;flex:0 0 25%;max-width:25%}.col-md-4{-ms-flex:0 0 33.333333%;flex:0 0 33.333333%;max-width:33.333333%}.col-md-5{-ms-flex:0 0 41.666667%;flex:0 0 41.666667%;max-width:41.666667%}.col-md-6{-ms-flex:0 0 50%;flex:0 0 50%;max-width:50%}.col-md-7{-ms-flex:0 0 58.333333%;flex:0 0 58.333333%;max-width:58.333333%}.col-md-8{-ms-flex:0 0 66.666667%;flex:0 0 66.666667%;max-width:66.666667%}.col-md-9{-ms-flex:0 0 75%;flex:0 0 75%;max-width:75%}.col-md-10{-ms-flex:0 0 83.333333%;flex:0 0 83.333333%;max-width:83.333333%}.col-md-11{-ms-flex:0 0 91.666667%;flex:0 0 91.666667%;max-width:91.666667%}.col-md-12{-ms-flex:0 0 100%;flex:0 0 100%;max-width:100%}}.btn{display:inline-block;text-align:center;white-space:nowrap;vertical-align:middle;-webkit-user-select:none;-moz-user-select:none;-ms-user-select:none;user-select:none;border:1px solid transparent;padding:.375rem .75rem;line-height:1.5;border-radius:.25rem;transition:color .15s ease-in-out,background-color .15s ease-in-out,border-color .15s ease-in-out,box-shadow .15s ease-in-out}@media screen and (prefers-reduced-motion:reduce){.btn{transition:none}}.btn.focus,.btn:focus{outline:0;box-shadow:0 0 0 .2rem rgba(0,123,255,.25)}.btn-info.focus,.btn-info:focus,.btn-info:not(:disabled):not(.disabled).active:focus,.btn-info:not(:disabled):not(.disabled):active:focus,.show>.btn-info.dropdown-toggle:focus{box-shadow:0 0 0 .2rem rgba(23,162,184,.5)}.btn.disabled,.btn:disabled{opacity:.65}.btn:not(:disabled):not(.disabled){cursor:pointer}a.btn.disabled,fieldset:disabled a.btn{pointer-events:none}.btn-info{color:#fff;background-color:#17a2b8;border-color:#17a2b8}.btn-info:hover{color:#fff;background-color:#138496;border-color:#117a8b}.btn-info.disabled,.btn-info:disabled{color:#fff;background-color:#17a2b8;border-color:#17a2b8}.btn-info:not(:disabled):not(.disabled).active,.btn-info:not(:disabled):not(.disabled):active,.show>.btn-info.dropdown-toggle{color:#fff;background-color:#117a8b;border-color:#10707f}.breadcrumb{display:-ms-flexbox;display:flex;-ms-flex-wrap:wrap;flex-wrap:wrap;padding:.75rem 1rem;margin-bottom:1rem;border-radius:.25rem}.breadcrumb-item+.breadcrumb-item{padding-left:.5rem}.border-0{border:0}.d-none{display:none}.d-inline-block{display:inline-block}.d-flex{display:-ms-flexbox;display:flex}.justify-content-end{-ms-flex-pack:end;justify-content:flex-end}.justify-content-center{-ms-flex-pack:center;justify-content:center}.justify-content-between{-ms-flex-pack:justify;justify-content:space-between}.align-items-end{-ms-flex-align:end;align-items:flex-end}.align-items-center{-ms-flex-align:center;align-items:center}.align-items-baseline{-ms-flex-align:baseline;align-items:baseline}.float-left{float:left}.float-right{float:right}.m-0{margin:0}.mr-0,.mx-0{margin-right:0}.mb-0,.my-0{margin-bottom:0}.ml-0,.mx-0{margin-left:0}.m-1{margin:.25rem}.mt-1,.my-1{margin-top:.25rem}.mr-1,.mx-1{margin-right:.25rem}.mb-1,.my-1{margin-bottom:.25rem}.ml-1,.mx-1{margin-left:.25rem}.m-2{margin:.5rem}.mt-2,.my-2{margin-top:.5rem}.mr-2,.mx-2{margin-right:.5rem}.mb-2,.my-2{margin-bottom:.5rem}.ml-2,.mx-2{margin-left:.5rem}.m-3{margin:1rem}.mt-3,.my-3{margin-top:1rem}.mr-3,.mx-3{margin-right:1rem}.mb-3,.my-3{margin-bottom:1rem}.ml-3,.mx-3{margin-left:1rem}.m-4{margin:1.5rem}.mt-4,.my-4{margin-top:1.5rem}.mr-4,.mx-4{margin-right:1.5rem}.mb-4,.my-4{margin-bottom:1.5rem}.ml-4,.mx-4{margin-left:1.5rem}.m-5{margin:3rem}.mt-5,.my-5{margin-top:3rem}.mr-5,.mx-5{margin-right:3rem}.mb-5,.my-5{margin-bottom:3rem}.ml-5,.mx-5{margin-left:3rem}.p-0{padding:0}.pt-0,.py-0{padding-top:0}.pr-0,.px-0{padding-right:0}.pb-0,.py-0{padding-bottom:0}.pl-0,.px-0{padding-left:0}.p-1{padding:.25rem}.pt-1,.py-1{padding-top:.25rem}.pr-1,.px-1{padding-right:.25rem}.pb-1,.py-1{padding-bottom:.25rem}.pl-1,.px-1{padding-left:.25rem}.p-2{padding:.5rem}.pt-2,.py-2{padding-top:.5rem}.pr-2,.px-2{padding-right:.5rem}.pb-2,.py-2{padding-bottom:.5rem}.pl-2,.px-2{padding-left:.5rem}.p-3{padding:1rem}.pt-3,.py-3{padding-top:1rem}.pr-3,.px-3{padding-right:1rem}.pb-3,.py-3{padding-bottom:1rem}.pl-3,.px-3{padding-left:1rem}.p-4{padding:1.5rem}.pt-4,.py-4{padding-top:1.5rem}.pr-4,.px-4{padding-right:1.5rem}.pb-4,.py-4{padding-bottom:1.5rem}.pl-4,.px-4{padding-left:1.5rem}.p-5{padding:3rem}.pt-5,.py-5{padding-top:3rem}.pr-5,.px-5{padding-right:3rem}.pb-5,.py-5{padding-bottom:3rem}.pl-5,.px-5{padding-left:3rem}@media (min-width:576px){.d-sm-none{display:none}.d-sm-block{display:block}.d-sm-inline{display:inline}.d-sm-inline-block{display:inline-block}.d-sm-flex{display:-ms-flexbox;display:flex}.mr-sm-2{margin-right:.5rem}}.text-justify{text-align:justify}.text-left{text-align:left}.text-right{text-align:right}.text-center{text-align:center}@media (min-width:768px){.d-md-none{display:none}.d-md-block{display:block}.d-md-inline{display:inline}.d-md-inline-block{display:inline-block}.d-md-flex{display:-ms-flexbox;display:flex}.mt-md-0,.my-md-0{margin-top:0}.p-md-5{padding:3rem}.pt-md-5,.py-md-5{padding-top:3rem}.pr-md-5,.px-md-5{padding-right:3rem}.pb-md-5,.py-md-5{padding-bottom:3rem}.text-md-left{text-align:left}}.col-md-6 ul{list-style-type:none}.details{flex-direction:column;justify-content:space-between;align-items:flex-start}.img-a{height:370px}.img-lp{height:335px}.img-plot{height:270px;max-width:420px}.paper_type{position:absolute;bottom:-30px;left:50px}.paper_type .a0{font-size:60px;padding-bottom:6px}.paper_type .a1{font-size:56px;padding-bottom:7px}.paper_type .a2{font-size:52px;padding-bottom:8px}.paper_type .a3{font-size:48px}.paper_type .a4{font-size:36px;padding-bottom:12px}.paper_type .a5{font-size:24px;padding-bottom:16px}.paper_type .a0,.paper_type .a1,.paper_type .a2,.paper_type .a3,.paper_type .a4,.paper_type .a5{box-shadow:0 5px .1em #a8a8a8;position:relative;top:0}#email_form label{width:100%}.btn-info:hover{background:#9DCB7D}footer{margin-top:0}.ul-plot{font-size:18px}.form-plot{border-color:#fc4a1a}#linkBuilding ul li a{color:#67a4ff}#linkBuilding p{font-size:14px;text-align:center}.text-orange,a.text-orange{color:#fc4a1a}.breadcrumb-item,.breadcrumb-item.active a,.text-grey,a.text-grey{color:#a8a8a8}.menu{background-color:#FDFDFD}.menu-item a{transition:all .1s;padding:20px 10px}@media screen and (max-width:640px){header .menu,header .menu .nav-collapse,header .menu nav{background-color:#f3f3f3}#contacts,header .hidden-xs,header .info{display:none}header .menu{position:fixed;top:auto;z-index:100;width:100%;bottom:0;height:60px}header .menu .nav-link{padding:10px 5px}header .menu-item a{padding:5px}header .menu #cart-header,header .menu .nav-toggle{position:absolute}header .menu #cart-header{right:50px;bottom:0}header .menu .nav-toggle{right:0;bottom:2px}header .menu nav>ul{background:#f3f3f3}header .menu .nav-collapse.opened{top:-275px}header .menu .usr{min-height:0}header .menu .usr.separator{width:30%;float:right;background:0 0;border-bottom:1px solid #fc4a1a}header .logo{width:98px;height:auto;display:block}header #btn-acessar,header #cart-header{padding:15px}}.text-strong{font-weight:700}.text-thin{font-weight:lighter}.text-toupper{text-transform:uppercase}@media (max-width:1200px){.exibir{display:none}.menu .nav-link{padding:20px}.menu .text-lg{font-size:14px}}@media (max-width:1400px){.menu .text-lg{font-size:16px}.menu .nav-link{padding:20px 10px}.menu .text-sm{font-size:12px}}.default-title{font-family:Economica,sans-serif}.default-text{font-family:Lato,sans-serif}.breadcrumb{background-color:transparent;font-size:15px}.breadcrumb-item+.breadcrumb-item::before{content:">>";font-weight:700}.breadcrumb-item{font-weight:lighter}.breadcrumb-item a{color:#a8a8a8;font-weight:700}.subtitle{font-family:Lato,sans-serif;font-size:14px}.text-hg{font-size:24px}.btn-b4,h3{font-size:18px}.bg-blue{background-color:#67A4FF}.bg-white{background-color:#FFF}.btn-b4{font-family:Economica,sans-serif;font-weight:700;line-height:17px;min-width:170px;height:50px}.newField{font-size:12px;font-family:Lato,sans-serif;width:auto}@media screen and (max-width:640px){footer,footer .contato{background-color:initial}body{overflow-x:hidden}footer{margin-top:20px;padding:0}footer>.row{margin:0}footer .hidden-xs{display:none}footer .contato{max-width:100vw;margin-left:0;flex:initial}footer .contato .ft-info{padding:15px 0;width:100%}footer .contato .ft-info .pedido{width:50%;padding-left:15px}footer .contato .ft-info .ft-social{width:50%;text-align:right;margin-right:0}footer .contato .ft-info .ft-social p{display:inline-block;margin-right:6px;vertical-align:super}footer .contato .ft-info .ft-social a{float:none}}.text-md{font-size:16px}.text-sm{font-size:14px}.shadow-sm{margin-left:0}
    </style>
</head>

<body>

    <?php
        require_once 'app/views/partial/amp-wsheader.php';
        require_once 'app/views/template/' . $page . '.php';
        require_once 'app/views/partial/amp-wsfooter.php';
    ?>

</body>

</html>
