<!DOCTYPE html>
<html lang="pt-br">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <?php require_once 'app/views/partial/layout-header-config.php'; ?>
    <link type="text/css" rel="stylesheet" href="/css/app.min.css" />
    <?php foreach ($this->stylesheets as $href) : ?>
        <link href="<?php echo $href; ?>" rel="stylesheet" type="text/css">
    <?php endforeach; ?>
    <link href="https://fonts.googleapis.com/css?family=Roboto+Slab|PT+Sans|Fira+Mono" rel="stylesheet">
    <link href='https://fonts.googleapis.com/css?family=Roboto:100,300,400,500,700,900|Material+Icons' rel="stylesheet">
</head>

<body>

    <?php
    require_once 'app/views/template/' . $page . '.php';

    foreach ($this->appendPartial as $p) {
        require_once 'app/views/partial/' . $p . '.php';
    }
    ?>

    <script type='text/javascript' src="/js/app-adm.min.js"></script>
    <?php foreach ($this->scripts as $path) : ?>
        <script type='text/javascript' src="<?php echo $path; ?>"></script>
    <?php endforeach; ?>
</body>

</html>
