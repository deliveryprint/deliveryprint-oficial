<!doctype html>
<html amp lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <script async src="https://cdn.ampproject.org/v0.js"></script>
    <?php require_once 'app/views/partial/layout-header-config.php'; ?>
    <link href="https://fonts.googleapis.com/css?family=Roboto+Slab|PT+Sans|Fira+Mono" rel="stylesheet">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Economica">
    <link rel="apple-touch-icon" sizes="180x180" href="/apple-touch-icon.png">
    <link rel="icon" type="image/png" sizes="32x32" href="/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="16x16" href="/favicon-16x16.png">
    <link rel="manifest" href="/site.webmanifest">
    <link rel="mask-icon" href="/safari-pinned-tab.svg" color="#5bbad5">
    <meta name="msapplication-TileColor" content="#da532c">
    <meta name="theme-color" content="#ffffff">
    <script async custom-element="amp-sidebar" src="https://cdn.ampproject.org/v0/amp-sidebar-0.1.js"></script>
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css" rel="stylesheet">

    <script type="application/ld+json">
        {
            "@context": "http://schema.org",
            "@type": "OfficeEquipmentStore",
            "name": "Delivery Print",
            "image" : "https://deliveryprint.com.br/img/logo_square.png",
            "telephone" : "(11)3807-7562",
            "url": "https://deliveryprint.com.br/",
            "email": "suporte@deliveryprint.com.br",
            "address" : {
                "@type" : "PostalAddress",
                "streetAddress" : "Rua Oscar Freire 2617, cj 410",
                "addressLocality" : "São Paulo",
                "addressRegion" : "SP",
                "addressCountry" : "Brasil",
                "postalCode" : "05409-012"
            },
            "openingHours": [
                "Mo-Fri 08:00-19:00 "
            ],
            "aggregateRating": {
                "@type": "AggregateRating",
                "ratingValue": "5",
                "reviewCount": "1000"
            },
            "paymentAccepted": "Visa, Master Card, Boleto",
            "priceRange": "$"
        }
    </script>
    <style amp-boilerplate>
        body {
            -webkit-animation: -amp-start 8s steps(1, end) 0s 1 normal both;
            -moz-animation: -amp-start 8s steps(1, end) 0s 1 normal both;
            -ms-animation: -amp-start 8s steps(1, end) 0s 1 normal both;
            animation: -amp-start 8s steps(1, end) 0s 1 normal both
        }

        @-webkit-keyframes -amp-start {
            from {
                visibility: hidden
            }

            to {
                visibility: visible
            }
        }

        @-moz-keyframes -amp-start {
            from {
                visibility: hidden
            }

            to {
                visibility: visible
            }
        }

        @-ms-keyframes -amp-start {
            from {
                visibility: hidden
            }

            to {
                visibility: visible
            }
        }

        @-o-keyframes -amp-start {
            from {
                visibility: hidden
            }

            to {
                visibility: visible
            }
        }

        @keyframes -amp-start {
            from {
                visibility: hidden
            }

            to {
                visibility: visible
            }
        }
    </style><noscript>
        <style amp-boilerplate>
            body {
                -webkit-animation: none;
                -moz-animation: none;
                -ms-animation: none;
                animation: none
            }
        </style>
    </noscript>
    <style amp-custom>
        .btn:focus,.btn:hover,a{text-decoration:none}.btn-orange.focus,.btn-orange:focus,.btn-orange:not(:disabled):not(.disabled).active:focus,.btn-orange:not(:disabled):not(.disabled):active:focus,.show>.btn-orange.dropdown-toggle:focus{box-shadow:0 0 0 .2rem rgba(255,255,255,.5)}.btn-orange,.btn-orange-outlined{cursor:pointer}*,.btn-orange,.btn-orange-outlined,::after,::before{box-sizing:border-box}.btn,.text-center{text-align:center}.btn-orange,.btn-orange-outlined,.text-toupper{text-transform:uppercase}.bg-solicitation{min-height:64.8148148148vh;background:url(/img/home/home-section.jpg) center -266px no-repeat content-box}.bg-solicitation .btn{border-width:3px}.bg-solicitation .jumbo-solicitation{padding-bottom:115px}.card-bg{width:20.8333333333vw;height:23.1481481481vh;top:-140px;position:relative;background-repeat:no-repeat;background-size:cover;background-position:center center}.card-bg :hover{background-size:120%}.card-bg a{display:block;height:100%}.card-bg-c1{background-image:url(/img/home/card-1.jpg)}.card-bg-c1 .card-txt{background-color:#fc4a1a;margin-bottom:0}.card-bg-c2{background-image:url(/img/home/card-2.jpg)}.card-bg-c2 .card-txt{background-color:#9DCB7D;margin-bottom:0}.card-bg-c3{background-image:url(/img/home/card-3.jpg)}.card-bg-c3 .card-txt{background-color:#4B78BA;margin-bottom:0}.card-bg .card-container{position:absolute;bottom:0}.solicitation-fix{position:relative}.benefits .card{background-color:#f1f1f1;padding:40px 25px;filter:drop-shadow(0 0 10px rgba(0, 0, 0, .25))}.benefits .card.featured{z-index:1;top:-40px;background-color:#F9F9F9;padding-bottom:90px}.benefits .card.featured .logo{width:194px;height:60px;margin:20px 0}.benefits .card h2{color:#a8a8a8}.benefits .card li.list-group-item{margin:10px 0;background-color:transparent;line-height:33px}.benefits .card li.list-group-item p{font-size:24px;font-weight:lighter}@media (max-width:1367px) and (min-width:642px){.solicitation-items{height:80px}.card-bg{top:-170px}.bg-solicitation>.container{transform:scale(.8);position:relative;top:-40px}}@media (max-width:1200px){.bg-solicitation{max-height:515px}.card-container .h4{font-size:16px}.card-container .h2{font-size:24px}.benefits .card h2{font-size:24px;margin-bottom:25px}.benefits .card li.list-group-item p{font-size:18px}}@media (max-width:991.97px){.bg-solicitation h1{font-size:63px}}@media (max-width:768px){.bg-solicitation{min-height:46.2962962963vh}#change-order-1{order:1;text-align:center}#change-order-2{order:2;text-align:center}#change-order-2 .btn-white{min-width:280px;height:60px;font-size:18px;line-height:45px;float:none}.bg-solicitation h1{font-size:2.5rem}.benefits .card h2{font-size:18px;margin-bottom:25px}.benefits .card li.list-group-item p{font-size:12px}}@media (max-width:641px){.bg-solicitation.text-center.pb-5{padding-bottom:0;max-width:100vw}.bg-solicitation .container{position:relative}.bg-solicitation .container h1{font-size:36px}.bg-solicitation .container .btn-b2{font-weight:700;font-size:24px;line-height:30px;min-width:220px;height:50px}.bg-solicitation .container div.jumbo-solicitation{margin-top:3rem;padding-bottom:1px}.bg-solicitation .container div.jumbo-solicitation .d-sm-inline-block{display:table;width:auto;padding:6px 1px;border-radius:6px;margin-right:0;font-size:14px;position:relative;left:50%;transform:translate(-50%,-50%)}.bg-solicitation .container .btn.btn-orange.mb-5.mt-1.btn-b2{margin-top:20px}.bg-solicitation .container .text-white.d-sm-inline-block.mr-4.btn-alpha.border-radius-hg.btn-b6-xs.meni{margin-bottom:10px}.solicitation-items{margin:10px 0}.solicitation-items div.card-bg{width:100%;top:0;margin:10px 22px}.solicitation-fix{top:0}}.container,.container-fluid{padding-right:15px;padding-left:15px;margin-right:auto;margin-left:auto;width:100%}.pt-5,.py-5{padding-top:3rem}.pb-5,.py-5{padding-bottom:3rem}@media (min-width:576px){.container{max-width:540px}}@media (min-width:768px){.container{max-width:720px}}@media (min-width:992px){.container{max-width:960px}}@media (min-width:1200px){.container{max-width:1140px}}.text-white{color:#fff}.pt-2,.py-2{padding-top:.5rem}.d-block{display:block}@media (min-width:576px){.d-sm-none{display:none}.pt-sm-5,.py-sm-5{padding-top:3rem}}.btn,.d-sm-inline-block{display:inline-block}.btn-orange{background-color:#fc4a1a;border-color:#fff;color:#fff}.btn-orange:hover{border-color:#e6e5e5}.btn-orange.disabled,.btn-orange:disabled{color:#fff;background-color:#fc4a1a;border-color:#fff}.btn-orange:not(:disabled):not(.disabled).active,.btn-orange:not(:disabled):not(.disabled):active,.show>.btn-orange.dropdown-toggle{color:#fff;background-color:#e03203;border-color:#dfdfdf}.btn-orange-outlined.focus,.btn-orange-outlined:focus,.btn-orange-outlined:not(:disabled):not(.disabled).active:focus,.btn-orange-outlined:not(:disabled):not(.disabled):active:focus,.show>.btn-orange-outlined.dropdown-toggle:focus{box-shadow:0 0 0 .2rem rgba(252,74,26,.5)}.btn-orange:disabled{background-color:#a8a8a8}.btn-orange:disabled:hover{background-color:currentColor}.btn-orange:active{background-color:#ca3b15}.btn-orange:hover{color:#fff;background-color:#9dcb7d}.btn-orange-outlined{background-color:rgba(0,0,0,0);border-color:#fc4a1a;color:#fc4a1a}.btn-orange-outlined:hover{background-color:#fc4a1a;border-color:#e03203}.btn-orange-outlined.disabled,.btn-orange-outlined:disabled{color:#fff;background-color:rgba(0,0,0,0);border-color:#fc4a1a}.btn-orange-outlined:not(:disabled):not(.disabled).active,.btn-orange-outlined:not(:disabled):not(.disabled):active,.show>.btn-orange-outlined.dropdown-toggle{color:#fff;background-color:rgba(0,0,0,0);border-color:#d32f03}.btn-orange-outlined:disabled:hover{background-color:currentColor}.btn-orange-outlined:hover{background-color:rgba(255,255,255,.2);color:#fc4a1a}.btn-orange-outlined:active{background-color:rgba(0,0,0,.2)}.btn-orange-outlined:disabled{background-color:transparent;border-color:#a8a8a8}.mb-1,.my-1{margin-bottom:.25rem}.mt-1,.my-1{margin-top:.25rem}.btn{font-family:Economica;font-weight:400;white-space:nowrap;vertical-align:middle;user-select:none;border:1px solid transparent;padding:.375rem .75rem;font-size:1rem;line-height:1.5;border-radius:.25rem;transition:color .15s ease-in-out,background-color .15s ease-in-out,border-color .15s ease-in-out,box-shadow .15s ease-in-out}@media screen and (prefers-reduced-motion:reduce){.btn{transition:none}}.btn.focus,.btn:focus{outline:0;box-shadow:0 0 0 .2rem rgba(0,123,255,.25)}.h1,.h2,.h3,.h4,.h5,.h6,h1,h2,h3,h4,h5,h6{margin-bottom:.5rem;line-height:1.2}.h3,h3{font-size:1.75rem}.mt-5,.my-5{margin-top:3rem}.bg-transparent{background-color:transparent}.btn-b6{font-size:18px;line-height:17px;min-width:120px;height:30px}.no-border{border:none}.d-flex,.row{display:flex}.mr-4,.mx-4{margin-right:1.5rem}.flex-wrap{flex-wrap:wrap}.text-left{text-align:left}.mt-0,.my-0{margin-top:0}.float-none{float:none}.p-2{padding:.5rem}.h4,h4{font-size:1.5rem}.h2,h2{font-size:2rem}.text-orange,a.text-orange{color:#fc4a1a}.mb-4,.my-4{margin-bottom:1.5rem}#how_it_works{overflow:hidden;z-index:2}#how_it_works .mk{height:500px;position:absolute}#how_it_works>.row{position:relative;overflow:hidden}#how_it_works.center{padding:3em 0 4.8em}#how_it_works.center.col-3,#how_it_works.center.col-lg-3,#how_it_works.center.col-md-3,#how_it_works.center.col-sm-3,#how_it_works.center.col-xl-3{margin-left:-12.5%}#how_it_works.center.col-2,#how_it_works.center.col-lg-2,#how_it_works.center.col-md-2,#how_it_works.center.col-sm-2,#how_it_works.center.col-xl-2{margin-left:-8%}#how_it_works .col-6{position:relative;padding:3em 0}#how_it_works .offset-half{margin-left:4.15%}@media (min-width:992px){#how_it_works #pos_left{left:20%}#how_it_works p.center{max-width:15.625vw}#how_it_works .mk{height:500px}}@media (max-width:1199.98px){#how_it_works .bg-orange,#how_it_works .bg-smoother-og{height:300px}#how_it_works>.row{margin:0}#how_it_works p{font-size:14px}}.col-6{flex:0 0 50%;max-width:50%}.col,.col-12{max-width:100%}.col-12{flex:0 0 100%}.clip.chevron{-webkit-clip-path:polygon(75% 0,100% 50%,75% 100%,0 100%,25% 50%,0 0);clip-path:polygon(75% 0,100% 50%,75% 100%,0 100%,25% 50%,0 0)}@media (max-width:767.98px){#how_it_works .clip{height:400px}#how_it_works p{font-size:18px}.clip.b{-webkit-clip-path:polygon(100% 0,100% 80%,50% 100%,0 80%,0 0,50% 20%);clip-path:polygon(100% 0,100% 80%,50% 100%,0 80%,0 0,50% 20%)}.clip.b>i{margin-top:120px}}.clip.hex{-webkit-clip-path:polygon(50% 0,100% 25%,100% 75%,50% 100%,0 75%,0 25%);clip-path:polygon(50% 0,100% 25%,100% 75%,50% 100%,0 75%,0 25%)}.clip.star{-webkit-clip-path:polygon(50% 0,61% 35%,98% 35%,68% 57%,79% 91%,50% 70%,21% 91%,32% 57%,2% 35%,39% 35%);clip-path:polygon(50% 0,61% 35%,98% 35%,68% 57%,79% 91%,50% 70%,21% 91%,32% 57%,2% 35%,39% 35%)}.clip.point-r{-webkit-clip-path:polygon(0 0,75% 0,100% 50%,75% 100%,0 100%);clip-path:polygon(0 0,75% 0,100% 50%,75% 100%,0 100%)}.bg-orange{background-color:#fc4a1a}.bg-smooth-og{background-color:#FF5A2B}.bg-smoother-og{background-color:#FF6F4A}.h1,.h2,.h3,.h4,.h5,.h6,h1,h2,h3,h4,h5,h6{font-family:Economica,sans-serif;font-weight:700;margin:0 0 20px;padding:0}.row{flex-wrap:wrap;margin-right:-15px;margin-left:-15px}.no-gutters{margin-right:0;margin-left:0}.text-grey,a.text-grey{color:#a8a8a8}.col,.col-1,.col-10,.col-11,.col-12,.col-2,.col-3,.col-4,.col-5,.col-6,.col-7,.col-8,.col-9,.col-auto,.col-lg,.col-lg-1,.col-lg-10,.col-lg-11,.col-lg-12,.col-lg-2,.col-lg-3,.col-lg-4,.col-lg-5,.col-lg-6,.col-lg-7,.col-lg-8,.col-lg-9,.col-lg-auto,.col-md,.col-md-1,.col-md-10,.col-md-11,.col-md-12,.col-md-2,.col-md-3,.col-md-4,.col-md-5,.col-md-6,.col-md-7,.col-md-8,.col-md-9,.col-md-auto,.col-sm,.col-sm-1,.col-sm-10,.col-sm-11,.col-sm-12,.col-sm-2,.col-sm-3,.col-sm-4,.col-sm-5,.col-sm-6,.col-sm-7,.col-sm-8,.col-sm-9,.col-sm-auto,.col-xl,.col-xl-1,.col-xl-10,.col-xl-11,.col-xl-12,.col-xl-2,.col-xl-3,.col-xl-4,.col-xl-5,.col-xl-6,.col-xl-7,.col-xl-8,.col-xl-9,.col-xl-auto{position:relative;width:100%;min-height:1px;padding-right:15px;padding-left:15px}.col{flex-basis:0;flex-grow:1}.text-italic{font-style:italic}@media (min-width:768px){.col-md{flex-basis:0;flex-grow:1;max-width:100%}.col-md-auto{flex:0 0 auto;width:auto;max-width:none}.col-md-1{flex:0 0 8.3333333333%;max-width:8.3333333333%}.col-md-2{flex:0 0 16.6666666667%;max-width:16.6666666667%}.col-md-3{flex:0 0 25%;max-width:25%}.col-md-4{flex:0 0 33.3333333333%;max-width:33.3333333333%}.col-md-5{flex:0 0 41.6666666667%;max-width:41.6666666667%}.col-md-6{flex:0 0 50%;max-width:50%}.col-md-7{flex:0 0 58.3333333333%;max-width:58.3333333333%}.offset-md-3{margin-left:25%}}@media (min-width:992px){.col-lg{flex-basis:0;flex-grow:1;max-width:100%}.col-lg-auto{flex:0 0 auto;width:auto;max-width:none}.col-lg-1{flex:0 0 8.3333333333%;max-width:8.3333333333%}.col-lg-2{flex:0 0 16.6666666667%;max-width:16.6666666667%}.col-lg-3{flex:0 0 25%;max-width:25%}.col-lg-4{flex:0 0 33.3333333333%;max-width:33.3333333333%}.col-lg-5{flex:0 0 41.6666666667%;max-width:41.6666666667%}.col-lg-6{flex:0 0 50%;max-width:50%}.col-lg-7{flex:0 0 58.3333333333%;max-width:58.3333333333%}.col-lg-8{flex:0 0 66.6666666667%;max-width:66.6666666667%}.col-lg-9{flex:0 0 75%;max-width:75%}.col-lg-10{flex:0 0 83.3333333333%;max-width:83.3333333333%}.col-lg-11{flex:0 0 91.6666666667%;max-width:91.6666666667%}.col-lg-12{flex:0 0 100%;max-width:100%}}@media (min-width:1200px){.col-xl{flex-basis:0;flex-grow:1;max-width:100%}.col-xl-auto{flex:0 0 auto;width:auto;max-width:none}.col-xl-1{flex:0 0 8.3333333333%;max-width:8.3333333333%}.col-xl-2{flex:0 0 16.6666666667%;max-width:16.6666666667%}.col-xl-3{flex:0 0 25%;max-width:25%}.col-xl-4{flex:0 0 33.3333333333%;max-width:33.3333333333%}.col-xl-5{flex:0 0 41.6666666667%;max-width:41.6666666667%}.col-xl-6{flex:0 0 50%;max-width:50%}.col-xl-7{flex:0 0 58.3333333333%;max-width:58.3333333333%}.col-xl-8{flex:0 0 66.6666666667%;max-width:66.6666666667%}.col-xl-9{flex:0 0 75%;max-width:75%}.col-xl-10{flex:0 0 83.3333333333%;max-width:83.3333333333%}.col-xl-11{flex:0 0 91.6666666667%;max-width:91.6666666667%}.col-xl-12{flex:0 0 100%;max-width:100%}}.text-yellow,a.text-yellow{color:#FDB713}@media (max-width:575.98px){.p,p{font-size:12px}.h1,h1{font-size:36px}.h2,h2{font-size:24px}.h3,h3{font-size:18px}.h4,h4{font-size:14px}.h5,h5{font-size:12px}}.p-4{padding:1.5rem}body{-webkit-text-size-adjust:100%;-ms-text-size-adjust:100%;text-size-adjust:100%;color:#37302a;background:#fff;font:400 100%/1.4 sans-serif}div,label,p,span{font-family:Lato,sans-serif;width:auto}article,aside,blockquote,body,dd,div,dl,dt,fieldset,figure,footer,form,h1,h2,h3,h4,h5,h6,header,hgroup,hr,label,legend,li,menu,nav,ol,p,pre,section,summary,td,th,ul{margin:0;padding:0;border:0}.h1,h1{font-size:72px}@media (max-width:575.98px){.p,p{font-size:12px}.urd{font-size:8px}.h1,h1{font-size:36px}.h2,h2{font-size:24px}.h3,h3{font-size:18px}.h4,h4{font-size:14px}.h5,h5{font-size:12px}}.contato,.pagamento{background-color:#f3f3f3;padding:10px}.float-left{float:left}.float-right{float:right}@media (min-width:576px){.col-sm-3{flex:0 0 25%;max-width:25%}.mr-sm-2,.mx-sm-2{margin-right:.5rem}}@media (min-width:768px){.d-md-none{display:none}.d-md-inline{display:inline}.d-md-inline-block{display:inline-block}.d-md-block{display:block}.d-md-table{display:table}.d-md-table-row{display:table-row}.d-md-table-cell{display:table-cell}.d-md-flex{display:flex}.d-md-inline-flex{display:inline-flex}.pt-md-4,.py-md-4{padding-top:1.5rem}.pt-md-5,.py-md-5{padding-top:3rem}}@media (min-width:992px){.col-lg-3{flex:0 0 25%;max-width:25%}}.pr-2,.px-2{padding-right:.5rem}.pl-2,.px-2{padding-left:.5rem}.fa{display:inline-block;font:normal normal normal 14px/1 FontAwesome;font-size:inherit;text-rendering:auto;-webkit-font-smoothing:antialiased;-moz-osx-font-smoothing:grayscale}.fa-5x{font-size:5em}.pt-3,.py-3{padding-top:1rem}.pb-3,.py-3{padding-bottom:1rem}.pt-4,.py-4{padding-top:1.5rem}.pr-4,.px-4{padding-right:1.5rem}.pb-4,.py-4{padding-bottom:1.5rem}.pl-4,.px-4{padding-left:1.5rem}.mr-auto,.mx-auto{margin-right:auto}.mb-auto,.my-auto{margin-bottom:auto}.ml-auto,.mx-auto{margin-left:auto}p{width:90%;margin:0 auto}@media (min-width:768px){.mt-md-0,.my-md-0{margin-top:0}.mb-md-0,.my-md-0{margin-bottom:0}}.btn-b2{font-weight:700;font-size:36px;line-height:65px;min-width:280px;height:80px}@media (max-width:575.98px){.bg-solicitation .container div.jumbo-solicitation .d-sm-inline-block,.btn-b6-xs{font-size:18px}.btn-b6-xs{font-weight:400;line-height:17px;min-width:120px;height:30px}}.mr-2,.mx-2{margin-right:.5rem}.ml-2,.mx-2{margin-left:.5rem}.mr-4{margin-right:1.5rem}.justify-content-end{justify-content:flex-end}.justify-content-center{justify-content:center}.text-right{text-align:right}.menu{background-color:#FDFDFD}.menu-item a{transition:all .1s;padding:20px 10px}@media screen and (max-width:640px){header .menu,header .menu .nav-collapse,header .menu nav{background-color:#f3f3f3}#contacts,header .hidden-xs,header .info{display:none}header .menu{position:fixed;top:auto;z-index:100;width:100%;bottom:0;height:60px}header .menu .nav-link{padding:10px 5px}header .menu-item a{padding:5px}header .menu #cart-header,header .menu .nav-toggle{position:absolute}header .menu #cart-header{right:50px;bottom:0}header .menu .nav-toggle{right:0;bottom:2px}header .menu nav>ul{background:#f3f3f3}header .menu .nav-collapse.opened{top:-275px}header .menu .usr{min-height:0}header .menu .usr.separator{width:30%;float:right;background:0 0;border-bottom:1px solid #fc4a1a}header .logo{width:98px;height:auto;display:block}header #btn-acessar,header #cart-header{padding:15px}}.text-strong{font-weight:700}.text-thin{font-weight:lighter}@media (max-width:1200px){.exibir{display:none}.menu .nav-link{padding:20px}.menu .text-lg{font-size:14px}}@media (max-width:1400px){.menu .text-lg{font-size:16px}.menu .nav-link{padding:20px 10px}.menu .text-sm{font-size:12px}}.default-title{font-family:Economica,sans-serif}.default-text,.subtitle{font-family:Lato,sans-serif}.breadcrumb{background-color:transparent;font-size:15px}.breadcrumb-item+.breadcrumb-item::before{content:">>";font-weight:700}.breadcrumb-item{font-weight:lighter}.breadcrumb-item a{color:#a8a8a8;font-weight:700}.subtitle{font-size:14px}.text-hg{font-size:24px}.btn-b4,h3{font-size:18px}.bg-blue{background-color:#67A4FF}.bg-white{background-color:#FFF}.btn-b4{font-family:Economica,sans-serif;font-weight:700;line-height:17px;min-width:170px;height:50px}.newField{font-size:12px;font-family:Lato,sans-serif;width:auto}@media screen and (max-width:640px){footer,footer .contato{background-color:initial}body{overflow-x:hidden}footer{margin-top:20px;padding:0}footer>.row{margin:0}footer .hidden-xs{display:none}footer .contato{max-width:100vw;margin-left:0;flex:initial}footer .contato .ft-info{padding:15px 0;width:100%}footer .contato .ft-info .pedido{width:50%;padding-left:15px}footer .contato .ft-info .ft-social{width:50%;text-align:right;margin-right:0}footer .contato .ft-info .ft-social p{display:inline;margin-right:6px;vertical-align:super}footer .contato .ft-info .ft-social a{float:none}}.text-md{font-size:16px}.text-sm{font-size:14px}.shadow-sm{margin-left:0}.mb-5{margin-bottom:3rem}.pedido span{margin-bottom:5px}.mt-2{margin-top:.5rem}
    </style>

    <style amp-boilerplate>
        .hiw-container a {
            float: none !important;
        }

        .hiw-container .text-hiw {
            font-size: 20px;
            font-family: "Economica" !important;
        }

        .hiw-container .btn-hiw {
            font-weight: bold;
            font-size: 24px;
            min-width: 220px;
            height: 50px;
        }

        .hiw-container .img-step-by-step {
            position: relative;
        }

        .hiw-container .img-step-by-step .step-number {
            position: absolute;
            top: 10px;
            left: 10px;
            height: 44px;
            width: 44px;
            border-radius: 22px;
            background-color: #fc4a1a;
            color: rgb(255, 255, 255);
            font-size: 28px;
            line-height: 44px;
            font-weight: bold;
        }

        .hiw-container .img-fluid {
            max-width: 100%
        }

        .hiw-container .mb-3 {
            margin-bottom: 1rem;
        }

        @media (min-width: 1200px) {
            .hiw-container .img-step-by-step img {
                max-height: 232px;
            }
        }


        .hiw-container ul {
            float: none;
            list-style-type: none;z
        }

        .hiw-container ul li {
            float: none;
            color: $grey-std;
        }

        .hiw-container .hiw-h1, h3 {
            font-size: 24px;
        }

        @media (min-width: 560px) {
            .hiw-container .hiw-h1, h3 {
                font-size: 36px;
            }
        }
    </style>
</head>

<body>

    <?php
        require_once 'app/views/partial/amp-wsheader.php';
        require_once 'app/views/template/' . $page . '.php';
        require_once 'app/views/partial/amp-wsfooter.php';
    ?>

</body>

</html>
