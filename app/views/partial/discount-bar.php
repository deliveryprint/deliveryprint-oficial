<section class="email-feed text-orange d-none pt-5">
    <div class="bg-lightgrey py-5" id="mail-subscribe">
        <div class="container">
            <div class="row">
                <div class="col-12 col-md-6">
                    <div class="row text-center text-lg-left">
                        <div class="col-12 col-lg-2">
                            <i class="fa fa-envelope fa-4x"></i>
                        </div>
                        <div class="col-12 col-lg-10">
                            <div class="text-hg font-weight-lighter">

                                <span class="d-none d-lg-block">
                                    Receba
                                    <strong>ofertas exclusivas</strong>
                                    <br /> por e-mail.
                                </span>
                                <span class="d-lg-none">
                                    Receba
                                    <strong>ofertas exclusivas</strong>
                                    <br class="d-block d-sm-none">
                                    por e-mail.
                                </span>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-12 col-md-6">
                    <div class="row align-items-end text-center text-lg-left">
                        <div class="col-12 col-lg-7 mt-5 mt-md-0">
                            <label class="font-weight-bold">Digite seu Email
                                <input v-model="userEmail" class="form-control" type="email" />
                            </label>
                        </div>
                        <div class="col-12 col-lg-5 mt-5 mt-lg-0">
                            <button class="btn btn-b3 btn-orange" type="button" @click="subscribeEmail">CADASTRAR</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<section class="bg-orange py-5 text-center row-order mt-0 d-flex flex-wrap solicitation-fix">
    <div class="container">
        <div class="row justify-content-center">
            <div class="d-flex justify-content-md-end justify-content-center col-sm-12 col-md-6 text-right order-2 order-md-1">
                <button class="btn btn-b1 btn-white" id="btn-coupon" value="">FAZER PEDIDO COM DESCONTO</button>
            </div>
            <div class="col-sm-12 col-md-6 text-left order-1 order-md-2 text-center text-md-left">
                <p class="text-white default-text font-weight-lighter text-hg h3">Ganhe
                    <strong id="discount_type">R$10,00 de desconto</strong>
                    <br> na primeira Impressão Online </p>
            </div>
        </div>
    </div>
</section>
