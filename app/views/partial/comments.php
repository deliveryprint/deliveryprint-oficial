<div class="container  mt-3">
    <div class="row pt-3">

        <!-- nw avaliations -->
        <div class="col-12" id="nw_avaliations">
            <h3 class="text-orange text-center pb-3">A primeira impressão é a que fica!</h3>

            <div class="row text-center">

                <div class="col-6 col-md-3  text-yellow offset-md-3">
                    <p class="h1 default-text font-weight-lighter font-italic">5,0</p>
                    <div class="stars ">
                        <i class="fa fa-star"></i>
                        <i class="fa fa-star"></i>
                        <i class="fa fa-star"></i>
                        <i class="fa fa-star"></i>
                        <i class="fa fa-star"></i>
                    </div>
                    <span class="text-sm text-orange font-italic">Classificação no Google</span>
                </div>

                <div class="col-6 col-md-3  text-yellow">
                    <p class="h1 default-text font-weight-lighter font-italic">4,8</p>
                    <div class="stars ">
                        <i class="fa fa-star"></i>
                        <i class="fa fa-star"></i>
                        <i class="fa fa-star"></i>
                        <i class="fa fa-star"></i>
                        <i class="fa fa-star-half-o"></i>
                    </div>
                    <span class="text-sm text-orange font-italic">Classificação no Facebook</span>
                </div>

            </div>
        </div>
        <!-- end avaliations -->

        <!-- comments -->
        <div class="col-12 text-center mb-5" id="customer_comments">

            <div class="row">
                <div class="col-6"></div>
                <div class="col-6"></div>
            </div>

            <div class="row text-grey mt-5">
                <div class="col-12 col-md-4">
                    <p class="font-weight-bold text-orange text-hg pb-4">Thomas Novaes</p>
                    <p class="font-italic text-md">"Muito ágil e de ótima qualidade, fui chamado para receber a
                        encomenda no exato momento em que rastreava
                        o pedido, ainda tinham 2 dias de prazo. Ficou exatamente da forma que queria. Continuem assim,
                        estão
                        de parabéns."</p>
                    <div class="stars text-orange p-4">
                        <i class="fa fa-star"></i>
                        <i class="fa fa-star"></i>
                        <i class="fa fa-star"></i>
                        <i class="fa fa-star"></i>
                        <i class="fa fa-star"></i>
                    </div>

                    </span>
                </div>
                <div class="col-12 col-md-4">
                    <p class="font-weight-bold text-orange text-hg pb-4">Luisa de Sousa</p>
                    <p class="font-italic text-md">"Todas as experiências foram ótimas. Impressão de alta qualidade,
                        entrega rápida, serviço eficiente,
                        super prático e o preço é bom. Estou muito satisfeita. Muito obrigada pelas entregas.
                        #Recomendo!"</p>
                    <div class="stars text-orange p-4">
                        <i class="fa fa-star"></i>
                        <i class="fa fa-star"></i>
                        <i class="fa fa-star"></i>
                        <i class="fa fa-star"></i>
                        <i class="fa fa-star"></i>
                    </div>
                </div>
                <div class="col-12 col-md-4">
                    <p class="font-weight-bold text-orange text-hg pb-4">Adriana Borges</p>
                    <p class="font-italic text-md">"Serviço de excelente qualidade, funcionários prontos para o melhor
                        atendimento ao cliente, só tenho
                        o que agradecer a essa empresa, que me socorreu em uma urgência, e só me surpreendeu em todos
                        os
                        quesitos! Super recomendo."</p>
                    <div class="stars text-orange p-4">
                        <i class="fa fa-star"></i>
                        <i class="fa fa-star"></i>
                        <i class="fa fa-star"></i>
                        <i class="fa fa-star"></i>
                        <i class="fa fa-star"></i>
                    </div>
                </div>
            </div>
        </div>
        <!-- end comments -->
    </div>
</div>
