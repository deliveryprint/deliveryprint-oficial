<template v-if="item.idItem in expanded">
    <tr>
        <td colspan="12">
            <div id="hiddenContentsTb2" class="default-text">
                <div class="hidden-contents" id="details">
                    <div class="ml-5 pl-4 row order-data">
                        <div class="col-sm-12 mb-5 title-options py-5 text-left">

                            <div class="text-hg text-orange">Status Atual:
                                <span class="label text-sm" v-bind:class="getCorrectClass(item.statusItem)">
                                    {{ item.statusItem }}
                                </span>
                            </div>
                            <br/>
                            <!-- DROPDOWN MENU -->
                            <div class="btn-group">
                                <button type="button" class="text-sm btn btn-orange-outlined dropdown-toggle default-text text-orange py-0 pr-5 border-md"
                                    data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    {{curStatus}}
                                </button>
                                <ul class="dropdown-menu">
                                    <li v-for="st in orderStatus">
                                        <a href="javascript:void(0);" v-on:click="setCurStatus(st)">{{st}}</a>
                                    </li>
                                </ul>
                            </div>
                            <!-- END DRPODOWN MENU -->
                            <button class="btn btn-b6 border-radius-md btn-orange-outlined border-md py-0" v-on:click="updateItemStatus(item.idItem, item.tbPedido_idPedido)">ALTERAR</button>
                        </div>
                        <!-- ORDER COMPLETE INFO -->
                        <div class="col-sm-3 text-left single-order">
                            <!-- DELIVERY DATA -->
                            <div class="row text-orange text-sm font-weight-lighter">
                                <h5 class="default-text text-orange col-sm-12 mb-5">
                                    <i class="fa fa-map fa-2x mr-2"></i> Dados da {{item.details.idRetirada ? 'Retirada': 'Entrega'}}
                                </h5>
                                <div class="col-sm-12 mb-2" v-if="!item.details.idRetirada">
                                    <span class="font-weight-bold mb-1">Endereço:</span>
                                    <span>
                                        {{item.details.logradouro}}, {{item.details.numero}}, {{item.details.complemento}}
                                        <br> CEP: {{item.details.cep}}
                                        <br>{{item.details.bairro}}, {{item.details.cidade}} - {{item.details.uf}}
                                    </span>
                                </div>
                                <div class="col-sm-12">
                                    <span class="font-weight-bold mb-1">Entregar para:</span>
                                    <span class="">{{item.details.nomeEntrega}}</span>
                                    <br> {{item.details.telEntrega}}
                                    <br> {{item.details.celCliente}}
                                </div>
                            </div>
                            <!-- CLIENT DATA -->
                            <div class="row">
                                <h5 class="default-text text-orange mb-4 col-sm-12 mb-5">
                                    <i class="fa fa-user fa-2x mr-2"></i>Dados do Cliente</h5>
                                <div class="col-sm-12 text-grey">
                                    <span class="">
                                        <strong>Cliente:</strong> {{item.details.nomeCliente}}
                                    </span>
                                    <br/>
                                    <br/>
                                    <span class="text-orange">
                                        <strong class="text-grey">E-mail:</strong>
                                        {{item.details.emailCliente}}
                                    </span>
                                    <br/>
                                    <br/>
                                    <span>
                                        <strong>Telefone:</strong> {{item.details.telCliente}} {{item.details.celCliente}}
                                    </span>
                                    <br/>
                                    <br/>
                                    <span>
                                        <strong>Endereço:</strong>
                                        {{item.details.logradouro}}, {{item.details.numero}}, {{item.details.complemento}}
                                        <br> CEP: {{item.details.cep}}
                                        <br>{{item.details.bairro}}, {{item.details.cidade}} - {{item.details.uf}}
                                    </span>
                                </div>
                                <!-- CLIENT NOTES -->
                            </div>
                            <div class="row">
                                <h5 class="default-text text-orange mb-4 col-sm-12 mb-5">
                                    <i class="fa fa-pencil-square-o fa-2x mr-2"></i>Observação do Cliente</h5>
                                <div class="col-sm-12">
                                    <span class=" text-grey">{{item.details.obsCliente}}</span>
                                </div>
                            </div>
                            <!-- ORDER DETAILS -->
                            <div class="row border-0">
                                <h5 class="default-text text-orange mb-4 col-sm-12 mb-5">
                                    <i class="fa fa-bars fa-2x mr-2"></i>
                                    Item #{{item.pretty_id}}
                                </h5>
                                <div class="col-sm-12 text-grey">
                                    Cor
                                    <span class="text-orange ml-3"> {{item.details.descricaoCor}}</span>
                                    <br/> Lado
                                    <span class="text-orange ml-2"> {{item.details.descricaoLado}}</span>
                                    <br/> Papel
                                    <span class="text-orange ml-1"> {{item.details.descricaoPapel}}</span>
                                    <br/> Acabamento
                                    <span class="text-orange ml-2"> {{item.details.descricaoAcabamento}}</span>
                                    <br/> Encadernações
                                    <span class="text-orange ml-2"> {{item.details.qdeAcabamentos}}</span>
                                    <br/> Borda
                                    <span class="text-orange ml-2"> {{item.details.borda == 'Horizontal' ? 'Lado Menor' : 'Lado Maior'}}</span>
                                    <br/> N.º de Páginas
                                    <span class="text-orange"> {{item.num_folhas}}</span>
                                    <br/>
                                    <br/>
                                </div>
                                <div class="col-sm-12 text-grey" v-if="item.tbDetalhesAcabamento_idDetalhes">
                                    <b class="text-orange"> Capa Dura</b>
                                    <br/>Cor da Capa
                                    <span class="text-orange ml-3"> {{item.details.corDaCapa}}</span>
                                    <br/> Cor da Letra do hot stamping
                                    <span class="text-orange ml-2"> {{item.details.corDaLetra}}</span>
                                    <br/> Texto da Capa
                                    <span class="text-orange ml-1"> {{item.details.textoCapa}}</span>
                                    <br/> Texto da Lombada
                                    <span class="text-orange ml-2"> {{item.details.textoLombada}}</span>
                                    <br/>
                                    <br/>
                                </div>
                                <span class="text-orange text-lg">Valor R$ {{item.details.valor_final | moneyFormatter}}</span>
                                <br/>
                            </div>
                        </div>
                        <!-- FILES TABLE -->
                        <div class="col-sm-6 mt-4 pl-5">
                            <table class="table text-center bg-transparent inset-table">
                                <thead class="text-orange">
                                    <tr>
                                        <th></th>
                                        <th> OK</th>
                                        <th>Enc.</th>
                                        <th>Qtd.</th>
                                        <th class="text-left">Arquivos</th>
                                        <th class="text-right">
                                            <a :href="item.details.downloadAllUrl" target="_blank">
                                                <i class="fa fa-download"></i>Baixar todos
                                            </a>
                                        </th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr v-for="(f, index) in item.details.files" :key="index" v-on:click="viewPdf(f)" v-bind:class="{active: curFileId == f.idArquivo}">
                                        <td>{{index + 1}}</td>
                                        <td>
                                            <label class="checkbox-btn check-blue">
                                                <input type="checkbox" v-model="f.is_ok" v-on:change="changeArquivoOkStatus(f)">
                                                <span class="checkmark" style="margin-left: -5px"></span>
                                            </label>
                                        </td>
                                        <td>0</td>
                                        <td>{{f.qtdImpressoes}}</td>
                                        <td class="text-left highlighted text-truncate" style="max-width: 240px;">
                                            {{index + 1}}
                                            <i class="fa fa-file-pdf-o text-orange mx-2"></i>
                                            {{f.nomeArquivo}}
                                        </td>
                                        <td class="text-orange text-right highlighted">
                                            <a :href="f.downloadUrl" target="_blank">
                                                <i class="fa fa-download text-hg"></i>
                                            </a>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                        <div class="col-sm-3">
                            <div class="pdf-box text-truncate">
                                <iframe v-bind:src="curFileUrl"></iframe>
                                <br/>
                                <span class="text-grey font-weight-lighter px-3">{{curFileName}}</span>
                                <br/>
                                <span class="text-grey font-weight-lighter px-3">{{curFileSize | formatBytes}}</span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </td>
    </tr>
</template>
