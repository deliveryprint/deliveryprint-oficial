#!/bin/sh

executeCommand () {
    eval $1
    if [ $? -eq 0 ]; then
        echo "$1 succeeded"
    else
        exit 1
    fi
}

executeCommand "git pull"
executeCommand "composer update"
executeCommand "composer dbpatch-update"
executeCommand "npm update"
executeCommand "pm2 restart mailgun_worker_dev"
executeCommand "grunt build"
