<?php
if($argc < 2) {
    throw new \InvalidArgumentException("Necessário informar o nome do arquivo .CSV");
}

$fileName = basename($argv[1]);
$filePath = __DIR__ . '/../data/csv/' . $fileName;

$newPatch = fopen($filePath . date('Y-m-d H:i:s') . ".sql", "w") or die("Não foi possível criar um novo patch!");
if (($handle = fopen($filePath, "r")) !== false) {
    // Lendo até a primeira linha de valores
    for($row = 1; $row < 3; $row++) {
        $data = fgetcsv($handle, 1000, ';');
    }

    fwrite($newPatch, "start transaction;\n\n");

    fwrite($newPatch, "delete from tbfaixa;\n\n");
    fwrite($newPatch, "alter table tbfaixa auto_increment = 1;\n\n");
    fwrite($newPatch, "INSERT INTO tbfaixa VALUES\n");
    fwrite($newPatch, "(" . $data[0] . ", " . $data[1] . ", " . $data[2] . ", " . $data[3] . ", " . $data[4] . ", " . $data[5]  . ")");
    while (($data = fgetcsv($handle, 1000, ";")) !== false) {
        fwrite($newPatch, ",\n(" . $data[0] . ", " . $data[1] . ", " . $data[2] . ", " . $data[3] . ", " . $data[4] . ", " . $data[5]  . ")");
    }
    fclose($handle);
}

fwrite($newPatch, ";\n\ncommit;");

fwrite($newPatch, "\n");
fclose($newPatch);
