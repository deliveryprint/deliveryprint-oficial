<?php

namespace IntecPhp\Validator;

/* estrutura do config
	[
		elName => [
			validators => [
				ClassValidator => config,
				...
			]
		],
		...
	]
*/

class InputValidator {
	private $data;
	private $errors;
    private $config;
    private $generalErrorMessage;

	function __construct($config) {
		$this->config = $config;
		$this->errors = [];
        $this->data = [];
        $this->generalErrorMessage = '';
	}

	public function setData($data) {
		$this->data = $data;
	}

	public function getErrorsMessages() {
		return $this->errors;
	}

	public function isValid() {

        $isValid = true;
        $notSetFields = [];

		foreach($this->config as $elName => $elConfig) {
            if (!array_key_exists("validators", $elConfig)) continue;

            if(!isset($this->data[$elName])) {

               if(empty($elConfig["optional"])) {
                   $notSetFields[] = $elName;
                   $isValid = false;
               }
           } else {

                if(!empty($elConfig["context"])) {
                    foreach($elConfig["context"] as $field => $contextValue) {

                        if(!isset($this->data[$field])) {
                            continue 2;
                        }

                        $v = $this->data[$field];

                        if(is_array($contextValue)) {
                            if(!in_array($v, $contextValue)) {
                                continue 2;
                            }
                        } else {
                            if($v != $contextValue) {
                                continue 2;
                            }
                        }
                    }
                }

                foreach($elConfig["validators"] as $validatorName => $validatorConfig ) {
                    $classPath = __NAMESPACE__ . "\\".$validatorName;

                    $validator = new $classPath($this->data[$elName] ?? null, $validatorConfig);

                    // validação inversa ou não
                    $inverse = $validatorConfig['inverse'] ?? false;

                    if (!$validator->isValid($inverse)) {
                        $this->generalErrorMessage = $validator->getMessage();
                        $this->errors[$elName] = $validator->getMessage();
                        $isValid = false;
                        break;
                    }
                }
            }
        }

        if($notSetFields) {
            $this->generalErrorMessage = 'Campos não preenchidos: ' . implode(', ', $notSetFields);
            return false;
        }

        if(!$isValid && !$this->generalErrorMessage) {
            $this->generalErrorMessage = 'Existem campos incorretos ou não preenchidos';
        }

        return $isValid;
    }

    public function getGeneralErrorMessage()
    {
        return $this->generalErrorMessage;
    }
}
