<?php

namespace IntecPhp\Middleware;

use Intec\Session\Session;
use IntecPhp\View\Layout;
use IntecPhp\Model\ResponseHandler;

class AuthenticationMiddleware
{
    private $layout;
    private $isLoggedIn;
    private $isAdmin;

    public function __construct(Layout $layout, bool $isLoggedIn, bool $isAdmin)
    {
        $this->layout = $layout;
        $this->isLoggedIn = $isLoggedIn;
        $this->isAdmin = $isAdmin;
    }

    public function isAuthenticated($request)
    {
        if(!$this->isLoggedIn) {
            if(!$request->isXmlHttpRequest()) {
                $this->layout
                    ->setLayout('layout-error')
                    ->render('http-error/403');
            } else {
                $rp = new ResponseHandler(403, 'Você não tem permissão para acessar este recurso');
                $rp->printJson();
            }
            exit;
        }
    }

    public function isAdminAuthenticated($request)
    {
        if(!$this->isAdmin) {
            if(!$request->isXmlHttpRequest()) {
                $this->layout
                    ->setLayout('layout-error')
                    ->render('http-error/403');
            } else {
                $rp = new ResponseHandler(403, 'Você não tem permissão para acessar este recurso');
                $rp->printJson();
            }
            exit;
        }
    }
}
