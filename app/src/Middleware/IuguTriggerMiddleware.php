<?php

namespace IntecPhp\Middleware;

class IuguTriggerMiddleware
{
    private $authToken;

    public function __construct(string $authToken)
    {
        $this->authToken = $authToken;
    }

    public function __invoke()
    {
        $headers = getallheaders();
        if (empty($headers['Authorization']) || $headers['Authorization'] != $this->authToken) {
            exit;
        }
    }
}

if (!function_exists('getallheaders')) {
    function getallheaders()
    {
        if (!is_array($_SERVER)) {
            return array();
        }

        $headers = array();
        foreach ($_SERVER as $name => $value) {
            if (substr($name, 0, 5) == 'HTTP_') {
                $headers[str_replace(' ', '-', ucwords(strtolower(str_replace('_', ' ', substr($name, 5)))))] = $value;
            }
        }
        return $headers;
    }
}
