<?php

namespace IntecPhp\View;


use IntecPhp\Model\Config;
use Exception;

/**
 * Description of Layout
 *
 * @author intec
 */
class Layout
{

    private $stylesheets;
    private $scripts;

    private $title = '';
    private $metaDescription = '';
    private $metaRobots = '';

    private $layout;
    private $renderLayout = true;
    private $appendPartial = [];

    private $canonical;
    private $amp;

    const DEFAULT_LAYOUT = 'layout';

    public function __construct($layoutName = self::DEFAULT_LAYOUT, $stylesheets = [], $scripts = [])
    {
        $this->stylesheets = $stylesheets;
        $this->scripts = $scripts;
        $this->layout = self::DEFAULT_LAYOUT;
        $this->title = 'DeliveryPrint';
        $this->metaDescription = 'Impressão e Encadernação barato com Alta Qualidade é na DeliveryPrint. Orçamento em poucos segundos, pagamento facilitado e frete para todo país. Tudo Online! Somos um modelo Inovador de Serviço de Impressão com Preços Acessíveis.';
    }

    public function setLayout($layout)
    {
        $this->layout = $layout;
        return $this;
    }

    public function setRenderLayout($renderLayout)
    {
        $this->renderLayout = $renderLayout;
        return $this;
    }

    public function setTitle($title)
    {
        $this->title = $title;
        return $this;
    }

    public function appendTitle($text, $separator = ' - ')
    {
        $this->title .= $separator . $text;
        return $this;
    }

    public function setMetaKeywords($keywords)
    {
        $this->metaKeywords = $keywords;
        return $this;
    }

    public function setMetaDescription($description)
    {
        $this->metaDescription = $description;
        return $this;
    }

    public function setMetaOgDataArray($ogData)
    {
        $this->metaOgDataArray = $ogData;
        return $this;
    }

    public function render($page, array $resp = [])
    {
        $this->contentId = $page;
        extract($resp);
        if ($this->renderLayout) {
            include_once 'app/views/partial/layout/' . $this->layout . '.php';
        } else {
            include_once 'app/views/template/' . $page . '.php';
        }
    }

    public function addScript($path)
    {
        $this->scripts[] = $path;
        return $this;
    }

    public function addStylesheet($href)
    {
        $this->stylesheets[] = $href;
        return $this;
    }

    public function appendPartial($partial)
    {
        if(file_exists('app/views/partial/'. $partial .'.php')) {
            $this->appendPartial[] = $partial;
        } else {
            throw new Exception('Partial \'' . $partial . '\' não encontrado');
        }
        return $this;
    }

    public function setCanonical($canonical)
    {
        $this->canonical = $canonical;
        return $this;
    }

    public function setAmp($amp)
    {
        $this->amp = $amp;
        return $this;
    }

    public function setMetaRobots($robots)
    {
        $this->metaRobots = $robots;
        return $this;
    }
}
