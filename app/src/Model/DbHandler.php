<?php


namespace IntecPhp\Model;

use PDO;
use PDOException;

/**
 * Faz a manipulação do banco de dados da aplicação
 *
 * @author intec
 */
class DbHandler
{
    private $conn;

    // Construtor privado: só a própria classe pode invocá-lo
    public function __construct(PDO $conn)
    {
        $this->conn = $conn;
    }

    public function beginTransaction()
    {
        return $this->conn->beginTransaction();
    }

    public function rollBack()
    {
        return $this->conn->rollBack();
    }

    public function commit()
    {
        return $this->conn->commit();
    }

    public function query($queryString, $params = [])
    {
        try {
            $sth = $this->conn->prepare($queryString);
            $sth->execute($params);
            return $sth;
        } catch (PDOException $e) {
            if ($this->conn->inTransaction()) {
                $this->conn->rollBack();
            }
            error_log($e->getMessage());
        }
    }

    public function prepare($queryString, array $params)
    {
        try {
            $sth = $this->conn->prepare($queryString);
            $sth->execute($params);
            return $sth;
        } catch (PDOException $e) {
            if ($this->conn->inTransaction()) {
                $this->conn->rollBack();
            }
            error_log($e->getMessage());
        }
    }

    public function lastInsertId()
    {
        return $this->conn->lastInsertId();
    }

    public function inTransaction()
    {
        return $this->conn->inTransaction();
    }

    private function __clone()
    {
    }

    private function __wakeup()
    {
    }
}
