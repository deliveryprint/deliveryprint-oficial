<?php

namespace IntecPhp\Model;

use Google\Cloud\Storage\StorageClient;
use ZipArchive;
use Imagick;
use Exception;

class PDF
{
    const ERR_DIRECTORY_NOT_CREATED = -1;
    const ERR_UPLOAD_FILE = -2;

    private $storageType;
    private $gCloudBucket;
    private $gCloudFolder;
    private $localStorageFolder;
    private $downloadFileUrl;
    private $storage;
    private $bucket;

    private $document;
    private $attr = null;
    private $hash;

    private $name = null;
    private $tmp = null;
    private $type = null;
    private $errorCode = UPLOAD_ERR_OK;
    private $size = null;

    public function __construct(array $storageConfig)
    {
        $this->storageType = $storageConfig['type'];
        $this->gCloudBucket = $storageConfig['gCloudBucket'];
        $this->gCloudFolder = $storageConfig['gCloudFolder'];
        $this->localStorageFolder = $storageConfig['localFolder'];
        $this->downloadFileUrl = $storageConfig['download'];

        putenv("GOOGLE_APPLICATION_CREDENTIALS=./storage-key.json");

        $this->storage = new StorageClient();
        $this->bucket = $this->storage->bucket($this->gCloudBucket);
    }

    public function read()
    {
        $this->document = new Imagick($this->localStorageFolder . "/" . $this->hash);
    }

    public function setAttributes($attr)
    {
        $this->attr = $attr;
    }

    public function setName($name)
    {
        $this->name = $name;
    }
    public function setTmp($tmp)
    {
        $this->tmp = $tmp;
    }
    public function setType($type)
    {
        $this->type = $type;
    }

    public function setErrorCode($code)
    {
        $this->errorCode = $code;
    }

    public function setSize($size)
    {
        $this->size = $size;
    }

    public function isValid()
    {
        if ($this->type == null) {
            throw new Exception("Erro na inicialização", 112);
        }

        return $this->type == "application/pdf" ? true : false;
    }

    public function getNumberOfPages()
    {
        return $this->document->getNumberImages();
    }

    public function hasError()
    {
        if ($this->errorCode == UPLOAD_ERR_OK) {
            return false;
        }

        return true;
    }

    public function throwError()
    {
        switch ($this->errorCode) {
            case UPLOAD_ERR_OK:
                break;
            case UPLOAD_ERR_NO_FILE:
                throw new Exception('Arquivo não foi enviado.');
            case UPLOAD_ERR_INI_SIZE:
            case UPLOAD_ERR_FORM_SIZE:
                throw new Exception('O tamanho do arquivo excede o limite máximo permitido.');
            default:
                throw new Exception('Erro inesperado.');
        }
    }

    public function save()
    {
        if ($this->hasError()) {
            $this->throwError();
        }

        if (!$this->isValid()) {
            throw new Exception("O Arquivo deve ser do tipo \"PDF\"", 113);
        }

        $hash = hash('sha256', uniqid());

        if ($this->storageType == 'local') {

            // Armazenamento local
            if (move_uploaded_file($this->tmp, $this->getFilePath($hash))) {
                $this->hash = $hash;
                return $hash;
            }
            return self::ERR_UPLOAD_FILE;
        } else if ($this->storageType == 'gcs') {

            // Armazenamento no Google Cloud
            $objectName = $this->gCloudFolder . "/" . $hash . ".pdf";

            try {
                $file = fopen($this->tmp, 'r');
                $object = $this->bucket->upload($file, [
                    'name' => $objectName
                ]);

                $this->hash = $hash;

                return $hash;
            } catch (Exception $e) {
                return self::ERR_UPLOAD_FILE;
            }
        } else {
            return self::ERR_UPLOAD_FILE;
        }
    }

    public function fileExists($fileName)
    {
        return file_exists($this->getFilePath($fileName));
    }

    public function readFile($fileName)
    {
        return readfile($this->getFilePath($fileName));
    }

    public function removeFile($fileName)
    {
        if ($this->fileExists($fileName)) {
            unlink($this->getFilePath($fileName));
        }
    }

    public function getAllFilesNames()
    {
        return preg_grep('/^([^.])/', scandir($this->localStorageFolder));
    }

    public function isFileOlderThan(string $date, $fileName)
    {
        $strFileDate = date("Y-m-d", filemtime($this->getFilePath($fileName)));
        $oldDate  = new \DateTime($date);
        $fileDate = new \DateTime($strFileDate);

        return ($fileDate < $oldDate);
    }

    public function getFileUrl($fileName)
    {
        try {
            if ($this->fileExists($fileName)) {
                $url = $this->downloadFileUrl . $fileName;
            } else {
                $url = $this->bucket->object($this->getGcloudObjectName($fileName))->signedUrl(new \DateTime('15 min'));
            }

            return $url;
        } catch (Exception $e) {
            return false;
        }
    }

    public function getFilePath($fileName)
    {
        return $this->localStorageFolder . '/' . $fileName;
    }

    public function getGCloudObjectName($fileName)
    {
        return $this->gCloudFolder . '/' . $fileName . '.pdf';
    }

    public function createZipFile($zipName, $files)
    {
        $zip = new ZipArchive();
        $tmpFile = '/tmp/' . uniqid() . '.zip';

        if ($zip->open($tmpFile, ZipArchive::CREATE)) {
            foreach ($files as $idx => $file) {
                // Testar se o arquivo existe no servidor. Se não, fazer donwload do Gcloud
                if ($this->fileExists($file['arquivo'])) {
                    $zip->addFile($this->getFilePath($file['arquivo']), $file['nomeArquivo'] . $idx . '.pdf');
                } else {
                    $tmpPath = '/tmp/' . $file['arquivo'];
                    $this->bucket->object($this->getGcloudObjectName($file['arquivo']))->downloadToFile($tmpPath);
                    $zip->addFile($tmpPath, $file['nomeArquivo'] . $idx . '.pdf');
                }
            }

            $zip->close();
            return $tmpFile;
        }

        return false;
    }
}
