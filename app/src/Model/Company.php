<?php

namespace IntecPhp\Model;

use IntecPhp\Entity\TbEmpresa;

use Exception;

class Company
{
    private $tbEmpresa;

    public function __construct(TbEmpresa $tbEmpresa)
    {
        $this->tbEmpresa = $tbEmpresa;
    }

    public function add($name, $phone, $email)
    {
        return $this->tbEmpresa->add($name, $phone, $email);
    }
}
