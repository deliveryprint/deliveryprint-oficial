<?php

namespace IntecPhp\Model;

use IntecPhp\Entity\TbFeriado;

class BusinessDay
{
    private $holiday;

    public function __construct(TbFeriado $holiday)
    {
        $this->holiday = $holiday;
    }

    public function isBusinessDay(\DateTime $day)
    {

        if($this->isSaturdayOrSunday($day)) {
            return false;
        }

        if($this->holiday->hasDate($day->format('Y-m-d'))) {
            return false;
        }

        return true;
    }

    private function isSaturdayOrSunday(\DateTime $day)
    {
        return $day->format('N') > 5;
    }

    public function nextBusinessDay(\DateTime $day)
    {
        $d = clone $day;

        do {
            $d->add(new \DateInterval('P1D'));
        } while(!$this->isBusinessDay($d));

        return $d;
    }

    public function nextSaturday(\DateTime $day)
    {
        $d = clone $day;

        $d->modify('next saturday');

        return $d;
    }

}
