<?php

namespace IntecPhp\Model;

use IntecPhp\Service\Session;

class Account
{
    public static function setOrder($sessionOrderId)
    {
        Session::set("idPedido", $sessionOrderId);
    }

    public static function getOrder()
    {
        return Session::get('idPedido');
    }

    public static function unsetOrder()
    {
        Session::unset('idPedido');
        Session::unset('idPagamento');
    }

    public static function getPayment()
    {
        return Session::get('idPagamento');
    }

    public static function setPayment($paymentId)
    {
        Session::set('idPagamento', $paymentId);
    }
}
