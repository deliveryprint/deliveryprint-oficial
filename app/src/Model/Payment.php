<?php

namespace IntecPhp\Model;

use IntecPhp\Service\PaymentService;
use IntecPhp\Entity\TbCliente;
use IntecPhp\Entity\TbPedido;
use IntecPhp\Model\Item;
use DateTime;
use DateInterval;
use Exception;

class Payment
{
    const LR_SUCCESS = ['00', '11'];
    const INTEREST_RATE = 0.023;

    private $payService;
    private $item;
    private $tbCliente;
    private $tbPedido;

    public function __construct(PaymentService $payService, Item $item, TbCliente $tbCliente, TbPedido $tbPedido)
    {
        $this->payService = $payService;
        $this->item       = $item;
        $this->tbCliente  = $tbCliente;
        $this->tbPedido   = $tbPedido;
    }

    public function makeCreditCardPayment($orderId, $paymentToken, $finalValue, $installments, $userInfo)
    {
        $data = $this->payService->generateCreditCardCharge($paymentToken, $finalValue, $installments, $userInfo);
        $this->tbPedido->beginTransaction();
        if ($data) {
            if (!in_array($data['LR'], self::LR_SUCCESS)) {
                throw new Exception('Erro ao Processar transação, verifique os dados do cartão');
            }
            $this->tbPedido->setIuguId($orderId, $data['invoice_id'], TbPedido::STATUS_ACCOMPLISHED, TbPedido::STATUS_IUGU_PAID, $this->getCurrentDatetime());
            $this->item->updateStatusItemsOfOrder($orderId, TbPedido::STATUS_ACCOMPLISHED);
        }
        $this->tbPedido->commit();

        return $data;
    }

    public function makeBankSlipPayment($orderId, $finalValue, $userInfo)
    {
        $dueDate = $this->generateDueDateFromCurDate(2);
        $data    = $this->payService->generateBankSlip($dueDate, $finalValue, $userInfo);
        $this->tbPedido->beginTransaction();
        if ($data) {
            $this->tbPedido->setIuguId($orderId, $data['id'], TbPedido::STATUS_PENDING, TbPedido::STATUS_IUGU_PENDING);
            $this->item->updateStatusItemsOfOrder($orderId, TbPedido::STATUS_PENDING);
        }
        $this->tbPedido->commit();

        return $data;
    }

    public function makePromotionPayment($orderId, $finalValue, $userInfo)
    {
        $curDate = new DateTime();
        $paymentDate = $curDate->format('Y-m-d');

        $this->tbPedido->beginTransaction();
        $this->tbPedido->setPromotionOrder($orderId, TbPedido::STATUS_ACCOMPLISHED, TbPedido::STATUS_IUGU_PAID, $paymentDate);
        $this->item->updateStatusItemsOfOrder($orderId, TbPedido::STATUS_ACCOMPLISHED);
        $this->tbPedido->commit();

        return [
            "total" => $finalValue
        ];
    }

    // Recebe a quantidade de dias que leva para o boleto expirar a partir da data atual
    private function generateDueDateFromCurDate($daysToExpire = null)
    {
        $curDate = new DateTime();
        $curDate->setTime(0, 0);
        if ($daysToExpire) {
            $curDate->add(new DateInterval('P' . $daysToExpire . 'D'));
        }
        $dueDate = $curDate->format('Y-m-d');

        return $dueDate;
    }

    private function getCurrentDatetime()
    {
        return date("Y-m-d H:i:s");
    }

    public function updateOrderStatusAfterPayment($iuguId)
    {
        $paid = true;
        $this->tbPedido->beginTransaction();
        $order = $this->tbPedido->getByIuguId($iuguId);
        if ($order['statusPagamento'] != TbPedido::STATUS_IUGU_PAID) {
            $paid = ($this->tbPedido->updateStatusByIuguId($iuguId, TbPedido::STATUS_IUGU_PAID, $this->getCurrentDatetime()) > 0);
            // $this->item->updateStatusItemsOfOrder($order['idPedido'], TbPedido::STATUS_ACCOMPLISHED);
            $this->item->registerUsedCoupons($order['idPedido'], TbPedido::STATUS_ACCOMPLISHED);
        }
        $this->tbPedido->commit();
        return $paid;
    }

    public function getInvoice($orderId)
    {
        $order = $this->tbPedido->getAllInfoById($orderId);
        if ($order && !empty($order['idIuguPagamento'])) {
            return $this->payService->fetchInvoice($order['idIuguPagamento']);
        }

        return false;
    }

    public function paySelectedOrder($orderId)
    {
        $order = $this->tbPedido->getAllInfoById($orderId);
        if ($order && ($order['statusPedido'] === TbPedido::STATUS_CART || $order['statusPedido'] === TbPedido::STATUS_PENDING)) {
            $cardBanner = !empty($order['cardBanner']);
            if (!empty($order['nomeEntrega'])) {
                $url = "/segundo-pagamento";
            } else {
                $url = "/carrinho";
            }
            return [
                'isSetIuguId' => false,
                'cardBanner'  => $cardBanner,
                'url'         => $url
            ];
        }

        return false;
    }

    public function getInfoCliente($id)
    {
        $p = $this->tbPedido->getAllInfoById($id);
        $u = $this->tbCliente->get($p["tbCliente_idCliente"]);

        return $u;
    }

    public function saveBasicCardInfo($orderId, $cardBanner, $endCardNumber)
    {
        return ($this->tbPedido->updateCardInfo($orderId, $cardBanner, $endCardNumber) > 0);
    }

    public function getFinalValue($orderId)
    {
        return $this->item->formattedFinalValue($orderId);
    }

    public function calcValueWithInstallments($orderId, $installments)
    {
        $finalValue = $this->item->calcPriceOfOrder($orderId);

        if (!$this->validateInstallments($finalValue, $installments)) {
            throw new Exception('O número de parcelas informado é inválido');
        }

        if ($installments > 1) {
            $finalValue = $finalValue * (self::INTEREST_RATE + 1);
        }

        $roundedValue = round($finalValue, 2);

        return intval($roundedValue * 100);
    }

    private function validateInstallments($finalValue, $installments)
    {
        if ($installments < 1 || $installments > 3) {
            return false;
        }

        if ($finalValue < 100.00) {
            return ($installments < 2);
        }

        return true;
    }

    public function getCommissionValuesByOrderId($orderId)
    {
        return $this->item->getCommissionData($orderId);
    }

    public function getCommissionValuesByIuguId($iuguId)
    {
        $order = $this->tbPedido->getByIuguId($iuguId);

        return $this->item->getCommissionData($order['idPedido']);
    }

    public function getFormattedECommerceInfo(int $orderId)
    {
        return $this->item->calcOrderAndDeliveryValueByOrder($orderId);
    }
}
