<?php

namespace IntecPhp\Model;

use Exception;
use IntecPhp\Entity\TbItem;
use IntecPhp\Entity\TbArquivo;
use IntecPhp\Entity\TbCupom;
use IntecPhp\Entity\TbPedido;

class Item
{
    const PAPER_TYPE_1_DESC = 'Papel Sulfite A4 75g';
    const PAPER_TYPE_2_DESC = 'Papel Sulfite A4 90g';
    const PAPER_TYPE_3_DESC = 'Papel Glossy A4 135g';
    const PAPER_TYPE_4_DESC = 'Papel Glossy A4 220g';
    const PAPER_TYPE_5_DESC = 'Papel Ofício 75g';
    const PAPER_TYPE_6_DESC = 'Papel Eco A4 75g';
    const PAPER_TYPE_7_DESC = 'Papel Carta 75g';
    const PAPER_TYPE_8_DESC = 'Papel Sulfite A3 75g';
    const PAPER_TYPE_9_DESC = 'Papel Couché A3 150g';
    const PAPER_TYPE_10_DESC = 'Papel Matte A4 230g';

    const FINISHING_TYPE_1_DESC = 'Folha Solta';
    const FINISHING_TYPE_2_DESC = 'Grampeado';
    const FINISHING_TYPE_3_DESC = 'Encadernar Espiral';
    const FINISHING_TYPE_4_DESC = 'Encadernar Capa Dura A4';
    const FINISHING_TYPE_5_DESC = 'Encadernar Capa Dura A3';
    const FINISHING_TYPE_6_DESC = 'Encadernar Wire-o';

    private $itemEnt;
    private $fileEnt;
    private $couponEnt;
    private $orderEnt;

    public function __construct(TbItem $itemEnt, TbArquivo $fileEnt, TbCupom $coupon, TbPedido $tbPedido)
    {
        $this->itemEnt = $itemEnt;
        $this->fileEnt = $fileEnt;
        $this->couponEnt = $coupon;
        $this->orderEnt = $tbPedido;
    }

    public function itemIsOfOrder($orderId, $itemId)
    {
        $res = $this->itemEnt->isOfOrder($orderId, $itemId);
        if ($res === false) {
            throw new Exception('Não foi possível consultar o item');
        }

        return $res;
    }

    public function deleteItem($itemId)
    {
        $this->fileEnt->deleteArquivoItemId($itemId);
        $res = $this->itemEnt->deleteItem($itemId);
        if ($res === false) {
            throw new Exception("Não foi possível remover o item");
        }

        return true;
    }

    public function updateItemObservation($itemId, $observation)
    {
        return $this->itemEnt->updateUserObservation($itemId, $observation);
    }

    public function getItemObservation($itemId)
    {
        $data = $this->itemEnt->get($itemId);

        if ($data === false) {
            throw new Exception("Não foi possível encontrar a observação");
        }

        return [
            "observation" => $data['obsCliente'],
            "date" => $data['obsMomentoSalvamento']
        ];
    }

    private function calcDiscount(array $order, float $value)
    {
        if ($order['discountType'] === TbCupom::TYPE_PERCENT) {
            return (float) ($value * ($order['discountValue'] / 100));
        }

        if ($order['discountValue'] > 7) {
            return (float) min($value * 0.5, $order['discountValue']);
        }

        return (float) $order['discountValue'];
    }

    public function calcPriceOfOrder($orderId)
    {
        $totalValueWithoutDiscount = $this->itemEnt->calcTotalValueByOrder($orderId);
        $order = $this->orderEnt->getById($orderId);

        $discount = $this->calcDiscount($order[0], $totalValueWithoutDiscount);

        $vfinal = round($totalValueWithoutDiscount - $discount, 2);
        if ($vfinal <= 0) $vfinal = 0.0;

        return $vfinal;
    }

    public function formattedFinalValue($orderId)
    {
        $finalValue   = $this->calcPriceOfOrder($orderId);
        $roundedValue = round($finalValue, 2);

        return intval($roundedValue * 100);
    }

    public function getAllStartingInRealized(
        $prettyId,
        $nameOrEmail,
        $freights,
        $withdrawals,
        $paymentStatus,
        $itemStatus,
        $orderPeriod,
        $deliveryPeriod
    ) {
        $items = $this->itemEnt->findNotInCart($prettyId, $nameOrEmail, $paymentStatus, $itemStatus, $orderPeriod);

        $filteredItems = [];

        foreach ($items as &$it) {
            $it['delivery'] = $this->itemEnt->getDeliveryInfo($it['idItem']);

            if ($deliveryPeriod) {
                if ($it['delivery']['prazo_retirada']) {
                    $deliveryDate = \DateTime::createFromFormat('Y-m-d H:i:s', $it['delivery']['prazo_retirada']);
                }

                if ($it['delivery']['prazo_frete']) {
                    $deliveryDate = \DateTime::createFromFormat('Y-m-d H:i:s', $it['delivery']['prazo_frete']);
                }

                if ($deliveryPeriod['from']) {
                    $fromDate = \DateTime::createFromFormat('Y-m-d', $deliveryPeriod['from']);
                    if ($fromDate > $deliveryDate) {
                        continue;
                    }
                }

                if ($deliveryPeriod['to']) {
                    $toDate = \DateTime::createFromFormat('Y-m-d', $deliveryPeriod['to']);
                    if ($toDate < $deliveryDate) {
                        continue;
                    }
                }
            }

            if (($withdrawals && !in_array($it['delivery']['rtype_id'], $withdrawals)) || ($freights && !in_array($it['delivery']['frtype_id'], $freights))) {
                continue;
            }

            $startAt = \DateTime::createFromFormat('Y-m-d H:i:s', $it['delivery']['start_date']);
            $now = new \DateTime();

            /*
            Pedidos com status "entregue" ou "cancelado" E com data/hora na coluna "Iniciar até" antes da data/hora atual não devem aparecer, somente serão visualizados se puxados através dos filtros. Todos os outros itens aparecem
            */
            if (
                (!$itemStatus || !in_array($it['statusItem'], $itemStatus)) && (($it['statusItem'] === 'Cancelado' || $it['statusItem'] === 'Entregue') && $now >= $startAt)
            ) {
                continue;
            }

            $filteredItems[] = $it;
        }

        usort($filteredItems, function ($a, $b) {
            $dateA = \DateTime::createFromFormat('Y-m-d H:i:s', $a['delivery']['start_date']);
            $dateB = \DateTime::createFromFormat('Y-m-d H:i:s', $b['delivery']['start_date']);

            if ($dateA == $dateB) {
                return $b['num_folhas'] <=> $a['num_folhas'];
            }

            return $dateA <=> $dateB;
        });

        return $filteredItems;
    }

    public function getDetailsById($id)
    {
        $item = $this->itemEnt->getDetails($id);
        if ($item) {
            $item['files'] = $this->itemEnt->getFilesById($id);

            $totalValueWithoutDiscount = $this->itemEnt->calcTotalValueByOrder($item['tbPedido_idPedido']);
            $order = $this->orderEnt->getWithItemsCount($item['tbPedido_idPedido']);

            $discount = $this->calcDiscount($order, $totalValueWithoutDiscount);
            $itemDiscount = $discount / $order['total_items'];
            $item['valor_final'] = round($item['valor_final'] - $itemDiscount, 2);

            return $item;
        }

        return [];
    }

    public function updateAdmObservation($id, $admObs)
    {
        return $this->itemEnt->updateAdmObservation($id, $admObs);
    }

    public function updateStatus($id, $status)
    {
        $this->itemEnt->beginTransaction();
        $this->updateStatusNoTransaction($id, $status);
        $this->itemEnt->commit();
    }

    private function updateStatusNoTransaction($id, $status)
    {
        $this->incrementCouponCounter($id, $status);
        $this->itemEnt->updateStatus($id, $status);
    }

    private function incrementCouponCounter(int $itemId, string $status)
    {
        $item = $this->itemEnt->getWithDeliveryValue($itemId);

        if ($this->itemEnt->isAcomplished($status)) {
            // aumenta o valor acumulado do cupom.
            // incrementa o contador
            $this->updateCouponPrices(
                $item['tbcupom_idDesc'],
                $item['valorItem'],
                $item['descTotal'],
                $item['delivery_value']
            );
            $this->couponEnt->incCounter($item['tbcupom_idDesc']);
        }
    }

    private function updateCouponPrices(
        $couponId,
        $itemValue,
        $discountValue,
        $deliveryValue
    ) {
        $totalValue = $itemValue + $discountValue;
        $coupon = $this->couponEnt->get($couponId);

        $incDiscount = $this->couponEnt->isPercent($coupon['type'])
            ? ($totalValue * ($coupon['value'] / 100)) : min($totalValue * 0.5, $coupon['value']);

        $incSale = $itemValue;

        $this->couponEnt->updatePrices($couponId, $incDiscount, $incSale, $deliveryValue);
    }

    public function updateStatusItemsOfOrder($orderId, $status)
    {
        $items = $this->itemEnt->getAllItemsIdByOrderId($orderId);
        $count = count($items);
        foreach ($items as $id) {
            $this->updateStatusNoTransaction($id['idItem'], $status);
        }
        return $count;
    }

    public function registerUsedCoupons(int $orderId, string $status)
    {
        $items = $this->itemEnt->getAllItemsIdByOrderId($orderId);
        $count = count($items);
        foreach ($items as $id) {
            $this->incrementCouponCounter($id['idItem'], $status);
        }
        return $count;
    }

    public function updateFileIsOk($id, $value)
    {
        $value = $value == 'true' ? 1 : null;

        return $this->fileEnt->updateFileIsOk($id, $value);
    }

    public function getFiles($id)
    {
        return $this->itemEnt->getFilesById($id);
    }

    public function getFile($hash)
    {
        return $this->itemEnt->getFileByHash($hash);
    }

    public function getCommissionData($orderId)
    {
        return $this->itemEnt->getCommissionDataByOrderId($orderId);
    }

    public function updateTrackingCode(int $id, string $trackingCode)
    {
        $isTracking = false;

        $itemInfo = $this->itemEnt->getWithDeliveryInfo($id);

        if (empty($itemInfo)) {
            throw new Exception('Não foi encontrado um item com o id informado');
        }

        // Verificar se o item é para entrega
        if (!empty($itemInfo['tbRetirada_idRetirada'])) {
            throw new Exception('O item informado não é para entrega');
        }

        $this->itemEnt->beginTransaction();

        $this->itemEnt->updateTrackingCode($id, $trackingCode);

        $orderId = $itemInfo['tbPedido_idPedido'];
        if ($this->isTrackingDeliveryReady($orderId)) {
            $this->updateTrackingStatusByOrderId($orderId, true);
            $isTracking = true;
        }

        $this->itemEnt->commit();

        return [
            'order_id'    => $orderId,
            'is_tracking' => $isTracking
        ];
    }

    private function isTrackingDeliveryReady(int $orderId)
    {
        $orderItems = $this->itemEnt->getAllByOrderId($orderId);

        foreach ($orderItems as $item) {
            if (empty($item['tracking_code'])) {
                return false;
            }
        }

        return true;
    }

    private function updateTrackingStatusByOrderId(int $orderId, bool $status)
    {
        return $this->itemEnt->updateTrackingStatusByOrderId($orderId, $status);
    }

    public function calcOrderAndDeliveryValueByOrder(int $orderId)
    {
        $res = $this->itemEnt->calcOrderAndDeliveryValueByOrder($orderId);

        $taxPercent = 0.0685;
        $tax = $taxPercent * (float) $res['order_value'];
        $orderValue = (float) $res['order_value'] - $tax;

        return [
            'id'             => $orderId,
            'order_value'    => round($orderValue, 2),
            'tax'            => round($tax, 2),
            'delivery_value' => round((float) $res['delivery_value'], 2)
        ];
    }
}
