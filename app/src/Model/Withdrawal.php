<?php

namespace IntecPhp\Model;

use IntecPhp\Model\BusinessDay;
use IntecPhp\Entity\TbAcabamento;
use IntecPhp\Entity\TbSettings;

class Withdrawal
{
    const WD_EXPRESS_1 = 1;
    const WD_EXPRESS_2 = 2;
    const WD_NORMAL_1 = 3;
    const WD_NORMAL_2 = 4;
    const WD_EXPRESS_3 = 5;

    const WD_TYPE_NORMAL = 'Normal';
    const WD_TYPE_EXPRESS = 'Expressa';

    private $businessDay;
    private $bindingEnt;
    private $withdrawalConfig;
    private $extraBDaysConfig;
    private $shippingPrices;

    public function __construct(
        BusinessDay $bday,
        TbAcabamento $bindingEnt,
        array $withdrawalConfig,
        TbSettings $settingsEnt,
        ShippingPrices $shippingPrices
    ) {
        $this->businessDay = $bday;
        $this->bindingEnt = $bindingEnt;
        $this->withdrawalConfig = $withdrawalConfig;
        $this->extraBDaysConfig = $settingsEnt->getExtraBDaysSettings();
        $this->shippingPrices = $shippingPrices->getShippingPrices();
    }

    public function getWithdrawalType(int $wType, \DateTime $orderTime, int $totalPaper, int $totalPages, array $bindingIds)
    {
        extract($this->processVars($orderTime, $bindingIds));

        $wTypeResult = null;
        switch ($wType) {
            case self::WD_EXPRESS_1:
                $wTypeResult = $this->calcExpress1($orderTime, $isBusinessDay, $totalPages, $hasHardcover);
                break;
            case self::WD_EXPRESS_2:
                $wTypeResult = $this->calcExpress2($totalPages, $hasHardcover);
                break;
            case self::WD_EXPRESS_3:
                $wTypeResult = $this->calcExpress3($totalPages, $hasHardcover);
                break;
            case self::WD_NORMAL_1:
                $wTypeResult = $this->calcNormal1($totalPages, $hasHardcover);
                break;
            case self::WD_NORMAL_2:
                $wTypeResult = $this->calcNormal2($totalPages, $hasHardcover);
                break;
            default:
                throw new \Exception('Tipo de retirada inválido');
        }

        if (!$wTypeResult) {
            throw new \Exception('Tipo de retirada escolhido não pode ser utilizado. Tente novamente.');
        }

        return $wTypeResult;
    }

    public function getWithdrawalTypes(\DateTime $orderTime, int $totalPaper, int $totalPages, array $bindingIds)
    {
        extract($this->processVars($orderTime, $bindingIds));

        $withdrawalTypes = [];

        if ($wType = $this->calcExpress1($orderTime, $isBusinessDay, $totalPages, $hasHardcover)) {
            $withdrawalTypes[] = $wType;
        }

        if ($wType = $this->calcExpress2($totalPages, $hasHardcover)) {
            $withdrawalTypes[] = $wType;
        }

        if ($wType = $this->calcNormal1($totalPages, $hasHardcover)) {
            $withdrawalTypes[] = $wType;
        }

        if ($wType = $this->calcNormal2($totalPages, $hasHardcover)) {
            $withdrawalTypes[] = $wType;
        }

        return $withdrawalTypes;
    }

    private function processVars(\DateTime $orderTime, array $bindingIds)
    {
        $hasHardcover = $this->bindingEnt->hasHardcoverInIds($bindingIds);
        $isBusinessDay = $this->businessDay->isBusinessDay($orderTime);

        return [
            'hasHardcover' => $hasHardcover,
            'isBusinessDay' => $isBusinessDay,
        ];
    }

    private function calcExpress1(\DateTime $orderTime, bool $isBusinessDay, int $totalPages, bool $hasHardcover)
    {
        $timelimit = new \DateTime();
        $expressTimelimit = $this->withdrawalConfig['express1_endtime'];
        $timelimit->setTime($expressTimelimit['hour'], $expressTimelimit['minute'], 0);
        $pageLimit = $this->withdrawalConfig['page_limits']['limit1'];

        if (
            ($orderTime <= $timelimit) &&
            $isBusinessDay && ($totalPages <= $pageLimit) &&
            !$hasHardcover
        ) {
            $withdrawalDate = new \DateTime();

            $extraBusinessDays = $this->extraBDaysConfig['wExpress1ExtraBDays'];
            if ($extraBusinessDays == 0) $extraBusinessDays = $this->extraBDaysConfig['withdrawalExtraBDays'];
            $bDays = $extraBusinessDays;

            for ($i = 0; $i < $bDays; $i++) {
                $withdrawalDate = $this->businessDay->nextBusinessDay($withdrawalDate);
            }

            $wTime = $this->withdrawalConfig['express1_withdrawal_time'];
            $withdrawalDate->setTime($wTime['hour'], $wTime['minute'], 0);

            $dispatchDate = new \DateTime();
            $dispatchDate->setTime(16, 30, 0);
            $finishAt = clone $dispatchDate;
            $startAt = new \DateTime();
            $startAt->setTime(15, 0, 0);

            $withdrawalPrice = $this->withdrawalConfig['db_values'] ? $this->shippingPrices['wExpress1Price'] : $this->withdrawalConfig['express1_price'];

            return $this->buildWithdrawalResult(self::WD_EXPRESS_1, 'Expressa 1', self::WD_TYPE_EXPRESS, $withdrawalPrice, $withdrawalDate, $dispatchDate, $finishAt, $startAt);
        }
    }

    private function calcExpress2(int $totalPages, bool $hasHardcover)
    {
        $pageLimit = $this->withdrawalConfig['page_limits']['limit2'];

        if (
            ($totalPages <= $pageLimit) &&
            !$hasHardcover
        ) {
            $withdrawalDate = $this->businessDay->nextBusinessDay(new \DateTime());

            $extraBusinessDays = $this->extraBDaysConfig['wExpress2ExtraBDays'];
            if ($extraBusinessDays == 0) $extraBusinessDays = $this->extraBDaysConfig['withdrawalExtraBDays'];
            $bDays = $extraBusinessDays;

            for ($i = 0; $i < $bDays; $i++) {
                $withdrawalDate = $this->businessDay->nextBusinessDay($withdrawalDate);
            }

            $wTime = $this->withdrawalConfig['express2_withdrawal_time'];
            $withdrawalDate->setTime($wTime['hour'], $wTime['minute'], 0);

            $dispatchDate = $this->businessDay->nextBusinessDay(new \DateTime());
            $dispatchDate->setTime(11, 0, 0);
            $finishAt = clone $dispatchDate;
            $startAt = $this->businessDay->nextBusinessDay(new \DateTime());
            $startAt->setTime(9, 30, 0);

            $withdrawalPrice = $this->withdrawalConfig['db_values'] ? $this->shippingPrices['wExpress2Price'] : $this->withdrawalConfig['express2_price'];

            return $this->buildWithdrawalResult(self::WD_EXPRESS_2, 'Expressa 2', self::WD_TYPE_EXPRESS, $withdrawalPrice, $withdrawalDate, $dispatchDate, $finishAt, $startAt);
        }
    }

    private function calcExpress3(int $totalPages, bool $hasHardcover)
    {
        $pageLimit = $this->withdrawalConfig['page_limits']['limit3'];

        if (
            ($totalPages <= $pageLimit) &&
            !$hasHardcover
        ) {
            $withdrawalDate = $this->businessDay->nextSaturday(new \DateTime());

            $wTime = $this->withdrawalConfig['express3_withdrawal_time'];
            $withdrawalDate->setTime($wTime['hour'], $wTime['minute'], 0);

            $dispatchDate = clone $withdrawalDate;
            $dispatchDate->setTime(15, 0, 0);
            $finishAt = clone $dispatchDate;
            $startAt = new \DateTime();
            $startAt->setTime(13, 0, 0);

            return $this->buildWithdrawalResult(self::WD_EXPRESS_3, 'Expressa 3', self::WD_TYPE_EXPRESS, $this->withdrawalConfig['express3_price'], $withdrawalDate, $dispatchDate, $finishAt, $startAt);
        }
    }

    private function calcNormal1(int $totalPages, bool $hasHardcover)
    {
        $days = ceil($totalPages / 3000);

        if ($hasHardcover) {
            $days += 1;
        }

        $extraBusinessDays = $this->extraBDaysConfig['wNormal1ExtraBDays'];
        if ($extraBusinessDays == 0) $extraBusinessDays = $this->extraBDaysConfig['withdrawalExtraBDays'];
        $days += $extraBusinessDays;

        $withdrawalDate = $this->businessDay->nextBusinessDay(new \DateTime());
        for ($i = 0; $i < $days; $i++) {
            $withdrawalDate = $this->businessDay->nextBusinessDay($withdrawalDate);
        }

        $wTime = $this->withdrawalConfig['normal1_withdrawal_time'];
        $withdrawalDate->setTime($wTime['hour'], $wTime['minute'], 0);

        $offset = ceil($totalPages / 3000);
        $dispatchDate = $this->businessDay->nextBusinessDay(new \DateTime());
        for ($i = 0; $i < $offset; $i++) {
            $dispatchDate = $this->businessDay->nextBusinessDay($dispatchDate);
        }
        $dispatchDate->setTime(11, 0, 0);
        $finishAt = clone $dispatchDate;
        $startAt = $this->businessDay->nextBusinessDay(new \DateTime());
        $startAt->setTime(14, 0, 0);

        return $this->buildWithdrawalResult(self::WD_NORMAL_1, 'Normal 1', self::WD_TYPE_NORMAL, $this->withdrawalConfig['normal1_price'], $withdrawalDate, $dispatchDate, $finishAt, $startAt);
    }

    private function calcNormal2(int $totalPages, bool $hasHardcover)
    {
        $days = ceil($totalPages / 3000) + 3;

        if ($hasHardcover) {
            $days += 1;
        }

        $extraBusinessDays = $this->extraBDaysConfig['wNormal2ExtraBDays'];
        if ($extraBusinessDays == 0) $extraBusinessDays = $this->extraBDaysConfig['withdrawalExtraBDays'];
        $days += $extraBusinessDays;

        $withdrawalDate = $this->businessDay->nextBusinessDay(new \DateTime());
        for ($i = 0; $i < $days; $i++) {
            $withdrawalDate = $this->businessDay->nextBusinessDay($withdrawalDate);
        }
        $wTime = $this->withdrawalConfig['normal2_withdrawal_time'];
        $withdrawalDate->setTime($wTime['hour'], $wTime['minute'], 0);

        $offset = ceil($totalPages / 3000);
        $dispatchDate = $this->businessDay->nextBusinessDay(new \DateTime());
        for ($i = 0; $i < $offset; $i++) {
            $dispatchDate = $this->businessDay->nextBusinessDay($dispatchDate);
        }
        $dispatchDate->setTime(11, 0, 0);
        $finishAt = clone $dispatchDate;
        $startAt = $this->businessDay->nextBusinessDay(new \DateTime());
        $startAt->setTime(14, 0, 0);

        return $this->buildWithdrawalResult(self::WD_NORMAL_2, 'Normal 2', self::WD_TYPE_NORMAL, $this->withdrawalConfig['normal2_price'], $withdrawalDate, $dispatchDate, $finishAt, $startAt);
    }

    private function buildWithdrawalResult(
        $id,
        $name,
        $type,
        $value,
        \DateTime $withdrawalDate,
        \DateTime $dispatchDate,
        \DateTime $finishAt,
        \DateTime $startAt
    ) {
        $d = new \DateTime();
        $formatDate = null;
        $formatTime = null;
        if ($d->format('d/m') === $withdrawalDate->format('d/m')) {
            $formatDate = 'Hoje';
            $formatTime = ' Após ' . $withdrawalDate->format('H') . 'h até 19h';
        } elseif (date('w', strtotime($withdrawalDate->format('Y-m-d H:i:s'))) == 6) {
            $formatDate = $withdrawalDate->format('d/m');
            $formatTime = ' Após ' . $withdrawalDate->format('H') . 'h até 16h';
        } else {
            $formatDate = $withdrawalDate->format('d/m');
            $formatTime =  'Após ' . $withdrawalDate->format('H') . 'h';
        }

        return [
            'id' => $id,
            'name' => $name,
            'type' => $type,
            'value' => $value,
            'withdrawalDate' => $formatDate,
            'withdrawalTime' => $formatTime,
            'deadline' => $withdrawalDate,
            'dispatchDate' => $dispatchDate,
            'finishDate' => $finishAt,
            'startDate' => $startAt
        ];
    }
}
