<?php

namespace IntecPhp\Model;

/**
 * Define constantes da aplicação
 *
 * @mstile-150x150author intec
 */
class Config
{

    // esta classe não pode ser instanciada
    private function __construct()
    {

    }

    public static function getDomain($suffix = "")
    {
        return $_SERVER['SERVER_NAME'] . $suffix;
    }
}
