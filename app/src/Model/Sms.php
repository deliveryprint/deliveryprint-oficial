<?php

namespace IntecPhp\Model;

use IntecPhp\Entity\SmsEntity;

class Sms
{
    private $smsEntity;

    public function __construct(SmsEntity $smsEntity)
    {
        $this->smsEntity = $smsEntity;
    }

    public function saveSms(string $idSms, int $idPedido, string $phoneNumber, string $message)
    {
        return $this->smsEntity->save($idSms, $idPedido, $phoneNumber, $message);
    }

    public function saveSmsReply(string $idSms, string $reply, string $replyDate)
    {
        return $this->smsEntity->saveReply($idSms, $reply, $replyDate);
    }

    public function getAll(string $startPeriod, string $endPeriod, string $filterType)
    {
        $sms = $this->smsEntity->getAllAndLastSmsDate($startPeriod, $endPeriod, $filterType);

        if (!empty($sms)) {
            for ($i = 0; $i < count($sms); $i++) {
                $sms[$i]['data_envio'] = (new \DateTime($sms[$i]['data_envio']))->format('d-m-Y H:i:s');
                if (!empty($sms[$i]['data_resposta'])) {
                    $sms[$i]['data_resposta'] = (new \DateTime($sms[$i]['data_resposta']))->format('d-m-Y H:i:s');
                }
            }
        }

        return $sms;
    }

    public function createSmsCsv(array $messages)
    {
        $delimiter = ';';

        $fileName = 'sms_' . date('d-m-Y H:i:s') . '.csv';

        if (($output = fopen("/tmp/$fileName", 'w')) === false) {
            throw new \Exception("Erro ao tentar criar o arquivo CSV");
        }

        // gerando o cabeçalho das colunas
        fputcsv($output, ['Id do Pedido', 'Nome do Cliente', 'Nº do Celular', 'Data do Envio', 'Data da Resposta', 'Resposta Recebida'], $delimiter);

        // criando cada linha do arquivo
        foreach ($messages as $sms) {
            fputcsv($output, $sms, $delimiter);
        }

        fclose($output);
        return $fileName;
    }

    public function numberAlreadyUsed(string $phoneNumber)
    {

        $numberUsed = $this->smsEntity->checkNumber($phoneNumber);

        if (empty($numberUsed)) {
            return false;
        }

        return true;
    }
}
