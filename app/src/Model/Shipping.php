<?php

namespace IntecPhp\Model;

use IntecPhp\Service\Shipping as ShippingCorreios;
use IntecPhp\Model\BusinessDay;
use IntecPhp\Entity\TbAcabamento;
use FlyingLuscas\Correios\Service as CorreiosService;
use IntecPhp\Model\ShippingSpecs;
use IntecPhp\Entity\TbSettings;
use IntecPhp\Entity\TbValorFrete;

class Shipping
{
    const SF_EXPRESS_1 = 1;
    const SF_EXPRESS_2 = 2;
    const SF_EXPRESS_3 = 3;
    const SF_EXPRESS_4 = 4;
    const SF_SEDEX_1 = 5;
    const SF_SEDEX_2 = 6;
    const SF_PAC_1 = 7;
    const SF_PAC_2 = 8;
    const SF_EXPRESS_5 = 9;

    const SF_TYPE_EXPRESS = 'Expressa';
    const SF_TYPE_SEDEX = 'CorreiosSEDEX';
    const SF_TYPE_PAC = 'CorreiosPAC';

    private $shippingSpecs;
    private $shippingCorreios;
    private $expressZips;
    private $businessDay;
    private $deliveryConfig;
    private $bindingEnt;
    private $extraBDaysConfig;
    private $valorFreteEnt;
    private $shippingPrices;

    public function __construct(
        ShippingSpecs $shippingSpecs,
        ShippingCorreios $shippingCorreios,
        array $expressZips,
        array $deliveryConfig,
        BusinessDay $bday,
        TbAcabamento $bindingEnt,
        TbSettings $settingsEnt,
        TbValorFrete $tbValorFrete,
        ShippingPrices $shippingPrices
    ) {
        $this->shippingSpecs = $shippingSpecs;
        $this->shippingCorreios = $shippingCorreios;
        $this->expressZips = $expressZips;
        $this->businessDay = $bday;
        $this->deliveryConfig = $deliveryConfig;
        $this->bindingEnt = $bindingEnt;
        $this->extraBDaysConfig = $settingsEnt->getExtraBDaysSettings();
        $this->valorFreteEnt = $tbValorFrete;
        $this->shippingPrices = $shippingPrices->getShippingPrices();
    }

    public function getFreightType(int $freightId, string $toZip, \DateTime $orderTime, int $paperType, int $totalPaper, int $totalPages, array $bindingIds, float $orderValue, bool $freightFallback = false)
    {
        extract($this->processVars($toZip, $orderTime, $paperType, $totalPaper, $bindingIds, $freightId, $freightFallback));
        $freight = [];
        switch ($freightId) {
            case self::SF_EXPRESS_1:
                $freight[] = $this->calcExpress1($isZipInRange, $orderTime, $isBusinessDay, $totalPages, $hasHardcover, $orderValue);
                break;
            case self::SF_EXPRESS_2:
                $freight[] = $this->calcExpress2($isZipInRange, $totalPages, $hasHardcover, $orderValue);
                break;
            case self::SF_EXPRESS_3:
                $freight[] = $this->calcExpress3($isZipInRange, $totalPages, $hasHardcover, $orderValue);
                break;
            case self::SF_EXPRESS_4:
                $freight[] = $this->calcExpress4($isZipInRange, $totalPages, $hasHardcover, $orderValue);
                break;
            case self::SF_EXPRESS_5:
                $freight[] = $this->calcExpress5($isZipInRange, $totalPages, $hasHardcover);
                break;
            case self::SF_SEDEX_1:
                $freight = $this->getMultipleSedex1($isZipInRange, $scorreios, $totalPages, $hasHardcover);
                break;
            case self::SF_SEDEX_2:
                $freight = $this->getMultipleSedex2($isZipInRange, $scorreios, $totalPages, $hasHardcover);
                break;
            case self::SF_PAC_1:
                $freight = $this->getMultiplePac1($isZipInRange, $scorreios, $totalPages, $hasHardcover);
                break;
            case self::SF_PAC_2:
                $freight = $this->getMultiplePac2($isZipInRange, $scorreios, $totalPages, $hasHardcover);
                break;
            default:
                throw new \Exception('Tipo de entrega inválida');
        }

        if (!$freight) {
            throw new \Exception('Tipo de entrega escolhido não pode ser utilizado. Tente novamente.');
        }

        return $freight;
    }

    private function getMultipleSedex1($isZipInRange, $scorreios, $totalPages, $hasHardcover)
    {
        $freight = [];

        foreach ($scorreios as $correiosFreight) {
            $numFreights = $correiosFreight['sedex']['numberOfFreights'];
            for ($i = 0; $i < $numFreights; $i++) {
                $fr = $this->calcSedex1($isZipInRange, $correiosFreight['sedex'], $totalPages, $hasHardcover);
                $fr['value'] /= $numFreights;
                $freight[] = $fr;
            }
        }

        return $freight;
    }

    private function getMultipleSedex2($isZipInRange, $scorreios, $totalPages, $hasHardcover)
    {
        $freight = [];

        foreach ($scorreios as $correiosFreight) {
            $numFreights = $correiosFreight['sedex']['numberOfFreights'];
            for ($i = 0; $i < $numFreights; $i++) {
                $fr = $this->calcSedex2($isZipInRange, $correiosFreight['sedex'], $totalPages, $hasHardcover);
                $fr['value'] /= $numFreights;
                $freight[] = $fr;
            }
        }

        return $freight;
    }

    private function getMultiplePac1($isZipInRange, $scorreios, $totalPages, $hasHardcover)
    {
        $freight = [];

        foreach ($scorreios as $correiosFreight) {
            $numFreights = $correiosFreight['pac']['numberOfFreights'];
            for ($i = 0; $i < $numFreights; $i++) {
                $fr = $this->calcPac1($isZipInRange, $correiosFreight['pac'], $totalPages, $hasHardcover);
                $fr['value'] /= $numFreights;
                $freight[] = $fr;
            }
        }

        return $freight;
    }

    private function getMultiplePac2($isZipInRange, $scorreios, $totalPages, $hasHardcover)
    {
        $freight = [];

        foreach ($scorreios as $correiosFreight) {
            $numFreights = $correiosFreight['pac']['numberOfFreights'];
            for ($i = 0; $i < $numFreights; $i++) {
                $fr = $this->calcPac2($isZipInRange, $correiosFreight['pac'], $totalPages, $hasHardcover);
                $fr['value'] /= $numFreights;
                $freight[] = $fr;
            }
        }

        return $freight;
    }

    public function getFreightTypes(string $toZip, \DateTime $orderTime, int $paperType, int $totalPaper, int $totalPages, array $bindingIds, float $orderValue, bool $freightFallback = false)
    {
        extract($this->processVars($toZip, $orderTime, $paperType, $totalPaper, $bindingIds, null, $freightFallback));

        $freightTypes = [];

        if ($shippingType = $this->calcExpress1($isZipInRange, $orderTime, $isBusinessDay, $totalPages, $hasHardcover, $orderValue)) {
            $freightTypes[] = $shippingType;
        }

        // Desativa entrega expressa 2
        // if ($shippingType = $this->calcExpress2($isZipInRange, $totalPages, $hasHardcover, $orderValue)) {
        //     $freightTypes[] = $shippingType;
        // }

        if ($shippingType = $this->calcExpress3($isZipInRange, $totalPages, $hasHardcover, $orderValue)) {
            $freightTypes[] = $shippingType;
        }

        if ($shippingType = $this->calcExpress4($isZipInRange, $totalPages, $hasHardcover, $orderValue)) {
            $freightTypes[] = $shippingType;
        }

        if ($this->deliveryConfig['express5_enabled']) {
            if ($shippingType = $this->calcExpress5($isZipInRange, $totalPages, $hasHardcover)) {
                $freightTypes[] = $shippingType;
            }
        }

        foreach ($scorreios as $correiosFreight) {
            if ($this->deliveryConfig['sedex1_enabled']) {
                if ($shippingType = $this->calcSedex1($isZipInRange, $correiosFreight['sedex'], $totalPages, $hasHardcover)) {
                    $freightTypes[] = $shippingType;
                }
            }

            if ($this->deliveryConfig['sedex2_enabled']) {
                if ($shippingType = $this->calcSedex2($isZipInRange, $correiosFreight['sedex'], $totalPages, $hasHardcover)) {
                    $freightTypes[] = $shippingType;
                }
            }

            if ($this->deliveryConfig['pac1_enabled']) {
                if ($shippingType = $this->calcPac1($isZipInRange, $correiosFreight['pac'], $totalPages, $hasHardcover)) {
                    $freightTypes[] = $shippingType;
                }
            }

            if ($this->deliveryConfig['pac2_enabled']) {
                if ($shippingType = $this->calcPac2($isZipInRange, $correiosFreight['pac'], $totalPages, $hasHardcover)) {
                    $freightTypes[] = $shippingType;
                }
            }
        }

        return $freightTypes;
    }

    private function processVars(string $toZip, \DateTime $orderTime, int $paperType, int $totalPaper, array $bindingIds, $freightId = null, bool $freightFallback = false)
    {
        $correios = [];
        if (in_array($freightId, [
            self::SF_SEDEX_1,
            self::SF_SEDEX_2,
            self::SF_PAC_1,
            self::SF_PAC_2,
            null
        ])) {
            $itemSpecs = $this->shippingSpecs->parseItemSpecs($paperType, $totalPaper);
            $correios = $this->calculateAllCorreiosFreight($toZip, $itemSpecs, $freightId, $freightFallback);
        }

        $hasHardcover = $this->bindingEnt->hasHardcoverInIds($bindingIds);
        $isZipInRange = $this->inZipRange($toZip);
        $isBusinessDay = $this->businessDay->isBusinessDay($orderTime);

        return [
            'scorreios' => $correios,
            'hasHardcover' => $hasHardcover,
            'isZipInRange' => $isZipInRange,
            'isBusinessDay' => $isBusinessDay,
        ];
    }

    private function calculateAllCorreiosFreight($toZip, $itemSpecs, $freightId, $freightFallback = false)
    {
        $correiosResult = [];
        foreach ($itemSpecs as $spec) {
            $correios = [];

            // Caso se deseje consultar a api dos correios
            if (!$freightFallback) {
                if (!$freightId) { // caso todos os tipos de frete
                    $correios = $this->shippingCorreios->calculateFreight($toZip, [$spec['item']]);
                } elseif (in_array($freightId, [ // caso sedex
                    self::SF_SEDEX_1,
                    self::SF_SEDEX_2,
                ])) {
                    $correios = $this->shippingCorreios->calculateSedexFreight($toZip, [$spec['item']]);
                } else { // caso pac
                    $correios = $this->shippingCorreios->calculatePacFreight($toZip, [$spec['item']]);
                }
            } else {
                // Caso se deseje consultar a tabela de fretes fixos
                if (!$freightId) {
                    $resultSedex = $this->valorFreteEnt->getFreightSedex($toZip, $spec['item']->weight);
                    $resultPac = $this->valorFreteEnt->getFreightPac($toZip, $spec['item']->weight);
                    $correios[] = ['name' => 'Sedex', 'code' => 4014, 'price' => $resultSedex[0]['custoEntrega'], 'deadline' => $resultSedex[0]['prazoEntrega'], 'error' => []];
                    $correios[] =  ['name' => 'PAC', 'code' => 4510, 'price' => $resultPac[0]['custoEntrega'], 'deadline' => $resultPac[0]['prazoEntrega'], 'error' => []];
                } elseif (in_array($freightId, [ // caso sedex
                    self::SF_SEDEX_1,
                    self::SF_SEDEX_2,
                ])) {
                    $resultSedex = $this->valorFreteEnt->getFreightSedex($toZip, $spec['item']->weight);
                    $correios[] = ['name' => 'Sedex', 'code' => 4014, 'price' => $resultSedex[0]['custoEntrega'], 'deadline' => $resultSedex[0]['prazoEntrega'], 'error' => []];
                } else { // caso pac
                    $resultPac = $this->valorFreteEnt->getFreightPac($toZip, $spec['item']->weight);
                    $correios[] =  ['name' => 'PAC', 'code' => 4510, 'price' => $resultPac[0]['custoEntrega'], 'deadline' => $resultPac[0]['prazoEntrega'], 'error' => []];
                }
            }

            $result = [
                'sedex' => null,
                'pac' => null
            ];

            foreach ($correios as $service) {
                if ($this->zipNotFound($service['error'])) {
                    throw new \Exception($service['error']['message'], $service['error']['code']);
                }

                switch ($service['code']) {
                    case CorreiosService::PAC:
                        $result['pac'] = $service;
                        $result['pac']['price'] = $service['price'] * $spec['amount'];
                        $result['pac']['numberOfFreights'] = $spec['amount'];
                        break;
                    case CorreiosService::SEDEX:
                        $result['sedex'] = $service;
                        $result['sedex']['price'] = $service['price'] * $spec['amount'];
                        $result['sedex']['numberOfFreights'] = $spec['amount'];
                        break;
                }
            }

            $correiosResult[] = $result;
        }

        return $correiosResult;
    }


    private function calcExpress1(bool $isZipInRange, \DateTime $orderTime, bool $isBusinessDay, int $totalPages, bool $hasHardcover, float $orderValue)
    {
        $today = new \DateTime();
        $pageLimit = $this->deliveryConfig['page_limits']['limit1'];

        if (
            ($totalPages <= $pageLimit) &&
            $isZipInRange &&
            !$hasHardcover
        ) {
            if ($isBusinessDay) {
                // Pedidos feitos durante dia de semana
                $expressTimeLimit = $this->deliveryConfig['express1_endtime'];
                $timelimit = new \DateTime();
                $timelimit->setTime($expressTimeLimit['hour'], $expressTimeLimit['minute'], 0);

                if ($orderTime <= $timelimit) {
                    $deliveryDate = new \DateTime();

                    $extraBusinessDays = $this->extraBDaysConfig['dExpress1ExtraBDays'];
                    if ($extraBusinessDays == 0) $extraBusinessDays = $this->extraBDaysConfig['deliveryExtraBDays'];
                    $bDays = $extraBusinessDays;

                    for ($i = 0; $i < $bDays; $i++) {
                        $deliveryDate = $this->businessDay->nextBusinessDay($deliveryDate);
                    }

                    $dTime = $this->deliveryConfig['express1_delivery_time'];
                    $deliveryDate->setTime($dTime['hour'], $dTime['minute'], 0);

                    $dispatchDate = new \DateTime();
                    $dispatchDate->setTime(17, 0, 0);
                    $finishAt = clone $dispatchDate;
                    $startAt = new \DateTime();
                    $startAt->setTime(15, 30, 0);

                    $deliveryPrice = $this->deliveryConfig['db_values'] ? $this->shippingPrices['dExpress1Price'] : $this->deliveryConfig['express1_price'];
                    if ($this->deliveryConfig['express_free_enabled'] && $orderValue >= $this->deliveryConfig['express_free_treshold']) {
                        $deliveryPrice = 0.0;
                    }

                    return $this->buildFreightResult(self::SF_EXPRESS_1, 'Expressa 1', self::SF_TYPE_EXPRESS, $deliveryPrice, $deliveryDate, $dispatchDate, $finishAt, $startAt);
                }
            } elseif ($this->deliveryConfig['express5_enabled'] && date('w', strtotime($today->format('Y-m-d H:i:s'))) == 6) {
                // Pedidos feitos no sábado
                $expressTimeLimit = $this->deliveryConfig['express5_endtime'];
                $timelimit = new \DateTime();
                $timelimit->setTime($expressTimeLimit['hour'], $expressTimeLimit['minute'], 0);

                if ($orderTime <= $timelimit) {
                    $deliveryDate = new \DateTime();

                    $dTime = $this->deliveryConfig['express5_delivery_time'];
                    $deliveryDate->setTime($dTime['hour'], $dTime['minute'], 0);

                    $dispatchDate = clone $deliveryDate;
                    $dispatchDate->setTime(15, 0, 0);
                    $finishAt = clone $dispatchDate;
                    $startAt = new \DateTime();
                    $startAt->setTime(13, 0, 0);

                    $deliveryPrice = $this->deliveryConfig['express5_price'];
                    if ($this->deliveryConfig['express_free_enabled'] && $orderValue >= $this->deliveryConfig['express_free_treshold']) {
                        $deliveryPrice = 0.0;
                    }

                    return $this->buildFreightResult(self::SF_EXPRESS_5, 'Expressa 5', self::SF_TYPE_EXPRESS, $deliveryPrice, $deliveryDate, $dispatchDate, $finishAt, $startAt);
                }
            }
        }
    }

    private function calcExpress2(bool $isZipInRange, int $totalPages, bool $hasHardcover, float $orderValue)
    {
        $pageLimit = $this->deliveryConfig['page_limits']['limit2'];
        if (
            ($totalPages <= $pageLimit) &&
            $isZipInRange &&
            !$hasHardcover
        ) {
            $deliveryDate = $this->businessDay->nextBusinessDay(new \DateTime());

            $extraBusinessDays = $this->extraBDaysConfig['dExpress2ExtraBDays'];
            if ($extraBusinessDays == 0) $extraBusinessDays = $this->extraBDaysConfig['deliveryExtraBDays'];
            $bDays = $extraBusinessDays;

            for ($i = 0; $i < $bDays; $i++) {
                $deliveryDate = $this->businessDay->nextBusinessDay($deliveryDate);
            }

            $dTime = $this->deliveryConfig['express2_delivery_time'];
            $deliveryDate->setTime($dTime['hour'], $dTime['minute'], 0);

            $dispatchDate = $this->businessDay->nextBusinessDay(new \DateTime());
            $dispatchDate->setTime(11, 0, 0);
            $finishAt = clone $dispatchDate;
            $startAt = $this->businessDay->nextBusinessDay(new \DateTime());
            $startAt->setTime(9, 0, 0);

            $deliveryPrice = $this->deliveryConfig['db_values'] ? $this->shippingPrices['dExpress2Price'] : $this->deliveryConfig['express2_price'];
            if ($this->deliveryConfig['express_free_enabled'] && $orderValue >= $this->deliveryConfig['express_free_treshold']) {
                $deliveryPrice = 0.0;
            }

            return $this->buildFreightResult(self::SF_EXPRESS_2, 'Expressa 2', self::SF_TYPE_EXPRESS, $deliveryPrice, $deliveryDate, $dispatchDate, $finishAt, $startAt);
        }
    }

    private function calcExpress3(bool $isZipInRange, int $totalPages, bool $hasHardcover, float $orderValue)
    {
        $pageLimit = $this->deliveryConfig['page_limits']['limit3'];
        if (
            ($totalPages <= $pageLimit) &&
            $isZipInRange &&
            !$hasHardcover
        ) {
            $deliveryDate = $this->businessDay->nextBusinessDay(new \DateTime());

            $extraBusinessDays = $this->extraBDaysConfig['dExpress3ExtraBDays'];
            if ($extraBusinessDays == 0) $extraBusinessDays = $this->extraBDaysConfig['deliveryExtraBDays'];
            $bDays = $extraBusinessDays;

            for ($i = 0; $i < $bDays; $i++) {
                $deliveryDate = $this->businessDay->nextBusinessDay($deliveryDate);
            }

            $dTime = $this->deliveryConfig['express3_delivery_time'];
            $deliveryDate->setTime($dTime['hour'], $dTime['minute'], 0);

            $dispatchDate = $this->businessDay->nextBusinessDay(new \DateTime());
            $dispatchDate->setTime(16, 45, 0);
            $finishAt = clone $dispatchDate;
            $startAt = $this->businessDay->nextBusinessDay(new \DateTime());
            $startAt->setTime(11, 0, 0);

            $deliveryPrice = $this->deliveryConfig['db_values'] ? $this->shippingPrices['dExpress3Price'] : $this->deliveryConfig['express3_price'];
            if ($this->deliveryConfig['express_free_enabled'] && $orderValue >= $this->deliveryConfig['express_free_treshold']) {
                $deliveryPrice = 0.0;
            }

            return $this->buildFreightResult(self::SF_EXPRESS_3, 'Expressa 3', self::SF_TYPE_EXPRESS, $deliveryPrice, $deliveryDate, $dispatchDate, $finishAt, $startAt);
        }
    }

    private function calcExpress4(bool $isZipInRange, int $totalPages, bool $hasHardcover, float $orderValue)
    {
        $pageLimit = $this->deliveryConfig['page_limits']['limit4'];
        if (
            ($totalPages <= $pageLimit) &&
            $isZipInRange
        ) {
            $deliveryDate = $this->businessDay->nextBusinessDay($this->businessDay->nextBusinessDay(new \DateTime()));

            $extraBusinessDays = $this->extraBDaysConfig['dExpress4ExtraBDays'];
            if ($extraBusinessDays == 0) $extraBusinessDays = $this->extraBDaysConfig['deliveryExtraBDays'];
            $bDays = $extraBusinessDays;

            for ($i = 0; $i < $bDays; $i++) {
                $deliveryDate = $this->businessDay->nextBusinessDay($deliveryDate);
            }

            $dTime = $this->deliveryConfig['express4_delivery_time'];
            $deliveryDate->setTime($dTime['hour'], $dTime['minute'], 0);

            $dispatchDate = $this->businessDay->nextBusinessDay($this->businessDay->nextBusinessDay(new \DateTime()));
            $dispatchDate->setTime(16, 45, 0);
            $finishAt = clone $dispatchDate;
            $startAt = $this->businessDay->nextBusinessDay(new \DateTime());
            $startAt->setTime(14, 0, 0);

            $deliveryPrice = $this->deliveryConfig['express4_price'];
            $additionalPrice = $this->deliveryConfig['express4_additional_price'];
            if ($this->deliveryConfig['db_values']) {
                $deliveryPrice = $this->shippingPrices['dExpress4Price'];
                $additionalPrice = $this->shippingPrices['dExpress4AdditionalPrice'];
            }
            if ($hasHardcover) {
                $deliveryPrice += $additionalPrice;
            } else {
                if ($this->deliveryConfig['express_free_enabled'] && $orderValue >= $this->deliveryConfig['express_free_treshold']) {
                    $deliveryPrice = 0.0;
                }
            }

            return $this->buildFreightResult(
                self::SF_EXPRESS_4,
                'Expressa 4',
                self::SF_TYPE_EXPRESS,
                $deliveryPrice,
                $deliveryDate,
                $dispatchDate,
                $finishAt,
                $startAt
            );
        }
    }

    private function calcExpress5(bool $isZipInRange, int $totalPages, bool $hasHardcover)
    {
        $pageLimit = $this->deliveryConfig['page_limits']['limit5'];

        if (
            ($totalPages <= $pageLimit) &&
            $isZipInRange &&
            !$hasHardcover
        ) {
            $deliveryDate = $this->businessDay->nextSaturday(new \DateTime());

            $dTime = $this->deliveryConfig['express5_delivery_time'];
            $deliveryDate->setTime($dTime['hour'], $dTime['minute'], 0);

            $dispatchDate = clone $deliveryDate;
            $dispatchDate->setTime(15, 0, 0);
            $finishAt = clone $dispatchDate;
            $startAt = new \DateTime();
            $startAt->setTime(13, 0, 0);

            $deliveryPrice = $this->deliveryConfig['db_values'] ? $this->shippingPrices['dExpress5Price'] : $this->deliveryConfig['express5_price'];

            return $this->buildFreightResult(self::SF_EXPRESS_5, 'Expressa 5', self::SF_TYPE_EXPRESS, $deliveryPrice, $deliveryDate, $dispatchDate, $finishAt, $startAt);
        }
    }

    private function calcSedex1(bool $isZipInRange, array $sedex, int $totalPages, bool $hasHardcover)
    {
        if (!$isZipInRange) {
            $deliveryDate = new \DateTime();

            $bDays = $sedex['deadline'] + ceil($totalPages / 1500) + $this->deliveryConfig['sedex1_extra_days'];

            if ($hasHardcover) {
                $bDays += 1;
            }

            $extraBusinessDays = $this->extraBDaysConfig['dSedex1ExtraBDays'];
            if ($extraBusinessDays == 0) $extraBusinessDays = $this->extraBDaysConfig['deliveryExtraBDays'];
            $bDays += $extraBusinessDays;

            for ($i = 0; $i < $bDays; $i++) {
                $deliveryDate = $this->businessDay->nextBusinessDay($deliveryDate);
            }
            $dTime = $this->deliveryConfig['sedex1_delivery_time'];
            $deliveryDate->setTime($dTime['hour'], $dTime['minute'], 0);

            $dispatchDate = $this->businessDay->nextBusinessDay(new \DateTime());
            $offset = $bDays - $sedex['deadline'];
            for ($i = 0; $i < $offset; $i++) {
                $dispatchDate = $this->businessDay->nextBusinessDay($dispatchDate);
            }
            $dispatchDate->setTime(14, 0, 0);
            $finishAt = clone $dispatchDate;
            $startAt = $this->businessDay->nextBusinessDay(new \DateTime());
            $startAt->setTime(14, 0, 0);

            return $this->buildFreightResult(self::SF_SEDEX_1, 'Sedex 1', self::SF_TYPE_SEDEX, $sedex['price'] + $this->deliveryConfig['sedex1_additional_price'], $deliveryDate, $dispatchDate, $finishAt, $startAt);
        }
    }

    private function calcSedex2(bool $isZipInRange, array $sedex, int $totalPages, bool $hasHardcover)
    {
        if (!$isZipInRange) {
            $deliveryDate = new \DateTime();

            $bDays = $sedex['deadline'] + ceil($totalPages / 1500) + $this->deliveryConfig['sedex2_extra_days'];

            if ($hasHardcover) {
                $bDays += 2;
            }

            $extraBusinessDays = $this->extraBDaysConfig['dSedex2ExtraBDays'];
            if ($extraBusinessDays == 0) $extraBusinessDays = $this->extraBDaysConfig['deliveryExtraBDays'];
            $bDays += $extraBusinessDays;

            for ($i = 0; $i < $bDays; $i++) {
                $deliveryDate = $this->businessDay->nextBusinessDay($deliveryDate);
            }
            $dTime = $this->deliveryConfig['sedex2_delivery_time'];
            $deliveryDate->setTime($dTime['hour'], $dTime['minute'], 0);

            $dispatchDate = $this->businessDay->nextBusinessDay(new \DateTime());
            $offset = ceil($totalPages / 3000);
            for ($i = 0; $i < $offset; $i++) {
                $dispatchDate = $this->businessDay->nextBusinessDay($dispatchDate);
            }
            $dispatchDate->setTime(15, 0, 0);
            $finishAt = clone $dispatchDate;
            $startAt = $this->businessDay->nextBusinessDay(new \DateTime());
            $startAt->setTime(14, 0, 0);

            return $this->buildFreightResult(
                self::SF_SEDEX_2,
                'Sedex 2',
                self::SF_TYPE_SEDEX,
                $sedex['price'] + $this->deliveryConfig['sedex2_additional_price'],
                $deliveryDate,
                $dispatchDate,
                $finishAt,
                $startAt
            );
        }
    }

    private function calcPac1(bool $isZipInRange, array $pac, int $totalPages, bool $hasHardcover)
    {
        if (!$isZipInRange) {
            $deliveryDate = new \DateTime();

            $bDays = $pac['deadline'] + ceil($totalPages / 1500) + $this->deliveryConfig['pac1_extra_days'];

            if ($hasHardcover) {
                $bDays += 1;
            }

            $extraBusinessDays = $this->extraBDaysConfig['dPac1ExtraBDays'];
            if ($extraBusinessDays == 0) $extraBusinessDays = $this->extraBDaysConfig['deliveryExtraBDays'];
            $bDays += $extraBusinessDays;

            for ($i = 0; $i < $bDays; $i++) {
                $deliveryDate = $this->businessDay->nextBusinessDay($deliveryDate);
            }
            $dTime = $this->deliveryConfig['pac1_delivery_time'];
            $deliveryDate->setTime($dTime['hour'], $dTime['minute'], 0);


            $dispatchDate = $this->businessDay->nextBusinessDay(new \DateTime());
            $offset = $bDays - $pac['deadline'];
            for ($i = 0; $i < $offset; $i++) {
                $dispatchDate = $this->businessDay->nextBusinessDay($dispatchDate);
            }
            $dispatchDate->setTime(14, 0, 0);
            $finishAt = clone $dispatchDate;
            $startAt = $this->businessDay->nextBusinessDay(new \DateTime());
            $startAt->setTime(14, 0, 0);

            return $this->buildFreightResult(self::SF_PAC_1, 'PAC 1', self::SF_TYPE_PAC, $pac['price'] + $this->deliveryConfig['pac1_additional_price'], $deliveryDate, $dispatchDate, $finishAt, $startAt);
        }
    }

    private function calcPac2(bool $isZipInRange, array $pac, int $totalPages, bool $hasHardcover)
    {
        if (!$isZipInRange) {
            $deliveryDate = new \DateTime();

            $bDays = $pac['deadline'] + ceil($totalPages / 4500) + $this->deliveryConfig['pac2_extra_days'];

            if ($hasHardcover) {
                $bDays += 1;
            }

            $extraBusinessDays = $this->extraBDaysConfig['dPac2ExtraBDays'];
            if ($extraBusinessDays == 0) $extraBusinessDays = $this->extraBDaysConfig['deliveryExtraBDays'];
            $bDays += $extraBusinessDays;
            for ($i = 0; $i < $bDays; $i++) {
                $deliveryDate = $this->businessDay->nextBusinessDay($deliveryDate);
            }
            $dTime = $this->deliveryConfig['pac2_delivery_time'];
            $deliveryDate->setTime($dTime['hour'], $dTime['minute'], 0);

            $dispatchDate = $this->businessDay->nextBusinessDay(new \DateTime());
            $offset = ceil($totalPages / 3000);
            for ($i = 0; $i < $offset; $i++) {
                $dispatchDate = $this->businessDay->nextBusinessDay($dispatchDate);
            }
            $dispatchDate->setTime(15, 0, 0);
            $finishAt = clone $dispatchDate;
            $startAt = $this->businessDay->nextBusinessDay(new \DateTime());
            $startAt->setTime(14, 0, 0);


            return $this->buildFreightResult(
                self::SF_PAC_2,
                'PAC 2',
                self::SF_TYPE_PAC,
                $pac['price'] + $this->deliveryConfig['pac2_additional_price'],
                $deliveryDate,
                $dispatchDate,
                $finishAt,
                $startAt
            );
        }
    }

    private function buildFreightResult(
        $id,
        $name,
        $type,
        $value,
        \DateTime $deliveryDate,
        \DateTime $dispatchDate,
        \DateTime $finishAt,
        \DateTime $startAt
    ) {
        $d = new \DateTime();

        $formatDate = null;
        $formatTime = null;
        if ($d->format('d/m') === $deliveryDate->format('d/m')) {
            $formatDate = 'Hoje';
            $formatTime = 'até as ' . $deliveryDate->format('H:i');
        } elseif (date('w', strtotime($deliveryDate->format('Y-m-d H:i:s'))) == 6) {
            $formatDate = $deliveryDate->format('d/m');
            $formatTime = 'até as ' . $deliveryDate->format('H:i');
        } else {
            $formatDate = $deliveryDate->format('d/m');
            $formatTime = 'das 09h às ' . $deliveryDate->format('H:i');
        }

        return [
            'id' => $id,
            'name' => $name,
            'type' => $type,
            'value' => $value,
            'deliveryDate' => $formatDate,
            'deliveryTime' => $formatTime,
            'deadline' => $deliveryDate,
            'dispatchDate' => $dispatchDate,
            'finishDate' => $finishAt,
            'startDate' => $startAt
        ];
    }

    private function inZipRange(string $zip)
    {
        $zip = (int) preg_replace('/\D/', '', $zip);

        foreach ($this->expressZips as $ezip) {
            if ($zip >= $ezip[0] &&  $zip <= $ezip[1]) {
                return true;
            }
        }

        return false;
    }

    private function zipNotFound($error)
    {
        return !empty($error) && $error['code'] == -3;
    }
}
