<?php

namespace IntecPhp\Model;

use IntecPhp\Entity\TbEntrega;
use IntecPhp\Entity\TbEndereco;
use IntecPhp\Entity\TbPedido;
use IntecPhp\Entity\TbItem;
use IntecPhp\Entity\TbArquivo;
use IntecPhp\Entity\TbRetirada;
use IntecPhp\Entity\TbFrete;
use IntecPhp\Entity\TbCliente;
use IntecPhp\Entity\TbDetalhesAcabamento;
use IntecPhp\Service\AddressFinder;
use IntecPhp\Model\PriceCalculator;

use IntecPhp\Model\Shipping;
use IntecPhp\Model\Withdrawal;
use IntecPhp\Model\Item;

use Exception;

/**
 * @Todo: Refatorar essa classe quebrando em outras
 */
class Order
{
    private $tbEntrega;
    private $tbEndereco;
    private $tbPedido;
    private $tbItem;
    private $tbArquivo;
    private $addrFinder;
    private $priceCalculator;
    private $tbRetirada;
    private $tbFrete;
    private $tbCliente;
    private $shipping;
    private $withdrawal;
    private $item;
    private $tbDetalhesAcabamento;

    public function __construct(
        TbEntrega $tbEntrega,
        TbRetirada $tbRetirada,
        TbFrete $tbFrete,
        TbEndereco $tbEndereco,
        TbPedido $tbPedido,
        TbItem $tbItem,
        TbArquivo $tbArquivo,
        TbCliente $tbCliente,
        AddressFinder $addrFinder,
        PriceCalculator $pcalc,
        Shipping $shipping,
        Withdrawal $withdrawal,
        Item $item,
        TbDetalhesAcabamento $tbDetalhes
    ) {
        $this->tbEntrega  = $tbEntrega;
        $this->tbRetirada  = $tbRetirada;
        $this->tbFrete  = $tbFrete;
        $this->tbEndereco = $tbEndereco;
        $this->tbPedido   = $tbPedido;
        $this->tbItem     = $tbItem;
        $this->tbArquivo  = $tbArquivo;
        $this->tbCliente  = $tbCliente;
        $this->addrFinder = $addrFinder;
        $this->priceCalculator = $pcalc;
        $this->shipping = $shipping;
        $this->withdrawal = $withdrawal;
        $this->item = $item;
        $this->tbDetalhesAcabamento = $tbDetalhes;
    }

    private function createAddressIfNotExists($idUser, $zip)
    {
        $idAddress = false;

        if ($idUser && ($idAddress = $this->tbEndereco->getIdByUserIdAndZip($idUser, $zip))) {
            return $idAddress;
        }

        $addr = $this->addrFinder->find($zip);
        return $this->tbEndereco->add($idUser, $addr['zipcode'], null, $addr['street'], $addr['district'], $addr['city'], $addr['uf'], null);
    }

    private function updateDeliveryOfAllItems($orderId, $userId, $zip, $newAddresId)
    {
        if ($zip) {
            $addressId = $this->tbEndereco->getIdByUserIdAndZip($userId, $zip);
            if (!$addressId) {
                $this->tbEndereco->updateUserId($userId, $newAddresId);
            } else {
                $this->tbPedido->updateAllDeliveriesAddrByOrderId($orderId, $addressId);
            }
        }
    }

    private function registerItems($idOrder, $items, $idUser, $idAddress)
    {
        foreach ($items as $item) {
            $finishing = $item['finishing'];
            $binBorder = null;
            if (!empty($finishing['binding']['border'])) {
                if ($finishing['binding']['border'] === 'Borda Horizontal') {
                    $binBorder = TbItem::HORIZONTAL_BORDER;
                    ;
                } else {
                    $binBorder = TbItem::VERTICAL_BORDER;
                }
            }

            $idDetalhes = null;
            if(isset($item['finishingDetails']) and !empty($item['finishingDetails'])) {
                $idDetalhes = $this->tbDetalhesAcabamento->add(
                    $item['finishingDetails']['corDaCapa'],
                    $item['finishingDetails']['corDaLetra'],
                    $item['finishingDetails']['textoCapa'],
                    $item['finishingDetails']['textoLombada']
                );
            }

            $itemValues = $this->priceCalculator->calculate($item, $idUser);

            $idItem = $this->tbItem->add(
                $item['color'],
                $item['side'],
                $item['finishing']['id'],
                $item['paper'],
                $idOrder,
                $itemValues['valueWithDiscount'],
                TbPedido::STATUS_CART,
                $itemValues['discountValue'],
                (int)$finishing['binding']['total'],
                $binBorder,
                $itemValues['couponDiscount'],
                $itemValues['generalCoupon'],
                $item['emailMktId'] == '' ? null : $item['emailMktId'],
                $item['observation'],
                filter_var ($item['delivery']['fallback'], FILTER_VALIDATE_BOOLEAN),
                $idDetalhes
            );

            $this->saveItemFiles($idItem, $item['files']);

            if ($item['delivery']['isFreight']) { // frete
                $this->createNewFreight($idItem, $item['delivery']['id'], $idAddress, $item['paper'], $itemValues['qPap'], $itemValues['qPag'], $item['finishing']['id'], $itemValues['value'], filter_var ($item['delivery']['fallback'], FILTER_VALIDATE_BOOLEAN));
            } else { // retirada
                $idWithdrawal = $this->createNewWithdrawal($idItem, $item['delivery']['id'], $itemValues['qPap'], $itemValues['qPag'], $item['finishing']['id']);
            }
        }
    }

    private function createNewWithdrawal(int $itemId, int $withdrawalId, int $totalPaper, int $totalPages, int $bindingId)
    {
        $wd = $this->withdrawal->getWithdrawalType(
            $withdrawalId,
            new \DateTime(),
            $totalPaper,
            $totalPages,
            [$bindingId]
        );

        $idRetirada = $this->tbRetirada->add(
            $withdrawalId,
            $wd['name'],
            $wd['type'],
            $wd['deadline']->format('Y-m-d H:i:s'),
            $wd['value']
        );

        $this->tbEntrega->add(
            null,
            $idRetirada,
            $itemId,
            $wd['value'],
            $wd['dispatchDate']->format('Y-m-d H:i:s'),
            $wd['finishDate']->format('Y-m-d H:i:s'),
            $wd['startDate']->format('Y-m-d H:i:s')
        );
    }

    private function createNewFreight(
        int $itemId,
        int $freightId,
        int $idAddress,
        int $paperType,
        int $totalPaper,
        int $totalPages,
        int $bindingId,
        float $orderValue,
        bool $freightFallback
    ) {
        $addr = $this->tbEndereco->get($idAddress);

        $freight = $this->shipping->getFreightType(
            $freightId,
            str_replace('-', '', $addr['cep']),
            new \DateTime(),
            $paperType,
            $totalPaper,
            $totalPages,
            [$bindingId],
            $orderValue,
            $freightFallback
        );

        $freightValue = array_sum(array_map(function ($fr) {
            return $fr['value'];
        }, $freight));

        $deliveryId = $this->tbEntrega->add(
            $idAddress,
            null,
            $itemId,
            $freightValue,
            $freight[0]['dispatchDate']->format('Y-m-d H:i:s'),
            $freight[0]['finishDate']->format('Y-m-d H:i:s'),
            $freight[0]['startDate']->format('Y-m-d H:i:s')
        );

        foreach ($freight as $fr) {
            $this->tbFrete->addFreight(
                $deliveryId,
                $freightId,
                $fr['name'],
                $fr['type'],
                $fr['deadline']->format('Y-m-d H:i:s'),
                $fr['value']
            );
        }
    }

    private function saveItemFiles(int $idItem, array $files)
    {
        foreach ($files as $file) {
            $name = $file['name'] ? $file['name'] : null;

            if (empty($file['fileSize']) && empty($file['userFileName'])) {
                throw new Exception('Por favor, informe todos os arquivos');
            }

            $this->tbArquivo->add(
                $idItem,
                $name,
                $file['numPages'],
                $file['numCopies'],
                $file['fileSize'],
                $file['userFileName']
            );
        }
    }

    public function registerUserOrder($items, $idUser, $zip)
    {
        $this->tbPedido->beginTransaction();
        $idAddress = null;
        if ($zip) {
            $idAddress = $this->createAddressIfNotExists($idUser, $zip);
        }
        $idOrder = $this->tbPedido->add($idUser);
        $this->registerItems($idOrder, $items, $idUser, $idAddress);
        $this->tbPedido->commit();
        return $idOrder;
    }

    public function applyDiscount($idOrder, $discountType, $discountValue, $emailMktId)
    {
        $this->tbPedido->updateDiscountInfo(
            $idOrder,
            $discountType,
            $discountValue,
            $emailMktId
        );
    }

    public function updateUserOrder($idOrder, $items, $idUser, $zip)
    {
        $idAddress = null;
        $this->tbPedido->beginTransaction();
        $numItems = $this->tbItem->countItems($idOrder);
        // Verificando se não é um pedido em que todos os itens foram excluídos
        if ($numItems > 0) {
            $currentItemsZip = $this->tbItem->getItemZipByOrder($idOrder);
            if ($zip) { // Não permite itens com ceps diferentes
                if ($currentItemsZip) {
                    if ($zip !== $currentItemsZip['cep']) {
                        throw new \Exception('O cep deve ser igual para todo os itens do carrinho');
                    } else {
                        $idAddress = $currentItemsZip['idEndereco'];
                    }
                } else {
                    // O item anterior do pedido teve a retirada como opção
                    throw new \Exception('Todos os itens do carrinho devem manter a retirada como opção');
                }
            } elseif ($currentItemsZip) {
                // O item anterior do pedido teve a entrega como opção
                throw new \Exception('Todos os itens do carrinho devem manter a entrega como opção');
            }
        } elseif ($zip) {
            $idAddress = $this->createAddressIfNotExists($idUser, $zip);
        }
        $this->registerItems($idOrder, $items, $idUser, $idAddress);
        $this->tbPedido->commit();
    }

    public function linkUserOrder($userId, $sessionOrderId)
    {
        $addressId   = false;
        $currOrderId = false;

        $this->tbPedido->beginTransaction();

        if ($sessionOrderId) {
            $currOrderId = $sessionOrderId;
            $sessionZip  = $this->tbItem->getItemZipByOrder($sessionOrderId);
            if ($sessionZip) {
                /* O pedido que está na sessão é para entrega. */
                // Definindo o id do usuário no endereço
                $this->tbEndereco->updateUserId($userId, $sessionZip['idEndereco']);
            }

            // Atribuindo o id do cliente ao pedido
            $this->tbPedido->updateUserId($userId, $sessionOrderId);
        }

        $this->tbPedido->commit();

        return $currOrderId;
    }

    public function getInfoOrder($id)
    {
        $items = $this->tbPedido->getById($id);
        foreach ($items as &$it) {
            $del = $this->tbItem->getDeliveryInfo($it['idItem']);
            $it['item_value'] = $del['delivery_value'] + $it['valorItem'];


            if ($del['prazo_retirada']) {
                $it['isWithdrawal'] = true;
                $it['delivery_date'] = \DateTime::createFromFormat('Y-m-d H:i:s', $del['prazo_retirada'])->format('d/m');
            } else {
                $it['isWithdrawal'] = false;
                $it['delivery_date'] = \DateTime::createFromFormat('Y-m-d H:i:s', $del['prazo_frete'])->format('d/m');
            }
        }

        return $items;
    }


    public function registerUserCompleteAddress($orderId, $userId, $userInfo, $addrInfo)
    {
        $addrInfoOld = $this->tbEndereco->getById($addrInfo['id']);
        if (!$addrInfo['complement']) {
            $addrInfo['complement'] = $addrInfoOld['complemento'] ? null : $addrInfoOld['complemento'];
        }

        // Atualizando o nome e o número de telefone do usuário
        $this->saveInOrderNameAndPhone($orderId, $userInfo['name'], $userInfo['tel']);
        if (isset($addrInfoOld['numero']) && $addrInfoOld['numero'] !== "") {
            // Existe diferença entre as informações submetidas e as já cadastradas
            if (($addrInfoOld['numero'] !== $addrInfo['number']) || ($addrInfoOld['complemento'] !== $addrInfo['complement'])) {
                // Verificando se já não existe um outro endereço cadastrado com exatamente as mesmas informações
                $addressId = $this->tbEndereco->searchByUserIdAndCompleteAddr(
                    $userId,
                    $addrInfoOld['cep'],
                    $addrInfo['number'],
                    $addrInfo['complement']
                );
                // Criando um novo endereço
                if (!$addressId) {
                    $addressId = $this->tbEndereco->add(
                        $userId,
                        $addrInfoOld['cep'],
                        $addrInfo['number'],
                        $addrInfo['street'],
                        $addrInfo['district'],
                        $addrInfoOld['cidade'],
                        $addrInfoOld['uf'],
                        $addrInfo['complement']
                    );
                }

                // Atualizando todas as entregas relacionadas com o pedido
                $this->tbPedido->updateAllDeliveriesAddrByOrderId($orderId, $addressId);

                return $addressId;
            }
        } else {
            $this->tbEndereco->update(
                $addrInfo['id'],
                $addrInfoOld['cep'],
                $addrInfo['number'],
                $addrInfo['street'],
                $addrInfo['district'],
                $addrInfoOld['cidade'],
                $addrInfoOld['uf'],
                $addrInfo['complement']
            );
        }

        return $addrInfo['id'];
    }

    public function getAll()
    {
        $data = [];
        $orders = $this->tbPedido->getAllNotCar();
        if (!$orders) {
            return $data;
        }

        foreach ($orders as $value) {
            $files = $this->tbPedido->getFileInfoByOrderId($value['idPedido']);


            $totalCopies = 0;

            foreach ($files as $file) {
                $totalCopies += ($file["numPaginas"] * $file["qtdImpressoes"]);
            }

            $deliveryType = "";
            $endAt = "";
            $sendAt = "";
            $deliveryDate = "";
            if ($this->tbPedido->isWithdrawal($value['idPedido'])) {
                $deliveryInfo = $this->tbPedido->getDeliveryWithdrawalInfoByOrderId($value['idPedido']);
                $deliveryType = $deliveryInfo['name'];
                $endAt = null;
                $sendAt = null;
                $deliveryDate = $deliveryInfo['prazo'];
            } else {
                $deliveryInfo = $this->tbPedido->getDeliveryInfoByOrderId($value['idPedido']);
                $deliveryType = $deliveryInfo['nomeServico'];
                $endAt = null;
                $sendAt = null;
                $deliveryDate = $deliveryInfo['prazo'];
            }

            $data[] = [
                "id"                =>      $value['idPedido'],
                "qtdImpressoes"     =>      $totalCopies,
                "tipoEntrega"       =>      $deliveryType == null ? "n/a" : $deliveryType,
                "iniciarAte"        =>      null,
                "finalizarAte"      =>      $endAt,
                "despacharAte"      =>      $sendAt,
                "statusPagamento"   =>      $value['statusPagamento'] == null ? "Não pago" : $value['statusPagamento'],
                "statusPedido"      =>      $value['statusPedido'],
                "dataPedido"        =>      $value['dataPedido'],
                "dataEntrega"       =>      $deliveryDate,
                "obervacao"         =>      $value['obsStatusPedido'],
                "nomeCliente" => strtolower($value['nomeCliente']),
                "email" => $value['emailCliente'],
                "details"           =>      [
                    "status"    =>      "...",
                    "delivery"  =>      [

                    ],
                    "deliveryTo" =>     "...",
                    "files"      =>     [

                    ],
                    "clientData" => [
                        "address" => [

                        ],
                    ],
                    "observation" => "...",
                    "orders"     => "..."
                ]
            ];
        }

        return $data;
    }

    public function updateStatus($id, $status)
    {
        $this->tbPedido->updateStatus($id, $status);
    }

    public function saveObs($id, $obs)
    {
        $this->tbPedido->saveObs($id, $obs);
    }

    public function saveInOrderNameAndPhone($orderId, $name, $phone)
    {
        return $this->tbPedido->updateNameAndPhone($orderId, $name, $phone);
    }

    public function itemsHaveSameStatus($orderId, $itemStatus)
    {
        return $this->tbPedido->itemsHaveSameStatus($orderId, $itemStatus);
    }

    public function hasDeliveryName($orderId)
    {
        $hasName = $this->tbPedido->hasDeliveryName($orderId);

        if (empty($hasName)) {
            return false;
        }

        return true;
    }

    public function getOrderFromDatetime($datetime)
    {
        return $this->tbPedido->getFromDatetime($datetime);
    }

    public function getOldFilesData()
    {
        $currentDate = new \DateTime();
        $currentDate->sub(new \DateInterval('P10D'));

        return $this->tbPedido->getOldsByStatus($currentDate->format('Y-m-d'));
    }

    public function getUnecessaryFiles()
    {
        $currentDate = new \DateTime();

        $cartDate     = (date_sub($currentDate, date_interval_create_from_date_string("3 days")))->format('Y-m-d');
        $cancelDate   = (date_sub($currentDate, date_interval_create_from_date_string("7 days")))->format('Y-m-d');
        $deliveryDate = (date_sub($currentDate, date_interval_create_from_date_string("7 days")))->format('Y-m-d');
        $pendingDate  = (date_sub($currentDate, date_interval_create_from_date_string("10 days")))->format('Y-m-d');

        $cartFiles     = $this->tbPedido->getByStatusAndDate(TbPedido::STATUS_CART, $cartDate);
        $cancelFiles   = $this->tbPedido->getByStatusAndDate(TbPedido::STATUS_CANCELED, $cancelDate);
        $deliveryFiles = $this->tbPedido->getByStatusAndDate(TbPedido::STATUS_DELIVERY, $deliveryDate);
        $pendingFiles  = $this->tbPedido->getByStatusAndDate(TbPedido::STATUS_PENDING, $pendingDate);

        return [
            TbPedido::STATUS_CART     => $cartFiles,
            TbPedido::STATUS_CANCELED => $cancelFiles,
            TbPedido::STATUS_DELIVERY => $deliveryFiles,
            TbPedido::STATUS_PENDING  => $pendingFiles
        ];
    }

    public function geCustomerFromOrder($orderId)
    {
        return $this->tbPedido->getCustomer($orderId);
    }

    public function updateItemObservation($itemId, $observation)
    {
        return $this->tbItem->updateUserObservation($itemId, $observation);
    }

    public function getItemObservation($itemId)
    {
        $data = $this->tbItem->get($itemId);

        return [
            "observation" => $data['obsCliente'],
            "date" => $data['obsMomentoSalvamento']
        ];
    }

    public function isFileRegistered(string $fileName)
    {
        return (!empty($this->tbArquivo->checkIsFileRegistered($fileName)));
    }

    public function getPhoneNumberById(int $id)
    {
        $order = $this->tbPedido->get($id);
        return $order['telEntrega'];
    }

    public function getPendingToNotify()
    {
        $currentDate = new \DateTime();
        $currentDate->sub(new \DateInterval('P1D'));
        $strNotifyDate = $currentDate->format('Y-m-d');

        return $this->tbPedido->getPendingToNotify($strNotifyDate);
    }
}
