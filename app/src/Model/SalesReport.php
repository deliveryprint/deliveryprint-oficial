<?php

namespace IntecPhp\Model;

use IntecPhp\Entity\OrderReportEntity;

class SalesReport
{
    private $orderEntity;

    const COLOR_ID   = 2;

    public function __construct(OrderReportEntity $orderEntity)
    {
        $this->orderEntity = $orderEntity;
    }

    public function getData(string $startPeriod, string $endPeriod)
    {
        $colors        = $this->getColors($startPeriod, $endPeriod);
        $finishes      = $this->getFinishes($startPeriod, $endPeriod);
        $averagePages  = $this->getAveragePages($startPeriod, $endPeriod);
        $paidValue     = $this->getPaidValue($startPeriod, $endPeriod);
        $canceledValue = $this->getCanceledValue($startPeriod, $endPeriod);
        $itemsCost     = $this->calculateItemsCost($startPeriod, $endPeriod);
        $itemsPrice    = $this->calculateItemsPrice($startPeriod, $endPeriod);
        $averageTicket = $this->calculateAverageTicket($startPeriod, $endPeriod);
        $rangeValues   = $this->calculateRangeTotal($itemsPrice, $itemsCost);
        $billingResult = $this->calculateBillingAndCosts($itemsPrice, $itemsCost);
        $periodSummary = $this->calculatePeriodSummary($startPeriod, $endPeriod);
        $deliveryTypes = $this->countDeliveryTypes($startPeriod, $endPeriod);
        $salesData     = $this->getSalesDataByDate($startPeriod, $endPeriod);

        return [
            'colors'         => $colors,
            'finishes'       => $finishes,
            'average_pages'  => $averagePages,
            'paid_value'     => $paidValue,
            'canceled_value' => $canceledValue,
            'items_cost'     => $itemsCost,
            'items_price'    => $itemsPrice,
            'average_ticket' => $averageTicket,
            'price_range'    => $rangeValues,
            'billing_result' => $billingResult,
            'period_summary' => $periodSummary,
            'delivery_types' => $deliveryTypes,
            'sales_data'     => $salesData
        ];
    }

    public function getPeriodProfit(string $startPeriod, string $endPeriod)
    {
        $itemsPrice = $this->calculateItemsPrice($startPeriod, $endPeriod);
        $itemsCost  = $this->calculateItemsCost($startPeriod, $endPeriod);

        return $this->calculateBillingAndCosts($itemsPrice, $itemsCost);
    }

    public function getRangesData(string $startPeriod, string $endPeriod)
    {
        $itemsCost     = $this->calculateItemsCost($startPeriod, $endPeriod);
        $itemsPrice    = $this->calculateItemsPrice($startPeriod, $endPeriod);
        $rangeValues   = $this->calculateRangeTotal($itemsPrice, $itemsCost);

        return $rangeValues;
    }

    public function getSalesDataByDate(string $startPeriod, string $endPeriod) {
        $formattedItems = [];
        $items = $this->orderEntity->getSalesDataByDate($startPeriod, $endPeriod);

        foreach ($items as $item) {
            $date = '' . $item['date'];
            $totalCost = round((float)$item['total_cost'], 2);
            $totalValue = round((float)$item['total_value'], 2);
            $totalSales = $item['total_sales'];
            $formattedItems[$date] = [
                "totalCost" => $totalCost,
                "totalValue" => $totalValue,
                "totalSales" => $totalSales,
                "date" => $date
            ];
        }

        return $formattedItems;
    }

    public function getColors(string $startPeriod, string $endPeriod)
    {
        $colors = $this->orderEntity->countColorsTypePaid($startPeriod, $endPeriod);

        if (!$colors) {
            return [
               "pb_sulfite_a4_75g" => 0,
               "c_sulfite_a4_75g"  => 0,
               "pb_sulfite_a4_90g" => 0,
               "c_sulfite_a4_90g"  => 0,
               "pb_couche_a4_115g" => 0,
               "c_couche_a4_115g"  => 0,
               "pb_couche_a4_170g" => 0,
               "c_couche_a4_170g"  => 0,
               "pb_oficio_75g"     => 0,
               "c_oficio_75g"      => 0,
               "pb_eco_a4_75g"     => 0,
               "c_eco_a4_75g"      => 0,
               "pb_carta_75g"      => 0,
               "c_carta_75g"       => 0,
               "pb_sulfite_a3_75g" => 0,
               "c_sulfite_a3_75g"  => 0,
               "pb_couche_a3_150g" => 0,
               "c_couche_a3_150g"  => 0,
               "pb_sulfite_a5_75g" => 0,
               "c_sulfite_a5_75g"  => 0
            ];
        }

        $keys = array_keys($colors);
        foreach ($keys as $key) {
            $colors[$key] = $colors[$key] ? (int)$colors[$key] : 0;
        }

        return $colors;
    }

    public function getFinishes(string $startPeriod, string $endPeriod)
    {
        $finishes = $this->orderEntity->countFinishesTypePaid($startPeriod, $endPeriod);

        if (!$finishes) {
            return [
                "solta"        => 0,
                "grampeado"    => 0,
                "espiral"      => 0,
                "capa_dura"    => 0,
                "capa_dura_a3" => 0,
                "wire_o"       => 0,
                "total"        => 0
            ];
        }

        $keys  = array_keys($finishes);
        $total = 0;
        foreach ($keys as $key) {
            $finishes[$key] = $finishes[$key] ? (int)$finishes[$key] : 0;
            $total += $finishes[$key];
        }
        $finishes['total'] = $total;

        return $finishes;
    }

    public function getCoupons(string $startPeriod, string $endPeriod)
    {
        $coupons = $this->orderEntity->countCouponTotals($startPeriod, $endPeriod);

        if (!$coupons) {
            return [];
        }

        return $coupons;
    }

    public function getInviteCouponsUsed(string $startPeriod, string $endPeriod)
    {
        $coupons = $this->orderEntity->getInviteCouponsUsed($startPeriod, $endPeriod);

        if (!$coupons) {
            return [];
        }

        return $coupons;
    }

    private function getAveragePages(string $startPeriod, string $endPeriod)
    {
        $averagePages  = $this->orderEntity->countAveragePagesPaid($startPeriod, $endPeriod);

        if (!$averagePages) {
            return [
                "average_overall"     => 0,
                "average_black_white" => 0,
                "average_colorful"    => 0
            ];
        }

        $keys = array_keys($averagePages);
        foreach ($keys as $key) {
            $averagePages[$key] = $averagePages[$key] ? round((float)$averagePages[$key], 2) : 0;
        }

        return $averagePages;
    }

    private function getPaidValue(string $startPeriod, string $endPeriod)
    {
        $paidValue = $this->orderEntity->calcPaidValue($startPeriod, $endPeriod);
        $paidValue = $paidValue ? round((float)$paidValue['total_paid'], 2) : 0;

        return $paidValue;
    }

    private function getCanceledValue(string $startPeriod, string $endPeriod)
    {
        $canceledValue = $this->orderEntity->calcCanceledValue($startPeriod, $endPeriod);
        $canceledValue = $canceledValue ? round((float)$canceledValue['total_canceled'], 2) : 0;

        return $canceledValue;
    }

    private function calculateItemsCost(string $startPeriod, string $endPeriod)
    {
        $formattedItems = [];
        $items = $this->orderEntity->calculateItemsCost($startPeriod, $endPeriod);

        foreach ($items as $item) {
            $itemId = '' . $item['idItem'];
            $deliveryValue = 0;
            if (isset($item['delivery_id'])) {
                $deliveryValue = $item['delivery_value'];
            } else {
                $deliveryValue = $item['withdrawal_value'];
            }
            $item['item_cost'] = $item['item_cost'] + $deliveryValue;
            $itemValue = round((float)$item['item_cost'], 2);
            $formattedItems[$itemId]['value'] = $itemValue;
            $formattedItems[$itemId]['data']  = (new \DateTime($item['dataPedido']))->format('d-m-Y');
        }

        return $formattedItems;
    }

    private function calculateItemsPrice(string $startPeriod, string $endPeriod)
    {
        $formattedItems = [];
        $items = $this->orderEntity->calculateItemsPrice($startPeriod, $endPeriod);

        foreach ($items as $item) {
            $itemId = '' . $item['idItem'];
            $itemValue = round((float)$item['item_price'], 2);

            // Calcualar o valor cobrado pelas páginas, com desconto aplicado, sem considerar acabamento e frete
            $finishingPrice = $item['valorAcabamento'] * $item['qdeAcabamentos'];
            $totalPrice = $item['valorItem'] + $item['descTotal'];
            $finishingDiscount = ($finishingPrice / $totalPrice) * $item['descTotal'];
            $finishingDiscounted = $finishingPrice - $finishingDiscount;
            $pricePages = $item['valorItem'] - $finishingDiscounted;

            // Formatar para devolver para o frontend
            $formattedItems[$itemId]['value'] = $itemValue;
            $formattedItems[$itemId]['valuePages'] = $pricePages;
            $formattedItems[$itemId]['data']  = (new \DateTime($item['dataPedido']))->format('d-m-Y');
        }

        return $formattedItems;
    }

    public function calculateAverageTicket(string $startPeriod, string $endPeriod)
    {
        $averageTicket = $this->orderEntity->calculateAverageTicket($startPeriod, $endPeriod);
        if (!$averageTicket) {
            return [
                'billing'      => 0,
                'total_orders' => 0
            ];
        }
        $billing      = round((float)$averageTicket['total_orders_value'], 2);
        $total_orders = (int)$averageTicket['total_orders'];

        return [
            'billing'      => $billing,
            'total_orders' => $total_orders
        ];
    }

    public function getCustomerSales(string $startPeriod, string $endPeriod)
    {
        $newCustomers = $this->orderEntity->getNewCustomersInPeriod($startPeriod, $endPeriod);
        $recurringCustomers = $this->orderEntity->getRecurringCustomersInPeriod($startPeriod, $endPeriod);

        return [
            'newCustomers'       => $newCustomers,
            'recurringCustomers' => $recurringCustomers
        ];
    }

    private function formatOrderFrequency( $numOrders, $totalDays )
    {
        if ($numOrders == 0) {
            return "Nenhum pedido no periodo";
        } else if ($numOrders == 1) {
            return "Apenas 1 pedido";
        }

        if ($totalDays == 0) {
            $totalDays = 1;
        }

        $frequency = round($numOrders / $totalDays, 1);

        if ($frequency < 1) {
            return "1 pedido a cada " . round($totalDays / $numOrders, 1) . " dias";
        } else if ($frequency == 1) {
            return "1 pedido por dia";
        } else {
            return $frequency . " pedidos por dia";
        }
    }

    public function generateLtvTable(string $startPeriod, string $endPeriod)
    {
        $fileName = 'ltv_' . date('d-m-Y H:i:s') . '.csv';

        if (($output = fopen("/tmp/$fileName", 'w')) === false) {
            throw new \Exception("Erro ao tentar criar o arquivo CSV");
        }

        $customersOrders = $this->orderEntity->getCustomersOrders($startPeriod, $endPeriod);
        $ltvTable = [];

        foreach($customersOrders as $order)
        {
            // Id do Cliente
            $customerId = $order['idCliente'];

            // Datas importantes
            $ingresso = new \DateTime($order['dtIngresso']);
            $dataPedido = new \DateTime($order['dataPedido']);

            // Encontrar o periodo correto para calcular a frequencia de pedidos
            if (!empty($startPeriod)) {
                $startPeriodDt = new \DateTime($startPeriod);
                if ($ingresso > $startPeriodDt) {
                    $startDate = $ingresso;
                } else {
                    $startDate = $startPeriodDt;
                }
            } else {
                $startDate = $ingresso;
            }

            if (!empty($endPeriod)) {
                $endDate = new \DateTime($endPeriod);
            } else {
                $endDate = new \DateTime();
            }

            $interval = $endDate->diff($startDate);
            $totalDays = $interval->days;

            if (!isset($ltvTable[$customerId]))
            {
                $ltvTable[$customerId] = [
                    'nomeCliente'        => $order['nomeCliente'],
                    'emailCliente'       => $order['emailCliente'],
                    'telCliente'         => $order['telEntrega'],
                    'dtIngresso'         => $ingresso,
                    'dataPrimeiraCompra' => $dataPedido,
                    'idPrimeiraCompra'   => $order['idPedido'],
                    'dataUltimaCompra'   => $dataPedido,
                    'idUltimaCompra'     => $order['idPedido'],
                    'frequencia'         => $this->formatOrderFrequency(1, $totalDays),
                    'ticketMedio'        => round($order['valor_pedido'], 2),
                    'valorTotal'         => $order['valor_pedido'],
                    'numPedidos'         => 1
                ];
            }
            else
            {
                // Encontrar a primeira e a ultima compras
                $dataPrimeiroPedido = $ltvTable[$customerId]['dataPrimeiraCompra'];
                $dataUltimoPedido = $ltvTable[$customerId]['dataUltimaCompra'];
                if ($dataPedido < $dataPrimeiroPedido) {
                    $ltvTable[$customerId]['dataPrimeiraCompra'] = $dataPedido;
                    $ltvTable[$customerId]['idPrimeiraCompra'] = $order['idPedido'];
                }
                if ($dataPedido > $dataUltimoPedido) {
                    $ltvTable[$customerId]['dataUltimaCompra'] = $dataPedido;
                    $ltvTable[$customerId]['idUltimaCompra'] = $order['idPedido'];
                }

                // Atualizar os totais do cliente
                $valorTotal = $ltvTable[$customerId]['valorTotal'];
                $numPedidos = $ltvTable[$customerId]['numPedidos'];
                $valorTotal += $order['valor_pedido'];
                $numPedidos += 1;

                // Preencher a linha na tabela ltv
                $ltvTable[$customerId]['valorTotal']  = $valorTotal;
                $ltvTable[$customerId]['numPedidos']  = $numPedidos;
                $ltvTable[$customerId]['frequencia']  = $this->formatOrderFrequency($numPedidos, $totalDays);
                $ltvTable[$customerId]['ticketMedio'] = round($valorTotal / $numPedidos, 2);
            }
        }

        fputcsv($output, ['Planílha de LTV'], ';');
        fputcsv($output, [
            'Nome', 'E-mail', 'Telefone', 'Data de Criação', 'Total de Pedidos', 'Primeiro Pedido', 'Último Pedido', 'Frequência', 'Ticket Médio'
        ], ';');

        foreach ($ltvTable as $row)
        {
            $dtIngresso = $row['dtIngresso']->format('d/m/Y H:i:s');
            $dataPrimeiraCompra = $row['dataPrimeiraCompra']->format('d/m/Y H:i:s');
            $dataUltimaCompra = $row['dataUltimaCompra']->format('d/m/Y H:i:s');
            fputcsv($output, [
                $row['nomeCliente'],
                $row['emailCliente'],
                $row['telCliente'],
                $dtIngresso,
                $row['numPedidos'],
                $dataPrimeiraCompra,
                $dataUltimaCompra,
                $row['frequencia'],
                $row['ticketMedio']
            ], ';');
        }

        fputcsv($output, ["\n"], ';');

        fclose($output);
        return $fileName;
    }

    // Função usada para ordernar o array de ranges em ordem do de maior quantidade de pedidos para o de menor
    private static function compareRangesValues($a, $b)
    {
        if ($a['num_items'] == $b['num_items'])
        {
            return 0;
        }
        return ($a['num_items'] < $b['num_items']) ? 1 : -1;
    }

    public function calculateRangeTotal(array $itemsPrice, array $itemsCost)
    {
        $rangeValues = [];

        foreach ($itemsPrice as $id => $itemPrice) {
            $itemInfo = $this->orderEntity->getItemPriceRangeInfo((int)$id);
            if (!$itemInfo) {
                throw new \Exception("Não foram encontradas informações do item");
            }

            $hasColor = $this->hasColor($itemInfo['colorId']);
            $priceRangeInfo = $this->orderEntity->getPriceRangeInfo($itemInfo['paperId'], $itemInfo['numPages'], $hasColor);

            if (!$priceRangeInfo) {
                throw new \Exception("Não foram encontradas informações da faixa de preços");
            }

            $rangeId = '' . $priceRangeInfo['idFaixa'];
            $colorName = $hasColor ? 'Colorido' : 'Preto e Branco';
            if (isset($rangeValues[$rangeId])) {
                $cost        = $rangeValues[$rangeId]['items_cost'];
                $price       = $rangeValues[$rangeId]['items_price'];
                $pagesPrice  = $rangeValues[$rangeId]['valuePages'];
                $numItems    = $rangeValues[$rangeId]['num_items'];

                $cost       += $itemsCost[$id]['value'];
                $price      += $itemsPrice[$id]['value'];
                $pagesPrice += $itemsPrice[$id]['valuePages'];
                $numItems   += 1;
                $result      = $price - $cost;

                $rangeValues[$rangeId]['items_cost']  = round($cost, 2);
                $rangeValues[$rangeId]['items_price'] = round($price, 2);
                $rangeValues[$rangeId]['valuePages']  = round($pagesPrice, 2);
                $rangeValues[$rangeId]['num_items']   = round($numItems, 2);
                $rangeValues[$rangeId]['result']      = round($result, 2);
            } else {
                $rangeValues[$rangeId]['paper_type']  = $itemInfo['paper_type'];
                $rangeValues[$rangeId]['color']       = $colorName;
                $rangeValues[$rangeId]['num_pages']   = '' . $priceRangeInfo['de'] . ' - ' . $priceRangeInfo['ate'] . ' pags';
                $rangeValues[$rangeId]['num_items']   = 1;
                $rangeValues[$rangeId]['items_cost']  = $itemsCost[$id]['value'];
                $rangeValues[$rangeId]['items_price'] = $itemsPrice[$id]['value'];
                $rangeValues[$rangeId]['valuePages']  = $itemsPrice[$id]['valuePages'];
                $result = $itemsPrice[$id]['value'] - $itemsCost[$id]['value'];
                $rangeValues[$rangeId]['result']      = round($result, 2);
            }
        }

        usort($rangeValues, array('IntecPhp\Model\SalesReport', 'compareRangesValues'));

        return $rangeValues;
    }

    private function hasColor(int $colorId)
    {
        $hasColor = ($colorId == self::COLOR_ID) ? 1 : 0;
        return $hasColor;
    }

    private function calculateBillingAndCosts(array $itemsPrice, array $itemsCost)
    {
        $costs  = 0;
        $prices = 0;
        $result = 0;

        foreach ($itemsPrice as $id => $itemPrice) {
            $costs  += $itemsCost[$id]['value'];
            $prices += $itemsPrice[$id]['value'];
        }

        $result = $prices - $costs;

        return [
            'billing' => round($prices, 2),
            'costs'   => round($costs, 2),
            'result'  => round($result, 2)
        ];
    }

    private function calculatePeriodSummary(string $startPeriod, string $endPeriod)
    {
        $summary = $this->orderEntity->calculatePeriodSummary($startPeriod, $endPeriod);

        if (empty($summary)) {
            return [
                'black_white' => [
                    'num_pages' => 0,
                    'value'     => 0
                ],
                'colorful' => [
                    'num_pages' => 0,
                    'value'     => 0
                ],
                'finishes' => [
                    'total' => 0,
                    'value' => 0
                ],
                'total_value' => 0
            ];
        }

        $blackWhite = [];
        $colorful   = [];
        $finishes   = [];
        $totalValue = 0;

        $blackWhite['num_pages'] = (int)$summary['total_pages_pb'];
        $blackWhite['value']     = round((float)$summary['total_value_pb'], 2);

        $colorful['num_pages']  = (int)$summary['total_pages_c'];
        $colorful['value']      = round((float)$summary['total_value_c'], 2);

        $finishes['total']      = (int)$summary['total_finishes'];
        $finishes['value']      = round((float)$summary['total_value_finishes'], 2);

        $totalValue = $blackWhite['value'] + $colorful['value'] + $finishes['value'];

        return [
            'black_white' => $blackWhite,
            'colorful'    => $colorful,
            'finishes'    => $finishes,
            'total_value' => round($totalValue, 2)
        ];
    }

    public function countDeliveryTypes(string $startPeriod, string $endPeriod)
    {
        $deliveryTypes = $this->orderEntity->countDeliveryTypes($startPeriod, $endPeriod);

        foreach ($deliveryTypes as $key => $deliveryType) {
            $deliveryTypes[$key] = (int)$deliveryType;
        }

        return $deliveryTypes;
    }

    public function generateSalesReportCsv(string $startPeriod, string $endPeriod)
    {
        $fileName = 'vendas_' . date('d-m-Y H:i:s') . '.csv';

        if (($output = fopen("/tmp/$fileName", 'w')) === false) {
            throw new \Exception("Erro ao tentar criar o arquivo CSV");
        }

        $colors        = $this->getColors($startPeriod, $endPeriod);
        $this->writeColorsInfo($output, $colors);
        fputcsv($output, ["\n"], ';');

        $finishes      = $this->getFinishes($startPeriod, $endPeriod);
        $this->writeFinishesInfo($output, $finishes);
        fputcsv($output, ["\n"], ';');

        $averagePages  = $this->getAveragePages($startPeriod, $endPeriod);
        $this->writeAveragePagesInfo($output, $averagePages);
        fputcsv($output, ["\n"], ';');

        $paidValue     = $this->getPaidValue($startPeriod, $endPeriod);
        $canceledValue = $this->getCanceledValue($startPeriod, $endPeriod);
        $this->writePaidAndCanceledInfo($output, $paidValue, $canceledValue);
        fputcsv($output, ["\n"], ';');

        $periodSummary = $this->calculatePeriodSummary($startPeriod, $endPeriod);
        $this->writePeriodSummaryInfo($output, $periodSummary);
        fputcsv($output, ["\n"], ';');

        $averageTicket = $this->calculateAverageTicket($startPeriod, $endPeriod);
        $this->writeAverageTicketInfo($output, $averageTicket);
        fputcsv($output, ["\n"], ';');

        $itemsPrice    = $this->calculateItemsPrice($startPeriod, $endPeriod);
        $itemsCost     = $this->calculateItemsCost($startPeriod, $endPeriod);
        $billingResult = $this->calculateBillingAndCosts($itemsPrice, $itemsCost);
        $this->writeBillingAndCostsInfo($output, $billingResult);
        fputcsv($output, ["\n"], ';');

        $deliveryTypes = $this->countDeliveryTypes($startPeriod, $endPeriod);
        $this->writeDeliveryTypesInfo($output, $deliveryTypes);
        fputcsv($output, ["\n"], ';');

        $this->writePeriodItemsInfo($output, $itemsPrice);
        fputcsv($output, ["\n"], ';');

        $rangeValues   = $this->calculateRangeTotal($itemsPrice, $itemsCost);
        $this->writeRangeTotalInfo($output, $rangeValues);

        fclose($output);
        return $fileName;
    }

    private function writeColorsInfo($output, array $colors)
    {
        fputcsv($output, ['Quantidade de Páginas compradas neste período'], ';');
        fputcsv($output, ['Tipo', 'Preto e Branco', 'Colorido', 'TOTAL DE PÁGINAS'], ';');
        fputcsv($output, ['Papel Sulfite A4 75g', $colors['pb_sulfite_a4_75g'], $colors['c_sulfite_a4_75g'], $colors['pb_sulfite_a4_75g'] + $colors['c_sulfite_a4_75g']], ';');
        fputcsv($output, ['Papel Sulfite A4 90g', $colors['pb_sulfite_a4_90g'], $colors['c_sulfite_a4_90g'], $colors['pb_sulfite_a4_90g'] + $colors['c_sulfite_a4_90g']], ';');
        fputcsv($output, ['Papel Couché A4 115g', $colors['pb_couche_a4_115g'], $colors['c_couche_a4_115g'], $colors['pb_couche_a4_115g'] + $colors['c_couche_a4_115g']], ';');
        fputcsv($output, ['Papel Couché A4 170g', $colors['pb_couche_a4_170g'], $colors['c_couche_a4_170g'], $colors['pb_couche_a4_170g'] + $colors['c_couche_a4_170g']], ';');
        fputcsv($output, ['Papel Ofício 75g', $colors['pb_oficio_75g'], $colors['c_oficio_75g'], $colors['pb_oficio_75g'] + $colors['c_oficio_75g']], ';');
        fputcsv($output, ['Papel Eco A4 75g', $colors['pb_eco_a4_75g'], $colors['c_eco_a4_75g'], $colors['pb_eco_a4_75g'] + $colors['c_eco_a4_75g']], ';');
        fputcsv($output, ['Papel Carta 75g', $colors['pb_carta_75g'], $colors['c_carta_75g'], $colors['pb_carta_75g'] + $colors['c_carta_75g']], ';');
        fputcsv($output, ['Papel Sulfite A3 75g', $colors['pb_sulfite_a3_75g'], $colors['c_sulfite_a3_75g'], $colors['pb_sulfite_a3_75g'] + $colors['c_sulfite_a3_75g']], ';');
        fputcsv($output, ['Papel Couché A3 150g', $colors['pb_couche_a3_150g'], $colors['c_couche_a3_150g'], $colors['pb_couche_a3_150g'] + $colors['c_couche_a3_150g']], ';');
        fputcsv($output, ['Papel Sulfite A5 75g', $colors['pb_sulfite_a5_75g'], $colors['c_sulfite_a5_75g'], $colors['pb_sulfite_a5_75g'] + $colors['c_sulfite_a5_75g']], ';');
    }

    private function writeFinishesInfo($output, array $finishes)
    {
        fputcsv($output, ['Quantidade de Acabamentos compradas neste período'], ';');
        fputcsv($output, ['TIPO', 'QTD'], ';');
        fputcsv($output, ['Folha Solta', $finishes['solta']], ';');
        fputcsv($output, ['Grampeado', $finishes['grampeado']], ';');
        fputcsv($output, ['Encadernar Espiral', $finishes['espiral']], ';');
        fputcsv($output, ['Encadernar Capa Dura A4', $finishes['capa_dura']], ';');
        fputcsv($output, ['Encadernar Capa Dura A3', $finishes['capa_dura_a3']], ';');
        fputcsv($output, ['Encadernar Wire-o', $finishes['wire_o']], ';');
        fputcsv($output, ['TOTAL', $finishes['total']], ';');
    }

    private function writeAveragePagesInfo($output, array $averagePages)
    {
        fputcsv($output, ['Média de Páginas compradas por dia'], ';');
        fputcsv($output, ['TIPO', 'QTD'], ';');
        fputcsv($output, ['Preto e Branco', $averagePages['average_black_white']], ';');
        fputcsv($output, ['Colorido', $averagePages['average_colorful']], ';');
        fputcsv($output, ['Total', $averagePages['average_overall']], ';');
    }

    private function writePaidAndCanceledInfo($output, float $paidValue, float $canceledValue)
    {
        fputcsv($output, ['Faturamento X Cancelado'], ';');
        fputcsv($output, ['TIPO', 'QTD'], ';');
        fputcsv($output, ['Compras Realizadas', $paidValue], ';');
        fputcsv($output, ['Pedidos Cancelados', $canceledValue], ';');
    }

    private function writePeriodSummaryInfo($output, array $periodSummary)
    {
        fputcsv($output, ['Resumo do Periodo'], ';');
        fputcsv($output, ['TIPO', 'QTD DE PÁGINAS COMPRADAS', 'VALOR TOTAL FATURADO'], ';');
        fputcsv($output, ['Preto e Branco', $periodSummary['black_white']['num_pages'], $periodSummary['black_white']['value']], ';');
        fputcsv($output, ['Colorido', $periodSummary['colorful']['num_pages'], $periodSummary['colorful']['value']], ';');
        fputcsv($output, ['Acabamentos', $periodSummary['finishes']['total'], $periodSummary['finishes']['value']], ';');
        fputcsv($output, ['Total Faturado', '', $periodSummary['total_value']], ';');
    }

    private function writeAverageTicketInfo($output, array $averageTicket)
    {
        fputcsv($output, ['Ticket Médio de Faturamento Diário'], ';');
        fputcsv($output, ['TIPO', 'QTD DE PEDIDOS NO PERIODO', 'TICKET MÉDIO'], ';');
        fputcsv($output, ['Faturamento', $averageTicket['total_orders'], $averageTicket['billing']], ';');
    }

    private function writeBillingAndCostsInfo($output, array $billingResult)
    {
        fputcsv($output, ['Resultado do Período'], ';');
        fputcsv($output, ['FATURADO', 'CUSTO', 'RESULTADO'], ';');
        fputcsv($output, [$billingResult['billing'], $billingResult['costs'], $billingResult['result']], ';');
    }

    private function writeRangeTotalInfo($output, array $rangeValues)
    {
        fputcsv($output, ['Quantidade de itens comprados por faixas'], ';');
        fputcsv($output, ['Id Faixa', 'Tipo de Papel', 'Cor', 'Qtd de Pág', 'Nº Itens', 'Faturamento', 'Custo', 'Resultado'], ';');
        foreach ($rangeValues as $id => $rv) {
            fputcsv($output, [$id, $rv['paper_type'], $rv['color'], $rv['num_pages'], $rv['num_items'], $rv['items_price'], $rv['items_cost'], $rv['result']], ';');
        }
    }

    private function writeDeliveryTypesInfo($output, array $deliveryTypes)
    {
        fputcsv($output, ['Quantidade de "tipos de Entrega" no Período'], ';');
        fputcsv($output, ['TIPO', 'VEZES COMPRADAS'], ';');
        fputcsv($output, ['ENTREGA TIPO 1', $deliveryTypes['total_delivery_1']], ';');
        fputcsv($output, ['ENTREGA TIPO 2', $deliveryTypes['total_delivery_2']], ';');
        fputcsv($output, ['ENTREGA TIPO 3', $deliveryTypes['total_delivery_3']], ';');
        fputcsv($output, ['ENTREGA TIPO 4', $deliveryTypes['total_delivery_4']], ';');
        fputcsv($output, ['ENTREGA TIPO 5', $deliveryTypes['total_delivery_5']], ';');
        fputcsv($output, ['ENTREGA TIPO 6', $deliveryTypes['total_delivery_6']], ';');
        fputcsv($output, ['ENTREGA TIPO 7', $deliveryTypes['total_delivery_7']], ';');
        fputcsv($output, ['ENTREGA TIPO 8', $deliveryTypes['total_delivery_8']], ';');
        fputcsv($output, ['RETIRADA TIPO 1', $deliveryTypes['total_withdrawal_1']], ';');
        fputcsv($output, ['RETIRADA TIPO 2', $deliveryTypes['total_withdrawal_2']], ';');
        fputcsv($output, ['RETIRADA TIPO 3', $deliveryTypes['total_withdrawal_3']], ';');
        fputcsv($output, ['RETIRADA TIPO 4', $deliveryTypes['total_withdrawal_4']], ';');
    }

    private function writePeriodItemsInfo($output, array $itemsPrice)
    {
        fputcsv($output, ['Pedidos que entraram nesse período'], ';');
        fputcsv($output, ['ID', 'VALOR', 'DATA'], ';');
        foreach ($itemsPrice as $id => $ip) {
            fputcsv($output, [$id, $ip['value'], $ip['data']], ';');
        }
    }
}
