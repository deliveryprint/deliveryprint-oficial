<?php

namespace IntecPhp\Model;

use IntecPhp\Model\PDF;
use IntecPhp\Entity\TbPedido;
use IntecPhp\Entity\TbItem;
use IntecPhp\Entity\TbEntrega;
use IntecPhp\Entity\TbArquivo;
use IntecPhp\Entity\TbFrete;
use IntecPhp\Entity\TbRetirada;
use IntecPhp\Entity\TbCliente;
use Exception;

class ShoppingCart
{
    private $orderEnt;
    private $itemEnt;
    private $deliveryEnt;
    private $fileEnt;
    private $freightEnt;
    private $withdrawalEnt;
    private $clientEnt;
    private $pdf;

    public function __construct(
        TbPedido $orderEnt, TbItem $itemEnt, TbEntrega $deliveryEnt,
        TbArquivo $fileEnt, TbFrete $freightEnt, TbRetirada $withdrawalEnt,
        TbCliente $clientEnt, PDF $pdf
    )
    {
        $this->orderEnt = $orderEnt;
        $this->itemEnt = $itemEnt;
        $this->deliveryEnt = $deliveryEnt;
        $this->fileEnt = $fileEnt;
        $this->freightEnt = $freightEnt;
        $this->withdrawalEnt = $withdrawalEnt;
        $this->clientEnt = $clientEnt;
        $this->pdf = $pdf;
    }

    public function getCartData($orderId, $userId)
    {
        $orderData = $this->orderEnt->getById($orderId);
        if($orderData) {
            $idx = count($orderData) - 1;
            $orderData = $orderData[$idx];
        }
        $items = $this->itemEnt->getByOrderId($orderId);

        $address = [
            'logradouro' => '',
            'bairro' => '',
            'cidade' => '',
            'cep' => '',
        ];

        $clientData = [
            'nomeCliente' => '',
            'telCliente' => '',
            'emailCliente' => ''
        ];

        if($userId && ($userData = $this->clientEnt->get($userId))){
            $clientData = [
                'nomeCliente' => $userData['nomeCliente'] ?? '',
                'telCliente' => $userData['telCliente'] ?? $userData['celCliente'] ?? '',
                'emailCliente'=> $userData['emailCliente'] ?? ''
            ];
        }

        $discount = 0;

        foreach ($items as &$item) {
            $delivery = $this->deliveryEnt->getWithAddressByItem($item['id']);
            $item['files'] = $this->fileEnt->filesOfItem($item['id']);

            // TODO
            foreach ($item['files'] as &$file) {
                $file['downloadUrl'] = $this->pdf->getFileUrl($file['arquivo']);
            }

            if (!$delivery['tbRetirada_idRetirada']) {
                $address = [
                    'id' => $delivery['idEndereco'],
                    'cep' => $delivery['cep'],
                    'numero' => $delivery['numero'],
                    'logradouro' => $delivery['logradouro'],
                    'bairro' => $delivery['bairro'],
                    'cidade' => $delivery['cidade'],
                    'uf' => $delivery['uf'],
                    'complemento' => $delivery['complemento']
                ];
                $freight = $this->freightEnt->firstOfDelivery($delivery['idEntrega']);
                $delivery['forecast'] = $this->formatForecast(new \DateTime($freight['prazo']));
                $delivery['estimatedDeliveryDate'] = $this->formatEstimatedDeliveryDate(new \DateTime($freight['prazo']));
            } else {
                $withdrawal = $this->withdrawalEnt->get($delivery['tbRetirada_idRetirada']);
                $delivery['forecast'] = $this->formatForecast(new \DateTime($withdrawal['prazo']), true);
                $delivery['estimatedDeliveryDate'] = $this->formatEstimatedDeliveryDate(new \DateTime($withdrawal['prazo']));
            }

            $item['delivery'] = $delivery;
            $item['preco'] = (float)$delivery['delivery_value'] + (float)$item['valorItem'];
        }

        return [
            "pedido" => [
                "data" => $orderData['dataPedido'],
                "discountType" => $orderData['discountType'],
                "discountValue" => $orderData['discountValue'],
                "tbEmailMkt_idEmailMkt" => $orderData['tbEmailMkt_idEmailMkt'],
                "id"   => $orderId
            ],
            "items" => $items,
            "endereco" => $address,
            "dadosCliente" => $clientData,
            "cartao" => [
                "bandeira" => $orderData['cardBanner'],
                "finalNum" => $orderData['endCardNumber']
            ]
        ];
    }

    private function formatForecast(\DateTime $forecast, $isWithdrawal = false)
    {
        $f = clone $forecast;
        $today = new \DateTime();

        $f->setTime(0, 0, 0);
        $today->setTime(0, 0, 0);

        $diff = $today->diff($f);
        $diffDays = (int)$diff->format('%R%a');

        $connective = $isWithdrawal ? ' após ' : ' até ';

        if(!$diffDays) {
            return 'Hoje' . $connective . $forecast->format('H') . 'h';
        }

        return $forecast->format('d/m') . $connective . $forecast->format('H') . 'h';
    }

    private function formatEstimatedDeliveryDate(\DateTime $prazo)
    {
        return $prazo->format('Y-m-d');
    }

    public function countOrderItems($orderId)
    {
        return $this->itemEnt->count($orderId);
    }
}
