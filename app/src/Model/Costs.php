<?php

namespace IntecPhp\Model;

use IntecPhp\Entity\PrintCostEntity;
use IntecPhp\Entity\FinishCostEntity;

class Costs
{
    private $printCostEntity;
    private $finishCostEntity;

    public function __construct(PrintCostEntity $printCostEntity, FinishCostEntity $finishCostEntity)
    {
        $this->printCostEntity  = $printCostEntity;
        $this->finishCostEntity = $finishCostEntity;
    }

    public function getAllCosts()
    {
        $printCosts  = $this->getAllPrintCosts();
        $finishCosts = $this->getAllFinishCosts();

        return [
            'print_costs'  => $printCosts,
            'finish_costs' => $finishCosts
        ];
    }

    private function getAllPrintCosts()
    {
        $costs = $this->printCostEntity->getAllWithDesc();
        if (!$costs) {
            return [];
        }

        return $costs;
    }

    private function getAllFinishCosts()
    {
        $costs = $this->finishCostEntity->getAllWithDesc();
        if (!$costs) {
            return [];
        }

        return $costs;
    }

    public function updatePrintCosts(array $printCosts)
    {
        $this->printCostEntity->beginTransaction();

        foreach ($printCosts as $printCost) {
            if (!is_numeric($printCost['value']) || $printCost['value'] < 0) {
                throw new \Exception("Valor inválido de custo de impressão");
            }

            $this->printCostEntity->updateValueById($printCost['id'], $printCost['value']);
        }

        $this->printCostEntity->commit();
    }

    public function updateFinishCosts(array $finishCosts)
    {
        $this->finishCostEntity->beginTransaction();

        foreach ($finishCosts as $finishCost) {
            if (!is_numeric($finishCost['value']) || $finishCost['value'] < 0) {
                throw new \Exception("Valor inválido de custo de acabamento");
            }

            $this->finishCostEntity->updateValueById($finishCost['id'], $finishCost['value']);
        }

        $this->finishCostEntity->commit();
    }
}
