<?php

namespace IntecPhp\Model;

use IntecPhp\Entity\TbIp;

use Exception;

class Ip
{
    private $tbIp;

    const TIME_PERIOD = 3; // Horas
    const MAX_TRIES = 4;
    const CAPTCHA_TRIES = 1;

    public function __construct(TbIp $tbIp)
    {
        $this->tbIp = $tbIp;
    }

    private function timeIsBig($ip)
    {
        $lastTime = \DateTime::createFromFormat('Y-m-d H:i:s', $ip['lastTime']);
        $now = new \DateTime();
        $difference = $now->diff($lastTime);
        $hours = ($difference->days * 24) + $difference->h;
        if ($hours < self::TIME_PERIOD) {
            return false;
        } else {
            return true;
        }
    }

    public function getByAddress($ipAddress)
    {
        return $this->tbIp->getByAddress($ipAddress);
    }

    public function addNewTry($userIp, $remoteAddress, $cfAddress, $orderId)
    {
        $data = $this->tbIp->getByAddress($userIp);

        if (!empty($data)) {
            $ip = $data[0];
            if ($this->timeIsBig($ip)) {
                return $this->tbIp->update($userIp, $remoteAddress, $cfAddress, 1, $orderId);
            } else {
                $tries = $ip['tries'];
                return $this->tbIp->update($userIp, $remoteAddress, $cfAddress, ($tries + 1), $orderId);
            }
        } else {
            return $this->tbIp->add($userIp, $remoteAddress, $cfAddress, $orderId);
        }
    }

    public function maxTriesExceeded($ipAddress)
    {
        $data = $this->tbIp->getByAddress($ipAddress);

        if (!empty($data)) {
            $ip = $data[0];
            $tries = $ip['tries'];
            return ($tries > self::MAX_TRIES);
        } else {
            throw new Exception('IP não registrado');
        }
    }

    public function needCaptcha($ipAddress)
    {
        $data = $this->tbIp->getByAddress($ipAddress);

        if (!empty($data)) {
            $ip = $data[0];
            if ($this->timeIsBig($ip)) {
                return false;
            } else {
                $tries = $ip['tries'];
                return ($tries >= self::CAPTCHA_TRIES);
            }
        } else {
            return false;
        }
    }
}
