<?php

namespace IntecPhp\Model;

use IntecPhp\Entity\TbEndereco;

class CustomerAddress
{

    private $addrEnt;

    public function __construct(TbEndereco $addrEnt)
    {
        $this->addrEnt = $addrEnt;
    }

    public function getAddress($customerId)
    {
        $addrs = $this->addrEnt->getFromCustomer($customerId);

        if(empty($addrs)) {
            return [];
        }

        return array_map(function($addr) {
            return [
                'addrId' => $addr['idEndereco'],
                'zip' => $addr['cep'],
                'number' => $addr['numero'],
                'street' => $addr['logradouro'],
                'district' => $addr['bairro'],
                'city' => $addr['cidade'],
                'uf' => $addr['uf'],
                'compl' => $addr['complemento']
            ];
        }, $addrs);
    }

    public function updateAddress($addrId, $zip, $number, $street, $district, $city, $uf, $compl)
    {
        if(!$this->addrEnt->update($addrId, $zip, $number, $street, $district, $city, $uf, $compl)) {
            throw new \Exception('Não foi possível atualizar o endereço');
        }
    }

    public function addAddress($customerId, $zip, $number, $street, $district, $city, $uf, $compl)
    {
        $this->addrEnt->beginTransaction();

        $addrId = $this->addrEnt->add($customerId, $zip, $number, $street, $district, $city, $uf, $compl);
        if(!$addrId) {
            throw new \Exception('Não foi possível adicionar o endereço');
        }

        $this->addrEnt->commit();

        return $addrId;
    }
}
