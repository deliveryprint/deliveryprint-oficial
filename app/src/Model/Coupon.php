<?php

namespace IntecPhp\Model;

use IntecPhp\Entity\TbCupom;
use IntecPhp\Entity\TbCliente;
use IntecPhp\Model\Account;
use Exception;

class Coupon
{
    private $couponEnt;
    private $userEnt;

    public function __construct(TbCupom $couponEnt, TbCliente $userEnt)
    {
        $this->couponEnt = $couponEnt;
        $this->userEnt = $userEnt;
    }

    public function checkCoupon($couponCode, $idUser, \DateTime $expirationDate)
    {
        // Buscar o cupom que possua esse código e seja válido
        $cp = $this->couponEnt->checkCoupon($couponCode, $expirationDate->format('Y-m-d H:i:s'));

        if ($cp) {
            // Testar se o cupom já está esgotado
            if ($cp['cupom_counter'] >= $cp['cupom_limit']) {
                throw new \Exception('Cupom esgotado: ' . $cp['codigo'] . '.');
            }

            // Retornar com sucesso se não houver usuário logado ou se o cupom for válido para o usuário logado
            if (!$idUser) {
                return $cp;
            } else {
                // Testar se o usuário logado já utilizou o cupom todas as vezes que podia
                $counter = $this->couponEnt->countUserCouponItems($cp['idDesc'], $idUser);
                if ($counter > $cp['cupom_user_limit']) {
                    throw new \Exception('Você já utilizou este cupom: ' . $cp['codigo'] . '.');
                }

                // Se o cupom foi gerado pelo sistema de indicações, ele possuirá um campo tbCliente_idCliente
                if (isset($cp['tbCliente_idCliente'])) {
                    // Se o campo invite_coupon for verdadeiro, significa que é um cupom que serve para convidar outros clientes
                    // Se for falso, é um cupom gerado como recompensa quando um novo cliente utiliza um cupom de convite
                    if ($cp['invite_coupon'] == true) {
                        // Impedir que o cliente indique a delivery para si mesmo
                        if ($cp['tbCliente_idCliente'] == $idUser) throw new \Exception('Cupom inválido: ' . $cp['codigo'] . '.');

                        // Testar se essa compra é a primeira do usuário
                        if ($this->userEnt->hasOrders($idUser)) {
                            throw new \Exception('Esse cupom só é válido para a primeira compra: ' . $cp['codigo'] . '.');
                        }
                    } else {
                        // Permitir que apenas o cliente que indicou possa usar os cupons que receber como recompensa
                        if ($cp['tbCliente_idCliente'] != $idUser) throw new \Exception('Cupom inválido: ' . $cp['codigo'] . '.');
                    }
                }

                return $cp;
            }
        }

        return false;
    }

    public function save(
        string $code,
        float $value,
        \DateTime $startdate,
        \DateTime $expirationDate,
        string $type,
        int $cuponLimit,
        string $email,
        int $cuponUserLimit,
        int $commissionPercent,
        string $comment
    ) {
        if ($startdate > $expirationDate) {
            $expirationDate->add(new \DateInterval('P1Y'));
        }

        if ($commissionPercent < 0 && $commissionPercent > 100) {
            throw new Exception("Percentual de comissão ($commissionPercent) não é válido");
        }

        if ($value <= 0) {
            throw new Exception('Valor do cupom é inválido');
        }

        if ($this->couponEnt->exists($code)) {
            throw new Exception("O cupom $code já existe!");
        }

        $this->couponEnt->save(
            $code,
            $value,
            $startdate->format('Y-m-d H:i:s'),
            $expirationDate->format('Y-m-d H:i:s'),
            $type,
            $cuponLimit,
            $email,
            $cuponUserLimit,
            $commissionPercent,
            $comment
        );
    }

    public function find(int $status, string $codeOrEmail, int $hasEmail)
    {
        return $this->couponEnt->find($status, $codeOrEmail, $hasEmail);
    }

    public function remove(array $coupons)
    {
        $this->couponEnt->remove($coupons);
    }

    public function deactive($couponId)
    {
        $this->couponEnt->deactive($couponId);
    }

    public function active($couponId, \DateTime $startdate, \DateTime $enddate)
    {
        $this->couponEnt->active($couponId, $startdate->format('Y-m-d H:i:s'), $enddate->format('Y-m-d'));
    }

    public function details($couponId)
    {
        $couponDetails = $this->couponEnt->details($couponId);
        if (empty($couponDetails)) {
            return [
                'cities' => [],
                'state'  => []
            ];
        }
        $stateAndCity = $this->couponEnt->getStatesAndCitiesOf($couponId);
        $states = [];
        $cities = [];

        foreach ($stateAndCity as $s) {
            if (!in_array($s['uf'], $states)) {
                $states[] = $s['uf'];
            }

            if (!in_array($s['cidade'], $cities)) {
                $cities[] = $s['cidade'];
            }
        }

        $couponDetails['cities'] = $cities;
        $couponDetails['state'] = $states;

        return $couponDetails;
    }

    public function findUsed(int $status, string $codeOrEmail, int $hasEmail)
    {
        return $this->couponEnt->findUsed($status, $codeOrEmail, $hasEmail);
    }

    public function createCouponsCsv(array $coupons)
    {
        $delimiter = ';';

        $fileName = 'cupons_' . date('d-m-Y H:i:s') . '.csv';

        if (($output = fopen("/tmp/$fileName", 'w')) === false) {
            throw new \Exception("Erro ao tentar criar o arquivo CSV");
        }

        // gerando o cabeçalho das colunas
        fputcsv($output, ['Id do Pedido.Item', 'Código do Cupom', 'Valor do Desconto', 'Valor Total do Item', 'Desconto Aplicado no Item', 'Data de Compra', 'Status do Pedido'], $delimiter);

        // criando cada linha do arquivo
        foreach ($coupons as $coupon) {
            $formattedArr = [];
            $formattedArr[] = $coupon['pretty_id'];
            $formattedArr[] = $coupon['codigo'];
            if ($coupon['type'] == 'ABS') {
                $formattedArr[] = 'R$ ' . number_format($coupon['value'], 2, ',', '.');
            } else {
                $formattedArr[] = $coupon['value'] . '%';
            }
            $formattedArr[] = 'R$ ' . number_format(round($coupon['valueNoDiscount'], 2), 2, ',', '.');
            $formattedArr[] = 'R$ ' . number_format(round($coupon['descTotal'], 2), 2, ',', '.');
            $formattedArr[] = (new \DateTime($coupon['dataPedido']))->format('d-m-Y H:i:s');
            $formattedArr[] = $coupon['statusPedido'];
            fputcsv($output, $formattedArr, $delimiter);
        }

        fclose($output);
        return $fileName;
    }
}
