<?php

namespace IntecPhp\Model;

use IntecPhp\Model\FileHandler;
use IntecPhp\Entity\CsvEntity;
use IntecPhp\Entity\TbFaixa;

class Csv extends FileHandler
{
    protected $path = "app/data/csv";
    protected $validType = "text/csv";
    private $csvEntity;
    private $tbFaixa;

    const STATUS_PENDING = 'Pendente';
    const STATUS_INVALID = 'Arquivo inválido';
    const STATUS_APPLIED = 'Aplicado';

    public function __construct(CsvEntity $csvEntity, TbFaixa $tbFaixa)
    {
        $this->csvEntity = $csvEntity;
        $this->tbFaixa   = $tbFaixa;
    }

    public function save()
    {
        if (!file_exists($this->path)) {
            throw new \Exception("O diretório dos arquivos não foi criado");
        }

        if ($this->hasError()) {
            $this->throwError();
        }

        if (!$this->isValid()) {
            throw new \Exception("O Arquivo deve ser do tipo \"CSV\"");
        }

        $hash = hash('sha256', uniqid());

        $pathFile = $this->path . "/" . $hash;

        if (move_uploaded_file($this->tmp, $pathFile)) {
            $this->hash = $hash;
            $id = $this->csvEntity->save($this->name, $this->hash, self::STATUS_PENDING);

            return [
                'id'   => $id,
                'file' => $hash
            ];
        }

        throw new \Exception("Não foi possível adicionar o arquivo ao diretório $this->path");
    }

    public function applyRangePrice(string $file)
    {
        $pathFile = $this->path . '/' . $file;
        if (!file_exists($pathFile)) {
            throw new \Exception("O arquivo CSV não foi encontrado");
        }

        $this->tbFaixa->beginTransaction();
        if (($csvFile = fopen($pathFile, "r")) !== false) {

            $data = fgetcsv($csvFile, 1000, ";");
            if (count($data) != 6 || $data[0] !== 'idFaixa' || $data[1] !== 'de' || $data[2] !== 'ate' || $data[3] !== 'preco' || $data[4] !== 'idPapel' || $data[5] !== 'hasColor') {
                $data = fgetcsv($csvFile, 1000, ";");
                if (count($data) != 6 || $data[0] !== 'idFaixa' || $data[1] !== 'de' || $data[2] !== 'ate' || $data[3] !== 'preco' || $data[4] !== 'idPapel' || $data[5] !== 'hasColor') {
                    $this->tbFaixa->rollBack();
                    throw new \Exception("A primeira linha do aquivo deve conter os campos idFaixa, de, ate, preco, idPapel e hasColor");
                }
            }

            $this->tbFaixa->eraseRangePrice();
            $row = 2;
            $id  = 1;

            while (($data = fgetcsv($csvFile, 1000, ";")) !== false) {
                $num = count($data);
                if ($num != 6) {
                    $this->tbFaixa->rollBack();
                    throw new \Exception("A linha $row não é composta pelos campos idFaixa, de, ate, preco, idPapel e hasColor");
                }


                $fields = [
                    'idFaixa'  => ctype_digit($data[0]) && ($idFaixa = intval($data[0])) == $id,
                    'de'       => ctype_digit($data[1]) && ($de = intval($data[1])),
                    'ate'      => ctype_digit($data[2]) && ($ate = intval($data[2])),
                    'preco'    => is_numeric($data[3])  && ($preco = floatval($data[3])),
                    'idPapel'  => ctype_digit($data[4]) && ($idPapel = intval($data[4])),
                    'hasColor' => ctype_digit($data[5]) && (($hasColor = intval($data[5])) == 0 || $hasColor == 1)
                ];

                foreach ($fields as $key => $value) {
                    if (!$value) {
                        $this->tbFaixa->rollBack();
                        throw new \Exception("O campo $key na linha $row é inválido");
                    }
                }

                $this->tbFaixa->add($idFaixa, $de, $ate, $preco, $idPapel, $hasColor);

                $row++;
                $id++;
            }
            fclose($csvFile);
        } else {
            throw new \Exception("Não foi possível abrir o arquivo CSV para a leitura");
        }
        $this->tbFaixa->commit();
    }

    public function getInfo(int $file_id)
    {
        $file = $this->csvEntity->get($file_id);
        if (!$file) {
            return [];
        }

        return $file;
    }

    public function updateCsvInvalid(int $id)
    {
        return $this->csvEntity->updateStatus($id, self::STATUS_INVALID);
    }

    public function updateCsvApplied(int $id)
    {
        return $this->csvEntity->updateStatus($id, self::STATUS_APPLIED, date('Y-m-d H:i:s'));
    }

    public function getAll()
    {
        $files = $this->csvEntity->getAll();
        if (!empty($files)) {
            for ($i = 0; $i < count($files); $i++) {
                $files[$i]['created_at'] = (new \DateTime($files[$i]['created_at']))->format('d-m-Y H:i:s');
                $files[$i]['updated_at'] = (new \DateTime($files[$i]['updated_at']))->format('d-m-Y H:i:s');
                if ($files[$i]['data_aplicacao']) {
                    $files[$i]['data_aplicacao'] = (new \DateTime($files[$i]['data_aplicacao']))->format('d-m-Y H:i:s');
                }
            }
        }

        return $files;
    }

    public function getById(int $id)
    {
        return $this->csvEntity->getById($id);
    }
}
