<?php

namespace IntecPhp\Model;

use IntecPhp\Entity\TbPedido;

class User
{
    private $tbPedido;

    public function __construct(TbPedido $tbPedido)
    {
        $this->tbPedido = $tbPedido;
    }

    public function myOrders($userId)
    {
        return $this->tbPedido->getByUserId($userId);
    }
}
