<?php

namespace IntecPhp\Model;

use IntecPhp\Entity\TbCliente;

class Customer
{
    private $cusEnt;

    public function __construct(TbCliente $tbClient)
    {
        $this->cusEnt = $tbClient;
    }

    public function getInfo($customerId)
    {
        $data = $this->cusEnt->get($customerId);
        if (empty($data)) {
            return [];
        }

        return [
            'name' => $data['nomeCliente'],
            'cpf' => $data['cpfCliente'],
            'tel' => $data['telCliente'],
            'cel' => $data['celCliente'],
            'email' => $data['emailCliente'],
            'birth' => $data['dtNascimento']
        ];
    }

    public function isVerified($customerId)
    {
        return $this->cusEnt->isVerified($customerId);
    }

    public function updateInfo($customerId, $name, $cpf, $tel, $cel, $email, \DateTime $birth = null)
    {
        if ($birth) {
            $birth = $birth->format('Y-m-d');
        }

        if (!$this->cusEnt->updateInfo($customerId, $name, $cpf, $tel, $cel, $email, $birth)) {
            throw new \Exception('Não foi possível atualizar as informações do cliente');
        }
    }

    public function getAll(string $startPeriod, string $endPeriod, string $filterType)
    {
        $users = $this->cusEnt->getAllAndLastOrderDate($startPeriod, $endPeriod, $filterType);

        if (!empty($users)) {
            for ($i = 0; $i < count($users); $i++) {
                $users[$i]['dtIngresso'] = (new \DateTime($users[$i]['dtIngresso']))->format('d-m-Y H:i:s');
                if (!empty($users[$i]['dataPedido'])) {
                    $users[$i]['dataPedido'] = (new \DateTime($users[$i]['dataPedido']))->format('d-m-Y H:i:s');
                }
            }
        }

        return $users;
    }

    public function createUsersCsv(array $users)
    {
        $delimiter = ';';

        $fileName = 'usuarios_' . date('d-m-Y H:i:s') . '.csv';

        if (($output = fopen("/tmp/$fileName", 'w')) === false) {
            throw new \Exception("Erro ao tentar criar o arquivo CSV");
        }

        // gerando o cabeçalho das colunas
        fputcsv($output, ['Nome', 'Email', 'Telefone', 'Data de Criação', 'Última Compra'], $delimiter);

        // criando cada linha do arquivo
        foreach ($users as $user) {
            fputcsv($output, $user, $delimiter);
        }

        fclose($output);
        return $fileName;
    }
}
