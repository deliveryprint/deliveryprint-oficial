<?php

namespace IntecPhp\Model;

use IntecPhp\Entity\TbCupom;
use IntecPhp\Entity\TbCliente;
use Exception;

class Invitation
{
    const INVITE_COUPON_TYPE = 'ABS';
    const INVITE_COUPON_VALUE = 15;
    const INVITE_COUPON_VALIDITY = 'P100Y';
    const INVITATION_COUPON_TYPE = 'ABS';
    const INVITATION_COUPON_VALUE = 10;
    const INVITATION_COUPON_VALIDITY = 'P6M';

    private $couponEnt;
    private $userEnt;

    public function __construct(TbCupom $couponEnt, TBCliente $userEnt)
    {
        $this->couponEnt = $couponEnt;
        $this->userEnt = $userEnt;
    }

    public function getInviteCodeByUser($userId)
    {
        if ($inviteCode = $this->userEnt->getInviteCode($userId)) {
            return $inviteCode;
        } else {
            // Gerar um novo código
            $inviteCode = $this->createInviteCode();

            // Armazenar no cliente
            $this->userEnt->updateInviteCode($userId, $inviteCode);

            // Criar o cupom de desconto correspondente
            $startdate = new \DateTime();
            $expirationDate = new \DateTime();
            $expirationDate->add(new \DateInterval(self::INVITE_COUPON_VALIDITY));
            $this->couponEnt->save(
                $inviteCode,
                self::INVITE_COUPON_VALUE,
                $startdate->format('Y-m-d H:i:s'),
                $expirationDate->format('Y-m-d H:i:s'),
                self::INVITE_COUPON_TYPE,
                1000,
                '',
                1,
                0,
                '',
                $userId,
                true
            );

            // Retornar o código
            return $inviteCode;
        }
    }

    private function createInviteCode()
    {
        return uniqid();
    }

    public function getAllInvitationCouponsByUser($userId)
    {
        $coupons = $this->couponEnt->getAllInvitationCouponsByUser($userId);

        foreach ($coupons as $key => $cp) {
            $counter = $this->couponEnt->countUserCouponItems($cp['idDesc'], $userId);
            $coupons[$key]['used'] = $counter >= 1 ? true : false;
        }

        return $coupons;
    }

    public function getAllInvitationCoupons()
    {
        return $this->couponEnt->getAllInvitationCoupons();
    }

    public function createInvitationCoupon($userId)
    {
        $uid = uniqid();
        $startdate = new \DateTime();
        $expirationDate = new \DateTime();
        $expirationDate->add(new \DateInterval(self::INVITATION_COUPON_VALIDITY));
        $this->couponEnt->save(
            $uid,
            self::INVITATION_COUPON_VALUE,
            $startdate->format('Y-m-d H:i:s'),
            $expirationDate->format('Y-m-d H:i:s'),
            self::INVITATION_COUPON_TYPE,
            1,
            '',
            1,
            0,
            '',
            $userId
        );
    }

    public function getInvitatorFromCouponId($couponId)
    {
        $coupon = $this->couponEnt->getById($couponId);

        if ($coupon) {
            $userId = $this->userEnt->getByInviteCode($coupon['codigo']);
            return $userId;
        } else {
            return false;
        }
    }
}
