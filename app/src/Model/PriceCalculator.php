<?php

namespace IntecPhp\Model;

use IntecPhp\Entity\TbFaixa;
use IntecPhp\Entity\TbPapel;
use IntecPhp\Entity\TbAcabamento;
use IntecPhp\Entity\TbCupomGeral;
use IntecPhp\Entity\TbCupom;

class PriceCalculator
{
    private $rangeEnt;
    private $paperEnt;
    private $finishingEnt;
    private $generalCouponEnt;
    private $couponEnt;

    const COLOR_ID = 2;
    const BACK_FRONT = 1;

    public function __construct(TbFaixa $rangeEnt, TbPapel $paperEnt, TbAcabamento $finishingEnt, TbCupomGeral $generalCouponEnt, TbCupom $couponEnt)
    {
        $this->rangeEnt = $rangeEnt;
        $this->paperEnt = $paperEnt;
        $this->finishingEnt = $finishingEnt;
        $this->generalCouponEnt = $generalCouponEnt;
        $this->couponEnt = $couponEnt;
    }

    public function calculate($item, $idUser)
    {

        $hasColor = $this->hasColor($item['color']);
        $qPag = $this->calcTotalPages($item['files']);
        $qPap = $this->calcQPap($qPag, $item['side']);
        $pPap = $this->getPaperPrice($item['paper']);
        $pip = $this->getRangePrice($item['paper'], $qPag, $hasColor);
        $qa = $this->calcTotalFinishings($item['finishing']['binding']['total'], $item['files']);
        $pa = $this->calcPriceFinishings($item['finishing']['id']);

        $itemValue = $qPag * $pip + $qPap * $pPap + $qa * $pa;

        error_log("$qPag * $pip + $qPap * $pPap + $qa * $pa = $itemValue");

        $generalCoupon = $this->calcGeneralCouponValue($itemValue);
        $couponDiscount = $this->calcCouponDiscountValue($itemValue, $item['cupomCode'], $idUser);
        $totalDiscount = $generalCoupon['value'] + $couponDiscount['value'];

        $valueWithDiscount = round($itemValue - $totalDiscount, 2);
        if ($valueWithDiscount <= 0) $valueWithDiscount = 0.0;

        return [
            'qPag' => $qPag,
            'qPap' => $qPap,
            'value' => $itemValue,
            'valueWithDiscount' => $valueWithDiscount,
            'discountValue' => $totalDiscount,
            'generalCoupon' => $generalCoupon['id'],
            'couponDiscount' => $couponDiscount['id'],
        ];
    }

    private function calcCouponDiscountValue($itemValue, $couponCode, $idUser)
    {
        $coup = [
            'id' => null,
            'value' => 0
        ];

        if($couponCode) {
            $coupon = $this->couponEnt->checkCoupon($couponCode, (new \DateTime())->format('Y-m-d H:i:s'));

            if($coupon) {

                $coup['id'] = $coupon['idDesc'];

                if($idUser) {
                    $counter = $this->couponEnt->countUserCouponItems($coupon['idDesc'], $idUser);
                    if($counter >= $coupon['cupom_user_limit']) {
                        return $coup;
                    }
                }

                if ($coupon['type'] === 'PERCENT') {
                    $coup['value'] = $itemValue * ($coupon['value'] / 100);
                } else {
                    if ($coupon['value'] > 7) {
                        $coup['value'] = min($itemValue * 0.5, $coupon['value']);
                    } else {
                        $coup['value'] = (float) $coupon['value'];
                    }
                }
            }
        }

        return $coup;
    }

    private function calcGeneralCouponValue(float $itemValue)
    {
        $coup = [
            'id' => null,
            'value' => 0
        ];
        $last = $this->generalCouponEnt->getLastActive();

        if($last === false) {
            throw new \Exception('Não foi possível encontrar o cupom de desconto geral');
        }

        if($last) {
            $coup['id'] = $last['idDescGeral'];
            $coup['value'] = $itemValue * ($last['value'] / 100);
        }

        return $coup;
    }

    private function hasColor($colorId)
    {
        $hasColor = $colorId == self::COLOR_ID ? true : false;
        error_log('hasColor: ' . $hasColor);
        return $hasColor;
    }

    private function getPaperPrice($paperId)
    {
        $paper = $this->paperEnt->get($paperId);
        if($paper === false) {
            throw new \Exception('Não foi possível encontrar o tipo de papel');
        }
        error_log('pPap: ' . $paper['precoPapel']);
        return $paper['precoPapel'];
    }

    private function getRangePrice($paperId, $qPag, $hasColor)
    {
        $rangePrice = $this->rangeEnt->getRangePrice($paperId, $qPag, $hasColor);

        if($rangePrice === false) {
            throw new \Exception('Não foi possível encontrar o intervalo de preços');
        }

        error_log('pip: ' . $rangePrice['preco']);
        return $rangePrice['preco'];
    }

    private function calcTotalFinishings($totalBindings, $files)
    {
        $numFiles = count($files);
        // $numBindings = $totalBindings > 0 ? $totalBindings : ($this->calcTotalCopies($files) * $numFiles);
        $numBindings = $totalBindings > 0 ? $totalBindings : $this->calcTotalCopies($files);
        error_log('qa: ' . $numBindings);
        return $numBindings;
    }

    private function calcPriceFinishings($finishingId)
    {
        $finishing = $this->finishingEnt->get($finishingId);

        if($finishing === false) {
            throw new \Exception('Tipo de acabamento não encontrado');
        }

        error_log('acabamento: ' . $finishing['valorAcabamento']);
        return $finishing['valorAcabamento'];
    }

    private function calcQPap($qPag, $sideType)
    {
        $qPap = $sideType == self::BACK_FRONT ? (ceil($qPag / 2)) : $qPag;
        error_log('qPap: ' . $qPap);
        return $qPap;
    }

    private function calcTotalPages($files)
    {
        $pages = 0;
        foreach($files as $f) {
            $pages += ($f['numPages'] * $f['numCopies']);
        }

        error_log('pages: ' . $pages);
        return $pages;
    }

    private function calcTotalCopies($files)
    {
        $copies = 0;
        foreach($files as $f) {
            $copies += $f['numCopies'];
        }

        error_log('copies: ' . $copies);
        return $copies;
    }
}
