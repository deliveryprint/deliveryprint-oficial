<?php

namespace IntecPhp\Model;

use IntecPhp\Entity\TbPapel;

class ShippingSpecs
{

    const MAX_ITEM_WEIGHT = 30;

    private $paperEnt;

    public function __construct(TbPapel $paperEnt)
    {
        $this->paperEnt = $paperEnt;
    }

    public function parseItemSpecs(int $paperId, int $totalPages) {

        if($totalPages < 1) {
            throw new \Exception('Número de páginas deve ser maior do que zero.');
        }

        if(($paper = $this->paperEnt->get($paperId)) === false) {
            throw new \Exception('Não foi possível encontrar o papel escolhido.');
        }

        $item = new \stdClass;

        $item->width = max(floatval($paper['largura']), 11);
        $item->length = max(floatval($paper['comprimento']), 16);
        $item->height = max(floatval($paper['espessura']) * $totalPages, 2);
        $item->weight = max(floatval($paper['peso']) * $totalPages, 0.1);
        $item->amount = 1;

        $specResult = $this->parseSpecResult($item);

        return $specResult;
    }

    private function parseSpecResult($item)
    {
        $specResult = [];

        if($item->weight > self::MAX_ITEM_WEIGHT) {
            $maxWeightItems = clone $item;
            $maxWeightItems->weight = self::MAX_ITEM_WEIGHT; // novo peso
            $maxWeightItems->height = ceil(($maxWeightItems->weight / $item->weight) * $item->height); // nova altura
            $maxWeightItemsAmount = intdiv($item->weight, self::MAX_ITEM_WEIGHT); // quantidade
            $specResult[] = [
                'item' => $maxWeightItems,
                'amount' => $maxWeightItemsAmount
            ];

            $residualWeightItem = clone $item;
            $residualWeightItem->weight = $item->weight - $maxWeightItemsAmount * $maxWeightItems->weight; // novo peso
            $residualWeightItem->height = ceil(($residualWeightItem->weight / $item->weight) * $item->height); // nova altura

            $specResult[] = [
                'item' => $residualWeightItem,
                'amount' => 1
            ];
        } else {
            $specResult[] = [
                'item' => $item,
                'amount' => 1
            ];
        }

        return $specResult;
    }
}
