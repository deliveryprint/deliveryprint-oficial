<?php

namespace IntecPhp\Model;

abstract class FileHandler
{
    protected $validType = "";
    protected $path = "";
    protected $name = null;
    protected $tmp = null;
    protected $type = null;
    protected $errorCode = UPLOAD_ERR_OK;
    protected $size = null;

    public function __construct()
    {
    }

    public function setName(string $name)
    {
        $this->name = $name;
    }
    public function setTmp(string $tmp)
    {
        $this->tmp = $tmp;
    }
    public function setType(string $type)
    {
        $this->type = $type;
    }

    public function setErrorCode(int $code)
    {
        $this->errorCode = $code;
    }

    public function setSize(int $size)
    {
        $this->size = $size;
    }

    public function isValid()
    {
        if ($this->type == null) {
            throw new \Exception("Erro na inicialização");
        }

        return $this->type == $this->validType ? true : false;
    }

    public function hasError()
    {
        if ($this->errorCode == UPLOAD_ERR_OK) {
            return false;
        }

        return true;
    }

    public function throwError()
    {
        switch ($this->errorCode) {
            case UPLOAD_ERR_OK:
                break;
            case UPLOAD_ERR_NO_FILE:
                throw new Exception('Arquivo não foi enviado.');
            case UPLOAD_ERR_INI_SIZE:
            case UPLOAD_ERR_FORM_SIZE:
                throw new Exception('O tamanho do arquivo excede o limite máximo permitido.');
            default:
                throw new Exception('Erro inesperado.');
        }
    }

    public function fileExists(string $fileName)
    {
        return file_exists($this->path . '/' . $fileName);
    }
}
