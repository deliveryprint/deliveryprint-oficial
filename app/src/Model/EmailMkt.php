<?php

namespace IntecPhp\Model;

use IntecPhp\Entity\TbEmailMkt;

class EmailMkt
{

    private $tbEmailMkt;

    public function __construct(TbEmailMkt $tbEmailMkt)
    {
        $this->tbEmailMkt = $tbEmailMkt;
    }

    public function saveEmail($email, $origin)
    {
        return $this->tbEmailMkt->add($email, $origin);
    }

    public function getAllInPeriod($period)
    {
        return $this->tbEmailMkt->getByPeriod($period);
    }

    public function getShoppingCartDiscountsByPeriod($period)
    {
        return $this->tbEmailMkt->getShoppingCartDiscountsByPeriod($period);
    }

    public function createEmailsCsv(array $header, array $items)
    {
        $delimiter = ';';

        $fileName = 'modal-emails_' . date('d-m-Y H:i:s') . '.csv';

        if (($output = fopen("/tmp/$fileName", 'w')) === false) {
            throw new \Exception("Erro ao tentar criar o arquivo CSV");
        }

        // gerando o cabeçalho das colunas
        fputcsv($output, $header, $delimiter);

        // criando cada linha do arquivo
        foreach ($items as $row) {
            fputcsv($output, $row, $delimiter);
        }

        fclose($output);
        return $fileName;
    }

}
