<?php

namespace IntecPhp\Model;

use IntecPhp\Entity\PasswordRecovery as PasswordRecoveryEntity;

class PasswordRecovery
{
    private $passEnt;

    public function __construct(PasswordRecoveryEntity $passEnt)
    {
        $this->passEnt = $passEnt;
    }

    public function createRecovery($email)
    {
        $hash = $this->generateHash();
        $this->passEnt->add($email, $hash);
        return $hash;
    }

    public function hashExists($hash)
    {
        return $this->passEnt->getCustomerIdFromHash($hash);
    }

    public function disableHash($hash)
    {
        return $this->passEnt->disableRecovery($hash);
    }

    private function generateHash()
    {
        return sha1(uniqid());
    }

}
