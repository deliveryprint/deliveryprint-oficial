<?php

namespace IntecPhp\Model;

use IntecPhp\Entity\TbShippingPrices;

class ShippingPrices
{
    // Entregas
    const D_EXPRESS_1 = 'dExpress1Price';
    const D_EXPRESS_2 = 'dExpress2Price';
    const D_EXPRESS_3 = 'dExpress3Price';
    const D_EXPRESS_4 = 'dExpress4Price';
    const D_EXPRESS_4_ADDITIONAL = 'dExpress4AdditionalPrice';
    const D_EXPRESS_5 = 'dExpress5Price';
    //Retiradas
    const W_EXPRESS_1 = 'wExpress1Price';
    const W_EXPRESS_2 = 'wExpress2Price';

    private $shippingPrices;

    public function __construct(TbShippingPrices $shippingPrices)
    {
        $this->shippingPrices = $shippingPrices;
    }

    public function getShippingPrices()
    {
        $prices = $this->shippingPrices->getShippingPrices();
        foreach ($prices as &$price) {
            $price = $price + 0;
        }

        return $prices;
    }

    public function getFieldsDeliveryShippingPrices()
    {
        return [
            self::D_EXPRESS_1,
            self::D_EXPRESS_2,
            self::D_EXPRESS_3,
            self::D_EXPRESS_4,
            self::D_EXPRESS_4_ADDITIONAL,
            self::D_EXPRESS_5
        ];
    }

    public function getFieldsWithdrawShippingPrices()
    {
        return [
            self::W_EXPRESS_1,
            self::W_EXPRESS_2
        ];
    }

    public function updateShippingPrices(array $fieldsShippingPrices, array $prices)
    {
        $formattedPrices = [];
        $arrayKeys = array_keys($prices);
        $diff = array_diff($fieldsShippingPrices, $arrayKeys);
        if (!empty($diff)) {
            throw new \Exception('O campo ' . $diff[0] . ' deve ser informado');
        }

        foreach ($fieldsShippingPrices as $field) {
            $formattedPrices[$field] = $prices[$field];
        }

        return $this->shippingPrices->updateShippingPrices($formattedPrices);
    }
}
