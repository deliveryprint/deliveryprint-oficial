<?php

namespace IntecPhp\Model;

use IntecPhp\Entity\TbCor;
use IntecPhp\Entity\TbLado;
use IntecPhp\Entity\TbPapel;
use IntecPhp\Entity\TbAcabamento;
use IntecPhp\Entity\TbFaixa;
use IntecPhp\Entity\TbCupomGeral;

class OrderPartial
{

    private $color;
    private $side;
    private $paper;
    private $finishing;
    private $paperPriceRanges;
    private $generalCoupon;

    public function __construct(TbCor $color, TbLado $side, TbPapel $paper, TbAcabamento $finishing, TbFaixa $paperPriceRanges, TbCupomGeral $generalCoupon)
    {
        $this->color = $color;
        $this->side = $side;
        $this->paper = $paper;
        $this->finishing = $finishing;
        $this->paperPriceRanges = $paperPriceRanges;
        $this->generalCoupon = $generalCoupon;
    }

    public function getData()
    {

        $colors = $this->color->getAll();

        $formattedColors = array_map(function ($item) {
            return [
                'id' => $item['idCor'],
                'description' => $item['descricaoCor'],
                'icon' => $item['iconeCor'],
                'hasColor' => $item['idCor'] != 1
            ];
        }, $colors);

        $sides = $this->side->getAll();
        $formattedSides = array_map(function ($item) {
            return [
                'id' => $item['idLado'],
                'description' => $item['descricaoLado'],
                'icon' => $item['iconeLado']
            ];
        }, $sides);

        $papers = $this->paper->getAll();

        // Remove alguns tipos de papel
        $papers = array_filter($papers, function ($paper) {
            $pDescription = $paper['descricaoPapel'];
            return
                $pDescription != Item::PAPER_TYPE_1_DESC &&
                $pDescription != Item::PAPER_TYPE_5_DESC &&
                $pDescription != Item::PAPER_TYPE_6_DESC &&
                $pDescription != Item::PAPER_TYPE_7_DESC &&
                $pDescription != Item::PAPER_TYPE_8_DESC &&
                $pDescription != Item::PAPER_TYPE_9_DESC;
        });

        $paperTypes = array_map(function ($item) {
            return [
                'id' => $item['idPapel'],
                'description' => $item['descricaoPapel'],
                'value' => $item['precoPapel'],
                'icon' => $item['iconePapel']
            ];
        }, $papers);

        $finishings = $this->finishing->getAllOrderedByPosition();

        // Remove alguns tipos de acabamento
        $finishings = array_filter($finishings, function ($finishing) {
            $fDescription = $finishing['descricaoAcabamento'];
            return
                $fDescription != Item::FINISHING_TYPE_4_DESC &&
                $fDescription != Item::FINISHING_TYPE_5_DESC &&
                $fDescription != Item::FINISHING_TYPE_6_DESC;
        });

        $finishingTypes = array_map(function ($item) {
            return [
                'id' => $item['idAcabamento'],
                'description' => $item['descricaoAcabamento'],
                'value' => $item['valorAcabamento'],
                'position' => $item['posicaoAcabamento'],
                'icon' => $item['iconeAcabamento'],
                'hasBorder' => ($item['idAcabamento'] != 1 && $item['idAcabamento'] != 2)
            ];
        }, $finishings);

        $paperRanges = $this->paperPriceRanges->getAllWithPaper();
        $couponValue = $this->generalCoupon->getLastActive();

        return [
            'colors' => $formattedColors,
            'sides' => $formattedSides,
            'paperTypes' => $paperTypes,
            'finishingTypes' => $finishingTypes,
            'paperRanges' => $paperRanges,
            'generalCoupon' => $couponValue['value'],
        ];
    }
}
