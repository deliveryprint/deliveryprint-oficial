<?php

namespace IntecPhp\Model;

use IntecPhp\Service\NFService;
use IntecPhp\Entity\TbPedido;
use IntecPhp\Model\Item;

class NF
{

    private $nf;
    private $tbPedido;
    private $item;

    public function __construct(NFService $nf, TbPedido $tbPedido, Item $item)
    {
        $this->nf = $nf;
        $this->tbPedido = $tbPedido;
        $this->item = $item;
    }

    public function makeNF($idIugu)
    {
        $infoNota = $this->tbPedido->getInfoNota($idIugu);
        $finalValue = $this->item->calcPriceOfOrder($infoNota['idPedido']);

        //verifica se é pessoa física ou juridica
        if(strlen($infoNota['cpfCliente']) > 14) {
            $tipoPessoa = 'J';
        } else {
            $tipoPessoa = 'F';
        }

        return $this->nf->makeNF($infoNota, $finalValue, $tipoPessoa);
    }
}
