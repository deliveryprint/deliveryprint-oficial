<?php

namespace IntecPhp\Controller;

use IntecPhp\Model\ResponseHandler;
use IntecPhp\Model\ShoppingCart;
use IntecPhp\Model\Account;
use IntecPhp\Service\AuthAccount;
use Exception;

class ShoppingCartController
{
    private $scart;
    private $iuguAccountId;
    private $iuguTestMode;
    private $order;
    private $authAccount;

    public function __construct(
        AuthAccount $authAccount,
        ShoppingCart $scart,
        $iuguAccountId,
        bool $iuguCardTest,
        $order
    )
    {
        $this->scart         = $scart;
        $this->iuguAccountId = $iuguAccountId;
        $this->iuguCardTest  = $iuguCardTest;
        $this->order = $order;
        $this->authAccount = $authAccount;
    }

    public function getCartData()
    {
        try {
            $orderId = Account::getOrder();
            $userId = $this->authAccount->get('id');
            $data = $orderId ? $this->scart->getCartData($orderId, $userId ?? null) : [];
            $iuguConfig = [
                'iuguConfig' => [
                    'iuguAccountId' => $this->iuguAccountId,
                    'iuguCardTest'  => $this->iuguCardTest
                ]
            ];
            $data = array_merge($data, $iuguConfig);
            $rh = new ResponseHandler(200, 'ok', $data);
        } catch (Exception $e) {
            $rh = new ResponseHandler(400, $e->getCode() . ": " . $e->getMessage());
        }

        $rh->printJson();
    }

    public function getPaymentData()
    {
        try {
            $orderId = Account::getPayment();
            $userId = $this->authAccount->get('id');
            $data = $orderId ? $this->scart->getCartData($orderId, $userId ?? null) : [];
            $rh = new ResponseHandler(200, 'ok', $data);
        } catch (Exception $e) {
            $rh = new ResponseHandler(400, $e->getCode() . ": " . $e->getMessage());
        }

        $rh->printJson();
    }

    public function getNumberItemsInCart()
    {
        try {
            $orderId = Account::getOrder();
            $n = $orderId ? $this->scart->countOrderItems($orderId) : 0;
            $rh = new ResponseHandler(200, 'ok', ["itemsInCart" => $n]);
        } catch (Exception $e) {
            $rh = new ResponseHandler(400, $e->getCode() . ": " . $e->getMessage());
        }

        $rh->printJson();
    }

    public function hasDeliveryName()
    {

        try {
            $orderId = Account::getOrder();

            if(empty($orderId)) {
                throw new Exception('Não encontramos seu pedido');
            }

            $hasName = $this->order->hasDeliveryName($orderId);
            if(!$hasName){
                throw new Exception('Para continuar é necessário inserir para quem entregar');
            }
            $rh = new ResponseHandler(200, 'ok');

        } catch (Exception $e) {
            $rh = new ResponseHandler(400, $e->getCode() . ": " . $e->getMessage());
        }

        $rh->printJson();
    }
}
