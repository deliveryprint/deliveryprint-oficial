<?php

namespace IntecPhp\Controller;

use IntecPhp\Model\Shipping;
use IntecPhp\Service\AddressFinder;
use IntecPhp\Model\ResponseHandler;
use IntecPhp\Model\PriceCalculator;
use IntecPhp\Service\AuthAccount;
use Exception;

class ShippingController
{
    private $shipping;
    private $addrFinder;
    private $priceCalculator;
    private $authAccount;

    public function __construct(Shipping $shipping, AddressFinder $addrFinder, PriceCalculator $pcalc, AuthAccount $authAccount)
    {
        $this->shipping = $shipping;
        $this->addrFinder = $addrFinder;
        $this->priceCalculator = $pcalc;
        $this->authAccount = $authAccount;
    }

    public function getFreightTypes($request)
    {
        $data = $request->getPostParams();

        try {

            if (empty($data['zip'])) {
                throw new Exception('CEP não informado');
            }

            if (empty($data['item']['paper'])) {
                throw new Exception('Tipo de papel não informado');
            }

            if (empty($data['totalPages'])) {
                throw new Exception('Número de páginas deve ser maior do que zero');
            }

            if (empty($data['totalPaper'])) {
                throw new Exception('Quantidade de papel deve ser maior do que zero');
            }

            $zip = $data['zip'];

            $idUser = $this->authAccount->get('id');

            $itemValues = $this->priceCalculator->calculate($data['item'], $idUser);

            $freights = $this->shipping->getFreightTypes(
                $zip,
                new \DateTime(),
                $data['item']['paper'],
                $data['totalPaper'],
                $data['totalPages'],
                array($data['item']['finishing']['id']),
                $itemValues['value'],
                filter_var($data['freightFallback'], FILTER_VALIDATE_BOOLEAN)
            );

            $availableFreights = [];
            $freightLength = count($freights);

            if ($freightLength < 1) {
                throw new Exception('Não foram encontradas opções de entrega para o CEP informado');
            }

            // exibe apenas os dois fretes mais rápidos, o de sábado (se houver) e o mais barato
            if ($freightLength >= 4) {
                $availableFreights[] = $freights[0];
                $availableFreights[] = $freights[1];
                unset($freights[0]);
                unset($freights[1]);

                $saturday = false;
                foreach ($freights as $key => $freight) {
                    if ($freight['id'] == 9) {
                        $availableFreights[] = $freights[$key];
                        unset($freights[$key]);
                        $saturday = true;
                        break;
                    }
                }

                usort($freights, function ($a, $b) {
                    return $b['value'] <=> $a['value'];
                });

                $freightLength = count($freights);

                if (!$saturday) {
                    $availableFreights[] = $freights[$freightLength - 2];
                }

                $availableFreights[] = $freights[$freightLength - 1];
            } else {
                $availableFreights = $freights;
            }

            $rp = new ResponseHandler(200, 'ok', [
                'freights' => $availableFreights
            ]);
        } catch (Exception $ex) {
            $rp = new ResponseHandler(400, $ex->getMessage());
        }

        $rp->printJson();
    }

    public function getAddressFromCep($request)
    {
        $data = $request->getPostParams();

        try {

            if (empty($data['zip'])) {
                throw new Exception('CEP não informado');
            }

            $zip = $data['zip'];

            $address = $this->addrFinder->find($zip);

            $rp = new ResponseHandler(200, 'ok', [
                'address' => $address
            ]);
        } catch (Exception $ex) {
            $rp = new ResponseHandler(400, $ex->getMessage());
        }

        $rp->printJson();
    }
}
