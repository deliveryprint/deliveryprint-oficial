<?php

namespace IntecPhp\Controller;

use IntecPhp\Model\ResponseHandler;
use Intec\Router\Request;
use IntecPhp\Entity\TbSettings;
use Exception;

class SettingsController
{
    private $settingsEnt;

    public function __construct(TbSettings $settingsEnt)
    {
        $this->settingsEnt = $settingsEnt;
    }

    public function getSettings(Request $r) {
        $params = $r->getPostParams();

        try {
            $settings = $this->settingsEnt->getExtraBDaysSettings();
            $rh = new ResponseHandler(200, 'Configurações lidas com sucesso.', ['settings' => $settings]);
        } catch (Exception $e) {
            $rh = new ResponseHandler(400, $e->getMessage());
        }

        $rh->printJson();
    }

    public function updateSettings(Request $r) {
        $params = $r->getPostParams();

        try {
            $this->settingsEnt->updateExtraBDaysSettings( $params['settings'] );
            $rh = new ResponseHandler(200, 'Configurações salvas com sucesso', []);
        } catch (Exception $e) {
            $rh = new ResponseHandler(400, $e->getMessage());
        }

        $rh->printJson();
    }

}
