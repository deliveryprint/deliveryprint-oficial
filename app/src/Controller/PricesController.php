<?php

namespace IntecPhp\Controller;

use IntecPhp\Model\ResponseHandler;
use IntecPhp\Entity\TbFaixa;
use Intec\Router\Request;
use Exception;

class PricesController
{
    private $tbFaixa;

    public function __construct(TbFaixa $tbFaixa)
    {
        $this->tbFaixa = $tbFaixa;
    }

    public function getAllWithPaper(Request $request)
    {
        try {
            $ranges = $this->tbFaixa->getAllWithPaper();
            $rh = new ResponseHandler(200, 'ok', [ 'ranges' => $ranges ]);
        } catch (Exception $e) {
            $rh = new ResponseHandler(400, $e->getMessage() . '. Código: ' . $e->getCode());
        }

        return $rh->printJson();
    }

    public function updatePriceRanges(Request $request)
    {
        $params = $request->getPostParams();

        try {
            if (empty($params['ranges'])) {
                throw new \Exception('Não foi informada a faixa de preços a ser aplicada');
            }

            $ranges = $params['ranges'];

            foreach ($ranges as $range) {
                if ($range['novoPreco']) {
                   $this->tbFaixa->update($range['idFaixa'], $range['novoPreco']);
                }
            }

            $newRanges = $this->tbFaixa->getAllWithPaper();

            $rh = new ResponseHandler(200, 'ok', [ 'newRanges' => $newRanges ]);
        } catch (Exception $e) {
            $rh = new ResponseHandler(400, $e->getMessage() . '. Código: ' . $e->getCode());
        }

        return $rh->printJson();
    }

    public function getPaperPrices(Request $request)
    {
        try {
            $ranges = $this->tbFaixa->getMinMaxPrices();

            $prices = [];
            foreach ($ranges as $range) {
                if ($range['hasColor'] == 0) {
                    if ($range['de'] == 1) {
                        $prices['BkMax'] = $range['preco'];
                    } else {
                        $prices['BkMin'] = $range['preco'];
                    }
                } else {
                    if ($range['de'] == 1) {
                        $prices['ColorMax'] = $range['preco'];
                    } else {
                        $prices['ColorMin'] = $range['preco'];
                    }
                }
            }

            $rh = new ResponseHandler(200, 'ok', [ 'prices' => $prices ]);
        } catch (Exception $e) {
            $rh = new ResponseHandler(400, $e->getMessage() . '. Código: ' . $e->getCode());
        }

        return $rh->printJson();
    }
}
