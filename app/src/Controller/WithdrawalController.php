<?php

namespace IntecPhp\Controller;

use IntecPhp\Model\Withdrawal;
use IntecPhp\Model\ResponseHandler;

class WithdrawalController
{

    private $withdrawal;

    public function __construct(Withdrawal $withdrawal)
    {
        $this->withdrawal = $withdrawal;
    }


    public function getWithdrawalTypes($request)
    {
        $data = $request->getPostParams();

        try {

            $time = (new \DateTime());
            $totalPages = (int)$data['totalPages'];
            $totalPaper = (int)$data['totalPaper'];
            $bindings = $data['bindings'];

            $withdrawals = $this->withdrawal->getWithdrawalTypes($time, $totalPaper, $totalPages, $bindings);

            if (count($withdrawals) > 4) {
                unset($withdrawals[3]);
                $withdrawals = array_values($withdrawals);
            }

            $rp = new ResponseHandler(200, 'ok', [
                'withdrawals' => $withdrawals
            ]);

        } catch (Exception $ex) {
            $rp = new ResponseHandler(400, $ex->getMessage());
        }


        $rp->printJson();
    }


}
