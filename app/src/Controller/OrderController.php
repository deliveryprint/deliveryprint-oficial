<?php

namespace IntecPhp\Controller;

use IntecPhp\Model\ResponseHandler;
use IntecPhp\Model\Order;
use IntecPhp\Model\Customer;
use IntecPhp\Model\Account;
use IntecPhp\Model\PDF;
use Intec\Router\Request;
use IntecPhp\Validator\InputValidator;
use IntecPhp\Service\AuthAccount;
use Pheanstalk\Pheanstalk;
use Exception;

class OrderController
{
    private $order;
    private $customer;
    private $authAccount;
    private $emailProducer;
    private $pdf;

    public function __construct(AuthAccount $authAccount, Order $order, Customer $customer, Pheanstalk $emailProducer, PDF $pdf)
    {
        $this->order = $order;
        $this->customer = $customer;
        $this->authAccount = $authAccount;
        $this->emailProducer = $emailProducer;
        $this->pdf = $pdf;
    }

    public function saveOrderInfo(Request $request)
    {
        $params = $request->getPostParams();

        try {
            $idUser = $this->authAccount->get('id');

            if (empty($params['items'])) {
                throw new Exception("Não foram definidos os itens do pedido!");
            }
            $items = $params["items"];
            $zip = null;
            if (!empty($params['zip'])) {
                $zip = $params['zip'];
            }

            if ($idOrder = Account::getOrder()) {
                $this->order->updateUserOrder($idOrder, $items, $idUser, $zip);
            } else {
                $idOrder = $this->order->registerUserOrder($items, $idUser, $zip);
                Account::setOrder($idOrder);
            }

            $rh = new ResponseHandler(200);
        } catch (Exception $e) {
            $rh = new ResponseHandler(400, $e->getMessage() . '. Código: ' . $e->getCode());
        }

        return $rh->printJson();
    }

    public function applyOrderDiscount(Request $request)
    {
        $params = $request->getPostParams();

        try {
            $emailMktId = $params['emailMktId'];

            $discountType = 'PERCENT';
            $discountValue = 6.0;

            $idOrder = Account::getOrder();
            $this->order->applyDiscount($idOrder, $discountType, $discountValue, $emailMktId);
            $rh = new ResponseHandler(200);
        } catch (Exception $e) {
            $rh = new ResponseHandler(400, $e->getMessage() . '. Código: ' . $e->getCode());
        }

        return $rh->printJson();
    }

    public function getInfoOrder($r)
    {
        $params = $r->getPostParams();

        try {
            if (empty($params['idPedido'])) {
                throw new Exception('Pedido não informado');
            }
            $data = $this->order->getInfoOrder($params['idPedido']);
            $rh = new ResponseHandler(200, 'ok', $data);
        } catch (Exception $e) {
            $rh = new ResponseHandler(400, $e->getCode() . ": " . $e->getMessage());
        }

        $rh->printJson();
    }

    public function saveUserCompleteAddress(Request $request)
    {
        $params = $request->getPostParams();

        $config = [
            'addrId' => [
                'validators' => [
                    'IsEmptyValidator' => [],
                    'IsNumericValidator' => []
                ]
            ],
            'street' => [
                'validators' => [
                    'IsEmptyValidator' => [],
                    'StringLengthValidator' => [
                        'minLength' => 3,
                        "maxLength" => 200,
                    ]
                ],
            ],
            'district' => [
                'validators' => [
                    'IsEmptyValidator' => [],
                    'StringLengthValidator' => [
                        'minLength' => 2,
                        "maxLength" => 50,
                    ]
                ],
            ],
            'number' => [
                'validators' => [
                    'IsEmptyValidator' => [],
                    'StringLengthValidator' => [
                        'minLength' => 1,
                        'maxLength' => 15
                    ]
                ]
            ],
            'complement' => [
                'validators' => [
                    'StringLengthValidator' => [
                        'maxLength' => 50
                    ]
                ]
            ],
            'name' => [
                'validators' => [
                    'IsEmptyValidator' => [],
                    'StringLengthValidator' => [
                        'minLength' => 3,
                        'maxLength' => 100
                    ]
                ]
            ],
            'tel' => [
                'validators' => [
                    'IsEmptyValidator' => [],
                    'StringLengthValidator' => [
                        'minLength' => 14,
                        'maxLength' => 15
                    ]
                ]
            ]
        ];

        try {
            $userId = $this->authAccount->get('id');
            $orderId = Account::getOrder();

            if (!$userId) {
                throw new Exception("É necessário efetuar o login para prosseguir!");
            }

            if (!$orderId) {
                throw new Exception("É necessário existir um pedido no carrinho para prosseguir!");
            }

            $iv = new InputValidator($config);
            $iv->setData($params);

            if (!$iv->isValid()) {
                $errors = $iv->getErrorsMessages();
                $rh = new ResponseHandler(400, 'Preencha os dados de entrega', $errors);
                return $rh->printJson();
            }

            $addrInfo = [
                "id"         => $params['addrId'],
                "number"     => $params['number'],
                "street"     => $params['street'],
                "district"   => $params['district'],
                "complement" => $params['complement'] ? $params['complement'] : null
            ];

            $userInfo = [
                "name" => $params['name'],
                "tel"  => $params['tel']
            ];

            $this->order->registerUserCompleteAddress($orderId, $userId, $userInfo, $addrInfo);

            $rh = new ResponseHandler(200, 'ok');
        } catch (Exception $e) {
            $rh = new ResponseHandler(400, $e->getCode() . ": " . $e->getMessage());
        }

        $rh->printJson();
    }

    public function changeStatus($r)
    {
        $params = $r->getPostParams();

        try {
            $this->order->updateStatus($params['id'], $params['status']);
            $rp = new ResponseHandler(200);
        } catch (Exception $ex) {
            $rp = new ResponseHandler(400, $ex->getMessage());
        }

        $rp->printJson();
    }

    /* função para adicionar nota ao pedido */
    public function changeObs($r)
    {
        $params = $r->getPostParams();

        try {
            $this->order->saveObs($params['id'], $params['observacao']);
            $rp = new ResponseHandler(200);
        } catch (Exception $ex) {
            $rp = new ResponseHandler(400, $ex->getMessage());
        }

        $rp->printJson();
    }

    public function saveUserBasicInfo(Request $request)
    {
        $params = $request->getPostParams();

        $config = [
            'name' => [
                'validators' => [
                    'IsEmptyValidator' => [],
                    'StringLengthValidator' => [
                        'minLength' => 3,
                        'maxLength' => 100
                    ]
                ]
            ],
            'tel' => [
                'validators' => [
                    'IsEmptyValidator' => [],
                    'StringLengthValidator' => [
                        'minLength' => 14,
                        'maxLength' => 15
                    ]
                ]
            ]
        ];

        try {
            $userId = $this->authAccount->get('id');
            $orderId = Account::getOrder();

            if (!$userId) {
                throw new Exception("É necessário efetuar o login para prosseguir!");
            }

            if (!$orderId) {
                throw new Exception("É necessário existir um pedido no carrinho para prosseguir!");
            }

            $iv = new InputValidator($config);
            $iv->setData($params);

            if (!$iv->isValid()) {
                $errors = $iv->getErrorsMessages();
                $rh = new ResponseHandler(400, 'Informe para quem a entrega deverá ser feita e seu telefone de contato', $errors);
                return $rh->printJson();
            }

            $this->order->saveInOrderNameAndPhone($orderId, $params['name'], $params['tel']);
            $rh = new ResponseHandler(200, 'ok');
        } catch (Exception $e) {
            $rh = new ResponseHandler(400, $e->getCode() . ": " . $e->getMessage());
        }

        $rh->printJson();
    }

    public function getNewOrder(Request $request)
    {
        $params = $request->getPostParams();

        try {
            if (empty($params['datetime'])) {
                throw new Exception("É necessário informar a data do pedido para se fazer a consulta");
            }
            $data = $this->order->getOrderFromDatetime($params['datetime']);
            $rp = new ResponseHandler(200, "ok", $data);
        } catch (Exception $e) {
            $rp = new ResponseHandler(400, $e->getMessage());
        }

        $rp->printJson();
    }

    public function saveItemObservation($r)
    {
        $params = $r->getPostParams();

        try {
            if (empty($params['id'])) {
                throw new Exception('Item não informado');
            }
            $itemId = $params['id'];
            $observation = $params['observation'];

            $this->order->updateItemObservation($itemId, $observation);

            $rh = new ResponseHandler(200, 'Alterações salvas com sucesso.');
        } catch (Exception $e) {
            $rh = new ResponseHandler(400, $e->getCode() . ": " . $e->getMessage());
        }

        $rh->printJson();
    }

    public function getItemObservation($r)
    {
        $params = $r->getPostParams();

        try {
            if (empty($params['id'])) {
                throw new Exception('Item não informado');
            }
            $itemId = $params['id'];

            $data = $this->order->getItemObservation($itemId);

            $rh = new ResponseHandler(200, '', $data);
        } catch (Exception $e) {
            $rh = new ResponseHandler(400, $e->getCode() . ": " . $e->getMessage());
        }

        $rh->printJson();
    }

    public function sendOrderEmail(Request $request)
    {
        $params = $request->getPostParams();

        try {

            $iv = new InputValidator([
                'email' => [
                    'validators' => [
                        'IsEmptyValidator' => [],
                        'StringLengthValidator' => [
                            "maxLength" => 100,
                        ],
                        'EmailValidator' => [],
                    ],
                ]
            ]);

            $iv->setData($params);

            if (!$iv->isValid()) {
                throw new \Exception('Email inválido');
            } else {
                $this->emailProducer->put($this->formatOrderEmail(
                    $params['email'],
                    $params['item']
                ));
            }

            $rh = new ResponseHandler(200, 'E-mail foi enviado!');
        } catch (\Exception $e) {
            $rh = new ResponseHandler(400, $e->getMessage());
        }

        $rh->printJson();
    }

    private function formatOrderEmail($email, $item)
    {
        $body = '
            Olá, <br>
            Meu email é ' . $email . ' e quero fechar um pedido de impressão, porém o site está muito lento para mostrar os prazos de entrega ;( <br>
            Entre em contato comigo ;) <br>
            Obrigado. <br><br>
            Detalhes do pedido: <br>
            Cor: ' . $item['color'] . ' <br>
            Lado: ' . $item['side'] . ' <br>
            Tipo de papel: ' . $item['paper'] . ' <br>
            Acabamento: ' . $item['finishing']['type'] . ' <br>';

        if ($item['finishing']['binding']['border'] !== '') {
            $body = $body .
                'Borda : ' . $item['finishing']['binding']['border'] .
                '<br> Quantidade de encadernações: ' . $item['finishing']['binding']['total'] . '<br>';
        }

        if ($item['cupomCode'] !== '') {
            $body = $body . 'Cupom: ' . $item['cupomCode'] . ' <br>';
        }

        $fileUrl = $this->pdf->getFileUrl($item['files'][0]['name']);

        $body = $body .
            'Arquivo: ' . $fileUrl . ' <br>
            Número de páginas: ' . $item['files'][0]['numPages'] . ' <br>
            Observação: ' . $item['observation'] . ' <br>';

        return json_encode([
            'to_name' => 'Suporte DeliveryPrint',
            'to_email' => 'suporte@deliveryprint.com.br',
            'subject' => '[URGENTE] QUERO FECHAR MEU PEDIDO | DeliveryPrint',
            'subject_prefix' => '',
            'body' => $body
        ]);
    }
}
