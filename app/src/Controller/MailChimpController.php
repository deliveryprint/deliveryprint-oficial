<?php

namespace IntecPhp\Controller;

use IntecPhp\Service\MailChimpService;
use IntecPhp\Validator\InputValidator;
use IntecPhp\Model\ResponseHandler;
use Intec\Router\Request;

class MailChimpController
{
    private $mailChimp;
    private $lists;

    public function __construct(MailChimpService $mailChimp, array $lists)
    {
        $this->mailChimp = $mailChimp;
        $this->lists     = $lists;
    }

    public function subscribeEmailToList(Request $request)
    {
        $params = $request->getPostParams();

        $config = [
            'email' => [
                'validators' => [
                    'IsEmptyValidator' => [],
                    'EmailValidator' => [],
                    'StringLengthValidator' => [
                        'maxLength' => 100,
                    ],
                ],
            ],
            'list' => [
                'validators' => [
                    'IsEmptyValidator' => [],
                    'InArrayValidator' => [
                        'allowedValues' => ['test_local', 'sub_home']
                    ],
                ],
            ]
        ];

        try {
            $iv = new InputValidator($config);
            $iv->setData($params);

            if (!$iv->isValid()) {
                $errors = $iv->getErrorsMessages();
                $rh = new ResponseHandler(400, $iv->getGeneralErrorMessage(), $errors);
                return $rh->printJson();
            }

            $list = $params['list'];
            $data = $this->mailChimp->subscribeEmailToList($params['email'], $this->lists[$list]);

            if ($data) {
                $rh = new ResponseHandler(200, 'Seu email foi cadastrado com sucesso!');
            } else {
                $rh = new ResponseHandler(400, 'O email informado já está cadastrado ou não é válido!');
            }
        } catch (\Exception $e) {
            $rh = new ResponseHandler(400, $e->getMessage());
        }

        $rh->printJson();
    }
}
