<?php

namespace IntecPhp\Controller;

use IntecPhp\Model\ResponseHandler;
use Intec\Router\Request;
use IntecPhp\Model\Sms;

class SmsController
{
    private $sms;

    public function __construct(Sms $sms)
    {
        $this->sms = $sms;
    }

    public function saveReply(Request $request)
    {
        $params = get_object_vars(json_decode(file_get_contents("php://input")));

        try {
            if (empty($params['sms_id']) || empty($params['resposta'])) {
                throw new \Exception('Não foi encontrado retorno da busca de sms');
            }
            $dateReply = (new \DateTime($params['data_resposta']))->format('Y-m-d H:i:s');
            $this->sms->saveSmsReply($params['sms_id'], $params['resposta'], $dateReply);
            $rh = new ResponseHandler(200, 'ok');
        } catch (\Exception $e) {
            $rh = new ResponseHandler(400, $e->getMessage() . '. Código: ' . $e->getCode());
        }

        $rh->printJson();
    }

    public function listSms(Request $request)
    {
        $params = $request->getQueryParams();

        try {
            $data = $this->sms->getAll($params['from'], $params['to'], $params['filterType']);

            $rh = new ResponseHandler(200, 'ok', ['sms' => $data]);
        } catch (\Exception $e) {
            $rh = new ResponseHandler(400, $e->getMessage() . '. Código: ' . $e->getCode());
        }

        $rh->printJson();
    }

    public function createSmsCsv(Request $request)
    {
        $params = $request->getPostParams();

        try {
            $messages = $this->sms->getAll($params['from'], $params['to'], $params['filterType']);

            if (empty($messages)) {
                throw new \Exception('Não foram encontradas informações de sms com os filtros definidos');
            }

            $fileName = $this->sms->createSmsCsv($messages);

            $rp = new ResponseHandler(200, 'ok', ['fileName' => $fileName]);
        } catch (\Exception $e) {
            $rp = new ResponseHandler(400, $e->getMessage());
        }

        $rp->printJson();
    }
}
