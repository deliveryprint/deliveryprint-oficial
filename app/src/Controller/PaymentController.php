<?php

namespace IntecPhp\Controller;

use IntecPhp\Validator\InputValidator;
use IntecPhp\Model\ResponseHandler;
use IntecPhp\Model\Payment;
use IntecPhp\Model\Customer;
use Intec\Router\Request;
use IntecPhp\Model\Account;
use IntecPhp\Model\Order;
use IntecPhp\Model\Item;
use IntecPhp\Model\Invitation;
use IntecPhp\Entity\TbPedido;
use Pheanstalk\Pheanstalk;
use IntecPhp\Model\Config;
use IntecPhp\Model\Ip;
use IntecPhp\Service\AuthAccount;

use Exception;

class PaymentController
{
    const MINIMUM_VALUE = 100; // Valor mínimo do pedido em centavos
    const PROMOTION_MAX_VALUE = 5; // Valor máximo do pedido em promoção em centavos

    private $payment;
    private $customer;
    private $emailProducer;
    private $order;
    private $item;
    private $tbPedido;
    private $authAccount;
    private $ip;
    private $invitation;
    private $useCaptcha;

    public function __construct(AuthAccount $authAccount, Payment $payment, Customer $customer, Pheanstalk $emailProducer, Order $order, Item $item, TbPedido $tbPedido, Ip $ip, Invitation $invitation, $useCaptcha)
    {
        $this->payment       = $payment;
        $this->customer      = $customer;
        $this->emailProducer = $emailProducer;
        $this->order         = $order;
        $this->item          = $item;
        $this->tbPedido      = $tbPedido;
        $this->authAccount   = $authAccount;
        $this->ip            = $ip;
        $this->invitation    = $invitation;
        $this->useCaptcha    = $useCaptcha;
    }

    public function getFormattedECommerceInfo(Request $request)
    {
        $params = $request->getPostParams();

        try {

            if (empty($params['orderId'])) {
                throw new Exception("Necessário submeter o id do pedido");
            }

            $orderId = $params['orderId'];

            $order = $this->payment->getFormattedECommerceInfo($orderId);

            $rh = new ResponseHandler(200, 'ok', ['order' => $order]);
        } catch (Exception $e) {
            $rh = new ResponseHandler(400, $e->getMessage() . '. Código: ' . $e->getCode());
        }

        return $rh->printJson();
    }

    private function formatEmailOrderConfirmed($name, $email, $orderId)
    {
        $url = Config::getDomain('/meus-pedidos');
        return json_encode([
            'to_name' => $name,
            'to_email' => $email,
            'subject' => 'Confirmação de Pedido de Impressão | DeliveryPrint',
            'subject_prefix' => '',
            'body' => 'Olá, ' . $name . '<br><br>
             MARAVILHA! Recebemos seu pedido e vamos correr para entregá-lo o quanto antes!
             Para acompanhar o status do(s) pedido(s) <a href="' . $url . '">clique aqui</a>.
             <br><br>
             Abaixo, o(s) ID(s) de seu(s) Pedido(s): <br>
             - <b>' . $orderId . '</b><br><br>

             <br><br>
             Qualquer dúvida ou alteração, por favor nos informe:<br>
             Email: suporte@deliveryprint.com.br <br>
             Telefone: ​ (11) 3807-7562
             <br><br>
             Siga-nos em nossas redes sociais:<br>
             Instagram: http://instagram.com/deliveryprintapp<br>
             Facebook: https://www.facebook.com/deliveryprintapp
             <br><br>
             Grande abraço, <br>
             Equipe DeliveryPrint',
            'bcc_name' => 'Suporte Delivery',
            'bcc_email' => 'suporte@deliveryprint.com.br'
        ]);
    }

    private function formatEmailCommissionAuthor($email, $couponCode, $commission)
    {
        return json_encode([
            'to_name' => $email,
            'to_email' => $email,
            'subject' => 'OBA! Mais uma venda Conquistada | DeliveryPrint',
            'subject_prefix' => '',
            'body' => 'Olá,<br>
            Tudo bem?<br><br>
            Acaba de ser finalizada em nosso site uma venda usando o seu CUPOM DE DESCONTO ' . $couponCode . ' ;)<br><br>
            Com isso foi gerada uma comissão de R$' . $commission . ' para você!<br><br>
            Para saber o Valor de Comissão acumulado e solicitar o recebimento do mesmo, favor enviar um email com sua solicitação para adm@deliveryprint.com.br .<br>
            Agradecemos muito sua indicação para usar nossos serviços.<br><br>
            Att,<br>
            Equipe Comercial da DeliveryPrint<br>',
            'bcc_name' => 'Adm Delivery',
            'bcc_email' => 'adm@deliveryprint.com.br'
        ]);
    }

    private function formatEmailBankSlipWarning(string $name, string $email)
    {
        return json_encode([
            'to_name' => $name,
            'to_email' => $email,
            'subject' => 'Informação Importante sobre seu pagamento | DeliveryPrint',
            'subject_prefix' => '',
            'body' => '
            <p>
                Olá,<br>
                Tudo certinho? ;)<br>
            </p>
            <p>
                Como você escolheu o pagamento por boleto em nosso site, precisamos que nos envie o comprovante imediatamente ao finalizar o pedido.
            </p>
            <p>
                <i><u>Explico: o boleto pode demorar até dois dias úteis para ser compensado para nós</u></i>.
            </p>
            <p>
                Por conta disso, <b>nos enviando o comprovante</b>, nós já liberamos o seu pedido para produção e você o receberá no prazo escolhido!
            </p>
            <p>
                Se você fizer o pedido e deixar para enviar o comprovante depois, o prazo de entrega apresentado pelo sistema poderá sofrer alteração.
            </p>
            <p>
                Mas pode ficar tranquilo(a), <b>caso você não nos envie</b> o comprovante, o seu pedido será liberado assim que <u>o boleto for compensado pelo nosso sistema de pagamentos</u>.
            </p>
            <p>
                Possíveis alterações no prazo de entrega serão sempre sinalizadas pelo nosso time de atendimento, que entrará em contato com você por email ou ligação!
            </p>
            <p>
                Agradecemos pela confiança em nossos serviços!
            </p>
            <p>
                Qualquer dúvida, não hesite em entrar em contato conosco.
            </p>
            <p>
                Um grande abraço!<br>
                Att<br>
                Equipe DeliveryPrint<br>
            </p>'
        ]);
    }

    public function updateOrderStatusAfterPayment(Request $request)
    {
        error_log('iugu calback received');
        $params = $request->getPostParams();

        try {
            if (empty($params['data'])) {
                throw new Exception("Não foram submetidas as informações para atualizar a fatura!");
            }

            if ($params['data']['status'] === 'paid') {
                if (!$this->payment->updateOrderStatusAfterPayment($params['data']['id'])) {
                    $rh = new ResponseHandler(400, 'Não foi possível atualizar o status do pedido!');
                    return $rh->printJson();
                }
            }

            $commission = $this->payment->getCommissionValuesByIuguId($params['data']['id']);
            foreach ($commission as $com) {
                $this->emailProducer->put($this->formatEmailCommissionAuthor(
                    $com['commission_email'],
                    $com['codigo'],
                    number_format($com['commission'], 2, ',', '')
                ));
            }

            $rh = new ResponseHandler(200, 'ok');
        } catch (Exception $e) {
            $rh = new ResponseHandler(400, $e->getMessage() . '. Código: ' . $e->getCode());
        }

        return $rh->printJson();
    }

    public function getSessionBankSlip(Request $request)
    {
        try {
            if (!($paymentId = Account::getPayment())) {
                throw new Exception("Nenhum boleto foi registrado para se efetuar o seu pagamento!");
            }

            $data = $this->payment->getInvoice($paymentId);

            if ($data && $data['payable_with'] === 'bank_slip') {
                $rh = new ResponseHandler(200, 'ok', $data);
            } else {
                $rh = new ResponseHandler(400, 'Não existe um id de boleto válido relacionado ao pedido!');
            }
        } catch (Exception $e) {
            $rh = new ResponseHandler(400, $e->getMessage() . '. Código: ' . $e->getCode());
        }

        return $rh->printJson();
    }

    public function paySelectedOrder(Request $request)
    {
        $params = $request->getPostParams();

        $config = [
            'orderId' => [
                'validators' => [
                    'IsEmptyValidator' => [],
                    'IsNumericValidator' => []
                ]
            ]
        ];

        try {
            $iv = new InputValidator($config);
            $iv->setData($params);

            if (!$iv->isValid()) {
                $errors = $iv->getErrorsMessages();
                $rh = new ResponseHandler(400, $iv->getGeneralErrorMessage(), $errors);
                return $rh->printJson();
            }

            $orderId = $params['orderId'];
            $data    = $this->payment->paySelectedOrder($orderId);

            if ($data) {

                // Remover o id antigo da sessão, e remover o id de pagamento também, caso exista
                // Definir o novo id de pedido na sessão
                Account::unsetOrder();
                Account::setOrder($orderId);

                $rh = new ResponseHandler(200, 'ok', $data);
            } else {
                $rh = new ResponseHandler(400, 'O pedido informado é inválido para se efetuar seu pagamento!');
            }
        } catch (Exception $e) {
            $rh = new ResponseHandler(400, $e->getMessage() . '. Código: ' . $e->getCode());
        }

        return $rh->printJson();
    }

    private function saveUserInfo($userId, $userInfo)
    {
        $birth = null;
        if ($userInfo['birth']) {
            $birth = \DateTime::createFromFormat('d/m/Y', $userInfo['birth']);
        }

        return $this->customer->updateInfo(
            $userId,
            $userInfo['fullName'],
            $userInfo['cpf_cnpj'],
            $userInfo['tel'],
            $userInfo['cel'],
            $userInfo['email'],
            $birth
        );
    }

    private function getIps()
    {
        $remoteAddress = $_SERVER['REMOTE_ADDR'];
        $cfAddress = isset($_SERVER['HTTP_CF_CONNECTING_IP']) ? $_SERVER['HTTP_CF_CONNECTING_IP'] : null;
        $userIp = isset($cfAddress) ? $cfAddress : $remoteAddress;

        return [ 'remoteAddress' => $remoteAddress, 'cfAddress' => $cfAddress, 'userIp' => $userIp ];
    }

    private function needCaptcha()
    {
        if ($this->useCaptcha) {
            $ips = $this->getIps();
            return $this->ip->needCaptcha($ips['userIp']);
        } else {
            return false;
        }
    }

    // Verifica os dados enviados em requisições de pagamento
    private function verifyPaymentRequest($params)
    {
        $userId = $this->authAccount->get('id');

        if (!$userId) {
            throw new Exception("É necessário efetuar o login para prosseguir!");
        }

        if (!$this->customer->isVerified($userId)) {
            throw new Exception("Por favor, confirme seu endereço de e-mail");
        }

        if (empty($params['userInfo'])) {
            throw new Exception("Necessário submeter as informações de cadastro e de endereço do usuário");
        }

        if (!($orderId = Account::getOrder())) {
            throw new Exception("Não existe um pedido registrado na sessão");
        }

        $hasName = $this->order->hasDeliveryName($orderId);
        if (!$hasName) {
            throw new Exception('Para continuar é necessário inserir "Para quem entregar"');
        }

        // Testar o ReCaptcha
        if ($this->needCaptcha()) {
            if (empty($params['g-recaptcha-response'])) {
                throw new Exception("Para continuar é necessário responder o reCaptcha corretamente");
            } else {
                $response = $params['g-recaptcha-response'];
                $url = 'https://www.google.com/recaptcha/api/siteverify';
                $query = http_build_query(array(
                    'secret' => '6Le9nrAUAAAAAM7ctQlTt778SnRUqEdt5F1sfIxj',
                    'response' => $response
                ));
                $options = array(
                    'http' => array(
                        'header' => "Content-Type: application/x-www-form-urlencoded\r\n" .
                            "Content-Length: " . strlen($query) . "\r\n" .
                            "User-Agent:MyAgent/1.0\r\n",
                        'method' => 'POST',
                        'content' => $query
                    )
                );
                $context  = stream_context_create($options);
                $verify = file_get_contents($url, false, $context);
                $captcha_success = json_decode($verify);
                if ($captcha_success->success == false) {
                    throw new Exception('Para continuar é necessário responder o reCaptcha corretamente');
                }
            }
        }

        // Testar se o pedido possui id do cliente
        if (!$this->tbPedido->getCustomer($orderId)) {
            throw new Exception("É necessário efetuar o login para prosseguir!");
        }
    }

    private function verifyIpTries($orderId)
    {
        $ips = $this->getIps();
        $ipId = $this->ip->addNewTry($ips['userIp'], $ips['remoteAddress'], $ips['cfAddress'], $orderId);
        $this->tbPedido->updateIp($orderId, $ipId);
        if ($this->ip->maxTriesExceeded($ips['userIp'])) {
            $this->tbPedido->updateStatus($orderId, TbPedido::STATUS_BLOCKED);
            $this->item->updateStatusItemsOfOrder($orderId, TbPedido::STATUS_BLOCKED);
            return false;
        }
        return true;
    }

    private function verifyInvitationCode($userId, $orderId)
    {
        $items = $this->order->getInfoOrder($orderId);
        $result = false;

        foreach ($items as $item) {
            if ($couponId = $item["tbCupom_idDesc"]) {
                if ($invitatorId = $this->invitation->getInvitatorFromCouponId($couponId)) {
                    // Não deixar um cliente indicar ele mesmo
                    if ($invitatorId != $userId) {
                        $this->invitation->createInvitationCoupon($invitatorId);
                        $result = true;
                    }
                }
            }
        }

        return $result;
    }

    public function creditCardPayment(Request $request)
    {
        $params = $request->getPostParams();

        $config = [
            'paymentToken' => [
                'validators' => [
                    'IsEmptyValidator' => [],
                    'StringLengthValidator' => [
                        'minLength' => 32,
                        'maxLength' => 32,
                    ],
                ],
            ],
            'installments' => [
                'validators' => [
                    'IsEmptyValidator' => [],
                    'IsNumericValidator' => [],
                ],
            ],
            'email' => [
                'validators' => [
                    'IsEmptyValidator' => [],
                    'StringLengthValidator' => [
                        "maxLength" => 100,
                    ],
                    'EmailValidator' => [],
                ],
            ],
            'cardBanner' => [
                'validators' => [
                    'IsEmptyValidator' => [],
                    'StringLengthValidator' => [
                        'minLength' => 3,
                        'maxLength' => 30
                    ],
                ],
            ],
            'endCardNumber' => [
                'validators' => [
                    'IsEmptyValidator' => [],
                    'IsNumericValidator' => [],
                ],
            ]
        ];

        try {
            // Validar os dados da requisição
            $userInfo = $params['userInfo'];
            $params = array_merge($params, $userInfo);
            $iv = new InputValidator($config);
            $iv->setData($params);

            if (!$iv->isValid()) {
                $errors = $iv->getErrorsMessages();
                $rh = new ResponseHandler(400, $iv->getGeneralErrorMessage(), $errors);
                return $rh->printJson();
            }

            // Verificar se os dados enviados e o captcha estão corretos
            $this->verifyPaymentRequest($params);

            // Obter o id do pedido
            $orderId = Account::getOrder();

            // Valor final do pedido
            $finalValue = $this->payment->calcValueWithInstallments($orderId, $params['installments']);
            if ($finalValue < self::MINIMUM_VALUE) {
                throw new Exception("O valor do pedido deve ser de no mínimo R$ 1,00");
            }

            // Testar se existem pedidos demais no ip do usuário
            if (!$this->verifyIpTries($orderId)) {
                $rh = new ResponseHandler(400, 'blocked', ['id' => $orderId]);
                return $rh->printJson();
            }

            // Criar o pagamento na Iugu
            $data = $this->payment->makeCreditCardPayment($orderId, $params['paymentToken'], $finalValue, $params['installments'], $params['userInfo']);

            // Verificar se quem está comprando está usando um cupom de indicação e criar um cupom para o cliente que o indicou
            $userId = $this->authAccount->get('id');
            $this->verifyInvitationCode($userId, $orderId);

            // Salvando as informações do cartão no pedido
            $this->payment->saveBasicCardInfo($orderId, $userInfo['cardBanner'], $userInfo['endCardNumber']);

            $u = $this->payment->getInfoCliente($orderId);
            $this->emailProducer->put($this->formatEmailOrderConfirmed(
                $u['nomeCliente'],
                $u['emailCliente'],
                $orderId
            ));

            $commission = $this->payment->getCommissionValuesByOrderId($orderId);
            foreach ($commission as $com) {
                $this->emailProducer->put($this->formatEmailCommissionAuthor(
                    $com['commission_email'],
                    $com['codigo'],
                    number_format($com['commission'], 2, ',', '')
                ));
            }

            $data['order'] = $this->payment->getFormattedECommerceInfo($orderId);

            // Removendo o id do pedido da sessão
            Account::unsetOrder();
            Account::setPayment($orderId);

            $rh = new ResponseHandler(200, 'ok', $data);
        } catch (Exception $e) {
            $rh = new ResponseHandler(400, $e->getMessage() . '. Código: ' . $e->getCode());
        }

        return $rh->printJson();
    }

    public function bankSlipPayment(Request $request)
    {
        $params = $request->getPostParams();

        $config = [
            'fullName' => [
                'validators' => [
                    'IsEmptyValidator' => [],
                    'StringLengthValidator' => [
                        'minLength' => 3,
                        "maxLength" => 100
                    ]
                ]
            ],
            'email' => [
                'validators' => [
                    'IsEmptyValidator' => [],
                    'StringLengthValidator' => [
                        "maxLength" => 100
                    ],
                    'EmailValidator' => []
                ]
            ],
            'cpf_cnpj' => [
                'validators' => [
                    'IsEmptyValidator' => [],
                    'StringLengthValidator' => [
                        'minLength' => 14,
                        'maxLength' => 18
                    ]
                ]
            ],
            'zipCode' => [
                'validators' => [
                    'IsEmptyValidator' => [],
                    'StringLengthValidator' => [
                        'minLength' => 9,
                        "maxLength" => 9,
                    ]
                ],
            ],
            'street' => [
                'validators' => [
                    'IsEmptyValidator' => [],
                    'StringLengthValidator' => [
                        'minLength' => 3,
                        "maxLength" => 200,
                    ]
                ],
            ],
            'district' => [
                'validators' => [
                    'IsEmptyValidator' => [],
                    'StringLengthValidator' => [
                        'minLength' => 2,
                        "maxLength" => 50,
                    ]
                ],
            ],
            'number' => [
                'validators' => [
                    'IsEmptyValidator' => [],
                    'StringLengthValidator' => [
                        'minLength' => 1,
                        'maxLength' => 15
                    ]
                ]
            ],
            'state' => [
                'validators' => [
                    'IsEmptyValidator' => [],
                    'StringLengthValidator' => [
                        'minLength' => 2,
                        'maxLength' => 4
                    ]
                ]
            ],
            'complement' => [
                'validators' => [
                    'StringLengthValidator' => [
                        'maxLength' => 50
                    ]
                ]
            ]
        ];

        try {
            // Validar os dados da requisição
            $userInfo = $params['userInfo'];
            $iv = new InputValidator($config);
            $iv->setData($userInfo);

            if (!$iv->isValid()) {
                $errors = $iv->getErrorsMessages();
                $rh = new ResponseHandler(400, $iv->getGeneralErrorMessage(), $errors);
                return $rh->printJson();
            }

            // Verificar se os dados enviados e o captcha estão corretos
            $this->verifyPaymentRequest($params);

            // Obter o id do pedido
            $orderId = Account::getOrder();

            // Atualizando as informações do usuário
            $userId = $this->authAccount->get('id');
            $this->saveUserInfo($userId, $userInfo);

            // Valor final do pedido
            // $finalValue = $this->item->formattedFinalValue($orderId);
            $finalValue = $this->payment->getFinalValue($orderId);
            if ($finalValue < self::MINIMUM_VALUE) {
                throw new Exception("O valor do pedido deve ser de no mínimo R$ 1,00");
            }

            // Testar se existem pedidos demais no ip do usuário
            if (!$this->verifyIpTries($orderId)) {
                $rh = new ResponseHandler(400, 'blocked', ['id' => $orderId]);
                return $rh->printJson();
            }

            // Criar o pagamento na Iugu
            $data = $this->payment->makeBankSlipPayment($orderId, $finalValue, $params['userInfo']);

            // Verificar se quem está comprando está usando um cupom de indicação e criar um cupom para o cliente que o indicou
            $this->verifyInvitationCode($userId, $orderId);

            $u = $this->payment->getInfoCliente($orderId);
            $this->emailProducer->put($this->formatEmailOrderConfirmed(
                $u['nomeCliente'],
                $u['emailCliente'],
                $orderId
            ));

            $this->emailProducer->put($this->formatEmailBankSlipWarning($u['nomeCliente'], $u['emailCliente']));

            $data['order'] = $this->payment->getFormattedECommerceInfo($orderId);

            // Removendo o id do pedido da sessão e adicionando o id do pagamento na sessão
            Account::unsetOrder();
            Account::setPayment($orderId);

            $rh = new ResponseHandler(200, 'ok', $data);
        } catch (Exception $e) {
            $rh = new ResponseHandler(400, $e->getMessage() . '. Código: ' . $e->getCode());
        }

        return $rh->printJson();
    }

    public function promotionPayment(Request $request)
    {
        $params = $request->getPostParams();

        $config = [
            'fullName' => [
                'validators' => [
                    'IsEmptyValidator' => [],
                    'StringLengthValidator' => [
                        'minLength' => 3,
                        "maxLength" => 100
                    ]
                ]
            ],
            'email' => [
                'validators' => [
                    'IsEmptyValidator' => [],
                    'StringLengthValidator' => [
                        "maxLength" => 100
                    ],
                    'EmailValidator' => []
                ]
            ],
            'cpf_cnpj' => [
                'validators' => [
                    'IsEmptyValidator' => [],
                    'StringLengthValidator' => [
                        'minLength' => 14,
                        'maxLength' => 18
                    ]
                ]
            ],
            'zipCode' => [
                'validators' => [
                    'IsEmptyValidator' => [],
                    'StringLengthValidator' => [
                        'minLength' => 9,
                        "maxLength" => 9,
                    ]
                ],
            ],
            'street' => [
                'validators' => [
                    'IsEmptyValidator' => [],
                    'StringLengthValidator' => [
                        'minLength' => 3,
                        "maxLength" => 200,
                    ]
                ],
            ],
            'district' => [
                'validators' => [
                    'IsEmptyValidator' => [],
                    'StringLengthValidator' => [
                        'minLength' => 2,
                        "maxLength" => 50,
                    ]
                ],
            ],
            'number' => [
                'validators' => [
                    'IsEmptyValidator' => [],
                    'StringLengthValidator' => [
                        'minLength' => 1,
                        'maxLength' => 15
                    ]
                ]
            ],
            'state' => [
                'validators' => [
                    'IsEmptyValidator' => [],
                    'StringLengthValidator' => [
                        'minLength' => 2,
                        'maxLength' => 4
                    ]
                ]
            ],
            'complement' => [
                'validators' => [
                    'StringLengthValidator' => [
                        'maxLength' => 50
                    ]
                ]
            ]
        ];

        try {
            // Validar os dados da requisição
            $userInfo = $params['userInfo'];
            $iv = new InputValidator($config);
            $iv->setData($userInfo);

            if (!$iv->isValid()) {
                $errors = $iv->getErrorsMessages();
                $rh = new ResponseHandler(400, $iv->getGeneralErrorMessage(), $errors);
                return $rh->printJson();
            }

            // Verificar se os dados enviados e o captcha estão corretos
            $this->verifyPaymentRequest($params);

            // Obter o id do pedido
            $orderId = Account::getOrder();

            // Atualizando as informações do usuário
            $userId = $this->authAccount->get('id');
            $this->saveUserInfo($userId, $userInfo);

            // Valor final do pedido
            // $finalValue = $this->item->formattedFinalValue($orderId);
            $finalValue = $this->payment->getFinalValue($orderId);
            if ($finalValue > self::PROMOTION_MAX_VALUE) {
                throw new Exception("O valor do pedido deve ser menor que R$0,05");
            }

            // Testar se existem pedidos demais no ip do usuário
            if (!$this->verifyIpTries($orderId)) {
                $rh = new ResponseHandler(400, 'blocked', ['id' => $orderId]);
                return $rh->printJson();
            }

            // Criar o pagamento na Iugu
            $data = $this->payment->makePromotionPayment($orderId, $finalValue, $params['userInfo']);

            $u = $this->payment->getInfoCliente($orderId);
            $this->emailProducer->put($this->formatEmailOrderConfirmed(
                $u['nomeCliente'],
                $u['emailCliente'],
                $orderId
            ));

            $data['order'] = $this->payment->getFormattedECommerceInfo($orderId);

            // Removendo o id do pedido da sessão e adicionando o id do pagamento na sessão
            Account::unsetOrder();
            Account::setPayment($orderId);

            $rh = new ResponseHandler(200, 'ok', $data);
        } catch (Exception $e) {
            $rh = new ResponseHandler(400, $e->getMessage() . '. Código: ' . $e->getCode());
        }

        return $rh->printJson();
    }

    public function checkCaptcha(Request $request)
    {
        try {
            $rh = new ResponseHandler(200, 'ok', [ 'check' => $this->needCaptcha() ]);
        } catch (Exception $e) {
            $rh = new ResponseHandler(400, $e->getMessage() . '. Código: ' . $e->getCode());
        }

        return $rh->printJson();
    }
}
