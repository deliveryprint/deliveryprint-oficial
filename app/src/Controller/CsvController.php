<?php

namespace IntecPhp\Controller;

use IntecPhp\Model\ResponseHandler;
use Intec\Router\Request;
use IntecPhp\Model\Csv;

class CsvController
{
    private $csv;

    public function __construct(Csv $csv)
    {
        $this->csv = $csv;
    }

    public function save(Request $request)
    {
        $fileParams = $request->getFilesParams();

        try {
            if (empty($fileParams['csv'])) {
                throw new \Exception('O arquivo CSV não foi submetido');
            }

            $this->csv->setName($fileParams['csv']['name']);
            $this->csv->setTmp($fileParams['csv']['tmp_name']);
            $this->csv->setType($fileParams['csv']['type']);
            $this->csv->setErrorCode($fileParams['csv']['error']);
            $this->csv->setSize($fileParams['csv']['size']);

            $data = $this->csv->save();

            $rh = new ResponseHandler(200, 'ok', $data);
        } catch (\Exception $e) {
            $rh = new ResponseHandler(400, $e->getMessage() . '. Código: ' . $e->getCode());
        }

        $rh->printJson();
    }

    public function changePrices(Request $request)
    {
        $params = $request->getPostParams();
        $id = false;

        try {
            if (empty($params['file_id'])) {
                throw new \Exception('Não foi informada a faixa de preços a ser aplicada');
            }

            $id = $params['file_id'];

            $fileInfo = $this->csv->getInfo($params['file_id']);
            if (empty($fileInfo)) {
                $rh = new ResponseHandler(400, 'Não foi encontrado nenhum arquivo a partir do id informado');
            } else {
                $this->csv->applyRangePrice($fileInfo['arquivo']);
                $this->csv->updateCsvApplied($params['file_id']);
                $rh = new ResponseHandler(204);
            }
        } catch (\Exception $e) {
            if ($id) {
                $this->csv->updateCsvInvalid($id);
            }
            $rh = new ResponseHandler(400, $e->getMessage() . '. Código: ' . $e->getCode());
        }

        $rh->printJson();
    }

    public function listCsv(Request $request)
    {
        try {
            $data = $this->csv->getAll();

            $rh = new ResponseHandler(200, 'ok', ['files' => $data]);
        } catch (\Exception $e) {
            $rh = new ResponseHandler(400, $e->getMessage() . '. Código: ' . $e->getCode());
        }

        $rh->printJson();
    }

    public function downloadTempCsv(Request $request)
    {
        $params = $request->getQueryParams();

        try {
            if (empty($fileName = $params['fileName'])) {
                throw new \Exception('Não foi informado o nome do arquivo');
            }

            $tmpFile = "/tmp/$fileName";

            if (!file_exists($tmpFile)) {
                throw new \Exception('Arquivo não encontrado');
            }

            header('Content-disposition: attachment; filename=' . $fileName);
            header('Content-type: application/csv');
            readfile($tmpFile);
            exit;
        } catch (\Exception $e) {
            $rp = new ResponseHandler(400, $e->getMessage());
        }

        $rp->printJson();
    }

    public function prepareCsvForDownload(Request $request) {
        $params = $request->getPostParams();

        try {
            if (empty($file_id = $params['file_id'])) {
                throw new \Exception('Não foi informado o id do arquivo');
            }

            $file = $this->csv->getById($file_id);

            $original = $file['arquivo'];
            $fileName = $file['nome'];

            if (copy("app/data/csv/$original", "/tmp/$fileName") === false) {
                throw new \Exception("Erro ao tentar copiar o arquivo CSV");
            }

            $rp = new ResponseHandler(200, 'ok', [ 'fileName' => $fileName ]);

        } catch (\Exception $e) {
            $rp = new ResponseHandler(400, $e->getMessage());
        }

        $rp->printJson();
    }
}
