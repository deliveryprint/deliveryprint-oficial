<?php

namespace IntecPhp\Controller;

use Intec\Router\Request;
use IntecPhp\Model\ResponseHandler;
use IntecPhp\Model\ShippingPrices;

class ShippingPricesController
{
    private $shippingPrices;

    public function __construct(ShippingPrices $shippingPrices)
    {
        $this->shippingPrices = $shippingPrices;
    }

    public function getShippingPrices()
    {
        try {
            $prices = $this->shippingPrices->getShippingPrices();
            $rh = new ResponseHandler(200, 'ok', [
                'prices' => $prices
            ]);
        } catch (\Exception $e) {
            $rh = new ResponseHandler(400, $e->getMessage());
        }

        $rh->printJson();
    }

    public function updateDeliveryShippingPrices(Request $r)
    {
        $params = $r->getPostParams();

        try {
            $this->shippingPrices->updateShippingPrices(
                $this->shippingPrices->getFieldsDeliveryShippingPrices(),
                $params
            );

            $rh = new ResponseHandler(204);
        } catch (\Exception $e) {
            $rh = new ResponseHandler(400, $e->getMessage());
        }

        $rh->printJson();
    }

    public function updateWithdrawShippingPrices(Request $r)
    {
        $params = $r->getPostParams();

        try {
            $this->shippingPrices->updateShippingPrices(
                $this->shippingPrices->getFieldsWithdrawShippingPrices(),
                $params
            );

            $rh = new ResponseHandler(204);
        } catch (\Exception $e) {
            $rh = new ResponseHandler(400, $e->getMessage());
        }

        $rh->printJson();
    }
}
