<?php

namespace IntecPhp\Controller;

use IntecPhp\Model\ResponseHandler;
use Intec\Router\Request;
use IntecPhp\Model\Costs;

class CostsController
{
    private $costs;

    public function __construct(Costs $costs)
    {
        $this->costs = $costs;
    }

    public function getAllCosts(Request $request)
    {
        try {
            $data = $this->costs->getAllCosts();

            $rh = new ResponseHandler(200, 'ok', $data);
        } catch (\Exception $e) {
            $rh = new ResponseHandler(400, $e->getMessage() . '. Código: ' . $e->getCode());
        }

        $rh->printJson();
    }

    public function updatePrintCosts(Request $request)
    {
        $params = $request->getPostParams();

        try {
            if (empty($params['print_costs'])) {
                throw new \Exception("Os custos de impressão não foram submetidos");
            }

            $this->costs->updatePrintCosts($params['print_costs']);

            $rh = new ResponseHandler(204);
        } catch (\Exception $e) {
            $rh = new ResponseHandler(400, $e->getMessage() . '. Código: ' . $e->getCode());
        }

        $rh->printJson();
    }

    public function updateFinishCosts(Request $request)
    {
        $params = $request->getPostParams();

        try {
            if (empty($params['finish_costs'])) {
                throw new \Exception("Os custos de acabamento não foram submetidos");
            }

            $this->costs->updateFinishCosts($params['finish_costs']);

            $rh = new ResponseHandler(204);
        } catch (\Exception $e) {
            $rh = new ResponseHandler(400, $e->getMessage() . '. Código: ' . $e->getCode());
        }

        $rh->printJson();
    }
}
