<?php

namespace IntecPhp\Controller;

use IntecPhp\Model\ResponseHandler;
use IntecPhp\Validator\InputValidator;
use Intec\Router\Request;
use IntecPhp\Model\EmailMkt;
use Exception;

class EmailMktController
{
    private $emailMkt;

    public function __construct(EmailMkt $emailMkt)
    {
        $this->emailMkt = $emailMkt;
    }

    public function saveEmail(Request $r)
    {
        $params = $r->getPostParams();

        $config = [
            'email' => [
                'validators' => [
                    'StringLengthValidator' => [
                        "maxLength" => 100,
                    ],
                    'EmailValidator' => [],
                ],
            ]
        ];

        $iv = new InputValidator($config);
        $iv->setData($params);

        if (!$iv->isValid()) {
            $errors = $iv->getErrorsMessages();
            $rh = new ResponseHandler(400, $iv->getGeneralErrorMessage(), $errors);
            return $rh->printJson();
        }

        try {
            $email = $params['email'];
            $origin = $params['origin'];
            $idEmail = $this->emailMkt->saveEmail($email, $origin);
            $rh = new ResponseHandler(200, 'E-mail recebido com sucesso.', ['idEmail' => $idEmail]);
        } catch (Exception $e) {
            $rh = new ResponseHandler(400, $e->getMessage());
        }

        $rh->printJson();
    }

    public function getAllEmails($request)
    {
        $params = $request->getQueryParams();

        try {
            $items = $this->emailMkt->getAllInPeriod($params['period']);
            $rh = new ResponseHandler(200, 'ok', ['items' => $items]);
        } catch (Exception $e) {
            $rh = new ResponseHandler(400, $e->getMessage());
        }

        $rh->printJson();
    }

    public function getShoppingCartDiscountsByPeriod($request)
    {
        $params = $request->getQueryParams();

        try {
            $items = $this->emailMkt->getShoppingCartDiscountsByPeriod($params['period']);
            $rh = new ResponseHandler(200, 'ok', ['items' => $items]);
        } catch (Exception $e) {
            $rh = new ResponseHandler(400, $e->getMessage());
        }

        $rh->printJson();
    }

    public function createEmailsCsv(Request $request)
    {
        $params = $request->getPostParams();

        try {
            $header = $params["header"];
            if (empty($header)) {
                throw new \Exception('Forneça um cabeçalho para o arquivo csv.');
            }

            $items = $params["items"];
            if (empty($items)) {
                throw new \Exception('Não foi fornecido nenhum item.');
            }

            $fileName = $this->emailMkt->createEmailsCsv($header, $items);
            $rp = new ResponseHandler(200, 'ok', ['fileName' => $fileName]);
        } catch (\Exception $e) {
            $rp = new ResponseHandler(400, $e->getMessage());
        }

        $rp->printJson();
    }

}
