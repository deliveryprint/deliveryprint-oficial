<?php

namespace IntecPhp\Controller;

use IntecPhp\Model\PDF;
use IntecPhp\Model\ResponseHandler;
use Exception;

class PDFController
{
    private $pdf;

    public function __construct(PDF $pdf)
    {
        $this->pdf = $pdf;
    }

    public function getNumberOfPages($request)
    {
        $parser = new \Smalot\PdfParser\Parser();
        $fileParams = $request->getFilesParams();

        try {

            if(empty($fileParams['file'])) {
                throw new Exception("O tamanho do arquivo excede o limite máximo permitido.");
            }

            $data = [];

            // Checar se algum dos arquivos é protegido por senha
            // TODO: O código comentado abaixo lê o arquivo PDF e caso obtenha um erro, supõe que o PDF
            // está protegido por senha. Porém, está acontecendo um erro com arquivos grandes em que é
            // alocada uma memória maior que o limite. Por isso esse código ficará comentado e no futuro
            // será interessante procurar uma outra solução para esse problema da senha
            // foreach($fileParams['file']['name'] as $key => $val) {
            //     try {
            //         $parser->parseFile($fileParams['file']['tmp_name'][$key]);
            //     } catch (Exception $e) {
            //         throw new Exception("Este arquivo PDF não pode ser processado pois está protegido por senha. Por favor, desabilite a proteção por senha ou envie outro arquivo.");
            //     }
            // }

            foreach($fileParams['file']['name'] as $key => $val) {

                $name = $fileParams['file']['name'][$key];
                $tmp = $fileParams['file']['tmp_name'][$key];
                $type = $fileParams['file']['type'][$key];
                $errorCode = $fileParams['file']['error'][$key];
                $size = $fileParams['file']['size'][$key];

                $this->pdf->setName($name);
                $this->pdf->setTmp($tmp);
                $this->pdf->setType($type);
                $this->pdf->setErrorCode($errorCode);
                $this->pdf->setSize($size);

                $fileName = $this->pdf->save();

                $data[] = [
                    "fileName" => $fileName,
                    "userFileName" => $name,
                    "fileSize" => $size
                ];
            }

            $rh = new ResponseHandler(200, 'ok', $data);
        } catch (Exception $e) {
            $rh = new ResponseHandler(400);
            $rh->printCustomJson(
                [
                    "error" => $e->getMessage()
                ]
            );
            return;
        }

        $rh->printJson();
    }

    public function showDocument($request)
    {
        $pdfName = $request->getUrlParams()[0];

        if($pdfName) {
            if($this->pdf->fileExists($pdfName)) {
                header("Content-type: application/pdf");
                header("Content-Disposition: inline; filename=$pdfName.pdf");
                $this->pdf->readFile($pdfName);
                exit;
            }
        }

        $rp = new ResponseHandler(404);
        $rp->printJson();
    }
}
