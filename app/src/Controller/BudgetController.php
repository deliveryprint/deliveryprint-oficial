<?php

namespace IntecPhp\Controller;

use IntecPhp\Validator\InputValidator;
use IntecPhp\Model\ResponseHandler;
use Intec\Router\Request;
use Pheanstalk\Pheanstalk;

class BudgetController
{
    private $emailProducer;

    const REQUEST_BUDGET_WITH_DESC = 1;

    public function __construct(Pheanstalk $emailProducer)
    {
        $this->emailProducer = $emailProducer;
    }

    public function requestBudget(Request $request)
    {
        $params = $request->getPostParams();
        $errors = [];

        try {
            if (empty($params['user_email'])) {
                throw new \Exception('Email não informado');
            }

            $iv = new InputValidator([
                'user_email' => [
                    'validators' => [
                        'IsEmptyValidator' => [],
                        'StringLengthValidator' => [
                            "maxLength" => 100,
                        ],
                        'EmailValidator' => [],
                    ],
                ]
            ]);

            $iv->setData($params);

            if (!$iv->isValid()) {
                $errors = $iv->getErrorsMessages();
                $rh = new ResponseHandler(400, $iv->getGeneralErrorMessage(), $errors);
            } else {
                $this->emailProducer->put($this->formatEmailBudgetRequest(
                    $params['user_email'],
                    $params['user_name'],
                    $params['user_tel'],
                    $params['subject']
                ));

                $rh = new ResponseHandler(200, 'Seu pedido foi enviado!');
            }
        } catch (\Exception $e) {
            $rh = new ResponseHandler(400, $e->getMessage());
        }

        $rh->printJson();
    }

    private function formatEmailBudgetRequest($name, $email, $tel, $subject)
    {
        return json_encode([
            'to_name' => 'Suporte DeliveryPrint',
            'to_email' => 'suporte@deliveryprint.com.br',
            'subject' => $subject. ' | DeliveryPrint',
            'subject_prefix' => '',
            'body' => 'Olá, Tudo bem?
            <br><br>
            Seguem abaixo meus dados, me passe o orçamento com seu melhor preço :
            <br><br>
            Nome: ' .$name. '<br>
            E-mail: ' .$email. '<br>
            Telefone: ' .$tel. '<br>
            <br>
            Obrigado.',
        ]);
    }

    public function plottBudgetRequest(Request $request)
    {
        $params = $request->getPostParams();
        $errors = [];

        try {
            if (empty($params['user_name']) || empty($params['user_email']) || empty($params['description'])) {
                throw new \Exception('Preencha as Informações Solicitadas');
            }

            $iv = new InputValidator([
                'user_email' => [
                    'validators' => [
                        'IsEmptyValidator' => [],
                        'StringLengthValidator' => [
                            "maxLength" => 100,
                        ],
                        'EmailValidator' => [],
                    ],
                ]
            ]);

            $iv->setData($params);

            if (!$iv->isValid()) {
                $errors = $iv->getErrorsMessages();
                $rh = new ResponseHandler(400, $iv->getGeneralErrorMessage(), $errors);
            } else {
                $this->emailProducer->put($this->formatEmailPlotBudgetRequest(
                    $params['user_email'],
                    $params['user_name'],
                    $params['description'],
                    $params['subject']
                ));

                $rh = new ResponseHandler(200, 'Seu pedido foi enviado!');
            }
        } catch (\Exception $e) {
            $rh = new ResponseHandler(400, $e->getMessage());
        }

        $rh->printJson();
    }

    private function formatEmailPlotBudgetRequest($name, $email, $desc, $subject)
    {
        return json_encode([
            'to_name' => 'Suporte DeliveryPrint',
            'to_email' => 'suporte@deliveryprint.com.br',
            'subject' => $subject. ' | DeliveryPrint',
            'subject_prefix' => '',
            'body' => '
            Nome: ' .$name. '<br>
            E-mail: ' .$email. '<br>
            Como você quer sua impressão: ' .$desc. '<br>
            <br>
            Obrigado.',
        ]);
    }

    public function sendEmail(Request $request)
    {
        $params = $request->getPostParams();

        try {
            if (empty($params['emailType'])) {
                throw new \Exception('Não foi possível enviar o email');
            }

            switch ($params['emailType']) {
                case self::REQUEST_BUDGET_WITH_DESC:
                    $this->sendBudgetRequestWithDescription($request);
                    break;
                default:
                    throw new \Exception('Tipo de email não selecionado');
            }

            $rh = new ResponseHandler(200, 'Seu pedido foi enviado!');
        } catch (\Exception $e) {
            $rh = new ResponseHandler(400, $e->getMessage());
        }

        $rh->printJson();
    }

    public function sendBudgetRequestWithDescription(Request $request)
    {
        $params = $request->getPostParams();
        $errors = [];

        if (empty($params['user_name']) || empty($params['user_email']) || empty($params['description']) || empty($params['user_tel'])) {
            throw new \Exception('Preencha as Informações Solicitadas');
        }

        $iv = new InputValidator([
                'user_email' => [
                    'validators' => [
                        'IsEmptyValidator' => [],
                        'StringLengthValidator' => [
                            "maxLength" => 100,
                        ],
                        'EmailValidator' => [],
                    ],
                ]
            ]);

        $iv->setData($params);

        if (!$iv->isValid()) {
            throw new \Exception('Email inválido');
        } else {
            $this->emailProducer->put($this->formatEmailBudgetRequestWithDescription(
                $params['user_name'],
                $params['user_email'],
                $params['user_tel'],
                $params['description'],
                $params['subject']
            ));
        }
    }

    private function formatEmailBudgetRequestWithDescription($name, $email, $tel, $desc, $subject)
    {
        return json_encode([
            'to_name' => 'Suporte DeliveryPrint',
            'to_email' => 'suporte@deliveryprint.com.br',
            'subject' => $subject. ' | DeliveryPrint',
            'subject_prefix' => '',
            'body' => '
            Nome: ' .$name. '<br>
            E-mail: ' .$email. '<br>
            Telefone: ' .$tel. '<br>
            Como você quer sua impressão: <br>' .$desc. '<br>
            <br>
            Obrigado.',
        ]);
    }
}
