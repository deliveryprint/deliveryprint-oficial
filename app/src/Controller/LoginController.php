<?php

namespace IntecPhp\Controller;

use IntecPhp\Validator\InputValidator;
use IntecPhp\Entity\TbCliente;
use Intec\Router\Request;
use IntecPhp\Model\ResponseHandler;

use IntecPhp\Model\Account;
use IntecPhp\Service\AuthAccount;
use IntecPhp\Service\VerifyEmail;

use IntecPhp\Model\Order;
use IntecPhp\Model\PasswordRecovery;
use IntecPhp\Model\Config;
use Pheanstalk\Pheanstalk;

use Exception;

class LoginController
{
    private $authAccount;
    private $tbCliente;
    private $order;
    private $pass;
    private $emailProducer;
    private $verifyEmail;

    public function __construct(AuthAccount $authAccount, TbCliente $tbCliente, PasswordRecovery $pr, Order $order, Pheanstalk $emailProducer, VerifyEmail $verifyEmail)
    {
        $this->authAccount = $authAccount;
        $this->tbCliente = $tbCliente;
        $this->order = $order;
        $this->pass = $pr;
        $this->emailProducer = $emailProducer;
        $this->verifyEmail = $verifyEmail;
    }

    public function createAccount(Request $req)
    {
        $params = $req->getPostParams();
        $errors = [];

        try {
            $iv = new InputValidator([
                'email' => [
                    'validators' => [
                        'IsEmptyValidator' => [],
                        'StringLengthValidator' => [
                            "maxLength" => 100,
                        ],
                        'EmailValidator' => [],
                    ],
                ],
                'password' => [
                    'validators' => [
                        'IsEmptyValidator' => [],
                        'StringLengthValidator' => [
                            "minLength" => 6,
                        ],
                    ],
                ],
            ]);

            if (!empty($params['password']) && strlen($params['password']) < 6) {
                throw new Exception('Sua senha deve conter ao menos 6 dígitos');
            }

            $iv->setData($params);

            if (!$iv->isValid()) {
                $errors = $iv->getErrorsMessages();
                $rh = new ResponseHandler(400, implode(' . ', $errors), $errors);
                $rh->printJson();
                return;
            }

            $id = $this->tbCliente->createIfNotExists($params['email'], $params['password']);

            // Enviar e-mail para verificação do e-mail
            // if($needVerification = !$this->verifyEmail->check($params['email'])) {
            //     $this->sendVerificationEmail($id);
            // } else {
            //     $this->tbCliente->verifyAccount($id);
            // }
            $needVerification = false;

            $token = $this->authAccount->login(['id' => $id]);
            $this->linkUserOrderOnLogin($id);
            $rh = new ResponseHandler(200, 'Usuario criado com sucesso', [
                'needVerification' => $needVerification,
                'token' => $token
            ]);
        } catch (Exception $ex) {
            if ($ex->getCode() == 111) {
                $userInfo = $this->tbCliente->exists($params['email'], TbCliente::FIELD_EMAIL);
                if ($this->tbCliente->checkPassword($params['password'], $userInfo['senhaCliente'])) {
                    $token = $this->authAccount->login([
                        'id' => $userInfo['idCliente'],
                        'name' => $userInfo['nomeCliente']
                    ]);

                    $this->linkUserOrderOnLogin($userInfo['idCliente']);
                    $rh = new ResponseHandler(200, 'Login realizado com sucesso', [
                        'token' => $token
                    ]);
                } else {
                    $rh = new ResponseHandler(400, 'Usuário ou senha incorreta');
                }
            } else {
                $rh = new ResponseHandler(400, $ex->getMessage());
            }
        }
        $rh->printJson();
    }

    public function loginAdm(Request $req)
    {
        $params = $req->getPostParams();
        $errors = [];

        $iv = new InputValidator([
            'email' => [
                'validators' => [
                    'IsEmptyValidator' => [],
                    'StringLengthValidator' => [
                        "maxLength" => 100,
                    ],
                    'EmailValidator' => [],
                ],
            ],
            'password' => [
                'validators' => [
                    'IsEmptyValidator' => [],
                    'StringLengthValidator' => [
                        "minLength" => 6,
                    ],
                ],
            ],
        ]);

        $iv->setData($params);

        if (!$iv->isValid()) {
            $errors = $iv->getErrorsMessages();
            $rh = new ResponseHandler(400, implode(' . ', $errors), $errors);
        } else {
            $userInfo = $this->tbCliente->existsAdm($params['email']);
            if ($this->tbCliente->checkPassword($params['password'], $userInfo['senhaCliente'])) {
                $token = $this->authAccount->login([
                    'id' => $userInfo['idCliente'],
                    'name' => $userInfo['nomeCliente'],
                    'isAdmin' => true
                ]);
                $rh = new ResponseHandler(200, 'Login realizado com sucesso', [
                    'token' => $token
                ]);
            } else {
                $rh = new ResponseHandler(400, 'Usuário ou senha incorreta');
            }
        }
        $rh->printJson();
    }

    public function changePassword(Request $req)
    {
        $params = $req->getPostParams();
        $userId = $this->authAccount->get('id');

        $iv = new InputValidator([
            'newPassword' => [
                'validators' => [
                    'IsEmptyValidator' => [],
                    'StringLengthValidator' => [
                        "minLength" => 6,
                    ],
                ],
            ],
        ]);

        $iv->setData($params);

        if (!$iv->isValid()) {
            $errors = $iv->getErrorsMessages();
            $rh = new ResponseHandler(400, $iv->getGeneralErrorMessage(), $errors);
            return $rh->printJson();
        }

        try {
            $encryptedPass = $this->tbCliente->getPassword($userId);
            $isCorrect = $this->tbCliente->checkPassword($params['currentPassword'], $encryptedPass);

            if (!$isCorrect) {
                throw new Exception('Senha incorreta');
            }

            $result = $this->tbCliente->changePassword($userId, $params['newPassword']);

            if ($result === false) {
                throw new Exception('Não foi possível alterar a senha');
            }
            $rh = new ResponseHandler(200, 'senha alterada com sucesso');
        } catch (\Exception $e) {
            $rh = new ResponseHandler(400, $e->getCode() . ': ' . $e->getMessage());
        }

        $rh->printJson();
    }

    public function passwordRecovery($request)
    {
        $params = $request->getPostParams();

        try {
            if (empty($params['email'])) {
                throw new Exception('Email não informado');
            }

            if ($u = $this->tbCliente->exists($params['email'])) {
                $hash = $this->pass->createRecovery($params['email']);
                if ($hash) {
                    $url = Config::getDomain('/recuperar-senha?hash=' . $hash);
                    $this->emailProducer->put(json_encode([
                        'to_name' => $u['nomeCliente'],
                        'to_email' => $u['emailCliente'],
                        'subject' => 'Recuperação de Senha',
                        'body' => 'Uma requisição para troca de senha foi realizada na nossa
                        plataforma. Para alterar a senha <a href="' . $url . '">clique aqui</a>.<br><br>
                        Caso o botão não funcione, pode copiar a URL abaixo e colar em seu navegador.
                        Ela irá te levar para a página para criar uma nova senha:<br>' . $url
                    ]));
                }
            }

            $rp = new ResponseHandler(200);
        } catch (\Exception $e) {
            $rp = new ResponseHandler(400, $e->getMessage());
        }

        $rp->printJson();
    }

    public function changePasswordFromRecovery($request)
    {
        $params = $request->getPostParams();

        try {
            if (empty($params['hash'])) {
                throw new Exception('Requsição de troca de senha inválida');
            }

            if (empty($params['password'])) {
                throw new Exception('Senha não informada');
            }

            $hash = $params['hash'];

            if ($customerId = $this->pass->hashExists($hash)) {
                if ($this->tbCliente->changePassword($customerId, $params['password'])) {
                    $this->pass->disableHash($hash);
                }
            }
            $rp = new ResponseHandler(200);
        } catch (Exception $e) {
            $rp = new ResponseHandler(400, $e->getMessage());
        }

        $rp->printJson();
    }

    private function linkUserOrderOnLogin(int $userId)
    {
        $orderId = Account::getOrder();
        if ($newOrderId = $this->order->linkUserOrder($userId, $orderId)) {
            Account::setOrder($newOrderId);
        }
    }

    private function sendVerificationEmail($userId)
    {
        if ($customer = $this->tbCliente->exists($userId, TbCliente::FIELD_ID)) {
            $hash = $this->tbCliente->getHash($userId);
            if ($hash) {
                $url = Config::getDomain('/verificar-email?hash=' . $hash);
                $this->emailProducer->put(json_encode([
                    'to_name' => '',
                    'to_email' => $customer['emailCliente'],
                    'subject' => 'Confirmação de E-mail | DeliveryPrint',
                    'body' => 'Bem vindo à Delivery Print! Para confirmar seu e-mail <a href="' . $url . '">clique aqui</a>.',
                    'body' => '
                    <p>
                        Olá,<br>
                    </p>
                    <p>
                        Bem vindo à Delivery Print! Estamos muito felizes te ter você conosco.
                    </p>
                    <p>
                        Antes de fazer seu pedido, pedimos que você confirme seu endereço de e-mail <a href="' . $url . '">clicando aqui</a>.
                    </p>
                    <p>
                        Agora, caso o link acima não funcione, copie o seguinte endereço e cole na barra de endereço do seu navegador:
                    </p>
                    <p>
                        ' . $url . '
                    </p>
                    <p>
                        É só isso! Muito obrigado e boas impressões!
                    </p>
                    <p>
                        Um grande abraço!<br>
                        Att<br>
                        Equipe DeliveryPrint<br>
                    </p>'
                ]));
            }
        } else {
            throw new Exception('Usuário não encontrado.');
        }
    }

    public function resendEmail()
    {
        try {
            $userId = $this->authAccount->get('id');

            if (!$userId) {
                throw new Exception('Requsição inválida');
            }

            if ($lastTry = $this->tbCliente->getLastVerificationTry($userId)) {
                $lastTime = \DateTime::createFromFormat('Y-m-d H:i:s', $lastTry);
                $now = new \DateTime();
                $difference = $now->diff($lastTime);
                $minutes = ($difference->days * 24 * 60) + ($difference->h * 60) + $difference->i;

                if ($minutes > 15) {
                    $this->tbCliente->setLastVerificationTry($userId);
                    $this->sendVerificationEmail($userId);
                }
            } else {
                $this->tbCliente->setLastVerificationTry($userId);
                $this->sendVerificationEmail($userId);
            }

            $rp = new ResponseHandler(200, 'ok');
        } catch (Exception $e) {
            $rp = new ResponseHandler(400, $e->getMessage());
        }

        $rp->printJson();
    }

    public function verifyEmail($request)
    {
        $params = $request->getPostParams();

        try {
            if (empty($params['hash'])) {
                throw new Exception('Ocorreu um problema ao tentar verificar o e-mail. Por favor entre em contato com a Delivery Print.');
            }

            $hash = $params['hash'];

            if ($customerId = $this->tbCliente->getCustomerIdFromHash($hash)) {
                $this->tbCliente->verifyAccount($customerId);
            }
            $rp = new ResponseHandler(200, "E-mail verificado com sucesso!");
        } catch (Exception $e) {
            $rp = new ResponseHandler(400, $e->getMessage());
        }

        $rp->printJson();
    }

    public function checkVerification()
    {
        try {
            $userId = $this->authAccount->get('id');

            if (!$userId) {
                throw new Exception('Requsição inválida');
            }

            $needVerification = !$this->tbCliente->isVerified($userId);

            $rp = new ResponseHandler(200, 'ok', ['needVerification' => $needVerification]);
        } catch (Exception $e) {
            $rp = new ResponseHandler(400, $e->getMessage());
        }

        $rp->printJson();
    }
}
