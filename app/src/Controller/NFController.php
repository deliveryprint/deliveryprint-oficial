<?php

namespace IntecPhp\Controller;

use IntecPhp\Validator\InputValidator;
use IntecPhp\Model\ResponseHandler;
use IntecPhp\Model\NF;
use Intec\Router\Request;

class NFController
{

    private $nf;

    public function __construct(NF $nf)
    {
        $this->nf = $nf;
    }

    public function makeNF(Request $request)
    {
        $params = $request->getPostParams();

        try {
            if (empty($params['data'])) {
                throw new \Exception("Não foram submetidas as informações para gerar a Nota Fiscal de Serviço!");
            }

            if ($params['data']['status'] === 'paid') {
                if (!$this->nf->makeNF($params['data']['id'])) {
                    $rh = new ResponseHandler(400, 'Não foi possível gerar a nota fiscal!');
                    return $rh->printJson();
                }
            }
            $rh = new ResponseHandler(200, 'ok');
        } catch (\Exception $e) {
            $rh = new ResponseHandler(400, $e->getMessage() . '. Código: ' . $e->getCode());
        }

        return $rh->printJson();
    }

}
