<?php

namespace IntecPhp\Controller;

use IntecPhp\Model\Coupon;
use IntecPhp\Model\ResponseHandler;
use Intec\Router\Request;
use Exception;

class CouponManagerController
{
    private $coupon;

    public function __construct(Coupon $coupon)
    {
        $this->coupon = $coupon;
    }

    public function addCoupon($request)
    {
        $params = $request->getPostParams();

        try {
            $sdate = explode('/', $params['startdate']);
            if (!checkdate($sdate[1], $sdate[0], date('Y'))) {
                throw new Exception('Data de início da validate não é válida');
            }

            $edate = explode('/', $params['expiration_date']);
            if (!checkdate($edate[1], $edate[0], date('Y'))) {
                throw new Exception('Data de término da validate não é válida');
            }

            $this->coupon->save(
                $params['code'],
                $params['value'],
                \DateTime::createFromFormat('d/m', $params['startdate'])->setTime(0, 0),
                \DateTime::createFromFormat('d/m', $params['expiration_date']),
                $params['type'],
                $params['cupom_limit'],
                $params['email'],
                $params['cupom_user_limit'],
                (int)$params['commission_percent'],
                $params['comment']
            );

            $rp = new ResponseHandler(200, 'Cupom salvo com sucesso');
        } catch (Exception $ex) {
            $rp = new ResponseHandler(400, $ex->getMessage());
        }

        $rp->printJson();
    }

    public function find($request)
    {
        $params = $request->getQueryParams();

        try {
            $coupons = $this->coupon->find($params['status'], $params['codeOrEmail'], $params['hasEmail']);
            $rp = new ResponseHandler(200, 'ok', $coupons);
        } catch (Exception $e) {
            $rp = new ResponseHandler(400, $e->getMessage());
        }

        $rp->printJson();
    }

    public function removeCoupon($request)
    {
        $params = $request->getQueryParams();

        try {
            $this->coupon->remove($params['coupons']);
            $rp = new ResponseHandler(200, 'Cupons removidos com sucesso');
        } catch (Exception $e) {
            $rp = new ResponseHandler(400, $e->getMessage());
        }

        $rp->printJson();
    }

    public function deactiveCoupon($request)
    {
        $params = $request->getPostParams();

        try {
            $this->coupon->deactive($params['couponId']);
            $rp = new ResponseHandler(200, 'Cupom desativado com sucesso');
        } catch (Exception $e) {
            $rp = new ResponseHandler(400, $e->getMessage());
        }

        $rp->printJson();
    }

    public function activeCoupon($request)
    {
        $params = $request->getPostParams();

        try {
            $this->coupon->active(
                $params['couponId'],
                \DateTime::createFromFormat('d/m', $params['startdate']),
                \DateTime::createFromFormat('d/m', $params['validade'])
            );
            $rp = new ResponseHandler(200, 'Cupom ativado com sucesso');
        } catch (Exception $e) {
            $rp = new ResponseHandler(400, $e->getMessage());
        }

        $rp->printJson();
    }

    public function couponDetails($request)
    {
        $params = $request->getQueryParams();

        try {
            $data = $this->coupon->details($params['couponId']);
            $rp = new ResponseHandler(200, 'ok', $data);
        } catch (Exception $e) {
            $rp = new ResponseHandler(400, $e->getMessage());
        }

        $rp->printJson();
    }

    public function createCouponsCsv(Request $request)
    {
        $params = $request->getPostParams();

        try {
            $coupons  = $this->coupon->findUsed($params['status'], $params['codeOrEmail'], $params['hasEmail']);
            if (empty($coupons)) {
                throw new \Exception('Não foram encontradas informações de cupons com os filtros definidos');
            }
            $fileName = $this->coupon->createCouponsCsv($coupons);
            $rp = new ResponseHandler(200, 'ok', ['fileName' => $fileName]);
        } catch (\Exception $e) {
            $rp = new ResponseHandler(400, $e->getMessage());
        }

        $rp->printJson();
    }
}
