<?php

namespace IntecPhp\Controller;

use IntecPhp\Model\ResponseHandler;
use Intec\Router\Request;
use IntecPhp\Validator\InputValidator;
use Pheanstalk\Pheanstalk;
use Exception;

class ContactController
{
    private $emailProducer;

    public function __construct(Pheanstalk $emailProducer)
    {
        $this->emailProducer = $emailProducer;
    }

    public function sendContactEmail(Request $request)
    {
        $params = $request->getPostParams();

        try {

            $iv = new InputValidator([
                'email' => [
                    'validators' => [
                        'IsEmptyValidator' => [],
                        'StringLengthValidator' => [
                            "maxLength" => 100,
                        ],
                        'EmailValidator' => [],
                    ],
                ]
            ]);

            $iv->setData($params);

            if (!$iv->isValid()) {
                throw new \Exception('Email inválido');
            } else {
                $this->emailProducer->put($this->formatContactEmail(
                    $params['name'],
                    $params['email'],
                    $params['phone']
                ));
            }

            $rh = new ResponseHandler(200, 'E-mail foi enviado!');
        } catch (\Exception $e) {
            $rh = new ResponseHandler(400, $e->getMessage());
        }

        $rh->printJson();
    }

    public function sendCampaignEmail(Request $request)
    {
        $params = $request->getPostParams();

        try {

            $iv = new InputValidator([
                'email' => [
                    'validators' => [
                        'IsEmptyValidator' => [],
                        'StringLengthValidator' => [
                            "maxLength" => 100,
                        ],
                        'EmailValidator' => [],
                    ],
                ]
            ]);

            $iv->setData($params);

            if (!$iv->isValid()) {
                throw new \Exception('Email inválido');
            } else {
                $this->emailProducer->put($this->formatCampaignEmail(
                    $params['name'],
                    $params['email'],
                    $params['pages'],
                    $params['color'],
                    $params['city']
                ));
            }

            $rh = new ResponseHandler(200, 'E-mail foi enviado!');
        } catch (\Exception $e) {
            $rh = new ResponseHandler(400, $e->getMessage());
        }

        $rh->printJson();
    }

    private function formatContactEmail($name, $email, $phone)
    {
        $body = '
            Nome: ' . $name . ' <br>
            E-mail: ' . $email . ' <br>
            Telefone: ' . $phone . ' <br>';

        return json_encode([
            'to_name' => 'Suporte DeliveryPrint',
            'to_email' => 'suporte@deliveryprint.com.br',
            'subject' => 'LEAD IMPRESSORA | DeliveryPrint',
            'subject_prefix' => '',
            'body' => $body
        ]);
    }

    private function formatCampaignEmail($name, $email, $pages, $color, $city)
    {
        $body = '
            Nome: ' . $name . ' <br>
            E-mail: ' . $email . ' <br>
            Número de Páginas: ' . $pages . ' <br>
            Cor: ' . $color . ' <br>
            Cidade: ' . $city . ' <br>';

        return json_encode([
            'to_name' => 'Suporte DeliveryPrint',
            'to_email' => 'suporte@deliveryprint.com.br',
            'subject' => 'Impressão LP Externa | DeliveryPrint',
            'subject_prefix' => '',
            'body' => $body
        ]);
    }
}
