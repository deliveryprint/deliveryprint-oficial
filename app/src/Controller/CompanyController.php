<?php

namespace IntecPhp\Controller;

use IntecPhp\Model\Company;
use IntecPhp\Model\ResponseHandler;
use IntecPhp\Validator\InputValidator;
use IntecPhp\Service\AuthAccount;
use Intec\Router\Request;
use Pheanstalk\Pheanstalk;

class CompanyController
{
    private $company;
    private $authAccount;
    private $emailProducer;

    public function __construct(AuthAccount $authAccount, Company $company, Pheanstalk $emailProducer)
    {
        $this->company = $company;
        $this->authAccount = $authAccount;
        $this->emailProducer = $emailProducer;
    }

    public function saveCompany($request)
    {
        $data = $request->getPostParams();
        $customerId = $this->authAccount->get('id');

        $config = [
            'name' => [
                'validators' => [
                    'StringLengthValidator' => [
                        'minLength' => 3,
                        "maxLength" => 100,
                    ]
                ],
            ],
            'email' => [
                'validators' => [
                    'StringLengthValidator' => [
                        "maxLength" => 100,
                    ],
                    'EmailValidator' => [],
                ],
            ],
            'phone' => [
                'validators' => [
                    'StringLengthValidator' => [
                        'minLength' => 14,
                        "maxLength" => 15,
                    ],
                ],
            ]
        ];

        try {
            $iv = new InputValidator($config);
            $iv->setData($data);

            if (!$iv->isValid()) {
                $errors = $iv->getErrorsMessages();
                $rh = new ResponseHandler(400, $iv->getGeneralErrorMessage(), $errors);
                return $rh->printJson();
            }

            $this->company->add($data['name'], $data['phone'], $data['email']);

            $this->emailProducer->put($this->formatEmailNewCompany(
                $data['name'], $data['phone'], $data['email']
            ));

            $r = new ResponseHandler(200, 'ok');
        } catch (\Exception $e) {
            $r = new ResponseHandler(400, $e->getCode() . ": " . $e->getMessage());
        }

        $r->printJson();
    }

    private function formatEmailNewCompany($name, $phone, $email)
    {
        return json_encode([
            'to_name' => 'Suporte DeliveryPrint',
            'to_email' => 'suporte@deliveryprint.com.br',
            'subject' => 'Nova Empresa Cadastrada | DeliveryPrint',
            'subject_prefix' => '',
            'body' => '
                Um cliente cadastrou uma nova empresa! <br>
                Nome: ' . $name . '; <br>
                Telefone: ' . $phone . '; <br>
                Email: ' . $email . '; <br>'
        ]);
    }
}
