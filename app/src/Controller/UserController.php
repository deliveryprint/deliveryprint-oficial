<?php

namespace IntecPhp\Controller;

use IntecPhp\Model\User;
use IntecPhp\Model\ResponseHandler;
use IntecPhp\Model\Account;
use IntecPhp\Service\AuthAccount;
use Exception;

class UserController
{
    private $user;
    private $authAccount;

    public function __construct(AuthAccount $authAccount, User $user)
    {
        $this->user = $user;
        $this->authAccount = $authAccount;
    }

    public function myOrders()
    {
        try {
            $id = $this->authAccount->get('id');

            if (empty($id)) {
                $rh = new ResponseHandler(200);
            } else {
                $orders = $this->user->myOrders($id);
                $rh = new ResponseHandler(200, 'ok', $orders);
            }
        } catch (Exception $e) {
            $rh = new ResponseHandler(400, $e->getCode() . ": " . $e->getMessage());
        }

        $rh->printJson();
    }
}
