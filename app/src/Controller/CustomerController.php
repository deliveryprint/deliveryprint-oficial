<?php

namespace IntecPhp\Controller;

use IntecPhp\Model\Customer;
use IntecPhp\Model\CustomerAddress;
use IntecPhp\Model\Account;
use IntecPhp\Model\ResponseHandler;
use IntecPhp\Validator\InputValidator;
use IntecPhp\Service\AuthAccount;
use Intec\Router\Request;

class CustomerController
{
    private $cus;
    private $cusAddr;
    private $authAccount;

    public function __construct(AuthAccount $authAccount, Customer $cus, CustomerAddress $cusAddr)
    {
        $this->cus = $cus;
        $this->cusAddr = $cusAddr;
        $this->authAccount = $authAccount;
    }

    public function getInfo()
    {
        $customerId = $this->authAccount->get('id');

        try {
            $user = $this->cus->getInfo($customerId);

            $user["birth"] = $user["birth"] ? date("d/m/Y", strtotime($user["birth"])) : '';

            $addrs = $this->cusAddr->getAddress($customerId);
            $r = new ResponseHandler(200, 'ok', [
                'user' => $user,
                'address' => $addrs,
            ]);
        } catch (\Exception $e) {
            $r = new ResponseHandler(400, $e->getCode() . ": " . $e->getMessage());
        }

        $r->printJson();
    }

    public function updateInfo($request)
    {
        $data = $request->getPostParams();
        $customerId = $this->authAccount->get('id');

        $config = [
            'name' => [
                'validators' => [
                    'StringLengthValidator' => [
                        'minLength' => 3,
                        "maxLength" => 100,
                    ]
                ],
            ],
            'email' => [
                'validators' => [
                    'StringLengthValidator' => [
                        "maxLength" => 100,
                    ],
                    'EmailValidator' => [],
                ],
            ],
            'birth' => [
                'validators' => [
                    'DateValidator' => [
                        'format' => 'd/m/Y'
                    ],
                ],
            ],
            'cpf' => [
                'validators' => [
                    'StringLengthValidator' => [
                        'minLength' => 14,
                        'maxLength' => 18,
                    ],
                ],
            ],
            'tel' => [
                'validators' => [
                    'StringLengthValidator' => [
                        'minLength' => 14,
                        "maxLength" => 15,
                    ],
                ],
            ],
            'cel' => [
                'validators' => [
                    'StringLengthValidator' => [
                        'minLength' => 14,
                        "maxLength" => 15,
                    ],
                ],
            ]
        ];

        try {
            $iv = new InputValidator($config);
            $iv->setData($data);

            if (!$iv->isValid()) {
                $errors = $iv->getErrorsMessages();
                $rh = new ResponseHandler(400, $iv->getGeneralErrorMessage(), $errors);
                return $rh->printJson();
            }

            $birth = null;
            if ($data['birth']) {
                $birth = \DateTime::createFromFormat('d/m/Y', $data['birth']);
            }

            $this->cus->updateInfo(
                $customerId,
                $data['name'],
                $data['cpf'],
                $data['tel'],
                $data['cel'],
                $data['email'],
                $birth
            );

            $r = new ResponseHandler(200, 'ok');
        } catch (\Exception $e) {
            $r = new ResponseHandler(400, $e->getCode() . ": " . $e->getMessage());
        }

        $r->printJson();
    }

    public function updateAddrInfo($request)
    {
        $data = $request->getPostParams();
        $customerId = $this->authAccount->get('id');

        $config = [
            'addrId' => [
                'validators' => [
                    'IsEmptyValidator' => [],
                    'IsNumericValidator' => []
                ],
            ],
            'zip' => [
                'validators' => [
                    'IsEmptyValidator' => [],
                    'StringLengthValidator' => [
                        'minLength' => 9,
                        "maxLength" => 9,
                    ]
                ],
            ],
            'number' => [
                'validators' => [
                    'StringLengthValidator' => [
                        'minLength' => 1,
                        "maxLength" => 15,
                    ]
                ],
            ],
            'street' => [
                'validators' => [
                    'IsEmptyValidator' => [],
                    'StringLengthValidator' => [
                        'minLength' => 3,
                        "maxLength" => 200,
                    ]
                ],
            ],
            'district' => [
                'validators' => [
                    'IsEmptyValidator' => [],
                    'StringLengthValidator' => [
                        'minLength' => 2,
                        "maxLength" => 50,
                    ]
                ],
            ],
            'city' => [
                'validators' => [
                    'IsEmptyValidator' => [],
                    'StringLengthValidator' => [
                        'minLength' => 3,
                        "maxLength" => 50,
                    ]
                ],
            ],
            'uf' => [
                'validators' => [
                    'IsEmptyValidator' => [],
                    'StringLengthValidator' => [
                        'minLength' => 2,
                        "maxLength" => 2,
                    ]
                ],
            ],
            'compl' => [
                'validators' => [
                    'StringLengthValidator' => [
                        "maxLength" => 50,
                    ]
                ],
            ],
        ];

        try {
            $iv = new InputValidator($config);
            $iv->setData($data);

            if (!$iv->isValid()) {
                $errors = $iv->getErrorsMessages();
                $rh = new ResponseHandler(400, $iv->getGeneralErrorMessage(), $errors);
                return $rh->printJson();
            }

            $this->cusAddr->updateAddress(
                $data['addrId'],
                $data['zip'],
                $data['number'],
                $data['street'],
                $data['district'],
                $data['city'],
                $data['uf'],
                $data['compl']
            );

            $r = new ResponseHandler(200, 'ok');
        } catch (\Exception $e) {
            $r = new ResponseHandler(400, $e->getCode() . ": " . $e->getMessage());
        }

        $r->printJson();
    }

    public function addAddrInfo($request)
    {
        $data = $request->getPostParams();
        $customerId = $this->authAccount->get('id');

        $config = [
            'zip' => [
                'validators' => [
                    'IsEmptyValidator' => [],
                    'StringLengthValidator' => [
                        'minLength' => 9,
                        "maxLength" => 9,
                    ]
                ],
            ],
            'number' => [
                'validators' => [
                    'StringLengthValidator' => [
                        'minLength' => 1,
                        "maxLength" => 15,
                    ]
                ],
            ],
            'street' => [
                'validators' => [
                    'IsEmptyValidator' => [],
                    'StringLengthValidator' => [
                        'minLength' => 3,
                        "maxLength" => 200,
                    ]
                ],
            ],
            'district' => [
                'validators' => [
                    'IsEmptyValidator' => [],
                    'StringLengthValidator' => [
                        'minLength' => 2,
                        "maxLength" => 50,
                    ]
                ],
            ],
            'city' => [
                'validators' => [
                    'IsEmptyValidator' => [],
                    'StringLengthValidator' => [
                        'minLength' => 3,
                        "maxLength" => 50,
                    ]
                ],
            ],
            'uf' => [
                'validators' => [
                    'IsEmptyValidator' => [],
                    'StringLengthValidator' => [
                        'minLength' => 2,
                        "maxLength" => 2,
                    ]
                ],
            ],
            'compl' => [
                'validators' => [
                    'StringLengthValidator' => [
                        "maxLength" => 50,
                    ]
                ],
            ],
        ];

        try {
            $iv = new InputValidator($config);
            $iv->setData($data);

            if (!$iv->isValid()) {
                $errors = $iv->getErrorsMessages();
                $rh = new ResponseHandler(400, $iv->getGeneralErrorMessage(), $errors);
                return $rh->printJson();
            }

            $id = $this->cusAddr->addAddress(
                $customerId,
                $data['zip'],
                $data['number'],
                $data['street'],
                $data['district'],
                $data['city'],
                $data['uf'],
                $data['compl']
            );

            $data['addrId'] = $id;
            $r = new ResponseHandler(200, 'ok', $data);
        } catch (\Exception $e) {
            $r = new ResponseHandler(400, $e->getCode() . ": " . $e->getMessage());
        }

        $r->printJson();
    }

    public function listUsers(Request $request)
    {
        $params = $request->getQueryParams();

        try {
            $data = $this->cus->getAll($params['from'], $params['to'], $params['filterType']);

            $rh = new ResponseHandler(200, 'ok', ['users' => $data]);
        } catch (\Exception $e) {
            $rh = new ResponseHandler(400, $e->getMessage() . '. Código: ' . $e->getCode());
        }

        $rh->printJson();
    }

    public function createUsersCsv(Request $request)
    {
        $params = $request->getPostParams();

        try {
            $users = $this->cus->getAll($params['from'], $params['to'], $params['filterType']);

            if (empty($users)) {
                throw new \Exception('Não foram encontradas informações de usuários com os filtros definidos');
            }

            $fileName = $this->cus->createUsersCsv($users);

            $rp = new ResponseHandler(200, 'ok', ['fileName' => $fileName]);
        } catch (\Exception $e) {
            $rp = new ResponseHandler(400, $e->getMessage());
        }

        $rp->printJson();
    }
}
