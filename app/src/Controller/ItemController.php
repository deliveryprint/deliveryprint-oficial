<?php

namespace IntecPhp\Controller;

use IntecPhp\Model\Account;
use IntecPhp\Model\ResponseHandler;
use IntecPhp\Model\Item;
use IntecPhp\Model\PDF;
use IntecPhp\Model\Order;
use Exception;
use IntecPhp\Entity\TbPedido;
use Pheanstalk\Pheanstalk;
use IntecPhp\Service\SmsService;
use IntecPhp\Model\Sms;
use Intec\Router\Request;

class ItemController
{
    private $item;
    private $order;
    private $downloadAllFilesUrl;
    private $emailEnable;
    private $emailProducer;
    private $smsService;
    private $sms;
    private $pdf;

    public function __construct(
        Item $item,
        Order $order,
        $downloadAllFilesUrl,
        $emailEnable,
        Pheanstalk $emailProducer,
        SmsService $smsService,
        Sms $sms,
        PDF $pdf
    ) {
        $this->item                = $item;
        $this->order               = $order;
        $this->downloadAllFilesUrl = $downloadAllFilesUrl;
        $this->emailEnable         = $emailEnable;
        $this->emailProducer       = $emailProducer;
        $this->smsService          = $smsService;
        $this->sms                 = $sms;
        $this->pdf                 = $pdf;
    }

    public function deleteItem($r)
    {
        $params = $r->getPostParams();

        try {
            if (empty($params['id'])) {
                throw new Exception('Item não informado');
            }
            $itemId = $params['id'];
            $orderId = Account::getOrder();
            if (empty($orderId)) {
                throw new Exception('Carrinho vazio');
            }

            if ($this->item->itemIsOfOrder($orderId, $itemId)) {
                $this->item->deleteItem($itemId);
            }

            $rh = new ResponseHandler(204);
        } catch (Exception $e) {
            $rh = new ResponseHandler(400, $e->getCode() . ": " . $e->getMessage());
        }

        $rh->printJson();
    }

    public function saveItemObservation($r)
    {
        $params = $r->getPostParams();

        try {
            if (empty($params['id'])) {
                throw new Exception('Item não informado');
            }
            $itemId = $params['id'];
            $observation = $params['observation'];

            $orderId = Account::getOrder();
            if (empty($orderId)) {
                throw new Exception('Carrinho vazio');
            }

            if ($this->item->itemIsOfOrder($orderId, $itemId)) {
                $this->item->updateItemObservation($itemId, $observation);
            }

            $rh = new ResponseHandler(200, 'Alterações salvas com sucesso.');
        } catch (Exception $e) {
            $rh = new ResponseHandler(400, $e->getCode() . ": " . $e->getMessage());
        }

        $rh->printJson();
    }

    public function getItemObservation($r)
    {
        $params = $r->getPostParams();

        try {
            if (empty($params['id'])) {
                throw new Exception('Item não informado');
            }
            $itemId = $params['id'];

            $orderId = Account::getOrder();
            if (empty($orderId)) {
                throw new Exception('Carrinho vazio');
            }
            $data = [];
            if ($this->item->itemIsOfOrder($orderId, $itemId)) {
                $data = $this->item->getItemObservation($itemId);
            }

            $rh = new ResponseHandler(200, '', $data);
        } catch (Exception $e) {
            $rh = new ResponseHandler(400, $e->getCode() . ": " . $e->getMessage());
        }

        $rh->printJson();
    }

    public function getDetails($r)
    {
        $itemId = $r->getUrlParams()[0];

        try {
            $data = $this->item->getDetailsById($itemId);

            $data['downloadAllUrl'] = $this->downloadAllFilesUrl . $itemId;

            foreach ($data['files'] as &$file) {
                $fileName = $file['arquivo'];

                // Checar se o arquivo está associado ao item
                $fileInfo = $this->item->getFile($fileName);
                if (empty($fileInfo)) {
                    throw new Exception('Arquivo não encontrado');
                }

                // Retornar a url do arquivo
                $file['downloadUrl'] = $this->pdf->getFileUrl($fileName);
            }
            unset($file);

            $rh = new ResponseHandler(200, 'ok', $data);
        } catch (Exception $e) {
            $rh = new ResponseHandler(400, $e->getMessage());
        }

        $rh->printJson();
    }

    public function getAllItems($request)
    {
        $params = $request->getQueryParams();

        try {
            $items = $this->item->getAllStartingInRealized(
                $params['itemId'],
                $params['nameOrEmail'],
                $params['delivery']['freights'],
                $params['delivery']['withdrawals'],
                $params['paymentStatus'],
                $params['itemStatus'],
                $params['orderPeriod'],
                $params['deliveryPeriod']
            );
            $data = [
                'items' => $this->splitItems($items),
            ];
            $rh = new ResponseHandler(200, 'ok', $data);
        } catch (Exception $e) {
            $rh = new ResponseHandler(400, $e->getMessage());
        }

        $rh->printJson();
    }

    private function splitItems($items)
    {
        $splittedItems = [
            'normalItems' => [],
            'fastItems' => []
        ];

        $expressArr = [1, 2];

        foreach ($items as $item) {
            if (
                in_array($item['delivery']['rtype_id'], $expressArr) ||
                in_array($item['delivery']['frtype_id'], $expressArr)
            ) {
                $splittedItems['fastItems'][] = $item;
            } else {
                $splittedItems['normalItems'][] = $item;
            }
        }

        return $splittedItems;
    }

    public function updateAdmObservation($r)
    {
        $params = $r->getPostParams();

        try {
            $this->item->updateAdmObservation($params['id'], $params['observacao']);
            $rp = new ResponseHandler(200);
        } catch (Exception $e) {
            $rp = new ResponseHandler(400, $e->getMessage());
        }

        $rp->printJson();
    }

    public function changeItemStatus($r)
    {
        $params = $r->getPostParams();

        try {
            $this->item->updateStatus($params['id'], $params['status']);
            if ($this->order->itemsHaveSameStatus($params['orderId'], $params['status'])) {
                $this->order->updateStatus($params['orderId'], $params['status']);
                $this->notifyCustomer($params['orderId'], $params['status']);
                // if ($params['status'] == TbPedido::STATUS_DELIVERY) {
                //     $this->validateCellphoneAndSendSms($params['orderId']);
                // }
            }
            $rp = new ResponseHandler(200);
        } catch (Exception $ex) {
            $rp = new ResponseHandler(400, $ex->getMessage());
        }

        $rp->printJson();
    }

    public function notifyCustomer($orderId, $status)
    {
        $customer = $this->order
            ->geCustomerFromOrder($orderId);
        switch ($status) {
            case TbPedido::STATUS_CANCELED:
                if ($this->emailEnable['canceled'])
                {
                    $this->emailProducer->put(json_encode([
                        'to_name' => $customer['nomeCliente'],
                        'to_email' => $customer['emailCliente'],
                        'subject_prefix' => '',
                        'subject' => 'Cancelamento de Pedido | DeliveryPrint',
                        'body' =>
                        "<p>Olá!<br></p>

<p>Que pena! =(<br></p>

<p>Seu pedido acaba de ser cancelado​, como solicitado.<br></p>

<p>Gostaríamos de entender o motivo do cancelamento de seu pedido, para que possamos
melhorar cada vez mais nossos serviços e a sua experiência em nossas plataformas.<br></p>

<p>Se possível, entre em contato com nossos canais de atendimento:<br>
email: suporte@deliveryprint.com.br<br>
telefone: (11) 3807-7562<br></p>

<p>De toda maneira, adoraríamos tê-lo de volta conosco.<br></p>
<p>Agradecemos pelo interesse em nosso trabalho e estamos à disposição, sempre que
precisar!<br></p>

<p><b>Siga-nos em nossas Redes Sociais e Ajude nossa Startup a continuar Crescendo:</b><br>
Instagram: https://instagram.com/deliveryprintapp<br>
Facebook: https://www.facebook.com/deliveryprintapp<br>
</p>

<p>Um grande abraço,<br>
Equipe DeliveryPrint</p>"
                    ]));
                }
                break;
            case TbPedido::STATUS_PRODUCTION:
                if ($this->emailEnable['production'])
                {
                    $this->emailProducer->put(json_encode([
                        'to_name' => $customer['nomeCliente'],
                        'to_email' => $customer['emailCliente'],
                        'subject_prefix' => '',
                        'subject' => 'Seu pedido já está sendo produzido! | DeliveryPrint',
                        'body' =>
                        "<p>Olá!<br></p>
<p><b>Temos uma Atualização!</b><br></p>

<p>Seu pedido está ok e <b>já está sendo processado pelo setor de produção!</b><br></p>

<p>Agora é só relaxar que nós faremos todo o trabalho para você!<br></p>

<p>Tomaremos todo o cuidado e carinho possíveis para que seu pedido fique do jeito que você
espera.<br></p>

<p>Desde já, gostaríamos de agradecer pela confiança em nossos serviços.<br></p>

<p>Um grande abraço,<br>
Equipe DeliveryPrint</p>"
                    ]));
                }
                break;
            case TbPedido::STATUS_DISPATCH:
                if ($this->emailEnable['dispatch'])
                {
                    $this->emailProducer->put(json_encode([
                        'to_name' => $customer['nomeCliente'],
                        'to_email' => $customer['emailCliente'],
                        'subject_prefix' => '',
                        'subject' => 'Seu Pedido Está Chegando! | DeliveryPrint',
                        'body' =>
                        "<p>Olá!<br></p>
<p>Venho te dar uma boa notícia!<br></p>

<p><b>Seu pedido está pronto e acaba de ser despachado!</b><br></p>

<p>Pode ficar na boa! Ele será entregue conforme o prazo escolhido por você em nosso site!<br></p>

<p>Mais uma vez, gostaríamos de agradecer pela confiança em nossos serviços.<br></p>

<p>Qualquer dúvida ou alteração em seu pedido, por favor nos informe:<br>
email: suporte@deliveryprint.com.br<br>
Telefone: ​(11) 3807-7562<br></p>

<p><b>Siga-nos em nossas Redes Sociais e Ajude nossa Startup a continuar Crescendo:</b><br>
Instagram: https://instagram.com/deliveryprintapp<br>
Facebook: https://www.facebook.com/deliveryprintapp<br>
</p>

<p>Um grande abraço,<br>
Equipe DeliveryPrint</p>"
                    ]));
                }
                break;
            case TbPedido::STATUS_DELIVERY:
                if ($this->emailEnable['delivery'])
                {
                    $this->emailProducer->put(json_encode([
                        'to_name' => $customer['nomeCliente'],
                        'to_email' => $customer['emailCliente'],
                        'subject_prefix' => '',
                        'subject' => 'Seu pedido foi entregue! | DeliveryPrint',
                        'body' =>
                        "<p>Olá!<br></p>
<p><b>Recebeu seu pedido de impressão?!</b><br></p>

<p>Seu pedido já foi entregue ​e foi feito com muito carinho e cuidado por nossos colaboradores!<br></p>

<p>Caso tenha algum problema, dúvida ou sugestão basta responder esse email ou nos ligar. Iremos resolver rápido, de verdade!<br></p>

<p>Se deu tudo certo, ajude nossa empresa continuar crescendo, avalie seu pedido e uso de nosso site:<br></p>

<ul>

<li>Avaliar no Google: <a href=\"https://g.page/DeliveryPrint/review\">https://g.page/DeliveryPrint/review</a></li>

<li>Instagram: <a href=\"https://instagram.com/deliveryprintapp\">https://instagram.com/deliveryprintapp</a></li>

</ul>

<p>Aguardamos seus próximos pedido, conte conosco!<br></p>

<p>Um grande abraço,<br>
Equipe DeliveryPrint</p>"
                    ]));
                }
                break;
            case TbPedido::STATUS_WAITING_WITHDRAWAL:
                if ($this->emailEnable['waiting_withdrawal'])
                {
                    $this->emailProducer->put(json_encode([
                        'to_name' => $customer['nomeCliente'],
                        'to_email' => $customer['emailCliente'],
                        'subject_prefix' => '',
                        'subject' => 'Seu pedido já pode ser Retirado | DeliveryPrint',
                        'body' =>
                        "<p>Olá!<br></p>
<p>Venho te dar uma boa notícia!<br></p>

<p>Seu pedido está pronto e você já pode vir retirar como escolheu!<br></p>

<p>
Nosso endereço é:<br>
<b>Rua Oscar Freire, 2617, cj 410, Pinheiros.</b><br>
Ficamos próximo ao metrô, 1 quarteirão da estação Sumaré ;)<br></p>

<p>As retiradas podem ser feitas das <b>12h até as 19h em dias úteis</b>. Você tem até <b>1 mês para retirar seu pedido</b> ou
ele será descartado sem reembolso.<br></p>

<p><b>NÃO SE ESQUEÇA!</b><br>
Lembre de informar seu ID de pedido na hora da retirada na recepção, o número de seu pedido é: <br>
- " . $orderId . " <br></p>

<p>Qualquer dúvida ou alteração em seu pedido, por favor nos informe:<br>
email: suporte@deliveryprint.com.br<br>
telefone: (11) 3807-7562<br></p>

<p>Nos ajude a melhorar e continuar crescendo ​;)<br>
Avalie nosso serviço: https://www.facebook.com/deliveryprintapp/<br></p>

<p>Um grande abraço,<br>
Equipe DeliveryPrint</p>"
                    ]));
                }
                break;
        }
    }

    private function validateCellphoneAndSendSms(int $orderId)
    {
        $phoneNumber     = $this->order->getPhoneNumberById($orderId);
        $formattedNumber = preg_replace('/[^0-9]/', '', $phoneNumber);
        $firstDigit      = (int)substr($formattedNumber, 2, 1);
        if ($firstDigit <= 9 && $firstDigit >= 6) {
            if ($this->sms->numberAlreadyUsed($phoneNumber)) {
                return false;
            }

            $message     = "DeliveryPrint: Usando a nota de 1 a 10, sendo 1 Horrível e 10 sensacional, como você avalia o seu pedido de impressão recebido e atendimento de nossa empresa?";
            $currentDate = new \DateTime();
            $currentDate->setTime(0, 0);

            // O sms é enviado dois dias após o status do pedido ter sido alterado para entregue
            $idSms = $this->smsService->sendSms($formattedNumber, $message, $currentDate->add(new \DateInterval('P2D')));
            if ($idSms) {
                $this->sms->saveSms($idSms, $orderId, $phoneNumber, $message);
            }
            return $idSms;
        }

        return false;
    }

    public function changeFileStatus($r)
    {
        $params = $r->getPostParams();

        try {
            $this->item->updateFileIsOk($params['idArquivo'], $params['value']);
            $rp = new ResponseHandler(200);
        } catch (Exception $ex) {
            $rp = new ResponseHandler(400, $ex->getMessage());
        }

        $rp->printJson();
    }

    public function downloadAll($request)
    {
        $itemId = $request->getUrlParams()[0];

        try {
            $files = $this->item->getFiles($itemId);

            if (empty($files)) {
                throw new Exception('O item não possui arquivos');
            }

            // Chamar o PDF Model para gerar um zip com os arquivo do item
            $zipName = 'arquivos-item-' . $files[0]['pretty_id'] . '.zip';
            $zipPath = $this->pdf->createZipFile($zipName, $files);

            if ($zipPath) {
                header('Content-disposition: attachment; filename=' . $zipName);
                header('Content-type: application/zip');
                readfile($zipPath);
                exit;
            }
        } catch (Exception $e) {
            $rp = new ResponseHandler(400, $e->getMessage());
        }

        $rp->printJson();
    }

    public function download($request)
    {
        $file = $request->getUrlParams()[0];

        try {
            $fileInfo = $this->item->getFile($file);

            if (empty($fileInfo)) {
                throw new Exception('Arquivo não encontrado');
            }

            header('Content-disposition: attachment; filename=' . $fileInfo['nomeArquivo']);
            header('Content-type: application/pdf');

            $this->pdf->readFile($file);
            exit;
        } catch (Exception $e) {
            $rp = new ResponseHandler(400, $e->getMessage());
        }

        $rp->printJson();
    }

    public function updateTrackingCode(Request $request)
    {
        $params = $request->getPostParams();

        try {
            if (empty($params['itemId']) || !filter_var($params['itemId'], FILTER_VALIDATE_INT)) {
                throw new Exception('Não foi informado um id de item válido');
            }

            if (empty($params['code'])) {
                throw new Exception('O código de rastreio não foi informado');
            }

            $trackingCode = trim($params['code']);

            if (strlen($trackingCode) !== 13) {
                throw new Exception('O código de rastreio informado é inválido');
            }

            $data = $this->item->updateTrackingCode($params['itemId'], $trackingCode);

            if ($data['is_tracking']) {
                $this->notifyTrackingReady($data['order_id']);
            }

            $rp = new ResponseHandler(200, 'ok', []);
        } catch (Exception $ex) {
            $rp = new ResponseHandler(400, $ex->getMessage());
        }

        $rp->printJson();
    }

    private function notifyTrackingReady(int $orderId)
    {
        $customer = $this->order->geCustomerFromOrder($orderId);

        $this->emailProducer->put(json_encode(
            [
                'to_name' => $customer['nomeCliente'],
                'to_email' => $customer['emailCliente'],
                'subject_prefix' => '',
                'subject' => 'Você pode acompanhar a entrega de seu Pedido | DeliveryPrint',
                'body' =>
                "<p>Olá novamente,<br>
                Espero que esteja tudo bem!<br></p>

                <p>Você pode <b>acompanhar a entrega</b> de seu pedido acessando nosso site e fazendo seu login.<br>
                Após isso basta clicar em \"Meus pedidos\" e depois clicar em \"Ver entrega\" sobre o ID de seu pedido.<br></p>

                <p>Seu pedido será entregue até a data escolhida no momento da compra, as entregas ocorrem das 7h30 até as 19h.<br></p>

                <p>Qualquer dúvida ou dificuldade basta responder esse email ou nos ligar ;)<br>
                *Nosso atendimento funciona das 9h30 até as 19h em dias úteis<br></p>

                <p>Obrigado!<br>
                Att<br>
                Equipe Deliveryprint</p>"
            ]
        ));
    }
}
