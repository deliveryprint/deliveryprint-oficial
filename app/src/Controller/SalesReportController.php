<?php

namespace IntecPhp\Controller;

use IntecPhp\Model\ResponseHandler;
use Intec\Router\Request;
use IntecPhp\Model\SalesReport;

class SalesReportController
{
    private $salesReport;

    public function __construct(SalesReport $salesReport)
    {
        $this->salesReport = $salesReport;
    }

    public function generateSalesReport(Request $request)
    {
        $params = $request->getQueryParams();

        try {
            if (!isset($params['from']) || !isset($params['to'])) {
                throw new \Exception("Necessário submeter o intervalo de pesquisa");
            }

            $data = $this->salesReport->getData($params['from'], $params['to']);

            $rh = new ResponseHandler(200, 'ok', $data);
        } catch (\Exception $e) {
            $rh = new ResponseHandler(400, $e->getMessage() . '. Código: ' . $e->getCode());
        }

        $rh->printJson();
    }

    public function getColorsData(Request $request)
    {
        $params = $request->getQueryParams();

        try {
            if (!isset($params['from']) || !isset($params['to'])) {
                throw new \Exception("Necessário submeter o intervalo de pesquisa");
            }

            $data = $this->salesReport->getColors($params['from'], $params['to']);

            $rh = new ResponseHandler(200, 'ok', $data);
        } catch (\Exception $e) {
            $rh = new ResponseHandler(400, $e->getMessage() . '. Código: ' . $e->getCode());
        }

        $rh->printJson();
    }

    public function getSalesData(Request $request)
    {
        $params = $request->getQueryParams();

        try {
            if (!isset($params['from']) || !isset($params['to'])) {
                throw new \Exception("Necessário submeter o intervalo de pesquisa");
            }

            $data = $this->salesReport->getSalesDataByDate($params['from'], $params['to']);

            $rh = new ResponseHandler(200, 'ok', $data);
        } catch (\Exception $e) {
            $rh = new ResponseHandler(400, $e->getMessage() . '. Código: ' . $e->getCode());
        }

        $rh->printJson();
    }

    public function getFinishesData(Request $request)
    {
        $params = $request->getQueryParams();

        try {
            if (!isset($params['from']) || !isset($params['to'])) {
                throw new \Exception("Necessário submeter o intervalo de pesquisa");
            }

            $data = $this->salesReport->getFinishes($params['from'], $params['to']);

            $rh = new ResponseHandler(200, 'ok', $data);
        } catch (\Exception $e) {
            $rh = new ResponseHandler(400, $e->getMessage() . '. Código: ' . $e->getCode());
        }

        $rh->printJson();
    }

    public function getDeliveryTypesData(Request $request)
    {
        $params = $request->getQueryParams();

        try {
            if (!isset($params['from']) || !isset($params['to'])) {
                throw new \Exception("Necessário submeter o intervalo de pesquisa");
            }

            $data = $this->salesReport->countDeliveryTypes($params['from'], $params['to']);

            $rh = new ResponseHandler(200, 'ok', $data);
        } catch (\Exception $e) {
            $rh = new ResponseHandler(400, $e->getMessage() . '. Código: ' . $e->getCode());
        }

        $rh->printJson();
    }

    public function getRangesData(Request $request)
    {
        $params = $request->getQueryParams();

        try {
            if (!isset($params['from']) || !isset($params['to'])) {
                throw new \Exception("Necessário submeter o intervalo de pesquisa");
            }

            $data = $this->salesReport->getRangesData($params['from'], $params['to']);

            $rh = new ResponseHandler(200, 'ok', $data);
        } catch (\Exception $e) {
            $rh = new ResponseHandler(400, $e->getMessage() . '. Código: ' . $e->getCode());
        }

        $rh->printJson();
    }

    public function getCouponsData(Request $request)
    {
        $params = $request->getQueryParams();

        try {
            if (!isset($params['from']) || !isset($params['to'])) {
                throw new \Exception("Necessário submeter o intervalo de pesquisa");
            }

            $data = $this->salesReport->getCoupons($params['from'], $params['to']);

            $rh = new ResponseHandler(200, 'ok', $data);
        } catch (\Exception $e) {
            $rh = new ResponseHandler(400, $e->getMessage() . '. Código: ' . $e->getCode());
        }

        $rh->printJson();
    }

    public function getInviteCouponsUsed(Request $request)
    {
        $params = $request->getQueryParams();

        try {
            if (!isset($params['from']) || !isset($params['to'])) {
                throw new \Exception("Necessário submeter o intervalo de pesquisa");
            }

            $data = $this->salesReport->getInviteCouponsUsed($params['from'], $params['to']);

            $rh = new ResponseHandler(200, 'ok', $data);
        } catch (\Exception $e) {
            $rh = new ResponseHandler(400, $e->getMessage() . '. Código: ' . $e->getCode());
        }

        $rh->printJson();
    }

    public function getPeriodProfit(Request $request)
    {
        $params = $request->getQueryParams();

        try {
            if (!isset($params['from']) || !isset($params['to'])) {
                throw new \Exception("Necessário submeter o intervalo de pesquisa");
            }

            $data = $this->salesReport->getPeriodProfit($params['from'], $params['to']);

            $rh = new ResponseHandler(200, 'ok', ['billing_result' => $data]);
        } catch (\Exception $e) {
            $rh = new ResponseHandler(400, $e->getMessage() . '. Código: ' . $e->getCode());
        }

        $rh->printJson();
    }

    public function getAverageTicket(Request $request)
    {
        $params = $request->getQueryParams();

        try {
            if (!isset($params['from']) || !isset($params['to'])) {
                throw new \Exception("Necessário submeter o intervalo de pesquisa");
            }

            $data = $this->salesReport->calculateAverageTicket($params['from'], $params['to']);

            $rh = new ResponseHandler(200, 'ok', ['average_ticket' => $data]);
        } catch (\Exception $e) {
            $rh = new ResponseHandler(400, $e->getMessage() . '. Código: ' . $e->getCode());
        }

        $rh->printJson();
    }

    public function getCustomerSales(Request $request)
    {
        $params = $request->getQueryParams();

        try {
            if (!isset($params['from']) || !isset($params['to'])) {
                throw new \Exception("Necessário submeter o intervalo de pesquisa");
            }

            $data = $this->salesReport->getCustomerSales($params['from'], $params['to']);

            $rh = new ResponseHandler(200, 'ok', $data);
        } catch (\Exception $e) {
            $rh = new ResponseHandler(400, $e->getMessage() . '. Código: ' . $e->getCode());
        }

        $rh->printJson();
    }

    public function generateSalesReportCsv(Request $request)
    {
        $params = $request->getQueryParams();

        try {
            if (!isset($params['from']) || !isset($params['to'])) {
                throw new \Exception("Necessário submeter o intervalo de pesquisa");
            }

            $fileName = $this->salesReport->generateSalesReportCsv($params['from'], $params['to']);

            $rp = new ResponseHandler(200, 'ok', ['fileName' => $fileName]);
        } catch (\Exception $e) {
            $rp = new ResponseHandler(400, $e->getMessage());
        }

        $rp->printJson();
    }

    public function generateLtvTable(Request $request)
    {
        $params = $request->getQueryParams();

        try {
            if (!isset($params['from']) || !isset($params['to'])) {
                throw new \Exception("Necessário submeter o intervalo de pesquisa");
            }

            $fileName = $this->salesReport->generateLtvTable($params['from'], $params['to']);

            $rp = new ResponseHandler(200, 'ok', ['fileName' => $fileName]);
        } catch (\Exception $e) {
            $rp = new ResponseHandler(400, $e->getMessage());
        }

        $rp->printJson();
    }
}
