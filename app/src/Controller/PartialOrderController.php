<?php

namespace IntecPhp\Controller;

use IntecPhp\Model\OrderPartial;
use IntecPhp\Model\ResponseHandler;

class PartialOrderController
{
    private $op;
    private $storageEndpoint;

    public function __construct(OrderPartial $op, string $storageEndpoint)
    {
        $this->op = $op;
        $this->storageEndpoint = $storageEndpoint;
    }

    public function loadPartials()
    {
        $data = $this->op->getData();
        $data['storageEndpoint'] = $this->storageEndpoint;
        $rp = new ResponseHandler(200, 'ok', $data);
        $rp->printJson();
    }

}
