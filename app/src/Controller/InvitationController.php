<?php

namespace IntecPhp\Controller;

use IntecPhp\Model\ResponseHandler;
use IntecPhp\Model\Invitation;
use IntecPhp\Service\AuthAccount;
use Exception;

class InvitationController
{
    private $authAccount;
    private $invitation;

    public function __construct(AuthAccount $authAccount, Invitation $invitation)
    {
        $this->authAccount = $authAccount;
        $this->invitation = $invitation;
    }

    public function getInviteCodeByUser($request)
    {
        try {
            $userId = $this->authAccount->get('id');
            $inviteCode = $this->invitation->getInviteCodeByUser($userId);

            $rh = new ResponseHandler(200, '', ['codigo' => $inviteCode]);
        } catch (Exception $e) {
            $rh = new ResponseHandler(400, $e->getMessage());
        }

        $rh->printJson();
    }

    public function checkInviteCode($request)
    {
        $params = $request->getPostParams();

        $config = [
            'couponCode' => [
                'validators' => [
                    'IsEmptyValidator' => [],
                    'StringLengthValidator' => [
                        'maxLength' => 25
                    ]
                ]
            ]
        ];

        $iv = new InputValidator($config);
        $iv->setData($params);

        if (!$iv->isValid()) {
            $errors = $iv->getErrorsMessages();
            $rh = new ResponseHandler(400, $iv->getGeneralErrorMessage(), $errors);
            return $rh->printJson();
        }
        try {
            $userId = $this->authAccount->get('id');
            $inviteCode = $this->invitation->getInviteCodeByUser($userId);

            $rh = new ResponseHandler(200, '', ['codigo' => $inviteCode]);
        } catch (Exception $e) {
            $rh = new ResponseHandler(400, $e->getMessage());
        }

        $rh->printJson();
    }

    public function getAllInvitationCouponsByUser($request)
    {
        try {
            $userId = $this->authAccount->get('id');
            $coupons = $this->invitation->getAllInvitationCouponsByUser($userId);

            $rh = new ResponseHandler(200, '', ['items' => $coupons]);
        } catch (Exception $e) {
            $rh = new ResponseHandler(400, $e->getMessage());
        }

        $rh->printJson();
    }

    public function getAllInvitationCoupons($request)
    {
        try {
            $coupons = $this->invitation->getAllInvitationCoupons();

            $rh = new ResponseHandler(200, '', ['items' => $coupons]);
        } catch (Exception $e) {
            $rh = new ResponseHandler(400, $e->getMessage());
        }

        $rh->printJson();
    }
}
