<?php

namespace IntecPhp\Controller;

use IntecPhp\Model\ResponseHandler;
use IntecPhp\Model\Coupon;
use IntecPhp\Model\Account;
use IntecPhp\Model\Order;
use IntecPhp\Entity\TbCupom;
use IntecPhp\Validator\InputValidator;
use IntecPhp\Service\AuthAccount;
use Exception;

class CouponController
{
    private $coupon;
    private $order;
    private $authAccount;
    private $couponEnt;

    public function __construct(AuthAccount $authAccount, Coupon $coupon, Order $order, TbCupom $couponEnt)
    {
        $this->coupon = $coupon;
        $this->order = $order;
        $this->authAccount = $authAccount;
        $this->couponEnt = $couponEnt;
    }

    public function checkCoupon($r)
    {
        $params = $r->getPostParams();

        $config = [
            'couponCode' => [
                'validators' => [
                    'IsEmptyValidator' => [],
                    'StringLengthValidator' => [
                        'maxLength' => 25
                    ]
                ]
            ]
        ];

        $iv = new InputValidator($config);
        $iv->setData($params);

        if (!$iv->isValid()) {
            $errors = $iv->getErrorsMessages();
            $rh = new ResponseHandler(400, $iv->getGeneralErrorMessage(), $errors);
            return $rh->printJson();
        }

        try {
            $couponCode = $params['couponCode'];
            $idUser = $this->authAccount->get('id');
            $data = $this->coupon->checkCoupon($couponCode, $idUser, new \DateTime());
            $rh = $data ? new ResponseHandler(200, '', [
                'codigo' => $data['codigo'],
                'value' => $data['value'],
                'type' => $data['type']
            ]) : new ResponseHandler(400, 'Cupom inválido!');
        } catch (Exception $e) {
            $rh = new ResponseHandler(400, $e->getMessage());
        }

        $rh->printJson();
    }

    public function validateCoupon($r)
    {
        $params = $r->getPostParams();

        try {

            $idUser = $this->authAccount->get('id');

            for ($i = 0 ; $i < count($params["itens"]); $i++) {
                $couponId = $params["itens"][$i]["cupom"];
                $cp = $this->couponEnt->get($couponId);
                $couponCode = $cp['codigo'];
                if( !empty($couponCode) ) {
                    $validCoupon = $this->coupon->checkCoupon($couponCode, $idUser, new \DateTime());
                    if(empty($validCoupon)) {
                        throw new \Exception('Cupom inválido: ' . $couponCode . '.');
                    }
                }
            }

            $rh = new ResponseHandler(204);

        } catch (Exception $e) {
            $idOrder = Account::getOrder();
            Account::unsetOrder();
            //Alterar status do pedido aqui
            $upStatus = $this->order->updateStatus($idOrder, 'Cancelado');
            if($upStatus > 0){
                $rh = new ResponseHandler(400, 'Ocorreu um problema!');
            } else {
                $rh = new ResponseHandler(405, $e->getMessage() . ' Faça um novo pedido!');
            }

        }

        $rh->printJson();

    }

}
