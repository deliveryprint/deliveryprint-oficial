<?php

namespace IntecPhp\Worker;

use IntecPhp\Model\Order;
use IntecPhp\Model\PDF;
use IntecPhp\Entity\TbPedido;

class RemoveFilesWorker
{
    private $order;
    private $pdf;

    public function __construct(Order $order, PDF $pdf)
    {
        $this->order = $order;
        $this->pdf   = $pdf;
    }

    public function execute()
    {
        $files = $this->order->getUnecessaryFiles();

        // Arquivos com status "Carrinho"
        foreach ($files[TbPedido::STATUS_CART] as $cartFiles) {
            $this->pdf->removeFile($cartFiles['file']);
        }

        // Arquivos com status "Cancelado"
        foreach ($files[TbPedido::STATUS_CANCELED] as $cancelFiles) {
            $this->pdf->removeFile($cancelFiles['file']);
        }

        // Arquivos com status "Entregue"
        foreach ($files[TbPedido::STATUS_DELIVERY] as $deliveryFiles) {
            $this->pdf->removeFile($deliveryFiles['file']);
        }

        // Arquivos com status "Pagamento Pendente"
        foreach ($files[TbPedido::STATUS_PENDING] as $pendingFiles) {
            $this->pdf->removeFile($pendingFiles['file']);
        }
    }
}
