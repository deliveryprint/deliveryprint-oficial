<?php

namespace IntecPhp\Worker;

use IntecPhp\Model\Order;
use Pheanstalk\Pheanstalk;

class NotifyPendingPaymentWorker
{
    private $order;
    private $emailProducer;

    public function __construct(Order $order, Pheanstalk $emailProducer)
    {
        $this->order = $order;
        $this->emailProducer = $emailProducer;
    }

    public function execute()
    {
        $orders = $this->order->getPendingToNotify();
        $emails = [];

        foreach ($orders as $order) {
            if (!in_array($order['emailCliente'], $emails)) {
                $this->emailProducer->put($this->formatEmailPendingPayment(
                    $order['nomeCliente'],
                    $order['emailCliente']
                ));
                $emails[] = $order['emailCliente'];
            }
        }
    }

    private function formatEmailPendingPayment(string $name, string $email)
    {
        return json_encode([
            'to_name' => $name,
            'to_email' => $email,
            'subject' => 'Pagamento Pendente | DeliveryPrint',
            'subject_prefix' => '',
            'body' => '
            <p>Olá,<br>
            Tudo bem?
            </p>
            <p>
            Ainda não consta em nosso sistema o pagamento de seu pedido, por isso ainda não iniciamos a produção dele.<br><br>
            Você teve alguma dificuldade para realizar o pagamento?<br><br>
            Qualquer problema ou dúvida pode responder esse email que estamos a disposição para ajudar.<br><br>
            O seu pedido sofrerá alterações no prazo de entrega ou retirada já que ainda não consta o pagamento do mesmo.<br><br>
            PS: Caso você já tenha pago, por favor, nos avise para corrigirmos aqui as informações e peço desculpas por esse email.
            </p>
            <p>
            Att<br>
            Equipe DeliveryPrint
            </p>'
        ]);
    }
}
