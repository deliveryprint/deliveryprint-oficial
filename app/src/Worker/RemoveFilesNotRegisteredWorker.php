<?php

namespace IntecPhp\Worker;

use IntecPhp\Model\Order;
use IntecPhp\Model\PDF;

class RemoveFilesNotRegisteredWorker
{
    private $order;
    private $pdf;

    public function __construct(Order $order, PDF $pdf)
    {
        $this->order = $order;
        $this->pdf   = $pdf;
    }

    public function execute()
    {
        $currentDate = new \DateTime();
        $currentDate->sub(new \DateInterval('P2D'));
        $strOldDate = $currentDate->format('Y-m-d');

        $files = $this->pdf->getAllFilesNames();
        foreach ($files as $file) {
            if (($this->order->isFileRegistered($file) === false) && ($this->pdf->isFileOlderThan($strOldDate, $file))) {
                $this->pdf->removeFile($file);
            }
        }
    }
}
