<?php

namespace IntecPhp\Entity;

use IntecPhp\Model\DbHandler;

class TbColaborador extends AbstractEntity
{
    const CLASSNAME = 'tbcolaborador';
    protected $name = 'tbcolaborador';
    protected $id = 'idColaborador';

    const FIELD_ID = 'id';
    const FIELD_CPF = 'cpf';
    const FIELD_EMAIL = 'email';

    public function checkLogin($userId)
    {
        $stm = $this->conn->query('select senha from tbcolaborador where idColaborador = ?', [
            $userId
        ]);

        if ($stm) {
            return $stm->fetch()['senha'];
        }

        return false;
    }

    public function exists($field, $type = self::FIELD_EMAIL)
    {
        $sql = "";
        switch ($type) {
            case self::FIELD_ID:
                $sql = 'select * from tbcolaborador where idColaborador=?';
                break;
            case self::FIELD_EMAIL:
                $sql = 'select * from tbcolaborador where email=?';
                break;
            default:
                throw new \Exception('Tipo de campo inválido');
        }

        $stm = $this->conn->query($sql, [
            $field
        ]);

        if ($stm && $u = $stm->fetch()) {
            return $u;
        }

        return false;
    }

    public function checkPassword($password, $encryptedPass)
    {
        return password_verify($password, $encryptedPass);
    }

}
