<?php

namespace IntecPhp\Entity;

class FinishCostEntity extends AbstractEntity
{
    protected $name = 'custo_acabamento';
    protected $id = 'id';

    public function getAllWithDesc()
    {
        $stmt = $this->conn->query(
            "select c_acab.id, acab.descricaoAcabamento as finish, c_acab.valor as value
            from $this->name c_acab
            join tbacabamento acab on c_acab.tbAcabamento_idAcabamento = acab.idAcabamento
            order by c_acab.id",
            []
        );

        if ($stmt) {
            return $stmt->fetchAll();
        }

        throw new \Exception("Não foi possível consultar a tabela $this->name");
    }

    public function updateValueById(int $id, float $value)
    {
        $stmt = $this->conn->query(
            "update $this->name set valor = ? where id = ?",
        [$value, $id]
        );

        if ($stmt) {
            return $stmt->rowCount();
        }

        throw new \Exception("Não foi possível consultar a tabela $this->name");
    }
}
