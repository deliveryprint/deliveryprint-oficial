<?php

namespace IntecPhp\Entity;

class PrintCostEntity extends AbstractEntity
{
    protected $name = 'custo_impressao';
    protected $id = 'id';

    public function getAllWithDesc()
    {
        $stmt = $this->conn->query(
            "select imp.id, pap.descricaoPapel as paper, cor.descricaoCor as color, lad.descricaoLado as side, imp.valor as value
            from $this->name imp
            join tbpapel pap on imp.tbPapel_idPapel = pap.idPapel
            join tbcor cor on imp.tbCor_idCor = cor.idCor
            join tblado lad on imp.tbLado_idLado = lad.idLado
            order by imp.id",
            []
        );

        if ($stmt) {
            return $stmt->fetchAll();
        }

        throw new \Exception("Não foi possível consultar a tabela $this->name");
    }

    public function updateValueById(int $id, float $value)
    {
        $stmt = $this->conn->query(
            "update $this->name set valor = ? where id = ?",
        [$value, $id]
        );

        if ($stmt) {
            return $stmt->rowCount();
        }

        throw new \Exception("Não foi possível consultar a tabela $this->name");
    }
}
