<?php

namespace IntecPhp\Entity;

class TbFaixa extends AbstractEntity
{
    protected $name = 'tbfaixa';
    protected $id = 'idFaixa';

    public function getAllWithPaper()
    {
        $stmt = $this->conn->query('select tf.idFaixa, tf.de, tf.ate, tf.preco, tf.idPapel, tf.hasColor,
        tp.descricaoPapel from tbfaixa tf join tbpapel tp on tf.idPapel = tp.idPapel order by tf.idFaixa asc, tf.idPapel asc');

        if ($stmt) {
            return $stmt->fetchAll();
        }

        return false;
    }

    public function getMinMaxPrices()
    {
        $stmt = $this->conn->query('select de, ate, preco, hasColor from tbfaixa where idPapel = 1 and (de = 1 or ate = 1000000)');

        if ($stmt) {
            return $stmt->fetchAll();
        }

        return false;
    }

    public function getRangePrice($paperId, $totalPage, $hasColor)
    {
        $stmt = $this->conn->query('select * from tbfaixa where idPapel=? and de <= ? and ate >= ? and hasColor=? limit 1', [
            $paperId, $totalPage, $totalPage, $hasColor
        ]);

        if ($stmt) {
            return $stmt->fetch();
        }

        return false;
    }

    public function eraseRangePrice()
    {
        $stmt = $this->conn->query("delete from tbfaixa", []);

        if ($stmt) {
            return $stmt->rowCount();
        }

        throw new \Exception('Erro ao tentar apagar os preços antigos');
    }

    public function add(int $id, int $from, int $to, float $price, int $paperId, int $hasColor)
    {
        $stmt = $this->conn->query(
            "insert into tbfaixa(idFaixa, de, ate, preco, idPapel, hasColor) values(?, ?, ?, ?, ?, ?)",
            [$id, $from, $to, $price, $paperId, $hasColor]
        );

        if ($stmt) {
            return $this->conn->lastInsertId();
        }

        throw new \Exception("Não foi possível salvar a faixa de preço");
    }

    public function update(int $id, float $newPrice)
    {
        $stmt = $this->conn->query(
            "update tbfaixa set preco = ? where idFaixa = ?",
            [$newPrice, $id]
        );

        if ($stmt) {
            return true;
        }

        throw new \Exception("Não foi possível salvar a faixa de preço");
    }
}
