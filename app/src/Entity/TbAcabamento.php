<?php

namespace IntecPhp\Entity;

class TbAcabamento extends AbstractEntity
{
    protected $name = 'tbacabamento';
    protected $id = 'idAcabamento';

    public function getAllOrderedByPosition()
    {
        $stmt = $this->conn->query("select * from tbacabamento order by posicaoAcabamento");

        if ($stmt) {
            return $stmt->fetchAll();
        }

        throw new \Exception('Não foi possível ler os tipos de encadernação.');
    }

    public function hasHardcoverInIds(array $ids)
    {

        $paramsIds = implode(',', array_map(function(){
            return '?';
        }, $ids));

        $stmt = $this->conn->query("select * from tbacabamento where idAcabamento in ($paramsIds)
            and lower(descricaoAcabamento) like '%capa dura%'", $ids);

        if($stmt) {
            return $stmt->rowCount();
        }

        throw new \Exception('Não foi possível encontrar o tipo de encadernação');
    }

}
