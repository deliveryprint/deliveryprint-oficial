<?php

namespace IntecPhp\Entity;

use IntecPhp\Entity\AbstractEntity;

class TbIp extends AbstractEntity
{
    protected $name = 'tbip';
    protected $id = 'idIp';


    public function getByAddress($ipAddress)
    {
        $stmt = $this->conn->query(
            "select * from $this->name where userIp = ?",
            [ $ipAddress ]
        );

        if ($stmt) {
            return $stmt->fetchAll();
        }

        throw new \Exception('Não foi possível pesquisar os IPs');
    }

    public function add($userIp, $remoteAddress, $cfAddress, $orderId)
    {
        $stmt = $this->conn->query("insert into
        tbip(userIp, remoteAddress, httpCfConnectingIp, lastTime, tbPedido_idPedido)
        values(?, ?, ?, now(), ?)", [$userIp, $remoteAddress, $cfAddress, $orderId]);

        if ($stmt) {
            return $this->conn->lastInsertId();
        }

        throw new \Exception("Não foi possível salvar o IP.");
    }

    public function update($userIp, $remoteAddress, $cfAddress, $tries, $orderId)
    {
        $stmt = $this->conn->query(
            "update tbip set remoteAddress = ?, httpCfConnectingIp = ?, tries = ?,
             totalTries = totalTries + 1, lastTime = now(), tbPedido_idPedido = ?
             where userIp = ?",
            [$remoteAddress, $cfAddress, $tries, $orderId, $userIp]
        );

        if ($stmt) {
            $ipId = $this->conn->query("select idIp from tbip where userIp = ?", [$userIp])->fetch()['idIp'];
            return $ipId;
        }

        throw new \Exception("Não foi possível atualizar o IP.");
    }
}
