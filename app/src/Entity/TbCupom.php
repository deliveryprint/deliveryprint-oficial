<?php

namespace IntecPhp\Entity;

use IntecPhp\Entity\AbstractEntity;

class TbCupom extends AbstractEntity
{
    protected $name = 'tbcupom';
    protected $id = 'idDesc';

    const COUPON_INACTIVE = 0;
    const COUPON_ACTIVE = 1;
    const COUPON_BOTH = 2;

    const TYPE_PERCENT = 'PERCENT';

    public function checkCoupon($code, $testDate)
    {
        $stmt = $this
            ->conn
            ->query("select * from $this->name where codigo = ?
                and ? between startdate and validade and status = 1", [
                    $code, $testDate
        ]);

        if ($stmt) {
            $row = $stmt->fetch();
            if ($row) {
                return $row;
            }

            return 0;
        }

        throw new \Exception('Não foi possível procurar pelo cupom de desconto');
    }

    public function countUserCouponItems($couponId, $userId)
    {
        $stmt = $this
            ->conn
            ->query("select count(i.tbcupom_idDesc) as user_usage from $this->name c
                join tbitem i on i.tbcupom_idDesc = c.idDesc
                join tbpedido p on p.idPedido = i.tbPedido_idPedido
                where c.idDesc = ? and p.tbCliente_idCliente = ?", [
                $couponId, $userId
            ]);

        if ($stmt) {
            $res = $stmt->fetch();
            if ($res) {
                return $res['user_usage'];
            }

            return 0;
        }

        throw new \Exception('Não foi possível contar os usos do cupom pelo usuário');
    }

    public function incCounter($couponId)
    {
        $stmt = $this->conn->query('update tbcupom set cupom_counter = cupom_counter + 1 where idDesc = ?', [$couponId]);

        if ($stmt) {
            return $stmt->rowCount();
        }

        throw new \Exception('Não foi possível utilizar o cupom de desconto');
    }

    public function decCounter($couponId)
    {
        $stmt = $this->conn->query('update tbcupom set cupom_counter = cupom_counter - 1 where idDesc = ?', [$couponId]);

        if ($stmt) {
            return $stmt->rowCount();
        }

        throw new \Exception('Não foi possível utilizar o cupom de desconto');
    }

    public function exists($couponCode)
    {
        $stmt = $this->conn->query('select * from tbcupom where codigo=? limit 1', [$couponCode]);

        if ($stmt) {
            return $stmt->rowCount();
        }

        throw new \Exception('Não foi possível verificar a existência do cupom');
    }

    public function save(
        string $code,
        float $value,
        $startdate,
        $expirationDate,
        string $type,
        int $cuponLimit,
        string $email,
        int $cuponUserLimit,
        int $commissionPercent,
        string $comment,
        int $userId = null,
        bool $invite_coupon = null
    ) {
        $stmt = $this->conn->query('insert into tbcupom
            (codigo, value, startdate, validade, type, cupom_limit, commission_email, cupom_user_limit, commission_percent, comment, status, cupom_counter, invite_coupon, tbCliente_idCliente)
            values(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, 1, 0, ?, ?)', [
                $code, $value, $startdate, $expirationDate,
                $type, $cuponLimit, $email, $cuponUserLimit,
                $commissionPercent, $comment, $invite_coupon, $userId
        ]);

        if ($stmt) {
            return true;
        }

        throw new \Exception('Não foi possível criar o cupom');
    }

    public function find(int $status, string $codeOrEmail, int $hasEmail)
    {
        $sql = "select t.*, (total_sales + total_delivery) as sales_delivery, i.total_discount_items from tbcupom t left join (select tbitem.tbcupom_idDesc, sum(tbitem.descTotal) as total_discount_items from tbitem group by tbitem.tbcupom_idDesc) i on t.idDesc = i.tbcupom_idDesc where t.tbCliente_idCliente is null";
        $params = [];

        if (!$hasEmail) {
            $sql .= ' and commission_email = ?';
            $params[] = '';
        } elseif ($hasEmail == 1) {
            $sql .= ' and commission_email <> ?';
            $params[] = '';
        }

        if (!in_array($status, [
            self::COUPON_INACTIVE,
            self::COUPON_ACTIVE,
            self::COUPON_BOTH
        ])) {
            throw new \Exception('Status inválido');
        }

        if ($status != self::COUPON_BOTH) {
            $sql .= ' and status=? ';
            $params[] = $status;
        }

        if ($codeOrEmail) {
            $sql .= ' and concat(codigo, commission_email) like ?';
            $params[] = "%$codeOrEmail%";
        }

        $stmt = $this->conn->query($sql, $params);

        if ($stmt) {
            return $stmt->fetchAll();
        }

        throw new \Exception('Não foi possível consultar os cupons');
    }

    public function isPercent($type)
    {
        return $type === self::TYPE_PERCENT;
    }

    public function updatePrices($id, $incDiscount, $incSale, $incDeliveryValue)
    {
        $stmt = $this->conn->query('update tbcupom
            set total_discount = total_discount + ?,
            total_sales = total_sales + ?, total_delivery = total_delivery + ? where idDesc = ?', [
                $incDiscount, $incSale, $incDeliveryValue, $id
        ]);

        if ($stmt) {
            return $stmt->rowCount();
        }

        throw new \Exception('Não foi possível atualizar os valores do cupom');
    }

    public function remove(array $coupons)
    {
        if (empty($coupons)) {
            throw new \Exception('Nenhum cupom informado');
        }

        $arr = implode(', ', array_fill(0, count($coupons), '?'));
        $stmt = $this->conn->query("delete from tbcupom where idDesc in ($arr)", $coupons);

        if ($stmt) {
            return $stmt->rowCount();
        }

        throw new \Exception('Não foi possível remover o(s) cupom(ns). Apenas cupons não utilizados podem ser removidos.');
    }

    public function deactive($couponId)
    {
        $stmt = $this->conn->query('update tbcupom set status=0 where idDesc = ?', [
            $couponId
        ]);

        if ($stmt) {
            return $stmt->rowCount();
        }

        throw new \Exception('Não foi possível desativar o cupom');
    }

    public function active($couponId, $startdate, $enddate)
    {
        $stmt = $this->conn->query('update tbcupom set status=1, startdate=?, validade=? where idDesc = ?', [
            $startdate, $enddate, $couponId
        ]);

        if ($stmt) {
            return $stmt->rowCount();
        }

        throw new \Exception('Não foi possível desativar o cupom');
    }

    public function details($couponId)
    {
        // join gigante!
        $stmt = $this->conn->query('
        select sum(p.itemTotalPage) as total_pages,
        SUM(case when i.tbCor_idCor = 1 then p.itemTotalPage else 0 end) as pb,
        SUM(case when i.tbCor_idCor = 2 then p.itemTotalPage else 0 end) as color,
        SUM(case when i.tbAcabamento_idAcabamento = 1 then 1 else 0 end) as folha_solta,
        SUM(case when i.tbAcabamento_idAcabamento = 2 then 1 else 0 end) as grampeado,
        SUM(case when i.tbAcabamento_idAcabamento = 3 then 1 else 0 end) as espiral,
        SUM(case when i.tbAcabamento_idAcabamento = 4 then 1 else 0 end) as capa_dura_a4,
        SUM(case when i.tbAcabamento_idAcabamento = 5 then 1 else 0 end) as capa_dura_a3,
        SUM(case when i.tbAcabamento_idAcabamento = 6 then 1 else 0 end) as wire_o,
        min(o.dataPedido) as first_use,
        max(o.dataPedido) as last_use
        from tbcupom c
        join tbitem i on i.tbcupom_idDesc = c.idDesc
        join tbpedido o on o.idPedido = i.tbPedido_idPedido
        join (select sum(t.numPaginas * t.qtdImpressoes) as itemTotalPage, t.tbItem_idItem from tbarquivo t group by t.tbItem_idItem) p on p.tbItem_idItem = i.idItem where idDesc = ? and i.statusItem != ?', [
            $couponId, TbPedido::STATUS_CART
        ]);

        if ($stmt) {
            return $stmt->fetch();
        }

        throw new \Exception('Não foi possível consultar os detalhes do cupom');
    }

    public function getStatesAndCitiesOf($couponId)
    {
        $stmt = $this->conn->query('select distinct a.uf, a.cidade from tbentrega d
        join tbitem i on i.idItem = d.id_item
        join tbendereco a on a.idEndereco = d.tbEndereco_idEndereco
        join tbcupom c on c.idDesc = i.tbcupom_idDesc where c.idDesc = ?', [
            $couponId
        ]);

        if ($stmt) {
            return $stmt->fetchAll();
        }

        throw new \Exception('Não foi possível consultar os estados');
    }

    public function findUsed(int $status, string $codeOrEmail, int $hasEmail)
    {
        $sql = "select concat(item.tbPedido_idPedido, '.', item.idItem) as pretty_id, cupom.codigo, cupom.value, cupom.type, (ent.delivery_value + item.valorItem + item.descTotal) as valueNoDiscount, item.descTotal, ped.dataPedido, ped.statusPedido from tbcupom cupom join tbitem item on cupom.idDesc = item.tbcupom_idDesc join tbpedido ped on item.tbPedido_idPedido = ped.idPedido join tbentrega ent on item.idItem = ent.id_item where ped.statusPedido != ?";
        $params = [TbPedido::STATUS_CART];

        if (!$hasEmail) {
            $sql .= ' and cupom.commission_email = ?';
            $params[] = '';
        } elseif ($hasEmail == 1) {
            $sql .= ' and cupom.commission_email <> ?';
            $params[] = '';
        }

        if (!in_array($status, [
            self::COUPON_INACTIVE,
            self::COUPON_ACTIVE,
            self::COUPON_BOTH
        ])) {
            throw new \Exception('Status inválido');
        }

        if ($status != self::COUPON_BOTH) {
            $sql .= ' and cupom.status=? ';
            $params[] = $status;
        }

        if ($codeOrEmail) {
            $sql .= ' and concat(cupom.codigo, cupom.commission_email) like ?';
            $params[] = "%$codeOrEmail%";
        }

        $stmt = $this->conn->query($sql, $params);

        if ($stmt) {
            return $stmt->fetchAll();
        }

        throw new \Exception('Não foi possível consultar os cupons');
    }

    public function getAllInvitationCouponsByUser(int $userId)
    {
        $stmt = $this->conn->query(
            'select idDesc, codigo, startdate, validade, type, value from tbcupom where tbCliente_idCliente = ? and invite_coupon is not true', [
            $userId
        ]);

        if ($stmt) {
            return $stmt->fetchAll();
        }

        throw new \Exception('Não foi possível buscar os cupons');
    }

    public function getAllInvitationCoupons()
    {
        $stmt = $this->conn->query('select * from tbcupom where tbCliente_idCliente is not null and invite_coupon = 0');

        if ($stmt) {
            return $stmt->fetchAll();
        }

        throw new \Exception('Não foi possível buscar os cupons');
    }

    public function getUserIdByInviteCode($inviteCode)
    {
        $stmt = $this->conn->query('select tbCliente_idCliente from tbcupom where codigo = ?', [$inviteCode]);

        if ($stmt && $inviteCode = $stmt->fetch()['invite_code']) {
            return $inviteCode;
        }

        return false;
    }

    public function getById($idDesc)
    {
        $stmt = $this->conn->query('select * from tbcupom where idDesc = ?', [$idDesc]);

        if ($stmt && $coupon = $stmt->fetch()) {
            return $coupon;
        }

        return false;
    }
}
