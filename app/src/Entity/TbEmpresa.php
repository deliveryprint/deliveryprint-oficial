<?php

namespace IntecPhp\Entity;

use IntecPhp\Entity\AbstractEntity;

class TbEmpresa extends AbstractEntity
{
    protected $name = 'tbempresa';
    protected $id = 'idEmpresa';


    public function add($name, $phone, $email)
    {
        $stmt = $this->conn->query("insert into
        tbempresa(nome, tel, email) values(?, ?, ?)",
        [$name, $phone, $email]);

        if ($stmt) {
            return $this->conn->lastInsertId();
        }

        throw new \Exception("Não foi possível salvar a nova empresa.");
    }
}
