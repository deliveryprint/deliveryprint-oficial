<?php

namespace IntecPhp\Entity;

use IntecPhp\Entity\AbstractEntity;

class TbFrete extends AbstractEntity
{
    const CLASSNAME = 'tbfrete';
    protected $name = 'tbfrete';
    protected $id = 'idFrete';

    public function addFreight($idDelivery, $typeId, $name, $type, $deadline, $value)
    {
        $stmt = $this
            ->conn
            ->query("insert into tbfrete(id_entrega, nomeServico, type_name, type_id, prazo, valor)
                values(?, ?, ?, ?, ?, ?)", [$idDelivery, $name, $type, $typeId, $deadline, $value]);

        if ($stmt) {
            return $this->conn->lastInsertId();
        }

        throw new Exception("Não foi possível adicionar o frete");
    }

    public function firstOfDelivery($deliveryId)
    {
        $stmt = $this->conn->query('select * from tbfrete where id_entrega = ? limit 1', [$deliveryId]);

        if($stmt) {
            return $stmt->fetch();
        }

        throw new \Exception('Não foi possível encontrar o frete da entrega');
    }

}
