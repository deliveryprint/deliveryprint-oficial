<?php

namespace IntecPhp\Entity;

use IntecPhp\Model\DbHandler;

class TbCliente extends AbstractEntity
{
    protected $name = 'tbcliente';
    protected $id = 'idCliente';

    const FIELD_ID = 'id';
    const FIELD_CPF = 'cpf';
    const FIELD_EMAIL = 'email';

    public function exists($field, $type = self::FIELD_EMAIL)
    {
        $sql = "";
        switch ($type) {
            case self::FIELD_ID:
                $sql = 'select * from tbcliente where idCliente=?';
                break;
            case self::FIELD_EMAIL:
                $sql = 'select * from tbcliente where emailCliente=?';
                break;
            case self::FIELD_CPF:
                $sql = 'select * from tbcliente where cpfCliente=?';
                break;
            default:
                throw new \Exception('Tipo de campo inválido');
        }

        $stm = $this->conn->query($sql, [
            $field
        ]);

        if ($stm && $u = $stm->fetch()) {
            return $u;
        }

        return false;
    }

    public function existsAdm($email)
    {
        $stm = $this->conn->query("select * from tbcliente where emailCliente=? and is_adm=1", [
            $email
        ]);

        if ($stm && $u = $stm->fetch()) {
            return $u;
        }

        return false;
    }

    public function createIfNotExists($email, $password)
    {
        if ($this->exists($email, self::FIELD_EMAIL)) {
            throw new \Exception('Email já existe', 111);
        }

        $encrytedPass = $this->encryptPassword($password);
        $hash = sha1(uniqid());

        $stmt = $this->conn->query(
            "insert into tbcliente (emailCliente, senhaCliente, statusCliente, dtIngresso, hash_code, verificado) values (?, ?, 1, now(), ?, 1)",
            [$email, $encrytedPass, $hash]
        );

        if ($stmt) {
            return (int) $this->conn->lastInsertId();
        }

        return false;
    }

    public function getHash($userId)
    {
        $stmt = $this->conn->query('select hash_code from tbcliente where idCliente = ?', [$userId]);

        if ($stmt && $hash = $stmt->fetch()['hash_code']) {
            return $hash;
        }

        return false;
    }

    public function getInviteCode($userId)
    {
        $stmt = $this->conn->query('select inviteCode from tbcliente where idCliente = ?', [$userId]);

        if ($stmt && $inviteCode = $stmt->fetch()['inviteCode']) {
            return $inviteCode;
        }

        return false;
    }

    public function getByInviteCode($inviteCode)
    {
        $stmt = $this->conn->query('select idCliente from tbcliente where inviteCode = ?', [$inviteCode]);

        if ($stmt && $idCliente = $stmt->fetch()['idCliente']) {
            return $idCliente;
        }

        return false;
    }

    public function updateInviteCode($userId, $inviteCode)
    {
        $stmt = $this->conn->query('update tbcliente set inviteCode = ? where idCliente = ?', [$inviteCode, $userId]);

        if ($stmt) {
            return true;
        }

        throw new \Exception('Não foi possível salvar o código de convite');
    }

    public function hasOrders($userId)
    {
        $stmt = $this->conn->query(
            "select * from tbcliente as c join tbpedido as p on p.tbCliente_idCliente = c.idCliente where idCliente = ? and statusPedido != 'Carrinho'",
            [$userId]
        );

        if ($stmt && $stmt->fetch()) {
            return true;
        }

        return false;
    }

    public function isVerified($userId)
    {
        $stmt = $this->conn->query('select verificado from tbcliente where idCliente = ?', [$userId]);

        if ($stmt && $verified = $stmt->fetch()) {
            return $verified['verificado'] == 1 ? true : false;
        }

        return false;
    }

    public function getLastVerificationTry($userId)
    {
        $stmt = $this->conn->query('select lastVerificationTry from tbcliente where idCliente = ?', [$userId]);

        if ($stmt && $lastTry = $stmt->fetch()) {
            return $lastTry['lastVerificationTry'];
        }

        return false;
    }

    public function setLastVerificationTry($userId)
    {
        $stmt = $this->conn->query('update tbcliente set lastVerificationTry = now() where idCliente = ?', [$userId]);

        if ($stmt) {
            return true;
        }

        return false;
    }

    public function getCustomerIdFromHash($hash)
    {
        $stmt = $this->conn->query('select idCliente from tbcliente where hash_code = ?', [$hash]);

        if ($stmt && $res = $stmt->fetch()) {
            return $res['idCliente'];
        }

        throw new \Exception('Ocorreu um erro inesperado. Por favor entre em contato com a Delivery Print.');
    }

    public function verifyAccount($customerId)
    {
        $stmt = $this->conn->query('update tbcliente set verificado = 1 where idCliente = ?', [$customerId]);

        if ($stmt) {
            return $stmt->rowCount();
        }

        throw new \Exception('Ocorreu um erro ao tentar verificar o e-mail. Por favor entre em contato com a Delivery Print.');
    }

    public function changePassword($userId, $password)
    {
        $encryptedPass = self::encryptPassword($password);

        $stm = $this->conn->query('update tbcliente set senhaCliente=? where idCliente=?', [
            $encryptedPass, $userId
        ]);

        if ($stm) {
            return $stm->rowCount();
        }

        return false;
    }

    public function getPassword($userId)
    {
        $stm = $this->conn->query('select senhaCliente from tbcliente where idCliente = ?', [
            $userId
        ]);

        if ($stm) {
            return $stm->fetch()['senhaCliente'];
        }

        return false;
    }



    public function encryptPassword($password)
    {
        return password_hash($password, PASSWORD_BCRYPT, [
            'cost' => 12
        ]);
    }

    public function checkPassword($password, $encryptedPass)
    {
        return password_verify($password, $encryptedPass);
    }

    public function updateInfo($customerId, $name, $cpf, $tel, $cel, $email, $birth)
    {
        $stmt = $this->conn->query('update tbcliente set nomeCliente = ?, cpfCliente = ?, telCliente = ?, celCliente = ?, emailCliente = ?, dtNascimento = ? where idCliente = ?', [
            $name, $cpf, $tel, $cel, $email, $birth, $customerId
        ]);

        if ($stmt) {
            return true;
        }

        return false;
    }

    public function getAllInfoForPaymentById($id)
    {
        $stmt = $this->conn->query(
            "select cli.nomeCliente, cli.cpfCliente, cli.emailCliente, end.cep, end.numero, end.complemento, end.logradouro, end.bairro, end. cidade, end.uf from tbcliente cli
             join tbendereco end on cli.idCliente = end.idCliente
             where cli.idCliente = ? limit 1",
            [$id]
        );

        if ($stmt && $userInfo = $stmt->fetch()) {
            return $userInfo;
        }

        return false;
    }

    public function updateNameAndPhone($id, $name, $phone)
    {
        $stmt = $this->conn->query(
            "update tbcliente set nomeCliente = ?, telCliente = ? where idCliente = ?",
            [$name, $phone, $id]
        );

        if ($stmt) {
            return $stmt->rowCount();
        }

        throw new \Exception("Não foi possível atualizar as informações do usuário");
    }

    public function getAllAndLastOrderDate(string $startPeriod, string $endPeriod, string $filterType)
    {
        $sql = 'select c.nomeCliente, c.emailCliente, c.telCliente, c.dtIngresso, max(p.dataPedido) as dataPedido from tbcliente c left join tbpedido p on c.idCliente = p.tbCliente_idCliente';
        $params = [];

        if ($filterType) {
            if ($filterType == 'Pedido') {
                $column = 'date(dataPedido)';
            } else {
                $column = 'date(c.dtIngresso)';
            }

            if ($startPeriod && $endPeriod) {
                $sql .= " where $column >= ? and $column <= ?";
                $params = [$startPeriod, $endPeriod];
            } elseif ($startPeriod) {
                $sql .= " where $column >= ?";
                $params = [$startPeriod];
            } else {
                $sql .= " where $column <= ?";
                $params = [$endPeriod];
            }
        }

        $sql .= " group by c.emailCliente order by c.dtIngresso";

        $stmt = $this->conn->query($sql, $params);

        if ($stmt) {
            return $stmt->fetchAll();
        }

        throw new \Exception('Não foi possível consultar os usuários registrados');
    }
}
