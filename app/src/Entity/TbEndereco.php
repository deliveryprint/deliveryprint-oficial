<?php

namespace IntecPhp\Entity;

use IntecPhp\Entity\AbstractEntity;

class TbEndereco extends AbstractEntity
{
    protected $name = 'tbendereco';
    protected $id = 'idEndereco';

    public function getIdByUserIdAndZip($userId, $zip)
    {
        $stmt = $this->conn->query("select idEndereco as id from tbendereco where idCliente = ? and cep = ?", [$userId, $zip]);

        if ($stmt) {
            if ($endereco = $stmt->fetch()) {
                return $endereco['id'];
            }

            return false;
        }

        throw new \Exception("Não foi possível consultar o endereço do usuário");
    }

    public function getFromCustomer($customerId)
    {
        $stmt = $this->conn->query('select * from tbendereco where idCliente = ? order by idEndereco desc limit 3',
        [$customerId]);

        if ($stmt) {
            return $stmt->fetchAll();
        }

        return false;
    }

    public function update($addrId, $zip, $number, $street, $district, $city, $uf, $compl)
    {
        $stmt = $this->conn->query('update tbendereco set cep = ?, numero = ?, logradouro = ?, bairro = ?, cidade = ?, uf = ?, complemento = ? where idEndereco = ?', [
            $zip, $number, $street, $district, $city, $uf, $compl, $addrId
        ]);

        if ($stmt) {
            return true;
        }

        return false;
    }

    public function add($customerId, $zip, $number, $street, $district, $city, $uf, $compl)
    {
        $stmt = $this->conn->query(
            'insert into tbendereco(idCliente, cep, numero,
            logradouro, bairro, cidade, uf, complemento) values(?, ?, ?, ?, ?, ?, ?, ?)',
            [$customerId, $zip, $number, $street, $district, $city, $uf, $compl]
        );

        if ($stmt) {
            return $this->conn->lastInsertId();
        }

        throw new \Exception("Não foi possível adicionar as informações da entrega.");
    }

    public function countNumberUserAddr($idUser)
    {
        $stmt = $this->conn->query("select count(*) as total from tbendereco where idCliente = ?", [
            $idUser
        ]);

        if ($stmt && $num = $stmt->fetch()) {
            return $num['total'];
        }

        return false;
    }

    public function userAddrAlreadyExists($idUser, $idAddress)
    {
        $stmt = $this->conn->query("select * from tbendereco where idEndereco = ? and idCliente = ?", [
            $idAddress, $idUser
        ]);

        if ($stmt) {
            if ($stmt->rowCount()) {
                return true;
            }
            return 0;
        }

        return false;
    }

    public function getById($id)
    {
        $stmt = $this->conn->query("select * from tbendereco where idEndereco = ?", [
            $id
        ]);

        if ($stmt) {
            return $stmt->fetch();
        }

        throw new \Exception("Não foi possível obter as informações do endereço de entrega do pedido");
    }

    public function updateUserId($idUser, $idAddress)
    {
        $stmt = $this->conn->query('update tbendereco set idCliente = ? where idEndereco = ?', [
            $idUser, $idAddress
        ]);

        if ($stmt) {
            return $stmt->rowCount();
        }

        throw new \Exception("Não foi possível atualizar as informações de endereço do usuário");
    }

    public function updateNumberAndComplement($id, $number, $complement)
    {
        $stmt = $this->conn->query('update tbendereco set numero = ?, complemento = ? where idEndereco = ?', [
            $number, $complement, $id
        ]);

        if ($stmt) {
            return $stmt->rowCount();
        }

        throw new \Exception("Não foi possível atualizar as informações de endereço do usuário");
    }

    public function searchByUserIdAndCompleteAddr($userId, $zip, $number, $complement)
    {
        $stmt = $this->conn->query('select idEndereco as id from tbendereco where idCliente = ? and cep = ? and numero = ? and complemento = ?', [
            $userId, $zip, $number, $complement
        ]);

        if ($stmt) {
            $address = $stmt->fetch();
            if ($address) {
                return $address['id'];
            } else {
                return false;
            }
        }

        throw new \Exception("Não foi possível consultar as informações do endereço de entrega");
    }
}
