<?php

namespace IntecPhp\Entity;

use IntecPhp\Entity\AbstractEntity;

class TbItem extends AbstractEntity
{
    const CLASSNAME = 'tbitem';
    protected $name = 'tbitem';
    protected $id = 'idItem';

    const HORIZONTAL_BORDER = 'Horizontal';
    const VERTICAL_BORDER   = 'Vertical';
    const STATUS_CART       = 'Carrinho';

    public function add(
        $color,
        $side,
        $finishing,
        $paper,
        $order,
        $value,
        $status,
        $totDiscount,
        $bindTotal,
        $bindBorder,
        $cupomDisc,
        $cupomGeneralDisc,
        $idEmailMkt,
        $obs,
        $deliveryFallback = NULL,
        $idDetalhes = NULL
    ) {

        $stmt = $this->conn->query(
            "insert into tbitem(tbCor_idCor, tbLado_idLado, tbAcabamento_idAcabamento, tbPapel_idPapel,
            tbPedido_idPedido, valorItem, statusItem, descTotal, qdeAcabamentos, borda, tbcupom_idDesc, tbcupomGeral_idDescGeral,
            tbEmailMkt_idEmailMkt, obsCliente, deliveryFallback, tbDetalhesAcabamento_idDetalhes) values(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)",
            [
                $color, $side, $finishing, $paper, $order, $value, $status, $totDiscount, $bindTotal, $bindBorder,
                $cupomDisc, $cupomGeneralDisc, $idEmailMkt, $obs, $deliveryFallback ? 1 : 0, $idDetalhes
            ]
        );

        // var_dump([$color, $side, $finishing, $paper, $order, $value, $status, $totDiscount,
        // $bindTotal, $bindBorder, $cupomDisc, $cupomGeneralDisc, $idEmailMkt, $obs]);

        if ($stmt) {
            return (int) $this->conn->lastInsertId();
        }

        throw new \Exception('Não foi possível salvar o item');
    }

    public function getByOrderId($orderId)
    {
        $stmt = $this->conn->query(
            'SELECT
                idItem as id, descricaoCor as cor,
                tl.descricaoLado as lado,
                tp.descricaoPapel as papel, ti.tbcupom_idDesc as cupom, ti.tbcupomGeral_idDescGeral as cupomGeral, ta.descricaoAcabamento as acabamento,
                ti.valorItem, ti.descTotal, obsCliente, borda as border, tbAcabamento_idAcabamento,
                concat(tbPedido_idPedido, \'.\', idItem) as pretty_id
            FROM tbitem ti JOIN tbcor tc ON ti.tbCor_idCor = tc.idCor
            JOIN tblado tl ON ti.tbLado_idLado = tl.idLado
            JOIN tbpapel tp ON ti.tbPapel_idPapel = tp.idPapel
            JOIN tbacabamento ta ON ti.tbAcabamento_idAcabamento = ta.idAcabamento
            WHERE tbPedido_idPedido=? GROUP BY ti.idItem',
            [$orderId]
        );

        if ($stmt) {
            return $stmt->fetchAll();
        }

        return false;
    }

    public function getWithDeliveryValue($itemId)
    {
        $stmt = $this->conn->query("select i.*, e.delivery_value from
            $this->name i join tbentrega e on i.idItem=e.id_item
            where $this->id = ?", [$itemId]);

        if ($stmt) {
            return $stmt->fetch();
        }

        throw new \Exception('Falha na consulta');
    }

    public function deleteItem($idItem)
    {
        $stmt = $this->conn->query(
            'delete from tbitem where idItem = ?',
            [$idItem]
        );

        if ($stmt) {
            return $stmt->rowCount();
        }

        return false;
    }

    public function count($orderId)
    {
        $stmt = $this->conn->query(
            'SELECT count(*) AS number
            FROM tbitem
            WHERE tbPedido_idPedido=? group by tbPedido_idPedido',
            [$orderId]
        );

        if ($stmt) {
            return $stmt->fetch()["number"];
        }

        return 0;
    }

    public function getFilesById($id)
    {
        $stmt = $this->conn->query(
            'SELECT ti.*, ta.*, concat(tbPedido_idPedido, \'.\', idItem) as pretty_id
            FROM tbitem ti JOIN tbarquivo ta ON ta.tbItem_idItem=ti.idItem
            WHERE ti.idItem=?',
            [$id]
        );

        if ($stmt) {
            return $stmt->fetchAll();
        }

        return 0;
    }

    public function updateAllByOrderId($oldOrderId, $newOrderId)
    {
        $stmt = $this->conn->query(
            'update tbitem set tbPedido_idPedido = ? where tbPedido_idPedido = ?',
            [$newOrderId, $oldOrderId]
        );

        if ($stmt) {
            return $stmt->rowCount();
        }

        return false;
    }

    public function updateUserObservation($id, $observation)
    {
        $stmt = $this->conn->query(
            "update tbitem set obsCliente = ?, obsMomentoSalvamento = now() where idItem = ?",
            [$observation, $id]
        );

        if ($stmt) {
            return true;
        }

        return false;
    }

    public function getAllItemsIdByOrderId($idOrder)
    {
        $stmt = $this->conn->query("select idItem from tbitem where tbPedido_idPedido = ?", [$idOrder]);

        if ($stmt) {
            return $stmt->fetchAll();
        }

        throw new \Exception('Não foi possível consultar os itens');
    }

    public function getLastByOrderId($idOrder)
    {
        $stmt = $this->conn->query(
            "select *from tbitem where tbPedido_idPedido = ? order by idItem desc limit 1",
            [$idOrder]
        );

        if ($stmt && $item = $stmt->fetch()) {
            return $item;
        }

        return false;
    }

    public function getItemZipByOrder($orderId)
    {
        $stmt = $this->conn->query('select e.idEndereco, e.cep from tbitem i
            join tbentrega d on i.idItem = d.id_item
            join tbendereco e on e.idEndereco = d.tbEndereco_idEndereco
            where i.tbPedido_idPedido = ? and d.tbRetirada_idRetirada is null limit 1', [$orderId]);

        if ($stmt) {
            return $stmt->fetch();
        }

        throw new \Exception('Não foi possível consultar o CEP');
    }

    public function isOfOrder($orderId, $itemId)
    {
        $stmt = $this->conn->query('select * from tbitem where tbPedido_idPedido = ? and idItem = ?', [
            $orderId, $itemId
        ]);

        if ($stmt) {
            return $stmt->rowCount();
        }

        return false;
    }

    public function calcTotalValueByOrder($orderId)
    {
        $stmt = $this->conn->query('select sum(i.valorItem + e.delivery_value) as order_value from tbitem i
            join tbentrega e on e.id_item = i.idItem where tbPedido_idPedido = ?', [$orderId]);

        if ($stmt) {
            if ($res = $stmt->fetch()) {
                return $res['order_value'];
            }
            return 0;
        }

        throw new \Exception('Não foi possível calcular o preço do pedido');
    }

    public function calcOrderAndDeliveryValueByOrder(int $orderId)
    {
        $stmt = $this->conn->query('select sum(i.valorItem) as order_value, sum(e.delivery_value) as delivery_value from tbitem i
            join tbentrega e on e.id_item = i.idItem where tbPedido_idPedido = ?', [$orderId]);

        if ($stmt) {
            return $stmt->fetch();
        }

        throw new \Exception('Não foi possível calcular o preço do pedido');
    }

    public function countItems($orderId)
    {
        $stmt = $this->conn->query('select count(item.idItem) as total from tbpedido ped
                                    join tbitem item on ped.idPedido = item.tbPedido_idPedido
                                    where ped.idPedido = ?', [$orderId]);

        if ($stmt) {
            if ($res = $stmt->fetch()) {
                return $res['total'];
            }
            return 0;
        }

        throw new \Exception('Não foi possível contabilizar o número de itens do pedido');
    }

    public function getDetails($itemId)
    {
        $stmt = $this->conn->query('select i.*, e.*, a.*, w.*, o.*, da.*,
        c.idCliente, c.nomeCliente, c.telCliente, c.celCliente, c.emailCliente, c.dtIngresso, c.dtNascimento, c.cpfCliente, c.is_adm,
        tc.descricaoCor, tl.descricaoLado, tp.descricaoPapel, ta.descricaoAcabamento, (i.valorItem + e.delivery_value) as valor_final from tbitem i
        join tbpedido o on o.idPedido = i.tbPedido_idPedido
        join tbcliente c on c.idCliente = o.tbCliente_idCliente
        join tbentrega e on e.id_item = i.idItem
        left join tbendereco a on a.idEndereco = e.tbEndereco_idEndereco
        left join tbretirada w on w.idRetirada = e.tbRetirada_idRetirada
        left join tbdetalhesacabamento da on i.tbDetalhesAcabamento_idDetalhes = da.idDetalhes
        JOIN tbcor tc ON i.tbCor_idCor = tc.idCor
        JOIN tblado tl ON i.tbLado_idLado = tl.idLado
        JOIN tbpapel tp ON i.tbPapel_idPapel = tp.idPapel
        JOIN tbacabamento ta ON i.tbAcabamento_idAcabamento = ta.idAcabamento
        where idItem = ?', [$itemId]);

        if ($stmt) {
            return $stmt->fetch();
        }

        throw new \Exception('Não foi possível carregar os detalhes do item');
    }

    public function findNotInCart($prettyId, $nameOrEmail, $paymentStatus, $itemStatus, $orderPeriod)
    {
        $params = [];

        $sql = 'select i.*, o.*, MAX(c.nomeCliente) as nomeCliente, MAX(c.emailCliente) as emailCliente, concat(o.idPedido, \'.\', i.idItem) as pretty_id, sum(f.numPaginas * f.qtdImpressoes) as num_folhas
        from tbitem i join tbpedido o on o.idPedido = i.tbPedido_idPedido
        join tbcliente c on c.idCliente = o.tbCliente_idCliente join tbarquivo f on f.tbItem_idItem = i.idItem where';

        if ($prettyId) {
            $sql .= ' concat(o.idPedido, \'.\', i.idItem) like ? and ';
            $params[] = "%$prettyId%";
        }

        if ($nameOrEmail) {
            $sql .= ' concat(nomeCliente, \' \', emailCliente) like ? and ';
            $params[] = "%$nameOrEmail%";
        }

        if ($paymentStatus) {
            $arr = implode(', ', array_fill(0, count($paymentStatus), '?'));
            $sql .= " o.statusPagamento in ($arr) and ";
            $params = array_merge($params, $paymentStatus);
        }

        if ($itemStatus) {
            $arr = implode(', ', array_fill(0, count($itemStatus), '?'));
            $sql .= " i.statusItem in ($arr) and ";
            $params = array_merge($params, $itemStatus);
        }

        if ($orderPeriod) {
            if ($orderPeriod['from'] && $orderPeriod['to']) {
                $sql .= " o.dataPedido between ? and ? and ";
                $params[] = $orderPeriod['from'];
                $params[] = $orderPeriod['to'];
            } elseif ($orderPeriod['from']) {
                $sql .= " o.dataPedido >= ? and";
                $params[] = $orderPeriod['from'];
            } elseif ($orderPeriod['to']) {
                $sql .= " o.dataPedido <= ? and ";
                $params[] = $orderPeriod['to'];
            }
        }

        $sql .= ' i.statusItem <> ? group by i.idItem order by o.idPedido desc';

        $params[] = self::STATUS_CART;
        $stmt = $this->conn->query($sql, $params);

        if ($stmt) {
            return $stmt->fetchAll();
        }

        throw new \Exception('Não foi possível carregar os itens');
    }

    public function updateAdmObservation($id, $observation)
    {
        $stmt = $this->conn->query(
            "update tbitem set obsAdmin = ? where idItem = ?",
            [$observation, $id]
        );

        if ($stmt) {
            return $stmt->rowCount();
        }

        throw new \Exception('Não foi possível atualizar a observação do item!');
    }

    public function getDeliveryInfo($id)
    {
        $stmt = $this->conn->query('select e.*, r.prazo as prazo_retirada,
        r.type_name as tipo_retirada, r.type_id as rtype_id,
        fr.prazo as prazo_frete, fr.nomeServico as tipo_frete, fr.type_id as frtype_id
        from tbentrega e
        left join tbretirada r on r.idRetirada = e.tbRetirada_idRetirada
        left join tbfrete fr on fr.id_entrega = e.idEntrega
        where e.id_item = ? limit 1', [$id]);

        if ($stmt) {
            return $stmt->fetch();
        }

        throw new \Exception('Não foi possível buscar informações de entrega dos itens');
    }

    public function updateStatus($id, $status)
    {
        $stmt = $this->conn->query(
            "update tbitem set statusItem = ? where idItem = ?",
            [$status, $id]
        );

        if ($stmt) {
            return $stmt->rowCount();
        }

        throw new \Exception('Não foi possível atualizar o status do item!');
    }

    public function updateStatusItemsOfOrder($orderId, $status)
    {
        $stmt = $this->conn->query(
            "update tbitem set statusItem = ? where tbPedido_idPedido = ?",
            [$status, $orderId]
        );

        if ($stmt) {
            return $stmt->rowCount();
        }

        throw new \Exception('Não foi possível atualizar o status dos itens do pedido');
    }

    public function getFileByHash($hash)
    {
        $stmt = $this->conn->query('select * from tbarquivo where arquivo=?', [$hash]);

        if ($stmt) {
            return $stmt->fetch();
        }

        throw new \Exception('Não foi possível buscar o arquivo');
    }

    public function isAcomplished($status)
    {
        return $status === TbPedido::STATUS_ACCOMPLISHED;
    }

    public function getCommissionDataByOrderId($orderId)
    {
        $stmt = $this->conn->query(
            'select (i.valorItem * c.commission_percent / 100) as commission, c.commission_email, c.codigo from tbitem i join tbcupom c on i.tbcupom_idDesc = c.idDesc where i.tbPedido_idPedido = ?',
            [$orderId]
        );

        if ($stmt) {
            return $stmt->fetchAll();
        }

        throw new \Exception('Não foi possível buscar a comissão.');
    }

    public function getAllByOrderId(int $orderId)
    {
        $stmt = $this->conn->query(
            "SELECT item.*
        FROM tbpedido ped
        JOIN tbitem item ON ped.idPedido = item.tbPedido_idPedido
        WHERE ped.idPedido = ?",
            [$orderId]
        );

        if ($stmt) {
            return $stmt->fetchAll();
        }

        throw new \Exception('Não foi possível buscar os itens do pedido');
    }

    public function getWithDeliveryInfo(int $id)
    {
        $stmt = $this->conn->query(
            "SELECT item.*, ent.tbRetirada_idRetirada
        FROM tbitem item
        JOIN tbentrega ent ON item.idItem = ent.id_item
        WHERE item.idItem = ?",
            [$id]
        );

        if ($stmt) {
            return $stmt->fetch();
        }

        throw new \Exception('Não foi possível buscar o item');
    }

    public function updateTrackingCode(int $id, string $trackingCode)
    {
        $stmt = $this->conn->query(
            "UPDATE $this->name SET tracking_code = ?
        WHERE idItem = ?",
            [$trackingCode, $id]
        );

        if ($stmt) {
            return $stmt->rowCount();
        }

        throw new \Exception('Não foi possível atualizar o código de rastreio do item');
    }

    public function updateTrackingStatusByOrderId(int $orderId, bool $status)
    {
        $stmt = $this->conn->query(
            "UPDATE tbpedido SET is_tracking = ?
        WHERE idPedido = ?",
            [(int) $status, $orderId]
        );

        if ($stmt) {
            return $stmt->rowCount();
        }

        throw new \Exception('Não foi possível buscar o item');
    }
}
