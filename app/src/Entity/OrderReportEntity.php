<?php

namespace IntecPhp\Entity;

use IntecPhp\Entity\AbstractEntity;

class OrderReportEntity extends AbstractEntity
{
    public function countColorsTypePaid(string $startPeriod, string $endPeriod)
    {
        $sql = "select
        sum(i1.count_pb_sulfite_a4_75g) as pb_sulfite_a4_75g,
        sum(i2.count_c_sulfite_a4_75g) as c_sulfite_a4_75g,
        sum(i3.count_pb_sulfite_a4_90g) as pb_sulfite_a4_90g,
        sum(i4.count_c_sulfite_a4_90g) as c_sulfite_a4_90g,
        sum(i5.count_pb_couche_a4_115g) as pb_couche_a4_115g,
        sum(i6.count_c_couche_a4_115g) as c_couche_a4_115g,
        sum(i7.count_pb_couche_a4_170g) as pb_couche_a4_170g,
        sum(i8.count_c_couche_a4_170g) as c_couche_a4_170g,
        sum(i9.count_pb_oficio_75g) as pb_oficio_75g,
        sum(i10.count_c_oficio_75g) as c_oficio_75g,
        sum(i11.count_pb_eco_a4_75g) as pb_eco_a4_75g,
        sum(i12.count_c_eco_a4_75g) as c_eco_a4_75g,
        sum(i13.count_pb_carta_75g) as pb_carta_75g,
        sum(i14.count_c_carta_75g) as c_carta_75g,
        sum(i15.count_pb_sulfite_a3_75g) as pb_sulfite_a3_75g,
        sum(i16.count_c_sulfite_a3_75g) as c_sulfite_a3_75g,
        sum(i17.count_pb_couche_a3_150g) as pb_couche_a3_150g,
        sum(i18.count_c_couche_a3_150g) as c_couche_a3_150g,
        sum(i19.count_pb_sulfite_a5_75g) as pb_sulfite_a5_75g,
        sum(i20.count_c_sulfite_a5_75g) as c_sulfite_a5_75g
        from tbpedido ped
        left join (select tbPedido_idPedido, sum(numPaginas * qtdImpressoes) as count_pb_sulfite_a4_75g from tbitem join tbarquivo on idItem = tbItem_idItem where tbCor_idCor = 1 and tbPapel_idPapel = 1 group by tbPedido_idPedido) as i1 on i1.tbPedido_idPedido = ped.idPedido
        left join (select tbPedido_idPedido, sum(numPaginas * qtdImpressoes) as count_c_sulfite_a4_75g from tbitem join tbarquivo on idItem = tbItem_idItem where tbCor_idCor = 2 and tbPapel_idPapel = 1 group by tbPedido_idPedido) as i2 on i2.tbPedido_idPedido = ped.idPedido
        left join (select tbPedido_idPedido, sum(numPaginas * qtdImpressoes) as count_pb_sulfite_a4_90g from tbitem join tbarquivo on idItem = tbItem_idItem where tbCor_idCor = 1 and tbPapel_idPapel = 2 group by tbPedido_idPedido) as i3 on i3.tbPedido_idPedido = ped.idPedido
        left join (select tbPedido_idPedido, sum(numPaginas * qtdImpressoes) as count_c_sulfite_a4_90g from tbitem join tbarquivo on idItem = tbItem_idItem where tbCor_idCor = 2 and tbPapel_idPapel = 2 group by tbPedido_idPedido) as i4 on i4.tbPedido_idPedido = ped.idPedido
        left join (select tbPedido_idPedido, sum(numPaginas * qtdImpressoes) as count_pb_couche_a4_115g from tbitem join tbarquivo on idItem = tbItem_idItem where tbCor_idCor = 1 and tbPapel_idPapel = 3 group by tbPedido_idPedido) as i5 on i5.tbPedido_idPedido = ped.idPedido
        left join (select tbPedido_idPedido, sum(numPaginas * qtdImpressoes) as count_c_couche_a4_115g from tbitem join tbarquivo on idItem = tbItem_idItem where tbCor_idCor = 2 and tbPapel_idPapel = 3 group by tbPedido_idPedido) as i6 on i6.tbPedido_idPedido = ped.idPedido
        left join (select tbPedido_idPedido, sum(numPaginas * qtdImpressoes) as count_pb_couche_a4_170g from tbitem join tbarquivo on idItem = tbItem_idItem where tbCor_idCor = 1 and tbPapel_idPapel = 4 group by tbPedido_idPedido) as i7 on i7.tbPedido_idPedido = ped.idPedido
        left join (select tbPedido_idPedido, sum(numPaginas * qtdImpressoes) as count_c_couche_a4_170g from tbitem join tbarquivo on idItem = tbItem_idItem where tbCor_idCor = 2 and tbPapel_idPapel = 4 group by tbPedido_idPedido) as i8 on i8.tbPedido_idPedido = ped.idPedido
        left join (select tbPedido_idPedido, sum(numPaginas * qtdImpressoes) as count_pb_oficio_75g from tbitem join tbarquivo on idItem = tbItem_idItem where tbCor_idCor = 1 and tbPapel_idPapel = 5 group by tbPedido_idPedido) as i9 on i9.tbPedido_idPedido = ped.idPedido
        left join (select tbPedido_idPedido, sum(numPaginas * qtdImpressoes) as count_c_oficio_75g from tbitem join tbarquivo on idItem = tbItem_idItem where tbCor_idCor = 2 and tbPapel_idPapel = 5 group by tbPedido_idPedido) as i10 on i10.tbPedido_idPedido = ped.idPedido
        left join (select tbPedido_idPedido, sum(numPaginas * qtdImpressoes) as count_pb_eco_a4_75g from tbitem join tbarquivo on idItem = tbItem_idItem where tbCor_idCor = 1 and tbPapel_idPapel = 6 group by tbPedido_idPedido) as i11 on i11.tbPedido_idPedido = ped.idPedido
        left join (select tbPedido_idPedido, sum(numPaginas * qtdImpressoes) as count_c_eco_a4_75g from tbitem join tbarquivo on idItem = tbItem_idItem where tbCor_idCor = 2 and tbPapel_idPapel = 6 group by tbPedido_idPedido) as i12 on i12.tbPedido_idPedido = ped.idPedido
        left join (select tbPedido_idPedido, sum(numPaginas * qtdImpressoes) as count_pb_carta_75g from tbitem join tbarquivo on idItem = tbItem_idItem where tbCor_idCor = 1 and tbPapel_idPapel = 7 group by tbPedido_idPedido) as i13 on i13.tbPedido_idPedido = ped.idPedido
        left join (select tbPedido_idPedido, sum(numPaginas * qtdImpressoes) as count_c_carta_75g from tbitem join tbarquivo on idItem = tbItem_idItem where tbCor_idCor = 2 and tbPapel_idPapel = 7 group by tbPedido_idPedido) as i14 on i14.tbPedido_idPedido = ped.idPedido
        left join (select tbPedido_idPedido, sum(numPaginas * qtdImpressoes) as count_pb_sulfite_a3_75g from tbitem join tbarquivo on idItem = tbItem_idItem where tbCor_idCor = 1 and tbPapel_idPapel = 8 group by tbPedido_idPedido) as i15 on i15.tbPedido_idPedido = ped.idPedido
        left join (select tbPedido_idPedido, sum(numPaginas * qtdImpressoes) as count_c_sulfite_a3_75g from tbitem join tbarquivo on idItem = tbItem_idItem where tbCor_idCor = 2 and tbPapel_idPapel = 8 group by tbPedido_idPedido) as i16 on i16.tbPedido_idPedido = ped.idPedido
        left join (select tbPedido_idPedido, sum(numPaginas * qtdImpressoes) as count_pb_couche_a3_150g from tbitem join tbarquivo on idItem = tbItem_idItem where tbCor_idCor = 1 and tbPapel_idPapel = 9 group by tbPedido_idPedido) as i17 on i17.tbPedido_idPedido = ped.idPedido
        left join (select tbPedido_idPedido, sum(numPaginas * qtdImpressoes) as count_c_couche_a3_150g from tbitem join tbarquivo on idItem = tbItem_idItem where tbCor_idCor = 2 and tbPapel_idPapel = 9 group by tbPedido_idPedido) as i18 on i18.tbPedido_idPedido = ped.idPedido
        left join (select tbPedido_idPedido, sum(numPaginas * qtdImpressoes) as count_pb_sulfite_a5_75g from tbitem join tbarquivo on idItem = tbItem_idItem where tbCor_idCor = 1 and tbPapel_idPapel = 10 group by tbPedido_idPedido) as i19 on i19.tbPedido_idPedido = ped.idPedido
        left join (select tbPedido_idPedido, sum(numPaginas * qtdImpressoes) as count_c_sulfite_a5_75g from tbitem join tbarquivo on idItem = tbItem_idItem where tbCor_idCor = 2 and tbPapel_idPapel = 10 group by tbPedido_idPedido) as i20 on i20.tbPedido_idPedido = ped.idPedido
        where ped.statusPagamento = ?";
        $params = [TbPedido::STATUS_IUGU_PAID];

        if ($startPeriod && $endPeriod) {
            $sql .= " and date(ped.dataPedido) >= ? and date(ped.dataPedido) <= ?";
            $params[] = $startPeriod;
            $params[] = $endPeriod;
        } elseif ($startPeriod) {
            $sql .= " and date(ped.dataPedido) >= ?";
            $params[] = $startPeriod;
        } elseif ($endPeriod) {
            $sql .= " and date(ped.dataPedido) <= ?";
            $params[] = $endPeriod;
        }

        $sql .= " group by ped.statusPagamento";
        $stmt = $this->conn->query($sql, $params);

        if ($stmt) {
            return $stmt->fetch();
        }

        throw new \Exception('Não foi possível buscar informações dos pedidos');
    }

    public function countFinishesTypePaid(string $startPeriod, string $endPeriod)
    {
        $sql = "select
        sum(count_solta) as solta,
        sum(count_grampeado) as grampeado,
        sum(count_espiral) as espiral,
        sum(count_capa_dura) as capa_dura,
        sum(count_capa_dura_a3) as capa_dura_a3,
        sum(count_wire_o) as wire_o
        from tbpedido ped
        left join (select tbPedido_idPedido, sum(qdeAcabamentos) as count_solta from tbitem where tbAcabamento_idAcabamento = 1 group by tbPedido_idPedido) as a1 on a1.tbPedido_idPedido = ped.idPedido
        left join (select tbPedido_idPedido, sum(qdeAcabamentos) as count_grampeado from tbitem where tbAcabamento_idAcabamento = 2 group by tbPedido_idPedido) as a2 on a2.tbPedido_idPedido = ped.idPedido
        left join (select tbPedido_idPedido, sum(qdeAcabamentos) as count_espiral from tbitem where tbAcabamento_idAcabamento = 3 group by tbPedido_idPedido) as a3 on a3.tbPedido_idPedido = ped.idPedido
        left join (select tbPedido_idPedido, sum(qdeAcabamentos) as count_capa_dura from tbitem where tbAcabamento_idAcabamento = 4 group by tbPedido_idPedido) as a4 on a4.tbPedido_idPedido = ped.idPedido
        left join (select tbPedido_idPedido, sum(qdeAcabamentos) as count_capa_dura_a3 from tbitem where tbAcabamento_idAcabamento = 5 group by tbPedido_idPedido) as a5 on a5.tbPedido_idPedido = ped.idPedido
        left join (select tbPedido_idPedido, sum(qdeAcabamentos) as count_wire_o from tbitem where tbAcabamento_idAcabamento = 6 group by tbPedido_idPedido) as a6 on a6.tbPedido_idPedido = ped.idPedido
        where ped.statusPagamento = ?";
        $params = [TbPedido::STATUS_IUGU_PAID];

        if ($startPeriod && $endPeriod) {
            $sql .= " and date(ped.dataPedido) >= ? and date(ped.dataPedido) <= ?";
            $params[] = $startPeriod;
            $params[] = $endPeriod;
        } elseif ($startPeriod) {
            $sql .= " and date(ped.dataPedido) >= ?";
            $params[] = $startPeriod;
        } elseif ($endPeriod) {
            $sql .= " and date(ped.dataPedido) <= ?";
            $params[] = $endPeriod;
        }

        $sql .= " group by ped.statusPagamento";
        $stmt = $this->conn->query($sql, $params);

        if ($stmt) {
            return $stmt->fetch();
        }

        throw new \Exception('Não foi possível buscar informações dos pedidos');
    }

    public function countAveragePagesPaid(string $startPeriod, string $endPeriod)
    {
        $subSql = "select distinct date_format(ped.dataPedido, '%m-%d') as day, sum(sum_overall) as sum_overall, sum(sum_black_white) as sum_black_white, sum(sum_colorful) as sum_colorful
        from tbpedido ped
        left join (select sum(numPaginas * qtdImpressoes) as sum_overall, tbPedido_idPedido from tbitem join tbarquivo on idItem = tbItem_idItem group by tbPedido_idPedido) as g on ped.idPedido = g.tbPedido_idPedido
        left join (select sum(numPaginas * qtdImpressoes) as sum_black_white, tbPedido_idPedido from tbitem join tbarquivo on idItem = tbItem_idItem where tbCor_idCor = 1 group by tbPedido_idPedido) as pb on ped.idPedido = pb.tbPedido_idPedido
        left join (select sum(numPaginas * qtdImpressoes) as sum_colorful, tbPedido_idPedido from tbitem join tbarquivo on idItem = tbItem_idItem where tbCor_idCor = 2 group by tbPedido_idPedido) as c on ped.idPedido = c.tbPedido_idPedido
        where ped.statusPagamento = ?";

        $params = [TbPedido::STATUS_IUGU_PAID];

        if ($startPeriod && $endPeriod) {
            $subSql .= " and date(ped.dataPedido) >= ? and date(ped.dataPedido) <= ?";
            $params[] = $startPeriod;
            $params[] = $endPeriod;
        } elseif ($startPeriod) {
            $subSql .= " and date(ped.dataPedido) >= ?";
            $params[] = $startPeriod;
        } elseif ($endPeriod) {
            $subSql .= " and date(ped.dataPedido) <= ?";
            $params[] = $endPeriod;
        }

        $subSql .= " group by day";

        $sql = "select avg(sum_overall) as average_overall, avg(sum_black_white) as average_black_white, avg(sum_colorful) as average_colorful from ($subSql) as p2";

        $stmt = $this->conn->query($sql, $params);

        if ($stmt) {
            return $stmt->fetch();
        }

        throw new \Exception('Não foi possível buscar informações dos pedidos');
    }

    public function calcPaidValue(string $startPeriod, string $endPeriod)
    {
        $sql = "select sum(item.valorItem + ent.delivery_value) as total_paid
        from tbpedido ped
        join tbitem item on ped.idPedido = item.tbPedido_idPedido
        join tbentrega ent on item.idItem = ent.id_item
        where ped.statusPagamento = ?";
        $params = [TbPedido::STATUS_IUGU_PAID];

        if ($startPeriod && $endPeriod) {
            $sql .= " and date(ped.dataPedido) >= ? and date(ped.dataPedido) <= ?";
            $params[] = $startPeriod;
            $params[] = $endPeriod;
        } elseif ($startPeriod) {
            $sql .= " and date(ped.dataPedido) >= ?";
            $params[] = $startPeriod;
        } elseif ($endPeriod) {
            $sql .= " and date(ped.dataPedido) <= ?";
            $params[] = $endPeriod;
        }

        $sql .= " group by ped.statusPagamento";

        $stmt = $this->conn->query($sql, $params);

        if ($stmt) {
            return $stmt->fetch();
        }

        throw new \Exception('Não foi possível buscar informações dos pedidos');
    }

    public function calcCanceledValue(string $startPeriod, string $endPeriod)
    {
        $sql = "select sum(item.valorItem + ent.delivery_value) as total_canceled
        from tbpedido ped
        join tbitem item on ped.idPedido = item.tbPedido_idPedido
        join tbentrega ent on item.idItem = ent.id_item
        where ped.statusPedido = ?";
        $params = [TbPedido::STATUS_CANCELED];

        if ($startPeriod && $endPeriod) {
            $sql .= " and date(ped.dataPedido) >= ? and date(ped.dataPedido) <= ?";
            $params[] = $startPeriod;
            $params[] = $endPeriod;
        } elseif ($startPeriod) {
            $sql .= " and date(ped.dataPedido) >= ?";
            $params[] = $startPeriod;
        } elseif ($endPeriod) {
            $sql .= " and date(ped.dataPedido) <= ?";
            $params[] = $endPeriod;
        }

        $sql .= " group by ped.statusPedido";

        $stmt = $this->conn->query($sql, $params);

        if ($stmt) {
            return $stmt->fetch();
        }

        throw new \Exception('Não foi possível buscar informações dos pedidos');
    }

    public function calculateItemsCost(string $startPeriod, string $endPeriod)
    {
        $sql = "select ped.idPedido, i.idItem, (total_pages * print_value + total_finish) as item_cost, total_pages, i.delivery_value, delivery_id, withdrawal_value, withdrawal_id, ped.dataPedido
        from tbpedido ped
        join
        (select item.idItem, item.tbPedido_idPedido, total_pages, (acab.valor * item.qdeAcabamentos) as total_finish, imp.valor as print_value, ent.delivery_value, ret.valor as withdrawal_value, frete.type_id as delivery_id, ret.type_id as withdrawal_id
            from tbitem item
            join tbentrega ent on item.idItem = ent.id_item
            left join tbfrete frete on ent.idEntrega = frete.id_entrega
            left join tbretirada ret on ent.tbRetirada_idRetirada = ret.idRetirada
            join custo_acabamento acab on item.tbAcabamento_idAcabamento = acab.tbAcabamento_idAcabamento
            join custo_impressao imp on item.tbPapel_idPapel = imp.tbPapel_idPapel and item.tbCor_idCor = imp.tbCor_idCor and item.tbLado_idLado = imp.tbLado_idLado
            join (select sum(arq.numPaginas * arq.qtdImpressoes) as total_pages, arq.tbItem_idItem from tbarquivo arq group by arq.tbItem_idItem) as a on item.idItem = a.tbItem_idItem) as i on ped.idPedido = i.tbPedido_idPedido
        where ped.statusPagamento = ?";
        $params = [TbPedido::STATUS_IUGU_PAID];

        if ($startPeriod && $endPeriod) {
            $sql .= " and date(ped.dataPedido) >= ? and date(ped.dataPedido) <= ?";
            $params[] = $startPeriod;
            $params[] = $endPeriod;
        } elseif ($startPeriod) {
            $sql .= " and date(ped.dataPedido) >= ?";
            $params[] = $startPeriod;
        } elseif ($endPeriod) {
            $sql .= " and date(ped.dataPedido) <= ?";
            $params[] = $endPeriod;
        }

        $stmt = $this->conn->query($sql, $params);

        if ($stmt) {
            return $stmt->fetchAll();
        }

        throw new \Exception('Não foi possível buscar informações dos pedidos');
    }

    public function getSalesDataByDate(string $startPeriod, string $endPeriod)
    {
        $sql = "select ped.idPedido, i.idItem, (total_pages * print_value + total_finish) as item_cost, valorItem, item_price, total_pages, i.delivery_value, delivery_id, withdrawal_id, ped.dataPedido
        from tbpedido ped
        join
        (select item.idItem, item.tbPedido_idPedido, item.valorItem, (item.valorItem + ent.delivery_value) as item_price, total_pages, (acab.valor * item.qdeAcabamentos) as total_finish, imp.valor as print_value, ent.delivery_value, frete.type_id as delivery_id, ret.type_id as withdrawal_id
            from tbitem item
            join tbentrega ent on item.idItem = ent.id_item
            left join tbfrete frete on ent.idEntrega = frete.id_entrega
            left join tbretirada ret on ent.tbRetirada_idRetirada = ret.idRetirada
            join custo_acabamento acab on item.tbAcabamento_idAcabamento = acab.tbAcabamento_idAcabamento
            join custo_impressao imp on item.tbPapel_idPapel = imp.tbPapel_idPapel and item.tbCor_idCor = imp.tbCor_idCor and item.tbLado_idLado = imp.tbLado_idLado
            join (select sum(arq.numPaginas * arq.qtdImpressoes) as total_pages, arq.tbItem_idItem from tbarquivo arq group by arq.tbItem_idItem) as a on item.idItem = a.tbItem_idItem) as i on ped.idPedido = i.tbPedido_idPedido
        where ped.statusPagamento = ?";
        $params = [TbPedido::STATUS_IUGU_PAID];

        if ($startPeriod && $endPeriod) {
            $sql .= " and date(ped.dataPedido) >= ? and date(ped.dataPedido) <= ?";
            $params[] = $startPeriod;
            $params[] = $endPeriod;
        } elseif ($startPeriod) {
            $sql .= " and date(ped.dataPedido) >= ?";
            $params[] = $startPeriod;
        } elseif ($endPeriod) {
            $sql .= " and date(ped.dataPedido) <= ?";
            $params[] = $endPeriod;
        }

        $sql = "select sum(subquery.item_cost) as total_cost, sum(subquery.item_price) as total_value, count(subquery.idPedido) as total_sales,
                sum(subquery.delivery_value) as total_delivery, date(subquery.dataPedido) as date
                from (" . $sql . ") as subquery group by date(subquery.dataPedido)";

        $stmt = $this->conn->query($sql, $params);

        if ($stmt) {
            return $stmt->fetchAll();
        }

        throw new \Exception('Não foi possível buscar informações dos pedidos');
    }

    public function calculateItemsPrice(string $startPeriod, string $endPeriod)
    {
        $sql = "select ped.idPedido, item.idItem, (item.valorItem + ent.delivery_value) as item_price, ped.dataPedido,
        item.valorItem, item.descTotal, item.qdeAcabamentos, a.valorAcabamento
        from tbpedido ped
        join tbitem item on ped.idPedido = item.tbPedido_idPedido
        join tbacabamento as a on item.tbAcabamento_idAcabamento = a.idAcabamento
        join tbentrega ent on item.idItem = ent.id_item
        where ped.statusPagamento = ?";
        $params = [TbPedido::STATUS_IUGU_PAID];

        if ($startPeriod && $endPeriod) {
            $sql .= " and date(ped.dataPedido) >= ? and date(ped.dataPedido) <= ?";
            $params[] = $startPeriod;
            $params[] = $endPeriod;
        } elseif ($startPeriod) {
            $sql .= " and date(ped.dataPedido) >= ?";
            $params[] = $startPeriod;
        } elseif ($endPeriod) {
            $sql .= " and date(ped.dataPedido) <= ?";
            $params[] = $endPeriod;
        }

        $stmt = $this->conn->query($sql, $params);

        if ($stmt) {
            return $stmt->fetchAll();
        }

        throw new \Exception('Não foi possível buscar informações dos pedidos');
    }

    public function countDeliveryTypes(string $startPeriod, string $endPeriod)
    {
        $sql = "select sum(total_delivery_1) as total_delivery_1, sum(total_delivery_2) as total_delivery_2, sum(total_delivery_3) as total_delivery_3, sum(total_delivery_4) as total_delivery_4, sum(total_delivery_5) as total_delivery_5, sum(total_delivery_6) as total_delivery_6, sum(total_delivery_7) as total_delivery_7, sum(total_delivery_8) as total_delivery_8, sum(total_withdrawal_1) as total_withdrawal_1, sum(total_withdrawal_2) as total_withdrawal_2, sum(total_withdrawal_3) as total_withdrawal_3, sum(total_withdrawal_4) as total_withdrawal_4
        from tbpedido ped
        join (
            select sum(total_delivery_1) as total_delivery_1, sum(total_delivery_2) as total_delivery_2, sum(total_delivery_3) as total_delivery_3, sum(total_delivery_4) as total_delivery_4, sum(total_delivery_5) as total_delivery_5, sum(total_delivery_6) as total_delivery_6, sum(total_delivery_7) as total_delivery_7, sum(total_delivery_8) as total_delivery_8, sum(count_withdrawal_1) as total_withdrawal_1, sum(count_withdrawal_2) as total_withdrawal_2, sum(count_withdrawal_3) as total_withdrawal_3, sum(count_withdrawal_4) as total_withdrawal_4, item.tbPedido_idPedido
            from tbitem item
            join (
                select sum(count_delivery_1) as total_delivery_1, sum(count_delivery_2) as total_delivery_2, sum(count_delivery_3) as total_delivery_3, sum(count_delivery_4) as total_delivery_4, sum(count_delivery_5) as total_delivery_5, sum(count_delivery_6) as total_delivery_6, sum(count_delivery_7) as total_delivery_7, sum(count_delivery_8) as total_delivery_8, count(r1.idRetirada) as count_withdrawal_1, count(r2.idRetirada) as count_withdrawal_2, count(r3.idRetirada) as count_withdrawal_3, count(r4.idRetirada) as count_withdrawal_4, ent.id_item
                from tbentrega ent
                left join (
                    select count(idFrete) as count_delivery_1, id_entrega from tbfrete where type_id = 1 group by id_entrega
                ) as f1 on ent.idEntrega = f1.id_entrega
                left join (
                    select count(idFrete) as count_delivery_2, id_entrega from tbfrete where type_id = 2 group by id_entrega
                ) as f2 on ent.idEntrega = f2.id_entrega
                left join (
                    select count(idFrete) as count_delivery_3, id_entrega from tbfrete where type_id = 3 group by id_entrega
                ) as f3 on ent.idEntrega = f3.id_entrega
                left join (
                    select count(idFrete) as count_delivery_4, id_entrega from tbfrete where type_id = 4 group by id_entrega
                ) as f4 on ent.idEntrega = f4.id_entrega
                left join (
                    select count(idFrete) as count_delivery_5, id_entrega from tbfrete where type_id = 5 group by id_entrega
                ) as f5 on ent.idEntrega = f5.id_entrega
                left join (
                    select count(idFrete) as count_delivery_6, id_entrega from tbfrete where type_id = 6 group by id_entrega
                ) as f6 on ent.idEntrega = f6.id_entrega
                left join (
                    select count(idFrete) as count_delivery_7, id_entrega from tbfrete where type_id = 7 group by id_entrega
                ) as f7 on ent.idEntrega = f7.id_entrega
                left join (
                    select count(idFrete) as count_delivery_8, id_entrega from tbfrete where type_id = 8 group by id_entrega
                ) as f8 on ent.idEntrega = f8.id_entrega
                left join tbretirada r1 on ent.tbRetirada_idRetirada = r1.idRetirada and r1.type_id = 1
                left join tbretirada r2 on ent.tbRetirada_idRetirada = r2.idRetirada and r2.type_id = 2
                left join tbretirada r3 on ent.tbRetirada_idRetirada = r3.idRetirada and r3.type_id = 3
                left join tbretirada r4 on ent.tbRetirada_idRetirada = r4.idRetirada and r4.type_id = 4
                group by ent.id_item
            ) as e on item.idItem = e.id_item
            group by item.tbPedido_idPedido
        ) as i on ped.idPedido = i.tbPedido_idPedido
        where ped.statusPagamento = ?";
        $params = [TbPedido::STATUS_IUGU_PAID];

        if ($startPeriod && $endPeriod) {
            $sql .= " and date(ped.dataPedido) >= ? and date(ped.dataPedido) <= ?";
            $params[] = $startPeriod;
            $params[] = $endPeriod;
        } elseif ($startPeriod) {
            $sql .= " and date(ped.dataPedido) >= ?";
            $params[] = $startPeriod;
        } elseif ($endPeriod) {
            $sql .= " and date(ped.dataPedido) <= ?";
            $params[] = $endPeriod;
        }

        $sql .= " group by ped.statusPagamento";

        $stmt = $this->conn->query($sql, $params);

        if ($stmt) {
            return $stmt->fetch();
        }

        throw new \Exception('Não foi possível buscar informações dos pedidos');
    }

    public function calculateAverageTicket(string $startPeriod, string $endPeriod)
    {
        $sql = "select avg(total_items_value) as total_orders_value, count(ped.idPedido) as total_orders
        from tbpedido ped
        join
        (select item.tbPedido_idPedido, sum(item.valorItem + ent.delivery_value) as total_items_value from tbitem item
            join tbentrega ent on item.idItem = ent.id_item
            group by item.tbPedido_idPedido) as i on ped.idPedido = i.tbPedido_idPedido
        where ped.statusPagamento = ?";
        $params = [TbPedido::STATUS_IUGU_PAID];

        if ($startPeriod && $endPeriod) {
            $sql .= " and date(ped.dataPedido) >= ? and date(ped.dataPedido) <= ?";
            $params[] = $startPeriod;
            $params[] = $endPeriod;
        } elseif ($startPeriod) {
            $sql .= " and date(ped.dataPedido) >= ?";
            $params[] = $startPeriod;
        } elseif ($endPeriod) {
            $sql .= " and date(ped.dataPedido) <= ?";
            $params[] = $endPeriod;
        }

        $sql .= "group by ped.statusPagamento";
        $stmt = $this->conn->query($sql, $params);

        if ($stmt) {
            return $stmt->fetch();
        }

        throw new \Exception('Não foi possível buscar informações dos pedidos');
    }

    public function getCustomersOrders(string $startPeriod, string $endPeriod)
    {
        $sql = "select cli.idCliente, cli.nomeCliente, cli.emailCliente, cli.telCliente, cli.dtIngresso,
        ped.idPedido, ped.dataPedido, ped.telEntrega, subquery.valor_pedido
        from tbcliente cli
        join tbpedido ped on ped.tbCliente_idCliente = cli.idCliente
        join (
            select item.tbPedido_idPedido, sum(item.valorItem + ent.delivery_value) as valor_pedido
            from tbitem item join tbentrega ent on item.idItem = ent.id_item
            group by item.tbPedido_idPedido) as subquery on ped.idPedido = subquery.tbPedido_idPedido
        where ped.statusPagamento = ?";
        $params = [TbPedido::STATUS_IUGU_PAID];

        if ($startPeriod && $endPeriod) {
            $sql .= " and date(ped.dataPedido) >= ? and date(ped.dataPedido) <= ?";
            $params[] = $startPeriod;
            $params[] = $endPeriod;
        } elseif ($startPeriod) {
            $sql .= " and date(ped.dataPedido) >= ?";
            $params[] = $startPeriod;
        } elseif ($endPeriod) {
            $sql .= " and date(ped.dataPedido) <= ?";
            $params[] = $endPeriod;
        }

        $sql .= "";
        $stmt = $this->conn->query($sql, $params);

        if ($stmt) {
            return $stmt->fetchAll();
        }

        throw new \Exception('Não foi possível buscar informações dos pedidos');
    }

    public function getNewCustomersInPeriod(string $startPeriod, string $endPeriod)
    {
        $sql = "select count(ped.idPedido) as total_pedidos
        from tbpedido as ped
        join tbcliente as cli on cli.idCliente = ped.tbCliente_idCliente
        join (
            select cli.idCliente, count(ped.idPedido) as num_pedidos from tbcliente as cli
            join tbpedido as ped on ped.tbCliente_idCliente = cli.idCliente group by cli.idCliente
        ) as subquery on cli.idCliente = subquery.idCliente
        where ped.statusPagamento = ?";
        $params = [TbPedido::STATUS_IUGU_PAID];

        if ($startPeriod && $endPeriod) {
            $sql .= " and date(ped.dataPedido) >= ? and date(ped.dataPedido) <= ?";
            $params[] = $startPeriod;
            $params[] = $endPeriod;
        } elseif ($startPeriod) {
            $sql .= " and date(ped.dataPedido) >= ?";
            $params[] = $startPeriod;
        } elseif ($endPeriod) {
            $sql .= " and date(ped.dataPedido) <= ?";
            $params[] = $endPeriod;
        }

        $sql .= " and subquery.num_pedidos = 1";
        $stmt = $this->conn->query($sql, $params);

        if ($stmt) {
            return $stmt->fetch()['total_pedidos'];
        }

        throw new \Exception('Não foi possível buscar informações dos pedidos');
    }

    public function getRecurringCustomersInPeriod(string $startPeriod, string $endPeriod)
    {
        $sql = "select count(ped.idPedido) as total_pedidos
        from tbpedido as ped
        join tbcliente as cli on cli.idCliente = ped.tbCliente_idCliente
        join (
            select cli.idCliente, count(ped.idPedido) as num_pedidos from tbcliente as cli
            join tbpedido as ped on ped.tbCliente_idCliente = cli.idCliente group by cli.idCliente
        ) as subquery on cli.idCliente = subquery.idCliente
        where ped.statusPagamento = ?";
        $params = [TbPedido::STATUS_IUGU_PAID];

        if ($startPeriod && $endPeriod) {
            $sql .= " and date(ped.dataPedido) >= ? and date(ped.dataPedido) <= ?";
            $params[] = $startPeriod;
            $params[] = $endPeriod;
        } elseif ($startPeriod) {
            $sql .= " and date(ped.dataPedido) >= ?";
            $params[] = $startPeriod;
        } elseif ($endPeriod) {
            $sql .= " and date(ped.dataPedido) <= ?";
            $params[] = $endPeriod;
        }

        $sql .= " and subquery.num_pedidos > 1";
        $stmt = $this->conn->query($sql, $params);

        if ($stmt) {
            return $stmt->fetch()['total_pedidos'];
        }

        throw new \Exception('Não foi possível buscar informações dos pedidos');
    }

    public function getItemPriceRangeInfo(int $itemId)
    {
        $sql = "select sum(arq.numPaginas * arq.qtdImpressoes) as numPages, item.tbPapel_idPapel as paperId, papel.descricaoPapel as paper_type, item.tbCor_idCor as colorId
        from tbitem item
        join tbpapel papel on item.tbPapel_idPapel = papel.idPapel
        join tbarquivo arq on item.idItem = arq.tbItem_idItem
        where item.idItem = ? group by arq.tbItem_idItem";
        $params = [$itemId];

        $stmt = $this->conn->query($sql, $params);

        if ($stmt) {
            return $stmt->fetch();
        }

        throw new \Exception('Não foi possível buscar informações do item');
    }

    public function getPriceRangeInfo(int $paperId, int $numPages, int $hasColor)
    {
        $sql = "select *from tbfaixa where de <= ? and ate >= ? and idPapel = ? and hasColor = ? limit 1";
        $params = [$numPages, $numPages, $paperId, $hasColor];

        $stmt = $this->conn->query($sql, $params);

        if ($stmt) {
            return $stmt->fetch();
        }

        throw new \Exception('Não foi possível buscar as informações da faixa de preços');
    }

    public function calculatePeriodSummary(string $startPeriod, string $endPeriod)
    {
        $sql = "select sum(count_pages_pb) as total_pages_pb, sum(value_pb) as total_value_pb, sum(count_pages_c) as total_pages_c, sum(value_c) as total_value_c, sum(count_finishes) as total_finishes, sum(value_finishes) as total_value_finishes
        from tbpedido ped
        left join (select item.tbPedido_idPedido, sum(count_pages_pb) as count_pages_pb, sum(item.valorItem + item.descTotal - (acab.valorAcabamento * item.qdeAcabamentos)) as value_pb from tbitem item join (select tbItem_idItem, sum(numPaginas * qtdImpressoes) as count_pages_pb from tbarquivo group by tbItem_idItem) as arq on item.idItem = arq.tbItem_idItem join tbacabamento acab on item.tbAcabamento_idAcabamento = acab.idAcabamento where item.tbCor_idCor = 1 group by item.tbPedido_idPedido) as pb on ped.idPedido = pb.tbPedido_idPedido
        left join (select item.tbPedido_idPedido, sum(count_pages_c) as count_pages_c, sum(item.valorItem + item.descTotal - (acab.valorAcabamento * item.qdeAcabamentos)) as value_c from tbitem item join (select tbItem_idItem, sum(numPaginas * qtdImpressoes) as count_pages_c from tbarquivo group by tbItem_idItem) as arq on item.idItem = arq.tbItem_idItem join tbacabamento acab on item.tbAcabamento_idAcabamento = acab.idAcabamento where item.tbCor_idCor = 2 group by item.tbPedido_idPedido) as c on ped.idPedido = c.tbPedido_idPedido
        left join (select item.tbPedido_idPedido, sum(item.qdeAcabamentos) as count_finishes, sum(acab.valorAcabamento * item.qdeAcabamentos) as value_finishes from tbitem item join tbacabamento acab on item.tbAcabamento_idAcabamento = acab.idAcabamento group by item.tbPedido_idPedido) as a on ped.idPedido = a.tbPedido_idPedido
        where ped.statusPagamento = ?";
        $params = [TbPedido::STATUS_IUGU_PAID];

        if ($startPeriod && $endPeriod) {
            $sql .= " and date(ped.dataPedido) >= ? and date(ped.dataPedido) <= ?";
            $params[] = $startPeriod;
            $params[] = $endPeriod;
        } elseif ($startPeriod) {
            $sql .= " and date(ped.dataPedido) >= ?";
            $params[] = $startPeriod;
        } elseif ($endPeriod) {
            $sql .= " and date(ped.dataPedido) <= ?";
            $params[] = $endPeriod;
        }

        $sql .= " group by ped.statusPagamento";
        $stmt = $this->conn->query($sql, $params);

        if ($stmt) {
            return $stmt->fetch();
        }

        throw new \Exception('Não foi possível buscar informações dos pedidos');
    }

    public function countCouponTotals(string $startPeriod, string $endPeriod)
    {
        $sql = "select idDesc, codigo, count(idDesc) as qtd from
            (select p.idPedido, i.idItem, c.idDesc, c.codigo, p.dataPedido, p.statusPagamento
            from tbpedido as p
            join tbitem as i on i.tbPedido_idPedido = p.idPedido
            join tbcupom as c on i.tbCupom_idDesc = c.idDesc
            where c.tbCliente_idCliente is null and p.statusPagamento = ?";

        $params = [TbPedido::STATUS_IUGU_PAID];

        if ($startPeriod && $endPeriod) {
            $sql .= " and date(p.dataPedido) >= ? and date(p.dataPedido) <= ?";
            $params[] = $startPeriod;
            $params[] = $endPeriod;
        } elseif ($startPeriod) {
            $sql .= " and date(p.dataPedido) >= ?";
            $params[] = $startPeriod;
        } elseif ($endPeriod) {
            $sql .= " and date(p.dataPedido) <= ?";
            $params[] = $endPeriod;
        }

        $sql .= ") as coupons group by idDesc order by qtd desc limit 10";
        $stmt = $this->conn->query($sql, $params);

        if ($stmt) {
            return $stmt->fetchAll();
        }

        throw new \Exception('Não foi possível buscar informações dos cupons');
    }

    public function getInviteCouponsUsed(string $startPeriod, string $endPeriod)
    {
        $sql = "select c.idDesc, c.codigo, c.value, c.type, c.tbCliente_idCliente, i.idItem, p.idPedido, p.dataPedido from tbcupom as c
            join tbitem as i on c.idDesc = i.tbCupom_idDesc
            join tbpedido as p on i.tbPedido_idPedido = p.idPedido
            where c.invite_coupon = 1 and p.statusPagamento = ?";

        $params = [TbPedido::STATUS_IUGU_PAID];

        if ($startPeriod && $endPeriod) {
            $sql .= " and date(p.dataPedido) >= ? and date(p.dataPedido) <= ?";
            $params[] = $startPeriod;
            $params[] = $endPeriod;
        } elseif ($startPeriod) {
            $sql .= " and date(p.dataPedido) >= ?";
            $params[] = $startPeriod;
        } elseif ($endPeriod) {
            $sql .= " and date(p.dataPedido) <= ?";
            $params[] = $endPeriod;
        }

        $stmt = $this->conn->query($sql, $params);

        if ($stmt) {
            return $stmt->fetchAll();
        }

        throw new \Exception('Não foi possível buscar informações dos cupons');
    }
}
