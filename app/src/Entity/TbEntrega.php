<?php

namespace IntecPhp\Entity;

use IntecPhp\Entity\AbstractEntity;

class TbEntrega extends AbstractEntity
{
    const CLASSNAME      = 'tbentrega';
    const STATUS_PENDING = 'Pendente';
    protected $name      = 'tbentrega';
    protected $id        = 'idEntrega';

    public function add($idAddress, $idWithdrawal, $idItem, $value, $dispatchDate, $finishDate, $startDate)
    {
        $stmt = $this->conn->query(
            "insert into tbentrega(status, tbEndereco_idEndereco, tbRetirada_idRetirada, id_item,
                delivery_value, dispatch_date, finish_date, start_date) values(?, ?, ?, ?, ?, ?, ?, ?)",
            [self::STATUS_PENDING, $idAddress, $idWithdrawal, $idItem, $value, $dispatchDate, $finishDate, $startDate]
        );

        if ($stmt) {
            return $this->conn->lastInsertId();
        }

        throw new \Exception("Não foi possível adicionar as informações da entrega");
    }

    public function getByOrder($orderId)
    {
        $stmt = $this->conn->query(
            'SELECT end.logradouro as rua, end.bairro, end.cidade, end.cep, end.uf
            FROM tbpedido tp JOIN tbentrega te ON tp.tbEntrega_idEntrega = te.idEntrega
            JOIN tbendereco end ON end.idEndereco = te.tbEndereco_idEndereco
            WHERE tp.idPedido=?',
            [$orderId]
        );

        if ($stmt) {
            return $stmt->fetchAll();
        }

        return false;
    }

    public function getByItem($itemId)
    {
        $stmt = $this->conn->query(
            'SELECT *
            FROM tbentrega
            WHERE id_item=?',
            [$itemId]
        );

        if ($stmt) {
            return $stmt->fetchAll();
        }

        throw new \Exception('Não foi possível consultar a entrega');
    }

    public function getWithAddressByItem($itemId)
    {
        $stmt = $this->conn->query(
            'SELECT
                d.idEntrega, d.status, d.tbEndereco_idEndereco, d.tbRetirada_idRetirada, d.delivery_value,
                a.idEndereco, a.cep, a.numero, a.logradouro, a.bairro, a.cidade, a.uf, a.complemento
            FROM tbentrega d
            LEFT JOIN tbendereco a on d.tbEndereco_idEndereco = a.idEndereco
            WHERE id_item=? order by d.idEntrega desc limit 1',
            [$itemId]
        );

        if ($stmt) {
            return $stmt->fetch();
        }

        throw new \Exception('Não foi possível encontrar as informações de entrega');
    }

    public function getById($id)
    {
        $stmt = $this->conn->query(
            "select *from tbentrega where idEntrega = ?",
            [$id]
        );

        if ($stmt && $delivery = $stmt->fetch()) {
            return $delivery;
        }

        return false;
    }

    public function updateAllLinks($idDelivery, $idAddress, $idFreight)
    {
        $stmt = $this->conn->query(
            'update tbentrega set tbEndereco_idEndereco = ?, tbFrete_idFrete = ? where idEntrega = ?',
            [$idAddress, $idFreight, $idDelivery]
        );

        if ($stmt) {
            return true;
        }

        return false;
    }

    public function getByItemId($idItem)
    {
        $stmt = $this->conn->query(
            'select *from tbentrega where id_item = ?',
            [$idItem]
        );

        if ($stmt) {
            return $stmt->fetch();
        }

        throw new \Exception('Não foi possível consultar a entrega');
    }

    public function updateByItemId($idItem, $idAddress, $idFreight, $idWithdrawal)
    {
        $stmt = $this->conn->query(
            'update tbentrega set tbEndereco_idEndereco = ?, tbFrete_idFrete = ?, tbRetirada_idRetirada = ? where id_item = ?',
            [$idAddress, $idFreight, $idWithdrawal, $idItem]
        );

        if ($stmt) {
            return $stmt->rowCount();
        }

        return false;
    }

    public function update($idDelivery, $idAddress, $idFreight, $idWithdrawal)
    {
        $stmt = $this->conn->query(
            'update tbentrega set tbEndereco_idEndereco = ?, tbFrete_idFrete = ?, tbRetirada_idRetirada = ? where idEntrega = ?',
            [$idAddress, $idFreight, $idWithdrawal, $idDelivery]
        );

        if ($stmt) {
            return $stmt->rowCount();
        }

        return false;
    }
}
