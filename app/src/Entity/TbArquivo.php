<?php

namespace IntecPhp\Entity;

use IntecPhp\Entity\AbstractEntity;

class TbArquivo extends AbstractEntity
{
    protected $name = 'tbarquivo';
    protected $id   = 'idArquivo';

    public function add($idItem, $fileName, $numPages, $numPrints, $fileSize, $userFileName)
    {
        $stmt = $this->conn->query(
            "insert into tbarquivo(tbItem_idItem, arquivo, numPaginas, qtdImpressoes, nomeArquivo, tamArquivo) values(?, ?, ?, ?, ?, ?)",
        [$idItem, $fileName, $numPages, $numPrints, $userFileName, $fileSize]
        );

        if ($stmt) {
            return $this->conn->lastInsertId();
        }

        throw new \Exception('Não foi possível adicionar arquivo ao item do pedido.');
    }

    public function deleteArquivoItemId($id)
    {
        $stmt = $this->conn->query(
            'delete from tbarquivo where tbItem_idItem = ?',
            [$id]
        );

        if ($stmt) {
            return $stmt->rowCount();
        }

        return false;
    }

    public function filesOfItem($itemId)
    {
        $stmt = $this->conn->query('select * from tbarquivo where tbItem_idItem = ?', [$itemId]);

        if ($stmt) {
            return $stmt->fetchAll();
        }

        throw new \Exception('Não foi possível encontrar os arquivos do item: ' . $itemId);
    }

    public function updateFileIsOk($id, $value)
    {
        $stmt = $this->conn->query(
            "update tbarquivo set is_ok = ? where idArquivo = ?",
            [$value, $id]
        );

        if ($stmt) {
            return $stmt->rowCount();
        }

        throw new \Exception('Não foi possível atualizar o status do arquivo!');
    }

    public function checkIsFileRegistered(string $fileName)
    {
        $stmt = $this->conn->query(
            "select idArquivo, arquivo from tbarquivo where arquivo = ?",
            [$fileName]
        );

        if ($stmt) {
            return $stmt->fetch();
        }

        throw new \Exception('Não foi possível consultar o arquivo!');
    }
}
