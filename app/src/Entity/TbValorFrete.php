<?php

namespace IntecPhp\Entity;

class TbValorFrete extends AbstractEntity
{
    const CLASSNAME = 'tbvalorfrete';

    const PAC   = 'PAC';
    const SEDEX = 'SEDEX';

    protected $name = 'tbvalorfrete';
    protected $id = 'idValorFrete';

    public function getFreightSedex($zip, $weight) {
        return $this->getByCepWeightAndMethod($zip, $weight, self::SEDEX);
    }

    public function getFreightPac($zip, $weight) {
        return $this->getByCepWeightAndMethod($zip, $weight, self::PAC);
    }

    public function getByCepWeightAndMethod($zip, $weight, $method)
    {
        $stmt = $this->conn->query(
            "SELECT custoEntrega, prazoEntrega from $this->name WHERE nomeMetodo = '$method'
            AND cepInicial <= $zip AND cepFinal >= $zip AND pesoInicial <= $weight AND pesoFinal >= $weight",
            []
        );

        if ($stmt) {
            return $stmt->fetchAll();
        }

        throw new \Exception('Não foi possível encontrar os valores de custo e prazo para os dados informados.');
    }

}
