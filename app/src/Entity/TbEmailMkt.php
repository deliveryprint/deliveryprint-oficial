<?php

namespace IntecPhp\Entity;

class TbEmailMkt extends AbstractEntity
{
    const CLASSNAME = 'tbemailmkt';
    protected $name = 'tbemailmkt';
    protected $id = 'idEmailMkt';

    public function add($email, $origin)
    {
        $stmt = $this->conn->query("insert into
        tbemailmkt(dataInscricao, email, origem)
        values(now(), ?, ?)", [$email, $origin]);

        if ($stmt) {
            return $this->conn->lastInsertId();
        }

        throw new \Exception("Não foi possível salvar o email.");
    }

    public function getByPeriod($period)
    {

        $params = [];

        $sql = 'select e.idEmailMkt, e.email, e.dataInscricao, e.origem, i.idItem, i.valorItem, i.statusItem
        from tbemailmkt as e
        left join tbitem as i on e.idEmailMkt = i.tbEmailMkt_idEmailMkt';

        if ($period) {
            if ($period['from'] && $period['to']) {
                $sql .= " where e.dataInscricao between ? and ?";
                $params[] = $period['from'];
                $params[] = $period['to'];
            } elseif ($period['from']) {
                $sql .= " where e.dataInscricao >= ?";
                $params[] = $period['from'];
            } elseif ($period['to']) {
                $sql .= " where e.dataInscricao <= ?";
                $params[] = $period['to'];
            }
        }

        $sql .= " order by e.dataInscricao";

        $stmt = $this->conn->query($sql, $params);

        if ($stmt) {
            return $stmt->fetchAll();
        }

        throw new \Exception('Não foi possível carregar os itens');
    }

    public function getShoppingCartDiscountsByPeriod($period)
    {

        $params = [];

        $sql = 'select em.idEmailMkt, em.email, em.dataInscricao, em.origem, p.discountType, p.discountValue, p.statusPedido,
            sum(i.valorItem + e.delivery_value) as order_value
            from tbpedido as p
            join tbemailmkt as em on p.tbEmailMkt_idEmailMkt = em.idEmailMkt
            join tbitem as i on i.tbPedido_idPedido = p.idPedido
            join tbentrega as e on e.id_item = i.idItem';

        if ($period) {
            if ($period['from'] && $period['to']) {
                $sql .= " where em.dataInscricao between ? and ?";
                $params[] = $period['from'];
                $params[] = $period['to'];
            } elseif ($period['from']) {
                $sql .= " where em.dataInscricao >= ?";
                $params[] = $period['from'];
            } elseif ($period['to']) {
                $sql .= " where em.dataInscricao <= ?";
                $params[] = $period['to'];
            }
        }

        $sql .= " group by p.idPedido order by em.dataInscricao";

        $stmt = $this->conn->query($sql, $params);

        if ($stmt) {
            return $stmt->fetchAll();
        }

        throw new \Exception('Não foi possível carregar os itens');
    }

}
