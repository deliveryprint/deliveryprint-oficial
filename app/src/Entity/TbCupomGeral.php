<?php

namespace IntecPhp\Entity;

class TbCupomGeral extends AbstractEntity
{
    protected $name = 'tbcupomGeral';
    protected $id = 'idDescGeral';

    public function getLastActive()
    {
        $stmt = $this->conn->query('select * from tbcupomGeral where status = 1 order by idDescGeral desc limit 1');

        if($stmt) {
            if($cd = $stmt->fetch()) {
                return $cd;
            }

            return 0;
        }

        throw new \Exception('Não foi possível procurar por um cupom de desconto geral');
    }
}
