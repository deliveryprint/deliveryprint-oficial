<?php

namespace IntecPhp\Entity;

class PasswordRecovery extends AbstractEntity
{
    protected $name = 'password_recovery';
    protected $id = 'id';

    public function add($email, $hash)
    {
        $stmt = $this->conn->query('insert into password_recovery(email, hash_code, active) values(?, ?, ?)', [
            $email, $hash, true
        ]);

        if($stmt) {
            return $stmt->rowCount();
        }

        throw new \Exception('Não foi possível criar a requisição de troca de senha');
    }

    public function disableRecovery($hash)
    {
        $stmt = $this->conn->query('update password_recovery set active = 0 where hash_code = ?', [$hash]);

        if($stmt) {
            return $stmt->rowCount();
        }

        throw new \Exception('Não foi possível desativar as requisições de senha anteriores');
    }

    public function getCustomerIdFromHash($hash)
    {
        $stmt = $this->conn->query('select c.idCliente from password_recovery p
            join tbcliente c on c.emailCliente = p.email where p.hash_code = ? and p.active = 1', [$hash]);

        if($stmt) {
            $res = $stmt->fetch();
            if($res) {
                return $res['idCliente'];
            }

            return false;
        }

        throw new \Exception('Não foi possível encontrar o usuário');
    }

}
