<?php

namespace IntecPhp\Entity;

class CsvEntity extends AbstractEntity
{
    protected $name = 'arquivo_csv';
    protected $id = 'id';

    public function save(string $name, string $file, string $status, string $applicationDate = null)
    {
        $stmt = $this->conn->query(
            "insert into arquivo_csv(nome, arquivo, status, data_aplicacao) values(?, ?, ?, ?)",
        [$name, $file, $status, $applicationDate]
        );

        if ($stmt) {
            return $this->conn->lastInsertId();
        }

        throw new \Exception('Não foi possível adicionar as informações do arquivo CSV');
    }

    public function updateStatus(int $id, string $status, string $applicationDate = null)
    {
        if (!$applicationDate) {
            $sql  = "update arquivo_csv set status = ? where id = ?";
            $data = [$status, $id];
        } else {
            $sql  = "update arquivo_csv set status = ?, data_aplicacao = ? where id = ?";
            $data = [$status, $applicationDate, $id];
        }

        $stmt = $this->conn->query($sql, $data);

        if ($stmt) {
            return $stmt->rowCount();
        }

        throw new \Exception('Não foi possível atualizar as informações referentes ao arquivo CSV');
    }

    public function getAll()
    {
        $stmt = $this->conn->query('select *from arquivo_csv', []);

        if ($stmt) {
            return $stmt->fetchAll();
        }

        throw new \Exception('Não foi possível consultar os arquivos registrados');
    }

    public function getById(int $id)
    {
        $stmt = $this->conn->query("select * from arquivo_csv where id = ?", [
            $id
        ]);

        if ($stmt) {
            return $stmt->fetch();
        }

        throw new \Exception('Não foi possível encontrar o arquivo solicitado');
    }
}
