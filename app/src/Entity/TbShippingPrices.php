<?php

namespace IntecPhp\Entity;

class TbShippingPrices extends AbstractEntity
{
    protected $name = 'tbshippingprices';
    protected $id   = 'idShippingPrice';

    public function getShippingPrices()
    {
        $stmt = $this->conn->query("SELECT *FROM $this->name WHERE $this->id = 1");

        if ($stmt) {
            return $stmt->fetch();
        }

        throw new \Exception('Não foi possível consultar os preços das entregas e retiradas expressas');
    }

    public function updateShippingPrices(array $prices)
    {
        $arrayKeys = array_keys($prices);
        $arrayValues = array_values($prices);

        $sql = "UPDATE $this->name SET ";
        for ($i = 0; $i < (count($arrayKeys) - 1); $i++) {
            $sql .= $arrayKeys[$i] . " = ?, ";
        }
        $sql .= $arrayKeys[$i] . " = ? WHERE $this->id = 1";

        $stmt = $this->conn->query($sql, $arrayValues);

        if ($stmt) {
            return $stmt->rowCount();
        }

        throw new \Exception('Não foi possível fazer a atualização de preços das entregas expressas');
    }
}
