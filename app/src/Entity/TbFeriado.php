<?php

namespace IntecPhp\Entity;

class TbFeriado extends AbstractEntity
{
    protected $name = 'tbferiado';
    protected $id = 'idFeriado';

    public function hasDate($date)
    {
        $stmt = $this->conn->query("select * from tbferiado where fdate=?", [
            $date
        ]);

        if($stmt) {
            return $stmt->rowCount();
        }

        return false;

    }

}
