<?php

namespace IntecPhp\Entity;

use IntecPhp\Entity\AbstractEntity;

class TbSettings extends AbstractEntity
{
    protected $name = 'tbsettings';
    protected $id = 'idSetting';

    public function getExtraBDaysSettings()
    {
        $stmt = $this->conn->query("select * from $this->name where $this->id = 1");

        if ($stmt) {
            return $stmt->fetch();
        }

        throw new \Exception('Não foi possível carregar as configurações');
    }

    public function updateExtraBDaysSettings($settings)
    {
        $deliveryExtraBDays = $settings['deliveryExtraBDays'];
        $withdrawalExtraBDays = $settings['withdrawalExtraBDays'];
        $dExpress1ExtraBDays = $settings['dExpress1ExtraBDays'];
        $dExpress2ExtraBDays = $settings['dExpress2ExtraBDays'];
        $dExpress3ExtraBDays = $settings['dExpress3ExtraBDays'];
        $dExpress4ExtraBDays = $settings['dExpress4ExtraBDays'];
        $dSedex1ExtraBDays = $settings['dSedex1ExtraBDays'];
        $dSedex2ExtraBDays = $settings['dSedex2ExtraBDays'];
        $dPac1ExtraBDays = $settings['dPac1ExtraBDays'];
        $dPac2ExtraBDays = $settings['dPac2ExtraBDays'];
        $wExpress1ExtraBDays = $settings['wExpress1ExtraBDays'];
        $wExpress2ExtraBDays = $settings['wExpress2ExtraBDays'];
        $wNormal1ExtraBDays = $settings['wNormal1ExtraBDays'];
        $wNormal2ExtraBDays = $settings['wNormal2ExtraBDays'];

        $stmt = $this->conn->query("update $this->name
            set deliveryExtraBDays = ?, withdrawalExtraBDays = ?, dExpress1ExtraBDays = ?, dExpress2ExtraBDays = ?, dExpress3ExtraBDays = ?,
            dExpress4ExtraBDays = ?, dSedex1ExtraBDays = ?, dSedex2ExtraBDays = ?, dPac1ExtraBDays = ?, dPac2ExtraBDays = ?,
            wExpress1ExtraBDays = ?, wExpress2ExtraBDays = ?, wNormal1ExtraBDays = ?, wNormal2ExtraBDays = ? where $this->id = 1", [
            $deliveryExtraBDays, $withdrawalExtraBDays, $dExpress1ExtraBDays, $dExpress2ExtraBDays, $dExpress3ExtraBDays,
            $dExpress4ExtraBDays, $dSedex1ExtraBDays, $dSedex2ExtraBDays, $dPac1ExtraBDays, $dPac2ExtraBDays, $wExpress1ExtraBDays,
            $wExpress2ExtraBDays, $wNormal1ExtraBDays, $wNormal2ExtraBDays
        ]);

        if ($stmt) {
            return $stmt->rowCount();
        }

        throw new \Exception('Não foi possível atualizar as configurações');
    }
}
