<?php

namespace IntecPhp\Entity;

class SmsEntity extends AbstractEntity
{
    protected $name = 'sms';
    protected $id = 'id';

    public function save(string $idSms, int $idPedido, string $phoneNumber, string $message, string $sendDate = null)
    {
        $stmt = $this->conn->query(
            "insert into sms(id_sms, id_pedido, celular, mensagem, data_envio) values(?, ?, ?, ?, ?)",
        [$idSms, $idPedido, $phoneNumber, $message, $sendDate]
        );

        if ($stmt) {
            return $this->conn->lastInsertId();
        }

        throw new \Exception('Não foi possível salvar as informações da mensagem enviada.');
    }

    public function saveReply(string $idSms, string $reply, string $dateReply)
    {
        $stmt = $this->conn->query(
            'update sms set resposta=?, data_resposta=? where id_sms=?',
        [$reply, $dateReply, $idSms]
        );

        if ($stmt) {
            return $stmt->rowCount();
        }

        throw new \Exception('Não foi possível salvar as informações da mensagem recebida.');
    }

    public function getAllAndLastSmsDate(string $startPeriod, string $endPeriod, string $filterType)
    {
        $sql = 'select s.id_pedido, c.nomeCliente, s.celular, s.data_envio, s.data_resposta, s.resposta  from sms s
            join tbpedido p on s.id_pedido = p.idPedido
            join tbcliente c on c.idCliente = p.tbCliente_idCliente';
        $params = [];

        if ($filterType) {
            if ($filterType == 'Envio') {
                $column = 'date(s.data_envio)';
            } else {
                $column = 'date(s.data_resposta)';
            }

            if ($startPeriod && $endPeriod) {
                $sql .= " where $column >= ? and $column <= ?";
                $params = [$startPeriod, $endPeriod];
            } elseif ($startPeriod) {
                $sql .= " where $column >= ?";
                $params = [$startPeriod];
            } else {
                $sql .= " where $column <= ?";
                $params = [$endPeriod];
            }
        }

        $sql .= " order by s.data_envio";

        $stmt = $this->conn->query($sql, $params);

        if ($stmt) {
            return $stmt->fetchAll();
        }

        throw new \Exception('Não foi possível consultar os sms registrados');
    }

    public function checkNumber(string $phoneNumber)
    {
        $stmt = $this->conn->query(
            "select id from sms where celular = ? limit 1",
            [$phoneNumber]
        );

        if ($stmt) {
            return $stmt->fetch();
        }

        throw new \Exception('Não foi possível consultar o número registrado');
    }
}
