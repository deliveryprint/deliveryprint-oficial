<?php

namespace IntecPhp\Entity;

use IntecPhp\Model\DbHandler;

abstract class AbstractEntity
{
    protected $conn;

    protected $name;
    protected $id;

    public function __construct(DbHandler $conn)
    {
        $this->conn = $conn;
    }

    public function get($id)
    {
        $stmt = $this->conn->query("select * from $this->name where $this->id = ?", [$id]);

        if ($stmt) {
            return $stmt->fetch();
        }

        throw new \Exception('Falha na consulta');
    }

    public function getAll()
    {
        $stmt = $this->conn->query("select * from $this->name ");

        if ($stmt) {
            return $stmt->fetchAll();
        }

        throw new \Exception('Falha na consulta');
    }

    public function beginTransaction()
    {
        if (!$this->conn->inTransaction()) {
            $this->conn->beginTransaction();
        }
    }

    public function rollBack()
    {
        if ($this->conn->inTransaction()) {
            $this->conn->rollBack();
        }
    }

    public function commit()
    {
        $this->conn->commit();
    }
}
