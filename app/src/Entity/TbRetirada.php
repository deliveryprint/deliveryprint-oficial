<?php

namespace IntecPhp\Entity;

class TbRetirada extends AbstractEntity
{
    const CLASSNAME = 'tbretirada';
    protected $name = 'tbretirada';
    protected $id   = 'idRetirada';

    public function add($id, $name, $type, $deadline, $value)
    {
        $stmt = $this->conn->query(
            'insert into tbretirada(type_id, type_name, name, prazo, valor) values(?, ?, ?, ?, ?)',
        [$id, $name, $type, $deadline, $value]
        );

        if ($stmt) {
            return $this->conn->lastInsertId();
        }

        throw new \Exception("Não foi possível adicionar as informações da retirada");
    }
}
