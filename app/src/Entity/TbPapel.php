<?php

namespace IntecPhp\Entity;

class TbPapel extends AbstractEntity
{
    protected $name = 'tbpapel';
    protected $id = 'idPapel';


    public function getAll()
    {
        $stmt = $this->conn->query(
            "select * from $this->name order by paper_order asc",
            []
        );

        if($stmt){
            return $stmt->fetchAll();
        }

        throw new \Exception('Não foi possível buscar os tipos de papel');
    }

}
