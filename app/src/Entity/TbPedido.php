<?php

namespace IntecPhp\Entity;

use IntecPhp\Entity\AbstractEntity;

class TbPedido extends AbstractEntity
{
    const CLASSNAME                 = 'tbpedido';
    const STATUS_CART               = 'Carrinho';
    const STATUS_PENDING            = 'Pagamento Pendente';
    const STATUS_ACCOMPLISHED       = 'Pedido Realizado';
    const STATUS_IUGU_PENDING       = 'Pendente';
    const STATUS_IUGU_PAID          = 'Pago';
    const STATUS_PRODUCTION         = 'Em Produção';
    const STATUS_DISPATCH           = 'Despachado';
    const STATUS_WAITING_WITHDRAWAL = 'Pronto para Retirada';
    const STATUS_DELIVERY           = 'Entregue';
    const STATUS_CANCELED           = 'Cancelado';
    const STATUS_BLOCKED            = 'Bloqueado';

    protected $name = 'tbpedido';
    protected $id = 'idPedido';

    public function add($idUSer)
    {
        $stmt = $this->conn->query("insert into
        tbpedido(tbCliente_idCliente, dataPedido, statusPedido)
        values(?, now(), ?)", [$idUSer, self::STATUS_CART]);

        if ($stmt) {
            return $this->conn->lastInsertId();
        }

        throw new \Exception("Não foi possível salvar o pedido.");
    }

    public function getByUserId($userId)
    {
        $stmt = $this->conn->query(
            "select o.*, sum(i.valorItem + e.delivery_value) as order_value from $this->name o
            join tbitem i on i.tbPedido_idPedido = o.idPedido
            join tbentrega e on i.idItem = e.id_item
            where tbCliente_idCliente = ? and o.statusPedido <> 'Carrinho' group by(o.idPedido) order by o.idPedido desc",
            [$userId]
        );

        if ($stmt) {
            return $stmt->fetchAll();
        }

        throw new \Exception('Não foi possível pesquisar os pedidos');
    }

    public function getById($id)
    {
        $stmt = $this->conn->query(
            'SELECT
                ped.dataPedido, ped.cardBanner, ped.endCardNumber, ped.discountValue, ped.discountType, ped.tbEmailMkt_idEmailMkt,
                ti.idItem, tc.descricaoCor, tl.descricaoLado, tp.descricaoPapel, ti.borda, ti.obsCliente, ti.tracking_code, ti.statusItem, ti.tbCupom_idDesc,
                ta.descricaoAcabamento, ti.valorItem, sum(tArq.numPaginas * tArq.qtdImpressoes) as numberPages, concat(tbPedido_idPedido, \'.\', idItem) as pretty_id,
                count(tArq.idArquivo) as arquivos, qdeAcabamentos as encadernacoes
            FROM
                tbpedido ped
                JOIN tbitem ti ON ped.idPedido = ti.tbPedido_idPedido
                JOIN tbcor tc ON ti.tbCor_idCor = tc.idCor
                JOIN tblado tl ON ti.tbLado_idLado = tl.idLado
                JOIN tbpapel tp ON ti.tbPapel_idPapel = tp.idPapel
                JOIN tbacabamento ta ON ti.tbAcabamento_idAcabamento = ta.idAcabamento
                JOIN tbarquivo tArq ON ti.idItem = tArq.tbItem_idItem
                JOIN tbentrega e on ti.idItem = e.id_item
                WHERE ti.tbPedido_idPedido = ? GROUP BY ti.idItem',
            [$id]
        );

        if ($stmt) {
            return $stmt->fetchAll();
        }

        throw new \Exception("Não foi possível encontrar o pedido");
    }

    public function getByUserIdAndStatus($idUser, $orderStatus)
    {
        $stmt = $this->conn->query(
            "select *from tbpedido where tbCliente_idCliente = ? and statusPedido = ? order by idPedido desc limit 1",
            [$idUser, $orderStatus]
        );

        if ($stmt) {
            return $stmt->fetch();
        }

        throw new \Exception('Não foi possível consultar as informações do pedido');
    }

    public function getAtSameAddress($idUser, $orderStatus, $zip)
    {
        $stmt = $this->conn->query(
            "select ped.*, end.idEndereco, end.cep from tbpedido ped
             join tbitem item on ped.idPedido = item.tbPedido_idPedido
             join tbentrega ent on item.idItem = ent.id_item
             join tbendereco end on ent.tbEndereco_idEndereco = end.idEndereco
             where ped.tbCliente_idCliente = ? and ped.statusPedido = ? and ent.tbRetirada_idRetirada is null and end.cep = ?
             order by ped.idPedido limit 1",
            [$idUser, $orderStatus, $zip]
        );

        if ($stmt) {
            return $stmt->fetch();
        }

        throw new \Exception('Não foi possível consultar as informações do pedido');
    }

    public function getLastIdByStatus($id, $status)
    {
        $stmt = $this->conn->query(
            'SELECT idPedido, dataPedido
            FROM tbpedido
            WHERE statusPedido = ? AND tbCliente_idCliente = ? order by idPedido desc limit 1',
            [$status, $id]
        );

        if ($stmt) {
            return $stmt->fetch();
        }

        throw new \Exception('Não foi possível pesquisar o pedido');
    }

    public function updateUserId($userId, $orderId)
    {
        $stmt = $this->conn->query(
            "update tbpedido set tbCliente_idCliente = ? where idPedido = ?",
            [$userId, $orderId]
        );

        if ($stmt) {
            return true;
        }

        return false;
    }

    public function updateLinkedId($orderId, $updateId)
    {
        $stmt = $this->conn->query(
            "update tbpedido set linkedOrderId = ? where idPedido = ?",
            [$updateId, $orderId]
        );

        if ($stmt) {
            return true;
        }

        return false;
    }

    public function getAllInfoById($id)
    {
        $stmt = $this->conn->query(
            "select *from tbpedido where idPedido = ?",
            [$id]
        );

        if ($stmt && $order = $stmt->fetch()) {
            return $order;
        }

        return false;
    }

    public function getAddrAndDeliveryByOrderId($id)
    {
        $stmt = $this->conn->query(
            "select end.cep, ent.tbEndereco_idEndereco, ent.tbFrete_idFrete from tbpedido ped
             join tbentrega ent on ent.idEntrega = ped.tbEntrega_idEntrega
             join tbendereco end on end.idEndereco = ent.tbEndereco_idEndereco
             where ped.idPedido = ?",
            [$id]
        );

        if ($stmt && $order = $stmt->fetch()) {
            return $order;
        }

        return false;
    }

    public function getDeliveryAndAddressInfoById($id)
    {
        $stmt = $this->conn->query(
            "select ent.idEntrega, ent.tbFrete_idFrete, item.idItem, end.idEndereco, end.cep from tbpedido ped
            join tbitem item on ped.idPedido = item.tbPedido_idPedido
            join tbentrega ent on item.idItem = ent.id_item
            join tbendereco end on ent.tbEndereco_idEndereco = end.idEndereco
            where ped.idPedido = ? and ent.tbEndereco_idEndereco is not null",
            [$id]
        );

        if ($stmt) {
            return $stmt->fetchAll();
        }

        throw new \Exception('Não foi possível consultar as informações de entrega do pedido');
    }

    public function updateAllDeliveriesAddrByOrderId($orderId, $addressId)
    {
        $stmt = $this->conn->query(
            "update tbentrega ent
            join tbitem item on ent.id_item = item.idItem
            join tbpedido ped on item.tbPedido_idPedido = ped.idPedido
            set ent.tbEndereco_idEndereco = ?
            where ped.idPedido = ? and ent.tbRetirada_idRetirada is null",
            [$addressId, $orderId]
        );

        if ($stmt) {
            return $stmt->rowCount();
        }

        throw new \Exception('Não foi possível atualizar as informações de entrega do pedido');
    }


    public function getFileInfoByOrderId($id)
    {
        $stmt = $this->conn->query(
            "SELECT
                *
            FROM
                tbpedido p
                JOIN tbitem i on p.idPedido = tbPedido_idPedido
                JOIN tbarquivo a on a.tbItem_idItem = i.idItem
            WHERE p.idPedido = ?
            ",
            [$id]
        );

        if ($stmt) {
            return $stmt->fetchAll();
        }

        return false;
    }

    public function getDeliveryWithdrawalInfoByOrderId($id)
    {
        $stmt = $this->conn->query(
            "SELECT
                *
            FROM
                tbpedido p
                JOIN tbitem i on p.idPedido = tbPedido_idPedido
                JOIN tbentrega e on e.id_item = i.idItem
                JOIN tbretirada r on r.idRetirada = e.tbRetirada_idRetirada
            WHERE p.idPedido = ? order by i.idItem desc
            ",
            [$id]
        );

        if ($stmt) {
            return $stmt->fetch();
        }

        return false;
    }

    public function getDeliveryInfoByOrderId($id)
    {
        $stmt = $this->conn->query(
            "SELECT
                *
            FROM
                tbpedido p
                JOIN tbitem i on p.idPedido = tbPedido_idPedido
                JOIN tbentrega e on e.id_item = i.idItem
                JOIN tbfrete f on f.id_entrega = e.idEntrega
            WHERE p.idPedido = ? order by i.idItem desc
            ",
            [$id]
        );

        if ($stmt) {
            return $stmt->fetch();
        }

        return false;
    }

    public function isWithdrawal($id)
    {
        $stmt = $this->conn->query(
            "SELECT
                *
            FROM
                tbpedido p
                JOIN tbitem i on p.idPedido = tbPedido_idPedido
                JOIN tbentrega e on e.id_item = i.idItem
            WHERE p.idPedido = ? order by i.idItem desc
            ",
            [$id]
        );

        if ($stmt) {
            $withdrawal = $stmt->fetch()['tbRetirada_idRetirada'];
            if (!$withdrawal) {
                return false;
            } else {
                return true;
            }
        }

        return false;
    }

    public function updateStatus($id, $status)
    {
        $stmt = $this->conn->query(
            "update tbpedido set statusPedido = ? where idPedido = ?",
            [$status, $id]
        );

        if ($stmt) {
            return $stmt->rowCount();
        }

        throw new \Exception('Não foi possível atualizar o status do pedido!');
    }

    public function updateIp($orderId, $ipId)
    {
        $stmt = $this->conn->query(
            "update tbpedido set tbIp_idIp = ? where idPedido = ?",
            [$ipId, $orderId]
        );

        if ($stmt) {
            return $stmt->rowCount();
        }

        throw new \Exception('Não foi possível atualizar o ip do pedido!');
    }

    public function setIuguId($id, $iuguId, $orderStatus, $paymentStatus, $paymentDate = null)
    {
        $stmt = $this->conn->query(
            "update tbpedido set idIuguPagamento = ?, statusPedido = ?, statusPagamento = ?, dataPagto = ? where idPedido = ?",
            [$iuguId, $orderStatus, $paymentStatus, $paymentDate, $id]
        );

        if ($stmt) {
            return $stmt->rowCount();
        }

        throw new \Exception('Não foi possível atualizar o id do pagamento da Iugu!');
    }

    public function setPromotionOrder($id, $orderStatus, $paymentStatus, $paymentDate = null)
    {
        $stmt = $this->conn->query(
            "update tbpedido set statusPedido = ?, statusPagamento = ?, dataPagto = ? where idPedido = ?",
            [$orderStatus, $paymentStatus, $paymentDate, $id]
        );

        if ($stmt) {
            return $stmt->rowCount();
        }

        throw new \Exception('Não foi possível atualizar o pedido de cortesia!');
    }

    public function updateStatusByIuguId($iuguId, $iuguStatus, $paymentDate)
    {
        $stmt = $this->conn->query(
            "update tbpedido set statusPagamento = ?, dataPagto = ? where idIuguPagamento = ?",
            [$iuguStatus, $paymentDate, $iuguId]
        );

        if ($stmt) {
            return $stmt->rowCount();
        }

        throw new \Exception('Não foi possível atualizar o status do pagamento!');
    }

    /*setar uma nova nota ao pedido */
    public function saveObs($id, $obsStatusPedido)
    {
        $stmt = $this->conn->query(
            "update tbpedido set obsStatusPedido = ? where idPedido = ?",
            [$obsStatusPedido, $id]
        );

        if ($stmt) {
            return $stmt->rowCount();
        }

        throw new \Exception('Não foi possível adicionar uma nota para o pedido!');
    }

    public function getAllNotCar()
    {
        $stmt = $this->conn->query(
            "select p.*, c.nomeCliente, c.emailCliente from tbpedido p
            inner join tbcliente c on p.tbCliente_idCliente = c.idCliente
            where statusPedido != ?",
            [self::STATUS_CART]
        );

        if ($stmt) {
            $order = $stmt->fetchAll();
            if (!empty($order)) {
                return $order;
            }
            return false;
        }

        throw new \Exception('Não foi possível consultar os pedidos!');
    }

    public function getInfoNota($idIugu)
    {
        $stmt = $this->conn->query(
            "SELECT
               p.*, c.*, e.*
            FROM
                tbpedido p
                JOIN tbcliente c on p.tbCliente_idCliente = c.idCliente
                LEFT JOIN tbendereco e on c.idCliente = e.idCliente
            WHERE p.idIuguPagamento = ? LIMIT 1 ",
            [$idIugu]
        );

        if ($stmt && $info = $stmt->fetch()) {
            return $info;
        }

        return false;
    }

    public function updateCardInfo($orderId, $cardBanner, $endCardNumber)
    {
        $stmt = $this->conn->query(
            "update tbpedido set cardBanner = ?, endCardNumber = ? where idPedido = ?",
            [$cardBanner, $endCardNumber, $orderId]
        );

        if ($stmt) {
            return $stmt->rowCount();
        }

        throw new \Exception('Não foi possível atualizar as informações do pedido!');
    }

    public function updateNameAndPhone($id, $name, $phone)
    {
        $stmt = $this->conn->query(
            "update tbpedido set nomeEntrega = ?, telEntrega = ? where idPedido = ?",
            [$name, $phone, $id]
        );

        if ($stmt) {
            return $stmt->rowCount();
        }

        throw new \Exception("Não foi possível atualizar o nome e telefone");
    }

    public function itemsHaveSameStatus($orderId, $itemStatus)
    {
        $stmt = $this->conn->query(
            "select count(idItem) as total, sum(case when statusItem = ? then 1 else 0 end) as eq from tbitem where tbPedido_idPedido = ?",
            [$itemStatus, $orderId]
        );

        if ($stmt) {
            $result = $stmt->fetch();
            return ($result['total'] == $result['eq']);
        }

        throw new \Exception("Não foi possível contabilizar o número de itens do pedido");
    }

    public function hasDeliveryName($orderId)
    {
        $stmt = $this->conn->query(
            "select nomeEntrega from tbpedido where idPedido = ?",
            [$orderId]
        );
        if ($stmt) {
            $result = $stmt->fetch();
            return $result['nomeEntrega'];
        }

        throw new \Exception("Não foi possível encontrar o nome de entrega");
    }

    public function getByIuguId($iuguId)
    {
        $stmt = $this->conn->query(
            "select *from tbpedido where idIuguPagamento = ?",
            [$iuguId]
        );
        if ($stmt) {
            return $stmt->fetch();
        }

        throw new \Exception("Não foi possível encontrar o pedido");
    }

    public function getFromDatetime($datetime)
    {
        $stmt = $this->conn->query(
            "select idPedido as id, dataPedido as datetime from tbpedido where dataPedido > ? and statusPedido != ? limit 1",
            [$datetime, self::STATUS_CART]
        );

        if ($stmt) {
            $result = $stmt->fetch();
            if (empty($result)) {
                return [];
            }
            return $result;
        }

        throw new \Exception('Não foi possível consultar os pedidos');
    }

    public function getOldsByStatus($date)
    {
        $stmt = $this->conn->query(
            "select ped.idPedido as id, item.idItem as itemId, date(ped.dataPedido) as data, arq.arquivo as file, ped.statusPedido as status from tbpedido ped
            join tbitem item on item.tbPedido_idPedido = ped.idPedido
            join tbarquivo arq on arq.tbItem_idItem = item.idItem
            where date(ped.dataPedido) < ? and (ped.statusPedido = ? or ped.statusPedido = ? or ped.statusPedido = ?)",
            [$date, self::STATUS_CART, self::STATUS_CANCELED, self::STATUS_DELIVERY]
        );

        if ($stmt) {
            return $stmt->fetchAll();
        }

        throw new \Exception('Não foi possível consultar os arquivos dos pedidos');
    }

    public function getByStatusAndDate(string $status, string $date)
    {
        $stmt = $this->conn->query(
            "select ped.idPedido as id, item.idItem as itemId, date(ped.dataPedido) as data, arq.arquivo as file, ped.statusPedido as status from tbpedido ped
            join tbitem item on item.tbPedido_idPedido = ped.idPedido
            join tbarquivo arq on arq.tbItem_idItem = item.idItem
            where date(ped.dataPedido) <= ? and ped.statusPedido = ?",
            [$date, $status]
        );

        if ($stmt) {
            return $stmt->fetchAll();
        }

        throw new \Exception('Não foi possível consultar os arquivos dos pedidos');
    }

    public function getCustomer($orderId)
    {
        $stmt = $this->conn->query('select c.* from tbpedido p
        join tbcliente c on p.tbCliente_idCliente = c.idCliente
            where p.idPedido = ?
        ', [
            $orderId
        ]);

        if ($stmt) {
            return $stmt->fetch();
        }

        throw new \Exception('Não foi possível buscar dados do cliente');
    }

    public function getPendingToNotify(string $notifyDate)
    {
        $stmt = $this->conn->query(
            "SELECT
                ped.idPedido, ped.dataPedido, ped.statusPedido, cli.*
            FROM tbpedido ped
            JOIN tbcliente cli ON ped.tbCliente_idCliente = cli.idCliente
            WHERE ped.statusPedido = ? AND date(ped.dataPedido) = ?",
            [self::STATUS_PENDING, $notifyDate]
        );

        if ($stmt) {
            return $stmt->fetchAll();
        }

        throw new \Exception('Não foi possível consultar as informações do pedido');
    }

    public function updateDiscountInfo($id, $discountType, $discountValue, $emailMktId)
    {
        $stmt = $this->conn->query(
            "update tbpedido set discountType = ?, discountValue = ?, tbEmailMkt_idEmailMkt = ? where idPedido = ?",
            [$discountType, $discountValue, $emailMktId, $id]
        );

        if ($stmt) {
            return $stmt->rowCount();
        }

        throw new \Exception("Não foi possível atualizar o desconto do pedido.");
    }

    public function getWithItemsCount(int $id)
    {
        $stmt = $this->conn->query(
            "SELECT ped.*, item.total_items
            FROM tbpedido ped
            JOIN (
                SELECT count(idItem) AS total_items, tbPedido_idPedido FROM tbitem GROUP BY tbPedido_idPedido
            ) AS item ON ped.idPedido = item.tbPedido_idPedido
            WHERE ped.idPedido = ?",
            [$id]
        );

        if ($stmt) {
            return $stmt->fetch();
        }

        throw new \Exception("Não foi possível consultar o pedido.");
    }
}
