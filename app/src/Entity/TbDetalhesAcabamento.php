<?php

namespace IntecPhp\Entity;

use IntecPhp\Entity\AbstractEntity;

class TbDetalhesAcabamento extends AbstractEntity
{
    protected $name = 'tbdetalhesacabamento';
    protected $id = 'idDetalhes';

    public function add($corDaCapa, $corDaLetra, $textoCapa, $textoLombada)
    {

        $stmt = $this->conn->query(
            "insert into $this->name(corDaCapa, corDaLetra, textoCapa, textoLombada) values(?, ?, ?, ?)",
            [$corDaCapa, $corDaLetra, $textoCapa, $textoLombada]
        );

        if ($stmt) {
            return $this->conn->lastInsertId();
        }

        throw new \Exception('Não foi possível salvar os detalhes');
    }
}
