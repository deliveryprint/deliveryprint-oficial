<?php

namespace IntecPhp\Service;

use FlyingLuscas\Correios\Client as CorreiosClient;
use FlyingLuscas\Correios\Service as CorreiosService;
use FlyingLuscas\Correios\PackageType;

class Shipping
{
    private $fromZip;

    const MAX_WEIGHT = 30;

    public function __construct(string $fromZip) {
        $this->fromZip = $fromZip;
    }

    public function calculateFreight(string $toZip, array $items)
    {
        $freight = $this->createFreight($toZip, $items);
        $freight->services(CorreiosService::SEDEX);
        $sedexFreight = $freight->calculate();
        $freight->services(CorreiosService::PAC);
        $pacFreight = $freight->calculate();
        return array_merge($sedexFreight, $pacFreight);
    }

    public function calculateSedexFreight(string $toZip, array $items)
    {
        $freight = $this->createFreight($toZip, $items);
        $freight->services(CorreiosService::SEDEX);
        return $freight->calculate();
    }

    public function calculatePacFreight(string $toZip, array $items)
    {
        $freight = $this->createFreight($toZip, $items);
        $freight->services(CorreiosService::PAC);
        return $freight->calculate();
    }

    private function createFreight(string $toZip, array $items)
    {
        $totalWeight = 0;

        foreach($items as $item) {
            $totalWeight += $item->weight;
        }

        $packageType = $totalWeight < 1 ? PackageType::ENVELOPE : PackageType::BOX;

        if($totalWeight > self::MAX_WEIGHT) {
            throw new \Exception('Peso máximo excedido (30kg).');
        }

        $client = new CorreiosClient();

        $freight = $client
            ->freight()
            ->package($packageType)
            ->origin($this->fromZip)
            ->destination($toZip);
        foreach($items as $item) {
            $freight->item(
                $item->width,
                $packageType === PackageType::ENVELOPE ? 0 : $item->height,
                $item->length,
                $item->weight,
                $item->amount
            );
        }

        return $freight;
    }
}
