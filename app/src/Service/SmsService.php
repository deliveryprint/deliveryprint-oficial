<?php

namespace IntecPhp\Service;

use TotalVoice\Client as TotalVoiceClient;

class SmsService
{
    private $tvClient;

    public function __construct(TotalVoiceClient $tvClient)
    {
        $this->tvClient = $tvClient;
    }

    public function sendSms(string $recipientNumber, string $message, \DateTime $sendDate)
    {
        $response = $this->tvClient->sms->enviar($recipientNumber, $message, true, false, $sendDate);

        if ($response->getStatusCode() != 200) {
            return false;
        }
        $r = json_decode($response->getContent());
        return $r->dados->id;
    }
}
