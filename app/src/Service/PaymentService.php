<?php

namespace IntecPhp\Service;

use Iugu;
use Iugu_Invoice;
use Iugu_Charge;
use Iugu_PaymentToken;

class PaymentService
{
    private $apiToken;
    private $accountId;

    public function __construct($apiToken, $accountId)
    {
        $this->apiToken  = $apiToken;
        $this->accountId = $accountId;
        Iugu::setApiKey($this->apiToken);
    }

    public function generateBankSlip($dueDate, $finalValue, $userInfo)
    {
        $invoice = Iugu_Invoice::create(
            array(
                "email"                   => $userInfo['email'],
                "due_date"                => $dueDate,
                "ensure_workday_due_date" => false,
                "ignore_due_email"        => false,
                "items"                   => array(
                    array(
                        "description" => "Pedido na DeliveryPrint",
                        "quantity"    => 1,
                        "price_cents" => $finalValue
                    )
                ),
                "payable_with" => "bank_slip",
                "payer"        => array(
                    "cpf_cnpj" => $userInfo['cpf_cnpj'],
                    "name"     => $userInfo['fullName'],
                    "address"  => array(
                        "zip_code"   => $userInfo['zipCode'],
                        "street"     => $userInfo['street'],
                        "number"     => $userInfo['number'],
                        "district"   => $userInfo['district'],
                        "city"       => $userInfo['city'],
                        "state"      => $userInfo['state'],
                        "complement" => $userInfo['complement']
                    )
                )
            )
        );

        if (!empty($invoice['errors'])) {
            if (!empty($invoice['errors']['payer.cpf_cnpj'])) {
                throw new \Exception('Digite um CPF ou CNPJ válido. Este está errado');
            } else {
                throw new \Exception('Não foi possível gerar o boleto. Preencha todos os campos corretamente');
            }
        }

        return [
            "id"         => $invoice['id'],
            "total"      => $invoice['total'],
            "secure_url" => $invoice['secure_url'],
        ];
    }

    public function duplicateBankSlip($bankSlipCode, $dueDate)
    {
        $iuguInvoice = new Iugu_Invoice(array(
            "id" => $bankSlipCode
        ));

        $invoice = $iuguInvoice->duplicate(
            array(
                "due_date" => $dueDate
            )
        );

        if (!$invoice) {
            throw new \Exception('Não foi possível gerar a segunda via do boleto. O id do boleto informado não é válido');
        }

        return [
            "id"         => $invoice['id'],
            "total"      => $invoice['total'],
            "secure_url" => $invoice['secure_url'],
        ];
    }

    public function generateCreditCardCharge($paymentToken, $finalValue, $installments, $userInfo)
    {
        $charge = Iugu_Charge::create(
            array(
                "token"  => $paymentToken,
                "email"  => $userInfo['email'],
                "months" => $installments,
                "items" => array(
                    array(
                        "description" => "Pedido na DeliveryPrint",
                        "quantity"    => 1,
                        "price_cents" => $finalValue
                    )
                )
            )
        );

        if (!empty($charge['errors'])) {
            throw new \Exception('Não foi possível efetuar o pagamento. Preencha todos os campos corretamente');
        }

        return [
            "message"    => $charge['message'],
            "url"        => $charge['url'],
            "pdf"        => $charge['pdf'],
            "invoice_id" => $charge['invoice_id'],
            "LR"         => $charge['LR']
        ];
    }

    /*
    Esta função está sendo utilizada somente para facilitar os testes
    Por meio dela é possível gerar uma Token de um cartão de crédito
    */
    private function createVirtualCreditCard()
    {
        $creditCardToken = Iugu_PaymentToken::create(
            array(
                "account_id" => $this->accountId,
                "method"     => 'credit_card',
                "test"       => true,
                "data"       => array(
                    "number"             => "4111111111111111",
                    "verification_value" => 158,
                    "first_name"         => "Testador",
                    "last_name"          => "Silva",
                    "month"              => 10,
                    "year"               => 2020
                )
            )
        );

        if (!empty($creditCardToken['errors'])) {
            throw new \Exception('Os dados informados do cartão não são válidos!');
        }

        return $creditCardToken;
    }

    public function fetchInvoice($iuguId)
    {
        $invoice = Iugu_Invoice::fetch($iuguId);

        if (!empty($invoice['errors'])) {
            throw new \Exception('O id da fatura informado não é válido!');
        }

        return [
            "id"           => $invoice['id'],
            "total"        => $invoice['total'],
            "secure_url"   => $invoice['secure_url'],
            "payable_with" => $invoice['payable_with']
        ];
    }
}
