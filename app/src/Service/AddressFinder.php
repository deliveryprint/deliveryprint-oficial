<?php

namespace IntecPhp\Service;

use Cagartner\CorreiosConsulta\CorreiosConsulta as CorreiosClient;

class AddressFinder
{
    private $client;

    public function __construct()
    {
        $this->client = new CorreiosClient();
    }

    public function find(string $zip)
    {
        $addr = $this->client->cep($zip);

        if (empty($addr)) {
            return [];
        }

        $addr['logradouro'] = strip_tags($addr['logradouro']);

        return [
            'zipcode' => $zip,
            'street' => $addr['logradouro'],
            'district' => $addr['bairro'],
            'city' => $addr['cidade'],
            'uf' => $addr['uf'],
        ];
    }
}
