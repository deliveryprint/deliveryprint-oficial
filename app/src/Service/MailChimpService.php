<?php

namespace IntecPhp\Service;

use DrewM\MailChimp\MailChimp;

class MailChimpService
{
    private $mailChimp;
    private $listId;

    public function __construct(string $token)
    {
        $this->mailChimp = new MailChimp($token);
    }

    public function subscribeEmailToList(string $email, string $listId)
    {
        $result = $this->mailChimp->post("lists/$listId/members", [
            'email_address' => $email,
            'status'        => 'subscribed'
        ]);

        return !empty($this->mailChimp->success());
    }
}
