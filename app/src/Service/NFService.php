<?php

namespace IntecPhp\Service;

use eNotasGW\eNotasGW;
use eNotasGW\Api\Exceptions as Exceptions;

class NFService
{

    private $empresaId;
    private $apiKey;

    public function __construct($empresaId, $apiKey)
    {
        $this->empresaId = $empresaId;
        $this->apiKey = $apiKey;
        eNotasGW::configure(array(
            'apiKey' => $this->apiKey
        ));
    }

    public function makeNF($infoNota, $finalValue, $tipoPessoa)
    {
        $params = array(
			'tipo' => 'NFS-e',
            'id' => $infoNota['idPedido'],
            'consumidorFinal' => true,
			'ambienteEmissao' => 'Producao', //'Homologacao' ou 'Producao'
            'enviarPorEmail' => true,
			'servico' => array(
                'descricao' => "NOTA FISCAL REFERENTE A SERVIÇOS DE FOTOCOPIAS\n\nREGIME TRIBUTÁRIO: ESSA EMPRESA SE DECLARA OPTANTE DO SIMPLES NACIONAL",
                'aliquotaIss' => 0,
                'codigoServicoMunicipio' => '06815',
                'ufPrestacaoServico' => 'SP',
                'municipioPrestacaoServico' => 'São Paulo'
			),
            'valorTotal' => $finalValue,
            'cliente' => array(
                'tipoPessoa' => $tipoPessoa,
                'nome' => $infoNota['nomeCliente'],
                'email' => $infoNota['emailCliente'],
                'cpfCnpj' => $infoNota['cpfCliente'],
                'telefone' => $infoNota['telCliente'],

            )
        );

        if ($infoNota['uf']) {
            $params['cliente']['endereco'] = array(
                'pais' => 'Brasil',
                'uf' => $infoNota['uf'],
                'cidade' => $infoNota['cidade'],
                'logradouro' => $infoNota['logradouro'],
                'numero' => $infoNota['numero'],
                'complemento' => $infoNota['complemento'],
                'bairro' => $infoNota['bairro'],
                'cep' => $infoNota['cep']
            );
        }

        $result = eNotasGW::$NFeApi->emitir($this->empresaId, $params);

        return $result;
    }

}
