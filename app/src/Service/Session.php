<?php

namespace IntecPhp\Service;

use Intec\Session\Session as SessionHandler;

class Session
{
    private function __construct()
    {
    }

    public static function get(string $key)
    {
        $session = self::getInstance();
        if ($session->exists($key)) {
            return $session->get($key);
        }

        return false;
    }

    public static function set(string $key, $value)
    {
        $session = self::getInstance();
        $session->set($key, $value);
    }

    public static function unset($key)
    {
        $session = self::getInstance();

        if ($session->exists($key)) {
            return $session->unset($key);
        }

        return false;
    }

    private static function getInstance()
    {
        if (session_status() !== PHP_SESSION_ACTIVE) {
            ini_set('session.gc_maxlifetime', 15552000);
            ini_set('session.cookie_lifetime', 15552000);
        }

        return SessionHandler::getInstance();
    }

    public static function clear()
    {
        SessionHandler::start();
        session_unset();
    }
}
