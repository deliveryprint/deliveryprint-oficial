var verificationApp = new Vue({
    el: "#verification-app",
    data: {
        loading: true
    },
    methods: {
        checkVerification() {
            $.get(
                "/checkVerification",
                null,
                "json"
            ).then(
                response => {
                    if (!response.data.needVerification) {
                        window.location.href = '/meus-pedidos'
                    }
                    this.loading = false;
                }
            );
        },
        requestResend() {
            $.get(
                "/resendVerificationEmail",
                null,
                "json"
            ).then(
                r => {
                    PrettyAlerts.show({
                        type: "success",
                        dismissable: true,
                        message: 'O e-mail de verificação foi reenviado. Por favor, confira sua caixa de entrada.'
                    });
                }
            );
        }
    },
    created() {
        this.checkVerification();
    }
})
