import { required, minLength } from 'vuelidate/lib/validators'

var cf = require("./lib/CurrencyFormatter");

var interestRate = 0.023;

$(document).ready(function () {
    $(".fa-info-circle")
        .popover({
            placement: "right",
            html: true,
            delay: { "show": 300, "hide": 100 },
            content: '<p>Encontra-se no verso do seu cartão (3 ou 4 digitos)</p> <div><img src="/img/CVV.svg" alt="frente e verso" style="height: 50px;"></div>'
        });
});
/* change icon colors */
$('.ipts-control input').on('focus', function () {
    $(this).parent().find('i').css('color', '#fc4a1a');
});
$('.ipts-control input').on('blur', function () {
    $(this).parent().find('i').css('color', '#a8a8a8');
});

var paymentBlockedModal = new Vue({
    el: "#paymentBlockedModal",
    data: {
        id: ""
    },
    methods: {
        closeModal: function() {
            $('#paymentBlockedModal').modal('hide');
        }
    }
});

var shopData = {
    user: {
        birth: null,
        cel: null,
        tel: null,
        email: null,
        cpf_cnpj: null,
        fullName: null,
        addrId: null,
        zipCode: null,
        street: null,
        number: null,
        district: null,
        city: null,
        state: null,
        complement: null,
        cardBanner: null,
        endCardNumber: null
    },
    order: {},
    card: {
        number: "",
        expiration: "",
        verificationValue: "",
        fullName: ""
    },
    items: [],
    resumo: [],
    endereco: {},
    iuguConfig: [],
    paymentType: 'option1',
    blockProceed: false,
    blockZipSearch: false,
    incompleteAddr: false,
    isResumoOpen: false,
    installments: 1,
    loading: false,
    reCaptchaEnable: false,
    reCaptchaId: '6Le9nrAUAAAAAI-GEekrIsM-eCgF5bDEsXEULLQ9',
    reCaptchaResponse: ''
}

function isNotBlankSpace(value) {
    return value != "";
}

var PageLoader = require('./components/shared/PageLoader.vue');
var OrderSummary = require('./components/OrderSummary.vue');

var vueExampleApp = new Vue({
    el: "#checkout",
    data: shopData,
    components: {
        'page-loader': PageLoader,
        'order-summary': OrderSummary
    },
    validations: {
        card: {
            number: {
                required,
                minLength: minLength(19) // Contando os espaços também
            },
            fullName: {
                required,
                checkName: name => {
                    if (name == "") return true;
                    var arrName = name.split(" ").filter(isNotBlankSpace);
                    return (arrName.length > 1);
                }
            },
            expiration: {
                required,
                checkDate: value => {
                    if (value == "") return true;
                    if (value.length < 5) return false;
                    var strCurrDate = moment().format('MM/YY');
                    var currDate = moment(strCurrDate, 'MM/YY');
                    var expDate = moment(value, 'MM/YY');
                    if (!expDate.isValid()) return false;

                    return (expDate >= currDate);
                }
            },
            verificationValue: {
                required,
                minLength: minLength(3)
            }
        }
    },
    methods: {
        status(validation) {
            return {
                invalid: validation.$error,
            }
        },
        searchZip() {

            if (this.blockZipSearch) {
                return;
            }

            var zip = this.user.zipCode.replace(/\D/g, '');
            if (zip.length < 8)
                return;

            $.get("https://apps.widenet.com.br/busca-cep/api/cep.json", { code: zip }, null).then(
                result => {
                    if (parseInt(result.status) !== 1) {
                        PrettyAlerts.show({
                            type: 'danger',
                            dismissable: true,
                            message: result.message
                        });
                        return;
                    }

                    this.user.street = result.address;
                    this.user.district = result.district;
                    this.user.city = result.city;
                    this.user.state = result.state;

                    if (this.user.street == "" || this.user.district == "") {
                        this.incompleteAddr = true;
                    } else {
                        this.incompleteAddr = false;
                    }
                });
        },
        closeResumo: function () {
            this.isResumoOpen ? $('.fa-angle-up').removeClass('fa-rotate') : $('.fa-angle-up').addClass('fa-rotate');
            this.isResumoOpen = !this.isResumoOpen;
        },
        countPages(index) {
            var totalPages = 0;
            this.items[index].files.forEach(f => {
                totalPages += (parseInt(f.numPaginas) * parseInt(f.qtdImpressoes));
            });

            return totalPages;
        },
        createPayment: function () {
            this.blockProceed = true;
            this.loading = true;

            // Testar se o usuario passou no recaptcha
            if (this.reCaptchaEnable) {
                this.reCaptchaResponse = grecaptcha.getResponse();
                if (!this.reCaptchaResponse) {
                    PrettyAlerts.show({
                        type: 'danger',
                        dismissable: true,
                        timed: true,
                        timeout: 10000,
                        message: 'Responda o ReCaptcha para continuar.'
                    });
                    this.blockProceed = false;
                    this.loading = false;
                    return;
                }
            }

            var url_success = window.location.pathname === '/segundo-pagamento' ? '/segundo-pagamento-finalizado' : "/pedido-finalizado";
            if (vueExampleApp.paymentType == "option1") {
                /*** Pagamento por boleto bancário ***/
                $.post('/bankSlipPayment', {
                    'userInfo': this.user,
                    'g-recaptcha-response': this.reCaptchaResponse
                }, null, 'json').then(
                    response => {
                        Cookies.remove("coupon");
                        Cookies.remove("emailMktId");
                        Cookies.set("boleto", "true");
                        Cookies.set("promotion", "false");
                        Cookies.set("sendToGa", "true");
                        var order = response.data.order;
                        window.location.href = url_success;
                    },
                    err => {
                        if (err.responseJSON.message == 'blocked') {
                            paymentBlockedModal.id = err.responseJSON.data.id;
                            vueExampleApp.blockProceed = false;
                            vueExampleApp.loading = false;
                            $('#paymentBlockedModal').modal('show');
                        } else if (err.responseJSON.message == "Por favor, confirme seu endereço de e-mail. Código: 0") {
                            window.location.href = '/verificacao-necessaria';
                            vueExampleApp.blockProceed = false;
                            vueExampleApp.loading = false;
                        } else {
                            PrettyAlerts.show({
                                type: 'danger',
                                dismissable: true,
                                timed: true,
                                timeout: 10000,
                                message: err.responseJSON.message
                            });
                            vueExampleApp.blockProceed = false;
                            vueExampleApp.loading = false;
                        }
                    }
                )

            } else {
                /*** Pagamento por cartão de crédito ***/

                // Definindo o ID da conta do Iugu
                Iugu.setAccountID(vueExampleApp.iuguConfig.iuguAccountId);
                Iugu.setTestMode(vueExampleApp.iuguConfig.iuguCardTest);

                // Formatando as informações do cartão
                var info = this.formatCardInfo();
                // Criando o objeto do cartão de crédito

                var cc = Iugu.CreditCard(info["cNumber"], info["exMonth"], info["exYear"], info["fName"], info["lName"], info["cVerification"]);
                var tokenResponseHandler = function (data) {
                    if (data.errors) {
                        PrettyAlerts.show({
                            type: 'danger',
                            dismissable: true,
                            message: 'Verifique se os dados do cartão estão corretos e tente novamente'
                        });
                        vueExampleApp.blockProceed = false;
                        vueExampleApp.loading = false;
                    } else {
                        var paymentToken = data.id.split('-').join('');
                        vueExampleApp.user.cardBanner = Iugu.utils.getBrandByCreditCardNumber(info["cNumber"]);
                        vueExampleApp.user.endCardNumber = info["cNumber"].substring(12, 16);

                        $.post('/creditCardPayment', {
                            paymentToken: paymentToken,
                            userInfo: vueExampleApp.user,
                            installments: vueExampleApp.installments,
                            'g-recaptcha-response': vueExampleApp.reCaptchaResponse
                        }, null, 'json').then(
                            response => {
                                Cookies.remove("coupon");
                                Cookies.remove("emailMktId");
                                Cookies.set("boleto", "false");
                                Cookies.set("promotion", "false");
                                Cookies.set("sendToGa", "true");
                                var order = response.data.order;
                                window.location.href = url_success;
                            },
                            err => {
                                if (err.responseJSON.message == 'blocked') {
                                    paymentBlockedModal.id = err.responseJSON.data.id;
                                    $('#paymentBlockedModal').modal('show');
                                    vueExampleApp.blockProceed = false;
                                    vueExampleApp.loading = false;
                                } else if (err.responseJSON.message == "Por favor, confirme seu endereço de e-mail. Código: 0") {
                                    window.location.href = '/verificacao-necessaria';
                                    vueExampleApp.blockProceed = false;
                                    vueExampleApp.loading = false;
                                } else {
                                    PrettyAlerts.show({
                                        type: 'danger',
                                        dismissable: true,
                                        message: err.responseJSON.message
                                    });
                                    vueExampleApp.blockProceed = false;
                                    vueExampleApp.loading = false;
                                }
                            }
                        )

                    }
                }
                Iugu.createPaymentToken(cc, tokenResponseHandler);
            }
        },
        formatCardInfo: function () {
            var cNumber = "";
            var exMonth = "";
            var exYear = "";
            var fName = "";
            var lName = "";
            var cVerification = "";

            // Número do cartão
            cNumber = this.card.number.replace(/ /g, '');
            // Dividindo a data de expiração em mês e ano
            var arrExpiration = this.card.expiration.split("/");
            if (arrExpiration.length > 1) {
                exMonth = arrExpiration[0];
                exYear = arrExpiration[1];
            }
            // Dividindo o nome do usuário em nome e sobrenome
            var arrFullName = this.card.fullName.split(" ").filter(isNotBlankSpace);
            if (arrFullName.length > 1) {
                fName = arrFullName[0];
                lName = arrFullName[arrFullName.length - 1];
            }
            // Código de verificação do cartão
            cVerification = this.card.verificationValue;

            var info = {
                cNumber: cNumber,
                exMonth: exMonth,
                exYear: exYear,
                fName: fName,
                lName: lName,
                cVerification: cVerification
            };

            return info;
        },
        getMaxInstallments: function (value) {
            if (value < 100.00) return 1;

            return 3;
        },
        getOptObj: function (value, text) {
            return {
                value: value,
                text: text
            };
        }
    },
    computed: {
        totalItems: function () {
            var result = 0;
            this.items.forEach(function (item) {
                result += parseFloat(item.preco);
            });
            return result;
        },
        itemsDiscount: function () {
            var result = 0;
            this.items.forEach(function (item) {
                result += parseFloat(item.descTotal);
            });
            return result;
        },
        orderDiscount: function () {
            var result = 0;
            if (this.order.discountValue !== null) {
                if (this.order.discountType === 'PERCENT') {
                    result = this.totalItems * (this.order.discountValue / 100);
                } else {
                    if (this.order.discountValue > 7) {
                        result = Math.min(this.totalItems * 0.5, this.order.discountValue);
                    } else {
                        result = parseFloat(this.order.discountValue);
                    }
                }
            }
            return result;
        },
        totalDiscount: function () {
            return this.itemsDiscount + this.orderDiscount;
        },
        totalNoDiscount: function () {
            return this.totalItems + this.itemsDiscount;
        },
        vfinal: function () {
            return this.totalItems - this.orderDiscount;
        },
        generateInstallmentsOpt: function () {
            var opts = [];
            var maxInstallments = this.getMaxInstallments(this.vfinal);
            if (maxInstallments < 2) return;
            var valueWithInterest = this.vfinal * (1 + interestRate);
            for (i = 2; i <= maxInstallments; i++) {
                var installmentValue = (valueWithInterest / i);
                opts.push(this.getOptObj(i, i + 'x de ' + cf.floatToCurrency(installmentValue) + ' com juros'));
            }

            return opts;
        }
    },
    created: function () {
        this.loading = true;
        this.blockProceed = true;

        $.get({
            url: "/checkCaptcha",
            dataType: "json",
            success: function (response) {
                vueExampleApp.reCaptchaEnable = response.data.check;
                vueExampleApp.blockProceed = false;
            },
            error: function (err) {
                PrettyAlerts.show({
                    type: 'danger',
                    dismissable: true,
                    message: err.responseJSON.message
                });
            }
        });

        $.get({
            url: "/getCartData",
            dataType: "json",
            success: function (response) {
                vueExampleApp.order = response.data.pedido;
                vueExampleApp.items = response.data.items;
                vueExampleApp.endereco = response.data.endereco;
                vueExampleApp.resumo = response.data.resumo;
                vueExampleApp.iuguConfig = response.data.iuguConfig;

                $.get({
                    url: '/getCustomerData',
                    dataType: 'json',
                    success: function (response) {
                        var d = response.data;
                        var u = d.user;

                        vueExampleApp.user.email = u.email;
                        vueExampleApp.user.cpf_cnpj = u.cpf;
                        vueExampleApp.user.fullName = u.name;
                        vueExampleApp.user.birth = u.birth;
                        vueExampleApp.user.cel = u.cel;
                        vueExampleApp.user.tel = u.tel;

                        if (d.address.length > 0 && d.address[0].number) {
                            vueExampleApp.blockZipSearch = true;
                            var laddr = d.address[0];
                            // vueExampleApp.user.addrId = laddr.addrId;
                            vueExampleApp.user.zipCode = laddr.zip;
                            vueExampleApp.user.street = laddr.street;
                            vueExampleApp.user.number = laddr.number;
                            vueExampleApp.user.district = laddr.district;
                            vueExampleApp.user.city = laddr.city;
                            vueExampleApp.user.state = laddr.uf,
                                vueExampleApp.user.complement = laddr.compl
                        } else {
                            vueExampleApp.user.zipCode = '05409-012';
                            vueExampleApp.user.street = 'Rua Oscar Freire';
                            vueExampleApp.user.number = 2617;
                            vueExampleApp.user.district = 'Pinheiros';
                            vueExampleApp.user.city = 'São Paulo';
                            vueExampleApp.user.state = 'SP'
                        }

                        if (vueExampleApp.vfinal < 0.05) {

                            // Pedido gratuito

                            $.post('/promotionPayment', {
                                userInfo: vueExampleApp.user
                            }, null, 'json').then(
                                response => {
                                    Cookies.remove("coupon");
                                    Cookies.remove("emailMktId");
                                    Cookies.set("promotion", "true");
                                    Cookies.set("boleto", "false");
                                    var order = response.data.order;
                                    window.location.href = "/pedido-finalizado";
                                },
                                err => {
                                    vueExampleApp.loading = false;
                                    PrettyAlerts.show({
                                        type: 'danger',
                                        dismissable: true,
                                        timed: true,
                                        timeout: 10000,
                                        message: err.responseJSON.message
                                    });
                                }
                            )
                        } else {
                            vueExampleApp.loading = false;
                        }
                    },
                    error: function (err) {
                        vueExampleApp.loading = false;
                        PrettyAlerts.show({
                            type: 'danger',
                            dismissable: true,
                            message: err.responseJSON.message
                        });
                    }
                });

            },
            error: function (err) {
                vueExampleApp.loading = false;
                PrettyAlerts.show({
                    type: 'danger',
                    dismissable: true,
                    message: err.responseJSON.message
                });
            }
        });
    }
});
