var invitationApp = new Vue({
    el: "#invitationApp",
    data: {
        logged: false,
        inviteCode: ''
    },
    methods: {
        tryToLogin() {
            Login.setRedirectUrl('indique-a-deliveryprint');
            Login.openRegisterModal();
        },
        getInviteCode() {
            $.get({
                url: "/getInviteCodeByUser",
                dataType: "json",
                success: function (response) {
                    invitationApp.inviteCode = response.data.codigo;
                },
                error: function (err) {
                    PrettyAlerts.show({
                        type: "danger",
                        dismissable: true,
                        message: err.responseJSON.message
                    });
                }
            });
        },
        copyUrl() {
            var copyText = document.getElementById("inviteUrlInput");

            /* Select the text field */
            copyText.select();
            copyText.setSelectionRange(0, 99999); /*For mobile devices*/

            /* Copy the text inside the text field */
            document.execCommand("copy");

            PrettyAlerts.show({
                type: "info",
                dismissable: true,
                message: "Link copiado!"
            });
        },
        shareOnFacebook() {

        }
    },
    computed: {
        inviteUrl() {
            if (this.inviteCode)
                return window.location.href.replace("/indique-a-deliveryprint", "") + '/impressao-online?cupom=' + this.inviteCode
            else
                return ''
        }
    },
    created: function () {
        if (this.logged = Login.isLoggedIn()) {
            this.getInviteCode();
        }
    }
});
