var data = {
    user_email: "",
    user_name : "",
    user_tel  : "",
    subject   : null,
};

var eMail = new Vue({
    el: '#email_form',
    data: data,
    methods: {
        sendEmail:function(){
            $.post('/budget/request', data, null, 'json').then(
                res=>{
                    PrettyAlerts.show({
                        type: 'info',
                        dismissable: true,
                        message: res.message
                    });
                },
                err=>{
                    PrettyAlerts.show({
                        type: 'danger',
                        dismissable: true,
                        message: err.responseJSON.message
                    });
                }
            )
        }
    },
    computed: {

    },
    created: function(){

    }
});
