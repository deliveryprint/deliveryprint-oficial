$(document).ready(function () {

    $('#section-resultado').hide();

    $('#scrollBtn').smoothScroll({
        speed: 1200
    });

    $('#btn-contato').click(function () {
        let name = $('#name').val()
        let email = $('#email').val()
        let pages = $('#pages').val()
        let color = $('[name=color]:checked').val()
        let city = $('#city').val()
        $.post('/emailCampanhaEntrega', {
            name: name,
            email: email,
            pages: pages,
            color: color,
            city: city
        }, null, 'json').then(r => {
            $('#main').fadeOut();
            $('#intro-line').fadeOut();
            $('#section-resultado').fadeIn();
        });
    });
})
