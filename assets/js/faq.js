$(document).ready(function () {
    $('#btn-hiw-coupon').val('COMOFUNCIONA10');
    $('#btn-hiw-coupon').on("click", function () {
        Cookies.set("coupon", $(this).val()) ?
            location.href = "/impressao-online" : false;
    });
});

var Faq = require('./components/Faq.vue');

var FaqApp = new Vue({
    el: "#faq",
    components: {
        'faq': Faq
    },
    methods: {
        openChat() {
            jivo_api.open();
        }
    }
});
