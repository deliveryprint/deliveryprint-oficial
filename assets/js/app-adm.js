global.jQuery = global.$ = require('jquery');
require('bootstrap');
AjaxForm = require('./lib/AjaxForm');
//FormValidator = require('./lib/FormValidator');
//global.FormFiller = require('./lib/FormFiller');
global.PrettyAlerts = require('./lib/PrettyAlerts');
LaLogin = require('./lib/LaLogin');
global.moment = require('moment');

global.Vue = require("vue/dist/vue.common");
require('./lib/VueFilters');
global.Cookies = require('js-cookie');

VueTheMask = require('vue-the-mask');
import Vuelidate from 'vuelidate'

Vue.use(VueTheMask);
Vue.use(Vuelidate);

(function($) {
    PrettyAlerts.init();
    AjaxForm.init('.intec-ajax-form');
    //FormValidator.init('.intec-form-validator');
    LaLogin.init();

})(jQuery);


$(document).ready(function(){
    $('body').on('click touchstart', '.close', function(e) {
        return $(this).parents('.popover').remove();
    });

    // Funcionalidade do menu
    $('.navbar-toggler').click(function() {
        $('body').toggleClass('sidebar-collapse');
    });
});

global.debounce = function(func, wait, immediate) {
    var timeout;
    return function() {
        var context = this,
            args = arguments;
        var later = function() {
            timeout = null;
            if (!immediate) func.apply(context, args);
        };
        var callNow = immediate && !timeout;
        clearTimeout(timeout);
        timeout = setTimeout(later, wait);
        if (callNow) func.apply(context, args);
    };
};
