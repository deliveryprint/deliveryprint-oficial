$(document).ready(function(){

    if($('#lp').hasClass("lp")){
        $('#how_it_works, .solicitation-fix, .hide-m')
        .removeClass('d-flex').addClass('d-none');
        $('header img.logo').removeClass('m-3').addClass('mt-1');
        $('header').addClass('d-none');
        $('#contacts').remove();
    }

});

var data = {
    user_email: "",
    user_name : "",
    description  : "",
    subject   : null,
};

var eMail = new Vue({
    el: '#email_form',
    data: data,
    methods: {
        sendEmail:function(){
            $.post('/plottBudget/request', data, null, 'json').then(
                res=>{
                    PrettyAlerts.show({
                        type: 'info',
                        dismissable: true,
                        message: res.message
                    });
                },
                err=>{
                    PrettyAlerts.show({
                        type: 'danger',
                        dismissable: true,
                        message: err.responseJSON.message
                    });
                }
            )
        }
    },
    computed: {

    },
    created: function(){

    }
});

var linkBuilding = {
    op: false,
    lo: false,
    gr: false
};

var linksVue = new Vue ({
    el: "#linkBuilding",
    data: linkBuilding,
    methods: {

    },
});
