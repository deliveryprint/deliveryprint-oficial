$(document).ready(function(){

    if($('#orc').hasClass("orc")){
        $('#how_it_works, .solicitation-fix, .hide-m')
        .removeClass('d-flex').addClass('d-none');
    }

});

var data = {
    user_email: "",
    user_name : "",
    description  : "",
    subject   : "Orçamento para Impressão",
};

var eMail = new Vue({
    el: '#email_form',
    data: data,
    methods: {
        sendEmail:function(){
            $.post('/plottBudget/request', data, null, 'json').then(
                res=>{
                    PrettyAlerts.show({
                        type: 'info',
                        dismissable: true,
                        message: res.message
                    });
                },
                err=>{
                    PrettyAlerts.show({
                        type: 'danger',
                        dismissable: true,
                        message: err.responseJSON.message
                    });
                }
            )
        }
    },
    computed: {

    },
    created: function(){

    }
});
