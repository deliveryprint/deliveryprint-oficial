var shippingPricesApp = new Vue({
    el: '#shippingPricesApp',
    data: {
        prices: {}
    },
    methods: {
        updateDeliveryPrices: function () {
            $.post(
                '/updateDeliveryShippingPrices',
                {
                    dExpress1Price: this.prices.dExpress1Price,
                    dExpress2Price: this.prices.dExpress2Price,
                    dExpress3Price: this.prices.dExpress3Price,
                    dExpress4Price: this.prices.dExpress4Price,
                    dExpress4AdditionalPrice: this.prices.dExpress4AdditionalPrice,
                    dExpress5Price: this.prices.dExpress5Price
                },
                null,
                'json'
            ).then(
                r => {
                    PrettyAlerts.show({
                        type: "info",
                        dismissable: true,
                        message: 'Preços atualizados!'
                    });
                },
                err => {
                    PrettyAlerts.show({
                        type: "danger",
                        dismissable: true,
                        message: err.responseJSON.message
                    });
                }
            );
        },
        updateWithdrawalPrices: function () {
            $.post(
                '/updateWithdrawShippingPrices',
                {
                    wExpress1Price: this.prices.wExpress1Price,
                    wExpress2Price: this.prices.wExpress2Price
                },
                null,
                'json'
            ).then(
                r => {
                    PrettyAlerts.show({
                        type: "info",
                        dismissable: true,
                        message: 'Preços atualizados!'
                    });
                },
                err => {
                    PrettyAlerts.show({
                        type: "danger",
                        dismissable: true,
                        message: err.responseJSON.message
                    });
                }
            );
        }
    },
    created: function () {
        $.get('/getShippingPrices', null, null, 'json').then(r => {
            this.prices = r.data.prices;
        }, err => {
            PrettyAlerts.show({
                type: "danger",
                dismissable: true,
                message: err.responseJSON.message
            });
        });
    }
});
