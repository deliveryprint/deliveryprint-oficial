require("./lib/deprecated/fileinput");
require("bootstrap-fileinput/js/locales/pt-BR");
require("bootstrap-fileinput/themes/fa/theme.js");

const pdfjsLib = require("pdfjs-dist");
//pdfjsLib.workerSrc = 'https://npmcdn.com/pdfjs-dist@2.0.943/build/pdf.worker.js';
const PriceCalculator = require("./lib/PriceCalculator");

const priceCalculator = new PriceCalculator();
//priceCalculator.setDebugMode(true);
var callbackStack = [];
var msgInstance = null;
var previousBatchLength = 0;
var previousBatchLengthAfter = 0;
var aux = 0;

$(document).ready(function () {
    $(".popover")
        .popover({
            placement: "right",
            html: true,
            content: $(".popover").html()
        })
        .popover("show");

    /**
     * Controle da seleção de opções
     */
    $("#collapseOne, #collapseTwo, #collapseThree, #collapseFour, #collapseFive, #collapseSix")
        .on("mousemove shown.bs.collapse", function () {
            if ($("#accordion .card").find(".active")) {
                /**
                 * @ac Catch da parent div correta à aquela
                 * que possui a classe .active
                 */
                ac = $("#accordion .card")
                    .find(".active")
                    .parents(".collapse")
                    .parent();
                /**
                 * Procura a children .card-header para alterar
                 * a cor do texto do header para #FC4A1A.
                 */
                $(ac)
                    .find(".card-header")
                    .children()
                    .css("color", "#FC4A1A");
                /**
                 * Torna o próximo collapsable para permitir a volta.
                 */
            }
        });
    // Fim Controle de Seleção de Opções.
    var yPos;
    $(window).on("scroll", function () {
        /**
         * Resumo þedido fixed
         *
         * @yPos Posição do scrollTop
         */
        yPos = $(this).scrollTop();
        /**
         * @r HTML Object
         */
        var r = $(".div-resumo");

        var h = $(".resumo-lateral");
        /**
         * @d Distância entre o topo da página e o objeto
         * resumo.
         */
        var d = $("header").innerHeight() + $("nav").innerHeight();
        /**
         * @o Comprimento da distância entre o topo da página e
         * a diferença entre o tamanho do objeto resumo X container.
         */
        var o = $("#orderApp").innerHeight() - $(h).innerHeight();
        /**
         * @t Distância total do topo da página para o travamento no rodapé.
         */
        var t = o + d - 50;

        if ($(window).innerWidth() > 900) {
            if (yPos > d && yPos <= t) {
                r.css("top", yPos - d + "px");
            } else if (yPos >= t - d) {
                r.css("top", o - 62 + "px");
            } else if (yPos <= d) {
                r.css("top", 0);
            }
        } else {
            r.removeAttr("style");
        }
    });

    $("#collapseSix").on("show.bs.collapse", function () {
        if (callbackStack.length > 0) {
            msgInstance = PrettyAlerts.show({
                type: "info",
                dismissable: false,
                message: "Contando páginas. Aguarde por favor ...",
                timed: false
            });
        }

        orderApp.showIncompleteMessage();
    });

    var $body = $("body");
    $body.on("hide.bs.modal", "#modalDesconto", function () {
        var $p = $('#anexoModal, #anexoModal2, #modalEncadernacao, #fileInfoApp');
        if ($p.hasClass('show')) {
            setTimeout(function () {
                $body.addClass("modal-open");
            }, 500);
        }
    });

    $("#modalEncadernacao").on("hidden.bs.modal", function (e) {
        if (!orderApp.binding.border && orderApp.binding.total < 1) {
            orderApp.curFinishing = {
                id: null,
                description: null
            };
        }
    });

    setTimeout(function () {
        $(document).mousemove(function (event) {
            if (!orderApp.pricesShown()) return;
            if (exitModal.firstTime && !orderApp.hasCoupon() && event.clientY <= 20) {
                exitModal.firstTime = false;
                exitModal.showExitModal();
            }
        });
    }, 5000);


    $('#modalDesconto').on('shown.bs.modal', function () {
        $('.modal-backdrop').css('background-color', '#fd9275').css('opacity', 1);
    });


    $('#collapseFive').on('shown.bs.collapse', function () {
        orderApp.stepTwo();
    });

});
var messages = {
    side: "Escolha primeiro a cor!",
    paper: "Escolha primeiro o lado!",
    finish: "Escolha primeiro o papel!",
    files: "Escolha primeiro o acabamento!",
    freight: "Informe primeiro a quantidade de páginas!"
};
var enabledItems = {
    side: false,
    paper: false,
    finish: false,
    files: false
};

var incompleteMsgApp = new Vue({
    el: '#incompleteMsgApp',
    data: {
        msg: null
    }
});

var FreightBoxList = require('./components/FreightBoxList.vue');
var PageLoader = require('./components/shared/PageLoader.vue');
import tutorialMixin from './mixins/tutorialMixin.js';

var orderData = {
    items: [],
    colors: [],
    sides: [],
    visiblePaperTypes: [],
    paperTypes: [],
    visibleFinishingTypes: [],
    finishingTypes: [],
    delivery: null,
    zip: "",
    shippingResult: false,
    loadingFreights: false,
    zipResult: false,
    calculatingZip: false,
    totalPages: 0,
    curColor: {
        id: null,
        hasColor: false,
        description: null
    },
    curSide: {
        id: null,
        enabled: false,
        description: null
    },
    curPaper: {
        id: null,
        enabled: false,
        description: null
    },
    curFinishing: {
        id: null,
        enabled: false,
        description: null
    },
    curFreight: {
        id: null,
        name: null,
        date: null,
        time: null
    },
    curPrice: null,
    curPriceNoFreight: null,
    curPriceNoDiscount: null,
    curPriceNoFreigtNoFinishing: null,
    totalDiscount: null,
    binding: {
        border: null,
        total: null
    },
    finishingDetails: null,
    addressLine1: null,
    addressLine2: null,
    files: [],
    availableDeliveryTypes: [],
    availableWithdrawalTypes: [],
    coupon: "",
    couponName: "",
    emailMktId: null,
    wrongCoupon: false,
    prevDeliverySelected: null,
    redirectUrl: "",
    isAttachment: false,
    isManual: false,
    isDisabled: false,
    mouseOver: false,
    dgPercent: 0,
    freightFallback: false,
    addingToCart: false,
    priceBkMin: 0,
    priceBkMax: 0,
    priceColorMin: 0,
    priceColorMax: 0
};

var orderApp = new Vue({
    el: "#orderApp",
    data: orderData,
    components: {
        'freight-box-list': FreightBoxList,
        'page-loader': PageLoader
    },
    mixins: [ tutorialMixin ],
    computed: {
        isNotComplete() {
            if (
                this.curColor.id > 0 &&
                this.curSide.id > 0 &&
                this.curPaper.id > 0 &&
                this.curFinishing.id > 0 &&
                this.totalPages > 0 &&
                this.curFreight.id > 0
            ) {
                return false;
            }

            return true;
        },
        showFreight: function () {
            if (
                this.totalPages > 0 &&
                this.curPaper.id > 0 &&
                this.curSide.id > 0
            ) {
                return true;
            }

            return false;
        }
    },
    methods: {
        doScroll: function (divBefore) {
            if (window.matchMedia("(max-height: 960px)").matches) {
                scrollTo(divBefore);
            }
        },
        hasCoupon() {
            return this.couponName.length > 0;
        },
        addCoupon() {
            if (this.coupon.length < 1) {
                return;
            }

            $('#spinnerModal').modal('show');

            $.post(
                "/checkCoupon",
                {
                    couponCode: this.coupon
                },
                null,
                "json"
            ).then(
                r => {
                    // Event tracking analytics
                    if (window.ga) {
                        ga('send', {
                            hitType: 'event',
                            eventCategory: 'Cupom',
                            eventAction: 'click',
                            eventLabel: 'Cupom'
                        });
                    }

                    var d = r.data;
                    this.couponName = d.codigo;
                    Cookies.set("coupon", this.couponName);
                    if (d.type === "ABS") {
                        priceCalculator.setDcMaxValor(d.value);
                        priceCalculator.setDcPecent(0);
                        Cookies.remove("coupon");
                        Cookies.remove("emailMktId");
                    } else {
                        priceCalculator.setDcMaxValor(0);
                        priceCalculator.setDcPecent(d.value);
                    }

                    this.curPriceNoFreight = priceCalculator.calculateWithoutFreight();
                    this.curPriceNoFreigtNoFinishing = priceCalculator.calculateWithoutFreightAndFinishing();
                    if (this.showFreight) {
                        this.curPrice = priceCalculator.calculate();
                        this.curPriceNoDiscount = priceCalculator.getPi();
                        this.totalDiscount = priceCalculator.getDt();
                    }

                    this.wrongCoupon = false;
                },
                r => {
                    this.wrongCoupon = true;
                    PrettyAlerts.show({
                        type: "danger",
                        dismissable: true,
                        message: r.responseJSON.message
                    });
                }
            ).then(
                r => {
                    $('#spinnerModal').modal('hide');
                }
            );
        },
        removeCoupon() {
            this.coupon = "";
            this.couponName = "";
            this.emailMktId = null;
            Cookies.remove("coupon");
            Cookies.remove("emailMktId");
            this.wrongCoupon = false;
            priceCalculator.setDcMaxValor(0);
            priceCalculator.setDcPecent(0);

            this.curPriceNoFreight = priceCalculator.calculateWithoutFreight();
            this.curPriceNoFreigtNoFinishing = priceCalculator.calculateWithoutFreightAndFinishing();
            if (this.showFreight) {
                this.curPrice = priceCalculator.calculate();
                this.curPriceNoDiscount = priceCalculator.getPi();
                this.totalDiscount = priceCalculator.getDt();
            }
        },
        toggleVisiblePaperTypes: function () {
            if (this.visiblePaperTypes.length === 3) {
                this.visiblePaperTypes = this.paperTypes;
            } else {
                this.visiblePaperTypes = this.getFirst3(this.paperTypes);
            }
        },
        toggleVisibleFinishingTypes: function () {
            if (this.visibleFinishingTypes.length === 3) {
                this.visibleFinishingTypes = this.finishingTypes;
            } else {
                this.visibleFinishingTypes = this.getFirst3(
                    this.finishingTypes
                );
            }
        },
        getFirst3(arr) {
            return arr.slice(0, 3);
        },
        calculateShipping() {
            calculateShipping();
        },
        calculateShippingNoDelay() {
            calcShippingMinDelay();
        },
        resetFreight(isManual = false) {
            this.curFreight = {
                id: null,
                name: null,
                date: null,
                time: null
            };

            this.shippingResult = false;
            this.loadingFreights = false;
            this.zipResult = false;
            this.calculatingZip = false;
            this.curPrice = 0;

            var tempDelivery = Cookies.get("delivery");
            if (tempDelivery !== undefined) {
                this.delivery = tempDelivery;
            }

            if (this.delivery == 0) {
                this.zip = "";
                if (
                    this.showFreight &&
                    this.curFinishing.id > 0 &&
                    this.delivery !== null
                ) {
                    orderApp.curPriceNoFreight = priceCalculator.calculateWithoutFreight();
                    orderApp.curPriceNoFreigtNoFinishing = priceCalculator.calculateWithoutFreightAndFinishing();

                    calculateWithdrawal(isManual);
                }
            } else {
                if (
                    this.showFreight &&
                    this.curFinishing.id > 0 &&
                    this.delivery !== null
                ) {
                    this.zip = Cookies.get("zip");
                    orderApp.curPriceNoFreight = priceCalculator.calculateWithoutFreight();
                    orderApp.curPriceNoFreigtNoFinishing = priceCalculator.calculateWithoutFreightAndFinishing();

                    calculateShipping();
                }
            }
        },
        changeFreight() {
            this.prevDeliverySelected = null;
            Cookies.set("delivery", this.delivery);
            this.resetFreight();
        },
        setDeliveryType(index) {
            // Event tracking analytics
            if (window.ga) {
                ga('send', {
                    hitType: 'event',
                    eventCategory: 'Frete e Preço',
                    eventAction: 'click',
                    eventLabel: 'Entrega' + (index + 1)
                });
            }

            var curDel = this.availableDeliveryTypes[index];

            this.curFreight.id = curDel.id;
            this.curFreight.name = curDel.name;
            this.curFreight.date = curDel.deliveryDate;
            this.curFreight.time = curDel.deliveryTime;

            priceCalculator.setPf(curDel.value);

            this.curPrice = priceCalculator.calculate();
            this.curPriceNoDiscount = priceCalculator.getPi();
            this.totalDiscount = priceCalculator.getDt();
            this.curPriceNoFreight = priceCalculator.calculateWithoutFreight();
            this.curPriceNoFreigtNoFinishing = priceCalculator.calculateWithoutFreightAndFinishing();
            // Cookies.set("prevDeliverySelected", this.prevDeliverySelected);

            this.toShoppingCart();
        },
        setWithdrawalType(index, isManual = false) {
            // Event tracking analytics
            if (window.ga) {
                ga('send', {
                    hitType: 'event',
                    eventCategory: 'Frete e Preço',
                    eventAction: 'click',
                    eventLabel: 'Retirada' + (index + 1)
                });
            }

            var curWit = this.availableWithdrawalTypes[index];

            this.curFreight.id = curWit.id;
            this.curFreight.name = curWit.name;
            this.curFreight.date = curWit.withdrawalDate;
            this.curFreight.time = curWit.withdrawalTime;

            priceCalculator.setPf(curWit.value);
            this.curPrice = priceCalculator.calculate();
            this.curPriceNoDiscount = priceCalculator.getPi();
            this.totalDiscount = priceCalculator.getDt();
            this.curPriceNoFreight = priceCalculator.calculateWithoutFreight();
            this.curPriceNoFreigtNoFinishing = priceCalculator.calculateWithoutFreightAndFinishing();

            this.prevDeliverySelected = curWit.id;

            // Cookies.set("prevDeliverySelected", this.prevDeliverySelected);

            this.toShoppingCart(isManual);
        },
        updateColor(index) {
            this.doScroll("#headingOne");
            this.curColor = this.colors[index];
            priceCalculator.setHasColor(this.curColor.hasColor);
            this.curPriceNoDiscount = priceCalculator.getPi();
            this.totalDiscount = priceCalculator.getDt();
            this.curPriceNoFreight = priceCalculator.calculateWithoutFreight();
            this.curPriceNoFreigtNoFinishing = priceCalculator.calculateWithoutFreightAndFinishing();
            enabledItems.side = true;
            $("#collapseTwo").collapse("show");
            // mudanca de cor não influencia no frete, se tudo ok calcula o preço
            if (!this.isNotComplete) {
                this.curPrice = priceCalculator.calculate();
            }

            // Event tracking analytics
            if (window.ga) {
                ga('send', {
                    hitType: 'event',
                    eventCategory: 'Cor',
                    eventAction: 'click',
                    eventLabel: this.curColor.description
                });
            }
        },
        updateSide(index) {
            if (enabledItems.side) {
                this.doScroll("#headingTwo");
                this.curSide = this.sides[index];
                priceCalculator.setIsFrontBack(this.curSide.id == 1);
                this.curPriceNoFreight = priceCalculator.calculateWithoutFreight();
                this.curPriceNoFreigtNoFinishing = priceCalculator.calculateWithoutFreightAndFinishing();
                this.resetFreight();
                enabledItems.paper = true;
                $("#collapseThree").collapse("show");

                // Event tracking analytics
                if (window.ga) {
                    ga('send', {
                        hitType: 'event',
                        eventCategory: 'Lado',
                        eventAction: 'click',
                        eventLabel: this.curSide.description
                    });
                }
            } else {
                this.showIncompleteMessage();
            }
        },
        updatePageType(index) {
            if (enabledItems.paper) {
                this.doScroll("#headingThree");
                this.curPaper = this.visiblePaperTypes[index];
                priceCalculator.setPaperId(this.curPaper.id);
                priceCalculator.setPPap(this.curPaper.value);
                this.curPriceNoFreight = priceCalculator.calculateWithoutFreight();
                this.curPriceNoFreigtNoFinishing = priceCalculator.calculateWithoutFreightAndFinishing();
                this.resetFreight();
                enabledItems.finish = true;
                $("#collapseFour").collapse("show");

                // Event tracking analytics
                if (window.ga) {
                    ga('send', {
                        hitType: 'event',
                        eventCategory: 'Papel',
                        eventAction: 'click',
                        eventLabel: this.curPaper.description
                    });
                }
            } else {
                this.showIncompleteMessage();
            }
        },
        updateFinishing(index) {
            if (enabledItems.finish) {
                this.curFinishing = this.visibleFinishingTypes[index];
                priceCalculator.setPa(this.curFinishing.value);
                enabledItems.files = true;

                if (this.curFinishing.hasBorder) {

                    // Configurar o bindingApp de acordo com o tipo de acabamento escolhido
                    bindingApp.step = 1;
                    if (index == 4 || index == 5) {
                        bindingApp.details = true;
                        bindingApp.totalSteps = 6
                    } else {
                        bindingApp.details = false;
                        bindingApp.totalSteps = 2
                    }

                    if (index == 2) {
                        bindingApp.observation = "*Cada encadernação suporta 300 folhas."
                    } else if (index == 3) {
                        bindingApp.observation = "*Cada encadernação suporta até 100 folhas (200 páginas frente e verso)"
                    } else if (index == 5) {
                        bindingApp.observation = "*Cada encadernação suporta 200 folhas."
                    } else {
                        bindingApp.observation = ""
                    }
                    $("#modalEncadernacao").modal("show");
                } else {
                    this.binding = {
                        border: null,
                        total: null
                    };
                    this.finishingDetails = null;
                    $("#collapseFive").collapse("show");
                }

                this.curPriceNoFreight = priceCalculator.calculateWithoutFreight();
                this.curPriceNoFreigtNoFinishing = priceCalculator.calculateWithoutFreightAndFinishing();
                this.resetFreight();

                // Event tracking analytics
                if (window.ga) {
                    ga('send', {
                        hitType: 'event',
                        eventCategory: 'Acabamento',
                        eventAction: 'click',
                        eventLabel: this.curFinishing.description
                    });
                }
            } else {
                this.showIncompleteMessage();
            }
        },
        isFileEnabled() {
            if (enabledItems.files) {
                $("#anexoModal").modal("show");

                // Event tracking analytics
                if (window.ga) {
                    ga('send', {
                        hitType: 'event',
                        eventCategory: 'Anexar',
                        eventAction: 'click',
                        eventLabel: 'Anexar arquivo'
                    });
                }
            } else {
                this.showIncompleteMessage();
            }
        },
        isManualEnabled() {
            if (enabledItems.files) {
                $('#fileInfoApp').modal('show');

                // Event tracking analytics
                if (window.ga) {
                    ga('send', {
                        hitType: 'event',
                        eventCategory: 'Anexar',
                        eventAction: 'click',
                        eventLabel: 'Informa manual'
                    });
                }
            } else {
                this.showIncompleteMessage();
            }
        },
        toShoppingCart(isManual = false) {
            this.redirectUrl = "/carrinho";
            if (!this.isFilesOk()) {
                this.isDisabled = false;
                PrettyAlerts.show({
                    type: "info",
                    message:
                        "Você deve informar " +
                        this.files.length +
                        " arquivo(s)"
                });
                $("#anexoModal2").modal("show");
                return;
            }

            if (!isManual) {
                this.addToShoppingCart();
            }
        },
        mobileToShoppingCart() {
            if (window.innerWidth < 548) {
                this.redirectUrl = "/carrinho";
                if (!this.isFilesOk()) {
                    this.isDisabled = false;
                    PrettyAlerts.show({
                        type: "info",
                        message: "Você deve informar " + this.files.length + " arquivo(s)"
                    });
                    $("#anexoModal2").modal("show");
                    return;
                }

                this.addToShoppingCart();
            }
        },
        newOrder() {
            this.redirectUrl = "/impressao-online";
            if (!this.isFilesOk()) {
                PrettyAlerts.show({
                    type: "info",
                    message: "Você deve informar " + this.files.length + " arquivo(s)"
                });
                $("#anexoModal2").modal("show");
                return;
            }
            this.addToShoppingCart();
        },
        isFilesOk() {
            this.filterFiles();
            return this.files.find(f => !f.name) === undefined;
        },
        filterFiles() {
            this.files = this.files.filter(function (el) {
                return el !== undefined;
            });
        },
        addToShoppingCart() {
            $(".submit-car").prop("disabled", true);
            this.addingToCart = true;
            return $
                .post(
                    "/makeOrder",
                    {
                        zip: this.zip,
                        items: [
                            {
                                color: this.curColor.id,
                                side: this.curSide.id,
                                paper: this.curPaper.id,
                                finishing: {
                                    id: this.curFinishing.id,
                                    binding: this.binding
                                },
                                finishingDetails: this.finishingDetails,
                                delivery: {
                                    isFreight: this.delivery,
                                    id: this.curFreight.id,
                                    fallback: this.freightFallback
                                },
                                cupomCode: this.couponName,
                                emailMktId: this.emailMktId,
                                files: this.files,
                                observation: vueObservationModal.observation
                            }
                        ]
                    },
                    null,
                    "json"
                )
                .then(
                    () => {
                        $(".submit-car").prop("disabled", false);
                        this.addingToCart = false;
                        window.location.href = this.redirectUrl;
                    },
                    err => {
                        PrettyAlerts.show({
                            type: "danger",
                            dismissable: true,
                            message: err.responseJSON.message
                        });
                        $(".submit-car").prop("disabled", false);
                        this.addingToCart = false;
                        this.isDisabled = false;
                    }
                );
        },
        showIncompleteMessage() {

            var collapse = '';

            if (!enabledItems.side) {
                incompleteMsgApp.msg = messages.side;
                collapse = '#collapseOne';
            } else if (!enabledItems.paper) {
                incompleteMsgApp.msg = messages.paper;
                collapse = '#collapseTwo';
            } else if (!enabledItems.finish) {
                incompleteMsgApp.msg = messages.finish;
                collapse = '#collapseThree';
            } else if (!enabledItems.files) {
                incompleteMsgApp.msg = messages.files;
                collapse = '#collapseFour';
            } else if (!enabledItems.freight) {
                incompleteMsgApp.msg = messages.freight;
                collapse = '#collapseFive';
            } else {
                return;
            }

            setTimeout(function () {
                scrollTo(collapse);
                $(collapse).collapse("show");
            }, 500);

            $("#incompleteMsgApp").modal('show');
        },
        pricesShown: function () {
            return (enabledItems.freight && ((this.delivery == 1 && this.shippingResult) || (this.delivery == 0)));
        },
        showDeliveryWarning: function () {
            PrettyAlerts.show({
                type: 'info',
                // dismissable: true,
                message: "Escolha uma das opções de Frete/Preço para Continuar"
            });
        },
        updatePdfPages(file, index) {
            var fileReader = new FileReader();

            callbackStack.push(1);

            fileReader.onload = function () {
                var typedarray = new Uint8Array(this.result);
                pdfjsLib.getDocument(typedarray).then(doc => {
                    callbackStack.pop();
                    orderApp.files[index].numPages = doc.numPages;
                    if (callbackStack.length == 0) {
                        fileInfoApp.calcTotalPages(true);
                    }
                });
            };

            fileReader.readAsArrayBuffer(file);
        },
        checkIfPasswordProtected(file, index) {
            var fileReader = new FileReader();
            callbackStack.push(1);

            fileReader.onload = function () {

                let typedarray = new Uint8Array(this.result);
                var loadingTask = pdfjsLib.getDocument(typedarray);

                loadingTask.promise.then(function (pdf) {
                    callbackStack.pop();
                }).catch(function (error) {
                    callbackStack.pop();
                });
            };

            fileReader.readAsArrayBuffer(file);

            return false;
        },
        formatCurrentItem() {
            var item = {
                'color': "",
                'side': "",
                'paper': "",
                'finishing': {
                    'type': "",
                    'binding': this.binding
                },
                'cupomCode': this.couponName,
                'emailMktId': this.emailMktId,
                'files': this.files,
                'observation': vueObservationModal.observation
            }


            for (var i = 0; i < this.colors.length; i++) {
                if (this.colors[i].id == this.curColor.id) {
                    item.color = this.colors[i].description;
                }
            }

            for (var i = 0; i < this.sides.length; i++) {
                if (this.sides[i].id == this.curSide.id) {
                    item.side = this.sides[i].description;
                }
            }

            for (var i = 0; i < this.paperTypes.length; i++) {
                if (this.paperTypes[i].id == this.curPaper.id) {
                    item.paper = this.paperTypes[i].description;
                }
            }

            for (var i = 0; i < this.finishingTypes.length; i++) {
                if (this.finishingTypes[i].id == this.curFinishing.id) {
                    item.finishing.type = this.finishingTypes[i].description;
                }
            }

            return item;
        },
        getMinMaxPrices() {
            $.post("/getPaperPrices", null, null, "json").then(
                response => {
                    let prices = response.data.prices;
                    orderApp.priceBkMin = prices.BkMin;
                    orderApp.priceBkMax = prices.BkMax;
                    orderApp.priceColorMin = prices.ColorMin;
                    orderApp.priceColorMax = prices.ColorMax;
                },
                err => {
                    PrettyAlerts.show({
                        type: "danger",
                        dismissable: true,
                        message: err.responseJSON.message
                    });
                }
            );
        }
    },
    created: function () {
        this.getMinMaxPrices();
        $.get("/getPartialOrderData", null, null, "json").then(function (r) {
            var d = r.data;
            var ca = [],
                cpb = [];

            orderApp.colors = d.colors;
            orderApp.sides = d.sides;

            d.paperRanges.forEach(pr => {
                if (pr.hasColor == 1) {
                    ca.push(pr);
                } else {
                    cpb.push(pr);
                }
            });

            orderApp.dgPercent = parseInt(d.generalCoupon);

            priceCalculator.setDgPercent(parseInt(d.generalCoupon));
            priceCalculator.setColorRange(ca);
            priceCalculator.setPbRange(cpb);

            orderApp.paperTypes = d.paperTypes;
            // orderApp.visiblePaperTypes = orderApp.getFirst3(
            //     orderApp.paperTypes
            // );
            orderApp.visiblePaperTypes = orderApp.paperTypes;

            orderApp.finishingTypes = d.finishingTypes;
            // orderApp.visibleFinishingTypes = orderApp.getFirst3(
            //     orderApp.finishingTypese
            // );
            orderApp.visibleFinishingTypes = orderApp.finishingTypes;

            orderApp.curPriceNoFreight = priceCalculator.calculateWithoutFreight();
            orderApp.curPriceNoFreigtNoFinishing = priceCalculator.calculateWithoutFreightAndFinishing();
            orderApp.totalDiscount = priceCalculator.getDt();

            var tempDelivery = Cookies.get("delivery");
            if (tempDelivery === undefined) {
                Cookies.set("delivery", 1);
            }

            orderApp.resetFreight();

            orderApp.zip = Cookies.get("zip");
            initFileUpload(d.storageEndpoint);
        });

        // Get coupon parameter from URL, check it and apply
        // If there is no coupon in the URL, search the cookies
        var url = new URL(window.location.href);
        var cupom = url.searchParams.get("cupom");
        if (cupom == null) {
            cupom = Cookies.get("coupon");
        }
        if (cupom) {
            this.coupon = cupom;
            this.addCoupon();
        }

        var emailMktId = Cookies.get("emailMktId");
        if (emailMktId) {
            this.emailMktId = emailMktId;
        }
    }
});

function calcShipping() {
    if (orderApp.totalPages < 1) {
        return;
    }
    if (!orderApp.zip) {
        return;
    }

    var zip = orderApp.zip.replace(/\-/g, "");

    if (zip.length === 8) {
        getFreights(zip);
        getAddress(zip);
    }
}

function getFreights(zip) {
    orderApp.shippingResult = false;
    orderApp.loadingFreights = true;

    var timeout = orderApp.freightFallback == false ? 5000 : 10000;

    $.post({
        url: "/getFreightTypes",
        data: {
            zip: zip,
            item: {
                color: orderApp.curColor.id,
                side: orderApp.curSide.id,
                paper: orderApp.curPaper.id,
                finishing: {
                    id: orderApp.curFinishing.id,
                    binding: orderApp.binding
                },
                cupomCode: orderApp.couponName,
                emailMktId: orderApp.emailMktId,
                files: orderApp.files,
                observation: vueObservationModal.observation
            },
            totalPages: priceCalculator.getQPag(),
            totalPaper: priceCalculator.getQPap(),
            freightFallback: orderApp.freightFallback
        },
        timeout: timeout,
        dataType: "json"
    }).then(
        response => {
            // Event tracking analytics
            if (window.ga) {
                ga('send', {
                    hitType: 'event',
                    eventCategory: 'Entrega/Retirada',
                    eventAction: 'click',
                    eventLabel: 'Entrega'
                });
            }

            var d = response.data;

            // orderApp.prevDeliverySelected = Cookies.get(
            //     "prevDeliverySelected"
            // );

            orderApp.availableDeliveryTypes = d.freights;
            var idx = orderApp.availableDeliveryTypes.findIndex(
                dt => dt.id == orderApp.prevDeliverySelected
            );

            if (idx > -1) {
                orderApp.setDeliveryType(idx);
            }
            /* else if (orderApp.prevDeliverySelected) {
                // PrettyAlerts.show({
                //     type: "danger",
                //     message: 'Atenção! Devido as características do seu pedido, escolha novo frete.'
                // });
                // return false;
            }*/

            orderApp.shippingResult = true;
            orderApp.loadingFreights = false;
            setTimeout(function () {
                scrollTo(".freight-box-list");
            }, 500);
        },
        err => {

            if (err.statusText === "timeout") {

                // Buscar os valores do frete na tabela fallback
                if (orderApp.freightFallback == false) {
                    orderApp.freightFallback = true;
                    calcShipping();
                } else {
                    $('#delayedFreightsModal').modal('show');

                    orderApp.shippingResult = false;
                    orderApp.loadingFreights = false;
                }
            } else {
                PrettyAlerts.show({
                    type: "danger",
                    message: err.responseJSON.message
                });

                // var msg = err.responseJSON.message;
                // if (msg.indexOf("cURL") < 0) {
                //     PrettyAlerts.show({
                //         type: "danger",
                //         message: msg
                //     });
                // } else {
                //     calculateShipping();
                // }

                orderApp.shippingResult = false;
                orderApp.loadingFreights = false;
            }
        }
    );
}

function getAddress(zip) {
    orderApp.zipResult = false;
    orderApp.calculatingZip = true;

    $.post({
        url: "/getAddress",
        data: {
            zip: zip
        },
        dataType: "json"
    }).then(
        response => {
            var d = response.data;
            var addr = d.address;

            if (typeof addr.street !== 'undefined') {
                if (addr.street.length > 1) {
                    orderApp.addressLine1 = addr.street;
                    orderApp.addressLine2 =
                        addr.district +
                        ", " +
                        addr.city +
                        " " +
                        addr.uf +
                        " - Brasil.";
                } else {
                    orderApp.addressLine1 = addr.city + " " + addr.uf + " - Brasil.";
                    orderApp.addressLine2 = null;
                }
            } else {
                orderApp.addressLine1 = null;
                orderApp.addressLine2 = null;
            }

            Cookies.set("zip", zip);
            orderApp.zipResult = true;
            orderApp.calculatingZip = false;
        },
        err => {
            PrettyAlerts.show({
                type: "danger",
                message: err.responseJSON.message
            });

            orderApp.zipResult = false;
            orderApp.calculatingZip = false;
        }
    );
}

var calculateShipping = debounce(calcShipping, 2000);
var calcShippingMinDelay = debounce(calcShipping, 250);

var calculateWithdrawal = debounce(function (isManual = false) {
    $.post(
        "/getWithdrawalTypes",
        {
            totalPages: priceCalculator.getQPag(),
            totalPaper: priceCalculator.getQPap(),
            bindings: [orderApp.curFinishing.id]
        },
        null,
        "json"
    ).then(
        r => {
            // Event tracking analytics
            if (window.ga) {
                ga('send', {
                    hitType: 'event',
                    eventCategory: 'Entrega/Retirada',
                    eventAction: 'click',
                    eventLabel: 'Retirada'
                });
            }

            var d = r.data;
            orderApp.availableWithdrawalTypes = d.withdrawals;

            // orderApp.prevDeliverySelected = Cookies.get("prevDeliverySelected");

            var idx = orderApp.availableWithdrawalTypes.findIndex(
                dt => dt.id == orderApp.prevDeliverySelected
            );

            setTimeout(function () {
                scrollTo(".freight-box-list");
            }, 500);

            if (idx > -1) {
                orderApp.setWithdrawalType(idx, isManual);
            } else if (orderApp.prevDeliverySelected) {
                return false;
            }
        },
        err => {
            orderApp.availableWithdrawalTypes = [];
        }
    );
}, 250);

var fileInfoData = {
    files: [
        {
            name: null,
            numPages: 1,
            numCopies: 1,
            userFileName: "",
            fileSize: 0
        }
    ]
};

var fileInfoApp = new Vue({
    el: "#fileInfoApp",
    data: fileInfoData,
    methods: {
        newItem() {
            this.files.push({
                name: null,
                numPages: 1,
                numCopies: 1
            });
            this.calcTotalPages();
        },
        removeItem(id) {
            this.files.splice(id, 1);
            this.calcTotalPages();
        },
        calcTotalPages(isManual = false) {
            fileAttachment.removeAll();
            $fi.fileinput("clear");

            for (var i = 0; i < this.files.length; i++) {
                if (this.files[i].numCopies < 1) {
                    this.files[i].numCopies = null;
                }
                if (this.files[i].numPages < 1) {
                    this.files[i].numPages = null;
                }
            }

            var prevTotalPages = orderApp.totalPages;
            orderApp.totalPages = 0;
            var totalCopies = 0;
            var countF = 0;

            this.files.forEach(f => {
                countF += parseInt(f.numPages) * parseInt(f.numCopies);

                if (countF <= 999999 || isNaN(countF)) {
                    orderApp.totalPages = isNaN(countF) ? 0 : countF;
                } else {
                    PrettyAlerts.show({
                        type: "danger",
                        dismissable: true,
                        message:
                            "O número de paginas deve ser menor que 1.000.000"
                    });
                    orderApp.totalPages = 0;
                    f.numPages = null;
                    f.numCopies = null;
                }

                totalCopies += parseInt(f.numCopies);
            });

            orderApp.isManual = orderApp.totalPages > 0;

            priceCalculator.setNumFiles(this.files.length);
            priceCalculator.setNumCopies(totalCopies);
            priceCalculator.setQPag(orderApp.totalPages);

            orderApp.curPriceNoFreight = priceCalculator.calculateWithoutFreight();
            orderApp.curPriceNoFreigtNoFinishing = priceCalculator.calculateWithoutFreightAndFinishing();

            if (prevTotalPages != orderApp.totalPages) {
                orderApp.resetFreight(isManual);
                enabledItems.freight = true;
            }

            fileAttachmentAfter.files = orderApp.files = this.files;
        },
        infoNext() {
            this.calcTotalPages();
            $("#collapseSix").collapse("show");
        },
        clear: function () {
            this.files = [
                {
                    name: null,
                    numPages: 1,
                    numCopies: 1
                }
            ];
        },
        increaseNumCopies(f) {
            f.numCopies++;
            this.calcTotalPages();
        },
        decreaseNumCopies(f) {
            if (f.numCopies > 1) {
                f.numCopies--;
                this.calcTotalPages();
            }
        },
        increaseNumPages(f) {
            f.numPages++;
            this.calcTotalPages();
        },
        decreaseNumPages(f) {
            if (f.numPages > 1) {
                f.numPages--;
                this.calcTotalPages();
            }
        }
    },
    computed: {
        fiTotalPages: function () {
            return orderApp.totalPages || 1;
        }
    }
});

var fileAttachment = new Vue({
    el: "#anexoModal",
    data: {
        files: []
    },
    methods: {
        haveFiles: function () {
            return this.files.length > 0;
        },
        countPdfPages(file, index) {
            var fileReader = new FileReader();

            callbackStack.push(1);

            fileReader.onload = function () {
                var typedarray = new Uint8Array(this.result);
                pdfjsLib.getDocument(typedarray).then(doc => {
                    callbackStack.pop();
                    fileAttachment.files[previousBatchLength + index].numPages =
                        doc.numPages;
                    if (callbackStack.length == 0) {
                        fileAttachment.calcTotalPages();
                    }
                });
            };

            fileReader.readAsArrayBuffer(file);
        },
        addFile(files, fileInfo, index) {
            // desabilitando o botao de inserir numero de paginas manual e limpando os campos
            fileInfoApp.clear();

            Vue.set(this.files, previousBatchLength + index, {
                name: fileInfo.fileName,
                numPages: 1,
                numCopies: 1,
                userFileName: fileInfo.userFileName,
                fileSize: fileInfo.fileSize
            });

            orderApp.isManual = false;

            this.countPdfPages(files[index], index);
        },
        removeFile(index) {
            this.files.splice(index, 1);
            this.calcTotalPages();
        },
        removeAll() {
            this.files = [];
            this.calcTotalPages();
        },
        trigRemove(index) {
            var thumb = $('#formOrder').find('.file-preview-thumbnails').children('.file-preview-frame')[index];
            $(thumb).find('.kv-file-remove').trigger('click');
        },
        trigRemoveInvalidFiles() {
            var hasInvalidFile = false;
            var thumb = $('#formOrder').find('.file-preview-thumbnails').children('.file-preview-error');
            $(thumb).each(function (i) {
                if ($(this).hasClass('file-preview-error')) {
                    $(this).find('.kv-file-remove').trigger('click');
                    hasInvalidFile = true;
                }
            });

            if (hasInvalidFile) {
                PrettyAlerts.show({
                    type: "danger",
                    message: "Os arquivos que não estão no formato PDF foram removidos do item"
                });
            }

        },
        calcTotalPages: function () {
            var prevTotalPages = orderApp.totalPages;
            orderApp.totalPages = 0;

            var totalCopies = 0;

            this.files.forEach(item => {
                orderApp.totalPages +=
                    parseInt(item.numPages) * parseInt(item.numCopies);
                totalCopies += parseInt(item.numCopies);
            });

            orderApp.isAttachment = orderApp.totalPages > 0;

            priceCalculator.setNumFiles(this.files.length);
            priceCalculator.setNumCopies(totalCopies);
            priceCalculator.setQPag(orderApp.totalPages);

            orderApp.curPriceNoFreight = priceCalculator.calculateWithoutFreight();
            orderApp.curPriceNoFreigtNoFinishing = priceCalculator.calculateWithoutFreightAndFinishing();

            if (prevTotalPages != orderApp.totalPages) {
                orderApp.resetFreight();
                enabledItems.freight = true;
            }

            orderApp.files = this.files;

            if (msgInstance) {
                PrettyAlerts.hide(msgInstance);
            }
        },
        attachmentNext() {
            this.calcTotalPages();
            if (orderApp.totalPages > 0) {
                $("#collapseSix").collapse("show");
            }
        },
        increaseNumCopies(f) {
            f.numCopies++;
            this.calcTotalPages();
        },
        decreaseNumCopies(f) {
            if (f.numCopies > 1) {
                f.numCopies--;
                this.calcTotalPages();
            }
        }
    }
});

var fileAttachmentAfter = new Vue({
    el: "#anexoModal2",
    data: {
        fileInfo: []
    },
    methods: {
        addFile(file, filesInfo, index) {
            var newFileIndex = 0;
            this.fileInfo = this.fileInfo.concat(filesInfo);
            // adiciona no máximo a quantidade de arquivos informada manualmente
            // ignora o restante

            orderApp.updatePdfPages(file, index);

            orderApp.files[index].name = filesInfo[0].fileName;
            orderApp.files[index].userFileName = filesInfo[0].userFileName;
            orderApp.files[index].fileSize = filesInfo[0].fileSize;
        },
        removeFile(index) {
            var length = orderApp.files.length;

            if (index >= length) {
                this.shiftLeftAndRemoveLastInfo(index);
                return;
            }

            for (var i = index + 1; i < length; i++) {
                // shift left file metadata
                orderApp.files[i - 1].name = orderApp.files[i].name;
                orderApp.files[i - 1].userFileName =
                    orderApp.files[i].userFileName;
                orderApp.files[i - 1].fileSize = orderApp.files[i].fileSize;
            }

            // Mais arquivos carregados do que informados manualmente

            if (this.fileInfo.length > length) {
                orderApp.files[length - 1].name = this.fileInfo[
                    length
                ].fileName;
                orderApp.files[length - 1].userFileName = this.fileInfo[
                    length
                ].userFileName;
                orderApp.files[length - 1].fileSize = this.fileInfo[
                    length
                ].fileSize;
            } else {
                orderApp.files[length - 1].name = null;
                orderApp.files[length - 1].userFileName = "";
                orderApp.files[length - 1].fileSize = 0;
            }

            this.shiftLeftAndRemoveLastInfo(index);
        },
        shiftLeftAndRemoveLastInfo(index) {
            var fileInfoLength = this.fileInfo.length;
            for (var i = index + 1; i < fileInfoLength; i++) {
                // shift left all available file metadata
                this.fileInfo[i - 1] = this.fileInfo[i];
            }
            this.fileInfo.pop();
        },
        removeAll() {
            for (var i = 0; i < orderApp.files.length; i++) {
                orderApp.files[i].name = null;
                orderApp.files[i].userFileName = null;
                orderApp.files[i].fileSize = null;
            }
            this.fileInfo = [];
        },
        tryToSave() {
            if (!orderApp.isFilesOk()) {
                PrettyAlerts.show({
                    type: "info",
                    message:
                        "Você deve informar " +
                        this.files.length +
                        " arquivo(s)"
                });

                return;
            }

            // orderApp.addToShoppingCart();
            $('#anexoModal2').modal('hide');
        },
        getFiles() {
            return orderApp.files;
        }
    }
});

var bindingApp = new Vue({
    el: "#modalEncadernacao",
    data: {
        border: null,
        total: 1,
        details: false,
        corDaCapaOptions: [
            'Preto',
            'Azul Royal',
            'Azul Marinho',
            'Azul Celeste',
            'Vermelho',
            'Vinho',
            'Verde'
        ],
        corDaLetraOptions: [
            'Prateada',
            'Dourada'
        ],
        textoCapaOptions: [
            'Igual à primeira página do PDF que irei anexar',
            'Irei mandar a capa para o e-mail de vocês após finalizar o pedido'
        ],
        textoLombadaOptions: [
            'Nome do autor; Titulo; nível do curso; faculdade; ano',
            'Sem escrito na lombada'
        ],
        corDaCapa: 0,
        corDaLetra: 0,
        textoCapa: 0,
        textoLombada: 0,
        step: 1,
        totalSteps: 6,
        observation: ""
    },
    methods: {
        setBorder(border) {
            this.border = border;
            this.step = 2;
        },
        setBinding() {
            orderApp.binding = {
                border: this.border,
                total: this.total
            };
            if (this.details) {
                orderApp.finishingDetails = {
                    corDaCapa: this.corDaCapaOptions[this.corDaCapa],
                    corDaLetra: this.corDaLetraOptions[this.corDaLetra],
                    textoCapa: this.textoCapaOptions[this.textoCapa],
                    textoLombada: this.textoLombadaOptions[this.textoLombada],
                }
            }
            priceCalculator.setNumBindings(this.total);
            orderApp.curPriceNoFreight = priceCalculator.calculateWithoutFreight();
            orderApp.curPriceNoFreigtNoFinishing = priceCalculator.calculateWithoutFreightAndFinishing();
            $("#modalEncadernacao").modal("hide");
            $("#collapseFive").collapse("show");
        },
        inc() {
            this.total++;
        },
        dec() {
            if (this.total > 1) {
                this.total--;
            }
        },
        previousStep() {
            if (this.step >= 2) {
                this.step -= 1;
            }
        },
        nextStep() {
            if (this.step < this.totalSteps) {
                this.step += 1;
            }
        }
    }
});

var observationModalData = {
    isSaved: false,
    placeholder: "",
    observation: ""
};

var vueObservationModal = new Vue({
    el: "#modalObservacoes",
    data: observationModalData,
    methods: {}
});

function initFileUpload(storageEndpoint) {

    global.$fi = $("#file-0a").fileinput({
        theme: "fa",
        language: "pt-BR",
        uploadUrl: storageEndpoint,
        deleteUrl: "/api/image",
        showCaption: true,
        autoReplace: false,
        overwriteInitial: false,
        uploadAsync: true,
        allowedFileExtensions: ["pdf"],
        ajaxSettings: {
            crossDomain: true
        }
    });

    global.$f2 = $("#file-0b").fileinput({
        theme: "fa",
        language: "pt-BR",
        uploadUrl: storageEndpoint,
        deleteUrl: "/api/image",
        showCaption: true,
        autoReplace: false,
        overwriteInitial: false,
        uploadAsync: true,
        allowedFileExtensions: ["pdf"],
        ajaxSettings: {
            crossDomain: true
        }
    });

    $("#copies").insertAfter("#anexoModal .file-preview");
    $("#copies2").insertAfter("#anexoModal2 .file-preview");

    $("#anexoModal,#anexoModal2")
        .find(".hidden-xs:eq(3)")
        .addClass("d-none d-lg-block default-title text-hg")
        .html("ADICIONAR ARQUIVO");

    $("#anexoModal,#anexoModal2")
        .find(".hidden-xs:eq(3)")
        .parents(".btn-file")
        .css("background-color", "#9DCB7D")
        .css("border-color", "#9DCB7D")
        .css("text-align", "center");

    $("#anexoModal .file-preview, #copies, #anexoModal #btno").hide();

    $("i.fa.fa-folder-open")
        .addClass("fa-plus d-block d-lg-none")
        .removeClass("fa-folder-open");

    $("#formOrder").on("click", ".kv-file-remove", function () {
        var index = $(this)
            .closest(".file-preview-frame")
            .prevAll(".file-preview-frame").length;
        fileAttachment.removeFile(index);
        if (fileAttachment.files.length < 1) $("#anexoModal .file-preview, #copies, #anexoModal #btno").fadeOut();
    });

    $("#formAtt2").on("click", ".kv-file-remove", function () {
        var index = $(this)
            .closest(".file-preview-frame")
            .prevAll(".file-preview-frame").length;
        fileAttachmentAfter.removeFile(index);
    });

    $fi.on("filebatchselected", function (event, files) {
        previousBatchLength = fileAttachment.files.length;
        // $("#btno").prop("disabled", true);
        $fi.fileinput("upload");

        // fileAttachment.trigRemoveInvalidFiles();
    });

    $f2.on("filebatchselected", function (event, files) {
        // $("#btno2").prop("disabled", true);
        $f2.fileinput("upload");
    });

    $fi.on("fileuploaded", function (event, data, previewId, index) {
        // $("#btno").prop("disabled", false);
        fileAttachment.addFile(data.files, data.response.data[0], index);
        $("#anexoModal .file-preview, #copies, #anexoModal #btno").fadeIn();
    });

    $fi.on('fileuploaderror', function (event, data, msg) {
        $fi.fileinput("reset");
        $fi.fileinput("enable");
        PrettyAlerts.show({
            type: "danger",
            message: msg.match("<pre>(.*)</pre>")[1],
            dismissable: true,
            timeout: 5000
        });
    });

    $f2.on("fileuploaded", function (event, data, previewId, index) {
        // $("#btno2").prop("disabled", false);
        fileAttachmentAfter.addFile(
            data.files[index],
            data.response.data,
            index + previousBatchLengthAfter
        );
        aux++;
    });

    $f2.on('fileuploaderror', function (event, data, msg) {
        $f2.fileinput("reset");
        $f2.fileinput("enable");
        PrettyAlerts.show({
            type: "danger",
            message: msg.match("<pre>(.*)</pre>")[1],
            dismissable: true,
            timeout: 5000
        });
    });

    $f2.on("filebatchuploadcomplete", function (event, files, extra) {
        previousBatchLengthAfter += aux;
        aux = 0;
    });

    $fi.on("fileclear", function (e) {
        fileAttachment.removeAll();
    });

    $f2.on("fileclear", function (e) {
        fileAttachmentAfter.removeAll();
    });
}



var exitModal = new Vue({
    el: '#modalDesconto',
    data: {
        firstTime: true,
        email: "",
        couponCode: 'DELIVERY10E',
        loading: false
    },
    methods: {
        applyDiscount: function () {
            var email = this.email;
            if (email !== "") {
                this.loading = true;

                $.post('/saveEmail', {
                    email: email,
                    origin: 'impressao-online'
                }, null, "json").then(
                    response => {
                        var emailMktId = response.data.idEmail;
                        Cookies.set("emailMktId", emailMktId);
                        orderApp.coupon = 'DESCATE10';
                        orderApp.emailMktId = emailMktId;
                        orderApp.addCoupon();
                        this.loading = false;
                        $('#modalDesconto').modal('hide');
                    },
                    err => {
                        this.loading = false;
                        $('#discountModalForm .invalid-feedback').html(err.responseJSON.message);
                        $('#discountModalForm').addClass('was-validated');
                        $('#emailDescontoFormGroup').addClass('is-invalid');
                    }
                );
            } else {
                $('#discountModalForm .invalid-feedback').html("Insira seu e-mail para liberar o desconto");
                $('#discountModalForm').addClass('was-validated');
                $('#emailDescontoFormGroup').addClass('is-invalid');
            }
        },
        showExitModal: function () {
            $('#modalDesconto').modal('show');
        }
    }
});

var delayedFreightsModal = new Vue({
    el: "#delayedFreightsModal",
    data: {
        loading: false,
        email: ""
    },
    methods: {
        sendEmail: function () {
            var email = this.email;
            if (email !== "") {
                this.loading = true;

                $.post('/emailFreteLento', {
                    email: email,
                    item: orderApp.formatCurrentItem()
                }, null, "json").then(
                    response => {
                        this.loading = false;
                        $('#delayedFreightsModal').modal('hide');
                        PrettyAlerts.show({
                            type: "success",
                            dismissable: true,
                            message: "Obrigado! Vamos entrar em contato com você para concluirmos o pedido."
                        });
                    },
                    err => {
                        this.loading = false;
                        $('#delayedFreightsModal .invalid-feedback').html(err.responseJSON.message);
                        $('#delayedFreightsModal').addClass('was-validated');
                        $('#delayedFreightsModal .form-group').addClass('is-invalid');
                    }
                );
            } else {
                $('#delayedFreightsModal .invalid-feedback').html("Insira seu e-mail.");
                $('#delayedFreightsModal').addClass('was-validated');
                $('#delayedFreightsModal .form-group').addClass('is-invalid');
            }
        }
    }
});
