
var vueName = new Vue({
    el: '#main-div',
    methods: {
        setCouponCookie: function (coupon) {
            Cookies.set('coupon', coupon);
            location.href = '/impressao-online';
        }
    },

});

var mailData = {
    userEmail: ""
};

var mailSeed = new Vue({
    el: '#mail-subscribe',
    data: mailData,
    methods: {
        subscribeEmail: function () {
            $.post('/subscribeEmailToList', {
                email: this.userEmail,
                list: 'sub_home'
            }, null, 'json').then(
                response => {
                    PrettyAlerts.show({
                        type: 'info',
                        dismissable: true,
                        message: response.message
                    });
                },
                err => {
                    PrettyAlerts.show({
                        type: 'danger',
                        dismissable: true,
                        message: err.responseJSON.message
                    });
                }
            )
        }
    },
});
