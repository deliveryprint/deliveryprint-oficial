$(document).ready(function() {
    $("#recoverPass")
        .on("ajaxForm:submitSuccess", function(e, response) {
            PrettyAlerts.show({
                type: "info",
                dismissable: true,
                message: "Senha alterada com sucesso"
            });

            $("#modalAcesso").modal();
        })
        .on("ajaxForm:submitError", function(e, response) {
            PrettyAlerts.show({
                type: "danger",
                dismissable: true,
                message: response.message
            });
        });

    var hashValue = getParameterByName('hash');
    $("#hash").val(hashValue);
});

function getParameterByName(name, url) {
    if (!url) url = window.location.href;
    name = name.replace(/[\[\]]/g, "\\$&");
    var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
        results = regex.exec(url);
    if (!results) return null;
    if (!results[2]) return "";
    return decodeURIComponent(results[2].replace(/\+/g, " "));
}
