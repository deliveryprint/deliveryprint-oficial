$(document).ready(function () {

    $('#section-resultado').hide();

    $('#scrollBtn').smoothScroll({
        speed: 1200
    });

    $('#btn-contato').click(function () {
        let name = $('#name').val()
        let email = $('#email').val()
        let phone = $('#phone').val()
        $.post('/emailContato', {
            name: name,
            email: email,
            phone: phone
        }, null, 'json').then(r => {
            $('#main').fadeOut();
            $('#section-processo').fadeOut();
            $('#section-resultado').fadeIn();
        });
    });
})
