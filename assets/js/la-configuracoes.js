var settingsApp = new Vue({
    el: '#settingsApp',
    data: {
        settings: {}
    },
    methods: {
        applyChanges: function () {
            $.post(
                '/updateSettings',
                {
                    settings: this.settings
                },
                null,
                'json'
            ).then(
                r => {
                    PrettyAlerts.show({
                        type: "info",
                        dismissable: true,
                        message: 'Dias extras atualizados!'
                    });
                },
                err => {
                    PrettyAlerts.show({
                        type: "danger",
                        dismissable: true,
                        message: err.responseJSON.message
                    });
                }
            );
        }
    },
    created: function () {
        $.get('/getSettings', null, null, 'json').then(r => {
            this.settings = r.data.settings;
        }, err => {
            PrettyAlerts.show({
                type: "danger",
                dismissable: true,
                message: err.responseJSON.message
            });
        });
    }
});
