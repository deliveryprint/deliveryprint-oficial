require("./lib/deprecated/fileinput");
require("bootstrap-fileinput/js/locales/pt-BR");
require("bootstrap-fileinput/themes/fa/theme.js");

var moment = require('moment');

$(document).ready(function () {

    $(window).on('resize scroll mousemove', function () {

        if ($(window).innerWidth() < 990) {
            $(".div-ent").removeClass("fixed");
            $(".div-ent").removeClass('fixed-bottom');
            $(".div-ent").css('top', 'auto')
        } else {

            scroll = $(window).scrollTop();
            var a = $(".bg-lightgrey").offset().top;

            if (scroll >= 200) {
                $(".div-ent").addClass('fixed');
                $(".div-ent").removeClass('fixed-bottom');
                $('.fixed').css('top', 1 + '%')
                var x = a - scroll;
                if (x < 400) {
                    $(".div-ent").removeClass("fixed");
                    $(".div-ent").addClass('fixed-bottom');
                    $('.fixed-bottom').css('top', a - 400 + 'px')
                }
            } else {
                $(".div-ent").removeClass("fixed");
                $(".div-ent").removeClass("fixed-bottom");
            }

        }

    });

    var $fi = $("#file-0a").fileinput({
        theme: "fa",
        language: "pt-BR",
        uploadUrl: "#",
        showCaption: true,
        autoReplace: false,
        overwriteInitial: false,
        allowedFileExtensions: ["pdf"]
    });

    $("#anexoModal")
        .find(".hidden-xs:eq(3)")
        .html("ADICIONAR ARQUIVO");
    $("#anexoModal")
        .find(".hidden-xs:eq(3)")
        .parents(".btn-file")
        .css("background-color", "#9DCB7D")
        .css("border-color", "#9DCB7D")
        .css("text-align", "center");
    /* dismiss popover */
    $(window).bind('load', function () {
        $('#anexoPopOver').popover({
            'placement': 'left',
            'content': 'Não esqueça dos anexos!'
        }).popover('show');

        $('.popover').on('mouseover', function () {
            $(this).hide();
        });
    });

    setTimeout(function () {
        $(document).mousemove(function (event) {
            // if it's the first time and the order doesn't have a coupon already and the mouse is outside the page
            if (AppModalSaida.firstTime && !shoppingCartApp.hasDiscount && event.clientY <= 20) {
                AppModalSaida.firstTime = false;
                AppModalSaida.showModal();
            }
        });
    }, 5000);
});


export const eventBus = new Vue();

var OrderSummary = require('./components/OrderSummary.vue');
var ShoppingCartAddressForm = require('./components/ShoppingCartAddressForm.vue');
var PageLoader = require('./components/shared/PageLoader.vue');

var shoppingCartApp = new Vue({
    el: "#shopping-items",
    components: {
        'order-summary': OrderSummary,
        'shopping-cart-address-form': ShoppingCartAddressForm,
        'page-loader': PageLoader
    },
    data: {
        pedido: {},
        items: [],
        endereco: {
            id: "",
            cep: "",
            numero: "",
            logradouro: "",
            bairro: "",
            cidade: "",
            complemento: "",
            uf: ""
        },
        cliente: {
            nomeCliente: "",
            telCliente: ""
        },
        incompleteAddr: false,
        fileUrl: '#',
        hasDiscount: false,
        loadingCartData: false,
        loading: false
    },
    methods: {
        goToPaymentPage: function () {

            Login.redirect(false);
            if (!Login.isLoggedIn()) {
                Login.openRegisterModal();
                Login.runOnSuccess(function () {
                    shoppingCartApp.validaCupom()
                });
            } else {
                shoppingCartApp.validaCupom()
            }

        },
        validaCupom: function () {

            this.loading = true;

            $.post('/validateCoupon', {
                itens: this.items
            }, null, "json").then(
                response => {
                    Cookies.remove("coupon");
                    Cookies.remove("emailMktId");
                    shoppingCartApp.completeAddress();
                },
                err => {
                    Cookies.remove("coupon");
                    Cookies.remove("emailMktId");
                    PrettyAlerts.show({
                        type: 'danger',
                        dismissable: true,
                        message: err.responseJSON.message
                    });
                    shoppingCartApp.loading = false;

                    setTimeout(function () { window.location.href = "/impressao-online"; }, 3000);

                }
            );
        },
        completeAddress: function () {

            if (shoppingCartApp.endereco.id) {
                // Existe endereço para entrega
                $.post('/saveUserCompleteAddress', {
                    number: shoppingCartApp.endereco.numero,
                    addrId: shoppingCartApp.endereco.id,
                    complement: shoppingCartApp.endereco.complemento,
                    street: shoppingCartApp.endereco.logradouro,
                    district: shoppingCartApp.endereco.bairro,
                    name: shoppingCartApp.cliente.nomeCliente,
                    tel: shoppingCartApp.cliente.telCliente
                }, null, "json").then(
                    response => {
                        window.location.href = "/carrinho/pagamentos";
                    },
                    err => {
                        PrettyAlerts.show({
                            type: 'danger',
                            dismissable: true,
                            message: err.responseJSON.message
                        });
                        shoppingCartApp.loading = false;
                    }
                );
            } else {
                $.post('/saveUserBasicInfo', {
                    name: shoppingCartApp.cliente.nomeCliente,
                    tel: shoppingCartApp.cliente.telCliente
                }, null, "json").then(
                    response => {
                        window.location.href = "/carrinho/pagamentos";
                    },
                    err => {
                        console.log(err.responseJSON);
                        PrettyAlerts.show({
                            type: 'danger',
                            dismissable: true,
                            message: err.responseJSON.message
                        });
                        shoppingCartApp.loading = false;
                    }
                );
            }
        },
        fetchCartData: function() {
            this.loadingCartData = true;
            this.loading = true;
            $.get({
                url: "/getCartData",
                dataType: "json",
                success: function (response) {
                    shoppingCartApp.pedido = response.data.pedido;
                    shoppingCartApp.items = response.data.items || [];
                    shoppingCartApp.endereco = response.data.endereco;
                    shoppingCartApp.cliente = response.data.dadosCliente;

                    if (response.data.endereco) {
                        if (response.data.endereco.logradouro.length <= 1 || response.data.endereco.bairro <= 1) {
                            shoppingCartApp.endereco.logradouro = "";
                            shoppingCartApp.endereco.bairro = "";
                            shoppingCartApp.incompleteAddr = true;
                        }
                    }

                    if (response.data.pedido && response.data.pedido.tbEmailMkt_idEmailMkt) {
                        shoppingCartApp.hasDiscount = true;
                    }

                    shoppingCartApp.loadingCartData = false;
                    shoppingCartApp.loading = false;
                },
                error: function (err) {
                    shoppingCartApp.loadingCartData = false;
                    shoppingCartApp.loading = false;

                    PrettyAlerts.show({
                        type: 'danger',
                        dismissable: true,
                        message: err.responseJSON.message
                    });
                }
            });
        }
    },
    computed: {
        hasDelivery() {
            if (this.endereco) {
                return this.endereco.cep.length > 0;
            }
            return false;
        },
        hasWithdrawal() {
            var r = this.items.find(i => {
                return i.delivery.tbRetirada_idRetirada > 0
            });
            return r !== undefined;
        }
    },
    created: function () {
        this.fetchCartData();
    }
});

var vueModalObservacoes = new Vue({
    el: "#modalObservacoes",
    data: {
        itemId: 0,
        comment: '',
        date: '12/12',
        isOrientationSaved: false

    },
    methods: {
        saveOrientation: function () {

            // atualizando a observacao do item
            $.post({
                url: '/saveItemObservation',
                data: {
                    id: this.itemId,
                    observation: this.comment
                },
                dataType: 'json',
                success: function (response) {
                    vueModalObservacoes.isOrientationSaved = true;

                    // necessário pegar a data que está salva no banco de dados para mostrar no modal
                    $.post({
                        url: '/getItemObservation',
                        data: {
                            id: vueModalObservacoes.itemId,
                        },
                        dataType: 'json',
                        success: function (response) {
                            if (response.data.date) {
                                vueModalObservacoes.date = moment(response.data.date).format("DD/MM/YYYY - HH:mm");
                                eventBus.$emit("observationEdited", {
                                    id: vueModalObservacoes.itemId,
                                    comment: vueModalObservacoes.comment
                                });
                            }
                        }

                    });
                },
                error: function () {
                    PrettyAlerts.show({
                        type: 'danger',
                        dismissable: true,
                        message: response.message
                    });
                }
            });
        },
        editOrientation: function () {
            this.isOrientationSaved = false;
        },
        openModal() {
            $("#modalObservacoes").modal("show");
        }
    },
    created() {
        eventBus.$on("openObsModal", obsData => {
            this.itemId = obsData.itemId;
            this.comment = obsData.comment;
            this.isOrientationSaved = obsData.isOrientationSaved;
            this.date = obsData.date;

            this.openModal();
        });
    }
});

var ModalSaidaCarrinho = require('./components/ModalSaidaCarrinho.vue');

var AppModalSaida = new Vue({
    el: "#modalSaida",
    data: {
        firstTime: true
    },
    components: {
        'modal-saida-carrinho': ModalSaidaCarrinho
    },
    methods: {
        showModal: function () {
            $('#exitModal').modal('show');
        }
    },
    created: function() {
        eventBus.$on("discountApplied", e => {
            shoppingCartApp.fetchCartData();
        });
    }
});
