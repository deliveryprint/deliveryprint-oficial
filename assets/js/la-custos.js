var costsData = {
    listPaper: [],
    listFinish: []
};

var fetchCosts = debounce(function (searchObj) {
    $.get({
        url: '/getAllCosts',
        data: searchObj,
        dataType: 'json',
        success: function (response) {
            costsData.listPaper = response.data.print_costs;
            costsData.listFinish = response.data.finish_costs;
        },
        error: function (err) {
            PrettyAlerts.show({
                type: 'danger',
                message: err.responseJSON.message
            });
        }
    });
}, 500);

var costsTable = new Vue({
    el: '#costsTable',
    data: costsData,
    methods: {
        updatePrintCosts: function () {
            $.post('/updatePrintCosts', {
                print_costs: this.listPaper
            }, null, 'json').then(
                response => {
                    PrettyAlerts.show({
                        type: 'info',
                        dismissable: true,
                        message: 'Valores Atualizados com Sucesso!'
                    });
                },
                err => {
                    PrettyAlerts.show({
                        type: "danger",
                        dismissable: true,
                        message: err.responseJSON.message
                    });
                }
            );
        },
        updateFinishCosts: function () {
            $.post('/updateFinishCosts', {
                finish_costs: this.listFinish
            }, null, 'json').then(
                response => {
                    PrettyAlerts.show({
                        type: 'info',
                        dismissable: true,
                        message: 'Valores Atualizados com Sucesso!'
                    });
                },
                err => {
                    PrettyAlerts.show({
                        type: "danger",
                        dismissable: true,
                        message: err.responseJSON.message
                    });
                }
            );
        }
    },
    created: function () {
        fetchCosts();
    }
});
