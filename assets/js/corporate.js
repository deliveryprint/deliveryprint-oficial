var PageLoader = require('./components/shared/PageLoader.vue');

var companyApp = new Vue({
    el: "#companyApp",
    components: {
        'page-loader': PageLoader
    },
    data: {
        loading: false,
        name: "",
        email: "",
        phone: ""
    },
    methods: {
        openModal: function () {
            $("#companyModal").modal("show");
        },
        submitCompany: function () {

            // Validar os campos
            let error = false;
            if (this.name.length == 0) {
                $("#nameGroup").addClass("is-invalid");
                error = true;
            }
            if (this.email.length == 0) {
                $("#emailGroup").addClass("is-invalid");
                error = true;
            }
            if (this.phone.length == 0) {
                $("#phoneGroup").addClass("is-invalid");
                error = true;
            }

            if (!error) {
                $.post(
                    "/salvar-empresa",
                    {
                        name: this.name,
                        email: this.email,
                        phone: this.phone
                    },
                    null,
                    "json"
                ).then(
                    response => {
                        window.location.href = '/impressao-online?cupom=NOVAEMPRESA';
                        this.loading = false;
                    },
                    err => {
                        this.loading = false;
                        PrettyAlerts.show({
                            type: "danger",
                            dismissable: true,
                            message: err.responseJSON.message
                        });
                    }
                );
            } else {
                $("#companyForm").addClass("was-validated");
            }
        }
    }
})
