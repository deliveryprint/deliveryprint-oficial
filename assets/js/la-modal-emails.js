require("chosen-js");

/* TABLE HIDDEN CONTENT */
$(document).ready(function () {

    /*** Fixa os filtros ao dar scroll na página ***/
    var $mainFilters = $("#mainFilters");
    var mainFiltersPos = $mainFilters.offset().top;

    $(window).scroll(function () {

        // obtém o scroll do topo da página
        var scrollPos = $(this).scrollTop();

        if (scrollPos >= mainFiltersPos) {
            $mainFilters.addClass('fixed-filter');
            $(".filter-by-date").addClass('fixed-filter');
        }
        else {
            $mainFilters.removeClass('fixed-filter');
            $(".filter-by-date").removeClass('fixed-filter');
        }
    });
});

var fetchItemsFn = debounce(function (searchObj) {
    getAllEmails(searchObj);
    getShoppingCartEmails(searchObj);
}, 500);

var getAllEmails = function (searchObj) {
    $.get({
        url: "/getAllEmails",
        data: searchObj,
        dataType: "json",
        success: function (response) {
            emailsTable.emails = response.data.items;
        },
        error: function (err) {
            PrettyAlerts.show({
                type: "danger",
                message: err.responseJSON.message
            });
        }
    });
};

var getShoppingCartEmails = function (searchObj) {
    $.get({
        url: "/getCartEmails",
        data: searchObj,
        dataType: "json",
        success: function (response) {
            emailsTable.shoppingCartEmails = response.data.items;
            emailsTable.calcOrdersValue();
        },
        error: function (err) {
            PrettyAlerts.show({
                type: "danger",
                message: err.responseJSON.message
            });
        }
    });
};

var filtersVue = new Vue({
    el: "#filters",
    data: {
        startPeriod: null,
        endPeriod: null,
        activeTab: 1
    },
    methods: {
        convertDate(date) {
            return moment(date, "DD/MM");
        },
        isPeriodOk() {
            var startAt = this.convertDate(this.startPeriod);
            var endAt = this.convertDate(this.endPeriod);

            if (this.startPeriod && !startAt.isValid()) {
                return false;
            }

            if (this.endPeriod && !endAt.isValid()) {
                return false;
            }

            return true;
        },
        getFilterValues() {

            var startAt = this.convertDate(this.startPeriod);
            var endAt = this.convertDate(this.endPeriod);
            var periodObj = {
                from: startAt.isValid() ? startAt.format("YYYY-MM-DD") : "",
                to: endAt.isValid() ? endAt.format("YYYY-MM-DD") : ""
            };

            return {
                period: periodObj
            };
        },
        createEmailsCsv() {

            var header = [ 'Data da Inscrição', 'E-mail', 'Comprou?', 'Valor do Pedido' ];
            var items = [];
            for (var i = 0; i < emailsTable.emails.length; i++) {
                var email = emailsTable.emails[i];
                if (email.origem == 'impressao-online') {
                    items.push([
                        email.dataInscricao,
                        email.email,
                        email.statusItem != null ? email.statusItem : "Não",
                        this.$options.filters.moneyFormatter(email.valorItem)
                    ]);
                }
            }

            $.post(
                "/createEmailsCsv",
                {
                    header: header,
                    items: items
                },
                null,
                "json"
            ).then(
                r => {
                    var fileName = r.data.fileName;
                    window.location.href = '/downloadCsv?fileName=' + fileName;
                },
                err => {
                    PrettyAlerts.show({
                        type: "danger",
                        dismissable: true,
                        message: err.responseJSON.message
                    });
                }
            );
        },
        createShoppingCartCsv() {

            var header = [ 'Data da Inscrição', 'E-mail', 'Comprou?', 'Valor do Pedido', 'Desconto', 'Valor Total' ];
            var items = [];
            for (var i = 0; i < emailsTable.shoppingCartEmails.length; i++) {
                var email = emailsTable.shoppingCartEmails[i];
                items.push([
                    email.dataInscricao,
                    email.email,
                    email.statusPedido != null ? email.statusPedido : "Não",
                    this.$options.filters.moneyFormatter(email.valorPedido),
                    this.$options.filters.moneyFormatter(email.discount),
                    this.$options.filters.moneyFormatter(email.order_value)
                ]);
            }

            $.post(
                "/createEmailsCsv",
                {
                    header: header,
                    items: items
                },
                null,
                "json"
            ).then(
                r => {
                    var fileName = r.data.fileName;
                    window.location.href = '/downloadCsv?fileName=' + fileName;
                },
                err => {
                    PrettyAlerts.show({
                        type: "danger",
                        dismissable: true,
                        message: err.responseJSON.message
                    });
                }
            );
        }
    },
    watch: {
        startPeriod() {
            if (this.isPeriodOk()) {
                fetchItemsFn(this.getFilterValues());
            }
        },
        endPeriod() {
            if (this.isPeriodOk()) {
                fetchItemsFn(this.getFilterValues());
            }
        }
    },
    filters: {
        formatDatePt: function (date) {
            if (!date) {
                return "d/m/aaaa";
            }

            var data = moment(date, "DD/MM");
            var dateString = data.locale("pt-br").format("DD [de] MMM");
            return (
                dateString.slice(0, 6) +
                dateString.charAt(6).toUpperCase() +
                dateString.slice(7)
            );
        }
    },
    created() {
        fetchItemsFn(this.getFilterValues());
    }
});


var emailsTable = new Vue({
    el: "#emailsTable",
    data: {
        emails: [],
        shoppingCartEmails: []
    },
    methods: {
        calcOrdersValue: function() {
            for (var i = 0; i < this.shoppingCartEmails.length; i++) {
                if (this.shoppingCartEmails[i].discountType === 'PERCENT') {
                    this.shoppingCartEmails[i].discount = this.shoppingCartEmails[i].order_value * (this.shoppingCartEmails[i].discountValue / 100);
                } else {
                    if (this.shoppingCartEmails[i].discountValue > 7) {
                        this.shoppingCartEmails[i].discount = Math.min(this.shoppingCartEmails[i].order_value * 0.5, this.shoppingCartEmails[i].discountValue);
                    } else {
                        this.shoppingCartEmails[i].discount = this.shoppingCartEmails[i].discountValue;
                    }
                }

                this.shoppingCartEmails[i].valorPedido = parseFloat(this.shoppingCartEmails[i].order_value - this.shoppingCartEmails[i].discount);
            }
        },
        setActiveTab: function(val) {
            filtersVue.activeTab = val;
        }
    }
});
