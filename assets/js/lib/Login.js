var LoginData = {
    isLoggedIn: false,
    redirect: true,
    loginCallback: []
};

const TOKEN_NAME = 'APPcf83e1357eefb8bdf1542';

function Login() { }

Login.init = function () {
    this.$createAccount = $("#createAccountForm");
    this.$recoverForm = $("#recoverForm");
    this.redirectUrl = "/meus-pedidos";
    this.verificationUrl = "/verificacao-necessaria";

    Login.createAccount();
    Login.recover();
    Login.checkLogin();
};

Login.recover = function () {
    this.$recoverForm.submit(function (e) {
        Login.setLoading();
    });

    this.$recoverForm
        .on("ajaxForm:submitSuccess", function (e, response) {
            Login.unsetLoading();
            PrettyAlerts.show({
                type: "info",
                dismissable: true,
                timed: true,
                timeout: 10000,
                message: 'Um link para recuperação de senha foi enviado para seu email de cadastro.'
            });
            $("#modalAcesso").modal('hide');
        })
        .on("ajaxForm:submitError", function (e, response) {
            Login.unsetLoading();
            PrettyAlerts.show({
                type: "danger",
                dismissable: true,
                timed: true,
                timeout: 10000,
                message: response.message
            });
        });
};

Login.createAccount = function () {
    this.$createAccount.submit(function (e) {
        Login.setLoading();
    });

    this.$createAccount
        .on("ajaxForm:submitSuccess", function (e, response) {
            Login.unsetLoading();
            Cookies.set(TOKEN_NAME, response.data.token.value, {
                expires: 180
            });

            PrettyAlerts.show({
                type: "info",
                dismissable: true,
                timed: true,
                timeout: 10000,
                message: response.message
            });

            if (response.data.needVerification) {
                setTimeout(function () {
                    window.location.href = Login.getVerificationUrl();
                }, 1000);
            } else {
                if (Login.willRedirect()) {
                    setTimeout(function () {
                        window.location.href = Login.getRedirectUrl();
                    }, 1000);
                } else {
                    Login.notifyLoginSuccess();
                    $("#modalAcesso").modal('hide');
                    Login.checkLogin();
                }
            }
        })
        .on("ajaxForm:submitError", function (e, response) {
            Login.unsetLoading();
            PrettyAlerts.show({
                type: "danger",
                dismissable: true,
                timed: true,
                timeout: 10000,
                message: response.message
            });
        });
    $("#btn-sair, #btn-sair-m").on("click", function () {
        Cookies.remove(TOKEN_NAME);
        window.location.href = '/';
        return false;
    });
};

Login.setLoading = function () {
    $('#loginForm').addClass('d-none');
    $('#loginForm').removeClass('d-block');
    $('#acessoModalLoading').addClass('d-block');
    $('#acessoModalLoading').removeClass('d-none');
}

Login.unsetLoading = function () {
    $('#loginForm').addClass('d-block');
    $('#loginForm').removeClass('d-none');
    $('#acessoModalLoading').addClass('d-none');
    $('#acessoModalLoading').removeClass('d-block');
}

Login.checkLogin = function () {
    if (!LoginData.isLoggedIn) {
        if (Cookies.get(TOKEN_NAME)) {
            $("#btn-acessar, #btn-acessar-m").hide();
            $("#main, #main-m").show();
            LoginData.isLoggedIn = true;
            Login.notifyLoginSuccess();
        } else {
            $("#main, #main-m").hide();
            $("#btn-acessar, #btn-acessar-m").show();
            LoginData.isLoggedIn = false;
        }
        $("#loginButtonContainer").removeClass("invisible");
    }
};

Login.runOnSuccess = function (fncbk) {

    if (Login.isLoggedIn()) {
        fncbk();
    }

    LoginData.loginCallback.push(fncbk);


};

Login.notifyLoginSuccess = function () {

    for (i = 0; i < LoginData.loginCallback.length; i++) {
        LoginData.loginCallback[i]();
    }

};

Login.isLoggedIn = function () {
    return LoginData.isLoggedIn;
};

Login.openRegisterModal = function () {
    $("#modalAcesso").modal("show");
};

Login.setRedirectUrl = function (url) {
    this.redirectUrl = url;
};

Login.getRedirectUrl = function () {
    return this.redirectUrl;
};

Login.getVerificationUrl = function () {
    return this.verificationUrl;
};

Login.redirect = function (redirect) {
    LoginData.redirect = redirect;
};
Login.willRedirect = function () {
    return LoginData.redirect;
};

module.exports = Login;
