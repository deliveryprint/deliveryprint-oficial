class PriceCalculator {
    constructor() {

        // páginas
        this.qPag = 0;
        this.isFrontBack = false;

        this.numFiles = 0;
        this.numCopies = 0;

        // Acabamento
        this.numBindings = 0;
        this.pa = 0;

        // papel
        this.paperId = 0;
        this.pPap = 0;

        // PRECO IMPRESSÃO
        this.colorRange = [];
        this.pbRange = [];
        this.hasColor = false;

        // DESCONTO
        this.dcMaxValor = 0;
        this.dgPercent = 0;
        this.dcPercent = 0;

        // FRETE
        this.pf = 0;

        // DEBUG MODE
        this.debugMode = false;
    }

    setDebugMode(debug) {
        this.debugMode = debug;
    }

    /* PRECO IMPRESSAO */

    setNumFiles(numFiles) {
        if (this.debugMode) console.log('files', numFiles);
        this.numFiles = numFiles;
    }

    setNumCopies(numCopies) {
        if (this.debugMode) console.log('copies', numCopies);
        this.numCopies = numCopies;
    }

    setNumBindings(numBindings) {
        if (this.debugMode) console.log('bindings', numBindings);
        this.numBindings = numBindings;
    }

    getQa() {

        if (this.numBindings > 0) {
            return this.numBindings;
        }
        // return this.numFiles * this.numCopies;
        return this.numCopies;
    }

    setPPap(pPap) {
        if (this.debugMode) console.log('pPap', pPap);
        this.pPap = pPap;
    }

    getPPap() {
        return this.pPap;
    }

    setQPag(qPag) {
        if (this.debugMode) console.log('qPag', qPag);
        this.qPag = qPag;
    }

    getQPag() {
        return this.qPag;
    }

    setIsFrontBack(isFrontBack) {
        if (this.debugMode) console.log('isFrontBack', isFrontBack);
        this.isFrontBack = isFrontBack;
    }

    getQPap() {
        if (this.isFrontBack) {
            return Math.ceil(this.qPag / 2);
        }

        return this.qPag;
    }

    setPa(pa) {
        if (this.debugMode) console.log('pa', pa);
        this.pa = parseFloat(pa);
    }

    getPa() {
        return this.pa;
    }

    /* DESCONTO */
    setDgPercent(dgPercent) {
        if (this.debugMode) console.log('dgPercent', dgPercent);
        this.dgPercent = dgPercent;
    }

    getDgPecent() {
        return this.dgPercent;
    }

    getDg() {
        return (this.dgPercent / 100) * this.getPi();
    }

    setDcPecent(dcPercent) {
        if (this.debugMode) console.log('dcPercent', dcPercent);
        this.dcPercent = dcPercent;
    }

    getDc() {
        return (this.dcPercent / 100) * this.getPi();
    }

    setDcMaxValor(dcMaxValor) {
        if (this.debugMode) console.log('dcMaxValor', dcMaxValor);
        this.dcMaxValor = dcMaxValor;
    }

    getDcMaxValor() {
        return this.dcMaxValor;
    }

    getDcValor() {
        if (this.getDcMaxValor() > 7) {
            return Math.min(this.getDcMaxValor(), 0.5 * this.getPi());
        } else {
            return parseFloat(this.getDcMaxValor());
        }
    }

    getDt() {
        return this.getDg() + this.getDc() + this.getDcValor();
    }

    /* FRETE */

    setPf(pf) {
        if (this.debugMode) console.log('pf', pf);
        this.pf = pf;
    }

    getPf() {
        return this.pf;
    }

    /* PREÇO IMPRESSÃO */

    setPaperId(paperId) {
        this.paperId = paperId;
    }

    setColorRange(cr) {
        this.colorRange = cr;
    }

    setPbRange(pbr) {
        this.pbRange = pbr;
    }

    setHasColor(hasColor) {
        if (this.debugMode) console.log('hasColor', hasColor);
        this.hasColor = hasColor;
    }

    /* PREÇO TOTAL */

    getPip() {

        var qPag = this.getQPag();
        if (this.paperId < 1 || qPag < 1) {
            return 0;
        }

        var rangeValue = 0;

        if (this.hasColor) {
            var idx = this.colorRange.findIndex(r => {
                return r.idPapel == this.paperId && (qPag >= parseInt(r.de) && qPag <= parseInt(r.ate));
            });

            if (idx < 0) {
                return this.colorRange[this.colorRange.length - 1].preco;
            }

            rangeValue = this.colorRange[idx].preco;
        } else {
            var idx = this.pbRange.findIndex(r => {
                return r.idPapel == this.paperId && (qPag >= parseInt(r.de) && qPag <= parseInt(r.ate))
            });

            if (idx < 0) {
                return this.pbRange[this.pbRange.length - 1].preco;
            }

            rangeValue = this.pbRange[idx].preco;
        }

        return parseFloat(rangeValue);
    }

    getPi() {

        var qPag = this.getQPag(),
            pip = this.getPip(),
            qPap = this.getQPap(),
            pPap = this.getPPap(),
            qa = this.getQa(),
            pa = this.getPa();

        if (this.debugMode) console.log(qPag + ' * ' + pip + ' + ' + qPap + ' * ' + pPap + ' + ' + qa + ' * ' + pa);
        return qPag * pip + qPap * pPap + qa * pa;
    }

    getPt() {
        return this.getPi() - this.getDt() + this.getPf();
    }

    calculate() {
        var pt = this.getPt();
        if (this.debugMode) console.log('valor total: ', pt);
        return pt;
    }

    calculateWithoutFreight() {
        var fr = this.getPi() - this.getDt();
        if (fr <= 0) fr = 0.0;
        if (this.debugMode) console.log('valor sem frete: ', fr);
        return fr;
    }

    calculateWithoutFreightAndFinishing() {
        let basePrice = this.getPi();
        let priceWithoutFreight = this.calculateWithoutFreight();
        let priceFinishing = this.getQa() * this.getPa();
        let finishingPercentDiscount = (this.dgPercent / 100) * priceFinishing + (this.dcPercent / 100) * priceFinishing;
        let finishingAbsDiscount = basePrice > 0.0 ? (priceFinishing / basePrice) * this.getDcValor() : 0.0;
        let discountedFinishing = priceFinishing - finishingPercentDiscount - finishingAbsDiscount;
        let fr = priceWithoutFreight - discountedFinishing;
        if (fr <= 0) fr = 0.0;

        if (this.debugMode) {
            console.log('valor do acabamento: ', priceFinishing)
            console.log('desconto % do acabamento: ', finishingPercentDiscount)
            console.log('desconto abs do acabamento: ', finishingAbsDiscount)
            console.log('valor descontado do acabamento: ', discountedFinishing)
            console.log('valor sem frete e sem acabamento: ', fr);
        }

        return fr;
    }
}

module.exports = PriceCalculator;
