function AdmNotification() { }

AdmNotification.currentDatetime = '0000-00-00 00:00:00';

AdmNotification.notifyNewOrders = function (onNotificationClick) {
    // Verificando se o browser suporta o sistema de notifcações
    if (!("Notification" in window)) {
        console.log("This browser does not support system notifications");
    }
    // Já está sendo permitido exibir notificações
    else if (Notification.permission === "granted") {
        AdmNotification.checkNewOrder(onNotificationClick);
    }
    // Necessário pedir permissão do usuário para exibir a notificação
    else if (Notification.permission !== "denied") {
        Notification.requestPermission(function (permission) {
            if (permission === "granted") {
                AdmNotification.checkNewOrder(onNotificationClick);
            }
        });
    }
};

AdmNotification.checkNewOrder = function (onNotificationClick) {
    AdmNotification.currentDatetime = AdmNotification.getCurrentDatetime();
    setInterval(
        function () {
            AdmNotification.getNewOrder(onNotificationClick);
        },
        60000);
};

AdmNotification.getNewOrder = function (onNotificationClick) {
    $.post("/getNewOrder", {
        datetime: AdmNotification.currentDatetime
    }).then(r => {
        if (r.data.datetime) {
            AdmNotification.currentDatetime = r.data.datetime;
            AdmNotification.createNotification(r.data.id, onNotificationClick);
        }
    });
};

AdmNotification.createNotification = function (orderId, onNotificationClick) {
    var options = {
        body: '\nFoi feito um novo pedido',
        requireInteraction: true
    };

    var notification = new Notification('DeliveryPrint', options);
    notification.onclick = function (event) {
        event.preventDefault();
        onNotificationClick(orderId);
        window.focus();
    };
};

AdmNotification.getCurrentDatetime = function () {
    var currentdate = new Date();
    var datetime = currentdate.getFullYear() + "-"
        + (currentdate.getMonth() + 1) + "-"
        + currentdate.getDate() + " "
        + currentdate.getHours() + ":"
        + currentdate.getMinutes() + ":"
        + currentdate.getSeconds();

    return datetime;
};

module.exports = AdmNotification;
