var cf = require("./CurrencyFormatter");

Vue.filter('moneyFormatter', function (value) {
    if (value == "")
        return '--,--';
    return cf.floatToCurrency(value);
});

Vue.filter('moneyFormatterWithZero', function (value) {
    return cf.floatToCurrency(value);
});
Vue.filter('moneyFormatterWithZeroAndCurrency', function (value) {
    return 'R$ ' + cf.floatToCurrency(value);
});

Vue.filter('percentFormatter', function (value) {
    return value + '%';
});

Vue.filter('dateFormatter', function(value) {
    if(!value) return "n/a";

    var formattedDate = moment(value, 'YYYY-MM-DD').format('DD/MM/YYYY');

    return formattedDate;
});

Vue.filter('dateFormatterY2', function(value) {
    if(!value) return "n/a";

    var formattedDate = moment(value, 'YYYY-MM-DD').format('DD/MM/YY');

    return formattedDate;
});

Vue.filter('dateMFormatter', function(value) {
    if(!value) return "n/a";

    var d = new Date(value);
    var formattedDate = moment(value, 'YYYY-MM-DD').format('DD/MM');

    return formattedDate;
});

Vue.filter('formatNumber', function(value) {
    if(!value) return 0;

    return value;
});

Vue.filter('datetimeFormatter', function(date) {

    if(!date) return "n/a";

    var arr = date.split(' ');
    var formattedDate = moment(arr[0], 'YYYY-MM-DD').format('DD/MM/YYYY');

    return formattedDate + ' ' + arr[1];
});

Vue.filter("formatBytes", function(bytes){
    if(!bytes) {
        return "n/a";
    }

    if(bytes == 0) return '0 Bytes';
    var k = 1024,
        dm = decimals || 2,
        sizes = ['Bytes', 'KB', 'MB', 'GB', 'TB', 'PB', 'EB', 'ZB', 'YB'],
        i = Math.floor(Math.log(bytes) / Math.log(k));

    return parseFloat((bytes / Math.pow(k, i)).toFixed(dm)) + ' ' + sizes[i];
});

Vue.filter("formatBytes", function(bytes){
    if(!bytes) {
        return "n/a";
    }

    if(bytes == 0) return '0 Bytes';
    var k = 1024,
        dm = decimals || 2,
        sizes = ['Bytes', 'KB', 'MB', 'GB', 'TB', 'PB', 'EB', 'ZB', 'YB'],
        i = Math.floor(Math.log(bytes) / Math.log(k));

    return parseFloat((bytes / Math.pow(k, i)).toFixed(dm)) + ' ' + sizes[i];
});

Vue.filter("filterDelivery", function(del) {

    switch(del) {
        case 'Entrega Expressa 1':
            return 'Entrega até 18h HOJE';
        case 'Entrega Expressa 2':
            return 'Entrega até 12h Próximo Dia';
        case 'Retirada Expressa 1':
            return 'Retirada até 17h HOJE';
        case 'Retirada Expressa 2':
            return 'Retirada até 12h Próximo Dia';
        default:
            return del;
    }
});
