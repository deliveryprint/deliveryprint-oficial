

$(function() {
    var tenho = $(".tenho");
    var senha = $(".recuperar-senha");

    $("#jatenho").on("click", function() {
        tenho.css("display", "none");
        senha.css("display", "none");
    });


    $("#recoverPass").on("click", function() {
        senha.css("display", "inline-block");
        tenho.css("display", "none");
    });

    $("#modalAcesso").on('shown.bs.modal', function () {
        tenho.show();
        senha.hide();
    });
});
