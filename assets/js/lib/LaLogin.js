function LaLogin()
{

}

const TOKEN_NAME = 'APPcf83e1357eefb8bdf1542';

LaLogin.init = function()
{
    this.$loginAdm =  $('#loginAdm');

    LaLogin.login();
}

LaLogin.login = function()
{
    this.$loginAdm.on('ajaxForm:submitSuccess', function(e, response){
        Cookies.set(TOKEN_NAME, response.data.token.value, {
            expires: 180
        });

        PrettyAlerts.show({
            type: 'info',
            dismissable: true,
            message: response.message
        });
        setTimeout(function() { window.location.href = '/adm/pedidos'; }, 1000 );
    }).on('ajaxForm:submitError', function(e, response){
        PrettyAlerts.show({
            type: 'danger',
            dismissable: true,
            message: response.message
        });
    });
    $("#btn-sair-adm").on('click', function() {
        Cookies.remove(TOKEN_NAME);
        window.location.href = "/adm/login";
        return false;
    });
}



module.exports = LaLogin

