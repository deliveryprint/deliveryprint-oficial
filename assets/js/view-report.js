require("moment/locale/pt-br");

var filters = {
    startPeriod: null,
    endPeriod: null,
    periodType: null,
    PERIOD_PAYMENT: "Pagamento",
};

var filtersVue = new Vue({
    el: "#filters",
    data: filters,
    methods: {
        setPeriodType(type) {
            this.periodType = type;
        },
        convertDate(date) {
            return moment(date, "DD/MM");
        },
        isPeriodOk() {
            var startAt = this.convertDate(this.startPeriod);
            var endAt = this.convertDate(this.endPeriod);

            if (this.startPeriod && !startAt.isValid()) {
                return false;
            }

            if (this.endPeriod && !endAt.isValid()) {
                return false;
            }

            return true;
        },
        getFilterValue: function () {
            var startAt = this.convertDate(this.startPeriod);
            var endAt = this.convertDate(this.endPeriod);

            var from = startAt.isValid() ? startAt.format("YYYY-MM-DD") : "";
            var to = endAt.isValid() ? endAt.format("YYYY-MM-DD") : "";

            return {
                from: from,
                to: to
            };
        }
    },
    watch: {
        periodType() {
            if (this.isPeriodOk()) {
                fetchReport(this.getFilterValue());
            }
        },
        startPeriod() {
            if (this.isPeriodOk()) {
                fetchReport(this.getFilterValue());
            }
        },
        endPeriod() {
            if (this.isPeriodOk()) {
                fetchReport(this.getFilterValue());
            }
        }
    },
    filters: {
        formatDatePt: function (date) {
            if (!date) {
                return "d/m/aaaa";
            }

            var data = moment(date, "DD/MM");
            var dateString = data.locale("pt-br").format("DD [de] MMM");
            return (
                dateString.slice(0, 6) +
                dateString.charAt(6).toUpperCase() +
                dateString.slice(7)
            );
        }
    },
    created() {
        this.periodType = this.PERIOD_PAYMENT;

    }
});

var fetchReport = debounce(function (searchObj) {
    $.get({
        url: '/generateSalesReport',
        data: searchObj,
        dataType: "json",
        success: function (response) {
            reportsTable.colors = response.data.colors;
            reportsTable.finishes = response.data.finishes;
            reportsTable.average_pages = response.data.average_pages;
            reportsTable.paid_value = response.data.paid_value;
            reportsTable.canceled_value = response.data.canceled_value;
            reportsTable.items_cost = response.data.items_cost;
            reportsTable.items_price = response.data.items_price;
            reportsTable.average_ticket = response.data.average_ticket;
            reportsTable.price_range = response.data.price_range;
            reportsTable.billing_result = response.data.billing_result;
            reportsTable.period_summary = response.data.period_summary;
        },
        error: function (err) {
            PrettyAlerts.show({
                type: "danger",
                message: err.responseJSON.message
            });
        }
    });
}, 500);

var listSales = {
    colors:[],
    finishes: [],
    average_pages: [],
    paid_value: null,
    canceled_value: null,
    paper_type: [
        'Papel Sulfite A4 75g',
        'Papel Sulfite A4 90g',
        'Papel Couché A4 115g',
        'Papel Couché A4 170g',
        'Papel Sulfite A3 75g',
        'Papel Couché A3 150g',
        'Papel Sulfite A5 75g',
        'Papel Carta',
        'Papel Ofício',
        'Papel Reciclado (Eco) A4'
    ],
    finish_type: [
        'Folha Solta',
        'Grampeamentos',
        'Espiral',
        'Capa Dura A4',
        'Wire-o',
        'Capa Dura A3'
    ],
    items_cost: [],
    items_price: [],
    average_ticket: [],
    price_range: [],
    billing_result: [],
    period_summary: {
        black_white: [],
        colorful: [],
        finishes: []
    }
};

var reportsTable = new Vue({
    el: "#reportsTable",
    data: listSales,
    methods: {
    },
    watch: {

    },
    created: function () {
        fetchReport(filtersVue.getFilterValue());
    }
});
