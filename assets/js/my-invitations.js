var AccountMenu = require('./components/user/AccountMenu.vue');

var myInvitationsApp = new Vue({
    el: "#myInvitationsApp",
    components: {
        'account-menu': AccountMenu
    },
    data: {
        coupons: []
    },
    methods: {
        getInvitationCoupons() {
            $.get({
                url: "/getAllInvitationCouponsByUser",
                dataType: "json",
                success: function (response) {
                    console.log(response);
                    myInvitationsApp.coupons = response.data.items;
                },
                error: function (err) {
                    PrettyAlerts.show({
                        type: "danger",
                        dismissable: true,
                        message: err.responseJSON.message
                    });
                }
            });
        }
    },
    created: function () {
        if (!Login.isLoggedIn()) {
            $("#modalAcesso").modal();
            return;
        } else {
            this.getInvitationCoupons();
        }
    }
});
