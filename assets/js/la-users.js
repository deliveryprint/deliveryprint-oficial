require("moment/locale/pt-br");

var filters = {
    startPeriod: null,
    endPeriod: null,
    periodType: null,
    PERIOD_ACCOUNT: "Conta",
    PERIOD_PURCHASE: "Pedido"
};

var fetchUsers = debounce(function (searchObj) {
    $.get({
        url: "/listUsers",
        data: searchObj,
        dataType: "json",
        success: function (response) {
            usersTable.listUsers = response.data.users;
        },
        error: function (err) {
            PrettyAlerts.show({
                type: "danger",
                message: err.responseJSON.message
            });
        }
    });
}, 500);

var filtersVue = new Vue({
    el: "#filters",
    data: filters,
    methods: {
        setPeriodType(type) {
            this.periodType = type;
        },
        convertDate(date) {
            return moment(date, "DD/MM");
        },
        isPeriodOk() {
            var startAt = this.convertDate(this.startPeriod);
            var endAt = this.convertDate(this.endPeriod);

            if (this.startPeriod && !startAt.isValid()) {
                return false;
            }

            if (this.endPeriod && !endAt.isValid()) {
                return false;
            }

            return true;
        },
        getFilterValue: function () {
            var startAt = this.convertDate(this.startPeriod);
            var endAt = this.convertDate(this.endPeriod);
            var filterType = "";

            var from = startAt.isValid() ? startAt.format("YYYY-MM-DD") : "";
            var to = endAt.isValid() ? endAt.format("YYYY-MM-DD") : "";

            if (from || to) {
                filterType = this.periodType;
            }

            return {
                from: from,
                to: to,
                filterType: filterType
            };
        },
        downloadCsv: function () {
            $.post('/createUsersCsv', this.getFilterValue(), null, 'json').then(r => {
                var fileName = r.data.fileName;
                window.location.href = '/downloadCsv?fileName=' + fileName;
            }, err => {
                PrettyAlerts.show({
                    type: "danger",
                    dismissable: true,
                    message: err.responseJSON.message
                });
            });
        }
    },
    watch: {
        periodType() {
            if (this.isPeriodOk()) {
                fetchUsers(this.getFilterValue());
            }
        },
        startPeriod() {
            if (this.isPeriodOk()) {
                fetchUsers(this.getFilterValue());
            }
        },
        endPeriod() {
            if (this.isPeriodOk()) {
                fetchUsers(this.getFilterValue());
            }
        }
    },
    filters: {
        formatDatePt: function (date) {
            if (!date) {
                return "d/m/aaaa";
            }

            var data = moment(date, "DD/MM");
            var dateString = data.locale("pt-br").format("DD [de] MMM");
            return (
                dateString.slice(0, 6) +
                dateString.charAt(6).toUpperCase() +
                dateString.slice(7)
            );
        }
    },
    created() {
        this.periodType = this.PERIOD_ACCOUNT;

    }
});

var usersData = {
    listUsers: [],
};

var usersTable = new Vue({
    el: "#usersTable",
    data: usersData,
    methods: {
        checkField: function (value) {
            if (!value) return "n/a";
            return value;
        }
    },
    watch: {

    },
    created: function () {
        fetchUsers(filtersVue.getFilterValue());
    }
});
