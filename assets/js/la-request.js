import { required, minLength } from 'vuelidate/lib/validators'

require("chosen-js");
require("moment/locale/pt-br");
AdmNotification = require("./lib/AdmNotification");

/* TABLE HIDDEN CONTENT */
$(document).ready(function () {
    var content = $(".hidden-contents");
    content.hide();

    /*** Fixa os filtros ao dar scroll na página ***/
    var $mainFilters = $("#mainFilters");
    var mainFiltersPos = $mainFilters.offset().top;

    $(window).scroll(function () {

        // obtém o scroll do topo da página
        var scrollPos = $(this).scrollTop();

        if (scrollPos >= mainFiltersPos) {
            $mainFilters.addClass('fixed-filter');
            $(".filter-by-date").addClass('fixed-filter');
        }
        else {
            $mainFilters.removeClass('fixed-filter');
            $(".filter-by-date").removeClass('fixed-filter');
        }
    });
});


const STATUS = [
    "Pagamento Pendente",
    "Pedido Realizado",
    "Pendência",
    "Cancelado",
    "Em Produção",
    "Acabamento",
    "Falta Despachar",
    "Despachado",
    "Entregue",
    "Pronto para Retirada",
    "Bloqueado"
];

const STATUS_CLASSES = {
    Enviado: "label-enviado",
    "Em Produção": "label-em-producao",
    "Pagamento Pendente": "label-em-producao",
    Acabamento: "label-em-producao",
    "Falta Despachar": "label-em-producao",
    Produzido: "label-produzido",
    Entregue: "label-entregue",
    Cancelado: "label-cancelado",
    Verificar: "label-verificar",
    Pendência: "label-pendencia",
    Despachado: "label-despachado",
    "Pronto para Retirada": "label-despachado",
    "Pedido Realizado": "label-realizado",
    Bloqueado: "label-bloqueado"
};

function getStatusClass(itemStatus) {
    return STATUS_CLASSES[itemStatus];
}

function changeRowColor(event) {
    var $checkbox = $("#" + event.currentTarget.id);
    if ($checkbox.is(":checked")) {
        var oldColor = $checkbox
            .closest("td")
            .next()
            .css("background-color");
        $checkbox.data("oldColor", oldColor);
        $checkbox
            .closest("td")
            .siblings()
            .css("background-color", "#9DCB7D");
    } else {
        $checkbox
            .closest("td")
            .siblings()
            .css("background-color", $checkbox.data("oldColor"));
    }
}

var fetchItemsFn = debounce(function (searchObj) {
    deliveryTable.clearAllExpanded();
    ninetyDeliveryTable.clearAllExpanded();

    $.get({
        url: "/getAllItems",
        data: searchObj,
        dataType: "json",
        success: function (response) {
            var d = response.data;

            deliveryTable.items = d.items.normalItems;

            ninetyDeliveryTable.items = d.items.fastItems;
        },
        error: function (err) {
            PrettyAlerts.show({
                type: "danger",
                message: err.responseJSON.message
            });
        }
    });
}, 500);


var filters = {
    idItem: "",
    nome: "",
    tipoEntrega: [],
    statusPgto: [],
    statusItem: [],
    startPeriod: null,
    endPeriod: null,
    periodType: null,
    PERIOD_ORDER: "Pedido",
    PERIOD_DELIVERY: "Entrega",
    statusArray: STATUS
};

var filtersVue = new Vue({
    el: "#filters",
    data: filters,
    methods: {
        setPeriodType(type) {
            this.periodType = type;
        },
        convertDate(date) {
            return moment(date, "DD/MM");
        },
        isPeriodOk() {
            var startAt = this.convertDate(this.startPeriod);
            var endAt = this.convertDate(this.endPeriod);

            if (this.startPeriod && !startAt.isValid()) {
                return false;
            }

            if (this.endPeriod && !endAt.isValid()) {
                return false;
            }

            return true;
        },
        getFilterValues() {
            var freights = [],
                withdrawals = [];

            this.tipoEntrega.forEach(d => {
                if (d[0] == "w") {
                    withdrawals.push(d[1]);
                } else {
                    freights.push(d[1]);
                }
            });

            var startAt = this.convertDate(this.startPeriod);
            var endAt = this.convertDate(this.endPeriod);
            var periodObj = {
                from: startAt.isValid() ? startAt.format("YYYY-MM-DD") : "",
                to: endAt.isValid() ? endAt.format("YYYY-MM-DD") : ""
            };

            var orderPeriod = "",
                deliveryPeriod = "";

            switch (this.periodType) {
                case this.PERIOD_ORDER:
                    orderPeriod = periodObj;
                    break;
                case this.PERIOD_DELIVERY:
                    deliveryPeriod = periodObj;
                    break;
            }

            return {
                itemId: this.idItem,
                nameOrEmail: this.nome,
                delivery: {
                    freights: freights.length ? freights : "",
                    withdrawals: withdrawals.length ? withdrawals : ""
                },
                paymentStatus: this.statusPgto.length ? this.statusPgto : "",
                itemStatus: this.statusItem.length ? this.statusItem : "",
                orderPeriod: orderPeriod,
                deliveryPeriod: deliveryPeriod
            };
        }
    },
    watch: {
        idItem() {
            fetchItemsFn(this.getFilterValues());
        },
        nome() {
            fetchItemsFn(this.getFilterValues());
        },
        tipoEntrega() {
            fetchItemsFn(this.getFilterValues());
        },
        statusPgto() {
            fetchItemsFn(this.getFilterValues());
        },
        statusItem() {
            fetchItemsFn(this.getFilterValues());
        },
        periodType() {
            if (this.isPeriodOk()) {
                fetchItemsFn(this.getFilterValues());
            }
        },
        startPeriod() {
            if (this.isPeriodOk()) {
                fetchItemsFn(this.getFilterValues());
            }
        },
        endPeriod() {
            if (this.isPeriodOk()) {
                fetchItemsFn(this.getFilterValues());
            }
        }
    },
    filters: {
        formatDatePt: function (date) {
            if (!date) {
                return "d/m/aaaa";
            }

            var data = moment(date, "DD/MM");
            var dateString = data.locale("pt-br").format("DD [de] MMM");
            return (
                dateString.slice(0, 6) +
                dateString.charAt(6).toUpperCase() +
                dateString.slice(7)
            );
        }
    },
    created() {
        this.periodType = this.PERIOD_ORDER;
        fetchItemsFn(this.getFilterValues());
        // Notificação de novos pedidos
        AdmNotification.notifyNewOrders(onNotificationClick);
    }
});

// vue segunda tabela
var deliveryTable = new Vue({
    el: "#deliveryTable",
    data: {
        items: [],
        expanded: {},
        orderStatus: STATUS,
        curStatus: null,
        curFileName: null,
        curFileUrl: "",
        curFileId: null,
        curFileSize: null
    },
    methods: {
        toggleLine: function (id) {
            this.curFileUrl = "";
            this.curFileName = "";

            if (this.expanded[id]) {
                this.$delete(this.expanded, id);
                return;
            }

            var self = this;
            $.get({
                url: "/getItemDetails/" + id,
                dataType: "json",
                success: function (response) {
                    var idx = self.items.findIndex(it => it.idItem == id);
                    self.items[idx].details = response.data;
                    self.curStatus = self.items[idx].statusItem;
                    Vue.set(self.expanded, id, true);
                },
                error: function () {
                    PrettyAlerts.show({
                        type: "danger",
                        dismissable: true,
                        message: "Error inesperado"
                    });
                }
            });
        },
        myAlert: function() {
            alert('Hello');
        },
        viewPdf: function (file) {
            this.curFileId = file.idArquivo;
            this.curFileName = file.nomeArquivo;
            this.curFileUrl = file.downloadUrl;
            this.curFileSize = file.tamArquivo;
        },
        updateItemStatus(id, orderId) {
            $.post("/item/changeStatus", {
                id: id,
                orderId: orderId,
                status: this.curStatus
            }).then(r => {
                PrettyAlerts.show({
                    type: "info",
                    message: "Status alterado com sucesso"
                });

                var idx = this.items.findIndex(it => it.idItem == id);
                Vue.set(this.items, idx, this.items[idx]);
                this.items[idx].statusItem = this.curStatus;

                if (this.curStatus === 'Despachado') {
                    modalRastreio.showModal(id);
                }
            });
        },
        setCurStatus(status) {
            this.curStatus = status;
        },
        //alterar a observação.
        changeObs(idx) {
            modalObservacao.id = this.items[idx].idItem;
            modalObservacao.observacao = this.items[idx].obsAdmin;
        },
        updateRow(id, obs) {
            var idx = this.items.findIndex(it => it.idItem == id);
            if (idx < 0) {
                return;
            }
            this.items[idx].obsAdmin = obs;
            Vue.set(this.items, idx, this.items[idx]);
        },
        clearAllExpanded() {
            if ($(".table-check:checked").length) {
                this.expanded = {};
                $(".table-check.checkbox").prop("checked", false);
            }
        },
        changeArquivoOkStatus: function (f) {
            $.post("/item/changeFileStatus", {
                idArquivo: f.idArquivo,
                value: f.is_ok
            }).then(r => {
                PrettyAlerts.show({
                    type: "info",
                    message: "Status alterado com sucesso"
                });
            });
        },
        checkbox: changeRowColor,
        getCorrectClass(itemStatus) {
            return getStatusClass(itemStatus);
        }
    },
    filters: {
        formatBytes: function (bytes, decimals) {
            if (!bytes) {
                return "n/a";
            }

            if (bytes == 0) return "0 Bytes";
            var k = 1024,
                dm = decimals || 2,
                sizes = [
                    "Bytes",
                    "KB",
                    "MB",
                    "GB",
                    "TB",
                    "PB",
                    "EB",
                    "ZB",
                    "YB"
                ],
                i = Math.floor(Math.log(bytes) / Math.log(k));

            return (
                parseFloat((bytes / Math.pow(k, i)).toFixed(dm)) +
                " " +
                sizes[i]
            );
        }
    }
});
var ninetyDeliveryTable = new Vue({
    el: "#ninetyTable",
    data: {
        items: [],
        expanded: {},
        orderStatus: STATUS,
        curStatus: null,
        curFileName: null,
        curFileUrl: "",
        curFileId: null,
        curFileSize: null
    },
    methods: {
        toggleLine: function (id) {
            this.curFileUrl = "";
            this.curFileName = "";

            if (this.expanded[id]) {
                this.$delete(this.expanded, id);
                return;
            }

            var self = this;
            $.get({
                url: "/getItemDetails/" + id,
                dataType: "json",
                success: function (response) {
                    var idx = self.items.findIndex(it => it.idItem == id);
                    self.items[idx].details = response.data;
                    self.curStatus = self.items[idx].statusItem;
                    Vue.set(self.expanded, id, true);
                },
                error: function () {
                    PrettyAlerts.show({
                        type: "danger",
                        dismissable: true,
                        message: "Error inesperado"
                    });
                }
            });
        },
        viewPdf: function (file) {
            this.curFileId = file.idArquivo;
            this.curFileName = file.nomeArquivo;
            this.curFileUrl = file.downloadUrl;
            this.curFileSize = file.tamArquivo;
        },
        updateItemStatus(id, orderId) {
            $.post("/item/changeStatus", {
                id: id,
                orderId: orderId,
                status: this.curStatus
            }).then(r => {
                PrettyAlerts.show({
                    type: "info",
                    message: "Status alterado com sucesso"
                });

                var idx = this.items.findIndex(it => it.idItem == id);
                Vue.set(this.items, idx, this.items[idx]);
                this.items[idx].statusItem = this.curStatus;

                if (this.curStatus === 'Despachado') {
                    modalRastreio.showModal(id);
                }
            });
        },
        setCurStatus(status) {
            this.curStatus = status;
        },
        //alterar a observação.
        changeObs(idx) {
            modalObservacao.id = this.items[idx].idItem;
            modalObservacao.observacao = this.items[idx].obsAdmin;
        },
        updateRow(id, obs) {
            var idx = this.items.findIndex(it => it.idItem == id);
            if (idx < 0) {
                return;
            }
            this.items[idx].obsAdmin = obs;
            Vue.set(this.items, idx, this.items[idx]);
        },
        clearAllExpanded() {
            if ($(".table-check:checked").length) {
                this.expanded = {};
                $(".table-check.checkbox").prop("checked", false);
            }
        },
        changeArquivoOkStatus: function (f) {
            $.post("/item/changeFileStatus", {
                idArquivo: f.idArquivo,
                value: f.is_ok
            }).then(r => {
                PrettyAlerts.show({
                    type: "info",
                    message: "Status alterado com sucesso"
                });
            });
        },
        checkbox: changeRowColor,
        getCorrectClass(itemStatus) {
            return getStatusClass(itemStatus);
        }
    },
    filters: {
        formatBytes: function (bytes, decimals) {
            if (!bytes) {
                return "n/a";
            }

            if (bytes == 0) return "0 Bytes";
            var k = 1024,
                dm = decimals || 2,
                sizes = [
                    "Bytes",
                    "KB",
                    "MB",
                    "GB",
                    "TB",
                    "PB",
                    "EB",
                    "ZB",
                    "YB"
                ],
                i = Math.floor(Math.log(bytes) / Math.log(k));

            return (
                parseFloat((bytes / Math.pow(k, i)).toFixed(dm)) +
                " " +
                sizes[i]
            );
        }
    }
});

$(document).ready(function () {
    $(".chosen-select").chosen();
    $(".chosen-select")
        .chosen()
        .change(function (event) {
            filtersVue.tipoEntrega = $(this).val();
        });

    $(".chosen-select-1").chosen();
    $(".chosen-select-1")
        .chosen()
        .change(function (event) {
            filtersVue.statusPgto = $(this).val();
        });

    $(".chosen-select-2").chosen();
    $(".chosen-select-2")
        .chosen()
        .change(function (event) {
            filtersVue.statusItem = $(this).val();
        });
});

var modalObservacao = new Vue({
    el: "#modalObservacoes",
    data: {
        id: null,
        observacao: null
    },
    methods: {
        saveObs() {
            $.post(
                "/saveAdmObs",
                {
                    id: this.id,
                    observacao: this.observacao
                },
                null,
                "json"
            ).then(
                function (r) {
                    PrettyAlerts.show({
                        type: "info",
                        message: "Observação salva com sucesso"
                    });
                    deliveryTable.updateRow(
                        modalObservacao.id,
                        modalObservacao.observacao
                    );
                    ninetyDeliveryTable.updateRow(
                        modalObservacao.id,
                        modalObservacao.observacao
                    );
                },
                function (err) {
                    PrettyAlerts.show({
                        type: "danger",
                        message: err.responseJSON.message
                    });
                }
            );
            $("#modalObservacoes").modal("hide");
        }
    }
});

var modalRastreio = new Vue({
    el: "#modalRastreio",
    data: {
        itemId: null,
        code: ""
    },
    validations: {
        code: {
            required,
            minLength: minLength(13)
        }
    },
    methods: {
        showModal(itemId) {
            $('#modalRastreio').modal('show');

            this.itemId = itemId;
            this.code = "";
        },
        saveTrackingCode() {
            $.post("/item/updateTrackingCode", {
                itemId: this.itemId,
                code: this.code
            }).then(
                r => {
                    PrettyAlerts.show({
                        type: "info",
                        message: "O código de rastreio foi atualizado com sucesso"
                    });
                    $('#modalRastreio').modal('hide');
                },
                err => {
                    PrettyAlerts.show({
                        type: "danger",
                        dismissable: true,
                        message: err.responseJSON.message
                    });
                }
            );
        }
    }
});

var fetchItemsFnAndToggleLine = debounce(function (searchObj) {

    deliveryTable.clearAllExpanded();
    ninetyDeliveryTable.clearAllExpanded();

    $.get({
        url: "/getAllItems",
        data: searchObj,
        dataType: "json",
        success: function (response) {
            var d = response.data;

            deliveryTable.items = d.items.normalItems;

            ninetyDeliveryTable.items = d.items.fastItems;

            if (ninetyDeliveryTable.items.length > 0) {
                ninetyDeliveryTable.toggleLine(ninetyDeliveryTable.items[0].idItem);
            } else {
                deliveryTable.toggleLine(deliveryTable.items[0].idItem);
            }
        },
        error: function (err) {
            PrettyAlerts.show({
                type: "danger",
                message: err.responseJSON.message
            });
        }
    });
}, 500);

function getOrderIdFilter(orderId) {
    return {
        itemId: orderId + ".",
        nameOrEmail: "",
        delivery: {
            freights: "",
            withdrawals: ""
        },
        paymentStatus: "",
        itemStatus: "",
        orderPeriod: "",
        deliveryPeriod: ""
    };
}

function onNotificationClick(orderId) {
    fetchItemsFnAndToggleLine(getOrderIdFilter(orderId));
}
