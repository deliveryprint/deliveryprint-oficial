$(document).ready(function(){
    
    if($('#map').hasClass("map")){
        $('#how_it_works, .solicitation-fix, .hide-m')
        .removeClass('d-flex').addClass('d-none');
        $('header img.logo').removeClass('m-3').addClass('mt-1');
        $('#contacts').remove();
    }
});