var warningApp = new Vue({
    el: '#verificationWarning',
    data: {
        needVerification: false
    },
    methods: {
        checkVerification() {
            $.get(
                "/checkVerification",
                null,
                "json"
            ).then(
                response => {
                    this.needVerification = response.data.needVerification;
                }
            );
        }
    },
    created() {
        this.checkVerification();
    }
})

var OrderList = require('./components/user/OrderList.vue');
var AccountMenu = require('./components/user/AccountMenu.vue');

var myRequestsApp = new Vue({
    el: "#orders",
    components: {
        'order-list': OrderList,
        'account-menu': AccountMenu
    },
    data: {
        orders: []
    },
    methods: {
        showModalInfo: function (id) {
            myItems.currentOrder = id;

            $.post({
                url: "/getInfoOrder",
                data: { idPedido: id },
                dataType: "json",
                success: function (response) {
                    myItems.items = response.data;
                    $("#detailsModal").modal();
                }
            });
        },
        payOrder: function (id) {
            $.post(
                "/paySelectedOrder",
                {
                    orderId: id
                },
                null,
                "json"
            ).then(
                response => {
                    if (response.data.cardBanner) {
                        Cookies.set("boleto", "false");
                    } else {
                        Cookies.set("boleto", "true");
                    }
                    window.location.href = response.data.url;
                },
                err => {
                    PrettyAlerts.show({
                        type: "danger",
                        dismissable: true,
                        message: err.responseJSON.message
                    });
                }
            );
        },
        showModalTracking(id) {
            $.post("/getInfoOrder", {
                idPedido: id
            }).then(
                r => {
                    itemsTracking.items = r.data;
                    $('#itemsTrackingModal').modal('show');
                }
            );
        }
    },
    created: function () {
        if (!Login.isLoggedIn()) {
            $("#modalAcesso").modal();
            return;
        }

        $.get({
            url: "/myOrders",
            dataType: "json",
            success: function (response) {
                myRequestsApp.orders = response.data;

                for (var i = 0; i < myRequestsApp.orders.length; i++) {
                    var order = myRequestsApp.orders[i];
                    var orderDiscount = 0;

                    if (order.discountValue !== null) {
                        if (order.discountType === "PERCENT") {
                            orderDiscount = order.order_value * (order.discountValue / 100);
                        } else {
                            if (order.discountValue > 7) {
                                orderDiscount = Math.min(order.order_value * 0.5, order.discountValue);
                            } else {
                                orderDiscount = parseFloat(order.discountValue);
                            }
                        }
                    }

                    myRequestsApp.orders[i].vfinal =
                        order.order_value - orderDiscount;
                }
            },
            error: function (err) {
                PrettyAlerts.show({
                    type: "danger",
                    dismissable: true,
                    message: err.responseJSON.message
                });
            }
        });
    }
});

var myItems = new Vue({
    el: "#detailsModal",
    data: {
        items: [
            {
                isWithdrawal: false,
                delivery_date: null
            }
        ],
        currentOrder: null
    }
});

var itemsTracking = new Vue({
    el: "#itemsTrackingModal",
    data: {
        items: [],
        currentCode: null
    },
    methods: {
        trackItem(code) {
            this.currentCode = code;

            $('#trackingForm').submit();
        }
    }
});
