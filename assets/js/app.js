// JQuery
//global.jQuery = global.$ = require('jquery');

// Bootstrap
//require('bootstrap');

// Vue e plugins
//global.Vue = require("vue/dist/vue.common");
var SocialSharing = require('vue-social-sharing');
require('./lib/VueFilters');
VueTheMask = require('vue-the-mask');
import Vuelidate from 'vuelidate'

Vue.use(VueTheMask);
Vue.use(Vuelidate);
Vue.use(SocialSharing);

// Bibliotecas
AjaxForm = require('./lib/AjaxForm');
global.PrettyAlerts = require('./lib/PrettyAlerts');
require('./lib/deprecated/ModalAcesso');
global.Login = require('./lib/Login');
global.moment = require('moment');
global.Cookies = require('js-cookie');


// Aqui começa o código da aplicação
(function ($) {
    PrettyAlerts.init();
    AjaxForm.init('.intec-ajax-form');
    Login.init();

    $.get({
        url: "/getNumberItemsInCart",
        dataType: "json",
        success: function (response) {
            response.data.itemsInCart = response.data.itemsInCart != null ? response.data.itemsInCart : 0;

            $("#cartInfo").html("(" + response.data.itemsInCart + ")");
        },
        error: function () {
            console.log(arguments);
        }
    });

})(jQuery);
/**
 * @xs xs Media Query
 */


$(document).ready(function () {
    $('body').on('click touchstart', '.close', function (e) {
        return $(this).parents('.popover').remove();
    });
    $('.custom-file-input').on('change', function () {
        var fileName = $(this).val().split("\\").reverse()[0];
        $(this).next('.custom-file-label').html(fileName);
    });
    $('#btn-coupon').val('GANHE10NA1A');
    $('#btn-coupon').on("click", function () {
        Cookies.set("coupon", $(this).val()) ?
            location.href = "/impressao-online" : false;
    });
    $('#orcamento').on("click", function () {
        Cookies.set("coupon", "ORCAMENTOHOME") ?
            location.href = "/impressao-online" : false;
    });
    if (getLocale() === "") {
        $('.email-feed').removeClass('d-none');
    }

    // Esconder a div feia do Jivo
    //document.getElementById('jvlabelWrap').style.display = 'none';

    // Habilitar o funcionamento da sidenav
    $(".navbar-toggler").click(function() {
        sidenavApp.toggleSidenav();
    })
});



global.debounce = function (func, wait, immediate) {
    var timeout;
    return function () {
        var context = this,
            args = arguments;
        var later = function () {
            timeout = null;
            if (!immediate) func.apply(context, args);
        };
        var callNow = immediate && !timeout;
        clearTimeout(timeout);
        timeout = setTimeout(later, wait);
        if (callNow) func.apply(context, args);
    };
};

global.scrollTo = function (target) {
    console.log('???', target);
    $("html, body").animate({
        scrollTop: $(target).offset().top
    }, 500);
}

function getLocale() {
    return String(window.location).split('/').reverse()[0];
}

// window.jivo_onOpen = function () {

//     let jivoLabelWrap = document.getElementById('jvlabelWrap');
//     let jivoGlobalClass = document.getElementsByClassName('globalClass_ET');
//     let jivoBtnCont = document.getElementById('jivoContainer');

//     jivoLabelWrap.style.display = 'block';
//     jivoGlobalClass[0].style.display = 'block';
//     jivoBtnCont.style.display = 'none';

// }

// window.jivo_onClose = function () {

//     let jivoLabelWrap = document.getElementById('jvlabelWrap');
//     let jivoGlobalClass = document.getElementsByClassName('globalClass_ET');
//     let jivoBtnCont = document.getElementById('jivoContainer');

//     jivoLabelWrap.style.display = 'none';
//     jivoGlobalClass[0].style.display = 'none';
//     jivoBtnCont.style.display = 'block';

// }

// window.jivo_onLoadCallback = function () {
//     let jivoLabelWrap = document.getElementById('jvlabelWrap');
//     let jivoBtnCont = document.getElementById('jivoContainer');

//     jivoLabelWrap.style.display = 'none';
//     jivoBtnCont.classList.add("loaded");

// }

var sidenavApp = new Vue({
    el: '#sidenavApp',
    data: {
        active: false
    },
    methods: {
        toggleSidenav() {
            this.active = !this.active;
        }
    }
})
