var fetchItemsFn = debounce(function (searchObj) {
    Coupon.expanded = {};
    $.get("/findCoupons", searchObj, null, "json").then(
        res => {
            Coupon.coupons = res.data;
        },
        err => {
            PrettyAlerts.show({
                type: "danger",
                message: err.responseJSON.message
            });
        }
    );
}, 500);

const CouponFilter = new Vue({
    el: "#coupon-filter",
    data: {
        status: 1,
        codeOrEmail: null,
        hasEmail: 2
    },
    methods: {
        searchObj() {
            return {
                status: this.status,
                codeOrEmail: this.codeOrEmail,
                hasEmail: this.hasEmail
            };
        },
        downloadCsv: function () {
            $.post('/createCouponsCsv', this.searchObj(), null, 'json').then(r => {
                var fileName = r.data.fileName;
                window.location.href = '/downloadCsv?fileName=' + fileName;
            }, err => {
                PrettyAlerts.show({
                    type: "danger",
                    dismissable: true,
                    message: err.responseJSON.message
                });
            });
        }
    },

    watch: {
        status() {
            fetchItemsFn(this.searchObj());
        },
        codeOrEmail() {
            fetchItemsFn(this.searchObj());
        },
        hasEmail() {
            fetchItemsFn(this.searchObj());
        }
    }
});

var Coupon = new Vue({
    el: "#coupon-table",
    data: {
        coupons: [],
        data: {
            total_pages: null,
            total_comission: null,
            total_sales: null,
            color: null,
            pb: null,
            espiral: null,
            wire_o: null,
            capa_dura: null,
            cities: [],
            state: [],
            first_use: null,
            last_use: null
        },
        expanded: {},
        startdate: null,
        validade: null,
        currId: null,
        couponId: null
    },
    methods: {
        toggleLine: function (id) {
            if (this.expanded[id]) {
                this.$delete(this.expanded, id);
                return;
            }

            $.get(
                "/couponDetails",
                {
                    couponId: id
                },
                null,
                "json"
            ).then(
                r => {
                    Vue.set(this.expanded, id, r.data);
                },
                err => {
                    PrettyAlerts.show({
                        type: "danger",
                        message: err.responseJSON.message
                    });
                }
            );
        },
        getCouponName(id) {
            for (idx of this.coupons) {
                if (idx.idDesc === id) {
                    return idx.codigo;
                }
            }
        },
        changeStatus: function (id, s) {
            s == "1"
                ? this.deactiveCoupon(id)
                : $("#coupon-status").modal("show");
            this.currId = id;
            this.couponId = this.coupons[id].idDesc;
        },
        removeCoupons: function () {
            $.get(
                "/removeCoupon",
                {
                    coupons: this.currentSelected
                },
                null,
                "json"
            ).then(
                res => {
                    PrettyAlerts.show({
                        type: "info",
                        dismissable: true,
                        message: res.message
                    });
                },
                err => {
                    PrettyAlerts.show({
                        type: "danger",
                        dismissable: true,
                        message: err.responseJSON.message
                    });
                }
            );
            var len = this.coupons.length - 1;

            fetchItemsFn(CouponFilter.searchObj());
        },
        deactiveCoupon: function (id) {
            $.post(
                "/deactiveCoupon",
                {
                    couponId: this.coupons[id].idDesc
                },
                null,
                "json"
            ).then(
                res => {
                    PrettyAlerts.show({
                        type: "info",
                        dismissable: true,
                        message: res.message
                    });
                    this.coupons[id].status = 0;
                },
                err => {
                    PrettyAlerts.show({
                        type: "danger",
                        dismissable: true,
                        message: err.responseJSON.message
                    });
                }
            );
        },
        activeCoupon: function () {
            var id = this.currId;
            $.post(
                "/activeCoupon",
                {
                    couponId: this.couponId,
                    startdate: this.startdate,
                    validade: this.validade
                },
                null,
                "json"
            ).then(
                res => {
                    PrettyAlerts.show({
                        type: "info",
                        dismissable: true,
                        message: res.message
                    });
                    fetchItemsFn(CouponFilter.searchObj());
                    $("#coupon-status").modal("hide");
                },
                err => {
                    PrettyAlerts.show({
                        type: "danger",
                        dismissable: true,
                        message: err.responseJSON.message
                    });
                }
            );
        }
    },
    computed: {
        currentSelected() {
            return Object.keys(this.expanded);
        }
    },
    created: function () {
        fetchItemsFn(CouponFilter.searchObj());
    }
});

/**
 * Send
 */
var outputs = {
    code: "",
    value: "",
    startdate: "",
    expiration_date: "",
    type: "",
    cupom_limit: "",
    email: "",
    cupom_user_limit: 1,
    commission_percent: "",
    comment: ""
};

var addCoupon = new Vue({
    el: "#addCoupon",
    data: outputs,
    methods: {
        validateData: function () {
            if (this.code.length < 3) {
                PrettyAlerts.show({
                    type: "danger",
                    message:
                        "O código do cupom deve possuir pelo menos 3 caracteres"
                });

                return false;
            }

            if (this.startdate == "") {
                PrettyAlerts.show({
                    type: "danger",
                    message:
                        "Por favor, informe a data de início da validade do cupom"
                });

                return false;
            }

            if (this.expiration_date == "") {
                PrettyAlerts.show({
                    type: "danger",
                    message:
                        "Por favor, informe a data de término da validade do cupom"
                });

                return false;
            }

            if (this.value == "" || this.value < 1) {
                PrettyAlerts.show({
                    type: "danger",
                    message: "Valor do cupom não é válido"
                });

                return false;
            }

            if (this.type == "") {
                PrettyAlerts.show({
                    type: "danger",
                    message: "Escolha o tipo do cupom"
                });

                return false;
            } else if (this.type == "PERCENT" && this.value > 100) {
                PrettyAlerts.show({
                    type: "danger",
                    message: "Valor do cupom deve estar entre 0 e 100"
                });

                return false;
            }

            if (this.cupom_limit == "") {
                PrettyAlerts.show({
                    type: "danger",
                    message:
                        "Por favor, informe o limite geral de usos do cupom"
                });

                return false;
            }

            if (this.cupom_user_limit == "") {
                PrettyAlerts.show({
                    type: "danger",
                    message:
                        "Por favor, informe o limite geral de usos (por usuário) do cupom"
                });

                return false;
            }

            return true;
        },
        saveCoupon: function () {
            if (!this.validateData()) {
                return;
            }

            $.post("/addCoupon", outputs, null, "json").then(
                res => {
                    PrettyAlerts.show({
                        type: "info",
                        dismissable: true,
                        message: res.message
                    });
                },
                err => {
                    PrettyAlerts.show({
                        type: "danger",
                        dismissable: true,
                        message: err.responseJSON.message
                    });
                }
            );
            fetchItemsFn(CouponFilter.searchObj());
        }
    },
    computed: {},
    created: function () { }
});
