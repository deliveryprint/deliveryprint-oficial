require("moment/locale/pt-br");

import BarChart from './components/admin/reports/BarChart.vue';
import LineChart from './components/admin/reports/LineChart.vue';
import PieChart from './components/admin/reports/PieChart.vue';
import DataTable from './components/admin/reports/DataTable.vue';
import SpotlightBox from './components/admin/reports/SpotlightBox.vue';
import AdminBox from './components/admin/AdminBox.vue';
import FiltersApp from './components/admin/Filters.vue';
import PageLoader from './components/shared/PageLoader.vue';

var chartColors = {
    red: 'rgb(255, 99, 132)',
    orange: 'rgb(255, 159, 64)',
    yellow: 'rgb(255, 205, 86)',
    green: 'rgb(75, 192, 192)',
    blue: 'rgb(54, 162, 235)',
    purple: 'rgb(153, 102, 255)',
    grey: 'rgb(201, 203, 207)'
};

var reportsApp = new Vue({
    el: '#reportsApp',
    components: {
        'bar-chart': BarChart,
        'line-chart': LineChart,
        'pie-chart': PieChart,
        'data-table': DataTable,
        'spotlight-box': SpotlightBox,
        'admin-box': AdminBox,
        'filters-app': FiltersApp,
        'page-loader': PageLoader
    },
    data: {
        loading: {
            sales: false,
            billingResults: false,
            pagesTotal: false,
            finishes: false,
            deliveryTypes: false,
            ranges: false,
            couponsTotal: false,
            customerSales: false
        },
        filtersValue: {
            from: null,
            to: null
        },
        chartHeight: 250,
        salesChartHeight: 120,
        chartsOptions: {
            responsive: true
        },
        salesChartData: {},
        salesChart2Data: {},
        salesChartOptions: {
            responsive: true,
            scales: {
                xAxes: [{
                    display: true,
                    scaleLabel: {
                        display: true,
                        labelString: 'Data'
                    }
                }],
                yAxes: [{
                    display: true,
                    scaleLabel: {
                        display: true,
                        labelString: 'R$'
                    },
                    ticks: {
                        precision: 2
                    }
                }]
            }
        },
        freightChartData: {},
        deliveryChartData: {},
        withdrawalChartData: {},
        rangeChartData: {},
        rangeChartOptions: {
            legend: {
                display: false
            },
            tooltips: {
                callbacks: {
                    title: function(tooltipItem, data) {
                        var datasetIndex = tooltipItem[0].datasetIndex;
                        var index = tooltipItem[0].index;

                        var paperType = data.datasets[datasetIndex].rangesData[index].paperType;
                        var color = data.datasets[datasetIndex].rangesData[index].color;
                        var numPages = data.datasets[datasetIndex].rangesData[index].numPages;

                        return paperType + '\n' + color + '\n' + numPages;
                    }
                }
            }
        },
        billingResult: [],
        pagesTotalHeaders: [{ text: 'Tipo' }, { text: 'Preto e Branco', align: 'right' }, { text: 'Colorido', align: 'right' }, { text: 'Total', align: 'right' }],
        couponsTotalHeaders: [{ text: 'Código' }, { text: 'Quantidade', align: 'right' }],
        pagesTotalItems: [],
        couponsTotalItems: [],
        invitationCoupons: [],
        totalPagesPb: 0,
        totalPagesColor: 0,
        totalPricePb: 0,
        totalPriceColor: 0,
        finishesHeaders: [{ text: 'Tipo' }, { text: 'Quantidade', align: 'right' }],
        finishesItems: [],
        deliveryTypesHeaders: [{ text: 'Tipo' }, { text: 'Total', align: 'right' }],
        deliveryTypesItems: [],
        rangesHeaders: [{ text: 'Id' }, { text: 'Tipo de Papel' }, { text: 'Cor' }, { text: 'Qtd de Pág' }, { text: 'N° Itens', align: 'right' }, { text: 'Faturamento', align: 'right' }, { text: 'Custo', align: 'right' }, { text: 'Resultado', align: 'right' }],
        rangesItems: [],
        averageTicket: 0.0,
        customerSalesChartData: {}
    },
    computed: {
        loadingAll() {
            return this.loading.sales &&
                   this.loading.billingREsults &&
                   this.loading.pagesTotal &&
                   this.loading.finishes &&
                   this.loading.deliveryTypes &&
                   this.loading.ranges &&
                   this.loading.couponsTotal &&
                   this.loading.customerSales
        }
    },
    methods: {
        downloadCsv: function () {
            $.get('/generateSalesReportCsv', this.filtersValue, null, 'json').then(r => {
                var fileName = r.data.fileName;
                window.location.href = '/downloadCsv?fileName=' + fileName;
            }, err => {
                PrettyAlerts.show({
                    type: "danger",
                    dismissable: true,
                    message: err.responseJSON.message
                });
            });
        },
        downloadLtvTable: function () {
            $.get('/generateLtvTable', this.formatFilterValue(), null, 'json').then(r => {
                var fileName = r.data.fileName;
                window.location.href = '/downloadCsv?fileName=' + fileName;
            }, err => {
                PrettyAlerts.show({
                    type: "danger",
                    dismissable: true,
                    message: err.responseJSON.message
                });
            });
        },
        formatFilterValue: function() {
          var startAt = moment(this.filtersValue.from, "DD/MM/YYYY");
          var endAt = moment(this.filtersValue.to, "DD/MM/YYYY");

          var from = startAt.isValid() ? startAt.format("YYYY-MM-DD") : "";
          var to = endAt.isValid() ? endAt.format("YYYY-MM-DD") : "";

          return {
            from: from,
            to: to
          };
        },
        getSalesData(period) {
            if (!period) period = { to: '', from: '' };

            this.salesChartData.datasets = [];
            this.salesChart2Data.datasets = [];
            this.loading.sales = true;

            $.get({
                url: '/getSalesData',
                data: period,
                dataType: "json",
                success: function (response) {
                    var salesChartLabels = [];
                    var salesNumberData = [];
                    var costData = [];
                    var valueData = [];
                    var salesData = response.data;
                    for (var key in salesData) {
                        if (!salesData.hasOwnProperty(key)) continue;

                        var dateSplit = salesData[key].date.split("-");
                        salesChartLabels.push(dateSplit[2] + "/" + dateSplit[1] + "/" + dateSplit[0]);

                        salesNumberData.push(salesData[key].totalSales);
                        valueData.push(salesData[key].totalValue);
                        costData.push(salesData[key].totalCost);
                    }
                    reportsApp.salesChartData = {
                        labels: salesChartLabels,
                        datasets: [
                            {
                                label: 'Valor total das vendas',
                                backgroundColor: chartColors.blue,
                                borderColor: chartColors.blue,
                                data: valueData,
                                fill: false
                            },
                            {
                                label: 'Custo total',
                                backgroundColor: chartColors.red,
                                borderColor: chartColors.red,
                                data: costData,
                                fill: false
                            }
                        ]
                    };
                    reportsApp.salesChart2Data = {
                        labels: salesChartLabels,
                        datasets: [
                            {
                                label: 'Total de vendas',
                                backgroundColor: chartColors.green,
                                borderColor: chartColors.green,
                                data: salesNumberData,
                                fill: false
                            }
                        ]
                    };

                    reportsApp.loading = false;
                },
                error: function (err) {
                    PrettyAlerts.show({
                        type: "danger",
                        message: err.responseJSON.message
                    });
                }
            });
        },
        getBillingResult(period) {
            if (!period) period = { to: '', from: '' };

            this.billingResult = [];
            this.loading.billingResults = true;

            $.get({
                url: '/getPeriodProfit',
                data: period,
                dataType: "json",
                success: function (response) {
                    reportsApp.billingResult = response.data.billing_result;
                    reportsApp.loading.billingResults = false;
                },
                error: function (err) {
                    reportsApp.loading.billingResults = false;
                    PrettyAlerts.show({
                        type: "danger",
                        message: err.responseJSON.message
                    })
                }
            })
        },
        getPagesTotal(period) {
            if (!period) period = { to: '', from: '' };

            this.pagesTotalItems = [];
            this.totalPagesPb = 0;
            this.totalPagesColor = 0;
            this.loading.pagesTotal = true;

            $.get({
                url: '/getColorsData',
                data: period,
                dataType: "json",
                success: function (response) {
                    var colors = response.data;
                    reportsApp.pagesTotalItems.push({ name: 'Papel Sulfite A4 75g', bwTotal: colors.pb_sulfite_a4_75g, cTotal: colors.c_sulfite_a4_75g, total: colors.pb_sulfite_a4_75g + colors.c_sulfite_a4_75g });
                    reportsApp.pagesTotalItems.push({ name: 'Papel Sulfite A4 90g', bwTotal: colors.pb_sulfite_a4_90g, cTotal: colors.c_sulfite_a4_90g, total: colors.pb_sulfite_a4_90g + colors.c_sulfite_a4_90g });
                    reportsApp.pagesTotalItems.push({ name: 'Papel Couché A4 115g', bwTotal: colors.pb_couche_a4_115g, cTotal: colors.c_couche_a4_115g, total: colors.pb_couche_a4_115g + colors.c_couche_a4_115g });
                    reportsApp.pagesTotalItems.push({ name: 'Papel Couché A4 170g', bwTotal: colors.pb_couche_a4_170g, cTotal: colors.c_couche_a4_170g, total: colors.pb_couche_a4_170g + colors.c_couche_a4_170g });
                    reportsApp.pagesTotalItems.push({ name: 'Papel Ofício 75g', bwTotal: colors.pb_oficio_75g, cTotal: colors.c_oficio_75g, total: colors.pb_oficio_75g + colors.c_oficio_75g });
                    reportsApp.pagesTotalItems.push({ name: 'Papel Eco A4 75g', bwTotal: colors.pb_eco_a4_75g, cTotal: colors.c_eco_a4_75g, total: colors.pb_eco_a4_75g + colors.c_eco_a4_75g });
                    reportsApp.pagesTotalItems.push({ name: 'Papel Carta 75g', bwTotal: colors.pb_carta_75g, cTotal: colors.c_carta_75g, total: colors.pb_carta_75g + colors.c_carta_75g });
                    reportsApp.pagesTotalItems.push({ name: 'Papel Sulfite A3 75g', bwTotal: colors.pb_sulfite_a3_75g, cTotal: colors.c_sulfite_a3_75g, total: colors.pb_sulfite_a3_75g + colors.c_sulfite_a3_75g });
                    reportsApp.pagesTotalItems.push({ name: 'Papel Couché A3 150g', bwTotal: colors.pb_couche_a3_150g, cTotal: colors.c_couche_a3_150g, total: colors.pb_couche_a3_150g + colors.c_couche_a3_150g });
                    reportsApp.pagesTotalItems.push({ name: 'Papel Sulfite A5 75g', bwTotal: colors.pb_sulfite_a5_75g, cTotal: colors.c_sulfite_a5_75g, total: colors.pb_sulfite_a5_75g + colors.c_sulfite_a5_75g });

                    reportsApp.pagesTotalItems.forEach(
                        function logArrayElements(element, index) {
                            reportsApp.totalPagesPb += element.bwTotal;
                            reportsApp.totalPagesColor += element.cTotal;
                        }
                    );
                    reportsApp.pagesTotalItems.push({ name: 'Totais', bwTotal: reportsApp.totalPagesPb, cTotal: reportsApp.totalPagesColor, total: reportsApp.totalPagesPb + reportsApp.totalPagesColor });

                    reportsApp.loading.pagesTotal = false;
                },
                error: function (err) {
                    reportsApp.loading.pagesTotal = false;
                    PrettyAlerts.show({
                        type: "danger",
                        message: err.responseJSON.message
                    });
                }
            });
        },
        getCouponsTotal(period) {
            if (!period) period = { to: '', from: '' };

            this.couponsTotalItems = [];
            this.invitationCoupons = [];
            this.loading.couponsTotal = true;

            $.get({
                url: '/getCouponsData',
                data: period,
                dataType: "json",
                success: function (response) {
                    var coupons = response.data;

                    reportsApp.couponsTotalItems = response.data;

                    $.get({
                        url: '/getInviteCouponsUsed',
                        data: period,
                        dataType: "json",
                        success: function (response) {
                            reportsApp.invitationCoupons = response.data;
                            reportsApp.loading.couponsTotal = false;
                        },
                        error: function (err) {
                            reportsApp.loading.couponsTotal = false;
                            PrettyAlerts.show({
                                type: "danger",
                                message: err.responseJSON.message
                            });
                        }
                    });
                },
                error: function (err) {
                    reportsApp.loading.couponsTotal = false;
                    PrettyAlerts.show({
                        type: "danger",
                        message: err.responseJSON.message
                    });
                }
            });
        },
        getFinishes(period) {
            if (!period) period = { to: '', from: '' };

            this.finishesItems = [];
            this.loading.finishes = true;

            $.get({
                url: '/getFinishesData',
                data: period,
                dataType: "json",
                success: function (response) {
                    var finishes = response.data;
                    reportsApp.finishesItems.push({ type: 'Folha Solta', quantity: finishes.solta });
                    reportsApp.finishesItems.push({ type: 'Grampeado', quantity: finishes.grampeado });
                    reportsApp.finishesItems.push({ type: 'Encadernar Espiral', quantity: finishes.espiral });
                    reportsApp.finishesItems.push({ type: 'Encadernar Capa Dura A4', quantity: finishes.capa_dura });
                    reportsApp.finishesItems.push({ type: 'Encadernar Capa Dura A3', quantity: finishes.capa_dura_a3 });
                    reportsApp.finishesItems.push({ type: 'Encadernar Wire-o', quantity: finishes.wire_o });
                    reportsApp.finishesItems.push({ type: 'Total', quantity: finishes.total });

                    reportsApp.loading.finishes = false;
                },
                error: function (err) {
                    reportsApp.loading.finishes = false;
                    PrettyAlerts.show({
                        type: "danger",
                        message: err.responseJSON.message
                    });
                }
            })
        },
        getDeliveryTypes(period) {
            if (!period) period = { to: '', from: '' };

            this.deliveryTypesItems = [];
            this.freightChartData.datasets = [];
            this.deliveryChartData.datasets = [];
            this.withdrawalChartData.datasets = [];
            this.loading.deliveryTypes = true;

            $.get({
                url: '/getDeliveryTypesData',
                data: period,
                dataType: "json",
                success: function (response) {
                    var deliveryTypes = response.data;
                    reportsApp.deliveryTypesItems.push({ type: 'ENTREGA TIPO 1', total: deliveryTypes.total_delivery_1 });
                    reportsApp.deliveryTypesItems.push({ type: 'ENTREGA TIPO 2', total: deliveryTypes.total_delivery_2 });
                    reportsApp.deliveryTypesItems.push({ type: 'ENTREGA TIPO 3', total: deliveryTypes.total_delivery_3 });
                    reportsApp.deliveryTypesItems.push({ type: 'ENTREGA TIPO 4', total: deliveryTypes.total_delivery_4 });
                    reportsApp.deliveryTypesItems.push({ type: 'ENTREGA TIPO 5', total: deliveryTypes.total_delivery_5 });
                    reportsApp.deliveryTypesItems.push({ type: 'ENTREGA TIPO 6', total: deliveryTypes.total_delivery_6 });
                    reportsApp.deliveryTypesItems.push({ type: 'ENTREGA TIPO 7', total: deliveryTypes.total_delivery_7 });
                    reportsApp.deliveryTypesItems.push({ type: 'ENTREGA TIPO 8', total: deliveryTypes.total_delivery_8 });
                    reportsApp.deliveryTypesItems.push({ type: 'RETIRADA TIPO 1', total: deliveryTypes.total_withdrawal_1 });
                    reportsApp.deliveryTypesItems.push({ type: 'RETIRADA TIPO 2', total: deliveryTypes.total_withdrawal_2 });
                    reportsApp.deliveryTypesItems.push({ type: 'RETIRADA TIPO 3', total: deliveryTypes.total_withdrawal_3 });
                    reportsApp.deliveryTypesItems.push({ type: 'RETIRADA TIPO 4', total: deliveryTypes.total_withdrawal_4 });

                    // Prepare delivery data for Pie Chart
                    reportsApp.freightChartData = {
                        datasets: [{
                            data: [
                                deliveryTypes.total_delivery_1 +
                                deliveryTypes.total_delivery_2 +
                                deliveryTypes.total_delivery_3 +
                                deliveryTypes.total_delivery_4 +
                                deliveryTypes.total_delivery_5 +
                                deliveryTypes.total_delivery_6 +
                                deliveryTypes.total_delivery_7 +
                                deliveryTypes.total_delivery_8,
                                deliveryTypes.total_withdrawal_1 +
                                deliveryTypes.total_withdrawal_2 +
                                deliveryTypes.total_withdrawal_3 +
                                deliveryTypes.total_withdrawal_4
                            ],
                            backgroundColor: [
                                chartColors.red,
                                chartColors.blue
                            ],
                            label: 'Entregas x Retiradas'
                        }],
                        labels: [
                            'Entregas',
                            'Retiradas'
                        ]
                    }

                    reportsApp.deliveryChartData = {
                        datasets: [{
                            data: [
                                deliveryTypes.total_delivery_1,
                                deliveryTypes.total_delivery_2,
                                deliveryTypes.total_delivery_3,
                                deliveryTypes.total_delivery_4,
                                deliveryTypes.total_delivery_5,
                                deliveryTypes.total_delivery_6,
                                deliveryTypes.total_delivery_7,
                                deliveryTypes.total_delivery_8
                            ],
                            backgroundColor: [
                                chartColors.red,
                                chartColors.orange,
                                chartColors.yellow,
                                chartColors.blue,
                                chartColors.green,
                                chartColors.grey,
                                chartColors.purple,
                                chartColors.blue
                            ],
                            label: 'Entregas'
                        }],
                        labels: [
                            'Tipo 1',
                            'Tipo 2',
                            'Tipo 3',
                            'Tipo 4',
                            'Tipo 5',
                            'Tipo 6',
                            'Tipo 7',
                            'Tipo 8'
                        ]
                    }

                    reportsApp.withdrawalChartData = {
                        datasets: [{
                            data: [
                                deliveryTypes.total_withdrawal_1,
                                deliveryTypes.total_withdrawal_2,
                                deliveryTypes.total_withdrawal_3,
                                deliveryTypes.total_withdrawal_4
                            ],
                            backgroundColor: [
                                chartColors.red,
                                chartColors.orange,
                                chartColors.yellow,
                                chartColors.blue
                            ],
                            label: 'Retiradas'
                        }],
                        labels: [
                            'Tipo 1',
                            'Tipo 2',
                            'Tipo 3',
                            'Tipo 4'
                        ]
                    }

                    reportsApp.loading.deliveryTypes = false;
                },
                error: function (err) {
                    reportsApp.loading.deliveryTypes = false;
                    PrettyAlerts.show({
                        type: "danger",
                        message: err.responseJSON.message
                    });
                }
            })
        },
        getRanges(period) {
            if (!period) period = { to: '', from: '' };

            this.rangesItems = [];
            this.totalPricePb = 0;
            this.totalPriceColor = 0;
            this.rangeChartData.datasets = [];
            this.loading.ranges = true;

            $.get({
                url: '/getRangesData',
                data: period,
                dataType: "json",
                success: function (response) {
                    var ranges = response.data;
                    var rangesCustomData = [];
                    var rangeChartLabels = [];
                    var rangeData = [];
                    for (var key in ranges) {
                        if (!ranges.hasOwnProperty(key)) continue;
                        var range = ranges[key];
                        rangesCustomData.push({
                            id: key,
                            paperType: range.paper_type,
                            color: range.color,
                            numPages: range.num_pages,
                            numItems: range.num_items,
                            earnings: range.items_price,
                            cost: range.items_cost,
                            result: range.result
                        })
                        range.color == 'Preto e Branco' ? reportsApp.totalPricePb += range.valuePages : reportsApp.totalPriceColor += range.valuePages;
                        rangeChartLabels.push(range.num_items);
                        rangeData.push(range.num_items);
                    }

                    reportsApp.rangeChartData = {
                        labels: rangeChartLabels,
                        datasets: [
                            {
                                backgroundColor: chartColors.blue,
                                borderColor: chartColors.blue,
                                rangesData: rangesCustomData,
                                data: rangeData
                            }
                        ]
                    };
                    reportsApp.rangesItems = rangesCustomData;

                    reportsApp.loading.ranges = false;
                },
                error: function (err) {
                    reportsApp.loading.ranges = false;
                    PrettyAlerts.show({
                        type: "danger",
                        message: err.responseJSON.message
                    });
                }
            })
        },
        getAverageTicket(period) {
            if (!period) period = { to: '', from: '' };

            this.averageTicket = 0.0;

            $.get({
                url: '/getAverageTicket',
                data: period,
                dataType: "json",
                success: function (response) {
                    reportsApp.averageTicket = response.data.average_ticket.billing;
                    console.log(reportsApp.averageTicket);
                },
                error: function (err) {
                    PrettyAlerts.show({
                        type: "danger",
                        message: err.responseJSON.message
                    });
                }
            });
        },
        getCustomersData(period) {
            if (!period) period = { to: '', from: '' };

            this.customerSalesChartData.datasets = [];

            $.get({
                url: '/getCustomerSales',
                data: period,
                dataType: "json",
                success: function (response) {
                    var newCustomers = response.data.newCustomers;
                    var recurringCustomers = response.data.recurringCustomers;

                    // Prepare customers data for Pie Chart
                    reportsApp.customerSalesChartData = {
                        datasets: [{
                            data: [
                                newCustomers,
                                recurringCustomers
                            ],
                            backgroundColor: [
                                chartColors.red,
                                chartColors.blue
                            ],
                            label: 'Clientes Novos x Recorrentes'
                        }],
                        labels: [
                            'Novos',
                            'Recorrentes'
                        ]
                    }

                    reportsApp.loading.customerSales = false;
                },
                error: function (err) {
                    reportsApp.loading.customerSales = false;
                    PrettyAlerts.show({
                        type: "danger",
                        message: err.responseJSON.message
                    });
                }
            })
        },
        getAllData(period) {
            if (!period) period = { to: '', from: '' };

            this.getSalesData(period);
            this.getBillingResult(period);
            this.getPagesTotal(period);
            this.getFinishes(period);
            this.getDeliveryTypes(period);
            this.getRanges(period);
            this.getCouponsTotal(period);
            this.getAverageTicket(period);
            this.getCustomersData(period);
        }
    },
    created() {
        this.getAllData();
    }
})
