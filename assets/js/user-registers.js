

$(document).ready(function() {

    $(".collapsed").click( function(){
        var elemento = $(this).children('.fa-angle-down');
        var elementoU = $(this).children('.fa-angle-up');
        setaCollapse(elemento, elementoU);
    });

});

var userData = {
    user:  {
        name: null,
        cpf: null,
        tel: null,
        cel: null,
        email: null,
        birth: null
    },
    curAddrIdx: -1,
    address: [],
    curAddress: {
        addrId: null,
        zip: null,
        number: null,
        street: null,
        district: null,
        city: null,
        uf: "",
        compl: null
    },
    password: {
        currentPassword: null,
        newPassword: null,
        newPasswordAgain: null
    }
};

var AccountMenu = require('./components/user/AccountMenu.vue');

var userApp = new Vue({
    el: '#forms',
    components: {
        'account-menu': AccountMenu
    },
    data: userData,
    methods: {
        updateCustomerData() {

            console.log('user', this.user);

            $.post('/updateCustomerData', this.user, null, 'json').then(function(r){
                PrettyAlerts.show({
                    type: 'info',
                    dismissable: true,
                    message: 'Dados salvos'
                });
            },  err => {
                PrettyAlerts.show({
                    type: 'danger',
                    dismissable: true,
                    message: err.responseJSON.message
                });
            });
        },
        newAddress() {
            this.curAddrIdx = -1;
            this.curAddress = {
                addrId: null,
                zip: null,
                number: null,
                street: null,
                district: null,
                city: null,
                uf: "",
                compl: null
            };
        },
        saveAddress() {

            var url, cb;

            if(this.curAddress.addrId) {
                url = '/updateCustomerAddress';
                cb = r => {
                    var idx = this.address.findIndex(addr => addr.addrId == this.curAddress.addrId);
                    this.address[idx] = this.curAddress;
                    PrettyAlerts.show({
                        type: 'info',
                        dismissable: true,
                        message: 'Informações salvas'
                    });
                };
            } else {
                url = '/addCustomerAddress';
                cb = r => {
                    this.curAddress = r.data;
                    this.address.push(this.curAddress);
                    PrettyAlerts.show({
                        type: 'info',
                        dismissable: true,
                        message: 'Informações salvas'
                    });
                };
            }

            $.post(url, this.curAddress, null, 'json').then(cb, err => PrettyAlerts.show({
                type: 'danger',
                dismissable: true,
                message: 'Preencha os campos de endereço corretamente'
            }));

        },
        setCurAddr(index) {
            this.curAddrIdx = index;
            this.curAddress = this.address[index];
        },
        searchZip() {
            var zip = this.curAddress.zip.replace(/\D/g, '');
            if (zip.length < 8)
                return;

            $.get("https://apps.widenet.com.br/busca-cep/api/cep.json", {code: zip},
             result => {
                if (parseInt(result.status) !== 1) {
                    return;
                }

                this.curAddress.street = result.address;
                this.curAddress.district = result.district;
                this.curAddress.number = '';
                this.curAddress.city = result.city;
                this.curAddress.uf = result.state;
            });
        },
        changePassword() {

            if(!this.password.newPassword || !this.password.newPasswordAgain) {
                PrettyAlerts.show({
                    type: 'danger',
                    dismissable: true,
                    message: 'Preencha todos os campos'
                });
                return false;
            }

            if(this.password.newPassword != this.password.newPasswordAgain) {
                PrettyAlerts.show({
                    type: 'danger',
                    dismissable: true,
                    message: 'Senhas não conferem'
                });
                return false;
            }

            $.post('/changePassword', this.password, null, 'json').then(r => {
                PrettyAlerts.show({
                    type: 'info',
                    dismissable: true,
                    message: 'Senha alterada com sucesso'
                });
                this.password.currentPassword = null;
                this.password.newPassword = null;
                this.password.newPasswordAgain = null;
            }, r => {
                PrettyAlerts.show({
                    type: 'danger',
                    dismissable: true,
                    message: r.responseJSON.message
                });
            });
        }
    },
    created() {
        $.get('/getCustomerData', null, null, 'json').then(function(r){
            var d = r.data;
            userApp.user = d.user;
            userApp.address = d.address;
        });
    }
});

function setaCollapse(elemento, elementoU) {

    elemento.addClass('fa-angle-up');

    if(elemento.hasClass('fa-angle-down')){
        var othersEl = $(".fa-angle-up");
        othersEl.removeClass('fa-angle-up');
        othersEl.addClass('fa-angle-down');
        elemento.removeClass('fa-angle-down');
        elemento.addClass('fa-angle-up');

    } else {
        // var elemento = $(this).children('.fa-angle-up');
        elementoU.addClass('fa-angle-down');
        elementoU.removeClass('fa-angle-up');
    }

}
