function registerECommerce(orderId, orderValue, tax, deliveryValue) {
    if (window.dataLayer) {
        // Disparar o evento de compra do Google Tag Manager - vai enviar os dados para o Ads e o Analytics
        window.dataLayer.push({
            'event': 'pedidoFinalizado',
            'transactionId': orderId,
            'transactionTotal': orderValue,
            'transactionTax': tax,
            'transactionShipping': deliveryValue
        });
    }
}

var ptype = new Vue({
    el: '#confirm-dialog',
    data: {
        isBoleto: (Cookies.get("boleto") === "true"),
        isPromotion: (Cookies.get("promotion") === "true"),
        email: "",
        forecast: "",
        isWithdrawal: false,
        cartao: {
            bandeira: "",
            finalNum: ""
        }
    },
    methods: {
        displayBankSlip: function () {
            $.post('/getBankSlip', {}, null, 'json').then(
                response => {
                    window.location.href = response.data.secure_url;
                },
                err => {
                    PrettyAlerts.show({
                        type: 'danger',
                        dismissable: true,
                        message: err.responseJSON.message
                    });
                }
            )
        }
    }
});

var shopData = {
    pedido: {},
    resumo: [],
    items: [],
    endereco: {
        cep: "",
        logradouro: "",
        bairro: "",
        cidade: ""
    },
    peddoFormatado: {}
};

var confirm = new Vue({
    el: "#itemList",
    data: shopData,
    methods: {

        countPages(index) {
            var totalPages = 0;
            this.items[index].files.forEach(f => {
                totalPages += (parseInt(f.numPaginas) * parseInt(f.qtdImpressoes));
            });

            return totalPages;
        },
    },
    created: function () {
        $.get({
            url: "/getPaymentData",
            dataType: "json",
            success: function (response) {
                confirm.pedido = response.data.pedido;
                confirm.items = response.data.items;
                ptype.email = response.data.dadosCliente.emailCliente;
                ptype.forecast = response.data.items[0].delivery.forecast;
                ptype.isWithdrawal = parseInt(response.data.items[0].delivery.tbRetirada_idRetirada);
                ptype.cartao = response.data.cartao;

                var estimatedDeliveryDate = response.data.items[0].delivery.estimatedDeliveryDate;

                // BEGIN GCR Opt-in Module Code
                window.renderOptIn = renderOptIn(confirm.pedido.id, ptype.email, estimatedDeliveryDate);
                // END GCR Opt-in Module Code

                if (Cookies.get("sendToGa")) {
                    Cookies.remove("sendToGa");

                    $.post({
                        url: "/getOrderFormatted",
                        data: { orderId: confirm.pedido.id },
                        dataType: "json",
                        success: function (response) {
                            var order = response.data.order;
                            registerECommerce(order.id, order.order_value, order.tax, order.delivery_value);
                        },
                        error: function () {
                            console.log(arguments);
                        }
                    });
                }
            },
            error: function () {
                console.log(arguments);
            }
        });
    }
});

var AccountMenu = require('./components/user/AccountMenu.vue');

var accountMenuApp = new Vue({
    el: '#accountMenuApp',
    components: {
        'account-menu': AccountMenu
    }
})

function renderOptIn(orderId, customerEmail, deliveryDate) {
    window.gapi.load('surveyoptin', function () {
        window.gapi.surveyoptin.render(
            {
                // REQUIRED
                "merchant_id": 115445284,
                "order_id": orderId,
                "email": customerEmail,
                "delivery_country": "BR",
                "estimated_delivery_date": deliveryDate,
            });
    });
}

function getUri() {
    return window.location.href;
}
function getParam() {
    var p = String(getUri()).split('?')[1].split('&')[0].split('=');
    return p[1];
}
function isBoleto(param) {
    if (param == "boleto") {
        return true;
    }
    return false;
}
