export default {
    data() {
        return {
            step: 1,
            active: false,
            overlay: false,
            overlayZIndex: 1
        }
    },
    computed: {
        showStep1() {
            return this.active && this.step == 1;
        },
        showStep2() {
            return this.active && this.step == 2;
        }
    },
    methods: {
        closeStep() {
            switch (this.step) {
                case 1:
                    Cookies.set('lastStep', 1, { expires: 365 });
                    $('#accordion').removeClass('highlighted');
                    this.overlay = false;
                    this.active = false;
                    this.enableClicks();
                    break;
                case 2:
                    Cookies.set('lastStep', 2, { expires: 365 });
                    $('#card-anexar').removeClass('highlighted');
                    this.overlay = false;
                    this.active = false;
                    this.step = 3;
                    this.enableClicks();
                    break;
            }
        },
        stepOne() {
            if (this.step > 1) return;
            $('#accordion').addClass('highlighted');
            this.overlayZIndex = 2;
            this.overlay = true;
            this.step = 1;
            this.active = true;
            this.disableClicks()
        },
        stepTwo() {
            if (this.step > 2) return;
            $('#card-anexar').addClass('highlighted');
            this.overlayZIndex = 4;
            this.overlay = true;
            this.step = 2;
            this.active = true;
            this.disableClicks();
        },
        initTutorial() {
            let lastStep = parseInt(Cookies.get('lastStep'));
            if (lastStep) this.step = lastStep + 1;
            if (this.step == 1) this.stepOne();
        },
        clickHandler(e) {
            let targetClass = e.target.className;
            if (targetClass !== "tutorial-step tutorial-step-1" &&
                targetClass !== "tutorial-p" &&
                targetClass !== "tutorial-btn-container clearfix" &&
                targetClass !== "tutorial-arrow") {
                this.closeStep();
            }
        },
        disableClicks() {
            document.addEventListener("click", this.clickHandler, true);
        },
        enableClicks() {
            document.removeEventListener('click', this.clickHandler, true);
        }
    },
    mounted() {
        this.initTutorial();
    }
}
