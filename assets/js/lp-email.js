var data = {
    user_email: "",
    user_name: "",
    user_tel: "",
    description: "",
    subject: "",
    emailType: null
};

const emailType = {
    'budget-request-description': 1
}

var eMail = new Vue({
    el: '#emailDiv',
    data: data,
    methods: {
        sendEmail() {
            $.post('/budget/sendEmail', data, null, 'json').then(
                res => {
                    PrettyAlerts.show({
                        type: 'info',
                        dismissable: true,
                        message: res.message
                    });
                },
                err => {
                    PrettyAlerts.show({
                        type: 'danger',
                        dismissable: true,
                        message: err.responseJSON.message
                    });
                }
            )
        }
    },
    mounted() {
        this.subject = this.$refs.subject_input.value;
        this.emailType = emailType[this.$refs.email_type.value];
    },
});
