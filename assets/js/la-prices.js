var CsvTable = require('./components/admin/CsvTable.vue');
var PricesForm = require('./components/admin/PricesForm.vue');

var pricesApp = new Vue({
    el: '#pricesApp',
    components: {
        CsvTable,
        PricesForm
    }
})
