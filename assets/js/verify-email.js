var verifyApp = new Vue({
    el: "#verify-app",
    data: {
        hash: '',
        loading: true,
        error: false,
        errorMsg: ''
    },
    methods: {
        getParameterByName(name, url) {
            if (!url) url = window.location.href;
            name = name.replace(/[\[\]]/g, "\\$&");
            var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
                results = regex.exec(url);
            if (!results) return null;
            if (!results[2]) return "";
            return decodeURIComponent(results[2].replace(/\+/g, " "));
        }
    },
    created() {
        this.hash = this.getParameterByName('hash');
        $.post(
            "/verifyEmail",
            {
                hash: this.hash
            },
            null,
            "json"
        ).then(
            r => {
                this.loading = false;
            },
            err => {
                this.loading = false;
                this.error = true;
                this.errorMsg = err.responseJSON.message;
            }
        );
    }
})
