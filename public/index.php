<?php

//Everything is relative to the application root now.
chdir(dirname(__DIR__));
if (!file_exists('./vendor/autoload.php')) {
    echo 'Please run `composer install` first!';
}

date_default_timezone_set('America/Sao_Paulo');

include './vendor/autoload.php';

$settings = require_once 'app/config/settings.php';

if(file_exists('app/config/settings.local.php')) {
    $settings = array_replace_recursive($settings, require 'app/config/settings.local.php');
}

if($settings['display_errors']) {
    error_reporting(E_ALL);
    ini_set("display_errors", 1);
};

use Intec\Router\SimpleRouter;
use IntecPhp\Middleware\HttpMiddleware;
use Pimple\Psr11\Container;

SimpleRouter::setRoutes(require 'app/config/routes.php');

SimpleRouter::setNotFoundFallback(HttpMiddleware::class . ':pageNotFound');
SimpleRouter::setErrorFallback(HttpMiddleware::class . ':fatalError');

require 'app/config/dependencies.php';

SimpleRouter::match($_SERVER['REQUEST_URI'], new Container($dependencies));
