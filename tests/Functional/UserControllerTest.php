<?php

namespace IntecPhp\Test\Functional;

use GuzzleHttp\Client;
use PHPUnit\Framework\TestCase;

class UserControllerTest extends TestCase
{
    public function testMyOrdersSuccess()
    {
        //         'base_url' => [$url, []],
        // 'defaults' => [
        //      'auth' => [$publishable_key, ''],
        //      'headers'  => ['content-type' => 'application/json', 'Accept' => 'application/json'],
        //      'body' => json_encode($body),
        // ],

        // $client = new Client([
        //     'base_uri' => 'http://localhost:8080',
        //     'defaults' => [
        //         'headers'  => [
        //             'content-type' => 'application/x-www-form-urlencoded',
        //             'Accept' => 'application/json'
        //         ],
        //     ]
        // ]);

        $client = new Client([
            'base_uri' => 'http://localhost:8080',
            'defaults' => [
                'headers'  => [
                    'content-type' => 'application/x-www-form-urlencoded'
                ],
            ],
        ]);

        // Content-Type: application/x-www-form-urlencoded; charset=UTF-8

        $response = $client->request(
            'GET',
            '/changePassword'
        );
        $body = $response->getBody()->getContents();

        var_dump($body);
    }
}
